#!/bin/sh

CURDIR=`pwd`

# BUILD_DIR_32=${CURDIR}/build-XCode-x32
BUILD_DIR_64=${CURDIR}/build-XCode-x64

INSTALL_BASE=${CURDIR}/../../..
WL_BASE=${INSTALL_BASE}/WrappedLogic

PROTOBUF_CPP_TARGET=${WL_BASE}/external/protobuf
PROTOBUF_PROTO_TARGET=${INSTALL_BASE}/Proto

# Copy proto compiler.
mkdir -p "${PROTOBUF_PROTO_TARGET}/bin"
cp "${BUILD_DIR_64}/Release/protoc" "${PROTOBUF_PROTO_TARGET}/bin/protoc-mac"

# Copy 64-bit libraries.
mkdir -p "${PROTOBUF_CPP_TARGET}/lib/mac/64-bit/release"
cp "${BUILD_DIR_64}/Release/libprotobuf.a" "${PROTOBUF_CPP_TARGET}/lib/mac/64-bit/release/libprotobuf.a"
mkdir -p "${PROTOBUF_CPP_TARGET}/lib/mac/64-bit/debug"
cp "${BUILD_DIR_64}/Debug/libprotobuf.a" "${PROTOBUF_CPP_TARGET}/lib/mac/64-bit/debug/libprotobuf.a"

