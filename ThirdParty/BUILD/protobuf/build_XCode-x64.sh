#!/bin/sh

mkdir build-XCode-x64
cd build-XCode-x64
cmake -DBUILD_TESTING:BOOL=OFF -G "Xcode" ../../../VENDOR/protobuf/cmake
if [ $? -eq 0 ]
then
  echo "Opening XCode project..."
  open "protobuf.xcodeproj"
else
  echo "Cmake failed. Check for errors." >&2
fi
