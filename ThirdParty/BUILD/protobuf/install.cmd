@echo off
SETLOCAL

SET CURDIR=%~dp0

SET BUILD_DIR_32=%CURDIR%build-32
SET BUILD_DIR_64=%CURDIR%build-64
SET BUILD_DIR_CS=%CURDIR%build-cs

SET EXTRACT_CMD=%CURDIR%build-64/extract_includes.bat

SET INSTALL_BASE=%CURDIR%../../..
SET WL_BASE=%INSTALL_BASE%/WrappedLogic

SET PROTOBUF_CPP_TARGET=%WL_BASE%/external/protobuf
SET PROTOBUF_CS_TARGET=%INSTALL_BASE%/Assets/ThirdParty/Google.Protobuf
SET PROTOBUF_PROTO_TARGET=%INSTALL_BASE%/Proto

# Copy include files.
mkdir "%PROTOBUF_CPP_TARGET%"
pushd "%PROTOBUF_CPP_TARGET%"
call "%EXTRACT_CMD%"
popd

# Copy proto compiler.
robocopy "%BUILD_DIR_32%/Release/" "%PROTOBUF_PROTO_TARGET%/bin/" protoc.exe

# Copy 32-bit libraries.
robocopy "%BUILD_DIR_32%/Release/" "%PROTOBUF_CPP_TARGET%/lib/win32/32-bit/release/" libprotobuf.lib
robocopy "%BUILD_DIR_32%/Debug/" "%PROTOBUF_CPP_TARGET%/lib/win32/32-bit/debug/" libprotobuf.lib

# Copy 64-bit libraries.
robocopy "%BUILD_DIR_64%/Release/" "%PROTOBUF_CPP_TARGET%/lib/win32/64-bit/release/" libprotobuf.lib
robocopy "%BUILD_DIR_64%/Debug/" "%PROTOBUF_CPP_TARGET%/lib/win32/64-bit/debug" libprotobuf.lib

# Copy .Net Assembly.
robocopy "%BUILD_DIR_CS%/bin/" "%PROTOBUF_CS_TARGET%/" *.dll *.xml
