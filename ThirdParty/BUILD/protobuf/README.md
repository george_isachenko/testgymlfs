1. On Windows 8 + (8, 8.1, 10) Microsoft .NET Framework 2.0 + 3.5 SP1 (dotnetfx35setup.exe) must be enabled in "Windows features" to build Protobuf!
See: http://getdotnet.azurewebsites.net/target-dotnet-platforms.html , https://msdn.microsoft.com/library/hh506443.aspx

2. We're also using our own project file because default is tuned to 'Portable' mode that targets .Net 4.0 or later.
So, it must be updated if Protobuf changes projects content (new/moved/deleted source files).

3. This version has missing some components that fail to compile on .Net 3.5 target: Compatibility/* folder, WellKnownTypes/TimeExtension.cs .
