@echo off
SETLOCAL

:start

CALL "%VS120COMNTOOLS%VsDevCmd.bat"
IF ERRORLEVEL 1 GOTO error_MSBUILD_SETUP

mkdir build-64
pushd build-64

cmake -DBUILD_TESTING:BOOL=OFF -G "Visual Studio 12 Win64" ../../../VENDOR/protobuf/cmake
IF ERRORLEVEL 1 GOTO error_CMAKE

MsBuild /m protobuf.sln /t:libprotobuf /p:Configuration=Release
IF ERRORLEVEL 1 GOTO error_MSBUILD_BUILD_RELEASE

MsBuild /m protobuf.sln /t:libprotobuf /p:Configuration=Debug
IF ERRORLEVEL 1 GOTO error_MSBUILD_BUILD_DEBUG

popd

mkdir build-32
pushd build-32

cmake -DBUILD_TESTING:BOOL=OFF -G "Visual Studio 12" ../../../VENDOR/protobuf/cmake
IF ERRORLEVEL 1 GOTO error_CMAKE

MsBuild /m protobuf.sln /t:libprotobuf /p:Configuration=Release
IF ERRORLEVEL 1 GOTO error_MSBUILD_BUILD_RELEASE

MsBuild /m protobuf.sln /t:libprotobuf /p:Configuration=Debug
IF ERRORLEVEL 1 GOTO error_MSBUILD_BUILD_DEBUG

:: Also build protoc.exe compiler in 32-bit Release version.
MsBuild /m protobuf.sln /t:protoc /p:Configuration=Release
IF ERRORLEVEL 1 GOTO error_MSBUILD_BUILD_RELEASE

popd

mkdir build-CS/bin
mkdir build-CS/obj
MsBuild /m Google.Protobuf/Google.Protobuf.csproj /t:Build /p:AssemblyName=Google.Protobuf;Configuration=Release;OutputPath=%~dp0/build-CS/bin/;IntermediateOutputPath=%~dp0/build-CS/obj/;DocumentationFile=%~dp0/build-CS/bin/Google.Protobuf.xml
IF ERRORLEVEL 1 GOTO error_MSBUILD_CSHARP

GOTO end

:error_CMAKE
ECHO Error: CMake failed.
GOTO end

:error_MSBUILD_SETUP
ECHO Error: Failed to setup MsBuild. Check for %VS120COMNTOOLS%VsDevCmd.bat file.
GOTO end

:error_MSBUILD_BUILD_RELEASE
ECHO Error: Failed to build Relese build using MsBuild.
GOTO end

:error_MSBUILD_BUILD_RELEASE
ECHO Error: Failed to build Debug build using MsBuild.
GOTO end

:error_MSBUILD_BUILD_CSHARP
ECHO Error: Failed to build C# assembly using MsBuild.
GOTO end

:end
