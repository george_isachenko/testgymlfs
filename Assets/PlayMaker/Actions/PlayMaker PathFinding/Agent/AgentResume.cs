// (c) Copyright HutongGames, LLC 2010-2014. All rights reserved.

using UnityEngine;
#if UNITY_5_5_OR_NEWER
using UnityEngine.AI;
#endif

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.NavMeshAgent)]
	[Tooltip("Resumes the movement along the current path of a navMesh Agent. \nNOTE: The Game Object must have a NavMeshAgent component attached.")]
	public class AgentResume : FsmStateAction
	{
		[RequiredField]
		[Tooltip("The Game Object to work with. NOTE: The Game Object must have a NavMeshAgent component attached.")]
		[CheckForComponent(typeof(NavMeshAgent))]
		public FsmOwnerDefault gameObject;
		
		private NavMeshAgent _agent;
		
		private void _getAgent()
		{
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			if (go == null) 
			{
				return;
			}
			
			_agent =  go.GetComponent<NavMeshAgent>();
		}	
		
		public override void Reset()
		{
			gameObject = null;
		}

		public override void OnEnter()
		{
			_getAgent();
			
			DoResume();

			Finish();		
		}
		
		void DoResume()
		{
			if (_agent == null) 
			{
				return;
			}
			
#if UNITY_5_6_OR_NEWER
			_agent.isStopped = false;
#else
			_agent.Resume();
#endif
		}

	}
}