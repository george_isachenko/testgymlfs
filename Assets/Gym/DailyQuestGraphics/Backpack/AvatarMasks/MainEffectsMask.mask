%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: MainEffectsMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: backpack
    m_Weight: 0
  - m_Path: joint_ALL
    m_Weight: 1
  - m_Path: joint_ALL/joint1
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint3
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint4
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint5
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint6
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint7
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint8
    m_Weight: 1
  - m_Path: joint_ALL/joint1/joint10
    m_Weight: 1
  - m_Path: joint_ALL/joint2
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint11
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint12
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint13
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint14
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint15
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint16
    m_Weight: 1
  - m_Path: joint_ALL/joint2/joint17
    m_Weight: 1
  - m_Path: joint_ALL/joint18
    m_Weight: 1
  - m_Path: joint_ALL/joint18/joint21
    m_Weight: 1
  - m_Path: joint_ALL/joint18/joint22
    m_Weight: 1
  - m_Path: joint_ALL/joint18/joint23
    m_Weight: 1
  - m_Path: joint_ALL/joint18/joint24
    m_Weight: 1
  - m_Path: joint_ALL/joint19
    m_Weight: 1
  - m_Path: joint_ALL/joint20
    m_Weight: 1
  - m_Path: joint_ALL/joint_sobachka
    m_Weight: 1
  - m_Path: joint_ALL/joint_sobachka/joint_ruchka
    m_Weight: 1
