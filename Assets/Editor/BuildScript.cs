﻿using UnityEngine;
using System.IO;
using UnityEditor;
using System.Linq;
using UnityEditor.Callbacks;
using System;

#if UNITY_IOS

using UnityEditor.iOS.Xcode;

#endif

public class BuildScript : MonoBehaviour
{
    static string androidBuildDir   { get { return new DirectoryInfo(Application.dataPath + "/../Build/Android").FullName; } }
    static string iosBuildDir       { get { return new DirectoryInfo(Application.dataPath + "/../Build/iOS").FullName; } }
    static string androidApkDir     { get { return string.Format("{0}/{1}_{2}({3}).apk", androidBuildDir, "dreamgym", Application.version, PlayerSettings.Android.bundleVersionCode); } }

    static bool CheckDir (string dir)
    {
        try
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            else
            {
                Directory.Delete(dir, true);
                Directory.CreateDirectory(dir);
            }
            return true;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return false;
        }
    }

    static string[] GetScenes ()
    {
        return EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
    }

    // Command-line build target.
    public static void IOSDbg ()
    {
        BuildIOSCommon(BuildOptions.Development, false);
    }

    // Command-line build target.
    public static void IOS ()
    {
        BuildIOSCommon(BuildOptions.None, false);
    }

    // Command-line build target.
    public static void AndroidDbg ()
    {
        BuildAndroidCommon(BuildOptions.Development, false);
    }

    // Command-line build target.
    public static void Android ()
    {
        BuildAndroidCommon(BuildOptions.None, false);
    }

    [MenuItem("Window/Build Script/Build iOS Debug")]
    public static void IOSDbgMenu ()
    {
        BuildIOSCommon(BuildOptions.Development, true);
    }

    [MenuItem("Window/Build Script/Build iOS Release")]
    public static void IOSMenu ()
    {
        BuildIOSCommon(BuildOptions.None, true);
    }

    [MenuItem("Window/Build Script/Build Android Debug")]
    public static void AndroidDbgMenu ()
    {
        BuildAndroidCommon(BuildOptions.Development, true);
    }

    [MenuItem("Window/Build Script/Build Android Release")]
    public static void AndroidMenu ()
    {
        BuildAndroidCommon(BuildOptions.None, true);
    }

    public static void BuildIOSCommon (BuildOptions buildOptions, bool interactive)
    {
        var iosBuildDir = BuildScript.iosBuildDir; // Cache it.

        if (!CheckDir(iosBuildDir))
        {
            Debug.LogErrorFormat("Cannot create or clean build output directory: '{0}'.", iosBuildDir);
            return;
        }

        var activeBuildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        var activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;

        var desiredBuildTargetGroup = BuildTargetGroup.iOS;
        var desiredBuildTarget = BuildTarget.iOS;

        if (activeBuildTargetGroup != desiredBuildTargetGroup || activeBuildTarget != desiredBuildTarget)
        {
            // Debug.LogFormat("Switching active build target to {0}, this can take a while...", desiredBuildTarget);

            // TODO: Remove this code when we can switch target and perform build (with PostBuildStep running) at the same time.
            if (interactive)
            {
                if (EditorUtility.DisplayDialog("Build Script"
                    , string.Format("We must switch to {0} platform first, current platform is: {1}. Do you want to switch now?"
                    , desiredBuildTarget, activeBuildTarget), "OK", "Cancel"))
                {
                    if (SwitchActiveBuildTarget(desiredBuildTargetGroup, desiredBuildTarget))
                    {
                        EditorUtility.DisplayDialog("Build Script"
                            , string.Format("Platform successfully switched to {0}, please invoke this menu again..."
                            , desiredBuildTarget), "OK");
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Build Script"
                            , string.Format("Failed to switch platform to {0}, check log for details..."
                            , desiredBuildTarget), "OK");
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat("Please switch active build target to {0} platform.", desiredBuildTarget);
            }
            return;
        }

        if ((activeBuildTarget == desiredBuildTarget && activeBuildTargetGroup == desiredBuildTargetGroup) ||
            SwitchActiveBuildTarget(desiredBuildTargetGroup, desiredBuildTarget))
        {

#if !UNITY_5_6_OR_NEWER
            var rememberedBundleIdentifier = PlayerSettings.iPhoneBundleIdentifier;
            PlayerSettings.iPhoneBundleIdentifier = "com.tatemgames.dreamgym2";
#endif

            try
            {
                PlayerSettings.iOS.buildNumber = (Convert.ToInt32(PlayerSettings.iOS.buildNumber) + 1).ToString();
            }
            catch (Exception ex)
            {
            	Debug.LogWarningFormat("Failed to bump iOS BuildNumber before building: {0}.", ex);
            }

            try
            {
                Debug.LogFormat("Building to: '{0}'...", iosBuildDir);

                BuildPipeline.BuildPlayer(GetScenes(), iosBuildDir, desiredBuildTarget, buildOptions);
            }
            finally
            {
                if (activeBuildTarget != desiredBuildTarget)
                {
                    Debug.LogFormat("Switching active build target back to {0}, this can take a while...", activeBuildTarget);
                    SwitchActiveBuildTarget(activeBuildTargetGroup, activeBuildTarget);
                }

#if !UNITY_5_6_OR_NEWER
                PlayerSettings.iPhoneBundleIdentifier = rememberedBundleIdentifier;
#endif
            }
        }
        else
        {
            Debug.LogErrorFormat("Failed to switch build target to {0}, see log for details.", desiredBuildTarget);
        }
    }

    private static void BuildAndroidCommon (BuildOptions buildOptions, bool interactive)
    {
        var androidBuildDir = BuildScript.androidBuildDir; // Cache it.

        if (!CheckDir(androidBuildDir))
        {
            Debug.LogErrorFormat("Cannot create or clean build output directory: '{0}'.", androidBuildDir);
            return;
        }

        var activeBuildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        var activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;

        var desiredBuildTargetGroup = BuildTargetGroup.Android;
        var desiredBuildTarget = BuildTarget.Android;

        if (activeBuildTargetGroup != desiredBuildTargetGroup || activeBuildTarget != desiredBuildTarget)
        {
            // Debug.LogFormat("Switching active build target to {0}, this can take a while...", desiredBuildTarget);

            // TODO: Remove this code when we can switch target and perform build (with PostBuildStep running) at the same time.
            if (interactive)
            {
                if (EditorUtility.DisplayDialog("Build Script"
                    , string.Format("We must switch to {0} platform first, current platform is: {1}. Do you want to switch now?"
                    , desiredBuildTarget, activeBuildTarget), "OK", "Cancel"))
                {
                    if (SwitchActiveBuildTarget(desiredBuildTargetGroup, desiredBuildTarget))
                    {
                        EditorUtility.DisplayDialog("Build Script"
                            , string.Format("Platform successfully switched to {0}, please invoke this menu again..."
                            , desiredBuildTarget), "OK");
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Build Script"
                            , string.Format("Failed to switch platform to {0}, check log for details..."
                            , desiredBuildTarget), "OK");
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat("Please switch active build target to {0} platform.", desiredBuildTarget);
            }
            return;
        }

        if ((activeBuildTarget == desiredBuildTarget && activeBuildTargetGroup == desiredBuildTargetGroup) ||
            SwitchActiveBuildTarget(desiredBuildTargetGroup, desiredBuildTarget))
        {
#if !UNITY_5_6_OR_NEWER
            var rememberedBundleIdentifier = PlayerSettings.iPhoneBundleIdentifier;
            PlayerSettings.iPhoneBundleIdentifier = "com.tatemgames.dreamgym";
#endif

            try
            {
                // Seems that it doesn't not WORK! See 'BuildOptions.AcceptExternalModificationsToPlayer' in a call to BuildPlayer() instead!
                // See https://forum.unity3d.com/threads/buildpipeline-failing-only-for-android-but-i-get-no-errors-and-it-appears-to-work-5-4-1p2.434327/
                EditorUserBuildSettings.exportAsGoogleAndroidProject = true; 

                EditorUserBuildSettings.androidBuildSubtarget = MobileTextureSubtarget.ETC;
                EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

                PlayerSettings.Android.bundleVersionCode++;

#if UNITY_EDITOR_OSX
                EditorPrefs.SetString("AndroidSdkRoot", "/Users/Shared/sdk/android-sdk-macosx/");
                EditorPrefs.SetString("JdkPath", "/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home/");
                EditorPrefs.SetString("AndroidNdkRoot", "/Users/Shared/sdk/android-ndk-r10e/");
#elif UNITY_EDITOR_WIN
                // 
#endif

                PlayerSettings.Android.keystoreName = "/Users/Shared/tatem.keystore";
                PlayerSettings.Android.keystorePass = "tatemNik2011";
                PlayerSettings.Android.keyaliasName = "tatemgames";
                PlayerSettings.Android.keyaliasPass = "TatemNik2011";

                Debug.LogFormat("Building to: '{0}'...", androidBuildDir);

                // var errorString = BuildPipeline.BuildPlayer(GetScenes(), androidAppName, desiredBuildTarget, buildOptions);
                var errorString = BuildPipeline.BuildPlayer(GetScenes(), androidBuildDir, desiredBuildTarget, buildOptions | BuildOptions.AcceptExternalModificationsToPlayer);
                if (errorString != null && errorString != string.Empty)
                {
                    Debug.LogErrorFormat("----------------\nError while building player:\n{0}\n----------------\n", errorString);
                }

                //File.Copy(androidAppName, "./Build/" + androidAppName);
            }
            finally
            {
                if (activeBuildTarget != desiredBuildTarget)
                {
                    Debug.LogFormat("Switching active build target back to {0}, this can take a while...", activeBuildTarget);
                    SwitchActiveBuildTarget(activeBuildTargetGroup, activeBuildTarget);
                }

#if !UNITY_5_6_OR_NEWER
                PlayerSettings.iPhoneBundleIdentifier = rememberedBundleIdentifier;
#endif
            }
        }
        else
        {
            Debug.LogErrorFormat("Failed to switch build target to {0}, see log for details.", desiredBuildTarget);
        }
    }

    [PostProcessBuild(1000000)]
    public static void OnPostProcessBuild (BuildTarget target, string pathToBuildProject)
    {
        if (target == BuildTarget.iOS || target == BuildTarget.tvOS)
        {
#if UNITY_IOS
            UpdateXCodeProject(pathToBuildProject);
            UpdatePlist(pathToBuildProject);
#else
            Debug.LogWarning("WARNING! Please set 'iPhone' platform target before building in MaxOS X Unity Editor!");
#endif
        }
        else if (target == BuildTarget.Android)
        {
            CopyAndroidFiles(pathToBuildProject);
        }
    }

#if UNITY_IOS
    private static void UpdateXCodeProject (string pathToBuildProject)
    {
        var pbxProjectPath = PBXProject.GetPBXProjectPath(pathToBuildProject);

        var proj = new PBXProject();
        proj.ReadFromFile(pbxProjectPath);

        string targetName = PBXProject.GetUnityTargetName();
        string targetGuid = proj.TargetGuidByName(targetName);

        proj.AddFrameworkToProject(targetGuid, "UserNotifications.framework", true);

        proj.WriteToFile(pbxProjectPath);
    }

    private static void UpdatePlist (string pathToBuildProject)
    {
        string plistPath = Path.Combine (pathToBuildProject, "Info.plist");

        var plistDocument = new PlistDocument();
        plistDocument.ReadFromFile(plistPath);

        PlistElement uiRequiredDeviceCapabilities = null;
        if (!plistDocument.root.values.TryGetValue("UIRequiredDeviceCapabilities", out uiRequiredDeviceCapabilities))
        {
            uiRequiredDeviceCapabilities = plistDocument.root.CreateArray("UIRequiredDeviceCapabilities");
        }

        // It can be either array or dictionary by specification, though Unity prefers writing arrays.
        // See: https://developer.apple.com/library/content/documentation/General/Reference/InfoPlistKeyReference/Articles/iPhoneOSKeys.html#//apple_ref/doc/uid/TP40009252-SW3
        if (uiRequiredDeviceCapabilities is PlistElementArray)
        {
            var arrayUIRequiredDeviceCapabilities = uiRequiredDeviceCapabilities as PlistElementArray;
            if (!arrayUIRequiredDeviceCapabilities.values.Any(x => x.AsString() == "gamekit"))
            {
                arrayUIRequiredDeviceCapabilities.AddString("gamekit");
            }

        }
        else if (uiRequiredDeviceCapabilities is PlistElementDict)
        {
            var dictUIRequiredDeviceCapabilities = uiRequiredDeviceCapabilities as PlistElementDict;
            if (!dictUIRequiredDeviceCapabilities.values.ContainsKey("gamekit"))
            {
                dictUIRequiredDeviceCapabilities.SetBoolean("gamekit", true);
            }
        }
        else
        {
            Debug.LogWarningFormat("UIRequiredDeviceCapabilities property in Info.Plist is not a dictionary or array, it's type is: {0}", uiRequiredDeviceCapabilities.GetType().Name);
        }

        if (!plistDocument.root.values.ContainsKey("NSCalendarsUsageDescription"))
        {
            plistDocument.root.SetString("NSCalendarsUsageDescription", "Required by third-party SDK");
        }

        // We're declaring that we USE encryption but only those that falls into "Exemptions" that allows us to skip registration of app in US Trade export regulator (SNAP-R).
        // Examples of Exemptions: use of HTTPS, use of encryption for own data protection (if 'common known' algorithms used).
        // When this key is set in Info.Plist, Apple skips dialogs that asks about encryption usage or its state changes.

        if (!plistDocument.root.values.ContainsKey("ITSAppUsesNonExemptEncryption"))
        {
            plistDocument.root.SetBoolean("ITSAppUsesNonExemptEncryption", false);
        }

        PlistElement tmpElement = null;
        if (!plistDocument.root.values.TryGetValue("CFBundleLocalizations", out tmpElement))
        {
            tmpElement = plistDocument.root.CreateArray("CFBundleLocalizations");
        }
            
        PlistElementArray CFBundleLocalizations = tmpElement as PlistElementArray;

        if (CFBundleLocalizations != null)
        {
            CFBundleLocalizations.AddString("en");
            CFBundleLocalizations.AddString("fr");
            CFBundleLocalizations.AddString("de");
            CFBundleLocalizations.AddString("en");
            CFBundleLocalizations.AddString("it");
            CFBundleLocalizations.AddString("zh_CN");
            CFBundleLocalizations.AddString("Russian");
            CFBundleLocalizations.AddString("Spanish");
            CFBundleLocalizations.AddString("Portuguese");
        }
        else
            Debug.LogError("Can't add localizations to plist");
                

        plistDocument.WriteToFile(plistPath);
    }
#endif

    private static bool SwitchActiveBuildTarget(BuildTargetGroup targetGroup, BuildTarget target)
    {
#if UNITY_5_6_OR_NEWER
        return EditorUserBuildSettings.SwitchActiveBuildTarget(targetGroup, target);
#else
        return EditorUserBuildSettings.SwitchActiveBuildTarget(target);
#endif
    }

    private static void CopyAndroidFiles (string pathToBuildProject)
    {
        {
            var fileName = "gradle.properties";
            var srcPath = new DirectoryInfo(string.Format("{0}/Plugins/Android/{1}", Application.dataPath, fileName)).FullName;
            var dstPath = new DirectoryInfo(string.Format("{0}/{1}/{2}", pathToBuildProject, Application.productName, fileName)).FullName;

            Debug.LogFormat ("Copying file '{0}' to exported Android project: '{1}'...", srcPath, dstPath);

            try
            {
                File.Copy (srcPath, dstPath, true);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }
    }
}
