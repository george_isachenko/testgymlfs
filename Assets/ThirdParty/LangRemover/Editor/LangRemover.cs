// Check http://forum.unity3d.com/threads/visual-studio-tools-for-unity-2-2.384014/#post-2498322
// and https://gist.github.com/sailro/3548b751faf821dbef7c

#if DISABLED_CODE

using System;
using UnityEditor;
using SyntaxTree.VisualStudio.Unity.Bridge;
using UnityEngine;

[InitializeOnLoad]
public class LangRemover {

	static LangRemover()
	{
		ProjectFilesGenerator.ProjectFileGeneration += (string name, string content) =>
		{
			// This will remove the language restriction to C# 4
			return content.Replace("<LangVersion Condition=\" '$(VisualStudioVersion)' != '10.0' \">4</LangVersion>", string.Empty);
		};
	}
	
}

#endif
