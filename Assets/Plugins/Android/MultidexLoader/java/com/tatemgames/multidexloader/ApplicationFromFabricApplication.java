package com.tatemgames.multidexloader;

import android.content.Context;
import android.support.multidex.MultiDex; 
import io.fabric.unity.android.FabricApplication;

/**
 * Multidex Application that extends io.fabric.unity.android.FabricApplication
 */

public class ApplicationFromFabricApplication extends FabricApplication
{
 	@Override
  	protected void attachBaseContext(Context base)
	{
    	super.attachBaseContext(base);
		MultiDex.install(this);
	}
}
