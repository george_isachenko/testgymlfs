#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("3MDH2sHc0Zm+mLyuq/2sq7ul6djGzIjLx8bMwdzBx8bbiMfOiN3bzYjJxsyIy83a3MHOwcvJ3MHHxojYysTNiNvcycbMydrMiNzN2sXbiMmG6A5f7+XXoPaYt66r/bWLrLCYvifbKchus/OhhzoaUOzgWMiQNr1dm57ymMqZo5ihrqv9rK67qv37mbvMnYu9473xtRs8X140Nmf4Emnw+J4x5IXQH0UkM3Rb3zNa3nrfmOdpty0rLbMxle+fWgEz6CaEfBk4unC+mLyuq/2sq7ul6djYxM2I+sfH3KCDrqmtra+qqb62wNzc2NuSh4ffjEpDeR/Yd6ftSY9iWcXQRU8dv7+lrqGCLuAuX6Wpqa2tqKsqqamo9M8noByIX2MEhIjH2B6XqZgkH+tnhIjLzdrcwc7By8nczYjYx8TBy9HaycvcwcvNiNvcydzNxc3G3NuGmB+zFTvqjLqCb6e1HuU09stg4yi/oPaYKqm5rqv9tYisKqmgmCqprJiuq/21pqy+rLyDeMHvPN6hVlzDJe3Wt+TD+D7pIWzcyqO4K+kvmyIpKqmorqGCLuAuX8vMramYKVqYgq7xD62h1L/o/rm23HsfI4uT7wt9x4eYKWuuoIOuqa2tr6qqmCkesikbGZjwRPKsmiTAGye1ds3bV8/2zRSYKqwTmCqrCwirqqmqqqmqmKWuoWjLm99fkq+E/kNyp4mmchLbsecd2MTNiPrHx9yI6+mYtr+lmJ6YnJpxntdpL/1xDzERmupTcH3ZNtYJ+rc5c7bv+EOtRfbRLIVDngr/5P1E+s3EwcnGy82Ix8aI3MDB24jLzdoDC9k67/v9aQeH6RtQU0vYZU4L5K9E1ZErI/uIe5BsGRcy56LDV4NUAHTWip1ijX1xp37DfAqMi7lfCQTYxM2I683a3MHOwcvJ3MHHxojp3Si8g3jB7zzeoVZcwyWG6A5f7+XXxM2I4cbLhpmOmIyuq/2so7u16diOmIyuq/2so7u16djYxM2I683a3NzBzsHLydzNiMrRiMnG0YjYydrc1+kAMFF5Ys40jMO5eAsTTLOCa7fRiMnb293FzduIycvLzdjcycbLzdKYKqnemKauq/21p6mpV6ysq6qprpinrqv9tbupqVesrZirqalXmLUdkgVcp6aoOqMZib6G3H2UpXPKvuFw3jebvM0J3zxhhaqrqaipCyqp39+GydjYxM2Gy8fFh8nY2MTNy8mCLuAuX6Wpqa2tqJjKmaOYoa6r/T020qQM7yPzfL6fm2Nsp+VmvMF5iMfOiNzAzYjcwM3GiMnY2MTBy8msrruq/fuZu5i5rqv9rKK7ounY2Ji5rqv9rKK7ounY2MTNiOHGy4aZI7EhdlHjxF2vA4qYqkCwllD4oXuVjs+IIpvCX6UqZ3ZDC4dR+8LzzBZc2zNGesynY9HnnHAKllHQV8NgpzWVW4PhgLJgVmYdEaZx9rR+Y5WdmpmcmJue8r+lm52YmpiRmpmcmIjr6ZgqqYqYpa6hgi7gLl+lqampYbHaXfWmfdf3M1qNqxL9J+X1pVmtqKsqqaeomCqpoqoqqamoTDkBocHOwcvJ3MHHxojp3dzAx9rB3NGZ+AIifXJMVHihr58Y3d2J");
        private static int[] order = new int[] { 5,43,45,42,31,55,8,20,48,49,12,35,53,47,21,44,46,59,33,30,25,34,52,37,26,55,57,48,58,44,38,57,51,54,57,51,42,53,39,59,40,58,44,53,48,48,53,54,58,58,58,53,56,58,56,55,56,59,59,59,60 };
        private static int key = 168;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
