#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("OogLKDoHDAMgjEKM/QcLCwsPCgn57XcnJt/BGX52lM+nqqF/hYkyGcOPYLG4hEyyr33p7JBN6OHJo3BGlysr8XTwbyXAgbWZVjQDvn8Mn3nQYrFtBMZt37ArrpFQ1ffsKz7I9wX95lkeJas8jHpssWWcZ5+93Ed30JI6p7zNfzzLEsjbSsHX+55+VsaJLPKLihi010IA3SJpXsrdvzZNaqEHCvuFkc8qnB2D6K27N2iq4PIhiAsFCjqICwAIiAsLCpzNUnK1ej6pRVUmxmOi+V0yeaOO9wnxVjnYB9yStNFGx4v0Egd9KYAVpwLqzWtPDML45e9xOlw6CYHEbYwz0ao/Fj5BSeAhe8hV3ubXE+M6/oHuuBCJZq84rcMWvUEb2wgJCwoL");
        private static int[] order = new int[] { 0,10,12,5,4,7,7,12,9,10,12,12,13,13,14 };
        private static int key = 10;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
