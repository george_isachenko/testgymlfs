﻿Shader "Kingdom/Unlit/Unlit + Stencil"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 6 // Default: Not Equal
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog
			
				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
			
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}
			
				fixed4 frag (v2f i) : SV_Target
				{
					// sample the texture
					fixed4 col = tex2D(_MainTex, i.uv);
					// apply fog
					UNITY_APPLY_FOG(i.fogCoord, col);
					return col;
				}
			ENDCG
		}
	}
}
