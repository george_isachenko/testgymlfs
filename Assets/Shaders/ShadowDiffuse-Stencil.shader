﻿Shader "Kingdom/Shadow Diffise + Stencil"
{
	Properties
	{
														_Color("Main Color", Color) = (1,1,1,1)
														_MainTex("Base (RGB)", 2D) = "white" {}
														_Shadow("Base (RGB)", 2D) = "white" {}

		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 3 // Default: Equal
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}
	
	SubShader
	{
		Tags
		{
			"RenderType"		= "Opaque"
			"Queue"				= "Transparent-1"
			"IgnoreProjector"	= "True"
		}
		LOD 200

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		CGPROGRAM
			#pragma surface surf Lambert

			sampler2D _MainTex;
			sampler2D _Shadow;
			fixed4 _Color;

			struct Input
			{
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				fixed4 c2 = tex2D(_Shadow, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb * c2.rgb;
				o.Alpha = c.a;
			}
		ENDCG
	}

	Fallback "Legacy Shaders/VertexLit"
}
