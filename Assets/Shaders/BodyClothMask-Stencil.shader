﻿Shader "Kingdom/Body Cloth Mask + Stencil"
{
	Properties
	{
														_DiffuseBody("Body texture (RGB) (_DiffuseBody)", 2D) = "white" {}
		[PerRendererData]								_DiffuseCloth("Cloth texture (RGBA) (_DiffuseCloth)", 2D) = "black" {}

		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 8 // Default: Always
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		LOD 200

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		CGPROGRAM
			#pragma surface surf Lambert noforwardadd noshadow 

			sampler2D _DiffuseBody, _DiffuseCloth;

			struct Input
			{
				float2 uv_DiffuseBody;
				float2 uv_DiffuseCloth;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed3 body = tex2D(_DiffuseBody, IN.uv_DiffuseBody);
				fixed4 cloth = tex2D(_DiffuseCloth, IN.uv_DiffuseCloth);
				o.Albedo = lerp(body.rgb, cloth.rgb, cloth.aaa);
				//o.Alpha = 1.0;
			}
		ENDCG
	}
}
