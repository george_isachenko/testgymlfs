﻿Shader "Kingdom/Body Cloth Mask Unlit"
{
	Properties
	{
							_DiffuseBody("Body texture (RGB) (_DiffuseBody)", 2D) = "white" {}
		[PerRendererData]	_DiffuseCloth("Cloth texture (RGBA) (_DiffuseCloth)", 2D) = "black" {}
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		LOD 200

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
					float2 texcoord1 : TEXCOORD1;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					half2 texcoord : TEXCOORD0;
					half2 texcoord1 : TEXCOORD1;
					UNITY_FOG_COORDS(1)
				};

				sampler2D _DiffuseBody, _DiffuseCloth;
				float4 _DiffuseBody_ST;
				float4 _DiffuseCloth_ST;

				v2f vert(appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.texcoord = TRANSFORM_TEX(v.texcoord, _DiffuseBody);
					o.texcoord1 = TRANSFORM_TEX(v.texcoord1, _DiffuseCloth);
					UNITY_TRANSFER_FOG(o, o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed3 body = tex2D(_DiffuseBody, i.texcoord);
					fixed4 cloth = tex2D(_DiffuseCloth, i.texcoord1);
					fixed4 col;
					col.rgb = lerp(body.rgb, cloth.rgb, cloth.aaa);
					UNITY_APPLY_FOG(i.fogCoord, col);
					UNITY_OPAQUE_ALPHA(col.a);
					return col;
				}
			ENDCG
		}
	}
}
