﻿Shader "Kingdom/Simple Diffise + Stencil"
{
	Properties
	{
														_MainTex("Base (RGB)", 2D) = "white" {}
														_Color("Main Color", Color) = (1,1,1,1)

		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 3 // Default: Equal
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}
	
	SubShader
	{
		Tags
		{
			"RenderType"		= "Opaque"
			"Queue"				= "Transparent-1"
			"IgnoreProjector"	= "True"
		}
		LOD 200

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		CGPROGRAM
			#pragma surface surf SimpleLambert

			half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten)
			{
				// for light direction used hardcoded value
				half NdotL = dot (s.Normal, half3(0,0.5,-0.5)); //lightDir);
				half4 c;
				c.rgb = s.Albedo * (NdotL * atten ); // * (NdotL * atten);
				c.a = s.Alpha;
				return c;
			}

			sampler2D _MainTex;
			fixed4 _Color;

			struct Input
			{
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
			}
		ENDCG
	}

	Fallback "Legacy Shaders/VertexLit"
}
