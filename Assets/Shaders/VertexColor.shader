﻿Shader "Kingdom/Vertex Color"
{
	Properties
	{
	}

	SubShader
	{
		Tags
		{
			"RenderType"		= "Opaque"
		}
		LOD 150

		CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			struct Input
			{
				float2 uv_MainTex;
				float3 color : COLOR;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				o.Albedo = IN.color.rgb;
			}
		ENDCG
	}
}
