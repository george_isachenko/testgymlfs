Shader "Kingdom/Transparent/Diffuse + Z-Offset + Stencil"
{
	Properties
	{
		_Color				("Main Color", Color)					= (1,1,1,1)
		_MainTex			("Base (RGB) Trans (A)", 2D)			= "white" {}
		_ZOffsetFactor		("Z Offset Factor", Float)				= -1.0
		_ZOffsetUnits		("Z Offset Units", Float)				= 0
		_StencilRef			("Stencil Ref (bitmask [0-255])", Range(0, 255))	= 1
		_StencilReadMask	("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		_StencilWriteMask	("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}

	SubShader
	{
		Tags
		{
			"Queue"					= "Transparent"
			"IgnoreProjector"		= "True"
			"RenderType"			= "Transparent"
		}
		LOD 200

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp NotEqual
		}

		Offset [_ZOffsetFactor],[_ZOffsetUnits]

		CGPROGRAM
			#pragma surface surf Lambert alpha:fade

			sampler2D _MainTex;
			fixed4 _Color;

			struct Input
			{
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
		ENDCG
	}
}
