﻿Shader "Kingdom/Body Cloth Mask Unlit + Stencil"
{
	Properties
	{
														_DiffuseBody("Body texture (RGB) (_DiffuseBody)", 2D) = "white" {}
		[PerRendererData]								_DiffuseCloth("Cloth texture (RGBA) (_DiffuseCloth)", 2D) = "black" {}

		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 8 // Default: Always
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		LOD 200

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
					float2 texcoord1 : TEXCOORD1;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					half2 texcoord : TEXCOORD0;
					half2 texcoord1 : TEXCOORD1;
					UNITY_FOG_COORDS(1)
				};

				sampler2D _DiffuseBody, _DiffuseCloth;
				float4 _DiffuseBody_ST;
				float4 _DiffuseCloth_ST;

				v2f vert(appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.texcoord = TRANSFORM_TEX(v.texcoord, _DiffuseBody);
					o.texcoord1 = TRANSFORM_TEX(v.texcoord1, _DiffuseCloth);
					UNITY_TRANSFER_FOG(o, o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed3 body = tex2D(_DiffuseBody, i.texcoord);
					fixed4 cloth = tex2D(_DiffuseCloth, i.texcoord1);
					fixed4 col;
					col.rgb = lerp(body.rgb, cloth.rgb, cloth.aaa);
					UNITY_APPLY_FOG(i.fogCoord, col);
					UNITY_OPAQUE_ALPHA(col.a);
					return col;
				}
			ENDCG
		}
	}
}
