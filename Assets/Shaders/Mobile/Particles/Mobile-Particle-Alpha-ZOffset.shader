// Modified built-in "Mobile\Mobile-Particle-Alpha.shader" with Z-Offset public settings.

// Simplified Alpha Blended Particle shader. Differences from regular Alpha Blended Particle one:
// - no Tint color
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "Kingdom/Mobile/Particles/Alpha Blended + Z-Offset"
{
	Properties
	{
						_MainTex ("Particle Texture", 2D)				= "white" {}
						_ZOffsetFactor("Z Offset Factor", Int)			= -1.0
						_ZOffsetUnits("Z Offset Units", Int)			= 0
	}

	Category
	{
		Tags
		{
			"Queue"					= "Transparent"
			"IgnoreProjector"		= "True"
			"RenderType"			= "Transparent"
			"PreviewType"			= "Plane"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Color (0,0,0,0) }
		Offset [_ZOffsetFactor], [_ZOffsetUnits]

		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
	
		SubShader
		{
			Pass
			{
				SetTexture [_MainTex]
				{
					combine texture * primary
				}
			}
		}
	}
}
