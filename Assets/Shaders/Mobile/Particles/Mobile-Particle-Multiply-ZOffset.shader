// Modified built-in "Mobile\Mobile-Particle-Multiply.shader" with Z-Offset public settings.

// Simplified Multiply Particle shader. Differences from regular Multiply Particle one:
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "Kingdom/Mobile/Particles/Multiply + Z-Offset"
{
	Properties
	{
		_MainTex			("Particle Texture", 2D)	= "white" {}
		_ZOffsetFactor		("Z Offset Factor", Int)	= -1.0
		_ZOffsetUnits		("Z Offset Units", Int)		= 0
	}

	Category
	{
		Tags
		{
			"Queue"					= "Transparent"
			"IgnoreProjector"		= "True"
			"RenderType"			= "Transparent"
			"PreviewType"			= "Plane"
		}
		Blend Zero SrcColor
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Color (1,1,1,1) }
		Offset[_ZOffsetFactor],[_ZOffsetUnits]
	
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
	
		SubShader
		{
			Pass
			{
				SetTexture [_MainTex]
				{
					combine texture * primary
				}

				SetTexture [_MainTex]
				{
					constantColor (1,1,1,1)
					combine previous lerp (previous) constant
				}
			}
		}
	}
}
