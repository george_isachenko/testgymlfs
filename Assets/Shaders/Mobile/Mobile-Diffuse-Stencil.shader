﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Kingdom/Mobile/Diffuse + Stencil"
{
	Properties
	{
							_MainTex				("Base (RGB)", 2D)											= "white" {}
							_StencilRef				("Stencil Ref (bitmask [0-255])", Range(0, 255))			= 1
							_StencilReadMask		("Stencil Read Mask (bitmask [0-255])", Range(0, 255))		= 255
							_StencilWriteMask		("Stencil Write Mask (bitmask [0-255])", Range(0, 255))		= 255
	}
	
	SubShader
	{
		Tags
		{
			"RenderType"		= "Opaque"
			"Queue"				= "Geometry-1" // Write to the stencil buffer before drawing any geometry to the screen
		}
		LOD 150

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp NotEqual
			Pass Zero
			Fail Zero
			ZFail Zero
		}

		CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
		ENDCG
	}
}
