Shader "Kingdom/Sprites/Opaque + Stencil"
{
	Properties
	{
		[PerRendererData]	_MainTex				("Sprite Texture", 2D)										= "white" {}
							_Color					("Tint", Color)												= (1,1,1,1)
		[MaterialToggle]	PixelSnap				("Pixel snap", Float)										= 0
							_StencilRef				("Stencil Ref (bitmask [0-255])", Range(0, 255))			= 1
							_StencilReadMask		("Stencil Read Mask (bitmask [0-255])", Range(0, 255))		= 255
							_StencilWriteMask		("Stencil Write Mask (bitmask [0-255])", Range(0, 255))		= 255
	}

	SubShader
	{
		Tags
		{ 
			//"Queue"					= "Geometry-1"  // Write to the stencil buffer before drawing any geometry to the screen
			"RenderType"			= "Opaque" 
			"PreviewType"			= "Plane"
			"CanUseSpriteAtlas"		= "True"
		}

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp NotEqual
			Pass Zero
			Fail Zero
			ZFail Zero
		}

		Cull Off
		Lighting Off

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#include "UnityCG.cginc"
			
				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color    : COLOR;
					float2 texcoord  : TEXCOORD0;
				};
			
				fixed4 _Color;

				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(IN.vertex);
					OUT.texcoord = IN.texcoord;
					OUT.color = IN.color * _Color;
					#ifdef PIXELSNAP_ON
					OUT.vertex = UnityPixelSnap (OUT.vertex);
					#endif

					return OUT;
				}

				sampler2D _MainTex;

				fixed4 SampleSpriteTexture (float2 uv)
				{
					fixed4 color = tex2D (_MainTex, uv);

					return color;
				}

				fixed4 frag(v2f IN) : SV_Target
				{
					fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
					c.rgb *= c.a;
					c.a = 1.0;
					return c;
				}
			ENDCG
		}
	}
}
