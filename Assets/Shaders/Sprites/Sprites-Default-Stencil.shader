Shader "Kingdom/Sprites/Default + Stencil"
{
	Properties
	{
		[PerRendererData]	_MainTex				("Sprite Texture", 2D)										= "white" {}
							_Color					("Tint", Color)												= (1,1,1,1)
		[MaterialToggle]	PixelSnap				("Pixel snap", Float)										= 0
		[HideInInspector]	_RendererColor			("RendererColor", Color)									= (1,1,1,1)
		[HideInInspector]	_Flip					("Flip", Vector)											= (1,1,1,1)
		[PerRendererData]	_AlphaTex				("External Alpha", 2D)										= "white" {}
		[PerRendererData]	_EnableExternalAlpha	("Enable External Alpha", Float)							= 0
							_StencilRef				("Stencil Ref (bitmask [0-255])", Range(0, 255))			= 1
							_StencilReadMask		("Stencil Read Mask (bitmask [0-255])", Range(0, 255))		= 255
							_StencilWriteMask		("Stencil Write Mask (bitmask [0-255])", Range(0, 255))		= 255
	}

	SubShader
	{
		Tags
		{ 
			"Queue"					= "Transparent" 
			"IgnoreProjector"		= "True" 
			"RenderType"			= "Transparent" 
			"PreviewType"			= "Plane"
			"CanUseSpriteAtlas"		= "True"
		}

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp NotEqual
			Pass Zero
			Fail Zero
			ZFail Zero
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
				#pragma vertex SpriteVert
				#pragma fragment SpriteFrag
				#pragma target 2.0
				#pragma multi_compile_instancing
				#pragma multi_compile _ PIXELSNAP_ON
				#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
				#include "UnitySprites.cginc"
			ENDCG
		}
	}
}
