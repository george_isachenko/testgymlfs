﻿Shader "Kingdom/Equipment Shadow"
{
	Properties
	{
		[PerRendererData]						_MainTex			("Main texture (RGBA)", 2D) = "white" {}
												_Color				("Tint", Color)				= (1,1,1,1)
												ColorSelector		("Color Channel Selector (0: Red, 1: Green, 2: Blue)", Int) = 0
		[MaterialToggle]						PixelSnap			("Pixel snap", Float)		= 0

		[Header(Blend parameters)]
		[Enum(UnityEngine.Rendering.BlendMode)]	_SrcBlend			("SrcBlend operation", Int)			= 5 // Default: SrcAlpha
		[Enum(UnityEngine.Rendering.BlendMode)]	_DstBlend			("DstBlend operation", Int)			= 10 // Default: OneMinusSrcAlpha

		[Header(Z Offset parameters)]
												_ZOffsetFactor		("Z Offset Factor", Float)	= -1.0
												_ZOffsetUnits		("Z Offset Units", Float)	= 0
	}

	SubShader
	{
		Tags
		{
			"Queue"					= "Transparent"
			"IgnoreProjector"		= "True"
			"RenderType"			= "Transparent"
			"PreviewType"			= "Plane"
			"CanUseSpriteAtlas"		= "True"
		}

		Blend [_SrcBlend] [_DstBlend]
		Cull Off
		Lighting Off
		ZWrite Off
		Offset [_ZOffsetFactor], [_ZOffsetUnits]

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON
				#include "UnityCG.cginc"

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex   : SV_POSITION;
					fixed4 color : COLOR;
					float2 texcoord  : TEXCOORD0;
				};

				fixed4 _Color;
				int ColorSelector;

				v2f vert(appdata_t IN)
				{
					v2f OUT;
					OUT.vertex = UnityObjectToClipPos(IN.vertex);
					OUT.texcoord = IN.texcoord;
					OUT.color = IN.color * _Color;
					#ifdef PIXELSNAP_ON
					OUT.vertex = UnityPixelSnap(OUT.vertex);
					#endif

					return OUT;
				}

				sampler2D _MainTex;
				sampler2D _AlphaTex;

				fixed4 SampleSpriteTexture(float2 uv)
				{
					fixed4 color = tex2D(_MainTex, uv);

					#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
					if (_AlphaSplitEnabled)
						color.a = tex2D(_AlphaTex, uv).r;
					#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

					return color;
				}

				fixed4 frag(v2f IN) : SV_Target
				{
					fixed4 s = SampleSpriteTexture(IN.texcoord);
					fixed4 o;

					if (ColorSelector <= 0)
					{
						o.rgb = s.rrr * IN.color.rgb;
						o.a = 1 - s.r * IN.color.a;
					}
					else if (ColorSelector >= 2)
					{
						o.rgb = s.bbb * IN.color.rgb;
						o.a = 1 - s.b * IN.color.a;
					}
					else
					{
						o.rgb = s.ggg * IN.color.rgb;
						o.a = 1 - s.g * IN.color.a;
					}

					return o;
				}
			ENDCG
		}
	}
}
