﻿Shader "Kingdom/Body Cloth Mask"
{
	Properties
	{
							_DiffuseBody("Body texture (RGB) (_DiffuseBody)", 2D) = "white" {}
		[PerRendererData]	_DiffuseCloth("Cloth texture (RGBA) (_DiffuseCloth)", 2D) = "black" {}
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		LOD 200

		CGPROGRAM
			#pragma surface surf Lambert noforwardadd noshadow 

			sampler2D _DiffuseBody, _DiffuseCloth;

			struct Input
			{
				float2 uv_DiffuseBody;
				float2 uv_DiffuseCloth;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed3 body = tex2D(_DiffuseBody, IN.uv_DiffuseBody);
				fixed4 cloth = tex2D(_DiffuseCloth, IN.uv_DiffuseCloth);
				o.Albedo = lerp(body.rgb, cloth.rgb, cloth.aaa);
				//o.Alpha = 1.0;
			}
		ENDCG
	}
}
