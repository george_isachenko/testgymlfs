﻿Shader "Kingdom/Stencil Mask + Z-Offset"
{
	Properties
	{
		[Header(Stencil parameters)]
		[IntRange]										_StencilRef("Stencil Ref (bitmask [0-255])", Range(0, 255)) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)]	_StencilComp("Stencil Comparison function", Float) = 8 // Default: Always
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpPass("Stencil Pass operation", Float) = 2 // Default: Replace
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpFail("Stencil Fail operation", Float) = 0 // Default: Keep
		[Enum(UnityEngine.Rendering.StencilOp)]			_StencilOpZFail("Stencil ZFail operation", Float) = 0 // Default: Keep
		[IntRange]										_StencilReadMask("Stencil Read Mask (bitmask [0-255])", Range(0, 255)) = 255
		[IntRange]										_StencilWriteMask("Stencil Write Mask (bitmask [0-255])", Range(0, 255)) = 255

		[Header(Z Offset parameters)]
														_ZOffsetFactor("Z Offset Factor", Float) = -1.0
														_ZOffsetUnits("Z Offset Units", Float) = 0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Geometry-1" // Write to the stencil buffer before drawing any geometry to the screen
		}  
		ColorMask 0 // Don't write to any colour channels
		ZWrite Off // Don't write to the Depth buffer
		Offset[_ZOffsetFactor],[_ZOffsetUnits]

		Stencil
		{
			Ref [_StencilRef]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPass]
			Fail [_StencilOpFail]
			ZFail [_StencilOpZFail]
		}

		Pass
		{
		}
	}
}
