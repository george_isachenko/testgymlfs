﻿Shader "Kingdom/Vertex Color + Diffuse"
{
	Properties
	{
		_Diffuse("Base (RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Tags
		{
			"RenderType"		= "Opaque"
		}
		LOD 150

		CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			sampler2D _Diffuse;

			struct Input
			{
				float2 uv_Diffuse;
				float3 color : COLOR;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed3 c = tex2D(_Diffuse, IN.uv_Diffuse);
				o.Albedo = IN.color.rgb * c;
			}
		ENDCG
	}
}
