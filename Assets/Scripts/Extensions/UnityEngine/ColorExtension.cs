﻿namespace UnityEngine
{
    public static class ColorExtension
    {
        #region Public static API.
        public static string ToRGBHex (this Color c)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
        }
        #endregion

        #region Private methods.
        private static byte ToByte (float f)
        {
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }
        #endregion
    }
}