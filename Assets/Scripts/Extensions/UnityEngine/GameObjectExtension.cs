﻿using UnityEngine.Assertions;

namespace UnityEngine
{
    public static class GameObjectExtension
    {
        #region Public static API.
        public static T FindComponent<T>
            (string objectName) where T : class
        {
            var gameObject = GameObject.Find(objectName);
            if (gameObject != null)
            {
                return gameObject.GetComponent<T>();
            }
            return null;
        }

        public static T GetComponentInChild<T>
            (this GameObject gameObject, string childName) where T : class
        {
            Assert.IsNotNull(gameObject);

            Transform subObjectTransform = gameObject.transform.FindDeepChild(childName);
            if (subObjectTransform != null)
            {
                return subObjectTransform.GetComponent<T>();
            }
            return null;
        }

        public static T GetComponentInSelfOrChild<T>
            (this GameObject gameObject, string childName) where T : class
        {
            Assert.IsNotNull(gameObject);

            T component = gameObject.GetComponent<T>();
            if (component != null)
                return component;

            Transform subObjectTransform = gameObject.transform.FindDeepChild(childName);
            if (subObjectTransform != null)
            {
                return subObjectTransform.GetComponent<T>();
            }
            return null;
        }

        public static T GetComponentInSelfOrParents<T>
            (this GameObject gameObject) where T : class
        {
            Assert.IsNotNull(gameObject);

            T component = gameObject.GetComponent<T>();
            if (component != null)
                return component;

            var parent = gameObject.transform.parent;
            while (parent != null) 
            {
                T parentComponent = parent.gameObject.GetComponent<T>();
                if (parentComponent != null)
                    return parentComponent;
                parent = parent.transform.parent;
            }
            return null;
        }

        public static void SetSpriteRenderersSortingLayer (this GameObject gameObject, int sortingLayer)
        {
            if (gameObject != null)
            {
                var spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
                if (spriteRenderers != null)
                {
                    foreach (var renderer in spriteRenderers)
                    {
                        renderer.sortingLayerID = sortingLayer;
                    }
                }
            }
        }

        public static string GetScenePath(this GameObject gameObject)
        {
            string path = "/" + gameObject.name;
            while (gameObject.transform.parent != null)
            {
                gameObject = gameObject.transform.parent.gameObject;
                path = "/" + gameObject.name + path;
            }
            return path;
        }
        #endregion
    }
}