﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace UnityEngine
{
    public static class TransformExtension
    {
        #region Public static API.
        public static void SetLayerRecursive (this Transform transform, int layer)
        {
            Assert.IsNotNull(transform);

            if (transform != null)
            {
                transform.gameObject.layer = layer;
                foreach (Transform child in transform)
                    child.SetLayerRecursive(layer);
            }
        }

        public static T GetComponentInChild<T>
            (this Transform transform, string childName) where T : class
        {
            Assert.IsNotNull(transform);
            Assert.IsNotNull(childName);

            if (transform != null && childName != null)
            {
                Transform subObjectTransform = transform.FindDeepChild(childName);
                if (subObjectTransform != null)
                {
                    return subObjectTransform.GetComponent<T>();
                }
            }
            return null;
        }

        public static Transform FindDeepChild (this Transform transform, string childName)
        {
            Assert.IsNotNull(transform);
            Assert.IsNotNull(childName);

            if (transform != null && childName != null)
            {
                var result = transform.Find(childName);
                if (result != null)
                    return result;

                foreach(Transform child in transform)
                {
                    result = FindDeepChild(child, childName);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

        public static IEnumerable<GameObject> FindDeepChildsWithTag (this Transform transform, string tag)
        {
            Assert.IsNotNull(transform);
            Assert.IsNotNull(tag);

            if (transform != null && tag != null)
            {
                foreach(Transform child in transform)
                {
                    if (child != null && child.gameObject != null)
                    {
                        if (child.gameObject.CompareTag(tag))
                        {
                            yield return child.gameObject;
                        }

                        foreach (var child2 in child.FindDeepChildsWithTag(tag))
                        {
                            yield return child2;
                        }
                    }
                }
            }
        }

        public static IEnumerable<GameObject> EnumerateDeepChildsWithTag (this Transform transform, string tag)
        {
            Assert.IsNotNull(transform);
            Assert.IsNotNull(tag);

            if (transform != null && tag != null)
            {
                foreach (Transform child in transform)
                {
                    if (child != null && child.gameObject != null)
                    {
                        if (child.gameObject.CompareTag(tag))
                        {
                            yield return child.gameObject;
                        }

                        foreach (var subChild in child.EnumerateDeepChildsWithTag(tag))
                        {
                            yield return subChild;
                        }
                    }
                }
            }

            yield break;
        }

        public static List<T> GetComponentsInChildsForObjectsWithTag<T> (this Transform transform, string tag, int reserve = 16)
        {
            List<T> result = new List<T>(reserve);
            foreach (var wallObject in transform.EnumerateDeepChildsWithTag(tag))
            {
                var tsr = wallObject.GetComponentsInChildren<T>();
                if (tsr != null)
                {
                    result.AddRange(tsr);
                }
            }

            return result;
        }

        public static IEnumerable<Transform> GetChildsWithNamePrefix (this Transform transform, string prefix)
        {
            Assert.IsNotNull(transform);
            Assert.IsNotNull(prefix);

            if (transform == null || prefix == null || prefix == string.Empty)
            {
                yield break;
            }

            foreach (Transform child in transform)
            {
                if (child.name.StartsWith(prefix))
                {
                    yield return child;
                }
            }
        }

        public static string GetScenePath(this Transform transform)
        {
            string path = "/" + transform.name;
            while (transform.parent != null)
            {
                transform = transform.parent;
                path = "/" + transform.name + path;
            }
            return path;
        }
        #endregion
   }
}