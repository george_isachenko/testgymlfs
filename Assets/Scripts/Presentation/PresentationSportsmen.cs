﻿using BridgeBinder;
using Data;
using Logic.Facades;
using Presentation.Base;
using Presentation.Helpers;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI;
using Data.Person;
using Logic.Persons;
using View.CameraHelpers;
using UnityEngine.UI;
using Logic.Persons.Events;
using System;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sportsmen")]
    [BindBridgeInterface(typeof(LogicSportsmen))]
    public class PresentationSportsmen : BasePresentationFacade
    {
        LogicSportsmen logic;

        SportsmanType lastSelectedType = Data.Person.SportsmanType.Sportsman_0;
        int selectionCounter = 0;
        CameraController camControl;

        [BindBridgeSubscribe]
        private void Subscribe(LogicSportsmen logic)
        {
            this.logic = logic;

            gui.hud.enablerSportclub.CanVisible_Callback = IsSportClubButtonVisible;
            gui.hud.btnSportClub.onClick.AddListener(OnButtonSportClubClick);
            gui.hud.btnSportClubTimer.textGetter = GetUnlockTimerString;
        }

        void Awake()
        {
            camControl = Camera.main.GetComponent<CameraController>();
            Assert.IsNotNull(camControl);
        }

        void Start()
        {
            gui.hud.btnSportsmen.onClick.AddListener(gui.sportsmen.Show);

            var genericView = gui.sportsmen.genericView;
            genericView.SetButtonCallback("btnIncCapacity", ExpandMembersLimit);
            genericView.SetButtonCallback("btnSell", gui.sportsmenAgency.Show);

            gui.sportsmen.OnUpdateContentOnShow_Callback = UpdateView;

            gui.sportsmen.OnHide_Callback = OnHide;
            gui.sportsmen.OnItemSelected_Callback = OnItemSelected;

            clubInteractionProvider.onRoomDeactivatedEvent += OnRoomDeactivated;
            clubInteractionProvider.onRoomActivatedEvent += OnRoomActivated;

            CharacterDeathEvent.Event += OnCharacterDeathEvent;

            CharacterLeavingRoomEvent.Event += OnCharacterLeavingRoomEvent;
        }

        void OnDestroy()
        {
            clubInteractionProvider.onRoomDeactivatedEvent -= OnRoomDeactivated;
            clubInteractionProvider.onRoomActivatedEvent -= OnRoomActivated;

            CharacterDeathEvent.Event -= OnCharacterDeathEvent;

            CharacterLeavingRoomEvent.Event -= OnCharacterLeavingRoomEvent;
        }

        void OnCharacterDeathEvent(int id)
        {
            if (logic.IsSportsmensId(id))
            {
                if (gui.sportsmen.isVisible)
                {
                    UpdateView();
                }
                UpdateHUD();
            }
        }

        void OnCharacterLeavingRoomEvent(Person person, bool kicked)
        {
            if (logic.IsSportsmensId(person.id))
            {
                if (gui.sportsmen.isVisible)
                {
                    UpdateView();
                }
                UpdateHUD();
            }
        }

        public void UpdateView()
        {
            var sportList = logic.GetSportsmenTypes();

            gui.sportsmen.UpdateItemsCount(sportList.Count);
            var viewItems = gui.sportsmen.items;

            var counter = 0;
            foreach (var item in sportList)
            {
                var viewItem = viewItems[counter];

                viewItem.idx = counter;
                var textFormat = item.isTraining ? "<color=#333333FF>x{0}</color>" : "x{0}";
                viewItem.text = string.Format(textFormat, item.count);
                viewItem.sportType = item.sportsmanType;
                viewItem.showTimeIcon = item.isTraining;
                ++counter;
            }

            gui.sportsmen.sportsmenCount = logic.countRatio;

            var genericView = gui.sportsmen.genericView;
            genericView.GetComponent<Button>("btnSell").interactable = logic.IsSportAgencyEnabled;
            //genericView.GetComponent<Image>("btnSell").color = logic.IsSportAgencyEnabled ? Color.white : Color.gray;
        }

        void OnHide()
        {
        }


        void OnItemSelected(SportsmenViewItem item)
        {
            if (item != null)
            {
                //Debug.LogError("Select visitor " + item.idx.ToString());

                var sportList = logic.GetSportsmenTypes();

                if (item.idx >= 0 && item.idx < sportList.Count)
                {
                    //Debug.Log(logic.members[item.idx].sportsmanData.name);

                    var ttype = sportList[item.idx].sportsmanType;

                    if (lastSelectedType != ttype)
                        selectionCounter = 0;

                    selectionCounter = logic.SelectNextSportsman(ttype, selectionCounter, item.isTraining);
                    if (selectionCounter >= sportList[item.idx].count)
                        selectionCounter = 0;

                    var actor = characterInteractionProvider.GetCharacterActor(logic.SelectedCharacter);
                    if (actor != null)
                    {
                        Vector3 offset = camControl.GetWorldOffsetForNormalizedScreenPoint(gui.hud.focusPointRight025.anchorMin);
                        camControl.AutoMoveTo(actor.position.x + offset.x, actor.position.z + offset.z);
                    }

                    lastSelectedType = ttype;
                }
            }

        }

        public void UpdateHUD()
        {
            gui.hud.sportsmenCount = logic.countRatio;
        }

        public void OnRoomDeactivated(string roomName)
        {
            UpdateHUD();
        }

        public void OnRoomActivated(string roomName)
        {
            UpdateHUD();
        }

        public void ExpandMembersLimit()
        {

            if (!logic.CanExpand())
            {
                Debug.LogError("PresentationSportsmen : upgrade not available");
                return;
            }

            var view = PresentationBuyHelper.BuyWithResources(gui, playerProfileInteractionProvider, logic.expandCost, logic.BuyNextExpand);
            view.header = Loc.Get("benchExpandHeader");

            var desciption = view.IsolateHeader("expandSportsmenLimit");

            desciption.SetText("newSize", logic.nextCapacity.ToString());

            camControl.AutoMoveTo(4.6f, -3.7f);

            desciption.SetText("sportsmansDescription", Loc.Get("benchExpandIncreaseCap"));
            view.actionButtonPrefixText = Loc.Get("idUpgrade");
        }

        public void HideExpandWindow()
        {
            gui.buyWithResources.Hide();
        }

        public void WarnSportRoomFull()
        {
            gui.confirmWnd.OnCantAddSportsmen();
            gui.confirmWnd.OnConfirm = logic.sportsmen.ShowExpandMemebersLimit;
        }

        private bool IsSportClubButtonVisible()
        {
            return logic.IsSportClubUnlockAvailable() && 
                characterInteractionProvider.selectedCharacterId == PersonData.InvalidId;
        }

        void OnButtonSportClubClick()
        {
            if (CameraController.instance.isAutoMove)
                return;

            var tRoom = clubInteractionProvider.GetRoomOfType(Data.Room.RoomType.SportClub);
            var tPos = tRoom.homeFocusPosition;
            CameraController.instance.AutoMoveTo(tPos.x, tPos.z, (bool v) => { if (v) tRoom.StartTransitionToRoom(); });

        }

        string GetUnlockTimerString()
        {
            var timeLeft = logic.GetUnlockTime();

            if (timeLeft == null)
            {
                gui.hud.btnSportClubBadge = true;
                return Loc.Get("idNew");
            }
            else
            { 
                if (timeLeft.secondsToFinish < 1)
                {
                    gui.hud.btnSportClubBadge = true;
                    return Loc.Get("idReady");
                }
                else
                {
                    gui.hud.btnSportClubBadge = false;
                    return View.PrintHelper.GetTimeString((int)timeLeft.secondsToFinish);
                }
            }
        }

    }
}

