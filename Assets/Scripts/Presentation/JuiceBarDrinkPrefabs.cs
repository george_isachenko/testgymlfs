﻿using System;
using Data.PlayerProfile;
using InspectorHelpers;
using UnityEngine;

namespace Presentation
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: JuiceDrinkPrefab")]
    public class JuiceBarDrinkPrefabs : ScriptableObject
    {
        [Serializable]
        public struct JuiceDrinkPrefab
        {
            public ResourceType juice;
            public GameObject prefab;
        }

        [ReorderableList]
        public JuiceDrinkPrefab[] array;
    }
}
