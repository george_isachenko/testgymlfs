﻿using Data;
using UnityEngine;
using View.UI;
using UnityEngine.Assertions;

namespace Presentation.Facades
{
    public class PresentationResolve : MonoBehaviour 
    {
        protected GUIRoot _gui;
        protected PresentationPlayerProfileBase playerProfile;

        // PUBLIC 
        public GUIRoot gui { get { return GetGUI();} }

        void Awake()
        {
            playerProfile = GetComponent<PresentationPlayerProfileBase>();
            Logic.ResolveEvents.StorageIsFull.Event += StorageIsFull;
        }

        void OnDestroy()
        {
            Logic.ResolveEvents.StorageIsFull.Event -= StorageIsFull;
        }

        private void StorageIsFull(bool showWarningFirst)
        {
            playerProfile.WarnStorageIsFull(showWarningFirst);
        }
            
        GUIRoot GetGUI()
        {
            if (_gui != null)
                return _gui;

            _gui = GameObject.Find("_GUIRoot").GetComponent<GUIRoot>();
            Assert.IsNotNull(_gui);

            return _gui;
        }
    }
}
