using BridgeBinder;
using Data.Person;
using Data.Room;
using Logic.Facades;
using Presentation.Base;
using Presentation.Providers;
using UnityEngine;
using View.UI;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Style Window")]
    [BindBridgeInterface(typeof (StyleWindowLogic))]
    public class StyleWindowPresenter : BasePresentationFacade<StyleWindowLogic>
    {
        private StyleWindowView view { get { return gui.styleWindow; } }
        
        [BindBridgeSubscribe]
        private void Subscribe(StyleWindowLogic questsLogic)
        {
            logic = questsLogic;
        }

        void Awake()
        {
            view.OnShowHandler.AddListener(UpdateView);
            view.OnShowHandler.AddListener(() =>
            {
                var manager = GetComponent<ICharacterInteractionProvider>();
                if (manager.selectedCharacterId != PersonData.InvalidId)
                    manager.DeselectCharacter(manager.selectedCharacterId);
            });
            view.onBuyDecorPress += ShowDecor;
            view.onGymExpandPress += ShowExpand;
            gui.hud.btnStyle.onClick.AddListener(delegate { view.Show(); });
        }

        private void UpdateView()
        {
//            var roomName = logic.roomManager.GetRoomNameOfType(RoomType.Club);
            // int stylePoints = logic.roomManager.roomCapacity.GetTotalStylePoints(roomName);
            // next = logic.roomManager.roomCapacity.GetStyleLevelForNextVisitor (stylePoints, roomName)

            var roomName = logic.GetClubRoomName();
            var capacity = logic.GetRoomCapacity();
            
            var totalStylePoints = capacity.GetTotalStylePoints(roomName);
            var stylePoints = capacity.GetNormalStylePoints(roomName);
            var stylePointsMax = capacity.GetNormalStylePointsMax(roomName);
            var level = capacity.GetStyleLevel(totalStylePoints);
            view.SetStyleLevelData(level, stylePoints, stylePointsMax);

            var currentGymSize = logic.GetClubSize();
            var gymSizeStylePoints = logic.GetGymSizeStylePoints();
            view.SetStyleExpandData(currentGymSize, gymSizeStylePoints);

            var currentDecorsCount = logic.GetDecorsCount();
            var decorStylePoints = logic.GetDecorsStyle();
            view.SetDecorStylePoints(currentDecorsCount, decorStylePoints);
        }

        private void ShowDecor()
        {
            view.Hide();
            gui.shopUltimate.Show();
            var shopPresenter = GetComponent<PresentationUltimateShop>();
            shopPresenter.ShowDecor();
        }
        private void ShowExpand()
        {
            view.Hide();
            var club = GetComponent<IClubInteractionProvider>();
            club.GetRoomOfType(RoomType.Club).StartExpand();
        }
    }
}