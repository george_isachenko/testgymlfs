﻿using System.Collections;
using System.Linq;
using BridgeBinder;
using Data.RankAchievement;
using Logic.Achievements.Facade;
using Presentation.Base;
using UnityEngine;
using View.Rooms.LinearExpandRoom;
using View.UI;
using Data.PlayerProfile;
using Logic.Quests.Tutorial;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(LogicRankAchievements))]
    public class PresentatoinRankAchievements : BasePresentationFacade<LogicRankAchievements> {

        RankAchievmentsView view;
        LinearExpandRoomView sportRoom;

        [BindBridgeSubscribe]
        private void Subscribe(LogicRankAchievements logic)
        {
            this.logic = logic;   
            gui.rankLvlUp.OnClaim = logic.OnCliamLevelUp;

            gui.hud.btnAchievementsEnabler.CanVisible_Callback = isVisibleBtnHUD;
            #if DEBUG
            gui.dbgView.OnDropAchievementsCallback = logic.DropAllAchievements;
            gui.dbgView.OnSyncAchievementsCallback = logic.SyncAllAchievements;
            #endif
        }

        public bool isVisibleBtnHUD()
        {
            if (gui.windows.states.inEditMode)
                return false;

            UpdateBtnCounterOnly();

            if (logic.currentRank > 1)
                return true;
            

            var completed = logic.tasks.Count(t => t.completed == true);

            return completed > 0;
        }

        void Awake()
        {
            gui.hud.OnShowAchievements = MoveToHQandShowView;
            view = gui.rankAchievements;

            view.OnUpdateContentOnShow_Callback = ShowUpdateView;
        }

        void Start()
        {
            UpdateBtn();
            gui.hq.onShowByCityClick += OnShowByCityClickHQ;
            gui.hq.onAchievementsClick = view.Show;

            var club = GetComponent<PresentationClubBase>();
            sportRoom = club.GetRoom("SportClub").roomConductor.gameObject.GetComponent<LinearExpandRoomView>();
        }

        public void Destroy()
        {
            gui.hq.onShowByCityClick += OnShowByCityClickHQ;
        }

        void OnShowByCityClickHQ()
        {
            if (!logic.isAnyTutorialRunning || logic.IsTutorialRunning<TutorSequenceTrainersUnlock>())
                gui.hq.Show();
        }

        public void UpdateIfVisible()
        {
            if (view.isVisible)
                UpdateView();
        }

        void MoveToHQandShowView()
        {
            if (view.hq != null)
            {
                view.Show();

                //gui.windows.freeze = true;
                //TransitionCameraTo(view.hq.transform.position, OnCameraStopMovement);
            }
            else
            {
                Debug.LogError("PresentationRankAchievements view.hq = null");
                view.Show();
            }
        }

        void OnCameraStopMovement(bool success)
        {
            if (success)
            {
                //view.Show();
            }

            //gui.windows.freeze = false;
        }

        void ShowUpdateView()
        {
            UpdateView();

            StartCoroutine(CenterOnUnclaimed());
        }

        IEnumerator CenterOnUnclaimed()
        {
            yield return new WaitForEndOfFrame();

            if (firstUnclaimedItem != null)
                view.CenterScrollToItem(firstUnclaimedItem);
        }

        bool firstLaunch = true;
        RectTransform firstUnclaimedItem = null;
        void UpdateView()
        {
            if (firstLaunch)
            {
                firstLaunch = false;
                logic.GoNextRunkIfNecessary();
                StartCoroutine(UglyHackFixLayout());
                logic.SyncAllAchievements();
            }



            var items = logic.tasks;
            view.list.UpdateItemsCount(items.Count);
            var viewItems = view.list.items;;

            view.completedRatio = logic.completedRatio;
            view.rankName = logic.rankName;

            var sprites = GUICollections.instance.rankAchievementIconList.items;
            var spritesRooms = sportRoom.expandAvailableIcons;

            firstUnclaimedItem = null;

            for(int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var viewItem = viewItems[i];

                viewItem.idx = i;
                viewItem.title = item.title;
                viewItem.subTitle = item.subTitle;

                viewItem.hint.title = item.hintTitle;
                viewItem.hint.desc = item.hintDesc;

                #if DEBUG

                viewItem.dbgAction = logic.OnDebugItemClick;

                if (item.data.type == Data.RankAchievementsTaskData.TaskType.Level)
                    viewItem.dbgText = "skip";
                else
                    viewItem.dbgText = "+1";
                #endif

                if (item.type == RankAchievementType.OpenSportRoom)
                {
                    var roomId = item.data.task.goal - 1;

                    if (roomId < spritesRooms.Length)
                        viewItem.icon = spritesRooms[roomId];
                    else
                        viewItem.icon = null;
                }
                else if (item.type == RankAchievementType.MakeJuice)
                {
                    viewItem.icon= GUICollections.instance.storageIcons.items[(int)ResourceType.Juice_01 + item.id].sprite;
                }
                else if (item.type == RankAchievementType.HarvestFruits || item.type == RankAchievementType.BuyTree)
                {
                    viewItem.icon= GUICollections.instance.storageIcons.items[(int)ResourceType.Fruit_01 + item.id].sprite;
                }
                else if (item.type == RankAchievementType.CompleteJuiceQuest)
                {
                    viewItem.icon = GUICollections.instance.storageIcons.items[(int)ResourceType.Juice_01].sprite;
                }
                else if ((int)item.type < sprites.Length)
                    viewItem.icon = sprites[(int)item.type].sprite;
                else
                    viewItem.icon = null;
                
                viewItem.reward = item.reward;

                if (!item.claimed)
                {
                    viewItem.claimOn = false;
                    viewItem.progressOn = true;
                    viewItem.completedOn = false;

                    viewItem.steps = item.steps;
                    viewItem.progress = item.progress;

                    viewItem.claimOn = item.completed;
                    viewItem.OnClaim_Callback = logic.OnCliam;

                    if (firstUnclaimedItem == null && item.completed)
                    {
                        firstUnclaimedItem = viewItem.transform as RectTransform;
                    }
                }
                else if (item.completedAndClaimed)
                {
                    viewItem.claimOn = false;
                    viewItem.progressOn = false;
                    viewItem.completedOn = true;
                }
                else
                {
                    Debug.LogError("IMPOSIBRU: Achievement item claimed but not completed");
                    viewItem.claimOn = false;
                    viewItem.progressOn = false;
                    viewItem.completedOn = true;
                    #if DEBUG
                    logic.FakeCompleteTask(i);
                    #endif
                }
            }

            UpdateBtn();
        }

        IEnumerator UglyHackFixLayout()
        {
            yield return new WaitForEndOfFrame();
            view.UglyHackFixLayout();
        }

        public void UpdateBtn()
        {
            var unclaimed = logic.tasks.Count(t => t.completed == true && t.claimed == false);
            gui.hud.btnAchievCounter = unclaimed;
            gui.hud.btnAchievementsEnabler.turnOn = true;

            UpdateBtnCounterOnly();
        }

        public void UpdateBtnCounterOnly()
        {
            var unclaimed = logic.tasks.Count(t => t.completed == true && t.claimed == false);
            gui.hud.btnAchievCounter = unclaimed;
            gui.hud.btnAchievCounterOn = unclaimed > 0;
        }

        public void RankLevelUp(int reward, int rank)
        {
            gui.rankLvlUp.Show();
            gui.rankLvlUp.reward = reward;
            gui.rankLvlUp.rank = rank;
        }


    }
}