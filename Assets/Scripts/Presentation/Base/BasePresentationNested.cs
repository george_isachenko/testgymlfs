﻿using Logic.FacadesBase;
using View.UI;

namespace Presentation.Base
{
    public class BasePresentationNested<TParentPresentationFacade> where TParentPresentationFacade : BasePresentationFacade
    {
        #pragma warning disable 649
        private BasePresentationCommon basePresentationCommon;
        #pragma warning restore 649

        public GUIRoot                      gui { get { return basePresentationCommon.gui; } }
        public TParentPresentationFacade    parent { get; protected set; }
    }

    public class BasePresentationNested<TLogicNested, TParentPresentationFacade> : BasePresentationNested<TParentPresentationFacade>
        where TLogicNested : BaseLogicNested
        where TParentPresentationFacade : BasePresentationFacade
    {
        protected TLogicNested                    logic;

        public BasePresentationNested(TLogicNested logic, TParentPresentationFacade parentFacade)
        {
            this.logic = logic;
            parent = parentFacade;
        }
    }
}
