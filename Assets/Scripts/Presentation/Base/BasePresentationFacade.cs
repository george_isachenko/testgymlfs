﻿using System;
using Core;
using Data;
using Data.Person;
using Logic.Facades;
using Logic.FacadesBase;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI;
using View.UI.Base;
using Core.Analytics;
using Data.Sport;
using System.Collections;

namespace Presentation.Base
{
    public class BasePresentationFacade : MonoBehaviour
    {
        #pragma warning disable 649
        private BasePresentationCommon              basePresentationCommon;
        #pragma warning restore 649

        private IClubInteractionProvider            _clubInteractionProvider;
        private ICharacterInteractionProvider       _characterInteractionProvider;
        private IEquipmentInteractionProvider       _equipmentInteractionProvider;
        private IQuestsInteractionProvider          _questsInteractionProvider;
        private IShopInteractionProvider            _shopInteractionProvider;
        private IPlayerProfileInteractionProvider   _playerProfileInteractionProvider;

        protected GUIRoot                           gui                                 { get { return basePresentationCommon.gui; } }

        public IClubInteractionProvider             clubInteractionProvider             { get { return GetClubInteractionProvider(); } }
        public ICharacterInteractionProvider        characterInteractionProvider        { get { return GetCharacterInteractionProvider(); } }
        public IEquipmentInteractionProvider        equipmentInteractionProvider        { get { return GetEquipmentInteractionProvider(); } }
        public IQuestsInteractionProvider           questsInteractionProvider           { get { return GetQuestsInteractionProvider(); } }
        public IShopInteractionProvider             shopInteractionProvider             { get { return GetShopInteractionProvider(); } }
        public IPlayerProfileInteractionProvider    playerProfileInteractionProvider    { get { return GetPlayerProfileInteractionProvider(); } }

        public void TransitionCameraTo(Vector3 pos, Action<bool> onComplete = null)
        {
            basePresentationCommon.TransitionCameraTo(pos, onComplete);
        }

        private ICharacterInteractionProvider GetCharacterInteractionProvider()
        {
            if (_characterInteractionProvider != null)
                return _characterInteractionProvider;

            _characterInteractionProvider = gameObject.GetComponent<ICharacterInteractionProvider>();
            Assert.IsNotNull(_characterInteractionProvider);

            return _characterInteractionProvider;
        }

        private IEquipmentInteractionProvider GetEquipmentInteractionProvider()
        {
            if (_equipmentInteractionProvider != null)
                return _equipmentInteractionProvider;

            _equipmentInteractionProvider = gameObject.GetComponent<IEquipmentInteractionProvider>();
            Assert.IsNotNull(_equipmentInteractionProvider);

            return _equipmentInteractionProvider;
        }

        private IClubInteractionProvider GetClubInteractionProvider()
        {
            if (_clubInteractionProvider != null)
                return _clubInteractionProvider;

            _clubInteractionProvider = gameObject.GetComponent<IClubInteractionProvider>();
            Assert.IsNotNull(_clubInteractionProvider);

            return _clubInteractionProvider;
        }

        private IQuestsInteractionProvider GetQuestsInteractionProvider()
        {
            if (_questsInteractionProvider != null)
                return _questsInteractionProvider;

            _questsInteractionProvider = gameObject.GetComponent<IQuestsInteractionProvider>();
            Assert.IsNotNull(_questsInteractionProvider);

            return _questsInteractionProvider;
        }

        private IShopInteractionProvider GetShopInteractionProvider()
        {
            if (_shopInteractionProvider != null)
                return _shopInteractionProvider;

            _shopInteractionProvider = gameObject.GetComponent<IShopInteractionProvider>();
            Assert.IsNotNull(_shopInteractionProvider);

            return _shopInteractionProvider;
        }

        public IPlayerProfileInteractionProvider GetPlayerProfileInteractionProvider()
        {
            if (_playerProfileInteractionProvider != null)
                return _playerProfileInteractionProvider;

            _playerProfileInteractionProvider = gameObject.GetComponent<IPlayerProfileInteractionProvider>();
            Assert.IsNotNull(_playerProfileInteractionProvider);

            return _playerProfileInteractionProvider;
        }
    }

    public class BasePresentationFacade<TLogic> : BasePresentationFacade
        where TLogic : class
    {
        protected TLogic logic;

        public void EvaluateSportsmenPurchase(SportsmanType type,  Action<Cost> onSuccess, Action<Cost> onFail, Action onCancel)
        {
            var list = new SportsmanCostUnit[1];

            list[0].sportsmanType = type;
            list[0].count = 1;

            EvaluateSportsmenPurchase(list, onSuccess, onFail, onCancel);
        }

        public void EvaluateSportsmenPurchase(SportsmanCostUnit[] list,  Action<Cost> onSuccess, Action<Cost> onFail, Action onCancel)
        {
            var view = gui.confirmWithBucks;

            var logicFacade = logic as BaseLogicFacade;

            if (logicFacade == null)
            {
                Debug.LogError("IMPOSSIBRU logicFacade == null");
                return;
            }
            else
            {
                var confirmView = gui.confirmWithBucks;

                var cost = LogicSportsmen.GetSportsmanCost(list);

                confirmView.OnNeedSportsmen(list, cost.value);

                confirmView.OnOpenShop = () =>
                {
                    var locker = logicFacade.sportsmen.agencyAvailable;
                    if (locker.canUse)
                    {
                        gui.sportsmenAgency.ShowBuyTab();
                        gui.sportsmenAgency.OnHideHandler.AddListener(GoBackToMyView);
                    }
                    else
                    {
                        gui.confirmWnd.OnCustom(Loc.Get("sportAgencyUnlockHeader"), Loc.Get("sportAgencyUnlockLVL", locker.level));
                    }

                    onCancel?.Invoke();
                };

                confirmView.OnConfirm = () => 
                {
                    if (logicFacade.playerProfile.CanSpend(cost))
                    {
                        onSuccess?.Invoke(cost);
                    }
                    else
                    {
                        onFail?.Invoke(cost);
                    }
                };
            }
        }

        public void BuyCoinsForBucks(Cost cost, Analytics.MoneyInfo info, Action onSuccess, Action onCancel = null)
        {
            var view = gui.confirmWithBucks;

            var logicFacade = logic as BaseLogicFacade;

            if (logicFacade == null)
            {
                Debug.LogError("IMPOSSIBRU logicFacade == null");
                return;
            }
            else
            {
                var bucksPrice = new Cost();
                bucksPrice.type = Cost.CostType.BucksOnly;
                bucksPrice.value = Cost.CoinsToFitBucks(cost.value);

                view.OnNeedResources2(cost, bucksPrice.value, false, true);

                view.OnOpenShop = () =>
                {
                    gui.shopUltimate.Show();
                    gui.shopUltimate.ShowBankCoins();
                    PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PopUp");

                    onCancel?.Invoke();

                    gui.shopUltimate.OnHideHandler.AddListener(GoBackToMyView);
                };
                    
                view.OnConfirm = () =>
                {
                    if (logicFacade.playerProfile.SpendMoney(bucksPrice, info))
                    {
                        logicFacade.playerProfile.AddCoins(cost.value, new Analytics.MoneyInfo(info.itemType, "BuyWithBucks"));

                        onSuccess();
                        //GoBackToMyView();
                    }
                    else
                    {
                        onCancel?.Invoke();
                    }
                };

                view.OnCancel = onCancel;
            }
        }

        protected virtual void GoBackToMyView()
        {
            var logicFacade = logic as BaseLogicFacade;

            if (logicFacade == null)
            {
                Debug.LogError("IMPOSSIBRU logicFacade == null");
                return;
            }
            else
            {
                var myView = GetView();

                if (myView != null)
                {
                    myView.OnHideHandler.RemoveListener(GoBackToMyView);

                    logicFacade.DelayedInvoke(() =>
                    {
                        if (gui.windows.GetCurrentType() != WindowTypes.None)
                            return;
                            myView.Show();
                    });
                }
                else
                {
                    Debug.LogError("Can't execute GoBackToMyView(), View() == null, this = " + this.GetType().ToString());
                }
            }
        }

        protected virtual UIBaseView GetView()
        {
            return null;
        }

        public void DelayedInvoke(Action action)
        {
            StartCoroutine(DelayedInvokeCoroutine(action));
        }

        private IEnumerator DelayedInvokeCoroutine(Action action)
        {
            yield return null;

            action?.Invoke();
        }
    }
}