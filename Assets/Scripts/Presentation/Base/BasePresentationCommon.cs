﻿using UnityEngine;
using View.UI;
using View.CameraHelpers;
using System;

namespace Presentation.Base
{
    public struct BasePresentationCommon
    {
        private GUIRoot                             _gui;

        public  GUIRoot                             gui { get { return GetGUI(); } }

        private GUIRoot GetGUI()
        {
            if (_gui != null)
                return _gui;

            _gui = GameObjectExtension.FindComponent<GUIRoot>("_GUIRoot");

            return _gui;
        }

        public void TransitionCameraTo (Vector3 position, Action<bool> onComplete)
        {
            var mainCamera = Camera.main;
            if (mainCamera != null)
            {
                var cameraController = mainCamera.GetComponent<CameraController>();

                if (cameraController != null)
                {
                    cameraController.AutoMoveTo ( position.x, position.z, onComplete);
                }
            }
        }
    }
}