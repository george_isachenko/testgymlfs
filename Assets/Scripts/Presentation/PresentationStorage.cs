﻿using System.Collections.Generic;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Persons;
using Logic.Persons.Events;
using Logic.PlayerProfile;
using Presentation.Base;
using Presentation.Facades;
using Presentation.Helpers;
using UnityEngine;
using UnityEngine.Assertions;
using View;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;
using View.UI.OverheadUI.Data;
using Core.Analytics;

namespace Presentation
{
    public class PresentationStorage : BasePresentationNested<LogicStorage, PresentationPlayerProfileBase>
    {
        public static readonly string storageRoomTag = "StorageRoom";
        private static readonly string iconAttachmentObjectName = "icon_attachment";

        SingleActionIcon.Holder overheadIcon;

        StorageRoomView storageRoomView;
        bool            deleteMode = false;

        List<StorageItemInfo>   tmpItems;
        StorageView             view;
        int wasCont = -1;

        public PresentationStorage
            ( LogicStorage logic
            , PresentationPlayerProfileBase parentFacade
            , ref SingleActionIcon.Holder overheadIcon)
        : base (logic, parentFacade)
        {
            view = gui.storage;

            //gui.hud.btnStorage.onClick.AddListener(view.Show);
            //gui.hud.btnStorage.CanEnabled_Callback = logic.IsAvailable;
            
            view.OnUpdateContentOnShow_Callback = UpdateView;
            view.OnHide_Callback = OnHide;
            view.onTabSwitchedCallback = OnTabSwitched;

            view.genericView.SetToggleCallback("delete", SwitchDeleteMode);
            view.genericView.SetButtonCallback("upgrade", () => { parentFacade.WarnStorageIsFull(false); });

            logic.onStorageUpdated += OnStorageUpdated;
            logic.onLevelUp += OnLevelUp;

            this.overheadIcon = overheadIcon;

            var storageObj = GameObject.FindGameObjectWithTag (storageRoomTag);
            Assert.IsNotNull(storageObj);
            if (storageObj != null)
            {
                storageRoomView = storageObj.GetComponent<StorageRoomView>();
                Assert.IsNotNull(storageRoomView);

                if (overheadIcon != null)
                {
                    var cityObject = GameObject.FindGameObjectWithTag (storageRoomTag);
                    Assert.IsNotNull(cityObject);
                    if (cityObject != null)
                    {
                        var iconAttachment = cityObject.transform.Find(iconAttachmentObjectName);
                        if (iconAttachment != null)
                        {
                            overheadIcon.Instantiate();
                            overheadIcon.instance.onClick.AddListener (OnOverheadUIClick);
                            overheadIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(iconAttachment.position);
                            overheadIcon.icon.owner = gui;

                            var overheadUIData = new OverheadUIDataBuildings();
                            overheadUIData.state = OverheadUIDataBuildings.State.RoomStorage;
                            overheadUIData.ApplyData (overheadIcon.instance);
                        }
                    }
                }
                else
                {
                    Debug.LogWarningFormat("{0}: Missing Overhead UI Icon prefab.", GetType().Name);
                }
            }
        }

        public void Start()
        {
            UpdateOverheadUI();

            view.OnShow_Callback += OnShowCallback;
            view.OnHide_Callback += OnHideCallback;

            view.onShowByCityClick += OnShowByCityClick;

            gui.makeJuice.storageInfoGetter = GetStorageRatio;
            gui.makeJuice.storageCapacityGetter = () => { return logic.capacityRatio; };
        }

        public void Destroy()
        {
            view.OnShow_Callback -= OnShowCallback;
            view.OnHide_Callback -= OnHideCallback;

            view.onShowByCityClick -= OnShowByCityClick;
        }

        private void OnShowByCityClick()
        {
            if (!logic.isAnyTutorialRunning)
                view.Show();
        }

        void OnShowCallback()
        {
            SportsmanTypeChangedEvent.Event += SportsmanTypeChangedEventOnEvent;
            CharacterBirthEvent.Event += CharacterBirthEventOnEvent;
        }

        void OnHideCallback()
        {
            SportsmanTypeChangedEvent.Event -= SportsmanTypeChangedEventOnEvent;
            CharacterBirthEvent.Event -= CharacterBirthEventOnEvent;
        }

        private void CharacterBirthEventOnEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                UpdateView();
        }

        private void SportsmanTypeChangedEventOnEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            UpdateView();
        }

        private string GetStorageRatio()
        {
            return logic.countTotal.ToString() + "/" + logic.capacity.ToString();
        }

        public void OnStorageUpdated(int countTotal, int capacity, float capacityRatio, bool haveNew )
        {
            if (storageRoomView != null)
                storageRoomView.OnStorageUpdated(capacityRatio);

            gui.hud.SetStorageNewItems(haveNew);

            gui.hud.storageProgress = capacityRatio;
            gui.hud.storageItemsTxt = GetStorageRatio();

            if (wasCont != -1 && wasCont != countTotal && wasCont < countTotal)
            {
                LeanTween.value(wasCont, countTotal, 1.0f).setOnUpdate((float a)=> { gui.hud.storageItemsTxt = ((int)a).ToString() + "/" + capacity.ToString(); });
            }

            gui.hud.SetPlatinum(logic.GetPlatinumCount());

            wasCont = countTotal;
        }

        public void OnStorageUpgraded(int from, int to)
        {
            gui.confirmWnd.OnCustom(Loc.Get("storageUpgradedHeader"), Loc.Get("storageUpgradedMessage", from, to));
        }

        public void OnLevelUp(int level)
        {
            UpdateOverheadUI();
        }

        private void OnTabSwitched(int tabindex)
        {
            UpdateView();
        }

        void OnHide()
        {
            deleteMode = false;
            tmpItems = null;
        }

        void UpdateView()
        {
            // we use tmpItems to keep order while editing

            var items = logic.allItems;

            if (tmpItems == null)
            {
                tmpItems = items;
            }
            else 
            {
                while (tmpItems.Count != items.Count)
                {
                    foreach (var itm in tmpItems)
                    {
                        var unavailable = (logic.GetResourcesCount(itm.id) == 0);

                        if (unavailable)
                        {
                            tmpItems.Remove(itm);
                            break;
                        }
                    }
                }
            }

            items = tmpItems;

            view.UpdateItemsCount(items.Count);
//            view.ClearHintCallbacks();
            var viewItems = view.items;

            view.capacityRatioStr    = logic.capacityRatioStr;
            view.capacityRatio       = logic.capacityRatio;

            view.genericView.SetActive("upgrade", logic.canUpgrade);
            view.genericView.SetToggelOn("delete", deleteMode);

            // var tooltip = parent.gameObject.GetComponent<TooltipPresenter>();

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var viewItem = viewItems[i];
                var genericItem = viewItems[i].genericView;

                viewItem.idx = i;
                
                genericItem.SetSprite("icon", item.sprite);
                genericItem.SetText("count", "x" + logic.GetResourcesCount(item.id));
                genericItem.SetActive("delete", deleteMode);
                genericItem.SetButtonCallback("delete", delegate() { DeleteItem(viewItem.idx); });

                viewItem.hint.title = item.hintTitle;
                viewItem.hint.desc = item.hintDesc;

                /*
                viewItem.startHint.AddListener((me) =>
                {
                    tooltip.ShowTooltipForResourceNear(items[me.idx].id, (RectTransform) me.transform);
                });

                viewItem.endHint.AddListener(me => tooltip.Hide());
                */
            }

            logic.OnWinStorageOpen();
            gui.hud.SetStorageNewItems(false);
        }

        void SwitchDeleteMode(bool value)
        {
            deleteMode = value;
            tmpItems = null;
            UpdateView();
        }

        void DeleteItem(int idx)
        {
            var items = tmpItems;
            if (idx >= items.Count)
            {
                Debug.LogError("PresentationStorage:DeleteItem wrong idx");
                return;
            }

            var item = items[idx];
            logic.SpendResource(item.id, new Analytics.MoneyInfo("Delete from storage", ((ResourceType)item.id).ToString()));
            UpdateView();
        }

        void UpdateOverheadUI()
        {
            if (overheadIcon != null && overheadIcon.icon != null)
            {
                overheadIcon.icon.visible = logic.IsAvailable();
            }
        }

        void OnOverheadUIClick()
        {
            view.Show();
        }

        /*
        public void OnAddPlatinum(int count)
        {
            if (count <= 0)
                return;
            gui.hud.SetPlatinum(logic.GetPlatinumCount());
        }
        */
    }
}
