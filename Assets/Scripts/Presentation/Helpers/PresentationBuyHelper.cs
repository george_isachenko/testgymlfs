﻿using System;
using System.Linq;
using Data;
using Logic.Facades;
using UnityEngine;
using View.UI;
using Presentation.Providers;

namespace Presentation.Helpers
{
    public static class PresentationBuyHelper
    {
        public static BuyWithResourcesView BuyWithResources(GUIRoot gui, IPlayerProfileInteractionProvider playerProfile, Cost cost, Action buyCallback)
        {
            if ( ! (cost.type == Cost.CostType.ResourcesAndCoins || cost.type == Cost.CostType.ResourcesOnly))
            {
                Debug.LogError("BuyWithResources wrong cost type");
                return null;
            }

            var storage = playerProfile.storage;

            // FIX: This check should not be here, really. It prevents buying 
/*
            if (!storage.canUpgrade)
            {
                Debug.LogError("PresentationStorage : upgrade not available");
                return null;
            }
*/

            var useCoins = cost.value > 0;
            var view = gui.buyWithResources;
            view.costView.mediator.SetView(cost, playerProfile);

            view.Show();
            view.genericView.SetButtonCallback("btnBuy", buyCallback);
            var coinsDiff = 0;
            if (cost.type == Cost.CostType.ResourcesAndCoins)
            {
                if (cost.value > playerProfile.coins)
                {
                    coinsDiff = cost.value - playerProfile.coins;

                }
            }
            var priceTotal = 0;
            if (useCoins && coinsDiff > 0)
            {
                var bucks = Cost.CoinsToFitBucks(coinsDiff);
                priceTotal += bucks;
            }

            priceTotal += (from res in cost.resources
                let resNeeded = res.Value
                let resInStorage = storage.GetResourcesCount(res.Key)
                let priceOn = resNeeded > resInStorage
                where priceOn
                select Cost.RecourcePriceInFitbucks(res.Key)*(resNeeded - resInStorage)).Sum();

            view.priceTotal = priceTotal;
            view.extraPriceOn = priceTotal > 0;

            return view;
        }

    }
}