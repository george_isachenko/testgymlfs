﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Presentation.Base;
using Logic.Quests.Juicer;
using View.UI.OverheadUI.Components;
using UnityEngine.Assertions;
using View.UI;
using Presentation.Facades;
using Logic.PlayerProfile.Events;

namespace Presentation.Quests
{
    public class PresentationQuestsJuicer : BasePresentationNested<JuiceQuestManager, PresentationQuests>
    {
        JuicerOrderIcon orderView;

        int itemsCount = 0;

        public PresentationQuestsJuicer(JuiceQuestManager logic, PresentationQuests parentFacade)
            :base(logic, parentFacade)
        {
            orderView = JuicerOrderIcon.instance;
            Assert.IsNotNull(orderView);

            orderView.dropHandler.OnDropCallback = OnDrop;
            orderView.dragHandler.OnDragBeginCallback = OnDragBegin;
            orderView.dragHandler.OnDragEndCallback = OnDragEnd;

            orderView.dropHandler.isOn = false;

            StorageUpdatedEvent.Event += OnStorageUpdatedEventEvent;
        }

        public void OnDestroy()
        {
            StorageUpdatedEvent.Event -= OnStorageUpdatedEventEvent;
        }

        void OnStorageUpdatedEventEvent(int countTotal, int capacity, float capacityRatio)
        {
            foreach(var quest in logic.quests)
            {
                parent.characterInteractionProvider.UpdateOverheadUI(quest.juicerId);
            }
        }




        public void UpdateOrder(JuiceQuestLogic quest)
        {
            if (quest.completed)
            {
                foreach( var viewItem in orderView.items)
                    viewItem.isOn = false;
                
                return;
            }

            itemsCount = quest.objectives.Count;
                
            UpdateViewItems(orderView.items,        quest.objectives);
            var availableAll = UpdateViewItems(orderView.itemsCopy,    quest.objectives);

            orderView.arrowOn = availableAll;

            orderView.rewardCoins.text = quest.reward.coins.ToString();
            orderView.rewardExp.text = quest.reward.experience.ToString();
        }

        bool UpdateViewItems(JuicerOrderIconItem[] viewItems, List<JuiceQuestObjectiveLogic> items)
        {
            var availableAll = true;
            for(int i = 0; i < viewItems.Length; i++)
            {
                viewItems[i].isOn = i < items.Count;

                if (i < items.Count)
                    availableAll &= UpdateViewItem(viewItems[i], items[i]);
            }

            return availableAll;
        }

        bool UpdateViewItem(JuicerOrderIconItem viewItem, JuiceQuestObjectiveLogic item)
        {
            viewItem.sprite = GUICollections.instance.storageIcons.items[(int)item.juice].sprite;
            var inStorage = logic.playerProfile.storage.GetResourcesCount(item.juice);
            string strColor = inStorage >= item.amount ? "<color=#B0E727FF>" : "<color=#FF8C8DFF>";
            viewItem.amount = strColor + inStorage.ToString() + "/" + item.amount.ToString() + "</color>";
            viewItem.checkerOn = inStorage >= item.amount;

            return inStorage >= item.amount;
        }

        public void BuyShortageOfJuices(JuiceQuestLogic quest)
        {
            var cost = quest.order;

            var costShortage = logic.playerProfile.GetShortageCost(cost);
            Assert.IsNotNull(costShortage);

            if (costShortage != null)
            {
                gui.confirmWithBucks.OnNeedResources2
                ( costShortage
                    , logic.playerProfile.storage.GetExtraBucksPrice(cost, null).value, 
                    false, true
                );

                gui.confirmWithBucks.OnOpenShop = () =>
                {
                    gui.shopUltimate.Show();
                    gui.shopUltimate.ShowBankBucks();
                    Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PresentationQuestsJuicer:BuyShortageOfFruits");
                };

                gui.confirmWithBucks.OnConfirm = () =>
                {
                    if (logic.BuyShortageOfJuices(quest))
                    {
                        logic.TryCompleteSelectedQuest();
                    }
                };
            }
        }

        void OnDragBegin()
        {
            orderView.OnDragBegin();
        }

        void OnDragEnd()
        {
            orderView.OnDragEnd(itemsCount);
        }

        void OnDrop()
        {
            logic.TryCompleteSelectedQuest();
        }

        
    }
}