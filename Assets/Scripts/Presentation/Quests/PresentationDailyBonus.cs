﻿using Logic.Quests.DailyBonus;
using Presentation.Base;
using Presentation.Facades;
using UnityEngine;
using View.UI;
using View.UI.Base;

namespace Presentation.Quests
{
    public class PresentationDailyBonus : BasePresentationNested<DailyBonusLogic, PresentationQuests>
    {
        DailyBonusView  view;
        
        public PresentationDailyBonus(DailyBonusLogic logic, PresentationQuests parentFacade)
            :base(logic, parentFacade)
        {
            view = gui.dailyBonus;

            view.OnUpdateContentOnShow_Callback = UpdateView;
            view.OnClosedByUserClick_Callback = logic.OnCloseByUserClick;
            gui.hud.btnDailyBonus.CanEnabled_Callback = IconVisible;
            view.timer.textGetter = () => { return View.PrintHelper.GetTimeString(logic.timeToNextDay); };
        }

        public void UpdateView()
        {
            
            if (!logic.IsAvailableByLevel())
            {
                Debug.LogError("PresentationDailyBonus : not available for this level");
                gui.hud.UpdateHUD();
                return;
            }

            view.btnCloseOn = logic.accepedByUser; //logic.items[0].unlockPrice.value > 0;

            view.UpdateItemsCount(logic.items.Length);
            //view.currentDay = logic.currentDay;
            //view.mandatoryReward = logic.mandatoryReward;

            for (int i = 0; i < logic.items.Length; i++)
            {
                var item = logic.items[i];
                var itemView = view.items[i];

                itemView.idx = i;
                itemView.sprite = item.data.reward.sprite;
                itemView.count = item.data.reward.value;
                //itemView.closed = item.closed;
                //itemView.unlockPrice = item.unlockPrice;
                //itemView.paid = item.paid;
                itemView.claimed = item.data.claimed;
                itemView.current = i == logic.currentDay && !item.data.claimed;
                itemView.superBonus = ((i + 1) % DailyBonusLogic.cSuperBonusPeriod == 0) && !item.data.claimed;
                itemView.dayNumber = i + 1;
                itemView.gray = (i > logic.currentDay) && !item.data.claimed;

                itemView.OnClickCallback = OnItemClick;   
            }
        }

        public void UpdateHUD()
        {
            gui.hud.btnDailyBonus.turnOn = true;
        }

        public void  OnItemClick(UIBaseViewListItem viewItem)
        {
            /*
            var item = logic.items[viewItem.idx];

            if (item.paid)
                logic.Buy(viewItem.idx);
            else if (item.closed)
                logic.Unlock(viewItem.idx);
            */

            logic.Unlock(viewItem.idx);

            UpdateView();
        }

        private bool IconVisible()
        {
            //if (parent.clubInteractionProvider.activeRoom == null)
            //    return false;
            //bool isClub = parent.clubInteractionProvider.activeRoom.roomConductor.roomSetupDataInfo.roomType == Data.Room.RoomType.Club;

            return /*isClub &&*/ logic.IsAvailable();
        }
    }
}