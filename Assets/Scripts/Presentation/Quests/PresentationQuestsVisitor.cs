﻿using System;
using System.Linq;
using Data.Estate;
using Data.Estate.Info;
using Data.Person.Info;
using Data.Room;
using Logic.Quests;
using Presentation.Base;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI;
using Data.Person;
using Logic.Quests.Visitor;
using System.Collections;
using Presentation.Facades;
using Logic.Persons;
using Data;
using View.Actors;

namespace Presentation.Quests
{
    public class PresentationQuestsVisitor : BasePresentationNested<VisitorQuestsManager, PresentationQuests>
    {
        public event Action<int> SelectVisitorById;
        // public event Action      RequestExpand; 

        private VisitorQuestsView view;
        private IEnumerator updateViewJob;

        public PresentationQuestsVisitor(VisitorQuestsManager logic, PresentationQuests parentFacade)
            :base(logic, parentFacade)
        {
            view = gui.visitorQuests;

            view.OnUpdateContentOnShow_Callback = ScheduleUpdateView;

            view.OnItemSelected_Callback = OnItemSelected;
            view.OnLiveUpdate_Callback = UpdateTimers;
            view.OnHide_Callback = OnHide;

            Logic.Persons.Events.SetSelectionEvent.Event += OnSetSelectionEvent;
            parent.characterInteractionProvider.onCharacterSelected += OnCharacterSelected;
            parent.characterInteractionProvider.onCharacterDeselected += OnCharacterDeselected;

            parent.equipmentInteractionProvider.onEquipmentSelected += OnEquipmentSelected;
            parent.equipmentInteractionProvider.onEquipmentDeselected += OnEquipmentDeselected;
        }

        public void OnDestroy()
        {
            Logic.Persons.Events.SetSelectionEvent.Event -= OnSetSelectionEvent;
            parent.characterInteractionProvider.onCharacterSelected -= OnCharacterSelected;
            parent.characterInteractionProvider.onCharacterDeselected -= OnCharacterDeselected;

            parent.equipmentInteractionProvider.onEquipmentSelected -= OnEquipmentSelected;
            parent.equipmentInteractionProvider.onEquipmentDeselected -= OnEquipmentDeselected;
        }

        void UpdateStyleAndCapacity()
        {
            var roomName = logic.roomManager.GetRoomNameOfType(RoomType.Club);
            Assert.IsTrue(roomName != null && roomName != string.Empty);

            //int stylePoints = logic.roomManager.roomCapacity.GetStylePoints (roomName);
            //int maxStylePoints = logic.roomManager.roomCapacity.GetMaxStylePoints(roomName);
            //var stylePoints = logic.roomManager.roomCapacity.GetCurrentStylePoints(roomName);
            //var stylePointsMax = logic.roomManager.roomCapacity.GetCurrentStylePointsMax(roomName);

            //gui.hud.stylePoints = stylePoints + "/" + stylePointsMax;
            //gui.hud.styleProgress = Mathf.Clamp01 ((float)stylePoints / (float)stylePointsMax);

            int maxVisitorsCount = logic.roomManager.roomCapacity.GetStyleCapacity (roomName);
            int actualVisitorsCount = logic.ActualQuestsCount();

            view.txtVisitorCount = actualVisitorsCount.ToString() + "/" + maxVisitorsCount;

            gui.hud.visitorQuestsCount = actualVisitorsCount.ToString() + "/" + maxVisitorsCount;

            bool isShowRestStylePoints = logic.playerProfile.level >= logic.roomManager.roomCapacity.GetFirstExpandLevel(roomName);
            if (isShowRestStylePoints) 
            {
                view.UpdateItemsCount(view.items.Count + 1);

                var viewItem = view.items[view.items.Count - 1];

                int stylePoints = logic.roomManager.roomCapacity.GetTotalStylePoints (roomName);
                viewItem.SetNextVisitorMode (logic.roomManager.roomCapacity.GetStyleLevelForNextVisitor (stylePoints, roomName));
                viewItem.idx = -1;
            }
        }

        void OnHide()
        {
            view.SetSelectedItem(null, false);
        }

        public void ScheduleUpdateView()
        {
            if (updateViewJob == null)
            {
                updateViewJob = UpdateViewJob();
                parent.StartCoroutine(updateViewJob);
            }
        }

        public void UpdateTimers()
        {
            if (logic.quests == null)
            {
                Debug.LogError("PresentationQuestsVisitor logic.quests == null");
                return;
            }
            
            for (int i = 0; i < logic.quests.Count; i++)
            {
                var quest = logic.quests[i];
                var viewItem = view.items[i];

                if (quest.timer != null)
                {
                    viewItem.time = quest.refreshTime;
                    viewItem.skipPrice = quest.PriceToSkip().value;
                }
            }
        }

        public void  OnSelectVisitor(int visitorId)
        {
            if (view.isVisible)
            {
                var questsCount = view.items.Count;
                for (int i = 0; i < questsCount; i++) 
                {
                    var viewItem = view.items [i];
                    viewItem.selected = visitorId == viewItem.idx;
                }   
            }
        }

        void UpdateVisitorInfo(Person person)
        {
            var trainersIsOn = logic.trainers.canUseMyFeature.canUse;
            var trainersAvailable = logic.trainers.availableSomeone;

            //var trainersCount = logic.trainers.trainers.Count();

            var visitor = person as PersonVisitor;
            var trainerClient = person as PersonTrainerClient;
            var trainerAssigned = trainerClient != null;

            if (visitor == null)
                return;
            gui.hud.personPanel.ShowVisitor();

            var visitorPanel = gui.hud.personPanel.visitor;
            var visitorQuest = visitor.visitorQuest;

            if (visitorQuest == null)
            {
                Debug.LogErrorFormat("Visitor id{0} {1} has no quest", visitor.id, visitor.name);
                visitorPanel.gameObject.SetActive(false);
                return;
            }

            var trainersAvailableSkill = logic.trainers.IsAvailableWithSkill(visitorQuest.trainerLevel);

            visitorPanel.personName   = person.name;

            visitorPanel.portrait = parent.characterInteractionProvider.GetPortrait(person.id);

            visitorPanel.btnAssignTrainerOn = trainersIsOn && visitor.canAssignTrainer && trainersAvailable && trainersAvailableSkill;
            visitorPanel.workingWithTrainerOn   = trainersIsOn && trainerAssigned;
            visitorPanel.btnTrainerNotAvailable = trainersIsOn && !trainersAvailable && (!trainerAssigned) && (!visitorQuest.completed);
            visitorPanel.btnNotTrainerWithSkill = trainersIsOn && trainersAvailable && (!trainerAssigned) && (!visitorQuest.completed) && (!trainersAvailableSkill);

            if (trainerClient != null)
            {
                visitorPanel.bgColor                = gui.hud.personPanel.trainer.bgColor;
                visitorPanel.btnClaimOn             = false;
                visitorPanel.btnKickOn              = false;
            }
            else
            {
                visitorPanel.bgColor                = Color.white;
                visitorPanel.btnClaimOn             = visitorQuest.completed;
                visitorPanel.btnKickOn              = person.state == PersonState.Idle;
            }

            for (int i = 0; i < 3; i++)
            {
                if (i < visitorQuest.objectivesCount)
                {
                    var obj = visitorQuest.objectives[i];

                    visitorPanel.tasks[i].empty = false;
                    visitorPanel.tasks[i].tier = 0;

                    if (obj.type == Data.Quests.Visitor.VisitorQuestType.Exercise)
                    {
                        visitorPanel.tasks[i].exerciseSprite = GUICollections.instance.exerciseSpritesList.array[obj.exercise.icon];
                        visitorPanel.tasks[i].tier = obj.exercise.tier;
                    }
                    else
                    {
                        visitorPanel.tasks[i].exerciseSprite = GUICollections.instance.stateSpritesList.array
                            [GUICollections.GetIconIdxForExerciseType(visitorQuest.objectives[i].exerciseType)];
                    }

                    visitorPanel.tasks[i].txtCount = obj.progress;
                    visitorPanel.tasks[i].done = obj.completed;

                }
                else
                {
                    visitorPanel.tasks[i].empty = true;
                }

                IEquipmentActor equipmentActor = null;
                if (parent.equipmentInteractionProvider.selectedEquipmentId != EquipmentData.InvalidId)
                    equipmentActor = parent.equipmentInteractionProvider.GetEquipmentActor(parent.equipmentInteractionProvider.selectedEquipmentId);

                if (equipmentActor != null)
                    visitorPanel.rewards = visitorQuest.CalculateEstimatedRewards(equipmentActor.equipmentInfo);
                else
                    visitorPanel.rewards = new Logic.Quests.Base.QuestEstimatedRewards(visitorQuest.reward);
            }
        }

        void OnSetSelectionEvent(Person person, bool selected, bool focus)
        {
            if (person is PersonVisitor && selected)
                UpdateVisitorInfo(person);
        }

        private void OnCharacterSelected (IPersonInfo personInfo, bool focus)
        {
            PersonSelectionChanged(personInfo.id, true);
        }

        private void OnCharacterDeselected (IPersonInfo personInfo)
        {
            PersonSelectionChanged(personInfo.id, false);
        }

        private void OnEquipmentSelected (int equipmentId, IEquipmentInfo equipmentInfo)
        {
            EquipmentSelectionChanged(equipmentId);
        }

        private void OnEquipmentDeselected (int equipmentId)
        {
            EquipmentSelectionChanged(equipmentId);
        }

        void OnItemSelected(VisitorQuestsViewItem item) 
        {
            if (item != null)
            {
                var roomName = logic.roomManager.GetRoomNameOfType(RoomType.Club);
                bool isShowRestStylePoints = logic.playerProfile.level >= logic.roomManager.roomCapacity.GetFirstExpandLevel(roomName);
                if (isShowRestStylePoints && item.idx == -1)
                {
                    gui.styleWindow.Show();
                }
                else
                {
                    SelectVisitorById?.Invoke(item.idx);
                }

                /*
                if (item.itemType == VisitorQuestsViewItem.ItemType.NextVisitor)
                {
                    if (RequestExpand != null)
                        RequestExpand.Invoke();
                }
                */
            }
        }

        private void UpdateNormalModeItem (VisitorQuestLogic quest, VisitorQuestsViewItem viewItem)
        {
            IEquipmentInfo selectedEquipmentInfo = null;
            if (parent.equipmentInteractionProvider.selectedEquipmentId != EquipmentData.InvalidId && viewItem.selected)
            {
                var equipmentActor = parent.equipmentInteractionProvider.GetEquipmentActor(parent.equipmentInteractionProvider.selectedEquipmentId);
                if (equipmentActor != null)
                    selectedEquipmentInfo = equipmentActor.equipmentInfo;
            }

            viewItem.SetNormalMode(quest.objectivesCount, quest.objectivesCompleted, quest.CalculateEstimatedRewards(selectedEquipmentInfo));
        }

        private IEnumerator UpdateViewJob()
        {
            yield return null;

            //var questsCount = logic.quests.Count;
            //view.UpdateItemsCount(questsCount);

            var questsList = logic.quests;
            view.UpdateItemsCount(questsList.Count);

            for (int i = 0; i < questsList.Count; i++)
            {
                var quest = questsList[i];
                var viewItem = view.items[i];

                viewItem.isTrainerClient = logic.trainers.IsTrainerClient(quest.visitorId);

                if (quest.timer != null)
                {
                    viewItem.SetTimerMode ();
                    viewItem.time = quest.refreshTime;
                    viewItem.skipPrice = quest.PriceToSkip().value;

                    viewItem.skipHandler += quest.OnTrySkipQuest;
                }
                else
                {
                    viewItem.selected = (parent.characterInteractionProvider.selectedCharacterId == quest.visitorId); // Set first, used by UpdateNormalModeItem().

                    UpdateNormalModeItem(quest, viewItem);

                    viewItem.visitorName            = quest.visitorName;
                    viewItem.idx                    = quest.visitorId;
                }
            }

            UpdateStyleAndCapacity();

            Assert.IsNotNull(updateViewJob);
            updateViewJob = null;
        }

        private void PersonSelectionChanged (int personId, bool selected)
        {
            if (view.isVisible)
            {
                foreach (var viewItem in view.items)
                {
                    if (viewItem.itemType == VisitorQuestsViewItem.ItemType.Normal && viewItem.idx == personId)
                    {
                        viewItem.selected = selected;
                        var quest = logic.GetVisitorQuest(personId);
                        if (quest != null)
                        {
                            UpdateNormalModeItem(quest, viewItem);
                        }
                        break;
                    }
                }
            }
        }

        private void EquipmentSelectionChanged (int equipmentId)
        {
            if (view.isVisible && parent.characterInteractionProvider.selectedCharacterId != PersonData.InvalidId)
            {
                foreach (var viewItem in view.items)
                {
                    if (viewItem.itemType == VisitorQuestsViewItem.ItemType.Normal &&
                        viewItem.idx == parent.characterInteractionProvider.selectedCharacterId)
                    {
                        var quest = logic.GetVisitorQuest(viewItem.idx);
                        if (quest != null)
                        {
                            UpdateNormalModeItem(quest, viewItem);
                        }
                    }
                }
            }
        }
    }
}