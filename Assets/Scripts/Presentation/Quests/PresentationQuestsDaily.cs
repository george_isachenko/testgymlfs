﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Data;
using Data.Quests;
using Data.Quests.DailyQuest;
using Logic.Quests;
using Logic.Quests.DailyQuest;
using Presentation.Base;
using UnityEngine;
using UnityEngine.UI;
using View.UI;
using View.UI.Base;
using Presentation.Facades;

namespace Presentation.Quests
{
    public class PresentationQuestsDaily : BasePresentationNested<DailyQuestManager, PresentationQuests>
    {
        private List<DailyQuestLogic> quests 
        {
            get { return logic.quests; }
        }

        private readonly DailyQuestView  view;
        private DailyQuestLogic selectedQuest;

        public PresentationQuestsDaily(DailyQuestManager logic, PresentationQuests parentFacade)
            : base(logic, parentFacade)
        {
            view = gui.dailyQuests;

            view.OnUpdateContentOnShow_Callback = UpdateContent;
            view.onClaimClickCallback = OnClaimClick;
            view.questsNamesList.OnItemSelected_Callback = OnItemSelected;
            view.onLiveUpdateCallback = UpdateDynamicValues;
            view.onSkipClickCallback += OnSkipClick;

            gui.hud.btnDailyQuests.CanEnabled_Callback = QuestsIconVisible;
            gui.hud.btnDailyQuests.onInactiveClick = ShowDailyBonusHint_;

            view.genericView.SetActiveChildren("objectivesGrid");
            view.genericView.SetButtonCallback("dailyBonus", gui.dailyBonus.Show);
            view.genericView.SetButtonCallback("ad", OpenShopAd);

            /*
            view.resourcesJumpAnimationStart.AddListener(() =>
            {
                    
                //gui.hud.btnStorageFakeGO.transform.FindChild("StorageTxt").GetComponent<Text>().text =
                //    logic.GetStorageCapacityRatioStr();
                //gui.hud.btnStorageFake.turnOn = true;

                    UpdateFakeStorageBtn();
            });
        */
                    
            view.resourcesJumpAnimationFinish.AddListener(() =>
            {
                gui.hud.btnStorageFake.turnOn = false;
                view.SetPresentDefaultParent();
            });

            view.OnHideHandler.AddListener(() =>
            {
                gui.hud.btnStorageFake.turnOn = false;
            });

#if DEBUG
            gui.dbgView.genericView.SetButtonCallback("completeFirstSimpleDailyQuest",
                () =>
                {
                    foreach (DailyQuestLogic questLogic in quests)
                    {
                        if (questLogic.dailyQuestType != DailyQuestType.DailyQuest && !questLogic.completed)
                        {
                            questLogic.CompleteSteps(50);
                            break;
                        }
                    }
                });

            gui.dbgView.genericView.SetButtonCallback("completeAllSimpleDailyQuest",
                () =>
                {
                    foreach (DailyQuestLogic questLogic in quests)
                    {
                        if (questLogic.dailyQuestType != DailyQuestType.DailyQuest && !questLogic.completed)
                        {
                            questLogic.CompleteSteps(50);
                        }
                    }
                });
#endif
        }

        void UpdateFakeStorageBtn()
        {
            gui.hud.btnStorageFakeGO.transform.Find("StorageTxt").GetComponent<Text>().text =
                logic.GetStorageCapacityRatioStr();

            gui.hud.btnStorageFakeGO.transform.Find("StorageBar").GetComponent<Slider>().value = logic.GetStorageCapacityRatio();

            gui.hud.btnStorageFake.turnOn = true;
        }

        public void OpenShopAd()
        {
            parent.shopInteractionProvider.ShowRewardedAd(OnAdsShowFinished);
        }

        public bool IsVideoAdAvailable()
        {
            return parent.shopInteractionProvider.IsRewardedVideoAvailable();
        }

        void OnAdsShowFinished(bool result, Cost reward)
        {
            if (result && reward != null)
            {
                parent.shopInteractionProvider.ConfirmAdReward(reward);
            }
        }

        private bool QuestsIconVisible()
        {
            //if (parent.clubInteractionProvider.activeRoom == null)
            //    return false;
            //var isClub = parent.clubInteractionProvider.activeRoom.roomConductor.roomSetupDataInfo.roomType == Data.Room.RoomType.Club;

            var isEditing = gui.windows.states.inEditMode; //parent.clubInteractionProvider.isInEditMode;

            return /*isClub &&*/ !isEditing && logic.IsAvailableByLevel();
        }

        public void UpdateContent()
        {
            var questsCount = quests.Count;

            if (questsCount == 0)
            {
                gui.hud.dailyQuestCount = 0;
                return;
            }

            gui.hud.btnDailyQuests.turnOn = true;
            gui.hud.dailyQuestCount = logic.NewQuests();


            logic.CheckItemsOrder();

            view.questsNamesList.UpdateItemsCount(questsCount);

            for (int i = 0; i < questsCount; i++)
            {
                var quest = quests[i];
                var viewItem = gui.dailyQuests.questsNamesList.items[i];

                viewItem.idx = i;
                if (i == 0)
                {
                    viewItem.localizationKey = quest.title;
                }
                else
                {
                    viewItem.title = quest.title;
                }
                viewItem.SetState(GetViewStateForQuest(quest));
            }

            if (gui.dailyQuests.questsNamesList.selectedItem == null ||
                gui.dailyQuests.questsNamesList.selectedItem == gui.dailyQuests.questsNamesList[0])
                gui.dailyQuests.questsNamesList.SetSelectedItem(gui.dailyQuests.questsNamesList[1], false);

            if (gui.dailyQuests.questsNamesList.selectedItem == null && gui.dailyQuests.questsNamesList.items.Count > 0)
            {
                gui.dailyQuests.questsNamesList.SetSelectedItem(gui.dailyQuests.questsNamesList.items[0], false);
            }
            else
            {
                OnItemSelected(gui.dailyQuests.questsNamesList.selectedItem);
            }

            var mainQuest = quests.FirstOrDefault(q => q.dailyQuestType == DailyQuestType.DailyQuest);
            if (mainQuest != null)
            {
                var progress = mainQuest.iterationsDone * 1f/ mainQuest.iterationsGoal;
                view.SetMainQuestProgress(progress);
                view.SetPresentIdleState(mainQuest.completed);
            }
            else
            {
                Debug.LogError("Can't find daily quest inn  quests scope and update it state. Update skipped.");
            }
        }

        private DailyQuestViewItem.DailyQuestViewItemState GetViewStateForQuest(DailyQuestLogic quest)
        {

            if (!string.IsNullOrEmpty(quest.refreshTime))
                return DailyQuestViewItem.DailyQuestViewItemState.Waiting;
            if (quest.completed)
                return DailyQuestViewItem.DailyQuestViewItemState.Completed;
            return quest.isNew
                ? DailyQuestViewItem.DailyQuestViewItemState.New
                : DailyQuestViewItem.DailyQuestViewItemState.Normal;
        }

        void UpdateDynamicValues()
        {
            if (view.questsNamesList.items.Count == quests.Count)
            {
                // timers
                for (int i = 0; i < view.questsNamesList.items.Count; i++)
                {
                    var quest = quests[i];
                    var viewItem = view.questsNamesList.items[i];

                    if (quest.timer != null)
                    {
                        viewItem.title = string.Format("<color=#0178A2FF>{0}</color>", quest.refreshTime);
                    }
                }

                // skip button
                if (selectedQuest != null && selectedQuest.claimed && selectedQuest.timer!=null)
                {
                    view.SetSkipCost(DynamicPrice.GetSkipCost((int) selectedQuest.timer.secondsToFinish));
                }
            }
            else
            {
                UpdateContent();
            }
        }

        void OnItemSelected(UIBaseViewListItem viewItem)
        {
            selectedQuest = null;
            var item = viewItem as DailyQuestViewItem;

            if (item != null)
            {
                var quest = quests[item.idx];
                selectedQuest = quest;
                
                if (quest.isNew)
                    logic.SaveLazily();

                quest.WasSeen();
                item.SetState(GetViewStateForQuest(quest));
                view.SetTextDescription(string.IsNullOrEmpty(quest.refreshTime)
                    ? quest.subTitle
                    : Loc.Get("dailyQuestCooldownDescription"));
                gui.hud.dailyQuestCount = logic.NewQuests();

                if (quest != null)
                {
                    gui.dailyQuests.claimed = quest.claimed;
                    SetViewTimerIfNeed(quest);


                    var rewards = new List<SportQuestRewardViewData>();
                    if (quest.dailyQuestType == DailyQuestType.DailyQuest)
                    {
                        rewards.Add(new SportQuestRewardViewData()
                        {
                            icon = Data.Cost.GetSprite(quest.GetRepairResource()),
                            countTxt = quest.reward.coins.ToString()
                        });

                        rewards.Add(new SportQuestRewardViewData()
                        {
                            icon = Data.Cost.GetSprite(quest.rewardResource),
                            countTxt = quest.reward.experience.ToString()
                        });
                    }
                    else
                    {
                        rewards.Add(new SportQuestRewardViewData()
                        {
                            icon = Data.Cost.GetSpriteCoins(),
                            countTxt = quest.reward.coins.ToString()
                        });

                        rewards.Add(new SportQuestRewardViewData()
                        {
                            icon = Data.Cost.GetSpriteExp(),
                            countTxt = quest.reward.experience.ToString()
                        });
                    }

                    view.SetRewards(rewards);

                    gui.dailyQuests.canClaim = quest.completed && !quest.claimed;
                    
                    ShowQuestInfo(quest);

                    view.SetForceShowSkipAnimationState(true);

                    return;
                }
                else
                {
                    Debug.LogError("PresentationQuestsDaily: Can't find quest related to view item");
                }
            }
            else
            {
                gui.dailyQuests.claimed = true;
            }
        }

        private void SetViewTimerIfNeed(DailyQuestLogic quest)
        {
            if (!quest.claimed) return;
            if (quest.timer != null)
            {
                view.SetTimer(quest.timer);
            }
            else
            {
                Debug.LogErrorFormat("DailyQuest: Quest '{0}' is claimed but it's timer is null...",
                    quest.dailyQuestType);
            }
        }

        void ShowQuestInfo(DailyQuestLogic quest)
        {
            var genericView = gui.dailyQuests.genericView;

            genericView.SetActive("objectivesGrid", true);
            genericView.SetActiveChildren("objectivesGrid", false);

            switch (quest.dailyQuestType)
            {
                case DailyQuestType.Exercise:
                {
                    var spriteExercise = GUICollections.instance.exerciseSpritesList.array[quest.exercise.icon];
                    var spriteTier = GUICollections.instance.exerciseLevelSpritesList.array[quest.exercise.tier];

                    genericView.SetActive("exercise", !quest.claimed);
                    genericView.SetSprite("exerciseIcon", spriteExercise);
                    genericView.SetSprite("exerciseTier", spriteTier);
                    genericView.SetText("exerciseProgress", quest.progress);
                    genericView.SetActive("exerciseCompleted", quest.completed);
                }
                    break;

                case DailyQuestType.ExerciseTier:
                {
                    var spriteTier = GUICollections.instance.exerciseLevelSpritesList.array[quest.relatedId];

                    genericView.SetActive("tier", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetActive("tierTier", true);
                    genericView.SetSprite("tierTier", spriteTier);
                    genericView.SetText("tierProgress", quest.progress);
                    genericView.SetActive("tierCompleted", quest.completed);
                }
                    break;

                case DailyQuestType.ExerciseTotal:
                    genericView.SetActive("tier", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetActive("tierTier", false);
                    genericView.SetText("tierProgress", quest.progress);
                    genericView.SetActive("tierCompleted", quest.completed);
                    
                    break;

                case DailyQuestType.Stats:
                    var spriteStats =
                        GUICollections.instance.stateSpritesList.array[
                            GUICollections.GetIconIdxForExerciseType((ExersiseType) quest.relatedId)];

                    genericView.SetActive("stats", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetSprite("statsIcon", spriteStats);
                    genericView.SetText("statsProgress", quest.progress);
                    genericView.SetActive("statsCompleted", quest.completed);
                    break;

                case DailyQuestType.Equipment:
                    genericView.SetActive("equipment", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetText("equipmentProgress", quest.progress);
                    genericView.SetActive("equipmentCompleted", quest.completed);

                    var exerciseIcon =
                        GUICollections.instance.exerciseSpritesList.array[logic.GetFirstExercise(quest.equipment).icon];
                    genericView.SetSprite("equipmentIcon", exerciseIcon);
                    break;

                case DailyQuestType.VisitorQuest:
                    genericView.SetActive("visitor", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetText("visitorProgress", quest.progress);
                    genericView.SetActive("visitorCompleted", quest.completed);
                    break;

                case DailyQuestType.ClaimDailyReward:
                    genericView.SetActive("dailyBonus", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetActive("dailyBonusCompleted", quest.completed);
                    if (logic.IsAvailableDailyBonus())
                        genericView.SetButtonCallback("dailyBonus", gui.dailyBonus.Show);
                    else
                        genericView.SetButtonCallback("dailyBonus", null);
                    break;

                case DailyQuestType.VideoAd:
                    genericView.SetActive("ad", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetActive("adCompleted", quest.completed);
                    genericView.GetComponent<Button>("ad").interactable = IsVideoAdAvailable() && !quest.completed;
                    break;

                case DailyQuestType.DailyQuest:
                    genericView.SetActive("dailyQeust", !quest.claimed);
                    genericView.SetActive("attentionIcon", quest.claimed);
                    genericView.SetText("dailyQuestProgress", quest.progress);
                    genericView.SetActive("dailyQuestCompleted", quest.completed);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnSkipClick(DailyQuestViewItem item)
        {
            var quest = quests[item.idx];

            if (logic.SkipQuest(quest))
                quest.timer.FastForward(quest.timer.secondsToFinish + 1);

            UpdateContent();
        }

        private void OnClaimClick(DailyQuestViewItem item)
        {
            if (item == null)
                return;

            if (quests.Count <= item.idx || item.idx < 0)
                return;

            var quest = quests[item.idx];
            var delayedSkipShow = false;

            if (!quest.completed)
                return;
            
            if (logic.CanCliamQuest(quest))
            {

                if (quest.dailyQuestType == DailyQuestType.DailyQuest)
                {
                    view.SetPresentParent(gui.transform);
                    view.Hide();
                    //gui.hud.btnStorageFake.turnOn = true;
                    UpdateFakeStorageBtn();

                    var icon1 = Data.Cost.GetSprite(quest.GetRepairResource());
                    //countTxt = quest.reward.coins.ToString();
                    var icon2 = Data.Cost.GetSprite(quest.rewardResource);
                    //countTxt = quest.reward.expirience.ToString();
                    view.InitBackPackAnimation(icon1,icon2,gui.hud.btnStorageFakeGO.transform);

                }

//                 if (item.idx!=0) // debug: skip megaquest claiming
                quest.Claim();
                delayedSkipShow = true;
            }
            else
            {
                parent.playerProfileInteractionProvider.WarnStorageIsFull(true);
            }

            UpdateContent();

            if (delayedSkipShow)
            {
                view.SetForceShowSkipAnimationState(false);
                view.ActivateDelayedSkipShowing();
            }
        }

        private void ShowDailyBonusHint_()
        {
            gui.hud.StartCoroutine(ShowDailyBonusHint_Coro());
        }

        System.Collections.IEnumerator ShowDailyBonusHint_Coro()
        {
            gui.hud.dailyQuestHint.SetActive(true);
            yield return new WaitForSeconds(5);
            gui.hud.dailyQuestHint.SetActive(false);
        }
    }
}

