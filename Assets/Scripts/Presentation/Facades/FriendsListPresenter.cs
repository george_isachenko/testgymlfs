using System;
using System.Collections.Generic;
using BridgeBinder;
using Core.Network.Friends;
using Core.SocialAPI;
using InspectorHelpers;
using Logic.Facades;
using Presentation.Base;
using UnityEngine;
using View;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Friends List")]
    [BindBridgeInterface(typeof (FriendsListLogic))]
    public class FriendsListPresenter
        : BasePresentationFacade<FriendsListLogic>
    {
        #region Public fields.
        public Friend.Desc premadeCharacter;
        [ReorderableList(LabelsEnumType = typeof(Friend.Gender))]
        public Sprite[] defaultAvatars;
        #endregion

        #region Private properties.
        private FriendsListView view { get { return gui.friendsList; } }
        #endregion

        #region Private fields.
        private List<Friend> cachedFriends;
        private bool pendingLogin = false;
        private bool pendingFriendsListUpdate = false;
        //private bool initialSync = true;
        private Friend dummyFriend;
        #endregion

        [BindBridgeSubscribe]
        private void Subscribe(FriendsListLogic logic)
        {
            this.logic = logic;

            view.AddInviteButtonCallback(() =>
            {
                logic.Invite(NetworkType.Facebook);
                Core.PersistentCore.instance.analytics.GameEvent("Invite Friends",
                    new Core.Analytics.GameEventParam("Level", logic.playerProfile.level.ToString()),
                    new Core.Analytics.GameEventParam("From", "Friend List"));
            });

            view.AddConnectButtonCallback(() =>
            {
                if (logic.IsSocialNetworkLoggedIn(NetworkType.Facebook))
                {
                    UpdateView();
                }
                else
                {
                    ConnectToSocialNetwork();
                }
            });
        }

        protected void Awake()
        {
            view.OnShowHandler.AddListener(UpdateView);
            view.OnItemSelected_Callback += (item) =>
            {
                if(item.IsAddFriend())
                {
                    if(logic.IsSocialNetworkLoggedIn(NetworkType.Facebook))
                    {
                        logic.Invite(NetworkType.Facebook);
                        Core.PersistentCore.instance.analytics.GameEvent("Invite Friends",
                            new Core.Analytics.GameEventParam("Level", logic.playerProfile.level.ToString()),
                            new Core.Analytics.GameEventParam("From", "Friend List"));
                    }
                    else 
                        ConnectToSocialNetwork(); 
                }
                else
                {
                    var friend = item.idx == 0 ? dummyFriend : cachedFriends[item.idx - 1];
                    SwitchToFriendsGym(friend);
                }
            };
        }

        protected void Start()
        {
            dummyFriend = new Friend(premadeCharacter);

            var reward = Data.DataTables.instance.balanceData.Social.facebookConnectReward;
            view.rewardCounTxt = reward.ToString();
        }

        private void UpdateView()
        {
            view.UpdateItemsCount((cachedFriends != null ? cachedFriends.Count : 0) + 2); // +2 for Lina as 0 ellem, and +Friends as last ellem

            {   // Lina!
                var item = view.items[0];
                item.idx = 0;
                item.FriendAvatar = dummyFriend.avatar;
                item.FriendLevel = dummyFriend.level;
                item.FriendName = dummyFriend.name;
            }

            {   // Add Friends
                var item = view.items[view.items.Count - 1];
                item.idx = 0;
                item.SetupAddFriends();
            }

            view.connectBtnInteractable = !pendingLogin;

            if (!pendingFriendsListUpdate /*&& (cachedFriends == null || cachedFriends.Count == 0)*/)
            {
                logic.TryToGiveFacebookConnectBonus();

                if (logic.GotFacebookConnectBonus())
                    view.UseConnectText();
                else
                    view.UseConnectWithBonusText();

                view.SetInviteButtonActive(false);
                view.SetConnectButtonActive(false);

                pendingFriendsListUpdate = true;

                logic.GetFriendsList((friends) =>
                {
                    if (this != null && pendingFriendsListUpdate) // We can be dead already when callback finishes (because of game reloading to friend's save).
                    {
                        pendingFriendsListUpdate = false;
                        //initialSync = false;
                        cachedFriends = friends;

                        var fbLoggedIn = logic.IsSocialNetworkLoggedIn(NetworkType.Facebook);
                        // var usingFBOnServer = logic.IsUsingSocialNetworkOnServer(SocialNetwork.Facebook);

                        view.SetInviteButtonActive(fbLoggedIn /*&& usingFBOnServer*/);
                        view.SetConnectButtonActive(!fbLoggedIn /*|| !usingFBOnServer*/);

                        UpdateFriendsList();
                    }
                });
            }
            else
            {
                var fbLoggedIn = logic.IsSocialNetworkLoggedIn(NetworkType.Facebook);

                view.SetInviteButtonActive(!fbLoggedIn /*&& usingFBOnServer*/);
                view.SetConnectButtonActive(fbLoggedIn /*|| !usingFBOnServer*/);
            }
/*
            else
            {
                var fbLoggedIn = logic.IsSocialNetworkLoggedIn(SocialNetwork.Facebook);
                var usingFBOnServer = logic.IsUsingSocialNetworkOnServer(SocialNetwork.Facebook);

                view.SetInviteButtonActive(fbLoggedIn && usingFBOnServer && !initialSync && !pendingFriendsListUpdate);
                view.SetConnectButtonActive(!fbLoggedIn && !initialSync && !pendingFriendsListUpdate);

                UpdateFriendsList();
            }
*/
        }

        private void UpdateFriendsList ()
        {
            if (cachedFriends != null)
            {
                view.UpdateItemsCount((cachedFriends != null ? cachedFriends.Count : 0) + 2);

                for (var i = 0; i < cachedFriends.Count; i++)
                {
                    var item = view.items[i + 1];

                    item.SetupFriendView();
                    var info = cachedFriends[i];
                    info.onAvatarUpdated = CreateAvatarSetter(item);
                    item.idx = i + 1;
                    if (info.avatar == null)
                    {
                        item.FriendAvatar = defaultAvatars[(int)info.gender];
                    }
                    else
                    {
                        item.FriendAvatar = info.avatar;
                    }
                    item.FriendLevel = info.level;
                    item.FriendName = info.name;
                }

                var lastIitem = view.items[view.items.Count - 1];
                // Add Friends
                lastIitem.idx = 0;
                lastIitem.SetupAddFriends();
            }
                
        }

        private void SwitchToFriendsGym (Friend friend)
        {
            // this call must be delayed to clean up tutorial before switching to friend started
            logic.DelayedInvoke( () => logic.ShowFriend (friend) );
        }

        private void ConnectToSocialNetwork()
        {
            pendingLogin = true;
            view.connectBtnInteractable = false;

            logic.Connect (NetworkType.Facebook, (networkType, result) =>
                {
                    if (this != null && pendingLogin) // We can be dead already when callback finishes (because of game reloading to friend's save).
                    {
                        pendingLogin = false;
                        UpdateView();
                    }
                });
        }

        private static Action<Sprite> CreateAvatarSetter(FriendsListItem item)
        {
            return s =>
            {
                if (item != null)
                    item.FriendAvatar = s;
            };
        }
           
    }
}