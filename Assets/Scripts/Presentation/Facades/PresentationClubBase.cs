﻿using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core.Timer;
using Data;
using Data.Room;
using Logic.Rooms.Facade;
using Presentation.Base;
using Presentation.Facades.Interfaces.Club;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.PrefabResources;
using View.Rooms;
using Data.Estate;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(ILogicRoomManager))]
    public abstract class PresentationClubBase
        : BasePresentationFacade<ILogicRoomManager>
        , IPresentationClub
        , IClubInteractionProvider
    {
        #region Static/readonly data.
        public static readonly string roomsRootObjectName = "_Rooms";
        #endregion

        #region IClubInteractionProvider events.
        public event FloorPointerClickEvent onFloorPointerClick;
        public event FloorPointerDownEvent onFloorPointerDown;
        public event FloorPointerUpEvent onFloorPointerUp;

        public event Providers.RoomResizedEvent onRoomResizedEvent;
        public event Providers.RoomDeactivatedEvent onRoomDeactivatedEvent;
        public event Providers.RoomActivatedEvent onRoomActivatedEvent;
        #endregion

        #region IClubInteractionProvider properties.
        IDictionary<string, IRoomView> IClubInteractionProvider.rooms
        {
            get
            {
                return rooms;
            }
        }

        IRoomView IClubInteractionProvider.activeRoom
        {
            get
            {
                return _activeRoomName != string.Empty ? GetRoom(_activeRoomName) : null;
            }
        }

        string IClubInteractionProvider.activeRoomName
        {
            get
            {
                return _activeRoomName;
            }
        }

        bool IClubInteractionProvider.isInEditMode
        {
            get
            {
                return logic.isEditMode;
            }
        }
        #endregion

        #region Public (Inspector) data.
        public GameObject wallFloorTexturesPrefab;
        #endregion

        #region Private data.
        protected Dictionary<string, IRoomView> rooms;
        protected WallFloorTextures wallFloorTextures;
        protected string _activeRoomName = string.Empty;
        protected List<string> activeRoomCandidates = new List<string>(8);
        protected int wasStylePoints = -1;
        protected int wasFitPoints = -1;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected void Subscribe(ILogicRoomManager logic)
        {
            this.logic = logic;

            List<IRoomSetupDataInfo> roomsSetupDataInfo = new List<IRoomSetupDataInfo>(rooms.Count);

            Assert.IsNotNull(rooms);
            foreach (var room in rooms.Values)
            {
                if (room.roomConductor != null)
                {
                    roomsSetupDataInfo.Add(room.roomConductor.roomSetupDataInfo);

                    room.SetActiveState(false);
                }
            }

            logic.RegisterRooms(roomsSetupDataInfo);

            logic.onRoomActivatedEvent += OnRoomActivatedEvent;
            logic.onRoomDeactivatedEvent += OnRoomDeactivatedEvent;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(ILogicRoomManager logic)
        {
            logic.onRoomActivatedEvent -= OnRoomActivatedEvent;
            logic.onRoomDeactivatedEvent -= OnRoomDeactivatedEvent;

            this.logic = null;
        }
        #endregion

        #region Unity API.
        protected void Awake()
        {
            wallFloorTextures = wallFloorTexturesPrefab.GetComponent<WallFloorTextures>();

            var roomsList = InitRooms();
            Assert.IsNotNull(roomsList);

            rooms = new Dictionary<string, IRoomView>(roomsList.Length);

            foreach (var room in roomsList)
            {
                var roomName = room.roomConductor != null ? room.roomConductor.roomSetupDataInfo.name : room.roomObject.name;
                rooms.Add(roomName, room);

            }

#if DEBUG
            gui.dbgView.btnOneMinuteLeft.onClick.AddListener(() =>
            {
                var t = logic.GetUnlockTimer(logic.GetRoomNameOfType(RoomType.SportClub));
                if (t != null && t.secondsToFinish > 0)
                {
                    t.secondsToFinish = 60;
                }
            });
#endif
        }

        protected void Start()
        {
            equipmentInteractionProvider.onEquipmentStateChanged += OnEquipmentStateChanged;
            equipmentInteractionProvider.onEquipmentDestroyed += OnEquipmentDestroyed;

            foreach (var room in rooms.Values)
            {
                room.deactivateRoomRequest          += OnDeactivateRoomRequest;
                room.activateRoomRequest            += OnActivateRoomRequest;

                room.floorPointerClickEvent         += OnFloorPointerClick;
                room.floorPointerDownEvent          += OnFloorPointerDown;
                room.floorPointerUpEvent            += OnFloorPointerUp;
            }

            wasStylePoints = -1;
            wasFitPoints = -1;
        }

        protected void OnDestroy()
        {
            equipmentInteractionProvider.onEquipmentStateChanged -= OnEquipmentStateChanged;
            equipmentInteractionProvider.onEquipmentDestroyed -= OnEquipmentDestroyed;

            Assert.IsNotNull(rooms);
            foreach (var room in rooms.Values)
            {
                room.deactivateRoomRequest          -= OnDeactivateRoomRequest;
                room.activateRoomRequest            -= OnActivateRoomRequest;

                room.floorPointerClickEvent         -= OnFloorPointerClick;
                room.floorPointerDownEvent          -= OnFloorPointerDown;
                room.floorPointerUpEvent            -= OnFloorPointerUp;
            }
        }
        #endregion

        #region IPresentationClub interface.
        void IPresentationClub.SetSize(string roomName, int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            //Debug.Log("PresentationClub.SetSize:");
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                roomView.SetNewSize(expandLevel, newSize, unlockedBlocks, animate);

                onRoomResizedEvent?.Invoke(roomName, newSize);
            }
        }

        bool IPresentationClub.IsIdxFloor(string roomName, int idx)
        {
            // TODO: Multiple rooms support???

            if (wallFloorTextures.array.Length > idx)
            {
                WallFloorTextures.WallFloor wf = wallFloorTextures.array[idx];

                if (wf.floor != null)
                    return true;
            }

            return false;
        }

        bool IPresentationClub.IsIdxWall(string roomName, int idx)
        {
            // TODO: Multiple rooms support???

            if (wallFloorTextures.array.Length > idx)
            {
                WallFloorTextures.WallFloor wf = wallFloorTextures.array[idx];

                if (wf.wall != null)
                    return true;
            }

            return false;
        }

        void IPresentationClub.SetWall(string roomName, int idx)
        {
            if (rooms.ContainsKey(roomName))
            {
                if (wallFloorTextures.array.Length > idx)
                {
                    WallFloorTextures.WallFloor wf = wallFloorTextures.array[idx];
                    rooms[roomName].SetWallsSprite(wf.wall);
                }
            }
        }

        void IPresentationClub.SetFloor(string roomName, int idx)
        {
            if (rooms.ContainsKey(roomName))
            {
                if (wallFloorTextures.array.Length > idx)
                {
                    WallFloorTextures.WallFloor wf = wallFloorTextures.array[idx];
                    rooms[roomName].SetFloorSprite(wf.floor);
                }
            }
        }

        int IPresentationClub.GetRoomZonesCount (string roomName, string zoneNamePrefix)
        {
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                return roomView.GetRoomZonesCount(zoneNamePrefix);
            }
            else
            {
                return -1;
            }
        }

        public virtual void ShowGymUnlockMessage(string title, string message, string roomname, TimerFloat timer)
        {
        }

        public virtual void ShowGymUnlockMessage(string title, string message, string roomName, Cost cost)
        {
        }
        #endregion

        #region Public API.
        public IRoomView GetRoom(string roomName)
        {
            IRoomView result;
            rooms.TryGetValue(roomName, out result);
            return result;
        }

        public IRoomView GetRoomOfType(RoomType roomType)
        {
            return (from room in rooms.Values
                    where room.roomConductor.roomSetupDataInfo.roomType == roomType
                    select room).FirstOrDefault();
        }

        public bool TransitionToRoomRequest(string roomName)
        {
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                roomView.StartTransitionToRoom();
                return true;
            }

            return false;
        }

        public void UpdateStyle()
        {
            var roomName = logic.GetRoomNameOfType(RoomType.Club);

            if (roomName == _activeRoomName)
            {
                //gui.hud.styleVisible = playerProfileInteractionProvider.level >= logic.roomCapacity.GetFirstExpandLevel(roomName);
                if (logic.activeRoomName == roomName)
                {
                    int totalStylePoints = logic.roomCapacity.GetTotalStylePoints(roomName);

                    if (wasStylePoints >= 0 && wasStylePoints != totalStylePoints)
                    {
                        LeanTween.value
                            (gameObject
                            , (float value) =>
                            {
                                int stylePoints = logic.roomCapacity.GetNormalStylePoints(roomName, (int)value);
                                int stylePointsMax = logic.roomCapacity.GetNormalStylePointsMax(roomName, (int)value);

                                gui.hud.stylePoints = stylePoints.ToString() + "/" + stylePointsMax.ToString();

                                var styleRatio = (value - (int)(value) + (float)stylePoints) / (float)stylePointsMax;
                                gui.hud.styleProgress = styleRatio;

                                gui.hud.styleCapacity = logic.roomCapacity.GetStyleLevel((int)value);
                            }
                            , (float)wasStylePoints
                            , (float)totalStylePoints
                            , 1.0f);
                    }
                    else
                    {
                        gui.hud.styleCapacity = logic.roomCapacity.GetStyleLevel(logic.roomCapacity.GetTotalStylePoints(roomName));

                        var stylePoints = logic.roomCapacity.GetNormalStylePoints(roomName);
                        var stylePointsMax = logic.roomCapacity.GetNormalStylePointsMax(roomName);

                        var styleRatio = (float)stylePoints / (float)stylePointsMax;

                        //string dbgStr = "(sp:" + logic.roomCapacity.GetTotalStylePoints(roomName); //+ " e:" +  logic.roomCapacity.GetMaxStylePoints(roomName).ToString() + " c:" + logic.roomCapacity.GetStyleForNextVisitor(stylePoints, roomName).ToString() + ")";

                        gui.hud.stylePoints = stylePoints.ToString() + "/" + stylePointsMax.ToString();
                        gui.hud.styleProgress = styleRatio;

                    }

                    wasStylePoints = totalStylePoints;
                }
            }
            else
            {
                //gui.hud.styleVisible = false;
            }
        }

        public void UpdateFitPoints()
        {
            //gui.hud.fitPointsVisible = logic.activeRoomName == clubInteractionProvider.GetRoomOfType(RoomType.SportClub).roomConductor.name;
            var points = logic.GetPlayerFitPointsCount();
            if (wasFitPoints < 0)
            {
                wasFitPoints = points;
                gui.hud.fitPointsCount = points;
            }
            else 
            if (points != wasFitPoints)
            {
                LeanTween.value(gameObject,
                    v => { gui.hud.fitPointsCount = (int) v; },
                    (float) wasFitPoints,
                    (float) points,
                    1f);
                wasFitPoints = points;
            }
        }
        #endregion

        #region Bridge events.
        protected virtual void OnRoomDeactivatedEvent(string roomName)
        {
            Assert.IsFalse(roomName == string.Empty);

            if (_activeRoomName != string.Empty)
            {
                IRoomView roomView;
                if (rooms.TryGetValue(roomName, out roomView))
                {
                    _activeRoomName = string.Empty;
                    roomView.SetActiveState(false);

                    onRoomDeactivatedEvent?.Invoke(roomName);
                }

                UpdateStyle();
            }
        }

        protected virtual void OnRoomActivatedEvent(string roomName)
        {
            Assert.IsFalse(roomName == string.Empty);

            if (_activeRoomName != string.Empty && _activeRoomName != roomName)
            {
                Debug.LogWarningFormat("Trying to activate room while another room is still active (old room: {0}, new room: {1})!"
                    , _activeRoomName, roomName);
            }

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                _activeRoomName = roomName;
                roomView.SetActiveState(true);

                UpdateStyle();
                UpdateFitPoints();

                onRoomActivatedEvent?.Invoke(roomName);
            }
        }
        #endregion

        #region Protected static functions.
        protected static IRoomView[] InitRooms()
        {
            IRoomView[] rooms = null;
            var roomsRootObject = GameObject.Find(roomsRootObjectName);
            if (roomsRootObject != null)
            {
                var roomGameObjects = roomsRootObject.GetComponent<GameObjectsArray>();
                Assert.IsTrue(roomGameObjects != null && roomGameObjects.array.Length > 0);
                if (roomGameObjects != null && roomGameObjects.array.Length > 0)
                {
                    rooms = new IRoomView[roomGameObjects.array.Length];
                    for (int i = 0; i < roomGameObjects.array.Length; i++)
                    {
                        var iRoomView = roomGameObjects.array[i].GetComponent<IRoomView>();
                        Assert.IsNotNull(iRoomView);
                        rooms[i] = iRoomView;
                    }
                }
            }
            return rooms;
        }
        #endregion

        #region Rooms events handlers.
        private void OnActivateRoomRequest(string roomName)
        {
            if (activeRoomCandidates.Contains(roomName))
            {
                if (activeRoomCandidates.Count > 1)
                    activeRoomCandidates.Sort( (x, y) => (x == roomName) ? 1 : ((y == roomName) ? -1 : 0)); // Put it to the end of list.
            }
            else
            {
                activeRoomCandidates.Add(roomName);
            }

            if (_activeRoomName != roomName)
            {
                logic.ActivateRoomRequest(roomName);
            }
        }

        private void OnDeactivateRoomRequest(string roomName)
        {
            if (activeRoomCandidates.Contains(roomName))
            {
                activeRoomCandidates.Remove(roomName);
            }

            if (_activeRoomName == roomName)
            {
                logic.DeactivateRoomRequest(roomName);
            }

            if (activeRoomCandidates.Count > 0)
            {
                logic.ActivateRoomRequest(activeRoomCandidates[activeRoomCandidates.Count - 1]);
            }
        }

        private void OnFloorPointerClick(PointerEventData pointerEventData, string roomName)
        {
            onFloorPointerClick?.Invoke(pointerEventData, roomName);
        }

        private void OnFloorPointerDown(PointerEventData pointerEventData, string roomName)
        {
            onFloorPointerDown?.Invoke(pointerEventData, roomName);
        }

        private void OnFloorPointerUp(PointerEventData pointerEventData, string roomName)
        {
            onFloorPointerUp?.Invoke(pointerEventData, roomName);
        }
        #endregion

        #region IEquipmentInteractionProvider events handlers.
        void OnEquipmentDestroyed(int equipmentId)
        {
            UpdateStyle();
        }

        void OnEquipmentStateChanged(int equipmentId, EquipmentState oldState, EquipmentState newState)
        {
            UpdateStyle();
        }
        #endregion
    }
}