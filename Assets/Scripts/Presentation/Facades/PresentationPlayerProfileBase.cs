﻿using BridgeBinder;
using Core;
using Core.Timer;
using Data;
using Data.PlayerProfile;
using Logic.PlayerProfile;
using Logic.PlayerProfile.Facade;
using Presentation.Base;
using Presentation.Facades.Interfaces.PlayerProfile;
using Presentation.Providers;
using Core.Analytics;
using UnityEngine;
using Presentation.Helpers;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(ILogicPlayerProfile))]
    public abstract class PresentationPlayerProfileBase
        : BasePresentationFacade<ILogicPlayerProfile>
        , IPresentationPlayerProfile
        , IPlayerProfileInteractionProvider
    {
        #region IPlayerProfileInteractionProvider events.
        public event Providers.LevelUpEvent  onLevelUp;
        #endregion

        #region IPlayerProfileInteractionProvider properties.
        int IPlayerProfileInteractionProvider.coins
        {
            get
            {
                return logic.coins;
            }
        }

        int IPlayerProfileInteractionProvider.fitBucks
        {
            get
            {
                return logic.fitBucks;
            }
        }

        int IPlayerProfileInteractionProvider.level
        {
            get
            {
                return logic.level;
            }
        }

        Cost IPlayerProfileInteractionProvider.levelUpReward
        {
            get
            {
                return logic.levelUpReward;
            }
        }

        int IPlayerProfileInteractionProvider.experience
        {
            get
            {
                return logic.experience;
            }
        }

        TimerFloat IPlayerProfileInteractionProvider.fatigue
        {
            get
            {
                return logic.fatigue;
            }
        }

        public virtual LogicStorage storage
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region Public (Inspector) data.
        #endregion

        #region Private data.
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected void Subscribe(ILogicPlayerProfile playerProfile)
        {
            logic = playerProfile;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(ILogicPlayerProfile playerProfile)
        {
            logic = playerProfile;
        }
        #endregion

        #region Bridge events.
        [BindBridgeEvent("onGameWillRestartEvent")]
        void OnWillRestartGame (GameMode mode, DemoLevelSource demoLevelSource)
        {
            gui.windows.ShowDefault();
        }
        #endregion

        #region Unity API.
        protected void Start()
        {
#if DEBUG
            gui.dbgView.profileLvl.OnIncAction = () =>
            {
                logic.DebugIncrementLevel();
            };

            gui.dbgView.profileLvl.OnDecAction = () =>
            {
                logic.DebugDecrementLevel();
            };

            gui.dbgView.fatigue.OnDecAction = () =>
            {
                logic.DebugDecreaseFatigue();
            };

            gui.dbgView.fatigue.OnIncAction = () =>
            {
                logic.DebugIncreaseFatigue();
            };

            gui.dbgView.resources.OnIncAction = () =>
            {
                logic.DebugIncreasResource((ResourceType)gui.dbgView.resourceTypeSelector.value);
            };

            gui.dbgView.resources.OnDecAction = () =>
            {
                logic.DebugDecreaseResource((ResourceType)gui.dbgView.resourceTypeSelector.value);
            };

            gui.dbgView.btnGetExpToNexLevel.onClick.AddListener(logic.DebugAddExperienceToNextLevel);
            gui.dbgView.btnPushTest.onClick.AddListener(logic.PushTest);

            gui.dbgView.btnZeroMoney.onClick.AddListener(logic.DebugZeroMoney);

            gui.dbgView.onAddMoneyEvent += logic.DebugMoarMoney;

            gui.dbgView.btnRateUs.onClick.AddListener(delegate{gui.promo.Show();});

            gui.dbgView.onPerformSaveGameDelete += () =>
            {
                logic.DebugDeleteDataAndStop();
            };
#endif

            logic.onLevelUpEvent += OnLevelUp;
        }

        protected void OnDestroy()
        {
            if (logic != null)
                logic.onLevelUpEvent -= OnLevelUp;

#if DEBUG
            gui.dbgView.onAddMoneyEvent -= logic.DebugMoarMoney;
#endif

        }
        #endregion

        #region IPlayerProfileInteractionProvider interface.
        bool IPlayerProfileInteractionProvider.SpendMoney(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary)
        {
            return logic.SpendMoney(cost, info, spendBucksIfNecessary);
        }

        bool IPlayerProfileInteractionProvider.CanSpend(Cost cost, bool forceFitBucks)
        {
            return logic.CanSpend(cost, forceFitBucks);
        }

        Cost IPlayerProfileInteractionProvider.GetShortageCost (Cost cost)
        {
            return logic.GetShortageCost(cost);
        }

        public virtual void WarnStorageIsFull(bool showWarningFirst)
        {
        }
        #endregion

        #region Private functions.
        private void OnLevelUp ()
        {
            onLevelUp?.Invoke();
        }
        #endregion
    }
}