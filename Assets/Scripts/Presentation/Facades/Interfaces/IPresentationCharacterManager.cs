﻿using System;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.Room;
using Logic.Persons;

namespace Presentation.Facades.Interfaces.CharacterManager
{
    public delegate void CharacterPositionUpdated(int id, RoomCoordinates position, float heading);

    public interface IPresentationCharacterManager
    {
        event CharacterPositionUpdated onCharacterPositionUpdated;

        void CreateCharacterRequest(int id, IPersonInfo personInfo, CharacterDestination spawnDestination);
        void DestroyCharacterRequest(int id);
        void MoveCharacterRequest(int id, CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo, Action<int, MovementResult> onComplete);
        void StopCharacterRequest(int id);
        void ChangeCharacterMovementModeRequest(int id, MovementMode mode);
        void TransferCharacterRequest(int id, string newRoomName);
        void StartExerciseRequest(int id, int equipmentId, int exerciseTier, int exerciseAnimation, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed);
        void SwitchExerciseRequest (int id, int tier, int characterAnimation, float speed);
        void EndExerciseRequest(int id, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags);
        void PlayEmoteAnimationRequest(int id, AnimationSocial animationId, int loopCount, bool rotateTowardsCamera, Action onComplete);
        void PersonLookAtRequest(int id, CharacterDestination lookAtDestination, Action onComplete);
        void UpdateOverheadUI(int id);
    }
}