﻿using Core.Timer;
using Data;
using Data.Room;

namespace Presentation.Facades.Interfaces.Club
{
    public interface IPresentationClub
    {
        bool TransitionToRoomRequest(string roomName);

        void SetSize(string roomName, int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate);
        void SetWall(string roomName, int idx);
        void SetFloor(string roomName, int idx);

        bool IsIdxFloor(string roomName, int idx);
        bool IsIdxWall(string roomName, int idx);
        void UpdateStyle();

        void ShowGymUnlockMessage(string title, string message, string roomname, TimerFloat timer);
        void ShowGymUnlockMessage(string title, string message, string roomName, Cost cost);
        void UpdateFitPoints();

        int GetRoomZonesCount (string roomName, string zoneNamePrefix);
    }
} 