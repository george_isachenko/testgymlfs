﻿using Data.Estate;
using System;
using System.Collections.Generic;
using Data.Estate.Static;
using Data.Estate.Info;
using Data.Room;

namespace Presentation.Facades.Interfaces.EquipmentManager
{
    public delegate void EquipmentPlacementUpdated(int id, EquipmentPlacement placement);

    public interface IPresentationEquipmentManager
    {
        event EquipmentPlacementUpdated onEquipmentPlacementUpdated;

        void CreateEquipmentRequest(int id, IEquipmentInfo equipmentInfo);
        void BindStaticEquipmentRequest(int id, StaticEquipmentID staticEquipmentId, IEquipmentInfo equipmentInfo);
        void DestroyEquipmentRequest(int id);
        void EquipmentStartExerciseRequest (int id, int exerciseAnimation, float speed);
        void EquipmentSwitchExerciseRequest (int id, int exerciseAnimation, float speed);
        void EquipmentEndExerciseRequest (int id);
        bool EquipmentStartPreviewModeRequest (IEquipmentInfo equipmentInfo, Action<EquipmentPlacement?> onPreviewComplete);
        void EquipmentEndPreviewModeRequest ();
        IEnumerable<KeyValuePair<StaticEquipmentID, StaticEquipmentInfo>> GetStaticEquipmentList();  // Static ID => Estate type map.
        void ShowStyleHintOverheadUIIcon (int id, string text);
    }
} 