﻿using System;
using UnityEngine;
using BridgeBinder;
using Logic.Facades;
using View.UI;
using Data.Person;
using View.UI.OverheadUI;
using UnityEngine.Assertions;
using View.UI.OverheadUI.Data;
using Data.Room;
using Logic.Persons;
using View.UI.OverheadUI.Components;
using Presentation.Base;
using Data;
using View.UI.Base;
using Logic.Persons.Events;
using Logic.SportAgency.Facade;
using Logic;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sport Agency")]
    [BindBridgeInterface(typeof (LogicSportAgency))]
    public class PresentationSportAgency
        : BasePresentationFacade<LogicSportAgency>
    {
        public static readonly string sportAgencyTag = "SportAgency";
        private static readonly string iconAttachmentObjectName = "icon_attachment";

        public SingleActionIcon.Holder overheadIcon;

        SportsmenAgencyView view;

        [BindBridgeSubscribe]
        void Subscribe(LogicSportAgency logicAgency)
        {
            this.logic = logicAgency;

            logic.onRoomSizeUpdated += OnRoomUpdated;
            logic.onLevelUp += OnLevelUp;
        }

        void Start()
        {
            if (overheadIcon != null)
            {
                var cityObject = GameObject.FindGameObjectWithTag (sportAgencyTag);
                Assert.IsNotNull(cityObject);
                if (cityObject != null)
                {
                    var iconAttachment = cityObject.transform.Find(iconAttachmentObjectName);
                    if (iconAttachment != null)
                    {
                        overheadIcon.Instantiate();
                        overheadIcon.instance.onClick.AddListener (OnOverheadUIClick);
                        overheadIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(iconAttachment.position);
                        overheadIcon.icon.owner = this;

                        var overheadUIData = new OverheadUIDataBuildings();
                        overheadUIData.state = OverheadUIDataBuildings.State.SportAgency;
                        overheadUIData.ApplyData (overheadIcon.instance);
                    }
                }
            }
            else
            {
                Debug.LogWarningFormat("{0}: Missing Overhead UI Icon prefab.", GetType().Name);
            }

            view = gui.sportsmenAgency;
            
            view.OnUpdateContentOnShow_Callback = UpdateView;
            view.OnItemSelected_Callback = OnSportsmanSelected;
            view.onTabSwitchedCallback += (a)=>UpdateView();
            view.onShowByCityClick += OnShowByCityClick;
            view.updateTimer += UpdateTimer;
            view.OnHide_Callback += DropSelection;

            view.OnShow_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event += SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event += CharacterBirthEventOnEvent;
            };
            view.OnHide_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event -= SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event -= CharacterBirthEventOnEvent;
            };

            UpdateOverheadUI();
        }

        public void DropSelection()
        {
            view.SetSelectedItem(null, false);
            logic.CleanUpSellSlots();
        }

        private void CharacterBirthEventOnEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                UpdateView();
        }

        private void SportsmanTypeChangedEventOnEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            UpdateView();
        }

        void OnRoomUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupData)
        {
            if (roomSetupData.roomType == RoomType.SportClub)
            {
                UpdateOverheadUI();
            }
        }

        private void OnLevelUp(int level)
        {
            UpdateOverheadUI();
        }

        void UpdateTimer()
        {
            view.textTime = View.PrintHelper.GetTimeString(logic.timerBuySlots.timeSpan);

            if (logic.timerBuySlots.secondsToFinish <= 0)
            {
                logic.RefreshBuySlots();
                UpdateView();
            }
        }

        void OnShowByCityClick()
        {
            if (logic.isClubInEditMode)
                return;

            if (logic.isAnyTutorialRunning && !logic.isSportAgencyTutorialRunning)
                return;

            var locker = logic.canUseMyFeature;
            switch (locker.type)
            {
                case CanUseFeatureResult.ResultType.DependencyLocked:
                    //gui.confirmWnd.OnCustom(Loc.Get("sportAgencyUnlockHeader"), Loc.Get("sportAgencyUnlockMsg"));
                    gui.sportAgencyLockedView.Show();
                    gui.sportAgencyLockedView.description = Loc.Get("sportAgencyUnlockMsg");
                    break;
                case CanUseFeatureResult.ResultType.LockedNeededLevel:
                    //gui.confirmWnd.OnCustom(Loc.Get("sportAgencyUnlockHeader"), Loc.Get("sportAgencyUnlockLVL", locker.level));
                    gui.sportAgencyLockedView.Show();
                    gui.sportAgencyLockedView.description = Loc.Get("sportAgencyUnlockLVL", locker.level);
                    break;
                case CanUseFeatureResult.ResultType.CanUse:
                    view.Show();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void UpdateView()
        {
            if (view.activeTab == 0)
            {
                UpdateSportsmen();
                UpdateSlotsSell();
            }
            else
            {
                DropSelection();
                UpdateSlotsBuy();
            }
        }


        private void UpdateSportsmen()
        {
            var sportList = logic.sportsmen.GetSportsmenTypes(false);

            view.UpdateItemsCount(sportList.Count);
            var viewItems = view.items;

            var counter = 0;
            foreach (var item in sportList)
            {
                var viewItem = viewItems[counter];

                viewItem.idx = counter;
                var textFormat = item.isTraining ? "<color=#0080B0FF>x{0}</color>" : "x{0}";
                viewItem.text = string.Format(textFormat, item.count);
                viewItem.sportType = item.sportsmanType;
                viewItem.showTimeIcon = item.isTraining;
                ++counter;
            }
        }

        void UpdateSlotsBuy()
        {
            view.genericView.IsolateSubView("buy");
            var viewList = view.slotsBuy;

            var items = logic.slotsBuy;
            viewList.UpdateItemsCount(items.Count);

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var viewItem = viewList.items[i];

                viewItem.icon = item.type;
                viewItem.text = item.name;
                viewItem.cost = item.cost;
                viewItem.isSold = item.isSold;
                viewItem.idx = i;
                viewItem.count = logic.sportsmen.GetSportsmanCount(item.type,null,PersonState.Entering, PersonState.Idle, /*PersonState.InQueue,*/ PersonState.ExerciseComplete);

                viewItem.OnClickCallback = OnBuyClick;
                viewItem.TimeOut_Callback = logic.BuyTimeOut;
            }
        }

        void UpdateSlotsSell()
        {
            view.genericView.IsolateSubView("sell");

            var items = logic.slotsSell;
            var count = logic.slotsSellAvailable;
            view.slotsSell.UpdateItemsCount(count);
            var viewItems = view.slotsSell.items;
            for (int i = 0; i < count; i++)
            {
                var item = items[i];
                var viewItem = viewItems[i];

                viewItem.unlockCost = logic.unlockSlotCost;
                viewItem.idx = i;
                viewItem.state = item.state;
                viewItem.sportType = item.sportsmanType;
                viewItem.price = logic.SportsmanPrice(item.sportsmanType);
                viewItem.timer = item.timer;

                viewItem.btnSellOn = (item.timer == null && !item.complete);
                viewItem.btnClaimOn = (item.complete);
                viewItem.Sell_Callback = logic.Sell_Click;
                viewItem.TimeOut_Callback = logic.SellTimeOut;
                viewItem.Claim_Callback = logic.SellClaim_Click;
                viewItem.Unlock_Callback = logic.UnlockSell_Click;
            }
        }

        public void OnSportsmanSelected(UIBaseViewListItem viewItem)
        {
            var item = viewItem as SportsmenViewItem;

            logic.SendSportsmanToSlot(item.sportType);

            UpdateView();
        }

        void OnBuyClick(UIBaseViewListItem viewItem)
        {
            logic.Buy_Click(viewItem.idx);
            UpdateView();
        }

        public void CantAddSportsmanMsg()
        {
            gui.confirmWnd.OnCantAddSportsmen("idCancel");
            gui.confirmWnd.OnConfirm = logic.sportsmen.ShowExpandMemebersLimit;
        }

        void UpdateOverheadUI()
        {
            var isAvailable = logic.canUseMyFeature.canUse;
            if (overheadIcon != null && overheadIcon.icon != null)
            {
                overheadIcon.icon.visible = logic.canUseMyFeature.canUse;
            }
        }

        void OnOverheadUIClick()
        {
            OnShowByCityClick();
        }

        protected override UIBaseView GetView()
        {
            return view;
        }

    }
}

