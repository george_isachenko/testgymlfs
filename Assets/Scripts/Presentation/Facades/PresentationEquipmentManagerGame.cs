﻿using System;
using System.Collections;
using BridgeBinder;
using Core;
using CustomAssets;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Estate.Upgrades;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Estate.Facade;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Actors;
using View.Actors.Events;
using View.Actors.Helpers;
using View.CameraHelpers;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;
using View.UI.OverheadUI.Data;
using CustomAssets.Lists;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Equipment Manager Game")]
    [BindBridgeInterface(typeof(ILogicEquipmentManager))]
    public class PresentationEquipmentManagerGame : PresentationEquipmentManagerBase
    {
        #region IEquipmentActorSharedDataProvider properties.
        public override EquipmentMovementMarker movementMarker
        {
            get
            {
                return movementMarkerInstance;
            }
        }
        #endregion

        #region Public (Inspector) fields.
        [Header("Equipment editing")]
        public LayerMask equipmentEditingRaycastMask;
        public LayerMask equipmentMovementRaycastMask;
        public string equipmentMovementLayerName;
        public EquipmentMovementMarker movementMarkerPrefab;
        [Tooltip("Time to prepare to editing equipment when pressing on it (show grid and spinner), in seconds.")]
        public float startPrepareEditTime = 0.8f;
        [Tooltip("Time to start editing equipment when pressing on it, in seconds from 'Start Prepare Edit Time'.")]
        public float startEditTime = 0.8f;
        public int editModeDragThreshold = 5;

        [Header("Grid settings")]
        public Color gridHiddenColor;
        public Color gridActiveColor;
        public float gridAnimationTime;

        [Header("Overhead UI")]
        public OverheadIconInstanceHolderTransform spinningWheelOverheadIcon = new OverheadIconInstanceHolderTransform();
        public EquipmentPlacementIcon.Holder equipmentPlacementIcon = new EquipmentPlacementIcon.Holder();

        [Header("Upgrades")]
        public GameObjectsList previewPrefabsList;
        #endregion

        #region Private data.
        protected IEquipmentActor selectedActor;
        protected IEquipmentActor pendingEquipment;   // Pending unlocking (existing) or new buying (temporary) equipment.
        protected Action<EquipmentPlacement?> onEquipmentPreviewComplete;
        protected IEquipmentActor editingEquipment;
        protected bool movingEquipment;
        protected Vector3 editingStartDelta;
        protected LayerMask originalRaycasterMask;
        protected int equipmentMovementLayerIdx = -1;
        protected int editingEquipmentOriginalLayer;
        protected int standardDragThreshold = -1;
        protected EquipmentMovementMarker movementMarkerInstance;
        protected IEnumerator editPreparationCoroutine = null;
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            if (equipmentMovementLayerName != null && equipmentMovementLayerName != string.Empty)
                equipmentMovementLayerIdx = LayerMask.NameToLayer(equipmentMovementLayerName);

            if (movementMarkerPrefab != null)
            {
                movementMarkerInstance = Instantiate(movementMarkerPrefab);
                movementMarkerInstance.gameObject.SetActive(false);
                movementMarkerInstance.transform.SetParent(transform, false);
            }

            // Setup us as click handler on floor collider.
            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onRoomResizedEvent += OnRoomResized;
                clubInteractionProvider.onFloorPointerClick += OnFloorPointerClick;
            }

            // Init exercise selector panel.
            gui.hud.exerciseSelectorPanel.exercise1SelectHandler += OnExerciseSelectorClick0;
            gui.hud.exerciseSelectorPanel.exercise2SelectHandler += OnExerciseSelectorClick1;
            gui.hud.exerciseSelectorPanel.exercise3SelectHandler += OnExerciseSelectorClick2;
            gui.hud.exerciseSelectorPanel.upgradeButtonHandler += OnExerciseSelectorUpgradeEquipmentClick;

            gui.upgradeEquipment.upgradeButtonHandler += OnUpgradeEquipmentClick;

            onEquipmentSelected += OnEquipmentSelected_HUDUpdate;
            onEquipmentDeselected += OnEquipmentDeselected_HUDUpdate;
        }

        protected new void Start()
        {
            base.Start();

            gui.hud.skipPanel.Hide();
            gui.hud.exerciseSelectorPanel.Hide();
            gui.hud.repairPanel.Hide();

            cameraController = Camera.main.GetComponent<CameraController>();
            Assert.IsNotNull(cameraController);
            if (cameraController != null)
            {
                cameraController.onCameraMoveEvent += OnCameraMove;
                cameraController.onCameraDualTouchEvent += OnCameraDualTouch;
            }

            spinningWheelOverheadIcon.Instantiate();
            equipmentPlacementIcon.Instantiate();

            gui.windows.OnWindowOpened_Callback += OnWindowOpened;

            //PlayerProfile.LevelUpEvent.Event += OnLevelUp;
            playerProfileInteractionProvider.onLevelUp += OnLevelUp;

            Debug.Log("PRESENTATION EQUIP: onRewardedVideoAvailable");
            Core.Advertisement.Advertisements.instance.onRewardedVideoAvailable += UpdateOverheadUIForTV;
            Core.Advertisement.Advertisements.instance.UpdateAdverticementEnabled_Callback += UpdateOverheadUIForTV;
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            // Very important, they can have callbacks into our code.
            gui.hud.skipPanel.Hide();
            gui.hud.exerciseSelectorPanel.Hide();
            gui.hud.repairPanel.Hide();

            //PlayerProfile.LevelUpEvent.Event -= OnLevelUp;

            if (Core.Advertisement.Advertisements.instance != null)
            {
                Core.Advertisement.Advertisements.instance.onRewardedVideoAvailable -= UpdateOverheadUIForTV;
                Core.Advertisement.Advertisements.instance.UpdateAdverticementEnabled_Callback -= UpdateOverheadUIForTV;
            }

            if (gui.windows != null)
            {
                gui.windows.OnWindowOpened_Callback -= OnWindowOpened;
            }

            if (playerProfileInteractionProvider != null)
            {
                playerProfileInteractionProvider.onLevelUp -= OnLevelUp;
            }

            if (Camera.main != null)
            {
                cameraController = Camera.main.GetComponent<CameraController>();
                if (cameraController != null)
                {
                    cameraController.onCameraMoveEvent -= OnCameraMove;
                    cameraController.onCameraDualTouchEvent -= OnCameraDualTouch;
                }
            }

            onEquipmentSelected -= OnEquipmentSelected_HUDUpdate;
            onEquipmentDeselected -= OnEquipmentDeselected_HUDUpdate;

            if (gui != null)
            {
                if (gui.upgradeEquipment != null)
                {
                    gui.upgradeEquipment.upgradeButtonHandler -= OnUpgradeEquipmentClick;
                }

                if (gui.hud != null)
                {
                    gui.hud.exerciseSelectorPanel.exercise1SelectHandler -= OnExerciseSelectorClick0;
                    gui.hud.exerciseSelectorPanel.exercise2SelectHandler -= OnExerciseSelectorClick1;
                    gui.hud.exerciseSelectorPanel.exercise3SelectHandler -= OnExerciseSelectorClick2;
                    gui.hud.exerciseSelectorPanel.upgradeButtonHandler -= OnExerciseSelectorUpgradeEquipmentClick;
                }
            }

            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onFloorPointerClick -= OnFloorPointerClick;
                clubInteractionProvider.onRoomResizedEvent -= OnRoomResized;
            }

            if (standardDragThreshold > 0 && EventSystem.current != null)
            {
                EventSystem.current.pixelDragThreshold = standardDragThreshold;
                standardDragThreshold = -1;
            }
        }
        #endregion

        #region UnityEditor API.
#if UNITY_EDITOR
        protected new void OnValidate()
        {
            base.OnValidate();

            if (startPrepareEditTime <= 0)
            {
                startPrepareEditTime = 1.0f;
            }

            if (startEditTime <= 0)
            {
                startEditTime = 1.0f;
            }
        }
#endif
        #endregion

        #region IPresentationEquipmentManager interface.
        public override void DestroyEquipmentRequest(int id)
        {
            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (!actor.equipmentInfo.isStatic)
                {
                    if (selectedActor == actor)
                        selectedActor = null;
                }
            }

            base.DestroyEquipmentRequest(id);
        }

        public override bool EquipmentStartPreviewModeRequest (IEquipmentInfo equipmentInfo, Action<EquipmentPlacement?> onPreviewComplete)
        {
            Assert.IsNotNull(clubInteractionProvider);
            Assert.IsNotNull(equipmentInfo);
            Assert.IsNull(pendingEquipment);

            if (equipmentInfo.id == EquipmentData.InvalidId)
            {   // Preview new item.
                var equipmentPrefab = FindPrefab(equipmentInfo);
                // Assert.IsNotNull(equipmentPrefab);
                if (equipmentPrefab != null)
                {
                    var actorObject = Instantiate(equipmentPrefab);
                    Assert.IsNotNull(actorObject);

                    if (actorObject != null)
                    {
                        var actor = actorObject.GetComponent<IEquipmentActor>();
                        if (actor != null)
                        {
                            var room = clubInteractionProvider.GetRoom(equipmentInfo.roomName);
                            Assert.IsNotNull(room);

                            actorObject.name = string.Format(nameFormatString, EquipmentData.InvalidId, equipmentInfo.estateType.locNameId.Trim(nameTrimChars));

                            //SetActorEventHandlers(actor, true, false);

                            actor.Init
                                ( this
                                , EquipmentData.InvalidId
                                , equipmentInfo
                                , room.roomConductor
                                , room.actorsDisplayMode
                                , CreateCachedStaticMeshImpostorForSkinnedEquipment(actor)
                                , null
                                , null
                                , actorSettings);

                            EquipmentPlacement placement = actor.wallMounted
                                ? room.roomConductor.FindFreeSpaceOnWall(actor.size, actor.wallSize, actor.wallSizeWithMargin, actor.occupiesFloorSpace)
                                : room.FindFreeSpaceForEquipment(actor.size);

                            if (placement.position.isValid)
                            {
                                actor.SetPlacement(placement);

                                pendingEquipment = actor;
                                SetEditMode(actor, false);

                                onEquipmentPreviewComplete = onPreviewComplete;
                                return true;
                            }
                            else
                            {
                                actor.Destroy();

                                gui.confirmWnd.OnCustom(Loc.Get("EquipmentPlacementNoFreeSpaceHeader"), Loc.Get("EquipmentPlacementNoFreeSpaceMessage"));
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogWarningFormat("Unsupported equipment type '{0}'.", equipmentInfo.estateType.itemType);
                    return false;
                }
            }
            else
            {   // Unlock existing sport equipment item.
                IEquipmentActor actor;
                if (actors.TryGetValue(equipmentInfo.id, out actor))
                {
                    pendingEquipment = actor;
                    SetEditMode(actor, false);

                    onEquipmentPreviewComplete = onPreviewComplete;
                    return true;
                }
                else
                {
                    Debug.LogWarningFormat("Cannot find existing equipment actor to unlock '{0}'.", equipmentInfo.id);
                    return false;
                }
            }

            return false;
        }

        public override void EquipmentEndPreviewModeRequest ()
        {
            Assert.IsNotNull(pendingEquipment);
            if (pendingEquipment != null)
            {
                SetEditMode(null, false);

                if (pendingEquipment.id == EquipmentData.InvalidId)
                {   // Remove temporary actor when previewing new item but left existing unlocking item.
                    pendingEquipment.Destroy();
                }

                pendingEquipment = editingEquipment = null;
            }

            onEquipmentPreviewComplete = null;
        }
        #endregion

        #region Bridge events handlers.
        [BindBridgeEvent("SetSelectionEvent")]
        private void OnSetSelectionEvent(int id, bool selected)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (selected)
                {
                    Assert.IsNull(editingEquipment);
                    Assert.IsNull(pendingEquipment);

                    selectedActor = actor;
                    selectedActor.SetSelection(true);

                    UpdateOverheadUI(selectedActor);

                    // TODO: Extract to separate function!
                    if (actor.equipmentInfo.estateType.itemType == EstateType.Subtype.Sport &&
                        !actor.equipmentInfo.isOccupied)
                    {
                        if (actor.equipmentInfo.isInDelivery)
                        {
                            cameraController.AutoMoveTo(actor.position.x, actor.position.z);
                        }
                        else
                        {
                            var cameraController = Camera.main.GetComponent<CameraController>();
                            if (cameraController != null)
                            {
                                Vector3 offset = cameraController.GetWorldOffsetForNormalizedScreenPoint (gui.hud.focusPointLeft025.anchorMin);
                                cameraController.AutoMoveTo(actor.position.x + offset.x, actor.position.z + offset.z);
                            }
                            else
                            {
                                cameraController.FocusCameraOnWorldPosition(actor.position);
                            }
                        }
                    }
                    else
                    {
                        cameraController.FocusCameraOnWorldPosition(actor.position);
                    }

                    //--- закрываем окно выбора визиторов при открытии селектора упражнений
                    if (!actor.equipmentInfo.isOccupied)
                        gui.windows.ShowDefault ();

                    SendEquipmentSelectedEvent(id, actor.equipmentInfo);

                    //--- сворачиваем плашку визитора
                    //gui.hud.visitorPanel.Fold();

                } 
                else
                {
                    actor.SetSelection(false);

                    if (selectedActor != null && selectedActor == actor)
                    {
                        UpdateOverheadUI(selectedActor);
                        selectedActor = null;
                    }

                    SendEquipmentDeselectedEvent(id);

                    //--- разворачиваем плашку визитора если была свернута привыборе тренажера
                    //gui.hud.visitorPanel.UnFold();
                }
            }
        }

        [BindBridgeEvent("SetEditModeEvent")]
        private void OnSetEditModeEvent(int id, bool canSellOrStore)
        {
            Assert.IsTrue(id == EquipmentData.InvalidId || pendingEquipment == null);

            IEquipmentActor editingActor = null;
            if (id != EquipmentData.InvalidId && actors.TryGetValue(id, out editingActor))
            {
            }

            SetEditMode(editingActor, canSellOrStore);
        }

        [BindBridgeEvent("EquipmentAssemblyCompleteEvent")]
        void OnEquipmentAssemblyCompleteEvent(int id, bool skipped)
        {
            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(actor.gameObject, null, (target, eventData) => target.OnAssemblyComplete());

                if (skipped)
                {
                    logic.SelectEquipmentRequest(EquipmentData.InvalidId);
                    logic.ConfirmAssemblyCompleteRequest(id);
                    if (actor.selectable)
                        logic.SelectEquipmentRequest(id);
                }
            }
        }

        [BindBridgeEvent("EquipmentAssemblyCompleteConfirmedEvent")]
        void OnEquipmentAssemblyCompleteConfirmedEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId);

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(actor.gameObject, null, (target, eventData) => target.OnAssemblyCompleteConfirmed());
            }
        }

        [BindBridgeEvent("EquipmentStartRepairEvent")]
        void OnEquipmentStartRepairEvent(int id)
        {
            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ShowSkipPanel(id, actor.equipmentInfo, OnOverheadUIClickSkipRepair, Loc.Get("idRepair"), true);
            }
        }
        #endregion

        #region IEquipmentInteractionProvider interface.
        public override void SelectEquipmentRequest(int equipmentId, bool allowDeselectionIfSelected)
        {
            ProcessEquipmentSelectionRequest(equipmentId, allowDeselectionIfSelected);
        }

        public override void DeselectEquipmentRequest()
        {
            logic.SelectEquipmentRequest(EquipmentData.InvalidId);
        }

        public override void DeselectEquipmentIfSelectedRequest(int equipmentId)
        {
            var selectedId = logic.selectedEquipmentId;
            if (selectedId != EquipmentData.InvalidId && selectedId == equipmentId)
            {
                logic.SelectEquipmentRequest(EquipmentData.InvalidId);
            }
        }
        #endregion

        #region Room floor event handlers.
        private void OnFloorPointerClick(PointerEventData eventData, string roomName)
        {
            if (logic != null)
            {
                logic.SelectEquipmentRequest(EquipmentData.InvalidId); // Deselect equipment.
            }
        }
        #endregion

        #region Overhead UI event handlers.
        void OnOverheadUIClickSkipAssembly(int actorId)
        {
            logic.SkipAssemblyRequest(actorId);
        }

        void OnOverheadUIClickSkipRepair(int actorId)
        {
            logic.SkipRepairEquipmentRequest(actorId);
        }

        void OnOverheadUIClickConfirmAssembly(int actorId)
        {
            logic.ConfirmAssemblyCompleteRequest(actorId);
        }

        void OnOverheadUIClickBroken(int actorId)
        {
            ProcessEquipmentSelectionRequest(actorId);
        }

        void OnOverheadUIClickLocked(int actorId)
        {
            logic.UnlockEquipmentRequest(actorId);
        }

        void OnOverheadUIClickWatchVideoAd(int actorId)
        {
            shopInteractionProvider.ShowRewardedAd(OnRewardedAdShowComplete);
        }

        void OnOverheadIconEditEquipmentCancel(int actorId)
        {
            Assert.IsNotNull(editingEquipment);

            if (editingEquipment != null)
            {
                if (pendingEquipment != null)
                {
                    var tmpCallback = onEquipmentPreviewComplete;
                    onEquipmentPreviewComplete = null;
                    SetEditMode(null, false);
                    if (tmpCallback != null)
                        tmpCallback(null);
                }
                else
                {
                    editingEquipment.ResetPlacement();
                    FinishEditMode();
                }

                //gui.hud.btnBack.onClick.Invoke ();
            }
        }

        void OnOverheadIconEditEquipmentRotate(int actorId)
        {
            Assert.IsNotNull(editingEquipment);

            if (editingEquipment != null)
            {
                if (!editingEquipment.wallMounted)
                {
                    var placement = editingEquipment.placement;
                    placement.heading += 1;
                    placement.position = editingEquipment.roomConductor.coordsMapper.AlignPositionToRoomCoordinatesRounded
                        ( editingEquipment.localPosition, editingEquipment.size.GetOrientedSize(placement.heading)
                        , placement.GetShiftRotated(editingEquipment.shift));

                    editingEquipment.SetPlacement (placement);
                    equipmentPlacementIcon.instance.confirmActive = editingEquipment.placementState.isValid;
                }
            }
        }

        void OnOverheadIconEditEquipmentConfirm(int actorId)
        {
            Assert.IsNotNull(editingEquipment);

            if (editingEquipment != null)
            {
                if (pendingEquipment != null)
                {
                    var tmpCallback = onEquipmentPreviewComplete;
                    onEquipmentPreviewComplete = null;

                    // Confirm should be already disable in UI if placement is not
                    // valid. So, it's just an additional safety feature: if not valid - treat it as cancel.
                    var validPlacement = pendingEquipment.placementState.isValid;
                    SetEditMode(null, false);
                    if (tmpCallback != null)
                    {
                        if (pendingEquipment.placementState.isValid)
                            tmpCallback(pendingEquipment.placement);
                        else
                            tmpCallback(null);
                    }
                }
                else
                {
                    FinishEditMode();
                }

                //gui.hud.btnBack.onClick.Invoke ();
            }
        }

        void OnOverheadIconEditEquipmentSell(int actorId)
        {
            Assert.IsNotNull(editingEquipment);

            if (editingEquipment != null)
            {
                var price = logic.GetSellPrice (logic.editingEquipmentId);

                if (price != null && price.type != Cost.CostType.None)
                {
                    gui.confirmWnd.OnSellConfirm (price, Loc.Get("ConfirmSellEquipmentHeader"), Loc.Get("ConfirmSellEquipmentMessage")); 
                    gui.confirmWnd.OnConfirm = OnSellConfirm;
                    gui.confirmWnd.OnCancel = () => { gui.confirmWnd.Hide(); FinishEditMode(); };
                }
            }
        }

        void OnExerciseSelectorClick0 ()
        {
            if (selectedActor != null)
            {
                logic.UseEquipmentRequest (selectedActor.id, characterInteractionProvider.selectedCharacterId, 0);
            }
        }

        void OnExerciseSelectorClick1 ()
        {
            if (selectedActor != null)
            {
                logic.UseEquipmentRequest (selectedActor.id, characterInteractionProvider.selectedCharacterId, 1);
            }
        }

        void OnExerciseSelectorClick2 ()
        {
            if (selectedActor != null)
            {
                logic.UseEquipmentRequest (selectedActor.id, characterInteractionProvider.selectedCharacterId, 2);
            }
        }

        #endregion

        #region Equipment actors event handlers.
        void OnEquipmentClick(int actorId, PointerEventData eventData)
        {
            // Debug.LogFormat("OnEquipmentClick: actorId: {0}, eventData: {1}", actorId, eventData);

            ProcessEquipmentSelectionRequest(actorId);
        }

        void OnEquipmentPointerDown(int actorId, PointerEventData eventData)
        {
            if (editPreparationCoroutine == null && editingEquipment == null)
            {
                IEquipmentActor actor;
                if (actors.TryGetValue(actorId, out actor) && logic.IsEditableEquipment(actor.id) && cameraController.GetTouchesCount() == 1)
                {
                    editPreparationCoroutine = PrepareToEditJob(actor.id, eventData);
                    StartCoroutine(editPreparationCoroutine);
                }
            }
        }

        void OnEquipmentPointerUp(int actorId, PointerEventData eventData)
        {
            CancelEditPreparation();
        }
        #endregion

        #region Protected virtual API.
        protected override void UpdateOverheadUI(IEquipmentActor actor)
        {
            var data = actor.equipmentInfo;
            var actorId = actor.id; // Fetched, so we will not grab whole actor into delegates in anonymous functions.

            actor.RemoveOverheadUIIcon();

            if (data.estateType.itemType == EstateType.Subtype.AdvertisingTV)
            {
                if (shopInteractionProvider.IsRewardedVideoAvailableForTV())
                {
                    var equipmentOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(equipmentOverheadIconPrefab);
                    var overheadUIData = new OverheadUIDataEquipment { state = OverheadUIDataEquipment.State.VideoAd };
                    overheadUIData.ApplyData(equipmentOverheadIcon.instance);
                    equipmentOverheadIcon.instance.onClick.AddListener(delegate()
                        {
                            OnOverheadUIClickWatchVideoAd(actorId);
                        });
                    actor.SetOverheadUIIcon(equipmentOverheadIcon);
                }
                else
                {
                    actor.RemoveOverheadUIIcon();
                }
            }
            else
            {
                switch (data.state)
                {
/*
                    case EquipmentState.Idle:
                    {
                    }
                    break;

                    case EquipmentState.ShopMode:
                    break;

                    case EquipmentState.WorkPending:
                    {
                    }; break;

                    case EquipmentState.Working:
                    {
                    }
                    break;

                    case EquipmentState.Waiting:
                    break;

                    case EquipmentState.Assembling:
                    break;
*/

                    case EquipmentState.AssemblyComplete:
                    {
                        var equipmentOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(equipmentOverheadIconPrefab);
                        var overheadUIData = new OverheadUIDataEquipment { state = OverheadUIDataEquipment.State.Complete };
                        overheadUIData.ApplyData(equipmentOverheadIcon.instance);
                        equipmentOverheadIcon.instance.onClick.AddListener(delegate() { OnOverheadUIClickConfirmAssembly(actorId); } );
                        actor.SetOverheadUIIcon(equipmentOverheadIcon);
                    }; break;

                    case EquipmentState.Broken:
                    {
                        var equipmentOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(equipmentOverheadIconPrefab);
                        var overheadUIData = new OverheadUIDataEquipment { state = OverheadUIDataEquipment.State.Broken };
                        overheadUIData.ApplyData(equipmentOverheadIcon.instance);
                        equipmentOverheadIcon.instance.onClick.AddListener(delegate() { OnOverheadUIClickBroken(actorId); } );
                        actor.SetOverheadUIIcon(equipmentOverheadIcon);
                    }; break;

                    case EquipmentState.Locked:
                    {
/*
                        var equipmentOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(equipmentOverheadIconPrefab);
                        var overheadUIData = new OverheadUIDataEquipment { state = OverheadUIDataEquipment.State.Mastering };
                        equipmentOverheadIcon.instance.ApplyData(overheadUIData);
                        equipmentOverheadIcon.instance.onClick.AddListener(delegate() { OnOverheadUIClickLocked(actorId); } );
                        actor.SetOverheadUIIcon(equipmentOverheadIcon);
*/
                    }; break;
                }
            }
        }

        protected override void SetActorEventHandlers
            (IEquipmentActor actor, bool editingMode, bool allowPlacing)
        {
            actor.pointerDownEvent.RemoveAllListeners ();
            actor.pointerUpEvent.RemoveAllListeners ();
            actor.pointerClickEvent.RemoveAllListeners ();
            actor.dragEvent.RemoveAllListeners();

            if (editingMode)
            {
                if (allowPlacing)
                {
                    actor.pointerDownEvent.AddListener ( OnPlaceEquipmentPointerDown );
                    actor.pointerUpEvent.AddListener ( OnPlaceEquipmentPointerUp );
                    actor.dragEvent.AddListener ( OnPlaceEquipmentDrag );
                }
                else
                {
                    actor.pointerDownEvent.AddListener ( OnEquipmentPointerDown );
                    actor.pointerUpEvent.AddListener ( OnEquipmentPointerUp );
                    actor.pointerClickEvent.AddListener ( OnEquipmentClick );
                }
            }
            else
            {
                actor.pointerDownEvent.AddListener ( OnEquipmentPointerDown );
                actor.pointerUpEvent.AddListener ( OnEquipmentPointerUp );
                actor.pointerClickEvent.AddListener ( OnEquipmentClick );
            }
        }

        #endregion

        #region Private functions.
        void ProcessEquipmentSelectionRequest(int equipmentActorId, bool allowDeselectIfSelected = true)
        {
            if (gui.windows.GetCurrentType () == WindowTypes.PopUp)
            {
                return;
            }

            if (pendingEquipment == null &&
                editPreparationCoroutine == null &&
                logic != null)
            {
                if (selectedActor != null && equipmentActorId == selectedActor.id && allowDeselectIfSelected)
                {
                    logic.SelectEquipmentRequest(EquipmentData.InvalidId);
                }
                else if (selectedActor == null || equipmentActorId != selectedActor.id)
                {
                    IEquipmentActor actor;
                    if (actors.TryGetValue(equipmentActorId, out actor))
                    {
                        if (actor.equipmentInfo.state == EquipmentState.WorkPending)
                        {
                            var equipmentOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(equipmentOverheadIconPrefab);
                            var overheadUIData = new OverheadUIDataEquipment { state = OverheadUIDataEquipment.State.Busy };
                            overheadUIData.ApplyData(equipmentOverheadIcon.instance);
                            equipmentOverheadIcon.instance.interactable = false;
                            equipmentOverheadIcon.icon.autoHideDelay = 1.0f;
                            actor.SetOverheadUIIcon(equipmentOverheadIcon);
                        }
                        else if (actor.selectable)
                        {
                            logic.SelectEquipmentRequest(equipmentActorId);
                        }
                    }
                }
            }
        }

        IEnumerator PrepareToEditJob (int editCandidateId, PointerEventData eventData)
        {
            yield return new WaitForSeconds(startPrepareEditTime);
            
            if (editCandidateId != EquipmentData.InvalidId && logic.canEditEquip)
            {
                // Check for actor presence and state again - it can be gone or change its state while waiting.
                IEquipmentActor actor;
                if (actors.TryGetValue(editCandidateId, out actor) && logic.IsEditableEquipment(actor.id))
                {
                    var activeRoom = clubInteractionProvider.activeRoom;
                    Assert.IsNotNull(activeRoom);

                    if (activeRoom != null)
                    {
                        activeRoom.roomConductor.AnimateGrid(gridHiddenColor, gridActiveColor, gridAnimationTime);

                        spinningWheelOverheadIcon.icon.owner = this;
                        spinningWheelOverheadIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(actor.position);
                        spinningWheelOverheadIcon.icon.visible = true;

                        actor = null; // Do not keep it while waiting.

                        yield return new WaitForSeconds(startEditTime);

                        if (logic.EditEquipmentRequest(editCandidateId))
                        {
                            OnPlaceEquipmentPointerDown(editCandidateId, eventData); // Go directly to movement mode.
                        }
                    }
                }
            }

            spinningWheelOverheadIcon.icon.visible = false;

            editPreparationCoroutine = null;
            yield break;
        }

        void CancelEditPreparation()
        {
            if (editPreparationCoroutine != null)
            {
                StopCoroutine(editPreparationCoroutine);
                editPreparationCoroutine = null;

                spinningWheelOverheadIcon.icon.visible = false;

                var activeRoom = clubInteractionProvider.activeRoom;
                Assert.IsNotNull(activeRoom);
                if (activeRoom != null)
                {
                    if (activeRoom.roomConductor.gridActive)
                        activeRoom.roomConductor.AnimateGrid(activeRoom.roomConductor.gridColor, gridHiddenColor, gridAnimationTime);
                }
            }
        }

        void OnRewardedAdShowComplete(bool result, Cost reward)
        {
            if (result && reward != null)
            {
                shopInteractionProvider.ConfirmAdReward(reward);
            }

            UpdateOverheadUIForTV();
        }

        void UpdateOverheadUIForTV()
        {
//            Debug.Log("UPDATE TV");

            foreach (var actor in actors.Values)
            {
                if (actor.equipmentInfo.estateType.itemType == EstateType.Subtype.AdvertisingTV)
                {
                    UpdateOverheadUI(actor);
                }
            }
        }

        void OnRewardedVideoAvailable()
        {
            UpdateOverheadUIForTV();
        }

        void OnSellConfirm()
        {
            logic.SellEditedEquipment ();
        }

        void OnPlaceEquipmentPointerDown(int actorId, PointerEventData eventData)
        {
            if (!movingEquipment && editingEquipment != null && eventData.pointerPressRaycast.isValid)
            {
                if (eventData.pointerPressRaycast.module is PhysicsRaycaster)
                {
                    var raycaster = eventData.pointerPressRaycast.module as PhysicsRaycaster;
                    raycaster.eventMask = equipmentMovementRaycastMask;
                }

                if (editingEquipmentOriginalLayer >= 0)
                {
                    editingEquipmentOriginalLayer = editingEquipment.layer;
                    editingEquipment.layer = equipmentMovementLayerIdx;
                }

                movingEquipment = true;

                var currentOverheadUIIcon = editingEquipment.overheadUIIcon;
                if (currentOverheadUIIcon != null)
                {
                    currentOverheadUIIcon.visible = false;
                    currentOverheadUIIcon.worldPositionProvider.allowUpdate = true;
                }

                editingEquipment.useDragThreshold = true;

                editingStartDelta = eventData.pointerPressRaycast.worldPosition - editingEquipment.position;

                SetDragEquipmentMode(true);
            }
        }

        void OnPlaceEquipmentPointerUp(int actorId, PointerEventData eventData)
        {
            if (movingEquipment && editingEquipment != null)
            {
                if (eventData.pointerPressRaycast.module is PhysicsRaycaster)
                {
                    var raycaster = eventData.pointerPressRaycast.module as PhysicsRaycaster;
                    raycaster.eventMask = equipmentEditingRaycastMask;
                }

                if (editingEquipmentOriginalLayer >= 0)
                {
                    editingEquipment.layer = editingEquipmentOriginalLayer;
                }

                movingEquipment = false;

                var currentOverheadUIIcon = editingEquipment.overheadUIIcon;
                if (currentOverheadUIIcon != null)
                {
                    currentOverheadUIIcon.visible = true;
                    currentOverheadUIIcon.worldPositionProvider.allowUpdate = false;
                }

                SetDragEquipmentMode(false);
            }

            //IEquipmentActor actor;
            //if (actors.TryGetValue(actorId, out actor))
            if(editingEquipment != null)
                cameraController.FocusCameraOnWorldPosition(editingEquipment.position);
        }

        void OnPlaceEquipmentDrag(int actorId, PointerEventData eventData)
        {
            if (movingEquipment && editingEquipment != null)
            {
                if (eventData.pointerCurrentRaycast.isValid)
                {
                    var cellSize = editingEquipment.roomConductor.coordsMapper.cellSize;
                    var originalPlacement = editingEquipment.placement;
                    var currentPosition = editingEquipment.localPosition;
                    var proposedPosition = (eventData.pointerCurrentRaycast.worldPosition - editingEquipment.roomConductor.roomObject.transform.position) - editingStartDelta;

                    if (Math.Abs(proposedPosition.x - currentPosition.x) >= cellSize + Mathf.Epsilon || Math.Abs(proposedPosition.z - currentPosition.z) >= cellSize + Mathf.Epsilon)
                    {
                        var proposedPositionCoords = editingEquipment.roomConductor.coordsMapper.AlignPositionToRoomCoordinatesRounded
                            (proposedPosition, editingEquipment.orientedSize, editingEquipment.shiftRotated);

                        var resultingPlacement = originalPlacement;

                        var valid = editingEquipment.roomConductor.ClampToContraints
                            ( proposedPositionCoords
                            , editingEquipment.size
                            , editingEquipment.wallMounted
                            , out resultingPlacement.position
                            , ref resultingPlacement.heading);

                        if (!valid)
                        {   // Try proposed position but with keeping previous X coordinate.
                            var proposedPositionCoordsKeepX = new RoomCoordinates(originalPlacement.position.x, proposedPositionCoords.y);
                            resultingPlacement.heading = originalPlacement.heading; // Reset back.
                            valid = editingEquipment.roomConductor.ClampToContraints
                                ( proposedPositionCoordsKeepX
                                , editingEquipment.size
                                , editingEquipment.wallMounted
                                , out resultingPlacement.position
                                , ref resultingPlacement.heading);

                            if (!valid)
                            {   // Now also try proposed position but with keeping previous Y coordinate.
                                var proposedPositionCoordsKeepY = new RoomCoordinates(proposedPositionCoords.x, originalPlacement.position.y);
                                resultingPlacement.heading = originalPlacement.heading; // Reset back.
                                valid = editingEquipment.roomConductor.ClampToContraints
                                    ( proposedPositionCoordsKeepY
                                    , editingEquipment.size
                                    , editingEquipment.wallMounted
                                    , out resultingPlacement.position
                                    , ref resultingPlacement.heading);
                            }
                        }

                        if (valid)
                        {
                            editingEquipment.SetPlacement (resultingPlacement);
                            equipmentPlacementIcon.instance.confirmActive = editingEquipment.placementState.isValid;
                        }
                    }
                }
            }
        }

        void OnRoomResized(string roomName, RoomCoordinates newSize)
        {
            foreach (var actor in actors)
            {
                var iEquipmentActor = actor.Value;
                if (iEquipmentActor.equipmentInfo.roomName == roomName && iEquipmentActor.wallMounted)
                {
                    if (iEquipmentActor.placement.heading == 2)
                    {
                        var placement = iEquipmentActor.placement;
                        placement.position.y  = newSize.y - iEquipmentActor.orientedSize.y;
                        iEquipmentActor.SetPlacement(placement);
                    }
                    else if (iEquipmentActor.placement.heading == 3)
                    {
                        var placement = iEquipmentActor.placement;
                        placement.position.x  = newSize.x - iEquipmentActor.orientedSize.x;
                        iEquipmentActor.SetPlacement(placement);
                    }
                }
            }
        }

        void SetEditMode (IEquipmentActor editingCandidate, bool canSellOrStore)
        {
            if (editingCandidate != null) // On
            {
                if (editingEquipment != editingCandidate)
                {
                    if (gui.hud.exerciseSelectorPanel.isShown)
                        gui.hud.exerciseSelectorPanel.Hide();
                    if (gui.hud.repairPanel.isShown)
                        gui.hud.repairPanel.Hide();

                    if (editingEquipment != null)
                    {
                        editingEquipment.SetEditMode(false, false);
                        if (ReferenceEquals(editingEquipment.overheadUIIcon, equipmentPlacementIcon.icon))
                        {
                            editingEquipment.RemoveOverheadUIIcon();
                        }
                        SetActorEventHandlers(editingEquipment, false, false);
                        editingEquipment = null;
                    }
                    else
                    {
                        if (standardDragThreshold <= 0 && EventSystem.current != null)
                        {
                            standardDragThreshold = EventSystem.current.pixelDragThreshold;
                            EventSystem.current.pixelDragThreshold = editModeDragThreshold;
                        }

                        foreach (var actor in actors.Values)
                        {
                            if (editingCandidate != actor)
                            {
                                actor.SetEditMode(true, false);
                            }
                        }

                        var raycaster = Camera.main.gameObject.GetComponent<PhysicsRaycaster>();
                        if (raycaster != null)
                        {
                            originalRaycasterMask = raycaster.eventMask;
                            raycaster.eventMask = equipmentEditingRaycastMask;
                        }

                        gui.windows.states.inEditMode = true;
                        gui.hud.ShowBackButton(OnFinishEditMode);
                        gui.hud.ShowMoney ();
                    }

                    editingCandidate.SetEditMode
                        ( true
                        , true);

                    var editingCandidateId = editingCandidate.id;

                    equipmentPlacementIcon.instance.onCancelClick.RemoveAllListeners();
                    equipmentPlacementIcon.instance.onCancelClick.AddListener(delegate() { OnOverheadIconEditEquipmentCancel(editingCandidateId); });
                    equipmentPlacementIcon.instance.onConfirmClick.RemoveAllListeners();
                    equipmentPlacementIcon.instance.onConfirmClick.AddListener(delegate() { OnOverheadIconEditEquipmentConfirm(editingCandidateId); });
                    equipmentPlacementIcon.instance.onRotateClick.RemoveAllListeners();
                    equipmentPlacementIcon.instance.onRotateClick.AddListener(delegate() { OnOverheadIconEditEquipmentRotate(editingCandidateId); });
                    equipmentPlacementIcon.instance.onSellClick.RemoveAllListeners();
                    equipmentPlacementIcon.instance.onSellClick.AddListener(delegate() { OnOverheadIconEditEquipmentSell(editingCandidateId); });

                    var overheadUIDataEquipmentPlacement = new OverheadUIDataEquipmentPlacement
                            ( pendingEquipment != null
                                ? OverheadUIDataEquipmentPlacement.Mode.InitialPlacement
                                : (canSellOrStore ? OverheadUIDataEquipmentPlacement.Mode.MovementSellAllowed : OverheadUIDataEquipmentPlacement.Mode.MovementNoSellOrStore )
                            , editingCandidate.editable && !editingCandidate.wallMounted
                            , true);
                    overheadUIDataEquipmentPlacement.ApplyData(equipmentPlacementIcon.instance);
                    //equipmentPlacementIcon.instance.rotateEnabled = editingCandidate.editable;
                    equipmentPlacementIcon.icon.worldPositionProvider = editingCandidate.defaultWorldPositionProvider;
                    equipmentPlacementIcon.icon.worldPositionProvider.allowUpdate = true;
                    equipmentPlacementIcon.icon.worldPositionProvider.allowUpdate = false; // Temporary hack to force update position.
                    equipmentPlacementIcon.instance.confirmActive = editingCandidate.placementState.isValid;
                    editingCandidate.SetOverheadUIIcon(equipmentPlacementIcon);

                    OverheadUIManager.instance.SetDefaultDisplayMode(false);
                    OverheadUIManager.instance.ClearLayerExceptions();
                    OverheadUIManager.instance.AddSortingLayerException(equipmentPlacementIcon.icon.sortingLayer);

                    //cameraController.FocusCameraOnActor(editingCandidate);

                    cameraController.AutoMoveTo(editingCandidate.position.x, editingCandidate.position.z);

                    if (editingCandidate.editable)
                    {
                        editingCandidate.roomConductor.SetGridColor(gridActiveColor);
                    }

                    SetActorEventHandlers(editingCandidate, true, editingCandidate.editable);

                    editingEquipment = editingCandidate;

                    //SetDragEquipmentMode(true);
                }

            }
            else if (editingEquipment != null) // Off
            {
                editingEquipment.roomConductor.SetGridColor(gridHiddenColor);

                if (movementMarkerInstance != null)
                {
                    movementMarkerInstance.gameObject.SetActive(false);
                    movementMarkerInstance.transform.SetParent(transform, false);
                }

                foreach (var actor in actors.Values)
                {
                    actor.SetEditMode(false, false);
                }

                if (pendingEquipment != null)
                {
                    if (ReferenceEquals(pendingEquipment.overheadUIIcon, equipmentPlacementIcon.icon))
                    {
                        pendingEquipment.RemoveOverheadUIIcon();
                    }
                    SetActorEventHandlers(pendingEquipment, false, pendingEquipment.editable);
                    pendingEquipment.SetEditMode(false, false);
                }
                else
                {
                    if (ReferenceEquals(editingEquipment.overheadUIIcon, equipmentPlacementIcon.icon))
                    {
                        editingEquipment.RemoveOverheadUIIcon();
                        UpdateOverheadUI(editingEquipment);
                    }
                    SetActorEventHandlers(editingEquipment, false, editingEquipment.editable);
                }

                OverheadUIManager.instance.ClearLayerExceptions();
                OverheadUIManager.instance.SetDefaultDisplayMode(true);

                editingEquipment = null;

                SetDragEquipmentMode(false);

                var raycaster = Camera.main.gameObject.GetComponent<PhysicsRaycaster>();
                if (raycaster != null)
                {
                    raycaster.eventMask = originalRaycasterMask;
                }

                gui.windows.states.inEditMode = false;
                gui.hud.HideBackButton ();

                if (standardDragThreshold > 0 && EventSystem.current !=  null)
                {
                    EventSystem.current.pixelDragThreshold = standardDragThreshold;
                    standardDragThreshold = -1;
                }
            }
        }

        void OnFinishEditMode()
        {
            if (editingEquipment != null)
            {   // TODO: Refactor all this call chains.
                OnOverheadIconEditEquipmentCancel(editingEquipment.id);
            }
        }

        void FinishEditMode()
        {
            logic.EditEquipmentRequest(EquipmentData.InvalidId);
        }

        void SetDragEquipmentMode(bool enabled)
        {
            // TODO: Make it more civilized and centralized way, probably Club manager should also manage camera and its state.
            var camCtrl = Camera.main.GetComponent<CameraController>();
            if (camCtrl != null)
            {
                camCtrl.states.dragEquip = enabled;
            }
        }

        void ShowSkipPanel(int id, IEquipmentInfo equipmentInfo, Action<int> callback, string actionName, bool animatedButton = false)
        {
            gui.hud.skipPanel.Display
                ( TimeSpan.FromSeconds(equipmentInfo.assemblyTotalTime)
                , equipmentInfo.assemblyEndTime
                , DynamicPrice.GetSkipCost
                , () =>
                {
                    callback(id);
                }
                , () => (logic.selectedEquipmentId != id)
                , null
                , actionName );

            if(animatedButton)
                gui.hud.skipPanel.ShowButtonWithAnimation();
        }

        void OnWindowOpened(bool isModal, bool hideOverheadUI)
        {
            if (isModal || hideOverheadUI)
                logic.SelectEquipmentRequest(EquipmentData.InvalidId);
        }

        void OnEquipmentSelected_HUDUpdate(int id, IEquipmentInfo equipmentInfo)
        {
            Assert.IsNotNull(equipmentInfo);

            if (editingEquipment != null && editingEquipment.id == id)
                return;

            switch (equipmentInfo.state)
            {
                case EquipmentState.Idle:
                {
                    if (equipmentInfo.estateType.itemType == EstateType.Subtype.Training || equipmentInfo.estateType.itemType == EstateType.Subtype.ActiveDecor) // TODO: ActiveDecor case is temporary...
                    {
                        var currentEstateType = equipmentInfo.estateType;

                        EquipmentUpgradeInfo currentUpgradeInfo, nextUpgradeInfo;
                        var state = logic.GetEquipmentUpgradeInfo (id, out currentUpgradeInfo, out nextUpgradeInfo);

                        if (equipmentInfo.estateType.itemType == EstateType.Subtype.Training)
                        {
                            bool activeMode = false;
                            if (characterInteractionProvider.selectedCharacterId != PersonData.InvalidId)
                            {
                                var characterActor = characterInteractionProvider.GetCharacterActor(characterInteractionProvider.selectedCharacterId);
                                Assert.IsNotNull(characterActor);
                                if (characterActor != null && characterActor.personInfo.isPlayerControllable && characterActor.personInfo.canBeTrainedByPlayer)
                                {
                                    activeMode = true;
                                }
                            }

                            gui.hud.exerciseSelectorPanel.UpdateIcons(currentEstateType);
                            gui.hud.exerciseSelectorPanel.SetActiveMode(activeMode);
                            gui.hud.exerciseSelectorPanel.SetUpgradeStates(state, equipmentInfo.upgradeLevel, nextUpgradeInfo != null ? nextUpgradeInfo.unlockLevel : 0);
                            gui.hud.exerciseSelectorPanel.Show();
                        }
                    }
                }; break;

                case EquipmentState.Repairing:
                {
                    ShowSkipPanel(id, equipmentInfo, OnOverheadUIClickSkipRepair, Loc.Get("idRepair"));
                }; break;

                case EquipmentState.Assembling:
                {
                    ShowSkipPanel(id, equipmentInfo, OnOverheadUIClickSkipAssembly, Loc.Get("idAssembling"));
                };  break;

                case EquipmentState.Broken:
                {
                    gui.hud.repairPanel.SetData
                        ( logic.repairCost
                        , logic.repairResourcesAvailable
                        , delegate()
                            {
                                logic.RepairEquipmentRequest(id);
                            }
                        );
                    gui.hud.repairPanel.Show();
                }; break;
            }
        }

        void OnEquipmentDeselected_HUDUpdate(int id)
        {
            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.equipmentInfo.estateType.itemType == EstateType.Subtype.Training)
                {
                    if (gui.hud.exerciseSelectorPanel.isShown)
                        gui.hud.exerciseSelectorPanel.Hide();

                    if (gui.hud.repairPanel.isShown)
                        gui.hud.repairPanel.Hide();
                }
            }
        }

        void OnCameraMove(float moveDist, bool overThreshold)
        {
            if (overThreshold)
                CancelEditPreparation();
        }

        void OnCameraDualTouch()
        {
            CancelEditPreparation();
        }

        private void OnLevelUp()
        {
            UpdateOverheadUIForTV();
        }
    
        private void OnExerciseSelectorUpgradeEquipmentClick ()
        {
            IEquipmentActor actor;

            int id = logic.selectedEquipmentId;

            if (id != EquipmentData.InvalidId && actors.TryGetValue(id, out actor))
            {
                var currentEstateType = actor.equipmentInfo.estateType;

                EquipmentUpgradeInfo currentUpgradeInfo, nextUpgradeInfo;
                var state = logic.GetEquipmentUpgradeInfo (id, out currentUpgradeInfo, out nextUpgradeInfo);
    
                ResourceType upgradeResourceType = nextUpgradeInfo != null
                    ? nextUpgradeInfo.resourceType
                    : (currentUpgradeInfo != null) ? currentUpgradeInfo.resourceType : ResourceType.None;

                var previewPrefab = (previewPrefabsList != null && currentEstateType.id < previewPrefabsList.array.Length) ? previewPrefabsList.array[currentEstateType.id] : null;

                if (previewPrefab == null)
                {
                    Debug.LogWarningFormat("Missing preview prefab for Estate type {0} ('{1}').", currentEstateType.id, Loc.Get(currentEstateType.locNameId));
                }

                gui.upgradeEquipment.Show
                    ( id
                    , previewPrefab
                    , null
                    , Loc.Get(currentEstateType.locNameId)
                    , state
                    , actor.equipmentInfo.upgradeLevel
                    , upgradeResourceType
                    , (upgradeResourceType != ResourceType.None) ? playerProfileInteractionProvider.storage.GetResourcesCount(upgradeResourceType) : 0
                    , currentUpgradeInfo
                    , nextUpgradeInfo );
            }
        }

        private void OnUpgradeEquipmentClick (int upgradeCandidateId)
        {
            if (upgradeCandidateId != EquipmentData.InvalidId)
            {
                EquipmentUpgradeInfo currentUpgradeInfo, nextUpgradeInfo;
                var state = logic.GetEquipmentUpgradeInfo (upgradeCandidateId, out currentUpgradeInfo, out nextUpgradeInfo);

                if (state == UpgradeAvailabilityState.EquipmentAvailable && nextUpgradeInfo != null)
                {
                    var cost = nextUpgradeInfo.cost;
                    if (playerProfileInteractionProvider.CanSpend(cost, false))
                    {
                        logic.UpgradeEquipment(upgradeCandidateId, false);
                    }
                    else
                    {
                        var costShortage = playerProfileInteractionProvider.GetShortageCost(cost);
                        Assert.IsNotNull(costShortage);
                        if (costShortage != null)
                        {
                            //var bucksCost = playerProfileInteractionProvider.storage.GetExtraBucksPrice(cost, null).value;

                            gui.confirmWithBucks.OnNeedResources2
                                ( costShortage
                                , playerProfileInteractionProvider.storage.GetExtraBucksPrice(cost, null).value
                                , false
                                , true );

                            gui.confirmWithBucks.OnOpenShop = () =>
                            {
                                gui.shopUltimate.Show();
                                gui.shopUltimate.ShowBankBucks();
                                Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "UpgradeEquipmentClick");
                            };

                            gui.confirmWithBucks.OnConfirm = () =>
                            {
                                logic.UpgradeEquipment(upgradeCandidateId, true);
                            };
                        }
                    }
                }
            }
        }
        #endregion
    }
}
