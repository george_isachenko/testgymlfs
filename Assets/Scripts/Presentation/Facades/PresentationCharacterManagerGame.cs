﻿using BridgeBinder;
using Core;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.Quests;
using Data.Quests.Visitor;
using Data.Room;
using InspectorHelpers;
using Logic.Persons;
using Logic.Persons.Events;
using Logic.Quests.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Actors;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;
using View.UI.OverheadUI.Data;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Character Manager Game")]
    public class PresentationCharacterManagerGame : PresentationCharacterManagerBase
    {
        #region Inspector fields.
        //------------------------------------------------------------------------
        [Header("Characters setup settings")]

        [Tooltip("Time (in seconds) after which to hide 'Angry' Overhead UI icons after refusing exercise.")]
        [SerializeField]
        protected float exerciseRefusedAngryIconDisplayTime = 2.0f;

        [SerializeField]
        protected float exerciseStartHappyIconDisplayTime = 2.0f;

        [SerializeField]
        protected float readyToSkipSwitchingLagTime = 4.0f;

        //------------------------------------------------------------------------
        [Header("Selection cursor settings")]

        [ReorderableList(LabelsEnumType = typeof(CharacterCursorType))]
        [SerializeField]
        protected GameObject[] cursorPrefabs;

        [SerializeField]
        protected GameObject moveDestinationCursorPrefab;

        //------------------------------------------------------------------------
        [Header("Overhead UI")]

        [SerializeField]
        protected JuicerOrderIcon.Holder juicerOrderIcon = new JuicerOrderIcon.Holder();
        #endregion

        #region Private data.
        private GameObject[] instancedCursors;
        private GameObject moveDestinationCursorInstance;
        private readonly List<ICharacterActor> readyToSkipActors = new List<ICharacterActor>(32);
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            if (cursorPrefabs != null)
            {
                instancedCursors = new GameObject[cursorPrefabs.Length];
                for (int i = 0; i < cursorPrefabs.Length; i++)
                {
                    if (cursorPrefabs[i] != null)
                    {
                        instancedCursors[i] = Instantiate(cursorPrefabs[i]);
                        instancedCursors[i].transform.SetParent(transform, false);
                        instancedCursors[i].SetActive(false);
                    }
                }
            }

            if (moveDestinationCursorPrefab != null)
            {
                moveDestinationCursorInstance = Instantiate(moveDestinationCursorPrefab);
                moveDestinationCursorInstance.transform.SetParent(transform, false);
                moveDestinationCursorInstance.SetActive(false);
            }

            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onFloorPointerClick += OnFloorPointerClick;
            }

            var visitorPanel = gui.hud.personPanel.visitor;
            visitorPanel.Init(this);
            gui.hud.sportsmanPanel.Init(this);
            visitorPanel.OnClaimRequest = OnVisitorPanelClaimVisitorQuest;
            visitorPanel.OnKickRequest = KickSelectedVisitor;

            // Init exercise selector panel.
            onCharacterSelected += OnCharacterSelected_HUDUpdate;
            onCharacterDeselected += OnCharacterDeselected_HUDUpdate;
            onStartExercise += OnStartExercise_HUDUpdate;
            onSwitchExercise += OnSwitchExercise_HUDUpdate;
            onEndExercise += OnEndExercise_HUDUpdate;

            juicerOrderIcon.Instantiate();

            StartCoroutine(ReadyToSkipProcessingJob());
        }

        protected new void Start()
        {
            base.Start();

            gui.windows.OnWindowOpened_Callback += OnWindowOpened;
            gui.dbgView.btnAddSportsman.onClick.AddListener(logic.sportsmen.Debug_CreateSportsman );
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            // Very important, they can have callbacks into our code.
            gui.hud.skipPanel.Hide();
            gui.hud.exerciseSelectorPanel.Hide();
            gui.hud.personPanel.OnCharacterDeselected();

            onCharacterSelected -= OnCharacterSelected_HUDUpdate;
            onCharacterDeselected -= OnCharacterDeselected_HUDUpdate;
            onStartExercise -= OnStartExercise_HUDUpdate;
            onSwitchExercise -= OnSwitchExercise_HUDUpdate;

            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onFloorPointerClick -= OnFloorPointerClick;
            }
        }
        #endregion

        #region IPresentationCharacterManager interface.
        #endregion

        #region ICharacterActorSharedDataProvider interface.
        public override GameObject GetCursorObject(CharacterCursorType type)
        {
            return GetCursorInstance(type);
        }
        #endregion

        #region ICharacterInteractionProvider interface.
        public override void KickVisitor(int id)
        {
            if (logic != null)
                logic.KickRequest(id);
        }

        public override void ClaimQuest(int id)
        {
            if (logic != null)
                logic.ClaimQuestRequest(id);
        }

        public override bool SelectCharacter(int id)
        {
            if (logic != null)
            {
                return logic.SelectCharacterRequest(id);
            }
            return false;
        }

        public override void DeselectCharacter(int id)
        {
            if (logic != null)
            {
                logic.DeselectCharacterIfSelectedRequest(id);
            }
        }

        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("CharacterSelectionEvent")]
        private void OnCharacterSelectionEvent(int id, bool selected, bool focus)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (selected)
                {
                    actor.SetSelection(true);

                    SendCharacterSelectedEvent(actor.personInfo, focus);

                    if (focus)
                        cameraController.FocusCameraOnWorldPosition(actor.position);
                }
                else
                {
                    actor.SetSelection(false);

                    // In this order!
                    SendCharacterDeselectedEvent(actor.personInfo);
                }
                UpdateOverheadUI(actor);
            }

            gui.hud.OnPersonSelection();
        }

        [BindBridgeEvent("CharacterRefusedExerciseEvent")]
        private void OnCharacterRefusedExerciseEvent(int id, int equipmentId, int exerciseId)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                var characterOverheadIcon = new OverheadIconInstanceHolder<AnimatedActionIcon>(characterOverheadFadingIconPrefab);
                var overheadUIData = new OverheadUIDataCharacter();

                characterOverheadIcon.icon.autoHideDelay = exerciseRefusedAngryIconDisplayTime;

                overheadUIData.state = OverheadUIDataCharacter.State.Angry;
                overheadUIData.ApplyData(characterOverheadIcon.instance);

                characterOverheadIcon.instance.onClick.RemoveAllListeners();
                characterOverheadIcon.icon.frontIcon = (id == logic.selectedCharacterId);
                characterOverheadIcon.instance.animationType = AnimatedActionIcon.AnimationType.VerticalFading;
                actor.SetOverheadUIIcon(characterOverheadIcon);

                actor.StopMovement();
                actor.QueuePlayEmoteAnimation(AnimationSocial.Refuse, 1, true, null);

                StartCoroutine(UpdateOverheadUIAfterDelayJob(id, exerciseRefusedAngryIconDisplayTime));
            }
        }

        [BindBridgeEvent("CharacterReadyToSkipExerciseEvent")]
        private void OnCharacterReadyToSkipExerciseEvent(int id, int equipmentId, int exerciseId)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            if (playerProfileInteractionProvider.level<=1)
                return;

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.personInfo.roomName ==
                    clubInteractionProvider.GetRoomOfType(RoomType.Club).roomConductor.roomSetupDataInfo.name)
                {
                    readyToSkipActors.Add(actor);
                    Debug.LogFormat("Add actor {0} to ready to skip list", actor.personInfo.name);
                }
            }
        }

        [BindBridgeEvent("CharacterSchedulePendingExerciseEvent")]
        private void OnCharacterSchedulePendingExerciseEvent
            (int id, int equipmentId, int exerciseId, ExerciseInitiator initiator, TimeSpan duration)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                var characterOverheadIcon = new OverheadIconInstanceHolder<AnimatedActionIcon>(characterOverheadFadingIconPrefab);
                var overheadUIData = new OverheadUIDataCharacter();

                characterOverheadIcon.icon.autoHideDelay = exerciseStartHappyIconDisplayTime;

                overheadUIData.state = OverheadUIDataCharacter.State.Happy;
                overheadUIData.ApplyData(characterOverheadIcon.instance);
                characterOverheadIcon.instance.animationType = AnimatedActionIcon.AnimationType.VerticalFading;

                characterOverheadIcon.instance.onClick.RemoveAllListeners();
                characterOverheadIcon.icon.frontIcon = (id == logic.selectedCharacterId);
                actor.SetOverheadUIIcon(characterOverheadIcon);
                
                StartCoroutine(UpdateOverheadUIAfterDelayJob(id, exerciseStartHappyIconDisplayTime));
            }
        }

        [BindBridgeEvent("CharacterSwitchPendingExerciseEvent")]
        private void OnCharacterSwitchPendingExerciseEvent(int id, int exerciseId, TimeSpan duration)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            UpdateOverheadUI(id);
        }

        [BindBridgeEvent("CharacterCancelPendingExerciseEvent")]
        private void OnCharacterCancelPendingExerciseEvent(int id, int equipmentId)
        {
            UpdateOverheadUI(id);
        }

        [BindBridgeEvent("CharacterLevelUpEvent")]
        private void OnCharacterLevelUpEvent(int id, int newLevel)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.RemoveOverheadUIIcon(); // A bit of hack.
                DelayedInvoke(() => UpdateOverheadUI(id));
            }
        }
        #endregion

        #region Protected virtual API.
        protected override void OnCharacterClick(int actorId, PointerEventData eventData)
        {
            // Debug.LogFormat("OnCharacterClick: actorId: {0}, eventData: {1}", actorId, eventData);

            if (gui.windows.GetCurrentType() == WindowTypes.PopUp)
            {
                return;
            }

            if (logic != null)
            {
                logic.SelectCharacterRequest(actorId);
            }
        }

        OverheadIconInstanceHolder<SingleActionIcon> OverheadIconHolderForPerson(IPersonInfo personInfo)
        {
            if (personInfo is IPersonTrainerInfo)
                return new OverheadIconInstanceHolder<SingleActionIcon>(characterOverheadTrainerLevelUpPrefab);
            if (personInfo is IPersonTrainerClientInfo)
                return new OverheadIconInstanceHolder<SingleActionIcon>(charTrainClientOverheadIconPrefab);
            else // IPersonVisitorInfo, IPersonJuicerInfo
                return new OverheadIconInstanceHolder<SingleActionIcon>(characterOverheadIconPrefab);
        }

        protected override void UpdateOverheadUI(ICharacterActor actor)
        {
            var personInfo = actor.personInfo;
            var actorId = actor.id; // Fetched, so we will not grab whole actor into delegates in anonymous functions.

            switch (personInfo.state)
            {
                case PersonState.Entering:
                {
                    if (!(personInfo is IPersonJuicerInfo))
                    {
                        actor.RemoveOverheadUIIcon();
                    }
                }; break;

                case PersonState.Idle:
                {
                    actor.RemoveOverheadUIIcon();

                    if (personInfo is IPersonVisitorInfo)
                    {
                        var questStage = logic.GetQuestStage(actor.id);

                        if (questStage != null)
                        {
                            if (personInfo is IPersonTrainerClientInfo)
                            {
                                if (questStage.Value == QuestStage.InProgress)
                                    return;
                            }

                            var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);
                            
                            var overheadUIData = new OverheadUIDataCharacter();

                            switch (questStage.Value)
                            {
                                case QuestStage.New:
                                {
                                    overheadUIData.state = OverheadUIDataCharacter.State.Question;
                                    overheadUIData.level = 0;
                                }; break;

                                case QuestStage.Completed:
                                {
                                    var visitorQuest = questsInteractionProvider.GetVisitorQuest(actorId);
                                    if (visitorQuest != null &&
                                        visitorQuest.reward.rewardType == QuestRewardType.BecameSportsmen)
                                    {
                                        overheadUIData.state = OverheadUIDataCharacter.State.BecameSportsmen;
                                    }
                                    else
                                    {
                                        overheadUIData.state = OverheadUIDataCharacter.State.Gold;
                                    }
                                }; break;

                                case QuestStage.InProgress:
                                {
                                    var objectiveData = logic.GetVisitorQuestCurrentObjectiveData(actor.id);

                                    if (objectiveData == null)
                                    {
                                        overheadUIData.state = OverheadUIDataCharacter.State.Gold;
                                        overheadUIData.level = 0;
                                    }
                                    else if (objectiveData.type == VisitorQuestType.Exercise)
                                    {
                                        overheadUIData.state = OverheadUIDataCharacter.State.Desire;
                                        overheadUIData.actionIcon = objectiveData.exercise.icon;
                                        overheadUIData.level = objectiveData.exercise.tier;
                                    }
                                    else if (objectiveData.type == VisitorQuestType.Stats)
                                    {
                                        switch (objectiveData.exerciseType)
                                        {
                                            case ExersiseType.Arms:
                                                overheadUIData.state = OverheadUIDataCharacter.State.Arms;
                                                break;
                                            case ExersiseType.Cardio:
                                                overheadUIData.state = OverheadUIDataCharacter.State.Cardio;
                                                break;
                                            case ExersiseType.Legs:
                                                overheadUIData.state = OverheadUIDataCharacter.State.Legs;
                                                break;
                                            case ExersiseType.Torso:
                                                overheadUIData.state = OverheadUIDataCharacter.State.Torso;
                                                break;
                                        }

                                        overheadUIData.statRemaining = objectiveData.iterationsGoal -
                                                                        objectiveData.iterationsDone;
                                        overheadUIData.level = 0;
                                    }
                                    else
                                    {
                                        overheadUIData.state = OverheadUIDataCharacter.State.Angry;
                                    }
                                }; break;
                            }

                            overheadUIData.ApplyData(characterOverheadIcon.instance);
                            characterOverheadIcon.instance.onClick.RemoveAllListeners();
                            characterOverheadIcon.instance.onClick.AddListener(
                                delegate() { OnOverheadUIClickSelectCharacter(actorId); });
                            characterOverheadIcon.icon.frontIcon = (actorId == logic.selectedCharacterId);
                            actor.SetOverheadUIIcon(characterOverheadIcon);
                        }

                    }
                    else if (personInfo is IPersonTrainerInfo)
                    {
                        var trainerInfo = personInfo as IPersonTrainerInfo;

                        if (trainerInfo.canLevelUp)
                        {
                            var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);


                            // SUPER UGLY HACK
                            // Sorry Anton!
                            var trainer = personInfo as PersonTrainer;

                            characterOverheadIcon.instance.onClick.AddListener(trainer.LevelUp);
                            actor.SetOverheadUIIcon(characterOverheadIcon);
                        }
                    }

                }; break;

                case PersonState.Leaving:
                {
                    if (!(personInfo is IPersonVisitorInfo || personInfo is IPersonTrainerClientInfo))    // For visitors, Overhead UI is updated in CharacterLeavingRoomEvent handler.
                    {
                        actor.RemoveOverheadUIIcon();
                    }
                }; break;

                case PersonState.ExerciseComplete:
                {
                    actor.RemoveOverheadUIIcon();

                    if (personInfo is IPersonVisitorInfo)
                    {
                        var questStage = logic.GetQuestStage(actor.id);

                        if (questStage != null)
                        {
                            if (personInfo is IPersonTrainerClientInfo)
                            {
                                if (questStage.Value != QuestStage.Completed)
                                    return;
                            }

                            var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);
                            var overheadUIData = new OverheadUIDataCharacter();

                            if (questStage.Value == QuestStage.Completed)
                            {
                                var visitorQuest = questsInteractionProvider.GetVisitorQuest(actorId);
                                if (visitorQuest != null &&
                                    visitorQuest.reward.rewardType == QuestRewardType.BecameSportsmen)
                                {
                                    overheadUIData.state = OverheadUIDataCharacter.State.BecameSportsmen;
                                }
                                else
                                {
                                    overheadUIData.state = OverheadUIDataCharacter.State.Gold;
                                }
                            }
                            else
                            {
                                overheadUIData.state = OverheadUIDataCharacter.State.Complete;
                            }

                            overheadUIData.ApplyData(characterOverheadIcon.instance);
                            characterOverheadIcon.instance.onClick.AddListener(() =>
                            {
                                OnOverheadUIClickSelectCharacter(actorId);
                            });
                            characterOverheadIcon.icon.frontIcon = (actorId == logic.selectedCharacterId);
                            actor.SetOverheadUIIcon(characterOverheadIcon);
                        }
                    }
                    else if (personInfo is IPersonJuicerInfo)
                    {
                        var questStage = logic.GetQuestStage(actor.id);

                        if (questStage != null)
                        {
                            if (questStage.Value == QuestStage.Completed)
                            {
                                var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);
                                var overheadUIData = new OverheadUIDataCharacter();

                                overheadUIData.state = OverheadUIDataCharacter.State.Gold;

                                overheadUIData.ApplyData(characterOverheadIcon.instance);
                                characterOverheadIcon.instance.onClick.AddListener(() =>
                                {
                                    OnOverheadUIClickSelectCharacter(actorId);
                                });
                                characterOverheadIcon.icon.frontIcon = (actorId == logic.selectedCharacterId);
                                actor.SetOverheadUIIcon(characterOverheadIcon);
                            }
                        }
                    }
                    else if (personInfo is IPersonSportsmanInfo)
                    {
                        var characterOverheadIcon =
                            new OverheadIconInstanceHolder<SingleActionIcon>(characterOverheadIconPrefab);
                        var overheadUIData = new OverheadUIDataCharacter();

                        overheadUIData.state = OverheadUIDataCharacter.State.Complete;
                        overheadUIData.ApplyData(characterOverheadIcon.instance);
                        characterOverheadIcon.instance.onClick.AddListener(() =>
                        {
                            OnOverheadUIClickSelectCharacter(actorId);
                        });
                        characterOverheadIcon.icon.frontIcon = (actorId == logic.selectedCharacterId);
                        actor.SetOverheadUIIcon(characterOverheadIcon);
                    }
                }; break;

                case PersonState.WorkoutPending:
                    break;

                case PersonState.Workout:
                {
                    if (personInfo is IPersonJuicerInfo)
                    {
                        actor.RemoveOverheadUIIcon();

                        var juiceQuest = questsInteractionProvider.GetJuiceQuest(actor.id);

                        if (juiceQuest != null)
                        {
                            if (actorId == logic.selectedCharacterId)
                            {
                                if (!juiceQuest.completed)
                                {
                                    juicerOrderIcon.icon.frontIcon = true; // (actorId == logic.selectedCharacterId);
                                    actor.SetOverheadUIIcon(juicerOrderIcon);
                                }
                            }
                            else
                            {
                                if (juiceQuest.isNew)
                                {
                                    var overheadUiData = new OverheadUIDataCharacter
                                    {
                                        state = OverheadUIDataCharacter.State.Question
                                    };
                                    var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);
                                    overheadUiData.ApplyData(characterOverheadIcon.instance);
                                    characterOverheadIcon.icon.frontIcon = false;
                                    characterOverheadIcon.instance.onClick.AddListener(() =>
                                    {
                                        OnOverheadUIClickSelectCharacter(actorId);
                                    });
                                    actor.SetOverheadUIIcon(characterOverheadIcon);
                                }
                                else if (!juiceQuest.completed && juiceQuest.readyToComplete)
                                {
                                        
                                    var overheadUiData = new OverheadUIDataCharacter
                                    {
                                                state = OverheadUIDataCharacter.State.TimequestNew
                                    };
                                        
                                    var characterOverheadIcon = new OverheadIconInstanceHolder<SingleActionAnimatedIcon>(characterOverheadAnimatedIconPrefab);

                                    overheadUiData.ApplyData(characterOverheadIcon.instance);
                                    characterOverheadIcon.icon.frontIcon = false;
                                    
                                    characterOverheadIcon.icon.scaleRatio = 1.0f;

                                    characterOverheadIcon.instance.onClick.AddListener(() =>
                                    {
                                        OnOverheadUIClickSelectCharacter(actorId);
                                    });
                                     
                                    actor.SetOverheadUIIcon(characterOverheadIcon);
                                    actor.overheadUIIcon.GetComponent<SingleActionAnimatedIcon>().curAnimation = SingleActionAnimatedIcon.Animations.Shaking;
                                    
                                }
                            }

                            if (juiceQuest.completed && juiceQuest.exerciseCompleteId == personInfo.workoutExercise.id)
                            {
                                var overheadUIData = new OverheadUIDataCharacter
                                {
                                    state = OverheadUIDataCharacter.State.Gold
                                };

                                var characterOverheadIcon = OverheadIconHolderForPerson(personInfo);

                                overheadUIData.ApplyData(characterOverheadIcon.instance);
                                characterOverheadIcon.icon.frontIcon = false;
                                characterOverheadIcon.instance.onClick.AddListener(() =>
                                {
                                    OnOverheadUIClickSelectCharacter(actorId);
                                });
                                actor.SetOverheadUIIcon(characterOverheadIcon);
                            }
                        }
                    }
                }; break;

                case PersonState.InQueue:
                {
                }; break;

                case PersonState.Sentenced:
                {
                    actor.RemoveOverheadUIIcon();
                }; break;

                case PersonState.Refused:
                {
                    actor.RemoveOverheadUIIcon();
                    if (personInfo is IPersonTrainerInfo)
                    {
                        var id = actor.id;

                        Action showIconAction = null;
                        showIconAction = () =>
                        {
                            // Refetch actor, because this action is invoked again from onClick callback.
                            ICharacterActor actor2;
                            if (this != null &&
                                gameObject != null &&
                                actors.TryGetValue(id, out actor2) &&
                                actor2.personInfo.state == PersonState.Refused)
                            {
                                var overheadUIData = new OverheadUIDataCharacter
                                {
                                    state = OverheadUIDataCharacter.State.Bored
                                };

                                var characterOverheadIcon = OverheadIconHolderForPerson(actor2.personInfo);

                                overheadUIData.ApplyData(characterOverheadIcon.instance);
                                characterOverheadIcon.icon.frontIcon = false;
                                characterOverheadIcon.instance.onClick.AddListener(() =>
                                {
                                    ShowHintOverheadUIIcon(actor2, Loc.Get("trainerCannotTrainOrSupportHint"), 2.0f, () => showIconAction.Invoke());
                                });
                                actor2.SetOverheadUIIcon(characterOverheadIcon);
                            }
                        };

                        showIconAction.Invoke();
                    }
                }; break;
            }
        }

        public override void DestroyCharacterRequest(int id)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (readyToSkipActors.Contains(actor))
                {
                    readyToSkipActors.Remove(actor);
                    // Debug.LogFormat("Remove actor {0} from ready to skip list", actor.personInfo.name);
                } 
            }

            base.DestroyCharacterRequest(id);
        }

        public override void EndExerciseRequest
            (int id, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            base.EndExerciseRequest(id, equipmentId, exercise, initiator, flags);
            
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (readyToSkipActors.Contains(actor))
                {
                    readyToSkipActors.Remove(actor);
                    // Debug.LogFormat("Remove actor {0} from ready to skip list", actor.personInfo.name);
                } 
            }
        }

        protected override void OnTrainerEnter(int id)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                SelectCharacter(id);
            }
        }
        #endregion

        #region Private functions.
        private void SetReadyToSkipOverhead(ICharacterActor actor)
        {
            var id = actor.id;
            var characterOverheadIcon = new OverheadIconInstanceHolder<AnimatedActionIcon>(characterOverheadFadingIconPrefab);
            var overheadUIData = new OverheadUIDataCharacter { state = OverheadUIDataCharacter.State.Timer };
            overheadUIData.ApplyData(characterOverheadIcon.instance);
            characterOverheadIcon.instance.animationType = AnimatedActionIcon.AnimationType.JumpingTimer;
            characterOverheadIcon.instance.onClick.RemoveAllListeners();
            characterOverheadIcon.instance.onClick.AddListener(() =>
            {
                //logic.SelectCharacterRequest(id);
                logic.SkipExerciseRequest(id);
                //logic.DeselectCharacterIfSelectedRequest(id);
            });
            characterOverheadIcon.icon.frontIcon = true;
            actor.SetOverheadUIIcon(characterOverheadIcon);
            CharacterShowReadyToSkipOverhead.Send(actor.personInfo);
        }

        void OnWindowOpened(bool isModal, bool hideOverheadUI)
        {
            if (isModal || hideOverheadUI)
                logic.DeselectCharacterIfSelectedRequest(PersonData.InvalidId);
        }

        void OnFloorPointerClick(PointerEventData eventData, string roomName)
        {
            if (gui.windows.GetCurrentType() == WindowTypes.PopUp)
            {
                return;
            }

            Assert.IsNotNull(clubInteractionProvider);

            var activeRoom = clubInteractionProvider.activeRoom;
            Assert.IsNotNull(activeRoom);

            if (activeRoom != null && activeRoom.roomConductor != null && logic != null && logic.hasSelection)
            {
                ICharacterActor actor;
                if (actors.TryGetValue(logic.selectedCharacterId, out actor))
                {
                    var clickWorldPosition = eventData.pointerPressRaycast.worldPosition;

                    Vector2 clubOrigin = new Vector2(activeRoom.roomObject.transform.position.x, activeRoom.roomObject.transform.position.z);
                    Vector2 clickPosition = new Vector2(clickWorldPosition.x, clickWorldPosition.z);
                    var point = clickPosition - clubOrigin;

                    if (logic.MoveCharacterRequest
                        (logic.selectedCharacterId, activeRoom.roomConductor.coordsMapper.ConvertPositionToRoomCoordinatesRounded(point)))
                    {
                        if (moveDestinationCursorInstance != null)
                        {
                            var cursorPoint = new Vector3(point.x, 0, point.y);

                            moveDestinationCursorInstance.SetActive(false); // Required to reset tween states.
                            moveDestinationCursorInstance.transform.SetParent(actor.roomConductor.roomObject.transform, false);
                            moveDestinationCursorInstance.transform.localPosition = cursorPoint;
                            moveDestinationCursorInstance.SetActive(true);
                        }
                    }
                }
            }
        }

/*
        void OnOverheadUIClickHappyExercise(int actorId)
        {
            isHappyWorkout = false;
            data.exerciseEndTime -= new TimeSpan(0, 0, (int)personManager.happyExerciseSettings.delay);
            UpdateOverheadUI(personManager.selectedCharacterId == actorId);

            //--- spawn Hint
            personManager.ShowHint(this, "30 sec");
        }
*/

        void OnOverheadUIClickSelectCharacter(int actorId)
        {
            var visitorQuest = questsInteractionProvider.GetVisitorQuest(actorId);

            if (visitorQuest != null && visitorQuest.completed)
            {
                if (visitorQuest.reward.rewardType == QuestRewardType.BecameSportsmen && !logic.sportsmen.CanAdd())
                {
                    gui.confirmWnd.OnCantAddSportsmen();
                    gui.confirmWnd.OnConfirm = logic.sportsmen.ShowExpandMemebersLimit;
                    gui.confirmWnd.OnCancel = () => { OnKickVisitorById(actorId); } ;
                    return;
                }
            }

            logic.SelectCharacterRequest(actorId);
            logic.ClaimQuestRequest(actorId);
        }

        void OnOverheadUIClickSkipExercise(int actorId)
        {
            logic.SkipExerciseRequest(actorId);
        }

        GameObject GetCursorInstance(CharacterCursorType type)
        {
            int cursorIdx = (int) type;
            Assert.IsTrue(cursorIdx >= 0 && cursorIdx < instancedCursors.Length);
            if (cursorIdx >= 0 && cursorIdx < instancedCursors.Length)
            {
                Assert.IsNotNull(instancedCursors[cursorIdx]);
                return instancedCursors[cursorIdx];
            }

            return null;
        }
            

        void OnKickVisitorById(int id)
        {
            logic.SelectCharacterRequest(id);
            KickSelectedVisitor();
        }


        void KickSelectedVisitor()
        {
            var selectedCharId = characterInteractionProvider.selectedCharacterId;

            gui.confirmWnd.OnCustom(Loc.Get("confirmSkipVisitorHeader"),
                Loc.Get("confirmSkipVisitorMessage"),
                null,
                true);

            gui.confirmWnd.OnConfirm = () =>
            {

                if (selectedCharId != PersonData.InvalidId)
                    characterInteractionProvider.KickVisitor(selectedCharId);
            };
        }

        void OnVisitorPanelClaimVisitorQuest()
        {
            if (logic.selectedCharacterId != PersonData.InvalidId)
                logic.ClaimQuestRequest(logic.selectedCharacterId);
        }

        private IEnumerator ReadyToSkipProcessingJob()
        {
            while (true)
            {
                if (readyToSkipSwitchingLagTime > 0)
                    yield return new WaitForSeconds(readyToSkipSwitchingLagTime);
                else
                    yield return null;

                if (readyToSkipActors.Any() && readyToSkipActors.All(i => i.overheadUIIcon == null || !i.overheadUIIcon.visible))
                {
                    // Debug.LogFormat("Add wait for skip overhead for {0}", readyToSkipPersons[0].personInfo.name);
                    SetReadyToSkipOverhead(readyToSkipActors[0]);
                }
                else
                {
                    /*Debug.LogFormat(
                        "Can't set overheads.\nPersons ready to skip: {0}\nPersons which has not null overhead: {1}\nPerson which has visible overhead:{2}",
                        readyToSkipPersons.Count,
                        readyToSkipPersons.Count(i => i.overheadUIIcon != null),
                        readyToSkipPersons.Count(i => i.overheadUIIcon != null && i.overheadUIIcon.visible));
                        */
                }
            }
        }

        private void ShowSkipPanel (IPersonInfo personInfo)
        {
            var show = false;
            if (personInfo is IPersonJuicerInfo)
            {
                var questStage = logic.GetQuestStage(personInfo.id);
                if (questStage != null && questStage.Value == QuestStage.Completed)
                {
                    var juiceQuest = questsInteractionProvider.GetJuiceQuest(personInfo.id);
                    if (juiceQuest != null && personInfo.workoutExercise != null && personInfo.workoutExercise.id == juiceQuest.exerciseId)
                    {
                        show = true;
                    }
                }
            }
            else
            {
                show = true;
            }

            if (show)
            {
                var interruptType = personInfo.workoutExercise.interruptType; // Cache this value, we don't want to cache personInfo in closure.
                var personId = personInfo.id; // Cache this value, we don't want to cache personInfo in closure.

                gui.hud.skipPanel.Display
                    ( TimeSpan.FromSeconds(personInfo.exerciseTotalTime)
                    , personInfo.exerciseEndTime
                    , (int totalSeconds) =>
                    {
                        switch (interruptType)
                        {
                            case Exercise.InterruptType.Uninterruptable:
                                return -1;

                            case Exercise.InterruptType.ForBucks:
                                return questsInteractionProvider.isTutorialRunning ? 0 : DynamicPrice.GetSkipCost(totalSeconds);

                            case Exercise.InterruptType.ForBucksNoFree:
                                return questsInteractionProvider.isTutorialRunning ? 0 : DynamicPrice.GetSkipCostNoFree(totalSeconds);

                            case Exercise.InterruptType.ForFree:
                                return 0;
                        }

                        return 0;
                    }
                    , () => { OnOverheadUIClickSkipExercise(personId); }
                    , () => (logic.selectedCharacterId != personId)
                    , null
                    , personInfo is IPersonJuicerInfo ? Loc.Get("idEating") : Loc.Get("idDoingExercise"));
            }
        }

        void OnCharacterSelected_HUDUpdate (IPersonInfo personInfo, bool focus)
        {
            int id = personInfo.id;

            DelayedInvoke(() =>
            {
                ICharacterActor actor;
                if (actors.TryGetValue(id, out actor)) 
                {
                    personInfo = actor.personInfo; // Must be re-fetched because of delayed action.

                    if (personInfo is IPersonVisitorInfo || personInfo is IPersonTrainerClientInfo || personInfo is IPersonTrainerInfo)
                    {
                        if (personInfo.state == PersonState.Idle)
                        {
                            gui.hud.exerciseSelectorPanel.SetActiveMode(personInfo.isPlayerControllable && personInfo.canBeTrainedByPlayer);
                        }
                        else
                        {
                            gui.hud.exerciseSelectorPanel.Hide();
                        }
                    }

                    if (personInfo.state == PersonState.Workout)
                    {
                        ShowSkipPanel(personInfo);
                        return;
                    }
                }
            });
        }

        void OnCharacterDeselected_HUDUpdate(IPersonInfo personInfo)
        {
            int id = personInfo.id;

            DelayedInvoke(() =>
            {
                ICharacterActor actor;
                if (actors.TryGetValue(id, out actor)) 
                {
                    personInfo = actor.personInfo; // Must be re-fetched because of delayed action.

                    if (personInfo is IPersonVisitorInfo || personInfo is IPersonTrainerClientInfo)
                    {
                        gui.hud.exerciseSelectorPanel.SetActiveMode(false);
                    }
                }
            });
        }

        void OnStartExercise_HUDUpdate
            ( IPersonInfo personInfo
            , int equipmentId
            , int exerciseAnimation
            , ExerciseInitiator initiator
            , TimeSpan duration
            , ExerciseFlags flags
            , float speed)
        {
            int id = personInfo.id;

            DelayedInvoke(() =>
            {
                ICharacterActor actor;
                if (actors.TryGetValue(id, out actor))
                {
                    personInfo = actor.personInfo; // Must be re-fetched because of delayed action.

                    if (personInfo.id == logic.selectedCharacterId &&
                        (flags & (ExerciseFlags.StartsInOffline | ExerciseFlags.EndsInOffline)) == 0)
                    {
                        if (personInfo is IPersonVisitorInfo || personInfo is IPersonTrainerClientInfo)
                        {
                            gui.hud.exerciseSelectorPanel.Hide();
                        }
                    }
                }
            });
        }

        void OnSwitchExercise_HUDUpdate (IPersonInfo personInfo, int exerciseAnimation, float speed)
        {
            int id = personInfo.id;

            DelayedInvoke(() =>
            {
                ICharacterActor actor;
                if (actors.TryGetValue(id, out actor))
                {
                    personInfo = actor.personInfo; // Must be re-fetched because of delayed action.

                    if (personInfo.id == logic.selectedCharacterId)
                    {
                        if (personInfo is IPersonVisitorInfo || personInfo is IPersonTrainerClientInfo)
                        {
                            gui.hud.exerciseSelectorPanel.Hide();
                        }

                        if (personInfo.state == PersonState.Workout)
                        {
                            ShowSkipPanel(personInfo);
                            return;
                        }
                    }
                }
            });
        }

        void OnEndExercise_HUDUpdate
            (IPersonInfo personInfo, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            int id = personInfo.id;

            DelayedInvoke(() =>
            {
                ICharacterActor actor;
                if (actors.TryGetValue(id, out actor))
                {
                    personInfo = actor.personInfo; // Must be re-fetched because of delayed action.

                    if (personInfo.id == logic.selectedCharacterId /*&& ((flags & ExerciseFlags.Skipped) != 0)*/)
                    {
                        gui.hud.skipPanel.Hide();
                    }
                }
            });
        }
        #endregion
    }
}