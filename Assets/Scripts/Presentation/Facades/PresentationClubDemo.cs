﻿using System.Collections;
using Core.Loader;
using Data.Room;
using UnityEngine;
using UnityEngine.Assertions;
using View.Rooms;
using Core;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Club Demo")]
    public class PresentationClubDemo : PresentationClubBase
    {
        #region Public (Inspector) data.
        #endregion

        #region Private data.
        #endregion

        #region Bridge.
        #endregion

        #region Unity API.
        protected new void Start()
        {
            base.Start();

            foreach (var room in rooms.Values)
            {
                room.transitionToRoomCheckRequest   += OnTransitionToRoomCheckRequest;
            }

            gui.hud.SetHUDMode(View.UI.HUD.HUD.HUDMode.None);

            gui.hud.ShowBackButton
                (() =>
                {
                    gui.hud.HideBackButton();
                    gui.hud.ShowAll(false);
                    StartCoroutine(SwitchBackToGameModeJob());
                });
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            Assert.IsNotNull(rooms);
            foreach (var room in rooms.Values)
            {
                room.transitionToRoomCheckRequest   -= OnTransitionToRoomCheckRequest;
            }
        }
        #endregion

        #region Rooms events handlers.
        private bool OnTransitionToRoomCheckRequest(string roomName)
        {
            if (logic.GetRoomExpandStage(roomName) > 0)
                return true;

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                if (roomView.roomConductor.roomSetupDataInfo.roomType == RoomType.SportClub)
                {
                    var nextExpandUnlockLevel = logic.GetNextExpandUnlockLevel(roomName);
                    if (nextExpandUnlockLevel < 0)
                        return false;

                    return (playerProfileInteractionProvider.level >= nextExpandUnlockLevel);
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        IEnumerator SwitchBackToGameModeJob()
        {
            var splashShowComplete = false;
            PersistentCore.instance.splashScreenView.Show(() => splashShowComplete = true);

            yield return new WaitUntil(() => splashShowComplete);

            PersistentCore.ScheduleGameModeSwitching(GameMode.MainGame);

            yield break;
        }
    }
}