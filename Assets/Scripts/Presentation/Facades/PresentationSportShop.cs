using BridgeBinder;
using Data.Estate;
using Data.Estate.Info;
using Data.Person;
using Data.Room;
using Logic.Persons;
using Logic.Facades;
using Presentation.Base;
using UnityEngine;
using View.UI.Shop;
using Logic.Persons.Events;
using Data.Sport;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sport Shop")]
    [BindBridgeInterface(typeof (LogicSportShop))]
    public class PresentationSportShop
        : BasePresentationFacade<LogicSportShop>
    {
        // private static readonly string iconAttachmentObjectName = "icon_attachment";

        private SportShopResourceLogic[] currentShopResources;
        private SportShopEquipLogic[] currentShopEquips;
        private IEquipmentInfo selectedEquipment;

        private SportShopView view
        {
            get { return gui.sportShop; }
        }

        [BindBridgeSubscribe]
        private void Subscribe(LogicSportShop logicShop)
        {
            this.logic = logicShop;

            logic.onRoomSizeUpdated += OnRoomUpdated;
        }

        private void Start()
        {
            gui.sportShop.onTabSwitchedCallback += (a)=>UpdateView();
            gui.sportShop.OnUpdateContentOnShow_Callback += UpdateView;
            gui.sportShop.OnItemSelected_Callback += OnBuyItem;
            gui.sportShop.onSetDirty += UpdateView;
            gui.sportShop.OnHide_Callback += OnWindowHide;
            view.onShowByCityClick += OnShowByCityClick;

            view.OnShow_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event += SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event += CharacterBirthEventOnEvent;
            };

            view.OnHide_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event -= SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event -= CharacterBirthEventOnEvent;
            };
        }

        private void CharacterBirthEventOnEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                UpdateView();
        }

        private void SportsmanTypeChangedEventOnEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            UpdateView();
        }

        private void OnRoomUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupData)
        {
           
        }

        private void OnShowByCityClick()
        {
            if (logic.canUseMyFeature)
            {
                view.Show();
            }
            else
            {
                gui.confirmWnd.OnCustom("Sport Items Shop", "Available with Sport Club.");
            }
        }

        private void OnWindowHide()
        {
            if (selectedEquipment != null)
            {
                equipmentInteractionProvider.DeselectEquipmentIfSelectedRequest(selectedEquipment.id);
            }
            selectedEquipment = null;
        }

        private void OnEquipmentSelected(int id, IEquipmentInfo equipmentInfo)
        {
            if (equipmentInfo.estateType.itemType != EstateType.Subtype.Sport || !equipmentInfo.isLocked) return;
            selectedEquipment = equipmentInfo;
            view.ShowTrainersTab();
        }

        private void OnEquipmentDeselected(int id)
        {
            if (selectedEquipment == null) return;
            selectedEquipment = null;
            view.Hide();
        }

        private void OnBuyItem(SportShopViewItem viewItem)
        {
            var index = viewItem.idx;
            if (currentShopResources == null)
            {
                var equip = currentShopEquips[index];
                var locker = logic.GetLockState(equip);

                if (locker.isLocked && locker.lockType == SportItemLocktype.fullStorage)
                {
                    gui.storage.Show();
                }
                else if (locker.isLocked && locker.lockType == SportItemLocktype.waitCooldown)
                {
                    logic.TryBreakLocker(equip, locker, UpdateView,
                        (e) => Debug.LogErrorFormat("Can't break blocker at item [{0}], error: {1}", equip.ToString(), e));
                }
                else
                {
                    logic.TryBuy(equip, view.Hide,
                        (e) => Debug.LogErrorFormat("Can't buy [{0}] item, error: {1}", equip.ToString(), e));
                }
            }
            else
            {
                var resource = currentShopResources[index];
                var locker = logic.GetLockState(resource);
                if (locker.isLocked && locker.lockType == SportItemLocktype.fullStorage)
                {
                    gui.storage.Show();
                }
                else if (locker.isLocked && locker.lockType == SportItemLocktype.waitCooldown)
                {
                    logic.TryBreakLocker(resource, locker, UpdateView,
                        (e) => Debug.LogErrorFormat("Can't break blocker at item [{0}], error: {1}", resource.ToString(), e));
                }
                else
                {
                    logic.TryBuy(resource, UpdateView,
                        (e) => Debug.LogErrorFormat("Can't buy [{0}] item, error: {1}", resource.ToString(), e));
                }
            }
        }

        private void UpdateView()
        {
            if (gui.sportShop.activeTab == 1)
            {
                currentShopResources = null;
                currentShopEquips = logic.shopTrainers;
                view.UpdateItemsCount(currentShopEquips.Length);
                for (var i = 0; i < currentShopEquips.Length; i++)
                {
                    view[i].idx = i;
                    view[i].lockInfo = logic.GetLockState(currentShopEquips[i]);
                    view[i].SetData(currentShopEquips[i]);
                }
            }
            else
            {
                currentShopResources = logic.shopResources;
                currentShopEquips = null;
                view.UpdateItemsCount(currentShopResources.Length);
                for (var i = 0; i < currentShopResources.Length; i++)
                {
                    view[i].idx = i;
                    view[i].lockInfo = logic.GetLockState(currentShopResources[i]);
                    view[i].SetData(currentShopResources[i]);
                }
            }
        }
            
    }
}