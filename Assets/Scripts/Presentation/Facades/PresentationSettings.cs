﻿using System;
using Core;
using Data;
using Presentation.Base;
using UnityEngine;
using View;
using View.UI;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Settings")]
    public class PresentationSettings
        : BasePresentationFacade
    {
        SettingsView view;

        string localizationCandidateId;

        void Awake()
        {
            view = gui.settingsWnd;

            view.currentLanguageBtnClickHandler.AddListener(() => view.SwitchTab(SettingsView.SettingsTabs.Localization));
            view.localizationPanelBackClickHandler.AddListener(OnLocalizationPanelBackClick);
        }

        void Start ()
        {
            view.musicOn = Sound.instance.musicOn;
            view.soundOn = Sound.instance.soundOn;

            view.onMusicHandler.AddListener(OnMusicToggled);
            view.onSoundHandler.AddListener(OnSoundToggled);

            view.OnShow_Callback = UpdateView;

            var socialManager = PersistentCore.instance.socialManager;

/*            view.OnConnectGooglePlay = () => { socialManager.TryLogin(SocialNetworks.GooglePlay); };
            view.OnConnectFacebook = () => { socialManager.TryLogin(SocialNetworks.Facebook); };
            view.OnConnectGameCenter = () => { socialManager.TryLogin(SocialNetworks.GameCenter); };
            view.OnFeedback = OnMailToClick;
*/
            gui.rateUs.btnMailTo.onClick.AddListener(OnMailToClick);

            view.tatemGamesBtnClickHandler.AddListener(()=> {Application.OpenURL("http://tatemgames.com/");});
            view.moreGamesBtnClickHandler.AddListener(()=> {Application.OpenURL("http://tatemgames.com/games/");});
            view.btnFeedBack.onClick.AddListener(OnMailToClick);

            view.currentLanguageName= Loc.instance.currentLanguageInfo.localizedName;
            view.currentLanguageSprite = GetFlagSprite(Loc.instance.currentLanguageInfo.iconId);
            
            var languages = Loc.instance.allSupportedLanguages;
            view.languagesList.UpdateItemsCount(languages.Length);

            for (var i = 0; i < languages.Length; i++)
            {
                var item = view.languagesList[i];
                item.idx = i;
                item.languageName = languages[i].localizedName;
                item.languageIcon = GetFlagSprite(languages[i].iconId);
            }

            view.languagesList.OnItemSelected_Callback += OnLanguageSelected;

            view.SwitchTab(SettingsView.SettingsTabs.Main);

            view.btnFriendship.onClick.AddListener(OnFriendshipClick);

            view.OnShowHandler.AddListener(OnSettingsViewShow);
            view.OnHideHandler.AddListener(OnSettingsViewHide);
        }

        private void OnLanguageSelected(SettingsLanguagesListItem item)
        {
            var languages = Loc.instance.allSupportedLanguages;
            var lang = languages[item.idx];
            if (Loc.instance.currentLanguageInfo != lang)
            {
                view.ShowReloadHint();

                view.languagesList.UpdateItemsCount(1);
                view.languagesList[0].languageName = languages[item.idx].localizedName;
                view.languagesList[0].languageIcon = GetFlagSprite(languages[item.idx].iconId);

                localizationCandidateId = lang.localizationId;

                view.languagesList.OnItemSelected_Callback -= OnLanguageSelected;
            }
        }

        private void OnSettingsViewShow ()
        {
            localizationCandidateId = string.Empty;

            view.SwitchTab(SettingsView.SettingsTabs.Main);
        }

        private void OnSettingsViewHide ()
        {
            CheckIfLanguageSwitched();
        }

        private bool CheckIfLanguageSwitched ()
        {
            if (localizationCandidateId != string.Empty)
            {
                Loc.instance.SetLanguage(localizationCandidateId);
                localizationCandidateId = string.Empty;
                PersistentCore.ScheduleFullReload();
                return true;
            }

            return false;
        }

        private void OnLocalizationPanelBackClick ()
        {
            if (!CheckIfLanguageSwitched())
            {
                view.SwitchTab(SettingsView.SettingsTabs.Main);
            }
        }

        private Sprite GetFlagSprite(string iconId)
        {
            return Resources.Load<Sprite>("UI/LanguageIcons/" + iconId);
        }
            
        void OnSoundToggled(bool value)
        {
            Debug.Log("Sound toggled " + value.ToString());
            Sound.instance.soundOn = value;
        }

        void OnMusicToggled(bool value)
        {
            Debug.Log("Music toggled " + value.ToString());
            Sound.instance.musicOn = value;
        }

        void UpdateView()
        {
            view.musicOn = Sound.instance.musicOn;
            view.soundOn = Sound.instance.soundOn;
        }

        void OnMailToClick()
        {
            gui.windows.ShowDefault();

            var profileId = PersistentCore.instance.networkController.profileId;
            string subject = string.Format("{0} improvements (Player id: {1})"
                , Application.productName
                , profileId != string.Empty ? profileId : "<none>" );
            string body = string.Format("Hello {0}!\n\n", Application.companyName);

            PersistentCore.instance.ContactSupport(subject, body);
        }

        void OnFriendshipClick()
        {
            gui.windows.ShowDefault();

            string subject = string.Format("{0} Tatem Games friendship", Application.productName);
            string body = string.Format("Hello {0}!\n\n", Application.companyName);

            PersistentCore.instance.ContactSupport(subject, body);
        }
    }
}
