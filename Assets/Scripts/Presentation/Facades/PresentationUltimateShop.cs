﻿using UnityEngine;
using System.Collections;
using BridgeBinder;
using View.UI;
using Presentation.Base;
using System.Collections.Generic;
using Data;
using Core;
using System;
using System.Linq;
using Data.Estate;
using Data.Estate.Info;
using Data.Person;
using Presentation.Providers;
using Logic.Shop;
using View.UI.Base;
using Data.PlayerProfile;
using Core.Analytics;
using Logic.Shop.Facade;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(LogicUltimateShop))]
    public class PresentationUltimateShop : BasePresentationFacade<LogicUltimateShop>, IShopInteractionProvider
    {
        ShopUltimateView    view;

        [BindBridgeSubscribe]
        void Subscribe(LogicUltimateShop logic)
        {
            this.logic = logic;
            gui.hud.enablerPromo.CanEnabled_Callback = delegate { return logic.isPromoPurchaseEnable && logic.promoPurchaseWasShow; };
        }

        void Start()
        {
            view = gui.shopUltimate;

            view.onShowByCityClick += OnShowByCityClick;

            view.OnUpdateContentOnShow_Callback = UpdateView;
            view.OnLiveUpdate_Callback = UpdateTimers;
            view.btnBackCallback = UpdateView;
            view.OnShowBankCoins_Callback = ShowBankCoins;
            view.OnShowBankBucks_Callback = ShowBankBucks;
            view.ShowCategory_Callback = ShowCategory;
            view.OnHide_Callback = UpdateNewItems;
            view.OnShowHandler.AddListener(() =>
            {
                var provider = GetComponent<ICharacterInteractionProvider>();
                if (provider.selectedCharacterId != PersonData.InvalidId)
                    provider.DeselectCharacter(provider.selectedCharacterId);
            });

            view.categoriesView.SetButtonCallback("btnEquip", ShowEquip);
            view.categoriesView.SetButtonCallback("btnInterior", ShowInterior);
            view.categoriesView.SetButtonCallback("btnDecor", ShowDecor);
            view.categoriesView.SetButtonCallback("btnSportEquip", ShowSportEquip);
            view.categoriesView.SetButtonCallback("btnSportResources", ShowSportResources);

            equipmentInteractionProvider.onEquipmentSelected += OnEquipmentSelected;
            equipmentInteractionProvider.onEquipmentDeselected += OnEquipmentDeselected;

//            gui.hud.btnStyle.onClick.AddListener(delegate { view.Show(); ShowDecor();});
            gui.hud.btnStyleEnabler.CanEnabled_Callback = delegate { return true;};//!logic.lockDecor; };

            gui.hud.btnPromo.onClick.AddListener(OnPromoPurchaseClick);
            gui.promo.buyBtn.onClick.AddListener(OnPromoPurchaseBuyClick);

            gui.shopUltimate.OnClosedByUserClick_Callback = OnHideShopWindowByUserClick;

            InitItems(logic.itemsEquip, UltimateShopItemType.Equip);
            InitItems(logic.itemsDecor, UltimateShopItemType.Decor);
            InitItems(logic.itemsInterior, UltimateShopItemType.Interior);
            InitItems(logic.itemsSportEquip, UltimateShopItemType.SportEquip);
            InitItems(logic.itemsSportResources, UltimateShopItemType.SportResources);

            UpdateNewItems();
        }

        void OnDestroy()
        {
            if (view != null)
            {
                view = gui.shopUltimate;

                view.onShowByCityClick -= OnShowByCityClick;
            }
        }

        private void OnShowByCityClick()
        {
            if (!logic.isAnyTutorialRunning)
                view.Show();
        }

        void UpdateView()
        {
            view.UpdateWindowNameForCategory(UltimateShopItemType.None);
            view.ShowCategories();
            view.btnBackOn = false;
            view.lockSportFeatures = logic.lockSportFeatures;
            view.lockDecors = logic.lockDecor;
            view.lockDecorsLevel = logic.decorStartLevel;

            UpdateNewItems();
        }

        void UpdateTimers()
        {           
            if (!view.isItemsVisible)
                return;
            
            var viewItems = view.list.items;
            var items = logic.itemsSportResources;

            if (viewItems.Count == items.Count 
                && viewItems[0].type == UltimateShopItemType.SportResources )
            {
                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    var viewItem = viewItems[i];

                    SetItemLock(viewItem, logic.GetLock(item, i));

                    //viewItem.locked = logic.IsLocked(item, i);

                    var timer = logic.GetTimeForItem(item);
                    if (timer != null)
                    {
                        viewItem.time = View.PrintHelper.GetTimeString(timer.timeSpan);
                        viewItem.skipPrice = DynamicPrice.GetSkipCost((int)timer.secondsToFinish);
                    }
                }
            }
        }

        void Update()
        {
            //--- update promo purchase icon
            if (logic.isPromoPurchaseEnable && logic.promoPurchaseWasShow)
            {
                gui.hud.txtPromoTimer.text = View.PrintHelper.GetMinSecTimeString(logic.GetTimerForPromoPurchase().timeSpan);

                if (!logic.isPromoPurchaseEnable)
                    gui.hud.UpdateHUD();
            }
        }

        private void SetItemLock(ShopUltimateViewItem viewItem, ShopUltimateViewItem.LockType lockType)
        {
            if (lockType > ShopUltimateViewItem.LockType.GymExpandSeparator)
                viewItem.SetLockType(lockType, logic.GetRoomNameForLock(lockType));
            else
                viewItem.SetLockType(lockType);
        }

        void OnSkip(ShopUltimateViewItem viewItem)
        {
            if (viewItem.type == UltimateShopItemType.SportResources)
            {
                var item = logic.itemsSportResources[viewItem.idx];

                var timer = logic.GetTimeForItem(item);
                if (timer != null)
                {
                    var bucks = DynamicPrice.GetSkipCost((int)timer.secondsToFinish);

                    var cost = new Cost(Cost.CostType.BucksOnly, bucks, 0);

                    if (playerProfileInteractionProvider.SpendMoney(cost, new Analytics.MoneyInfo("SportResource",  "Skip " + ((ResourceType)item.id).ToString())))
                    {
                        logic.RemoveTimerForItem(item);
                    }

                    return;
                }
                else
                    Debug.LogError("OnSkip : no timer");
            }
        }

        void InitItems(List<UltimateShopItemData> items, UltimateShopItemType category)
        {
            view.SwitchTo(category);

            view.ScrollToBegin();
            view.ShowItems();
            view.btnBackOn = true;

            if (view.list.items.Count == 0)
            {
                view.list.OnItemSelected_Callback = Buy;
                view.list.UpdateItemsCount(items.Count);

                var viewItems = view.list.items;

                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    var viewItem = viewItems[i];

                    viewItem.SetForceShowSkipAnimationState(true);
                    viewItem.type       = item.type;
                    viewItem.title      = item.name;
                    viewItem.icon       = item.icon;
                    viewItem.meshPrefab = item.meshPrefab;
                    viewItem.cost       = item.cost;
                    //viewItem.locked     = logic.IsLocked(item, i);
                    SetItemLock(viewItem, logic.GetLock(item, i));
                    viewItem.idx        = i;
                    viewItem.owned      = logic.IsItemOwned(item);
                    viewItem.style      = logic.GetStyle(item);
                    viewItem.lockGymSize= logic.GetNeedSizeForEquip(item);

                    if (item.type == UltimateShopItemType.Interior)
                        viewItem.interiorId = item.id;
                    else
                        viewItem.interiorId = -1;

                    if (item.type == UltimateShopItemType.SportResources)
                        viewItem.onSkip = OnSkip;

                    if (item.type == UltimateShopItemType.Equip)
                    {
                        viewItem.exerciseOn = true;
                    }
                }
            }
            else
            {
                var viewItems = view.list.items;

                for (int i = 0; i < items.Count; i++)
                {
                    var item = items[i];
                    var viewItem = viewItems[i];

                    viewItem.SetForceShowSkipAnimationState(true);
                    SetItemLock(viewItem, logic.GetLock(item, i));
                    viewItem.owned      = logic.IsItemOwned(item);
                    viewItem.style      = logic.GetStyle(item);
                    viewItem.lockGymSize= logic.GetNeedSizeForEquip(item);

                    if (item.type == UltimateShopItemType.Equip)
                    {
                        if (!viewItem.exercise.isInited)
                            UpdateExerciseInfo(item, viewItem);
                    }
                }

                logic.MarkNewItemsAsSeen(category);
            }

            UpdateItemsCoutners(items);
        }

        void UpdateExerciseInfo(UltimateShopItemData item, ShopUltimateViewItem viewItem)
        {
            if (item.type == UltimateShopItemType.Equip)
            {
                viewItem.exerciseOn = true;
                var stats = DataTables.instance.estateTypes[item.id].stats;
                var exercise = viewItem.exercise;

                if (stats.cardio > 0)
                {
                    exercise.typeSprite = GUICollections.instance.stateSpritesList.array[GUICollections.GetIconIdxForExerciseType(ExersiseType.Cardio)];
                    exercise.power = stats.cardioNormalized;
                }
                else if (stats.arms > 0)
                {
                    exercise.typeSprite = GUICollections.instance.stateSpritesList.array[GUICollections.GetIconIdxForExerciseType(ExersiseType.Arms)];
                    exercise.power = stats.armsNormalized;
                }
                else if (stats.legs > 0)
                {
                    exercise.typeSprite = GUICollections.instance.stateSpritesList.array[GUICollections.GetIconIdxForExerciseType(ExersiseType.Legs)];
                    exercise.power = stats.legsNormalized;
                }
                else if (stats.torso > 0)
                {
                    exercise.typeSprite = GUICollections.instance.stateSpritesList.array[GUICollections.GetIconIdxForExerciseType(ExersiseType.Torso)];
                    exercise.power = stats.torsoNormalized;
                }
            }
            else
                viewItem.exerciseOn = false;

        }

        void UpdateItemsCoutners(List<UltimateShopItemData> items)
        {
            view.list.UpdateItemsCount(items.Count);

            var viewItems = view.list.items;

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var viewItem = viewItems[i];

                bool outOfStock;

                viewItem.inStock    = logic.GetItemsInStock(item, out outOfStock);
                viewItem.outOfStock = outOfStock;
                viewItem.inStorage  = logic.GetItemsInStorage(item);
            }

        }

        private void ShowCategory(UltimateShopItemType category)
        {
            switch (category)
            {
            case UltimateShopItemType.Equip:
                ShowEquip();
                break;

            case UltimateShopItemType.Decor:
                ShowDecor();
                break;

            case UltimateShopItemType.Interior:
                ShowInterior();
                break;

            case UltimateShopItemType.SportEquip:
                ShowSportEquip();
                break;

            case UltimateShopItemType.SportResources:
                ShowSportResources();
                break;

            case UltimateShopItemType.Bank:
                Debug.LogError("Can't show Bank through this method, use ShowBankCoins and ShowBankBucks");
                break;

            default:
                Debug.LogError("Wrong type of shop category");
                break;
            }

        }

        public void ShowEquip()
        {
            view.UpdateWindowNameForCategory(UltimateShopItemType.Equip);
            InitItems(logic.itemsEquip, UltimateShopItemType.Equip);
        }

        public void ShowInterior()
        {
            view.UpdateWindowNameForCategory(UltimateShopItemType.Interior);
            InitItems(logic.itemsInterior, UltimateShopItemType.Interior);
        }

        public void ShowDecor()
        {
            if (logic.lockDecor)
                return;

            view.UpdateWindowNameForCategory(UltimateShopItemType.Decor);
            InitItems(logic.itemsDecor, UltimateShopItemType.Decor);
        }

        public void ShowSportEquip()
        {
            if (logic.lockSportFeatures)
                return;

            view.UpdateWindowNameForCategory(UltimateShopItemType.SportEquip);
            InitItems(logic.itemsSportEquip, UltimateShopItemType.SportEquip);
        }

        public void ShowSportResources()
        {
            if (logic.lockSportFeatures)
                return;

            view.UpdateWindowNameForCategory(UltimateShopItemType.SportResources);
            InitItems(logic.itemsSportResources, UltimateShopItemType.SportResources);
        }

        void ShowBankCoins()
        {
            view.UpdateWindowNameForCategory(UltimateShopItemType.Bank);

            var bankView = view.bankCoinsView;

            ShopUltimateBankViewList row;

            for (int i = 0; i < logic.itemsBankCoins.Count; i++)
            {
                row = i < 3 ? bankView.row1 : bankView.row2;
                var rowIdx = i % 3;

                var viewItem = row[rowIdx];
                var item = logic.itemsBankCoins[i];

                viewItem.icon = item.icon;
                viewItem.cost = item.cost;
                viewItem.priceReal = logic.GetIapStringPrice(item.id);
                viewItem.idx = item.id;
                viewItem.SetBadge(item.badgeType);
                viewItem.SetPercent(item.percent);
                viewItem.OnClickCallback = OnBuyBankItem;
            }
        }

        void OnBuyBankItem(UIBaseViewListItem viewItem)
        {
            UltimateShopItemData item = logic.BankItemByIdx(viewItem.idx);

            if (item == null)
            {
                Debug.LogError("ACHTUNG: can't find bank item with id " + viewItem.idx.ToString());
                return;
            }

            var oldItem = logic.GetOldShopItemData(item);
            
            if (logic.buyIAP.InitPreview(oldItem, null))
            {
                view.Hide();
                logic.buyIAP.ConfirmPurchase();
            }
            else
            {
                gui.confirmWnd.OnCustom(Loc.Get("inAppPurchaseNotAvailableHeader"), Loc.Get("inAppPurchaseNotAvailableMessage"));
            }   
        }

        void ShowBankBucks()
        {
            view.UpdateWindowNameForCategory(UltimateShopItemType.Bank);

            var bankView = view.bankBucksView;

            ShopUltimateBankViewList row;

            for (int i = 0; i < logic.itemsBankBucks.Count; i++)
            {
                row = i < 3 ? bankView.row1 : bankView.row2;
                var rowIdx = i % 3;

                var viewItem = row[rowIdx];
                var item = logic.itemsBankBucks[i];

                viewItem.icon = item.icon;
                viewItem.cost = item.cost;
                viewItem.priceReal = logic.GetIapStringPrice(item.id);
                viewItem.idx = item.id;
                viewItem.SetBadge(item.badgeType);
                viewItem.SetPercent(item.percent);
                viewItem.OnClickCallback = OnBuyBankItem;
            }
        }

        UltimateShopItemData GetItemFromView(ShopUltimateViewItem viewItem)
        {
            switch (viewItem.type )
            {
            case UltimateShopItemType.Equip:
                return logic.itemsEquip[viewItem.idx];
            case UltimateShopItemType.Decor:
                return logic.itemsDecor[viewItem.idx];
            case UltimateShopItemType.Interior:
                return logic.itemsInterior[viewItem.idx];
            case UltimateShopItemType.SportEquip:
                return logic.itemsSportEquip[viewItem.idx];
            case UltimateShopItemType.SportResources:
                return logic.itemsSportResources[viewItem.idx];
            case UltimateShopItemType.None:
                Debug.LogError("GetItemFromView : wrong type NONE");
                return null;
            }

            return null;
        }

        IEnumerator ExecuteActionInRoom(string room, Action action)
        {
            clubInteractionProvider.TransitionToRoomRequest(room);

            if (clubInteractionProvider.activeRoomName != room)
            {
                yield return new WaitUntil( () => clubInteractionProvider.activeRoomName == room );
            }

            action.Invoke();

            yield break;
        }

        void TryToBuyEquipDecor(ShopItemData item)
        {
            view.Hide();
            logic.buyEquipmentDecor.InitPreview(item, OnEquipmentDecorPreviewFinished);
        }

        void TryToBuyInterior(UltimateShopItemData item)
        {
            var oldItem = logic.GetOldShopItemData(item);

            if (logic.IsItemOwned(item))
            {
                view.Hide();

                logic.buyInterior.RestoreOwnedItem(oldItem);
            }
            else if (logic.buyInterior.InitPreview(oldItem))
            {
                gui.confirmWnd.OnBuyWallFloorConfirm(oldItem);

                gui.confirmWnd.wallFloorHasPaint.SetId(item.id);
                gui.confirmWnd.OnConfirm = OnInteriorBuyConfirm;
                gui.confirmWnd.OnCancel = OnInteriorBuyCancel;
            }
        }

/*
        void TryToBuySportEquip(UltimateShopItemData item)
        {
            logic.TryBuy(item);
            view.Hide();
        }
*/

        void Buy(ShopUltimateViewItem viewItem)
        {
            var item = GetItemFromView(viewItem);
            var oldItem = logic.GetOldShopItemData(item);

            switch (viewItem.type)
            {
                case UltimateShopItemType.Decor:
                case UltimateShopItemType.Equip:
                    StartCoroutine(ExecuteActionInRoom("Club", () => TryToBuyEquipDecor(oldItem) ));

                    return;
                case UltimateShopItemType.Interior:
                    StartCoroutine(ExecuteActionInRoom("Club", () => TryToBuyInterior(item) ));
                    return;
                case UltimateShopItemType.SportResources:

                    if (playerProfileInteractionProvider.storage.isFull && !logic.forceBuySportResource)
                    {
                        playerProfileInteractionProvider.WarnStorageIsFull(true);
                        return;
                    }

                    if (logic.TryBuy(item))
                    {
                        logic.AddTimerForItem(item);
                        UpdateItemsCoutners(logic.itemsSportResources);
                        viewItem.SetForceShowSkipAnimationState(false);
                        viewItem.ActivateDelayedSkipShowing();
                    }
                    else
                    {
                        BuyWithBucksInsteadOfCoins(viewItem, item);
                    }
                    break;
                case UltimateShopItemType.SportEquip:
                    StartCoroutine(ExecuteActionInRoom("SportClub", () => TryToBuyEquipDecor(oldItem) ));
                    return;
            }

            Debug.Log("BUY " + viewItem.type.ToString() + " " + viewItem.idx.ToString());
        }

        void BuyWithBucksInsteadOfCoins(ShopUltimateViewItem viewItem, UltimateShopItemData item)
        {
            var cost = item.cost;

            if (cost.type == Cost.CostType.CoinsOnly)
            {
                var diffCost = new Cost();
                diffCost.type = Cost.CostType.CoinsOnly;
                diffCost.value = cost.value - logic.playerProfile.coins;

                BuyCoinsForBucks(diffCost, new Analytics.MoneyInfo(item.GetCategoryString() , "BuyWithBucks"),
                    () =>
                    {
                        if (logic.TryBuy(item))
                        {
                            viewItem.SetForceShowSkipAnimationState(false);
                            viewItem.ActivateDelayedSkipShowing();

                            view.Show();
                            ShowSportResources();

                            ScrollToSportResource((ResourceType)item.id);
                        }
                    });
            }
        }

        void BuyWithBucksInsteadOfCoins(IShopPurchaseProtocol protocol)
        {
            var cost = protocol.itemToBuy.price;

            if (cost.type == Cost.CostType.CoinsOnly)
            {
                var diffCost = new Cost();
                diffCost.type = Cost.CostType.CoinsOnly;
                diffCost.value = cost.value - logic.playerProfile.coins;

                string itemName = protocol.itemToBuy.category.ToString();
                if (protocol.itemToBuy.category == ShopItemCategory.EQUIPMENTS || 
                    protocol.itemToBuy.category == ShopItemCategory.DECORS || 
                    protocol.itemToBuy.category == ShopItemCategory.SPORT_EQUIPMENTS)
                    itemName = protocol.itemToBuy.estateTypeData.locNameId;
                

                BuyCoinsForBucks(diffCost, new Analytics.MoneyInfo(protocol.itemToBuy.GetCategoryString() + " with Conversion" , itemName), () => { protocol.ConfirmPurchase(); } , protocol.CancelPurchase);
            }
            else
            {
                protocol.CancelPurchase();
            }
        }

        void OnInteriorBuyConfirm()
        {
            if (!logic.buyInterior.ConfirmPurchase())
            {
                BuyWithBucksInsteadOfCoins(logic.buyInterior);
            }
            else
                gui.windows.ShowDefault();
        }

        void OnInteriorBuyCancel()
        {
            logic.buyInterior.CancelPurchase();
            InitItems(logic.itemsInterior, UltimateShopItemType.Interior);
            gui.confirmWnd.Hide();
        }

        void OnEquipmentDecorPreviewFinished(bool accepted)
        {
            if (accepted)
            {
                if (!logic.buyEquipmentDecor.ConfirmPurchase())
                {
                    BuyWithBucksInsteadOfCoins(logic.buyEquipmentDecor);
                }
            }
            else
            {
                logic.buyEquipmentDecor.CancelPurchase();
                gui.windows.ShowDefault();
            }

        }

        void OnEquipmentDecorPreviewCancel()
        {
            gui.windows.ShowDefault();
            logic.buyEquipmentDecor.CancelPurchase();
        }

        private void OnEquipmentSelected(int id, IEquipmentInfo equipmentInfo)
        {
            if (equipmentInfo.estateType.itemType != EstateType.Subtype.Sport || !equipmentInfo.isLocked) 
                return;
            //selectedEquip = equipmentdata;
            view.Show();
            ShowSportEquip();
        }

        private void OnEquipmentDeselected(int id)
        {
            //if (selectedEquip == null) return;
            //selectedEquip = null;

            if( (gui.windows.GetCurrentType() != WindowTypes.VisitorQuests) &&
                (gui.windows.GetCurrentType() != WindowTypes.SportsmansList) &&
                !logic.isClubInEditMode
            )
                view.Hide();
        }

        public void UpdateNewItems()
        {
            var newEquipOn = false;
            var newDecorOn = false;
            var newInteriorOn = false;
            // var newSportEquipOn = false;
            var newSportResOn = false;

            var newItems = logic.GetNew();
            gui.hud.SetShopNewItems(newItems.Count);

            foreach (var newItem in newItems)
            {
                if (newItem.type == UltimateShopItemType.Equip)
                    newEquipOn = true;
                else if (newItem.type == UltimateShopItemType.Decor)
                    newDecorOn = true;
                else if (newItem.type == UltimateShopItemType.Interior)
                    newInteriorOn = true;
//              else if (newItem.type == UltimateShopItemType.SportEquip)
//                  newSportEquipOn = true;
                else if (newItem.type == UltimateShopItemType.SportResources)
                    newSportResOn = true;
            }

            view.newEquipOn = newEquipOn;
            view.newDecorOn = newDecorOn;
            view.newInteriorOn = newInteriorOn;
            view.newSportEquipOn = newSportResOn;
            view.newSportResOn = newSportResOn;

            UpdateNewBadges(logic.itemsEquip, newItems);
            UpdateNewBadges(logic.itemsDecor, newItems);
            UpdateNewBadges(logic.itemsInterior, newItems);
            UpdateNewBadges(logic.itemsSportEquip, newItems);
            UpdateNewBadges(logic.itemsSportResources, newItems);
        }

        void UpdateNewBadges(List<UltimateShopItemData> items, List<UltimateShopNewInfo> newItems)
        {
            if (items.Count == 0)
            {
                Debug.LogError("UpdateNewBadges ACHTUNG");
                return;
            }

            view.SwitchTo(items[0].type);
            var viewItems = view.list.items;

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var viewItem = viewItems[i];

                viewItem.newOn = IsItemNew(item, newItems);
            }
        }

        bool IsItemNew(UltimateShopItemData item, List<UltimateShopNewInfo> newItems)
        {
            foreach (var newItem in newItems)
            {
                if (item.type == newItem.type && item.id == newItem.id)
                    return true;
            }

            return false;
        }

        public void ScrollToSportResource(ResourceType resource)
        {
            var resources = DataTables.instance.sportResources;
            var pos = 0f;
            for (int i = 0; i < resources.Length; i++)
            {
                if (resource == resources[i].id)
                {
                    pos = i*1f/(resources.Length-1);
                    break;
                }
            }
            view.ScrollToPosition(pos);
        }

        bool IShopInteractionProvider.IsRewardedVideoAvailable()
        {
            return logic.IsRewardedVideoAvailable();
        }

        bool IShopInteractionProvider.IsRewardedVideoAvailableForTV()
        {
            return logic.IsRewardedVideoAvailableForTV();
        }

        bool IShopInteractionProvider.ShowRewardedAd(Action<bool, Cost> callback)
        {
            var result = logic.ShowRewardedAd(callback);

            #if UNITY_EDITOR
            if (result)
                gui.hud.modalMode = true;
            #endif

            return result;
        }

        void IShopInteractionProvider.ConfirmAdReward(Cost reward)
        {
            OnAdsShowFinished(true, reward);
            Logic.Achievements.WatchAd.Send();
        }

        void OnAdsShowFinished(bool result, Cost reward)
        {
            if (result && reward != null)
            {
                gui.confirmWnd.OnAdReward(reward.value);
                gui.confirmWnd.OnConfirm = delegate()
                {  
                    logic.ConfirmRewardedAd (reward);
                };
                gui.confirmWnd.OnCancel = delegate()
                {  
                    logic.ConfirmRewardedAd (reward);
                };
            }
        }

        void OnPromoPurchaseClick()
        {
            ShowCurPromo();
        }

        void OnPromoPurchaseBuyClick()
        {
            if (logic.isPromoPurchaseEnable)
            {
                PromoPurchases ppInfo = DataTables.instance.promoPurchases[logic.currentPromoId]; 
                logic.buyIAP.BuyIAP(ppInfo.newPriceId);     
            }

            view.Hide();
        }

        protected override UIBaseView GetView()
        {
            return view;
        }

        void OnHideShopWindowByUserClick()
        {
            if (logic.isPromoPurchaseEnable && !logic.promoPurchaseWasShow)
            {
                gui.windows.OnWindowCloseEvent += OnShopWindowsClosed;
            }               
        }

        void OnShopWindowsClosed(WindowTypes type)
        {
            gui.windows.OnWindowCloseEvent -= OnShopWindowsClosed;
            if (type != WindowTypes.ShopUltimate)
                return;

            if (logic.isAnyTutorialRunning)
                return;

            ShowCurPromo();
            logic.InitPromoPurchase();
        }

        void ShowCurPromo()
        {
            if (logic.isPromoPurchaseEnable)
            {
                if (logic.currentPromoId >= DataTables.instance.promoPurchases.Length)
                {
                    Debug.LogError("Error logic.currentPromoId");
                    return;                
                }

                PromoPurchases ppInfo = DataTables.instance.promoPurchases[logic.currentPromoId]; 
                Data.IAP.IAPEntryData oldIap;
                Data.IAP.IAPEntryData newIap;

                if (DataTables.instance.iapEntries.TryGetValue(ppInfo.oldPriceId, out oldIap) &&
                    DataTables.instance.iapEntries.TryGetValue(ppInfo.newPriceId, out newIap))
                {
                    var shopItem = logic.FindShopItemByIAPProductId(ppInfo.newPriceId);
                    if (shopItem != null && shopItem.bankItemData != null)
                    {
                        gui.promo.Show();
                        gui.promo.Setup(ppInfo, oldIap.defaultCost, newIap.defaultCost, shopItem.bankItemData.Value.coins, shopItem.bankItemData.Value.bucks, () =>
                        {
                            var timer = logic.GetTimerForPromoPurchase();
                            return timer != null ? timer.timeSpan : TimeSpan.Zero;
                        });
                    }
                }
            }
        }

        public void OnPurchaseIAP()
        {
            gui.hud.UpdateHUD();
        }
    }
}