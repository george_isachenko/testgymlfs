﻿// #define DEBUG_NOTIFICATION_TIMES

using System;
using System.Linq;
using BridgeBinder;
using Data;
using InspectorHelpers;
using Logic.Notifications.Facade;
using Presentation.Base;
using UnifiedNotifications;
using UnifiedNotifications.Extensions.Android;
using UnityEngine;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof (LogicNotifications))]
    [AddComponentMenu("Kingdom/Presentation/PresentationNotifications")]
    public class PresentationNotifications :
        BasePresentationFacade<LogicNotifications>
    {
        #region Types.
        [Serializable]
        public class NotificationDescription
        {
            public string name;

            public string id;

            [Header("Icons")]
            public string smallIcon;
            public string largeIcon;

            [Header("Sound")]
            public bool useSound;
            public string customSoundName;

            [Header("Vibration")]
            public bool useVibration;
            public int[] vibrationPattern;

            [Header("Lights")]
            public bool useLights;
            public Color lightsColor;
            public int lightsOnMs;
            public int lightsOffMs;
        }
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected NotificationDescription[] notifications;

        [Tooltip("Time duration (in seconds from now) in which requests are ignored.")]
        [SerializeField]
        protected float deadZoneTime;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected void Subscribe(LogicNotifications logic)
        {
            this.logic = logic;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(LogicNotifications logic)
        {
            this.logic = null;
        }
        #endregion

        #region Public API.
        public void ClearLocalNotifications()
        {
            // Debug.Log("Remove all registered local notifications.");
            UnifiedNotificationsService.ClearLocalNotifications(true, true);
        }

        public void CancelLocalNotification( string name, bool cancelPending, bool cancelShown)
        {
            // Debug.LogFormat("Canceling notification '{0}', cancel pending: {1}, cancel shown: {2}", name, cancelPending, cancelShown);

            var notificationDesc = notifications.Where (x => x.name == name).FirstOrDefault();
            if (notificationDesc == null)
            {
                Debug.LogErrorFormat("Unknown notification description '{0}'.", name);
                return;
            }

            UnifiedNotificationsService.CancelLocalNotification(notificationDesc.id, cancelPending, cancelShown);
        }

        public void SetNotification
            ( string name, DateTime when, bool cancelPrevious, params string[] messageParameters)
        {
            // Debug.LogFormat("Setting notification '{0}', when: {1}, cancel previous: {2}", name, when, cancelPrevious);

            var notificationDesc = notifications.Where (x => x.name == name).FirstOrDefault();
            if (notificationDesc == null)
            {
                Debug.LogErrorFormat("Unknown notification description '{0}'.", name);
                return;
            }

#if DEBUG_NOTIFICATION_TIMES
            when = DateTime.UtcNow + TimeSpan.FromSeconds(30.0);
#else
            if (deadZoneTime > 0 && when < DateTime.UtcNow + TimeSpan.FromSeconds(deadZoneTime))
            {
                Debug.LogFormat("Fire date of notification '{0}' is too soon ({1}), ignoring it.", name, when);
                return;
            }
#endif

            var title = Loc.Get(string.Format("Notification{0}Title", notificationDesc.name));

            var message = (messageParameters != null && messageParameters.Length > 0)
                ? Loc.Get(string.Format("Notification{0}Message", notificationDesc.name), messageParameters)
                : Loc.Get(string.Format("Notification{0}Message", notificationDesc.name));

            var notification = UnifiedNotificationsService.CreateLocalNotification();
            
            notification.title = title;
            notification.message = message;
            notification.useSound = notificationDesc.useSound;
            if (notificationDesc.useSound)
            {
                if (notificationDesc.customSoundName != string.Empty)
                    notification.customSoundName = notificationDesc.customSoundName;
            }

            var androidExtension = notification.GetExtension<INotificationExtensionAndroid>();

            androidExtension.useVibration = notificationDesc.useVibration;
            if (notificationDesc.useVibration)
            {
                if (notificationDesc.vibrationPattern != null)
                    androidExtension.vibrationPattern = notificationDesc.vibrationPattern;
            }

            androidExtension.useLights = notificationDesc.useLights;
            if (notificationDesc.useLights)
            {
                androidExtension.lightsColor = notificationDesc.lightsColor;
                androidExtension.lightsOn = notificationDesc.lightsOnMs;
                androidExtension.lightsOff = notificationDesc.lightsOffMs;
            }

            if (notificationDesc.smallIcon != string.Empty)
                androidExtension.smallIconResource = notificationDesc.smallIcon;

            if (notificationDesc.largeIcon != string.Empty)
                androidExtension.largeIconResource = notificationDesc.largeIcon;

/*
            Debug.LogFormat("Scheduling notification '{0}': Id: '{1}', When: {2}, Title: '{3}', Message: '{4}', Cancel previous: {5}."
                , name
                , notificationDesc.id
                , when
                , title
                , message
                , cancelPrevious);
*/

            UnifiedNotificationsService.ScheduleLocalNotification(notificationDesc.id, when, notification, cancelPrevious);
        }
        #endregion
    }
}