﻿using BridgeBinder;
using Logic.Estate.Facade;
using UnityEngine;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Equipment Manager Demo")]
    [BindBridgeInterface(typeof(ILogicEquipmentManager))]
    public class PresentationEquipmentManagerDemo : PresentationEquipmentManagerBase
    {
        #region Public (Inspector) fields.
        #endregion

        #region Private data.
        #endregion

        #region Bridge interface.
        #endregion

        #region Unity API.
        #endregion

        #region UnityEditor API.
        #endregion

        #region IPresentationEquipmentManager interface.
        #endregion

        #region Bridge events handlers.
        #endregion

        #region IEquipmentInteractionProvider interface.
        #endregion

        #region Private functions.
        #endregion
    }
} 