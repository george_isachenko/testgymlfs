﻿using System;
using System.Collections;
using System.Linq;
using BridgeBinder;
using Core;
using Data;
using Data.Estate.Info;
using Data.JuiceBar;
using Data.Person;
using Data.Person.Info;
using Data.Room;
using Logic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View;
using View.Actors;
using View.Actors.OverheadUI;
using View.CameraHelpers;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;
using System.Collections.Generic;
using Data.PlayerProfile;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Juice Bar Game")]
    public class PresentationJuiceBarGame
        : PresentationJuiceBarBase
    {
        #region Static data.
        private static readonly string uiFocusPointName = "_UI_Focus_Point";
        int leanTweenUpdater = -1;
        #endregion

        #region Types.
        #endregion

        #region Inspector fields.
        //------------------------------------------------------------------------
        [Header("Fruit tree Overhead UI")]

        [SerializeField]
        protected SingleActionIcon fruitTreeOverheadIconPrefab;

        [SerializeField]
        protected Sprite fruitTreeUnlockAvailableIcon;

        [SerializeField]
        protected Vector3 lockedFruitTreeOverheadUIShift;

        [SerializeField]
        protected Vector3 unlockedFruitTreeOverheadUIShift;

        //------------------------------------------------------------------------
        [Header("Fruit Tree Selection cursor settings")]

        [SerializeField]
        protected GameObject treeSelectionLockedCursorPrefab;

        [SerializeField]
        protected GameObject treeSelectionCursorPrefab;

        //------------------------------------------------------------------------
        [Header("Overhead UI")]

        [SerializeField]
        protected SingleActionIcon characterOverheadIconPrefab;

        //------------------------------------------------------------------------
        [Header("Camera movement")]

        [SerializeField]
        protected float cameraTimeMoveIn;

        [SerializeField]
        protected float cameraTimeMoveOut;

        [SerializeField]
        protected BarJuiceCloudView.Holder juicerOrderIcon = new BarJuiceCloudView.Holder();
        #endregion

        #region Private data.
        protected GameObject treeSelectionLockedCursorInstance;
        protected GameObject treeSelectionCursorInstance;
        protected IFruitTreeActor selectedTree;
        protected JuiceRecipesTableData currentRecipe;
        #endregion

        #region Bridge.
        #endregion

        #region Unity API.
        protected new void Awake ()
        {
            base.Awake();

            if (treeSelectionLockedCursorPrefab != null)
            {
                treeSelectionLockedCursorInstance = Instantiate(treeSelectionLockedCursorPrefab);
                treeSelectionLockedCursorInstance.transform.SetParent(transform, false);
                treeSelectionLockedCursorInstance.SetActive(false);
            }

            if (treeSelectionCursorPrefab != null)
            {
                treeSelectionCursorInstance = Instantiate(treeSelectionCursorPrefab);
                treeSelectionCursorInstance.transform.SetParent(transform, false);
                treeSelectionCursorInstance.SetActive(false);
            }
            
            if (actors != null)
            {
                foreach (var actor in actors.Values)
                {
                    actor.pointerClickEvent.AddListener (OnTreeActorClick);
                }
            }

            gui.hud.btnStorage.CanVisible_Callback = CanShowStorageButton;
            gui.hud.enablerJuiceBar.CanVisible_Callback = IsButtonVisible;
            gui.hud.btnJuiceBar.onClick.AddListener(OnButtonJuiceBarClick);
            gui.hud.btnJuiceBarTimer.textGetter = OnGetUnlockTimerString;
        }

        protected new void Start()
        {
            base.Start();

            gui.makeJuice.SubscribeOnDrop(OnDrop);
            gui.makeJuice.SubscribeOnDrag(OnDragBegin, OnDragEnd);

            foreach( var orderView in gui.makeJuice.orderViews)
            {
                orderView.OnClickCallback = OnClaimOrder;
                orderView.OnClickUnlockCallback = logic.OnOrderSlotUnlockClick;
            }

            gui.makeJuice.recipeDetails.ResourceCount = logic.playerProfile.storage.GetResourcesCount;
            gui.makeJuice.UpdateOrdersCallback = UpdateOrders;
            gui.makeJuice.OnUpdateContentOnShow_Callback = UpdateMakeJuice;
            //gui.makeJuice.OnShow_Callback = OnShowMakeJuice;
            //gui.makeJuice.OnHide_Callback = OnHideMakeJuice;

            clubInteractionProvider.onRoomActivatedEvent += OnRoomActivatedEvent;
            clubInteractionProvider.onRoomDeactivatedEvent += OnRoomDeactivatedEvent;

            characterInteractionProvider.onCharacterSelected += OnCharacterSelected;
            equipmentInteractionProvider.onEquipmentSelected += OnEquipmentSelected;

            gui.makeJuice.getOrderSkipCost = OnGetOrderSkipCost;
            gui.makeJuice.onShowByCityClick += OnShowByCityClick;

            gui.makeJuice.onStorageClick = ()=> { gui.makeJuice.Hide(); CameraController.instance.onTransformUnstashComplete += OnCameraUnstash; };
            gui.makeJuice.storageButtonEnableGetter = () => { return !questsInteractionProvider.isTutorialRunning; };

            logic.onJuiceAddInOrder += OnAddToOrder;

            juicerOrderIcon.Instantiate();
            juicerOrderIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(JuiceCloud.instance.transform.position);
            juicerOrderIcon.instance.btn.onClick.AddListener(OnClickJuiceCloud);
            UpdateJuiceCloud();

            leanTweenUpdater = LeanTween.delayedCall(1, UpdateJuiceCloud).setRepeat(-1).setDelay(1).id;
        }

        protected new void OnDestroy ()
        {
            LeanTween.cancel(leanTweenUpdater);

            gui.hud.btnJuiceBarTimer.textGetter = null;

            gui.makeJuice.onShowByCityClick -= OnShowByCityClick;
            gui.makeJuice.getOrderSkipCost = null;

            characterInteractionProvider.onCharacterSelected -= OnCharacterSelected;
            equipmentInteractionProvider.onEquipmentSelected -= OnEquipmentSelected;

            clubInteractionProvider.onRoomActivatedEvent -= OnRoomActivatedEvent;
            clubInteractionProvider.onRoomDeactivatedEvent -= OnRoomDeactivatedEvent;

            logic.onJuiceAddInOrder -= OnAddToOrder;

            gui.makeJuice.UpdateOrdersCallback = null;
            gui.makeJuice.recipeDetails.ResourceCount = null;
            gui.makeJuice.OnUpdateContentOnShow_Callback = null;

            foreach( var orderView in gui.makeJuice.orderViews)
            {
                orderView.OnClickCallback = null;
                orderView.OnClickUnlockCallback = null;
            }

            if (actors != null)
            {
                foreach (var actor in actors.Values)
                {
                    actor.pointerClickEvent.RemoveListener(OnTreeActorClick);
                }
                actors = null;
            }
        }
        #endregion

        #region Public API.

        public void UpdateJuiceCloud()
        {
            juicerOrderIcon.icon.visible = true;
            juicerOrderIcon.instance.items = GetOrdersReadyInfo();
        }

        public void OnClickJuiceCloud()
        {
            var list = GetOrdersReadyAsResourceSequence();
            LeanTween.delayedCall(juicerOrderIcon.instance.hideAnimTime, () => { gui.fx.AddFxResource(list); } );

            var items = GetOrdersReadyInfo();
            foreach(var item in items)    
            {
                for(int i = 0; i < item.count; i++)
                    logic.ClaimOrder(item.idx);
            }
        }

        public ResourceType[] GetOrdersReadyAsResourceSequence()
        {
            return logic.orders.Where(order => order.time.secondsToFinish == 0 ).Select(order => order.id).ToArray();
        }

        public List<StorageItemInfo> GetOrdersReadyInfo()
        {
            var result = new List<StorageItemInfo>();
            for(var i = 0; i < logic.orders.Count; i++)
            {
                var order = logic.orders[i];
                if (order.time.secondsToFinish == 0)
                {
                    var prevResult = result.Where(inf => inf.id == order.id).FirstOrDefault();

                    if (prevResult == null)
                    {
                        var info = new StorageItemInfo();
                        info.id = order.id;
                        info.sprite = View.UI.GUICollections.instance.storageIcons.items[(int)order.id].sprite;
                        info.count = 1;
                        info.idx = i;

                        result.Add(info);
                    }
                    else
                    {
                        prevResult.count++;
                    }
                }
            }

            return result.OrderByDescending(x => x.idx).ToList();
        }

        public override void UpdateMakeJuice()
        {
            if (selectedTree != null)
                SelectTree(null);

            var view = gui.makeJuice;
            var items = logic.availableJuices;

            gui.makeJuice.orderSlotsAvailable = logic.orderSlotsAvailable;
            //gui.makeJuice.availableJuices = logic.availableJuices;
            var list = logic.availableJuices;
            var count = list.Count;

            for(int i = 0; i < view.recipes.Count; i++)
            {
                view.recipes[i].gameObject.SetActive( i < count );

                if (i < count)
                {
                    var res = list[i];
                    view.recipes[i].icon = GUICollections.instance.storageIcons.items[(int)res].sprite;
                    view.recipes[i].attention = logic.IsJuiceNeeded(res);
                }

            }
            gui.makeJuice.orderSlotUnlockPrice = logic.GetOrderUnlockCost();//DataTables.instance.balanceData.JuiceBar.unlockOrderSlotStartPrice;
            
            UpdateOrders();
        }

        public override void BuyShortageOfFruits(JuiceRecipesTableData recipe)
        {
            var cost = recipe.cost;

            var costShortage = logic.playerProfile.GetShortageCost(cost);
            Assert.IsNotNull(costShortage);

            if (costShortage != null)
            {
                gui.confirmWithBucks.OnNeedResources2
                ( costShortage
                    , playerProfileInteractionProvider.storage.GetExtraBucksPrice(cost, null).value, 
                    false, true
                );

                gui.confirmWithBucks.OnOpenShop = () =>
                {
                    gui.shopUltimate.Show();
                    gui.shopUltimate.ShowBankBucks();
                    PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PresentationJuiceBar:BuyShortageOfFruits");
                };

                gui.confirmWithBucks.OnConfirm = () =>
                {
                    if (logic.BuyShortageOfFruits(recipe))
                    {
                        gui.makeJuice.Show();
                    }
                };
            }
        }

        public override void InitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int unlockLevel
            , Cost unlockCost
            , int numFruits
            , int maxFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft)
        {
            Assert.IsNotNull(juiceBarRoomView);

            IFruitTreeActor actor;
            if (actors != null && actors.TryGetValue(name, out actor))
            {
                actor.Init
                    ( this
                    , juiceBarRoomView.roomConductor
                    , juiceBarRoomView.actorsDisplayMode
                    , unlocked
                    , canUnlock
                    , unlockLevel
                    , unlockCost
                    , numFruits
                    , maxFruits
                    , regenTotalTime
                    , regenNextTimeLeft);

                UpdateTreeUI(actor);
            }
            else
            {
                Debug.LogWarningFormat("There's no Juice Bar tree actor with a name '{0}' in a RoomObject of JuiceBar.", name);
            }
        }

        public override void UpdateFruitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int numFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft)
        {
            IFruitTreeActor actor;
            if (actors != null && actors.TryGetValue(name, out actor))
            {
                actor.UpdateState (unlocked, canUnlock, numFruits, regenTotalTime, regenNextTimeLeft);

                UpdateTreeUI(actor);
                if (IsTreeSelected(actor))
                {
                    UpdateSkipPanel(actor);
                }
            }
            else
            {
                Debug.LogWarningFormat("There's no Juice Bar tree actor with a name '{0}' in a RoomObject of JuiceBar.", name);
            }
        }

        public override void SelectFruitTreeByIndex(int index)
        {
            var juiceBarRoomView = clubInteractionProvider.GetRoomOfType(RoomType.JuiceBar);

            Assert.IsNotNull(juiceBarRoomView);
            Assert.IsNotNull(juiceBarRoomView.roomObject);

            if (index < 0)
            {
                SelectTree(null);
            }
            else if (juiceBarRoomView != null && juiceBarRoomView.roomObject)
            {
                var fruitTrees = juiceBarRoomView.roomObject.GetComponentsInChildren<IFruitTreeActor>(true);
                if (fruitTrees.Length > index)
                    SelectTree(fruitTrees[index]);
            }
        }

        public override void ShowMakeJuiceUI () // Use by tutorial, invoked from logic.
        {
            MoveCameraToJuiceBar();

            LeanTween.delayedCall(cameraTimeMoveIn, OnCameraStop);
        }
        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("onJuicerTransfer")]
        private void OnJuicerTransfer(int personId)
        {
            ICharacterActor actor = characterInteractionProvider.GetCharacterActor(personId);
            Assert.IsNotNull(actor);
            if (actor != null)
            {
                var availableJuices = logic.availableJuices;

                Assert.IsTrue(availableJuices.Count > 0);
                if (availableJuices.Count > 0 && characterOverheadIconPrefab != null)
                {
                    var possibleDesire = logic.availableJuices.ElementAtOrDefault(UnityEngine.Random.Range(0, availableJuices.Count));

                    var characterOverheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(characterOverheadIconPrefab);
                    characterOverheadIcon.instance.iconSprite = GUICollections.instance.storageIcons.items[(int)possibleDesire].sprite;
                    characterOverheadIcon.icon.autoHideDelay = 3.0f;

                    // By using custom WorldPositionProvider we ignoring Overhead UI visibility changes when room active state changes.
                    var customWorldPositionProvider = new ActorPositionWorldPositionProvider<int>(actor, true, true);
                    characterOverheadIcon.icon.worldPositionProvider = customWorldPositionProvider;

                    actor.SetOverheadUIIcon(characterOverheadIcon, false);
                }
            }
        }

        #endregion

        #region IFruitTreeActorSharedDataProvider interface.
        public override GameObject GetCursorObject (IFruitTreeActor actor)
        {
            Assert.IsNotNull(actor);

            return actor.unlocked ? treeSelectionCursorInstance : treeSelectionLockedCursorInstance;
        }
        #endregion

        #region Event handlers.
        private void OnShowByCityClick()
        {
            if (!questsInteractionProvider.isTutorialRunning && logic.canUseMyFeature.canUse)
            {
                ShowMakeJuiceUI();
            }
        }

        void OnCameraStop()
        {
            gui.makeJuice.Show();
        }

        void OnDrop()
        {
            StartCoroutine(MakeJuice());
        }

        public int OnGetOrderSkipCost(int totalSeconds)
        {
            return logic.GetOrderSkipCost(totalSeconds);
        }

        void OnDragBegin()
        {
            var idx = gui.makeJuice.GetIndexOfDraggable(JuiceDragHandler.itemBeingDragged);
            JuiceDragHandler.itemDraggedIndex = idx;

            gui.makeJuice.OnDragBegin(idx);

            if (idx >= 0)
            {
                currentRecipe = DataTables.instance.juiceRecipes[idx];
                gui.makeJuice.recipeDetails.recipe = currentRecipe;
                gui.makeJuice.recipeDetails.inStorage = logic.playerProfile.storage.GetResourcesCount(currentRecipe.id);
                gui.makeJuice.recipeDetails.juiceName = Loc.Get(currentRecipe.loc_id);
                gui.makeJuice.recipeDetails.time = PrintHelper.GetTimeString(currentRecipe.make_time);

                gui.makeJuice.UpdateArrows(logic.orders);
            }
        }

        void OnDragEnd()
        {
            gui.makeJuice.OnDragEnd();
            gui.makeJuice.UpdateArrows(logic.orders);
        }

        void OnRoomActivatedEvent (string roomName)
        {
            if (roomName == juiceBarRoomView.roomName)
            {
                UpdateActorsDisplayMode(roomName);
            }
        }

        void OnRoomDeactivatedEvent (string roomName)
        {
            if (roomName == juiceBarRoomView.roomName)
            {
                UpdateActorsDisplayMode(roomName);
            }

            SelectTree(null);
        }

        private void OnCharacterSelected (IPersonInfo personInfo, bool focus)
        {
            SelectTree(null);
        }

        private void OnEquipmentSelected (int id, IEquipmentInfo equipmentInfo)
        {
            SelectTree(null);
        }

        void OnTreeActorClick (string actorId, PointerEventData eventData)
        {
            IFruitTreeActor actor;
            if (actorId != null && actors != null && actors.TryGetValue(actorId, out actor))
            {
                if (juiceBarRoomView.isActivated)
                {
                    characterInteractionProvider.SelectCharacter(PersonData.InvalidId);
                    equipmentInteractionProvider.DeselectEquipmentRequest();

                    SelectTree(actor);

                    if (actor.unlocked)
                    {
                    }
                    else
                    {
                        if (actor.canUnlock)
                        {
                        
                        }
                        else
                        {
                            gui.confirmWnd.OnCustom
                                ( Loc.Get("JuiceBarUnlockFruitTreeHeader", Loc.Get(string.Format("JuiceBarFruitTree_{0}", actor.id)))
                                , Loc.Get("JuiceBarUnlockFruitTreeMessage", actor.unlockLevel));
                        }
                    }
                }
                else if (logic.canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse)
                {
                    clubInteractionProvider.TransitionToRoomRequest(juiceBarRoomView.roomName);
                }
            }
        }

        public void MoveCameraToJuiceBar()
        {
            var target = juiceBarRoomView.viewObject.transform.Find(uiFocusPointName);
            if (target != null)
            {
                CameraController.instance.StashCameraTransformAndMoveTo
                    ( target.transform.position
                    , target.transform.rotation.eulerAngles
                    , cameraTimeMoveIn
                    , cameraTimeMoveOut);
            }
        }

/*
        public void OnHideMakeJuice()
        {
            var target = JuiceBarCameraSettings.instance;

            CameraController.instance.UnstashCameraTransform(target.timeMoveOut);
        }
*/

        string OnGetUnlockTimerString()
        {
            var canUseResult = logic.canUseMyFeature;

            if (canUseResult.type == CanUseFeatureResult.ResultType.Locked)
            {
                gui.hud.btnJuiceBarBadge = true;
                return Loc.Get("idNew");
            }
            else if (canUseResult.type == CanUseFeatureResult.ResultType.Pending)
            {
                var timer = logic.GetUnlockTimer();
                if (timer != null)
                {
                    if (timer.secondsToFinish < 1)
                    {
                        gui.hud.btnJuiceBarBadge = true;
                        return Loc.Get("idReady");
                    }
                    else
                    {
                        gui.hud.btnJuiceBarBadge = false;
                        return PrintHelper.GetTimeString((int)timer.secondsToFinish);
                    }
                }
            }

            return string.Empty;
        }
        #endregion

        #region Private methods.
        void UpdateOrders()
        {
            logic.CalcOrders();
            gui.makeJuice.orders = logic.orders;
        }

        void SelectTree (IFruitTreeActor newSelection, bool canDeselect = true)
        {
            var same = ReferenceEquals(newSelection, selectedTree);
            if (selectedTree != null && same && canDeselect)
            {
                if (selectedTree != null)
                {
                    selectedTree.SetSelection(false);
                    selectedTree = null;

                    OnTreeSelectionChanged(selectedTree);
                }
            }
            else if (!same)
            {
                if (selectedTree != null)
                {
                    selectedTree.SetSelection(false);
                    var tree = selectedTree;
                    selectedTree = null;
                    UpdateTreeUI(tree);
                }

                if (newSelection != null)
                {
                    newSelection.SetSelection(true);
                    selectedTree = newSelection;
                    UpdateTreeUI(newSelection);
                }
                OnTreeSelectionChanged(selectedTree);
            }

            gui.hud.UpdateHUD();
        }

        void OnTreeSelectionChanged (IFruitTreeActor actor)
        {
            if (selectedTree != null)
            {
                UpdateSkipPanel(actor);
                return;
            }
            else
            {
                gui.hud.skipPanel.Hide();
            }
        }

        private void UpdateSkipPanel (IFruitTreeActor actor)
        {
            if (actor.numFruits < actor.maxFruits && actor.regenEndNextTime > DateTime.UtcNow)
            {
                gui.hud.skipPanel.Display
                    ( actor.regenTotalTime
                    , actor.regenEndNextTime
                    , getSkipCostCallback: (int totalSeconds) =>
                    {
                        return DynamicPrice.GetSkipCostNoFree(totalSeconds);
                    }
                    , onClick: () =>
                    {
                        logic.SkipFruitTreeRegen(actor.id);
                    }
                    , onHideCheck: () =>
                    {
                        return !IsTreeSelected(actor);
                    }
                    ,onEnableButtonCheckCallback: null
                    ,skipName: Loc.Get("idGrowingFruit"));
            }
            else
            {
                gui.hud.skipPanel.Hide();
            }
        }

        void UpdateTreeUI (IFruitTreeActor actor)
        {
            if (actor.unlocked)
            {
                if (actor.numFruits > 0)
                {
                    var overheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(fruitTreeOverheadIconPrefab);

                    overheadIcon.instance.labelText = actor.numFruits.ToString();
                    overheadIcon.instance.iconSprite = actor.fruitTypeSprite;
                    overheadIcon.instance.onClick.AddListener(() =>
                    {
                        var collected = logic.CollectFruitsFromTree(actor.id);

                        if ( collected > 0)
                        {
                            var fruitTreeDefinition = DataTables.instance.fruitTrees.FirstOrDefault(x => x.name == actor.id);

                            if (fruitTreeDefinition != null)
                            {
                                var resId = fruitTreeDefinition.type;
                                gui.fx.AddFxResource(resId, collected);
                            }
                            else
                                Debug.LogError("UpdateTreeUI fruitTreeDefinition == null");
                                
                        }
                        else
                        {
                            //SelectTree(null);
                            playerProfileInteractionProvider.WarnStorageIsFull(true);
                        }
                    });
                    overheadIcon.icon.frontIcon = IsTreeSelected(actor);
                    overheadIcon.icon.worldPositionProvider = new ActorPositionWorldPositionProvider<string>(actor, unlockedFruitTreeOverheadUIShift, false, true);
                    actor.SetOverheadUIIcon(overheadIcon);
                }
                else
                {
                    actor.RemoveOverheadUIIcon();
                }
            }
            else
            {
                if (actor.canUnlock)
                {
                    var overheadIcon = new OverheadIconInstanceHolder<SingleActionIcon>(fruitTreeOverheadIconPrefab);

                    overheadIcon.instance.labelText = string.Empty;
                    overheadIcon.instance.iconSprite = fruitTreeUnlockAvailableIcon;

                    overheadIcon.instance.onClick.AddListener(() =>
                    {
                        OnUnlockTreeClick(actor);
                    });
                    overheadIcon.icon.frontIcon = IsTreeSelected(actor);
                    overheadIcon.icon.worldPositionProvider = new ActorPositionWorldPositionProvider<string>(actor, lockedFruitTreeOverheadUIShift, false, true);
                    actor.SetOverheadUIIcon(overheadIcon);
                }
                else
                {
                    actor.RemoveOverheadUIIcon();
                }
            }
        }

        private void OnUnlockTreeClick (IFruitTreeActor actor)
        {
            if (!ReferenceEquals(selectedTree, actor))
                SelectTree(actor);

            gui.confirmWnd.OnBuyConfirm
                (actor.unlockCost
                , Loc.Get("JuiceBarUnlockFruitTreeHeader", Loc.Get(string.Format("JuiceBarFruitTree_{0}", actor.id)))
                , Loc.Get("JuiceBarUnlockFruitTreeAvailableMessage")
                ,actor.fruitTypeSprite);

            gui.confirmWnd.OnConfirm = () =>
            {
                SelectTree(null); // Must be re-selected after unlock, to change cursor type.

                if (logic.TryUnlockTree(actor.id, false))
                {
                    SelectTree(actor);
                }
                else
                {
                    var costShortage = playerProfileInteractionProvider.GetShortageCost(actor.unlockCost);
                    if (costShortage != null)
                    {
                        gui.confirmWithBucks.OnNeedResources2
                            (costShortage
                            , (costShortage.type == Cost.CostType.CoinsOnly)
                                ? Cost.CoinsToFitBucks(costShortage.value)
                                : playerProfileInteractionProvider.storage.GetExtraBucksPrice(actor.unlockCost, null).value
                            , true
                            , true);

                        gui.confirmWithBucks.OnOpenShop = () =>
                        {
                            gui.shopUltimate.Show();

                            if (costShortage.type == Cost.CostType.CoinsOnly)
                            {
                                gui.shopUltimate.ShowBankCoins();
                            }
                            else
                            {
                                gui.shopUltimate.ShowBankBucks();
                            }
                            PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PresentationJuiceBar:UnlockFruitTree");
                        };

                        gui.confirmWithBucks.OnConfirm = () =>
                        {
                            if (logic.TryUnlockTree(actor.id, true))
                            {
                                SelectTree(actor);
                            }
                            else
                            {
                                gui.shopUltimate.Show();
                                gui.shopUltimate.ShowBankBucks();
                                PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PresentationJuiceBar:UnlockFruitTree");
                            }
                        };
                    }
                }
            };

/*
            var viewData = new GymUnlockViewData
                ( actor.id
                , Loc.Get("JuiceBarUnlockFruitTreeHeader", Loc.Get(string.Format("JuiceBarFruitTree_{0}", actor.id)))
                , Loc.Get("JuiceBarUnlockFruitTreeAvailableMessage")
                , actor.unlockCost
                , playerProfileInteractionProvider);

            gui.gymUnlock.UpdateView(viewData, (string name) =>
            {
                if (!logic.TryUnlockTree(actor.id, true))
                {
                    gui.confirmWnd.OnNeedMoney(playerProfileInteractionProvider.storage.GetExtraBucksPrice(actor.unlockCost, null));
                    gui.confirmWnd.OnConfirm = () =>
                    {
                        gui.shopUltimate.Show();
                        gui.shopUltimate.ShowBankBucks();
                    };
                }

/ *
                if (!logic.TryUnlockTree(actor.id, false))
                {
                    var costShortage = playerProfileInteractionProvider.GetShortageCost(actor.unlockCost);
                    if (costShortage != null)
                    {
                        gui.confirmWithBucks.OnNeedResources2
                            ( costShortage
                            , playerProfileInteractionProvider.storage.GetExtraBucksPrice(actor.unlockCost, null).value
                            , false
                            , true);

                        gui.confirmWithBucks.OnOpenShop = () =>
                        {
                            gui.shopUltimate.Show();
                            gui.shopUltimate.ShowBankBucks();
                            Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PresentationJuiceBar:UnlockFruitTree");
                        };

                        gui.confirmWithBucks.OnConfirm = () =>
                        {
                            logic.TryUnlockTree(actor.id, true);
                        };
                    }
                }
* /
            });
            gui.gymUnlock.Show();

*/
        }

        bool CanShowStorageButton()
        {
            if(clubInteractionProvider.activeRoom != null)
                return clubInteractionProvider.activeRoom.roomConductor.roomSetupDataInfo.roomType == RoomType.JuiceBar;

            return false;
        }

        IEnumerator MakeJuice()
        {
            yield return new WaitForEndOfFrame();
            if (logic.Make(currentRecipe))
                UpdateMakeJuice();

            currentRecipe = null;
        }

        bool IsTreeSelected(IFruitTreeActor actor)
        {
            return ReferenceEquals(actor, selectedTree);
        }

        bool IsButtonVisible()
        {
            return (logic.canUseMyFeature.type == CanUseFeatureResult.ResultType.Locked ||
                   logic.canUseMyFeature.type == CanUseFeatureResult.ResultType.Pending) &&
                   characterInteractionProvider.selectedCharacterId == PersonData.InvalidId;
        }

        void OnButtonJuiceBarClick()
        {
            if (CameraController.instance.isAutoMove)
                return;

            var tRoom = clubInteractionProvider.GetRoomOfType(Data.Room.RoomType.JuiceBar);
            var tPos = tRoom.homeFocusPosition;
            CameraController.instance.AutoMoveTo(tPos.x, tPos.z, (bool v) => { if (v) tRoom.StartTransitionToRoom(); });
        }

        void OnClaimOrder(int index)
        {
            if (logic.ClaimOrder(index))
                gui.makeJuice.OnClaimJuice(index);
        }

        void OnAddToOrder()
        {
            gui.makeJuice.UpdateStorageInfo();
        }

        void OnCameraUnstash()
        {
            gui.storage.Show();
            CameraController.instance.onTransformUnstashComplete -= OnCameraUnstash;
        }
        #endregion
    }
}
