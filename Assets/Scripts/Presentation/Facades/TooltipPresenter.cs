using System.Linq;
using Data;
using Data.PlayerProfile;
using Presentation.Base;
using UnityEngine;
using View.UI;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Tooltip Presenter")]
    public class TooltipPresenter
        : BasePresentationFacade
    {
        private TooltipView view => gui.tooltip;

        private void Awake()
        {
            SubscribeScrollView(gui.buyWithResources.costView);
            SubscribeScrollView(gui.gymUnlock.costView);
        }

        private void SubscribeScrollView(CostViewSubList costView)
        {
            costView.onShowHintRequest += ShowTooltipForResourceNear;
            costView.onHideTooltipRequest += Hide;
        }

        public void Hide()
        {
            view.Hide();
        }

        public void Show(string title, string desc, RectTransform target)
        {
            view.title = title;
            view.description = desc;
            view.Show(target);
        }

        public void ShowTooltipForResourceNear(ResourceType resourceType, RectTransform target)
        {
            var info = DataTables.instance.resourcesInfo.FirstOrDefault(i => i.resourceId == resourceType);
            if (info == null)
            {
                Debug.LogErrorFormat("Tooltip: can't find info for resourece {0}, skip showing.", resourceType);
                return;
            }

            view.title = Loc.Get(info.resourceNameLocId);
            view.description = Loc.Get(info.resourceDescriptiontLocId);
            view.Show(target);
        }
    }
}