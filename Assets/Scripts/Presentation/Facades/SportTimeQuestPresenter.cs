using System;
using BridgeBinder;
using Data;
using Data.Person;
using Data.Room;
using Logic.Persons;
using Logic.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Data;
using View.UI.OverheadUI.Components;
using Presentation.Base;
using Logic.Persons.Events;
using Logic;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sport Time Quest")]
    [BindBridgeInterface(typeof (SportTimeQuestLogic))]
    public class SportTimeQuestPresenter
        : BasePresentationFacade<SportTimeQuestLogic>
    {
        public static readonly string timeQuestAgentTag = "TimeQuestAgent";
        private static readonly string iconAttachmentObjectName = "icon_attachment";

        public SingleActionIcon.Holder overheadIcon;

        private SportTimeQuestView view
        {
            get { return gui.sportTimeQuest; }
        }

        [BindBridgeSubscribe]
        private void Subscribe(SportTimeQuestLogic timeQuestLogic)
        {
            logic = timeQuestLogic;

            logic.onQuestCompleted += UpdateView;
            logic.onQuestClaimed += UpdateView;
            logic.onRoomSizeUpdated += OnRoomUpdated;
            logic.onLevelUp += OnLevelUp;

            view.onHelpButtonPressed += HelpResponce;
            // view.onBuyCompleteButtonPressed += BuyComplete;
            view.onClaimButtonPressed += Claim;
            view.onTimerFinished += UpdateView;
            view.onSkipWaitingForNewQuest += SkipTimer;
            view.OnShow_Callback += UpdateView;
            view.onShowByCityClick += OnShowByCityClick;
        }

        private void CharacterBirthEventOnEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                UpdateView();
        }

        private void SportsmanTypeChangedEventOnEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            UpdateView();
        }

        void Start()
        {
            if (overheadIcon != null)
            {
                var cityObject = GameObject.FindGameObjectWithTag (timeQuestAgentTag);
                Assert.IsNotNull(cityObject);
                if (cityObject != null)
                {
                    var iconAttachment = cityObject.transform.Find(iconAttachmentObjectName);
                    if (iconAttachment != null)
                    {
                        var iconAttachmentObject = iconAttachment.gameObject;

                        overheadIcon.Instantiate();
                        overheadIcon.instance.onClick.AddListener (OnOverheadUIClick);
                        overheadIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(iconAttachmentObject.transform.position);
                        overheadIcon.icon.owner = this;

                        var overheadUIData = new OverheadUIDataBuildings();
                        overheadUIData.state = OverheadUIDataBuildings.State.TimeQuestAgent;
                        overheadUIData.ApplyData (overheadIcon.instance);
                    }
                }
            }
            else
            {
                Debug.LogWarningFormat("{0}: Missing Overhead UI Icon prefab.", GetType().Name);
            }

            view.OnShow_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event += SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event += CharacterBirthEventOnEvent;
            };
            view.OnHide_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event -= SportsmanTypeChangedEventOnEvent;
                CharacterBirthEvent.Event -= CharacterBirthEventOnEvent;
            };

            gui.hud.SetTimeQuestsActivityGetter(()=>logic.canUseMyFeature.canUse);

            UpdateOverheadUI();
        }

        private void OnShowByCityClick()
        {
            var useCase = logic.canUseMyFeature;
            switch (useCase.type)
            {
                case CanUseFeatureResult.ResultType.DependencyLocked:
                    gui.confirmWnd.OnCustom(Loc.Get("timeQuestLockedHeader"), Loc.Get("timeQuestLockedMessage"));
                    break;
                case CanUseFeatureResult.ResultType.LockedNeededLevel:
                    gui.confirmWnd.OnCustom(Loc.Get("timeQuestLockedHeader"),
                        Loc.Get("timeQuestLockedByLevelMessage", useCase.level));
                    break;
                case CanUseFeatureResult.ResultType.CanUse:
                    view.Show();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnRoomUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupData)
        {
            if (roomSetupData.roomType == RoomType.SportClub)
            {
                UpdateOverheadUI();
            }
        }

        private void OnLevelUp(int obj)
        {
            UpdateOverheadUI();
        }

        private void SkipTimer()
        {
            try
            {
                logic.SkipTimer();
                UpdateView();
            }
            catch (Exception e)
            {
                Debug.LogError("Somethong wrong with skipping timer of time quest: " + e);
            }
        }

        private void UpdateView()
        {
            var viewData = new SportTimeQuestInfoViewData(logic.quest, logic.sportsmanManager);
            view.SetQuestInfo(viewData);
        }

        private void Claim()
        {
            try
            {
                logic.Claim();
            }
            catch (Exception e)
            {
                Debug.LogError("Somethong wrong with claiming of time quest: " + e);
            }
        }

/*
        private void BuyComplete()
        {
            try
            {
                logic.BuyCompleteAndClaim();
            }
            catch (Exception e)
            {
                Debug.LogError("Somethong wrong with buying of time quest: " + e);
            }
        }
*/

        private void HelpResponce()
        {
            Debug.Log(GetType().Name + ": Help!");
        }

        void UpdateOverheadUI()
        {
            if (overheadIcon != null && overheadIcon.icon != null)
            {
                overheadIcon.icon.visible = logic.canUseMyFeature.canUse;
            }
        }

        void OnOverheadUIClick()
        {
            OnShowByCityClick();
        }
    }
}