using System;
using System.Linq;
using BridgeBinder;
using Data.Person;
using Data.Room;
using Logic.Persons;
using Logic.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Data;
using View.UI.OverheadUI.Components;
using View.Actions;
using View.PrefabResources;
using Presentation.Base;
using View.UI;
using Data;
using Presentation.Providers;
using View.UI.Base;
using Logic.Persons.Events;
using Logic.Quests.SportQuest.SportLaneQuest;
using Logic.Quests.SportQuest.SportLaneQuest.Facade;
using System.Collections;
using Data.Sport;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sport Lane Quest")]
    [BindBridgeInterface(typeof(SportLaneQuestLogic))]
    public class SportLaneQuestPresenter
        : BasePresentationFacade<SportLaneQuestLogic>
    {
        static readonly string  sportLaneQuestBoardTag = "SportLaneQuestBoard";
        static readonly string  iconAttachmentObjectName = "icon_attachment";

        int                     selectedQuestIndex = 0;
        LeanTweenTransform[]    questPlanksOnBoard;
        IEnumerator             updateQuestViewJob;

        LaneQuestLogic          laneQuest               { get { return logic.currentLaneQuest; } }
        SportLaneQuestView      view                    { get { return gui.sportLaneQuest; } }

        public SingleActionIcon.Holder overheadIcon;

        private SingleSportLaneQuest selectedQuest
        {
            get
            {
                var quest = logic.currentLaneQuest.GetQuest(selectedQuestIndex);
                if (quest == null)
                {
                    selectedQuestIndex = 0;
                    quest = logic.currentLaneQuest.GetQuest(selectedQuestIndex);
                }
                return quest;
            }
        }

        [BindBridgeSubscribe]
        private void Subscribe(SportLaneQuestLogic laneQuestLogic)
        {
            logic = laneQuestLogic;

            view.onQuestSelected += OnQuestSelected;
            view.onClaimButtonPressed = Claim;
            view.OnShow_Callback += ScheduleUpdateQuestView;
            view.onShowByCityClick += OnShowByCityClick;
            view.onSkipCooldown.AddListener(TrySkipCooldown);

            logic.onQuestsUpdated += ScheduleUpdateQuestView;
            logic.onRoomSizeUpdated += OnRoomUpdated;
        }

        [BindBridgeUnsubscribe]
        private void Unsubscribe(SportLaneQuestLogic laneQuestLogic)
        {
            view.onQuestSelected -= OnQuestSelected;
            view.OnShow_Callback -= ScheduleUpdateQuestView;
            view.onShowByCityClick -= OnShowByCityClick;
            view.onSkipCooldown.RemoveListener(TrySkipCooldown);

            logic.onQuestsUpdated -= ScheduleUpdateQuestView;
            logic.onRoomSizeUpdated -= OnRoomUpdated;

            logic = null;
        }

        private void OnCharacterBirthEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                ScheduleUpdateQuestView();
        }

        private void OnSportsmanTypeChangedEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            ScheduleUpdateQuestView();
        }

        private void OnCharacterLeavingRoomEvent (Person person, bool kicked)
        {
            if (person is PersonSportsman)
                ScheduleUpdateQuestView();
        }

        private void OnCharacterEnteredRoomEvent (Person person)
        {
            if (person is PersonSportsman)
                ScheduleUpdateQuestView();
        }

        void Start()
        {
            var cityObject = GameObject.FindGameObjectWithTag (sportLaneQuestBoardTag);
            Assert.IsNotNull(cityObject);

            if (cityObject != null)
            {
                if (overheadIcon != null)
                {
                    var iconAttachment = cityObject.transform.Find(iconAttachmentObjectName);
                    if (iconAttachment != null)
                    {
                        overheadIcon.Instantiate();
                        overheadIcon.instance.onClick.AddListener (OnOverheadUIClick);
                        overheadIcon.icon.worldPositionProvider = new FixedWorldPositionProvider(iconAttachment.position);
                        overheadIcon.icon.owner = this;
                    }
                }
                else
                {
                    Debug.LogWarningFormat("{0}: Missing Overhead UI Icon prefab.", GetType().Name);
                }

                var boardPlankObjects = cityObject.GetComponent<GameObjectsArray>();
                if (boardPlankObjects != null && boardPlankObjects.array.Length > 0)
                {
                    questPlanksOnBoard = new LeanTweenTransform[boardPlankObjects.array.Length];

                    for (int i = 0; i < questPlanksOnBoard.Length; i++)
                    {
                        var leanTweenTransform = boardPlankObjects.array[i].GetComponent<LeanTweenTransform>();
                        if (leanTweenTransform != null)
                        {
                            questPlanksOnBoard[i] = leanTweenTransform;
                        }
                    }
                }
            }

            view.OnShow_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event += OnSportsmanTypeChangedEvent;
                CharacterBirthEvent.Event += OnCharacterBirthEvent;
                CharacterEnteredRoomEvent.Event += OnCharacterEnteredRoomEvent;
                CharacterLeavingRoomEvent.Event += OnCharacterLeavingRoomEvent;
            };

            view.OnHide_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event -= OnSportsmanTypeChangedEvent;
                CharacterBirthEvent.Event -= OnCharacterBirthEvent;
                CharacterEnteredRoomEvent.Event -= OnCharacterEnteredRoomEvent;
                CharacterLeavingRoomEvent.Event -= OnCharacterLeavingRoomEvent;
            };

            UpdateOverheadUI();
            UpdateBoardPlanks();

            logic.OnExerciseFinish += delegate () { UpdateBoardPlanks(); UpdateOverheadUI(); };
            logic.SetAutoFinishCooldownCondition(() => gui.sportLaneQuest.isVisible);

#if DEBUG
            gui.dbgView.btnSkipLaneQuest.onClick.AddListener(DebugSkip);

            gui.dbgView.btnOneMinuteLeft.onClick.AddListener(() =>
            {
                logic.currentLaneQuest.currentStateTimer.secondsToFinish = 60;
            });
#endif
        }

        private void OnShowByCityClick()
        {
            if (logic.isClubInEditMode)
                return;

            if (logic.isAnyTutorialRunning && !logic.isLaneQuestTutorialRunning)
                return;

            if (logic.canUseMyFeature)
            {
                view.Show();
            }
            else
            {
                gui.confirmWnd.OnCustom(Loc.Get("laneQuestLockedHeader"), Loc.Get("laneQuestLockedMessage"));
            }
        }

        private void TrySkipCooldown()
        {
            try
            {
                logic.SkipCooldown();
                    //gui.confirmWnd.OnNeedMoney(logic.GetSkipCooldownCost());
            }
            catch (Exception e)
            {
                Debug.LogError("Something wrong with skipping coolodwn of lane quest: " + e);
            }
        }

        protected override UIBaseView GetView()
        {
            return view;
        }

        private void Claim()
        {
            if (logic.currentLaneQuest != null)
            {
                var quest = logic.currentLaneQuest.GetQuest(selectedQuestIndex);
                if (quest != null)
                {
                    var sportsmenShortageList = quest.GetSporsmenShortage(logic.sportsmen);

                    if (sportsmenShortageList != null && sportsmenShortageList.Length > 0)
                    {
                        EvaluateSportsmenPurchase(sportsmenShortageList, 
                            (cost) =>
                            {
                                PerformClaim(sportsmenShortageList);
                                GoBackToMyView();
                            },
                            (cost) =>
                            {
                                logic.playerProfile.SendNotEnoughMoneyEvent(cost, true);
                            },
                            null
                        );
                    }
                    else
                    {
                        PerformClaim(null);
                    }
                }
                else
                {
                    Debug.LogErrorFormat("logic.currentLaneQuest.GetQuest() returned NULL! selectedQuestIndex = ", selectedQuestIndex);
                }
            }
            else
            {
                Debug.LogError("logic.currentLaneQuest is NULL!");
            }
        }

        private void PerformClaim(SportsmanCostUnit[] ignoredShortage)
        {
            try
            {
                logic.Claim(selectedQuestIndex, ignoredShortage);
            }
            catch (NeedMoreStorageException)
            {
                playerProfileInteractionProvider.WarnStorageIsFull(true);
            }
            catch (Exception e)
            {
                Debug.LogError("Something wrong with claiming of lane quest: " + e);
            }
        }

/*
        private void BuyComplete()
        {
            try
            {
                logic.BuyCompleteAndClaim(selectedQuestIndex);
                ScheduleUpdateQuestView();
            }
            catch (Exception e)
            {
                Debug.LogError("Something wrong with buying of lane quest: " + e);
            }
        }
*/

/*
        private void HelpResponce()
        {
            logic.AddRandomNeededSportsman(selectedQuest, logic.sportsmanManager);
            ScheduleUpdateQuestView();
        }
*/

        private void OnQuestSelected(int questindex)
        {
            if (selectedQuestIndex != questindex)
            {
                if (view.SetSelectedQuest(questindex))
                {
                    selectedQuestIndex = questindex;
                    ScheduleUpdateQuestView();
                }
            }
        }

        private void OnRoomUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupData)
        {
            if (roomSetupData.roomType == RoomType.SportClub)
            {
                UpdateOverheadUI();
            }
        }

        public void TakeRewardFX(float seconds)
        {
            var reward = logic.sequenceReward;
            var canvGroup = view.sequenceRewardCanvas;
            canvGroup.alpha = 1;

            LeanTween.alphaCanvas(canvGroup, 0, seconds);

            UiFxSprite fx = UiFxSprite.Platinum;
            var fxCount = 0;

            if (reward.type == Cost.CostType.BucksOnly)
            {
                fx = UiFxSprite.Bucks;
                fxCount = reward.value;
            }
            else
            {
                fxCount = reward.resources[Data.PlayerProfile.ResourceType.Platinum];
            }
                 

            gui.fx.AddFx(fx, fxCount, view.sequenceRewardPos);
        }

        public void UpdateReward()
        {
            var reward = logic.sequenceReward;
            view.sequenceRewardOn   = laneQuest.state == LaneQuestState.WaitForNewQuests && reward != null;

            if (reward != null)
            {
                view.sequenceReward = reward;
                logic.DestroyRewardInSeconds(3);
            }
        }

        private void ScheduleUpdateQuestView()
        {
            if (updateQuestViewJob == null)
            {
                updateQuestViewJob = UpdateQuestViewJob();
                if (updateQuestViewJob != null)
                    StartCoroutine(updateQuestViewJob);
            }
        }

        private IEnumerator UpdateQuestViewJob()
        {
            yield return null;

            try
            {
                logic.FinishCooldownStateIfCan();

                var questsList = laneQuest.GetQuestList();
                var questsCompleteStatus = questsList.Select
                    ( q => q.claimed
                        ? LaneQuestNameItem.QuestState.Claimed
                        : (logic.GetSportsmenShortage(q) <= 0) ? LaneQuestNameItem.QuestState.Completed : LaneQuestNameItem.QuestState.New)
                    .ToArray();

                view.progressOn = laneQuest.state != LaneQuestState.WaitForNewQuests;
                view.progress = logic.progress;

                UpdateReward();

                /*
                Debug.LogFormat(
                    "--- Update lane quest view ---\n lane quest state: {0}\ncurrent quests count: {1}\nselected quest index: {2}",
                    laneQuest.state,
                    questsList.Count,
                    selectedQuestIndex);
                    */
            
                var sq = selectedQuest;
                if (selectedQuest != null)
                {
                    var viewData = new SportLaneQuestInfoViewData(sq, logic.sportsmanManager, laneQuest)
                
                    {
                        isBuyButtonInteractable = !logic.isTutorialQuest,
                    };

                    switch (laneQuest.state)
                    {
                        case LaneQuestState.Tutorial:
                            view.SetQuestNames(questsList.Select(q => Loc.Get(q.questTypeId)).ToArray(), selectedQuestIndex);
                            viewData.bubbleText = Loc.Get("laneQuestLetsdoit");
                            break;
                        case LaneQuestState.ProgressAccumulation:
                            view.SetQuestNames(questsList.Select(q => Loc.Get(q.questTypeId)).ToArray(), selectedQuestIndex);
                            viewData.bubbleText = Loc.Get("laneQuestEventsDuration");
                            break;
                        case LaneQuestState.WaitForNewQuests:
                            view.HideQuestNames();
                            viewData.bubbleText = laneQuest.isLastProgressAccumulationFailed ? Loc.Get("laneQuestFailedInTime") : Loc.Get("laneQuestSuccesfulyParticipated");
                            break;
                    }
                    view.SetQuestComplete(questsCompleteStatus);

                    view.SetQuestInfo(viewData);

                    UpdateBoardPlanks();
                    UpdateOverheadUI();
                }
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            Assert.IsNotNull(updateQuestViewJob);
            updateQuestViewJob = null;
        }

        public void UpdateBoardPlanks()
        {
            if (laneQuest == null) return;

            var featureUnlocked = logic.canUseMyFeature;
            var questsList = laneQuest.GetQuestList();

            for (int i = 0; i < questPlanksOnBoard.Length; i++)
            {
                var plank = questPlanksOnBoard[i];
                if (plank != null)
                {
                    if (featureUnlocked)
                    {
                        var quest = (i < questsList.Count) ? questsList[i] : null;

                        int stage = 0;

                        if (quest != null)
                        {
                            //var viewData = new SportLaneQuestInfoViewData(quest, logic.sportsmanManager, laneQuest);

                            var sportsmenShortage = logic.GetSportsmenShortage(quest);

                            var completed = (sportsmenShortage <= 0);

                            stage = (laneQuest.state == LaneQuestState.ProgressAccumulation || laneQuest.state == LaneQuestState.Tutorial)
                                ? ( /*quest.claimed*/ completed ? 1 : 2)
                                : 0;

                            if (quest.completed)
                                stage = 0;
                        }

                        plank.RunTween(stage);
                    }
                    else
                    {
                        plank.RunTween(0);
                    }
                }
            }
        }

        void UpdateOverheadUI()
        {
            if (overheadIcon != null && overheadIcon.icon != null)
            {
                var canUse = logic.canUseMyFeature;
                if (canUse)
                {
                    var overheadUIData = new OverheadUIDataBuildings();
                    overheadUIData.state = laneQuest.state == LaneQuestState.WaitForNewQuests
                        ? OverheadUIDataBuildings.State.SportLaneQuestBoardCooldown
                        : OverheadUIDataBuildings.State.SportLaneQuestBoard;
                    overheadUIData.ApplyData (overheadIcon.instance);
                }

                overheadIcon.icon.visible = canUse;
            }
        }

        void OnOverheadUIClick()
        {
            OnShowByCityClick();
        }

#if DEBUG
        void DebugSkip()
        {
            logic.DebugSkip();
        }
#endif
    }
}