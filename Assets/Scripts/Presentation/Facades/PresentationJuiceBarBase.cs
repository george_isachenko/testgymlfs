﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Data;
using Data.JuiceBar;
using Data.PlayerProfile;
using Data.Room;
using Logic.JuiceBar.Facade;
using Presentation.Base;
using UnityEngine;
using UnityEngine.Assertions;
using View.Actors;
using View.Rooms;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof (LogicJuiceBar))]
    public abstract class PresentationJuiceBarBase
        : BasePresentationFacade<LogicJuiceBar>
        , IFruitTreeActorSharedDataProvider
    {
        #region Static data.
        private static readonly string juiceTraysRootObjectName = "_JuiceTrays";
        #endregion

        #region Inspector fields.

        //------------------------------------------------------------------------
        [Header("Juice drinks")]

        [SerializeField]
        protected JuiceBarDrinkPrefabs juiceDrinks;
        #endregion

        #region Protected data.
        protected IRoomView juiceBarRoomView;
        protected Dictionary<string, IFruitTreeActor> actors;
        protected Transform juiceTraysRoot;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected void Subscribe(LogicJuiceBar logicJuiceBar)
        {
            this.logic = logicJuiceBar;
        }
        #endregion

        #region Unity API.
        protected void Awake ()
        {
            var juiceBarRoomView = clubInteractionProvider.GetRoomOfType(RoomType.JuiceBar);
            Assert.IsNotNull(juiceBarRoomView);
            Assert.IsNotNull(juiceBarRoomView.roomObject);

            if (juiceBarRoomView != null && juiceBarRoomView.roomObject)
            {
                this.juiceBarRoomView = juiceBarRoomView;
                var fruitTrees = juiceBarRoomView.roomObject.GetComponentsInChildren<IFruitTreeActor>(true);
                if (fruitTrees != null && fruitTrees.Count() > 0)
                {
                    // Select only unique-named trees and convert them to Dictionary by name as a key.
                    actors = fruitTrees.GroupBy(x => x.gameObject.name, (key, group) => group.FirstOrDefault()).ToDictionary(k => k.gameObject.name, v => v);
                }

                juiceTraysRoot = juiceBarRoomView.viewObject.transform.FindDeepChild(juiceTraysRootObjectName);
            }
        }

        protected void Start()
        {
        }

        protected void OnDestroy ()
        {
        }
        #endregion

        #region Public API.
        public virtual void BuyShortageOfFruits(JuiceRecipesTableData recipe)
        {
        }

        public virtual void UpdateMakeJuice()
        {
        }

        public abstract void InitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int unlockLevel
            , Cost unlockCost
            , int numFruits
            , int maxFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft);

        public abstract void UpdateFruitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int numFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft);

        public virtual void SelectFruitTreeByIndex(int index)
        {
        }

        public virtual void ShowMakeJuiceUI ()
        {
        }
        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("onJuiceConsumeStarted")]
        private void OnJuiceConsumeStarted (int personId, int equipmentId, ResourceType juiceType)
        {
            ICharacterActor actor = characterInteractionProvider.GetCharacterActor(personId);
            Assert.IsNotNull(actor);
            if (actor != null)
            {
                var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(equipmentId);
                Assert.IsNotNull(equipmentActor);

                if (equipmentActor != null)
                {
                    var pointName = equipmentActor.equipmentInfo.staticEquipmentName;
                    var placementPoint = juiceTraysRoot.FindDeepChild(pointName);
                    if (placementPoint != null)
                    {
                        var prefab = juiceDrinks.array.Where(x => x.juice == juiceType).Select(x => x.prefab).FirstOrDefault();
                        if (prefab != null)
                        {
                            Instantiate(prefab, placementPoint, false);
                        }
                        else
                        {
                            Debug.LogWarningFormat("No prefab set for Juice Type '{0}' in the 'Juice Drinks' list."
                                , juiceType);
                        }
                    }
                    else
                    {
                        Debug.LogWarningFormat("Cannot find placement point named {0} within object '{1}/{2}"
                            , pointName, juiceBarRoomView.viewObject.name, juiceTraysRootObjectName);
                    }
                }
            }
        }

        [BindBridgeEvent("onJuiceConsumeEnded")]
        private void OnJuiceConsumeEnded (int personId, int equipmentId)
        {
            ICharacterActor actor = characterInteractionProvider.GetCharacterActor(personId);
            Assert.IsNotNull(actor);
            if (actor != null)
            {
                var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(equipmentId);
                Assert.IsNotNull(equipmentActor);

                if (equipmentActor != null)
                {
                    var pointName = equipmentActor.equipmentInfo.staticEquipmentName;
                    var placementPoint = juiceTraysRoot.FindDeepChild(pointName);
                    if (placementPoint != null)
                    {
                        foreach (Transform child in placementPoint)
                        {
                            Destroy(child.gameObject);
                        }
                    }
                    else
                    {
                        Debug.LogWarningFormat("Cannot find placement point named {0} within object '{1}/{2}"
                            , pointName, juiceBarRoomView.viewObject.name, juiceTraysRootObjectName);
                    }
                }
            }
        }
        #endregion

        #region IFruitTreeActorSharedDataProvider interface.
        public abstract GameObject GetCursorObject (IFruitTreeActor actor);
        #endregion

        #region Event handlers.
        #endregion

        #region Protected API.
        protected void UpdateActorsDisplayMode (string roomName)
        {
            if (actors != null)
            {
                var mode = juiceBarRoomView.actorsDisplayMode;

                foreach (var actor in actors.Values)
                {
                    actor.SetDisplayMode(mode);
                }
            }
        }
        #endregion
    }
}
