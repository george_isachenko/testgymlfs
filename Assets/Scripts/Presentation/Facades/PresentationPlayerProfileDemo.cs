﻿using UnityEngine;
using Logic.PlayerProfile;
using Logic.PlayerProfile.Events;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Player Profile Demo")]
    public class PresentationPlayerProfileDemo : PresentationPlayerProfileBase
    {
        #region IPlayerProfileInteractionProvider properties.
        #endregion

        #region Public (Inspector) data.
        #endregion

        #region Private data.
        #endregion

        #region Bridge.
        #endregion

        #region Bridge events.
        #endregion

        #region Unity API.
        protected void Awake()
        {
            PlayerProfileUpdatedEvent.Event += OnPlayerProfileUpdatedEvent;

        }

        protected new void Start()
        {
            base.Start();
            gui.hud.enablerFriends.CanEnabled_Callback = () => { return false; };
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            PlayerProfileUpdatedEvent.Event -= OnPlayerProfileUpdatedEvent;
        }
        #endregion

        #region IPlayerProfileInteractionProvider interface.
        #endregion

        #region Private functions.
        void OnPlayerProfileUpdatedEvent()
        {
            gui.hud.profileLevel = logic.level.ToString();
            gui.hud.profileExpTxt = logic.profileExpTxt;
            gui.hud.profileExpProgress = logic.profileExpProgress;
        }
        #endregion
    }
}