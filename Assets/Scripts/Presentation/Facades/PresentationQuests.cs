﻿using BridgeBinder;
using Data.Room;
using Logic.Quests.Facade;
using Logic.Quests.Juicer;
using Logic.Quests.Visitor;
using Presentation.Base;
using Presentation.Providers;
using Presentation.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using View.CameraHelpers;
using View.Rooms;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Quests")]
    [BindBridgeInterface(typeof(LogicQuests))]
    public class PresentationQuests
        : BasePresentationFacade<LogicQuests>, IQuestsInteractionProvider
    {
        public GameObject tutorialArrowPrefab;
        public GameObject overheadIconArrowPrefab;

        private CameraController                camControl;

        public PresentationQuestsVisitor    visitorQuests   { get; private set; }
        public PresentationQuestsDaily      dailyQuests     { get; private set; }
        public PresentationQuestsJuicer     juiceQuests     { get; private set; } 
        public PresentationTutorial         tutorial        { get; private set; }
        public PresentationDailyBonus    dailyBonus      { get; private set; }


        [BindBridgeSubscribe]
        private void Subscribe(LogicQuests questsLogic)
        {
            logic = questsLogic;

            visitorQuests = new PresentationQuestsVisitor(logic.visitorQuests, this);
            visitorQuests.SelectVisitorById += SelectVisitorById;
            // visitorQuests.RequestExpand += OnRequestExpand;

            dailyQuests = new PresentationQuestsDaily(logic.dailyQuests, this);
            dailyBonus = new PresentationDailyBonus(logic.dailyBonusNew, this);
            juiceQuests = new PresentationQuestsJuicer(logic.juiceQuests, this);

            tutorial = new PresentationTutorial(logic.tutorial, this, ref tutorialArrowPrefab, ref overheadIconArrowPrefab);

            logic.visitorQuests.presenter = visitorQuests;
            logic.dailyQuests.presenter = dailyQuests;
            logic.juiceQuests.presenter = juiceQuests;
            logic.dailyBonusNew.presenter = dailyBonus;
        }

        void Awake()
        {
            camControl = Camera.main.GetComponent<CameraController>();
            Assert.IsNotNull(camControl);
        }

        void OnDestroy()
        {
            juiceQuests.OnDestroy();
            visitorQuests.OnDestroy();
        }

        public IRoomView GetClubView()
        {
            var clubView = clubInteractionProvider.GetRoomOfType(RoomType.Club) as IRoomView;
            Assert.IsNotNull(clubView);
            return clubView;
        }

        private void SelectVisitorById(int id)
        {
            characterInteractionProvider.SelectCharacter(id);
            var actor = characterInteractionProvider.GetCharacterActor(id);

            if (actor != null)
            {
                Vector3 offset = camControl.GetWorldOffsetForNormalizedScreenPoint (gui.hud.focusPointRight025.anchorMin);
                camControl.AutoMoveTo(actor.position.x + offset.x, actor.position.z + offset.z);
            }
        }

/*
        private void OnRequestExpand()
        {
            var clubView = GetClubView();
            if (clubView != null)
            {
                clubView.StartExpand();
            }
        }
*/

        VisitorQuestLogic IQuestsInteractionProvider.GetVisitorQuest(int characterId)
        {
            return logic.visitorQuests.GetVisitorQuest(characterId);
        }

        JuiceQuestLogic IQuestsInteractionProvider.GetJuiceQuest(int charactedId)
        {
            return logic.juiceQuests.GetQuest(charactedId);
        }

        bool IQuestsInteractionProvider.isTutorialRunning
        {
            get
            {
                return logic.tutorial.isRunning;
            }
        }
    }
}
