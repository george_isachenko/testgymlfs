using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Persons;
using Logic.Facades;
using Presentation.Base;
using UnityEngine;
using View.UI;
using View.UI.Base;
using Logic.Persons.Events;
using Core.Analytics;
using Data.Sport;
using UnityEngine.Assertions;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Sportsman Training")]
    [BindBridgeInterface(typeof (SportsmanTrainingLogic))]
    public class SportsmanTrainingPresenter
        : BasePresentationFacade<SportsmanTrainingLogic>
    {
        private SportsmanTrainingView view
        {
            get { return gui.sportsmanTrainingView; }
        }

        private IEquipmentInfo selectedEquipment;
        private List<SportExercise> currentExercises;
        private bool skipDeselectOnViewHide = false;

        [BindBridgeSubscribe]
        private void Subscribe(SportsmanTrainingLogic sportsmanTrainingLogic)
        {
            logic = sportsmanTrainingLogic;
        }

        private void Awake()
        {
            equipmentInteractionProvider.onEquipmentSelected += OnEquipmentSelected;
            equipmentInteractionProvider.onEquipmentDeselected += OnEquipmentDeselected;
            view.OnHide_Callback += () =>
            {
                OnViewHided();
                SportsmanTypeChangedEvent.Event -= OnSportsmanTypeChangedEvent;
                CharacterBirthEvent.Event -= OnCharacterBirthEvent;
            };
            view.onExerciseSelected += OnExerciseSelected;
            view.OnShow_Callback += () =>
            {
                SportsmanTypeChangedEvent.Event += OnSportsmanTypeChangedEvent;
                CharacterBirthEvent.Event += OnCharacterBirthEvent;
                UpdateView();
                skipDeselectOnViewHide = false;
            };
        }

        private void OnCharacterBirthEvent(Person person, bool restored, CharacterDestination arg3)
        {
            if (person is PersonSportsman && !restored)
                UpdateView();
        }

        private void OnSportsmanTypeChangedEvent(PersonSportsman personSportsman, SportsmanType sportsmanType)
        {
            UpdateView();
        }

        private void OnExerciseSelected(int exerciseindex)
        {
            Assert.IsNotNull(currentExercises);
            Assert.IsNotNull(selectedEquipment);

            if (currentExercises == null || selectedEquipment == null)
            {
                Debug.LogWarningFormat ("OnExerciseSelected: currentExercises: {0}, selectedEquipment: {1}", currentExercises != null, selectedEquipment != null);
                return;
            }

            var exercise = currentExercises[exerciseindex];
            var canTrainFree = logic.HasResourcesForExercise(exercise);
            if (canTrainFree)
            {
                UpdateView();

                if (logic.Train(exercise, selectedEquipment.id, characterInteractionProvider.selectedCharacterId))
                    view.Hide();
            }
            else
            {
                var existingSportsmans = logic.sportsmanManager.GetSportsmanCount(exercise.trainFrom,
                    null,
                    //PersonState.InQueue,
                    PersonState.Entering,
                    PersonState.Idle,
                    PersonState.ExerciseComplete);

                if (existingSportsmans == 0)
                {
                    EvaluateSportsmenPurchase(exercise.trainFrom, (sportsmenCost) =>
                    
                        {
                            if (logic.QuickPurchaseSportsmen(sportsmenCost, exercise))
                            {
                                DelayedInvoke(() =>
                                {
                                    UpdateView();
                                    OnExerciseSelected(exerciseindex);
                                }); 
                            }
                        },
                        (sportsmenCost) =>
                        {
                            logic.playerProfile.SendNotEnoughMoneyEvent(sportsmenCost, true);
                        },
                        null);
                    return;
                }

                var existingResources = logic.storage.GetResourcesCount(exercise.needResource);
                if (existingResources != 0)
                    return; // TODO: Why this check performed and it returns???

                var confirmView = gui.confirmWithBucks;

                var cost = new Cost
                {
                    type = Cost.CostType.ResourcesOnly
                };

                cost.AddResource(exercise.needResource);
                confirmView.OnNeedResources2(cost, logic.storage.GetExtraBucksPrice(cost, null).value);
                confirmView.OnOpenShop = () =>
                {
                    skipDeselectOnViewHide = true;
                    gui.shopUltimate.Show();
                    gui.shopUltimate.ShowItems();
                    var shopPrtesenter = GetComponent<PresentationUltimateShop>();
                    shopPrtesenter.ShowSportResources();
                    shopPrtesenter.ScrollToSportResource(exercise.needResource);
                    gui.shopUltimate.OnHideHandler.AddListener(ReturnToTrainingAfterShopUltimateHide);
                };
                
                confirmView.OnConfirm = () =>
                {
                    if (!logic.playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("SportClub", "Training"), true))
                        return;

                    logic.playerProfile.AddResource(exercise.needResource, 1, new Analytics.MoneyInfo("SportClub", exercise.needResource.ToString()), true);
                    OnExerciseSelected(exerciseindex);
                };
            }
        }

        private void ReturnToTrainingAfterAgencyHide()
        {
            gui.sportsmenAgency.OnHideHandler.RemoveListener(ReturnToTrainingAfterAgencyHide);
            DelayedInvoke(() =>
            {
                if (gui.windows.GetCurrentType() != WindowTypes.None)
                    return;

                if (selectedEquipment != null)
                {
                    logic.SelectEquip(selectedEquipment.id);
                    view.Show();
                }
            });
        }

        protected override void GoBackToMyView()
        {
            if (selectedEquipment != null)
            {
                logic.SelectEquip(selectedEquipment.id);
            }
            base.GoBackToMyView();
        }

        protected override UIBaseView GetView()
        {
            return view;
        }

        private void ReturnToTrainingAfterShopUltimateHide()
        {
            gui.shopUltimate.OnHideHandler.RemoveListener(ReturnToTrainingAfterShopUltimateHide);
            DelayedInvoke(() =>
            {
                if (gui.windows.GetCurrentType() != WindowTypes.None)
                    return;

                if (selectedEquipment != null)
                {
                    logic.SelectEquip(selectedEquipment.id);
                    view.Show();
                }
            });
        }

        private void OnViewHided()
        {
            if (selectedEquipment != null && !skipDeselectOnViewHide)
            {
                equipmentInteractionProvider.DeselectEquipmentIfSelectedRequest(selectedEquipment.id);
            }
        }

        private void OnEquipmentSelected(int id, IEquipmentInfo equipmentInfo)
        {
            if (equipmentInfo.estateType.itemType == EstateType.Subtype.Sport && equipmentInfo.state == EquipmentState.Idle)
            {
                selectedEquipment = equipmentInfo;
                UpdateView();
                view.Show();
            }
        }

        private void OnEquipmentDeselected(int id)
        {
//             if (selectedEquipment != null && selectedEquipment.id == id)
//                 selectedEquipment = null;

            if (!view.isActiveAndEnabled)
                return;

            view.Hide();
        }

        private void UpdateView()
        {
            if (selectedEquipment != null)
            {
                currentExercises = logic.GetExercisesForEquip(selectedEquipment.estateType.id);
                if (currentExercises != null)
                {
                    var data = selectedEquipment == null
                        ? null
                        : DataTables.instance.sportEquipment.FirstOrDefault(e => e.id == selectedEquipment.estateType.id);
                    var viewData = new SportTrainingViewData(currentExercises,
                        logic.storage,
                        logic.sportsmanManager,
                        logic.sportAgencyLogic)
                    {
                        headerText = data == null ? "Unnamed" : Loc.Get(data.sportEquipRoomLocId)
                    };
                    view.FillExercisesFields(viewData);
                }
            }
            else
                currentExercises = null;
        }
    }
}