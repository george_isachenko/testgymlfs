﻿using System;
using Data;
using UnityEngine;
using UnityEngine.Assertions;
using View.Actors;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Juice Bar Demo")]
    public class PresentationJuiceBarDemo
        : PresentationJuiceBarBase
    {
        #region Inspector fields.
        #endregion

        #region Private data.
        #endregion

        #region Bridge.
        #endregion

        #region Unity API.
        protected new void Awake ()
        {
            base.Awake();
        }

        protected new void Start()
        {
            base.Start();

            clubInteractionProvider.onRoomActivatedEvent += OnRoomActivatedEvent;
            clubInteractionProvider.onRoomDeactivatedEvent += OnRoomDeactivatedEvent;
        }

        protected new void OnDestroy ()
        {
            base.OnDestroy();

            clubInteractionProvider.onRoomActivatedEvent -= OnRoomActivatedEvent;
            clubInteractionProvider.onRoomDeactivatedEvent -= OnRoomDeactivatedEvent;
        }
        #endregion

        #region Public API.
        public override void InitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int unlockLevel
            , Cost unlockCost
            , int numFruits
            , int maxFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft)
        {
            Assert.IsNotNull(juiceBarRoomView);

            IFruitTreeActor actor;
            if (actors != null && actors.TryGetValue(name, out actor))
            {
                actor.Init
                    ( this
                    , juiceBarRoomView.roomConductor
                    , juiceBarRoomView.actorsDisplayMode
                    , unlocked
                    , canUnlock
                    , unlockLevel
                    , unlockCost
                    , numFruits
                    , maxFruits
                    , regenTotalTime
                    , regenNextTimeLeft);
            }
            else
            {
                Debug.LogWarningFormat("There's no Juice Bar tree actor with a name '{0}' in a RoomObject of JuiceBar.", name);
            }
        }

        public override void UpdateFruitTree
            ( string name
            , bool unlocked
            , bool canUnlock
            , int numFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft)
        {
            IFruitTreeActor actor;
            if (actors != null && actors.TryGetValue(name, out actor))
            {
                actor.UpdateState (unlocked, canUnlock, numFruits, regenTotalTime, regenNextTimeLeft);
            }
            else
            {
                Debug.LogWarningFormat("There's no Juice Bar tree actor with a name '{0}' in a RoomObject of JuiceBar.", name);
            }
        }
        #endregion

        #region Bridge event handlers.
        #endregion

        #region IFruitTreeActorSharedDataProvider interface.
        public override GameObject GetCursorObject (IFruitTreeActor actor)
        {
            Assert.IsNotNull(actor);
            return null;
        }
        #endregion

        #region Event handlers.
        void OnRoomActivatedEvent (string roomName)
        {
            if (roomName == juiceBarRoomView.roomName)
            {
                UpdateActorsDisplayMode(roomName);
            }
        }

        void OnRoomDeactivatedEvent (string roomName)
        {
            if (roomName == juiceBarRoomView.roomName)
            {
                UpdateActorsDisplayMode(roomName);
            }
        }
        #endregion

        #region Private methods.
        #endregion
    }
}
