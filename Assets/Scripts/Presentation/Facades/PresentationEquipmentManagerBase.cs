﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using CustomAssets.Lists;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Estate.Static;
using Data.Room;
using InspectorHelpers;
using Logic.Estate.Facade;
using Presentation.Base;
using Presentation.Facades.Interfaces.EquipmentManager;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Actors;
using View.Actors.Events;
using View.Actors.Helpers;
using View.Actors.Settings;
using View.CameraHelpers;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(ILogicEquipmentManager))]
    public abstract class PresentationEquipmentManagerBase
        : BasePresentationFacade<ILogicEquipmentManager>
        , IPresentationEquipmentManager
        , IEquipmentInteractionProvider
        , IEquipmentActorSharedDataProvider
    {
        #region Types.
        [Serializable]
        public struct DeliveryPrefab
        {
            public RoomCoordinates minSize;
            public RoomCoordinates maxSize;
            public GameObject prefab;
            public bool wallEquipment;
        }

        [Serializable]
        public struct RepairPrefab
        {
            public RoomCoordinates size;
            public GameObject prefab;
            public bool wallEquipment;
        }
        #endregion

        #region Static data.
        protected static readonly int initialActorsCapacity = 128;
        protected static readonly int initialStaticEquipmentCapacity = 64;
        protected static readonly string nameFormatString = @"E:{0} {1}";
        protected static readonly char[] nameTrimChars = { '"', '\'', '{', '}', '(', ')' };
        #endregion

        #region IPresentationEquipmentManager events.
        public event EquipmentPlacementUpdated onEquipmentPlacementUpdated;
        #endregion

        #region IEquipmentInteractionProvider events.
        public event OnEquipmentSelected onEquipmentSelected;
        public event OnEquipmentDeselected onEquipmentDeselected;
        public event OnEquipmentDestroyed onEquipmentDestroyed;
        public event OnEquipmentStateChanged onEquipmentStateChanged;
        #endregion

        #region IEquipmentActorSharedDataProvider properties.
        public virtual EquipmentMovementMarker movementMarker
        {
            get
            {
                return null;
            }
        }

        int IEquipmentActorSharedDataProvider.animationSelectorParameterHash
        {
            get
            {
                return animationSelectorParameterHash;
            }
        }
        #endregion

        #region IEquipmentInteractionProvider properties.
        int IEquipmentInteractionProvider.selectedEquipmentId
        {
            get
            {
                return logic.selectedEquipmentId;
            }
        }
        #endregion

        #region Public (Inspector) fields.
        [Header("Actors prefabs")]
        [ReorderableList(LabelsEnumType = typeof(EstateType.Subtype))]
        public GameObjectsList[] prefabsList;

        [Header("Animation")]
        [Tooltip("Animator Controller parameter name to set to index of animation that must be played on animated equipment")]
        public string animationSelectorParameter;

        [Header("Actor customization")]
        public EquipmentActorSettings actorSettings = new EquipmentActorSettings();
        
        [Header("Equipment delivery & repair")]
        [ReorderableList]
        public DeliveryPrefab[] deliveryPrefabs;
        [ReorderableList]
        public RepairPrefab[] repairPrefabs;

        [Header("Overhead UI")]
        public SingleActionIcon equipmentOverheadIconPrefab;
        public HintTextIcon hintTextIconPrefab;
        public HintTextIcon hintStyleTextIconPrefab;
        #endregion

        #region Private data.
        protected Dictionary<int, IEquipmentActor> actors = new Dictionary<int, IEquipmentActor>(initialActorsCapacity);
        protected Dictionary<StaticEquipmentID, IEquipmentActor> staticEquipment;
        protected int animationSelectorParameterHash;
        protected Dictionary<int, GameObject> staticImpostors = new Dictionary<int, GameObject>(256);
        protected CameraController cameraController;
        #endregion

        #region Bridge interface.
        [BindBridgeSubscribe]
        protected void Subscribe(ILogicEquipmentManager logicEquipmentManager)
        {
            logic = logicEquipmentManager;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(ILogicEquipmentManager logicEquipmentManager)
        {
            logic = null;
        }
        #endregion

        #region Unity API.
        protected void Awake()
        {
            if (animationSelectorParameter != null && animationSelectorParameter != string.Empty)
                animationSelectorParameterHash = Animator.StringToHash(animationSelectorParameter);

            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onRoomDeactivatedEvent += OnRoomDeactivatedEvent;
                clubInteractionProvider.onRoomActivatedEvent += OnRoomActivatedEvent;
            }

            if (playerProfileInteractionProvider != null)
            {
                playerProfileInteractionProvider.onLevelUp += OnLevelUp;
            }
        }

        protected void Start()
        {
        }

        protected void OnDestroy()
        {
            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onRoomDeactivatedEvent -= OnRoomDeactivatedEvent;
                clubInteractionProvider.onRoomActivatedEvent -= OnRoomActivatedEvent;
            }

            if (playerProfileInteractionProvider != null)
            {
                playerProfileInteractionProvider.onLevelUp -= OnLevelUp;
            }
        }
        #endregion

        #region UnityEditor API.
#if UNITY_EDITOR
        protected void OnValidate()
        {
        }
#endif
        #endregion

        #region IPresentationEquipmentManager interface.
        public virtual void CreateEquipmentRequest(int id, IEquipmentInfo equipmentInfo)
        {
            Assert.IsNotNull(clubInteractionProvider);

            Assert.IsFalse(actors.ContainsKey(id));
            if (!actors.ContainsKey(id))
            {
                var equipmentPrefab = FindPrefab(equipmentInfo);
                // Assert.IsNotNull(equipmentPrefab);
                if (equipmentPrefab == null)
                {
                    Debug.LogWarningFormat("Unsupported equipment type '{0}' for actor id: {1}", equipmentInfo.estateType.itemType, id);
                    return; // TODO: Not all equipment types are supported yet.
                }

                var actorObject = Instantiate(equipmentPrefab);
                Assert.IsNotNull(actorObject);

                if (actorObject != null)
                {
                    var actor = actorObject.GetComponent<IEquipmentActor>();
                    if (actor != null)
                    {
                        var room = clubInteractionProvider.GetRoom(equipmentInfo.roomName);
                        Assert.IsNotNull(room);

                        actorObject.name = string.Format(nameFormatString, id, Loc.Get(equipmentInfo.estateType.locNameId).Trim(nameTrimChars));

                        SetActorEventHandlers(actor, false, false);

                        var repairPrefab = SelectRepairPrefab(actor.size, actor.wallMounted);
                        if (repairPrefab == null &&
                            (equipmentInfo.estateType.itemType == EstateType.Subtype.Training || equipmentInfo.estateType.itemType == EstateType.Subtype.Sport))
                        {
                            Debug.LogWarningFormat("There is no repair prefab for equipment '{0}' of size {1} (wall equipment: {2})."
                                , equipmentInfo.estateType.locNameId, actor.size, actor.wallMounted);
                        }

                        actor.Init
                            ( this
                            , id
                            , equipmentInfo
                            , room.roomConductor
                            , room.actorsDisplayMode
                            , CreateCachedStaticMeshImpostorForSkinnedEquipment(actor)
                            , SelectDeliveryPrefab(actor.size, actor.wallMounted)
                            , repairPrefab
                            , actorSettings);

                        var placement = equipmentInfo.placement;

                        if (!placement.position.isValid)
                        {
                            placement = actor.wallMounted
                                ? room.roomConductor.FindFreeSpaceOnWall(actor.size, actor.wallSize, actor.wallSizeWithMargin, actor.occupiesFloorSpace)
                                : room.FindFreeSpaceForEquipment(actor.size);
                        }
                        actor.onEquipmentActorPlacementUpdated += OnEquipmentActorPlacementUpdated;
                        actor.SetPlacement(placement);

                        actors.Add(id, actor);

                        // UpdateOverheadUI(actor);

                        if (equipmentInfo.isBroken || equipmentInfo.isRepairing)
                        {
                            actor.SetRepairMode(true);
                        }

                        if (equipmentInfo.state == EquipmentState.AssemblyComplete)
                        {
                            UpdateOverheadUI(actor);
                            ExecuteEvents.Execute<IEquipmentActorEvents>(actor.gameObject, null, (target, eventData) => target.OnAssemblyComplete());
                        }
                    }
                }
            }
        }

        public virtual void BindStaticEquipmentRequest
            (int id, StaticEquipmentID staticEquipmentId, IEquipmentInfo equipmentInfo)
        {
            Assert.IsNotNull(clubInteractionProvider);

            Assert.IsFalse(actors.ContainsKey(id));
            if (!actors.ContainsKey(id))
            {
                FindPrefab(equipmentInfo);

                IEquipmentActor actor;
                if (staticEquipment.TryGetValue(staticEquipmentId, out actor))
                {
                    var room = clubInteractionProvider.GetRoom(equipmentInfo.roomName);
                    Assert.IsNotNull(room);

                    var isInitialBinding = (actor.equipmentInfo == null);

                    SetActorEventHandlers(actor, false, false);

                    var repairPrefab = SelectRepairPrefab(actor.size, actor.wallMounted);
                    if (repairPrefab == null &&
                            (equipmentInfo.estateType.itemType == EstateType.Subtype.Training || equipmentInfo.estateType.itemType == EstateType.Subtype.Sport))
                    {
                        Debug.LogWarningFormat("There is no repair prefab for static equipment '{0}' of size {1} (wall equipment: {2})."
                            , staticEquipmentId.ToString(), actor.size, actor.wallMounted);
                    }

                    actor.Init
                        ( this
                        , id
                        , equipmentInfo
                        , room.roomConductor
                        , room.actorsDisplayMode
                        , CreateCachedStaticMeshImpostorForSkinnedEquipment(actor)
                        , SelectDeliveryPrefab(actor.size, actor.wallMounted)
                        , repairPrefab
                        , actorSettings);

                    actor.SetPlacement(equipmentInfo.placement);

                    if (isInitialBinding) // Do not set second copy of event handler on rebind.
                    {
                        actor.onEquipmentActorPlacementUpdated += OnEquipmentActorPlacementUpdated;
                    }

                    actors.Add(id, actor);

                    UpdateOverheadUI(actor);
                }
            }
        }

        public virtual void DestroyEquipmentRequest(int id)
        {
            // TODO: Enable assert after supporting all equipment types.
            // Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (!actor.equipmentInfo.isStatic)
                {
                    actor.onEquipmentActorPlacementUpdated -= OnEquipmentActorPlacementUpdated;

                    actor.Destroy();
                    actors.Remove(id);
                }
                else
                {
                    Debug.LogWarningFormat("Static equipment cannot be destroyed: {0} (Static id: {1})", id, actor.equipmentInfo.staticEquipmentId);
                }

                onEquipmentDestroyed?.Invoke(id);
            }
            else
            {
                Debug.LogWarningFormat("Invalid equipment actor id: {0}", id);
            }
        }

        public virtual void EquipmentStartExerciseRequest (int id, int exerciseAnimation, float speed)
        {
            // TODO: Enable assert after supporting all equipment types.
            // Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.selectedEquipmentAnimation = exerciseAnimation;
                actor.animationSpeed = speed;
                // Real exercise animation will be queued later via character manager and IEquipmentInteractionProvider.StartUsingEquipment().
            }
        }

        public virtual void EquipmentSwitchExerciseRequest (int id, int exerciseAnimation, float speed)
        {
            // TODO: Enable assert after supporting all equipment types.
            // Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.selectedEquipmentAnimation = exerciseAnimation;
                actor.animationSpeed = speed;
                // Real exercise animation will be queued later via character manager and IEquipmentInteractionProvider.StartUsingEquipment().
            }
        }

        public virtual void EquipmentEndExerciseRequest (int id)
        {
            // TODO: Enable assert after supporting all equipment types.
            // Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.selectedEquipmentAnimation = -1;
                actor.animationSpeed = 1.0f;
                // Real work will be queued later via character manager and IEquipmentInteractionProvider.StopUsingEquipment().
            }
        }

        public virtual bool EquipmentStartPreviewModeRequest (IEquipmentInfo equipmentInfo, Action<EquipmentPlacement?> onPreviewComplete)
        {
            return false;
        }

        public virtual void EquipmentEndPreviewModeRequest ()
        {
        }

        IEnumerable<KeyValuePair<StaticEquipmentID, StaticEquipmentInfo>> IPresentationEquipmentManager.GetStaticEquipmentList()
        {
            if (staticEquipment == null)
            {
                List<KeyValuePair<StaticEquipmentID, IEquipmentActor> > staticActors = new List<KeyValuePair<StaticEquipmentID, IEquipmentActor> >(initialStaticEquipmentCapacity);

                foreach (var room in clubInteractionProvider.rooms)
                {
                    var roomEquip = room.Value.roomObject.GetComponentsInChildren<IEquipmentActor>(true);
                    foreach (var equip in roomEquip)
                    {
                        if (equip.equipmentInfo == null)    // Add only "unbound" (not initialized yet) equipment.
                        {
                            staticActors.Add(new KeyValuePair<StaticEquipmentID, IEquipmentActor>(new StaticEquipmentID(room.Key, equip.name), equip));
                        }
                    }
                }

                if (staticActors.Count > 0)
                {
                    staticEquipment = staticActors.ToDictionary(x => x.Key, x => x.Value);
                }
            }

            return staticEquipment.ToDictionary
                ( x => x.Key
                , x => new StaticEquipmentInfo(x.Value.estateType, x.Value.placement, x.Value.showAtLevel, x.Value.initiallyLocked, x.Value.editable));
        }

        public virtual void ShowStyleHintOverheadUIIcon(int id, string text_)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                var hintTextIcon = new OverheadIconInstanceHolder<HintTextIcon>(hintStyleTextIconPrefab);

                SpritesList sl = GUICollections.instance.commonIcons;

                hintTextIcon.icon.autoHideDelay = 2.0f;
                hintTextIcon.instance.txtHint.text = text_;
                hintTextIcon.instance.sprIcon.sprite = sl.array[3];
                hintTextIcon.instance.sprIcon.gameObject.SetActive(true);

                hintTextIcon.icon.frontIcon = (id == logic.selectedEquipmentId);
                actor.SetOverheadUIIcon(hintTextIcon);
            }
        }
        #endregion

        #region Bridge events handlers.
        [BindBridgeEvent("EquipmentAssemblyStartEvent")]
        void OnEquipmentAssemblyStartEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(actor.gameObject, null, (target, eventData) => target.OnAssemblyStarted());
            }
        }

        [BindBridgeEvent("EquipmentStateChangedEvent")]
        void OnEquipmentStateChangedEvent(int id, EquipmentState oldState, EquipmentState newState)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                UpdateOverheadUI(actor);

                onEquipmentStateChanged?.Invoke(id, oldState, newState);
            }
        }

        [BindBridgeEvent("EquipmentStartPendingExerciseEvent")]
        void OnEquipmentStartPendingExerciseEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));
        }

        [BindBridgeEvent("EquipmentSwitchPendingExerciseEvent")]
        void OnEquipmentSwitchPendingExerciseEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));
        }

        [BindBridgeEvent("EquipmentCancelPendingExerciseEvent")]
        void OnEquipmentCancelPendingExerciseEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));
        }

        [BindBridgeEvent("EquipmentRepairRequiredEvent")]
        void OnEquipmentRepairRequiredEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.SetRepairMode(true);
            }
        }

        [BindBridgeEvent("EquipmentEndRepairEvent")]
        void OnEquipmentEndRepairEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.SetRepairMode(false);
            }
        }

        [BindBridgeEvent("EquipmentStartUnlockingEvent")]
        void OnEquipmentStartUnlockingEvent(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.StartedUnlocking();
            }
        }

        [BindBridgeEvent("EquipmentUpgradedEvent")]
        void OnEquipmentUpgradedEvent(int id, int upgradeLevel)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ShowHintOverheadUIIcon(actor, Loc.Get("equipmentUpgradedHintMessage"), 2.5f);
                ExecuteEvents.Execute<IEquipmentActorEvents>
                    (actor.gameObject, new UpgradeEventData(upgradeLevel), (target, eventData) => target.OnUpgraded((UpgradeEventData)eventData));
            }
        }
        #endregion

        #region IEquipmentInteractionProvider interface.
        IEquipmentActor IEquipmentInteractionProvider.GetEquipmentActor (int equipmentId)
        {
            IEquipmentActor actor;
            return (actors.TryGetValue(equipmentId, out actor)) ? actor : null;
        }

        public virtual void SelectEquipmentRequest(int equipmentId, bool allowDeselectionIfSelected)
        {
        }

        public virtual void DeselectEquipmentRequest()
        {
        }

        public virtual void DeselectEquipmentIfSelectedRequest(int equipmentId)
        {
        }
        #endregion

        #region Protected API.
        protected void UpdateOverheadUI(int actorId)
        {
            Assert.IsTrue(actorId != EquipmentData.InvalidId);

            IEquipmentActor actor;
            if (actors.TryGetValue(actorId, out actor))
            {
                UpdateOverheadUI(actor);
            }
        }

        protected void SendEquipmentSelectedEvent (int id, IEquipmentInfo equipmentInfo)
        {
            onEquipmentSelected?.Invoke(id, equipmentInfo);
        }

        protected void SendEquipmentDeselectedEvent (int id)
        {
            onEquipmentDeselected?.Invoke(id);
        }

        protected GameObject FindPrefab(IEquipmentInfo equipmentInfo)
        {
            var prefabList = SelectPrefabList(equipmentInfo.estateType);

            if (prefabList != null)
            {
                int prefabIdx = equipmentInfo.estateType.resourceId;

                if (prefabIdx >= 0 && prefabIdx < prefabList.array.Length)
                {
                    return prefabList.array[prefabIdx];
                }
            }

            return null;
        }

        protected GameObject CreateCachedStaticMeshImpostorForSkinnedEquipment(IEquipmentActor actor)
        {
            // TODO: Impostors usage is temporary disabled because it causes crash on iOS because of unaligned access in BakeMesh() on startup.

/*
            var smr = actor.skinnedMeshRenderer;
            if (smr != null)
            {
                var instanceId = smr.sharedMesh.GetInstanceID();
                GameObject impostorObject = null;
                if (!staticImpostors.TryGetValue(instanceId, out impostorObject))
                {
                    Debug.LogFormat("Name: {0}", smr.sharedMesh.name);

                    var staticMesh = new Mesh();
                    smr.BakeMesh(staticMesh);
                    staticMesh.name = string.Format("{0} (Generated)", name);

                    impostorObject = new GameObject();
                    impostorObject.SetActive(false);

                    var meshFilter = impostorObject.AddComponent<MeshFilter>();
                    meshFilter.sharedMesh = staticMesh;

                    var meshRenderer = impostorObject.AddComponent<MeshRenderer>();
                    meshRenderer.sharedMaterials = smr.sharedMaterials;
                    
                    impostorObject.name = string.Format("{0} ({1}) (Impostor)", smr.sharedMesh.name, instanceId);
                    impostorObject.transform.SetParent(transform, false);

                    staticImpostors.Add(instanceId, impostorObject);
                }

                return impostorObject;
            }
*/
            return null;
        }

        protected void ShowHintOverheadUIIcon(int id, string text_, float delay = 5.0f)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId && actors.ContainsKey(id));

            IEquipmentActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                ShowHintOverheadUIIcon(actor, text_, delay);
            }
        }

        protected void ShowHintOverheadUIIcon(IEquipmentActor actor, string text_, float delay = 5.0f)
        {
            Assert.IsNotNull(actor);

            var hintTextIcon = new OverheadIconInstanceHolder<HintTextIcon>(hintTextIconPrefab);

            hintTextIcon.icon.autoHideDelay = delay;
            hintTextIcon.instance.txtHint.text = text_;
            hintTextIcon.icon.frontIcon = (actor.id == logic.selectedEquipmentId);
            actor.SetOverheadUIIcon(hintTextIcon);
        }
        #endregion

        #region Protected virtual API.
        protected virtual void UpdateOverheadUI(IEquipmentActor actor)
        {
        }

        protected virtual void SetActorEventHandlers(IEquipmentActor actor, bool editingMode, bool allowPlacing)
        {
        }
        #endregion

        #region Private functions.
        GameObjectsList SelectPrefabList(EstateType estateType)
        {
            int prefabIdx = (int)estateType.itemType;
            if (prefabsList != null && prefabIdx >= 0 && prefabIdx < prefabsList.Length)
            {
                return prefabsList[prefabIdx];
            }

            return null;
        }

        GameObject SelectDeliveryPrefab (RoomCoordinates size, bool wallEquipment)
        {
            foreach (var prefab in deliveryPrefabs)
            {
                if (prefab.wallEquipment == wallEquipment &&
                    size.x >= prefab.minSize.x && size.y >= prefab.minSize.y &&
                    size.x <= prefab.maxSize.x && size.y <= prefab.maxSize.y)
                {
                    return prefab.prefab;
                }
            }
            return null;
        }

        GameObject SelectRepairPrefab (RoomCoordinates size, bool wallEquipment)
        {
            var idx = Array.FindIndex(repairPrefabs, (pi => pi.size == size && pi.wallEquipment == wallEquipment));
            return (idx >= 0) ? repairPrefabs[idx].prefab : null;
        }

        void OnRoomActivatedEvent(string roomName)
        {
            UpdateActorsDisplayMode(roomName);
        }

        void OnRoomDeactivatedEvent(string roomName)
        {
            UpdateActorsDisplayMode(roomName);
        }

        void OnLevelUp ()
        {
            foreach (var actor in actors.Values)
            {
                actor.UpdateAvailability();
            }
        }

        private void OnEquipmentActorPlacementUpdated(int id, EquipmentPlacement placement)
        {
            onEquipmentPlacementUpdated?.Invoke(id, placement);
        }

        void UpdateActorsDisplayMode(string roomName)
        {
            var room = clubInteractionProvider.GetRoom(roomName);
            Assert.IsNotNull(room);
            var mode = room.actorsDisplayMode;

            foreach (var actor in actors.Values)
            {
                if (actor.equipmentInfo.roomName == roomName)
                {
                    actor.SetDisplayMode(mode);
                }
            }
        }
        #endregion
    }
} 