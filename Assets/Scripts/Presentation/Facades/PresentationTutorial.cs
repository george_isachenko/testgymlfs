﻿using System;
using System.Collections.Generic;
using Data.Estate.Info;
using Data.Person.Info;
using Logic.Persons;
using Logic.Quests;
using Logic.Quests.Tutorial;
using Presentation.Base;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.OverheadUI;
using View.CameraHelpers;
using Data.Room;
using View.Actors;

namespace Presentation.Facades
{
    public class PresentationTutorial
        : BasePresentationNested<TutorManager, PresentationQuests>
    {
        private GameObject tutorialArrowPrefab; 
        private GameObject overheadIconArrowPrefab;

        private Dictionary<TutorStepLogic, OverheadIconInstanceHolderTransform> overheadArrowUi = new Dictionary<TutorStepLogic, OverheadIconInstanceHolderTransform>();
        private Dictionary<TutorStepLogic, GameObject> uiArrow = new Dictionary<TutorStepLogic, GameObject>();

        //public IClubInteractionProvider clubInteractionProvider { get { return parent.GetClubInteractionProvider(); } }

        public PresentationTutorial
            ( TutorManager tutor
            , PresentationQuests parentFacade
            , ref GameObject tutorialArrowPrefab
            , ref GameObject overheadIconArrowPrefab)
            : base (tutor, parentFacade)
        {
            this.logic.Presenter = this;
            this.tutorialArrowPrefab = tutorialArrowPrefab;
            this.overheadIconArrowPrefab = overheadIconArrowPrefab;

            gui.levelUp.OnHide_Callback = this.logic.OnLevelUpWindowHide;
            gui.tutorialMsg.onHideHandler.AddListener(this.logic.OnMessageHide);
        }

        public GameObject gameObject { get { return parent.gameObject; } }

        public void DisableHud()
        {
            gui.hud.CustomDefaultView = CustomDefaultHud;
            gui.hud.EnableMoneyButtons(false);
            gui.hud.UpdateHUD();
        }

        public void CustomDefaultHud()
        {
            gui.hud.btnDailyQuests.forceOff = true;
            gui.hud.btnDailyBonus.forceOff = true;
            gui.hud.btnLaneQuests.forceOff = true;
            gui.hud.enablerPromo.forceOff = true;
            gui.hud.btnAchievementsEnabler.forceOff = true;
            gui.hud.enablerJuiceBar.forceOff = true;
            gui.hud.enablerSportclub.forceOff = true;
            gui.hud.btnStorage.forceOff = true;
            gui.hud.enablerFriends.forceOff = true;
            gui.hud.enablerTrainers.forceOff = true;

            gui.hud.btnVisitorQuests.gameObject.SetActive(false);
            gui.hud.btnShop.gameObject.SetActive(false);
            gui.hud.btnSettings.gameObject.SetActive(false);
            gui.hud.btnBack.gameObject.SetActive(false);
            gui.hud.btnStyle.gameObject.SetActive(false);
            gui.hud.btnSportsmen.gameObject.SetActive(false);
            
        }

        public void EnableHud()
        {
            gui.hud.btnDailyQuests.forceOff = false;
            gui.hud.btnDailyBonus.forceOff = false;
            gui.hud.btnLaneQuests.forceOff = false;
            gui.hud.enablerPromo.forceOff = false;
            gui.hud.btnAchievementsEnabler.forceOff = false;
            gui.hud.enablerJuiceBar.forceOff = false;
            gui.hud.enablerSportclub.forceOff = false;
            gui.hud.btnStorage.forceOff = false;
            gui.hud.enablerFriends.forceOff = false;
            gui.hud.enablerTrainers.forceOff = false;

            gui.hud.btnVisitorQuests.gameObject.SetActive(true);

            gui.hud.CustomDefaultView = null;
            gui.hud.EnableMoneyButtons(true);
            gui.hud.UpdateHUD();
        }

        public void ShowTextHintMessage(string content, bool anchoredAtLeftSide)
        {
            gui.tutorialMsg.ShowTextHint(content, anchoredAtLeftSide);
        }

        public void ShowModalMessage(string title, string message)
        {
            gui.tutorialMsg.ShowModalMessage(title, message);
        }

        private GameObject GetUiArrow(TutorStepLogic forStep)
        {
            if (uiArrow.ContainsKey(forStep))
                return uiArrow[forStep];

            Assert.IsNotNull(tutorialArrowPrefab);
            return uiArrow[forStep] = UnityEngine.Object.Instantiate(tutorialArrowPrefab);
        }

        public IWorldPositionProvider GetWorldPositionProvider(IPersonInfo personInfo)
        {
            return parent.characterInteractionProvider.GetCharacterActor(personInfo.id).defaultWorldPositionProvider;
        }

        public IWorldPositionProvider GetWorldPositionProvider(IEquipmentInfo equipmentInfo)
        {
            return parent.equipmentInteractionProvider.GetEquipmentActor(equipmentInfo.id).defaultWorldPositionProvider;
        }

        public OverheadIconInstanceHolderTransform ShowArrow(TutorStepLogic arrowOwner, Func<IWorldPositionProvider> worldPositionProviderGetter, Vector3 screenPositionShift, Vector3 rotationEuler)
        {
            HideArrow(arrowOwner);

            if (overheadIconArrowPrefab == null)
            {
                Debug.LogErrorFormat("{0}: Missing tutorial arrow Overhead UI prefab.", GetType().Name);
                return null;
            }

            if (worldPositionProviderGetter == null)
            {
                Debug.LogErrorFormat("{0}: Missing world position provider getter function.", GetType().Name);
                return null;
            }

            OverheadIconInstanceHolderTransform iconHolder;
            overheadArrowUi.TryGetValue(arrowOwner, out iconHolder);

            if (iconHolder == null)
            {
                iconHolder = new OverheadIconInstanceHolderTransform(overheadIconArrowPrefab.transform);
                overheadArrowUi.Add(arrowOwner, iconHolder);
            }

            iconHolder.icon.transform.eulerAngles = rotationEuler;
            iconHolder.icon.screenPositionShift = screenPositionShift;
            iconHolder.icon.worldPositionProvider = worldPositionProviderGetter();
            iconHolder.icon.visible = true;

            return iconHolder;
        }

        public void ShowArrow(TutorStepLogic arrowOwner, Func<Transform> target, Vector3 positionOffset, Vector3 rotationEuler)
        {
            if (target() == null)
                return;

            HideArrow(arrowOwner);

            var arrow = GetUiArrow(arrowOwner);
            arrow.SetActive(true);
            arrow.transform.SetParent(target(), false);
            arrow.transform.localPosition = positionOffset;
            arrow.transform.localEulerAngles = rotationEuler;
        }

        public void HideArrow(TutorStepLogic arrowOwner)
        {
            OverheadIconInstanceHolderTransform iconHolder;
            if (overheadArrowUi.TryGetValue(arrowOwner, out iconHolder) && iconHolder != null)
            {
                iconHolder.icon.visible = false;
            }

            if (uiArrow.ContainsKey(arrowOwner))
            {
                UnityEngine.Object.Destroy(uiArrow[arrowOwner]);
                uiArrow.Remove(arrowOwner);
            }
        }

        public void ShowRectAt(IWorldPositionProvider provider, Vector2 rectSize, Vector2 rectOffset, bool isShadowVisible = true)
        {
            gui.tutorialMsg.ShowRectAt(provider, rectSize, rectOffset, isShadowVisible);

        }
        public void ShowRectAt(RectTransform rectTransform, bool isShadowVisible = true)
        {
            gui.tutorialMsg.ShowRectAt(rectTransform, isShadowVisible);
        }
        public void ShowBlockRect( bool isShadowVisible = true)
        {
            gui.tutorialMsg.ShowBlockRect(isShadowVisible);
        }

        public void HideRect()
        {
            gui.tutorialMsg.HideRect();
        }

        public void ShowDecorShop()
        {
            gui.shopUltimate.Show();
            gui.shopUltimate.ShowItems();
            var shopPrtesenter = parent.GetComponent<PresentationUltimateShop>();
            shopPrtesenter.ShowDecor();
        }

		public void ShowItemShop()
		{
			gui.shopUltimate.Show();
			gui.shopUltimate.ShowItems();
			var shopPrtesenter = parent.GetComponent<PresentationUltimateShop>();
			shopPrtesenter.ShowEquip();
		}

        public bool IsClubInEditingMode()
        {
            return parent.clubInteractionProvider.isInEditMode;
        }

        public void ShowRateUs()
        {
            gui.hud.modalModeConfirm = false;
            gui.rateUs.Show();
        }

        public OverheadUIIcon GetOverheadIcon(IPersonInfo targetPerson)
        {
            var manager = parent.GetComponent<ICharacterInteractionProvider>();
            if (manager == null)
                return null;
            var actor = manager.GetCharacterActor(targetPerson.id);
            return actor?.overheadUIIcon;
        }

        public void SetFriendsButtonActive(bool active)
        {
            gui.hud.enablerFriends.forceOff = false;
            gui.hud.enablerFriends.turnOn = active;
        }

        public void LockCamera(bool lockVale)
        {
            var cam = Camera.main.gameObject.GetComponent<CameraController>();
            cam.states.stickToPos = lockVale;
        }

        public View.Rooms.IRoomView GetRoomView(RoomType type_)
        {
            return (parent as BasePresentationFacade).clubInteractionProvider.GetRoomOfType(type_);
        }

        public IFruitTreeActor GetFruitTreeActor(int index)
        {
            var juiceBarRoomView = (parent as BasePresentationFacade).clubInteractionProvider.GetRoomOfType(RoomType.JuiceBar);      

            Assert.IsNotNull(juiceBarRoomView);
            Assert.IsNotNull(juiceBarRoomView.roomObject);

            if (juiceBarRoomView != null && juiceBarRoomView.roomObject)
            {
                var fruitTrees = juiceBarRoomView.roomObject.GetComponentsInChildren<IFruitTreeActor>(true);
                if (fruitTrees.Length > index)
                    return fruitTrees[index];
            }

            return null;
        }
    }
}
