﻿using BridgeBinder;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.Quests;
using Data.Room;
using Logic.Persons;
using Logic.Persons.Facade;
using Presentation.Base;
using Presentation.Facades.Interfaces.CharacterManager;
using Presentation.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View;
using View.Actors;
using View.Actors.Settings;
using View.Actors.Setup;
using View.CameraHelpers;
using View.Rooms;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(ILogicPersonManager))]
    public abstract class PresentationCharacterManagerBase
        : BasePresentationFacade<ILogicPersonManager>
        , IPresentationCharacterManager
        , ICharacterInteractionProvider
        , ICharacterActorSharedDataProvider
    {
        #region Readonly/static data.
        protected static readonly char[] nameTrimChars = { '"', '\'', '{', '}', '(', ')' };
        protected static readonly string nameFormatString = @"C:{0} {1}";
        #endregion

        #region IPresentationCharacterManager events.
        public event CharacterPositionUpdated onCharacterPositionUpdated;
        #endregion

        #region Inspector fields.
        //------------------------------------------------------------------------
        [Header("Characters setup settings")]

        [SerializeField]
        protected CharactersSetupDataSet charactersSetupDataSet;

        //------------------------------------------------------------------------
        [Header("Actor customization")]

        [SerializeField]
        protected CharacterActorSettings actorSettings;

        //------------------------------------------------------------------------
        [Header("Overhead UI")]

        [SerializeField]
        protected SingleActionIcon characterOverheadIconPrefab;

        [SerializeField]
        protected SingleActionAnimatedIcon characterOverheadAnimatedIconPrefab;

        [SerializeField]
        protected SingleActionIcon charTrainClientOverheadIconPrefab;

        [SerializeField]
        protected AnimatedActionIcon characterOverheadFadingIconPrefab;

        [SerializeField]
        protected SingleActionIcon characterOverheadTrainerLevelUpPrefab;

        [SerializeField]
        protected HintTextIcon hintTextIconPrefab;
        #endregion

        #region Private data.
        protected RuntimeCharacterSetupData[] runtimeCharacterSetupData = null;
        protected Dictionary<int, ICharacterActor> actors = new Dictionary<int, ICharacterActor>();
        protected CameraController cameraController;
        #endregion

        #region Bridge interface.
        [BindBridgeSubscribe]
        protected void Subscribe(ILogicPersonManager logicCharacterManager)
        {
            logic = logicCharacterManager;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(ILogicPersonManager logicCharacterManager)
        {
            logic = null;
        }
        #endregion

        #region ICharacterInteractionProvider events.
        public event OnCharacterSelected onCharacterSelected;
        public event OnCharacterDeselected onCharacterDeselected;
        public event OnCharacterCreated onCharacterCreated;
        public event OnCharacterRemoved onCharacterRemoved;
        public event OnStartExercise onStartExercise;
        public event OnSwitchExercise onSwitchExercise;
        public event OnEndExercise onEndExercise;
        #endregion

        #region ICharacterInteractionProvider properties.
        int ICharacterInteractionProvider.selectedCharacterId
        {
            get
            {
                return logic.selectedCharacterId;
            }
        }
        #endregion

        #region Unity API.
        protected void Awake()
        {
            runtimeCharacterSetupData = PrepareRuntimeCharactersSetupData(charactersSetupDataSet.charactersSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData);

            cameraController = Camera.main.GetComponent<CameraController>();
            Assert.IsNotNull(cameraController);

            // Setup us as click handler on floor collider.
            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onRoomDeactivatedEvent += OnRoomDeactivatedEvent;
                clubInteractionProvider.onRoomActivatedEvent += OnRoomActivatedEvent;
            }
        }

        protected void Start()
        {
        }

        protected void OnDestroy()
        {
            if (clubInteractionProvider != null)
            {
                clubInteractionProvider.onRoomDeactivatedEvent -= OnRoomDeactivatedEvent;
                clubInteractionProvider.onRoomActivatedEvent -= OnRoomActivatedEvent;
            }
        }
        #endregion

        #region IPresentationCharacterManager interface.
        public virtual void CreateCharacterRequest(int id, IPersonInfo personInfo, CharacterDestination spawnDestination)
        {
            Assert.IsNotNull(clubInteractionProvider);
            Assert.IsFalse(actors.ContainsKey(id));
            if (!actors.ContainsKey(id))
            {
                var room = clubInteractionProvider.GetRoom(personInfo.roomName);
                Assert.IsNotNull(room);

                switch (spawnDestination.mode)
                {
                    case CharacterDestination.Mode.Coordinates:
                    {
                        var worldPosition = room.roomConductor.roomObject.transform.TransformPoint
                            (room.roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates ( spawnDestination.coordinates ));
                        var rotation = Quaternion.AngleAxis(spawnDestination.heading, Vector3.up);
                        //Debug.LogFormat("Spawn: {0} ({1})", worldPosition, spawnDestination.coordinates);
                        DoCreateCharacter(id, personInfo, room, worldPosition, rotation);
                    }; break;

                    case CharacterDestination.Mode.Zone:
                    {
                        if (spawnDestination.zoneName != null &&
                            spawnDestination.zoneName != string.Empty)
                        {
                            Vector3 worldPosition;
                            Quaternion rotation;
                            if (room.GenerateRandomPointInsideZone(spawnDestination.zoneName, out worldPosition, out rotation))
                            {
                                DoCreateCharacter(id, personInfo, room, worldPosition, rotation);
                                return;
                            }
                            else
                            {
                                Debug.LogErrorFormat("Room Zone named '{0}' not found in room '{1}'.", spawnDestination.zoneName, room.roomConductor.roomObject.name);
                            }
                        }
                    }; break;

                    default:
                    {
                        Debug.LogWarning("CharacterDestination for spawning must be in modes \"Coordinates\" or \"Zone\", spawning at the center of room.");

                        var worldPosition = room.roomConductor.roomObject.transform.TransformPoint(room.roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates ( spawnDestination.coordinates ));
                        if (room.GetZoneCenter(CharacterDestination.wholeRoom, out worldPosition))
                        {
                            var rotation = Quaternion.AngleAxis(spawnDestination.heading, Vector3.up);
                            DoCreateCharacter(id, personInfo, room, worldPosition, rotation);
                            return;
                        }

                    }; break;
                }
            }
        }

        public virtual void DestroyCharacterRequest(int id)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.Destroy();
                actor.onCharacterActorPositionUpdated -= OnCharacterActorPositionUpdated;
                actors.Remove(id);

                if (onCharacterRemoved != null)
                    onCharacterRemoved(id);
            }
        }

        public virtual void MoveCharacterRequest
            (int id, CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo, Action<int, MovementResult> onComplete)
        {
            Assert.IsNotNull(clubInteractionProvider);
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.Move (destination, movementSettingsInfo, onComplete);
            }
            else
            {
                Debug.LogErrorFormat("Character actor with id {0} not found! Cannot move to destination.", id);
            }
        }

        public virtual void StopCharacterRequest(int id)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.StopMovement();
            }
        }

        public virtual void ChangeCharacterMovementModeRequest(int id, MovementMode mode)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.ChangeMovementMode(mode);
            }
        }

        public virtual void TransferCharacterRequest(int id, string newRoomName)
        {
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                var roomView = clubInteractionProvider.GetRoom(newRoomName);
                Assert.IsNotNull(roomView);
                Assert.IsNotNull(roomView.roomConductor);

                if (roomView != null)
                {
                    actor.TransferToRoom(roomView.roomConductor);
                    actor.SetDisplayMode(roomView.actorsDisplayMode);
                    UpdateOverheadUI(actor);
                }
            }
        }

        public virtual void StartExerciseRequest
            (int id, int equipmentId, int exerciseTier, int exerciseAnimation, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed)
        {
            Assert.IsTrue(actors.ContainsKey(id));
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.QueueEquipmentExercise(equipmentId, equipmentInteractionProvider, exerciseTier, exerciseAnimation, initiator, duration, flags, speed);

                UpdateOverheadUI(actor);

                SendStartExerciseEvent(actor.personInfo, equipmentId, exerciseAnimation, initiator, duration, flags, speed);
            }
        }

        public virtual void SwitchExerciseRequest (int id, int exerciseTier, int exerciseAnimation, float speed)
        {
            Assert.IsTrue(actors.ContainsKey(id));
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.QueueSwitchEquipmentExercise(equipmentInteractionProvider, exerciseTier, exerciseAnimation, speed);

                actor.RemoveOverheadUIIcon(); // A bit of hack.
                DelayedInvoke(() => UpdateOverheadUI(id));

                if (onSwitchExercise != null)
                    onSwitchExercise.Invoke(actor.personInfo, exerciseAnimation, speed);
            }
        }

        public virtual void EndExerciseRequest
            (int id, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            Assert.IsTrue(actors.ContainsKey(id));
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.QueueEquipmentExerciseEnd(equipmentId, equipmentInteractionProvider, flags);

                var questStage = logic.GetQuestStage(id);
                if (questStage != null)
                {
                    if (questStage.Value == QuestStage.InProgress)
                    {
                        if ((flags & ExerciseFlags.Skipped) != 0)
                            logic.SelectCharacterRequest(id);

                        actor.QueuePlayEmoteAnimation(AnimationSocial.Victory, 1, false, null);
                    }
                    else if (questStage.Value == QuestStage.Completed)
                    {
                        actor.QueuePlayEmoteAnimation(AnimationSocial.VictoryJump, -1, true, null);
                    }
                }

                UpdateOverheadUI(actor);

                SendEndExerciseEvent(actor.personInfo, equipmentId, exercise, initiator, flags);
            }
        }

        public virtual void PlayEmoteAnimationRequest
            (int id, AnimationSocial animationId, int loopCount, bool rotateTowardsCamera, Action onComplete)
        {
            Assert.IsTrue(actors.ContainsKey(id));
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.personInfo is IPersonTrainerInfo || actor.personInfo is IPersonTrainerClientInfo)
                {
                    Debug.LogFormat("Play emote request: Person: {0} ({1}), animation: {2}, loop count: {3}, rotate towards camera: {4}."
                        , id, actor.personInfo.name, animationId, loopCount, rotateTowardsCamera);
                }

                actor.QueuePlayEmoteAnimation(animationId, loopCount, rotateTowardsCamera, onComplete);
            }
        }

        public virtual void PersonLookAtRequest
            (int id, CharacterDestination lookAtDestination, Action onComplete)
        {
            Assert.IsNotNull(clubInteractionProvider);
            Assert.IsTrue(actors.ContainsKey(id));

            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.personInfo is IPersonTrainerInfo || actor.personInfo is IPersonTrainerClientInfo)
                {
                    Debug.LogFormat("Look at request: Person: {0} ({1}), destination: {2}.", id, actor.personInfo.name, lookAtDestination);
                }

                switch (lookAtDestination.mode)
                {
                    case CharacterDestination.Mode.Coordinates:
                    {
                        var newPosition = actor.roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(lookAtDestination.coordinates);
                        var position = actor.roomConductor.roomObject.transform.TransformPoint(newPosition);
                        actor.QueueLookAt(position, onComplete);
                        return;
                    };

                    case CharacterDestination.Mode.Equipment:
                    {
                        IEquipmentActor equipmentActor = equipmentInteractionProvider.GetEquipmentActor(lookAtDestination.equipmentId);
                        if (equipmentActor != null)
                        {
                            var navPointNamePrefix = lookAtDestination.navPointNamePrefix;

                            if (navPointNamePrefix != null)
                            {
                                var np = equipmentActor.GetClosestNavigationPoint(actor.position, actor.navigationAreaMask, navPointNamePrefix);
                                actor.QueueLookAt(np.position, onComplete);
                            }
                            else
                            {
                                actor.QueueLookAt(equipmentActor.position, onComplete);
                            }

                            return;
                        }
                        else
                        {
                            Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot look at equipment.", lookAtDestination.equipmentId);
                        }
                    }; break;

                    case CharacterDestination.Mode.Person:
                    {
                        ICharacterActor otherActor;
                        if (actors.TryGetValue(lookAtDestination.characterId, out otherActor))
                        {
                            actor.QueueLookAt(otherActor.position, onComplete);
                            return;
                        }
                        else
                        {
                            Debug.LogErrorFormat("Character actor with id {0} not found! Cannot look at actor.", lookAtDestination.characterId);
                        }
                    }; break;

                    case CharacterDestination.Mode.Zone:
                    {
                        var roomView = clubInteractionProvider.GetRoom(actor.personInfo.roomName);
                        if (roomView != null)
                        {
                            Vector3 position;
                            if (roomView.GetZoneCenter(lookAtDestination.zoneName, out position))
                            {
                                actor.QueueLookAt(position, onComplete);
                                return;
                            }
                            else
                            {
                                Debug.LogErrorFormat("Room Zone named '{0}' not found in room '{1}'.", lookAtDestination.zoneName, actor.roomConductor.roomObject.name);
                            }
                        }
                        else
                        {
                            Debug.LogErrorFormat("Cannot find room '{0}'.", actor.personInfo.roomName);
                        }
                    }; break;
                }
            }
            else
            {
                Debug.LogErrorFormat("Character actor with id {0} not found! Cannot look at destination.", id);
            }

            onComplete?.Invoke(); // Emergency fall-back.
        }
        #endregion

        #region ICharacterActorSharedDataProvider interface.
        public virtual GameObject GetCursorObject(CharacterCursorType type)
        {
            return null;
        }

        public bool GetWorldPositionForDestination (ICharacterActor actor, CharacterDestination destination, out Vector3 worldPosition)
        {
            Assert.IsNotNull(clubInteractionProvider);
            Assert.IsNotNull(equipmentInteractionProvider);

            switch (destination.mode)
            {
                case CharacterDestination.Mode.Coordinates:
                {
                    worldPosition = actor.roomConductor.roomObject.transform.TransformPoint(actor.roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(destination.coordinates));
                    return true;
                };

                case CharacterDestination.Mode.Equipment:
                {
                    IEquipmentActor equipmentActor = equipmentInteractionProvider.GetEquipmentActor(destination.equipmentId);
                    if (equipmentActor != null)
                    {
                        var navPointNamePrefix = destination.navPointNamePrefix;

                        var navPoint = (navPointNamePrefix != null)
                                ? equipmentActor.GetClosestNavigationPoint(actor.position, actor.navigationAreaMask, navPointNamePrefix)
                                : equipmentActor.GetClosestApproachingPoint(actor.position, actor.navigationAreaMask, destination.exerciseTier);

                        worldPosition = navPoint.position;
                        return true;
                    }
                    else
                    {
                        Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot move to equipment.", destination.equipmentId);
                    }
                }; break;

                case CharacterDestination.Mode.Person:
                {
                    ICharacterActor otherActor;
                    if (actors.TryGetValue(destination.characterId, out otherActor))
                    {
                        worldPosition = otherActor.GetNearestWorldPoint(actor.position, actor.navigationAgentRadius, destination.distanceMultiplier);
                        return true;
                    }
                    else
                    {
                        Debug.LogErrorFormat("Character actor with id {0} not found! Cannot move to character actor.", destination.characterId);
                    }
                }; break;

                case CharacterDestination.Mode.Zone:
                {
                    var roomView = clubInteractionProvider.GetRoom(actor.personInfo.roomName);
                    if (roomView != null)
                    {
                        if (roomView.GenerateRandomPointInsideZone(destination.zoneName, out worldPosition))
                        {
                            return true;
                        }
                        else
                        {
                            Debug.LogErrorFormat("Room Zone named '{0}' not found in room '{1}'.", destination.zoneName, actor.roomConductor.roomSetupDataInfo.name);
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat("Cannot find room '{1}'.", actor.personInfo.roomName);
                    }
                }; break;
            }

            worldPosition = default(Vector3);
            return false;
        }
        #endregion

        #region ICharacterInteractionProvider interface.
        public virtual void KickVisitor(int id)
        {
        }

        public virtual void ClaimQuest(int id)
        {
        }

        ICharacterActor ICharacterInteractionProvider.GetCharacterActor(int id)
        {
            ICharacterActor actor;
            return (id != PersonData.InvalidId && actors.TryGetValue(id, out actor)) ? actor : null;
        }

        public virtual bool SelectCharacter(int id)
        {
            return false;
        }

        public virtual void DeselectCharacter(int id)
        {
        }

        CharacterPortrait ICharacterInteractionProvider.GetPortrait(int id)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                return actor.GetPortrait();
            }

            return null;
        }

        CharacterPortrait ICharacterInteractionProvider.GetPortrait(PersonAppearance appearance, string typeName)
        {
            var rcsd = FindRuntimeCharacterSetupData(appearance, typeName);
            if (rcsd != null)
            {
                return rcsd.CreatePortrait(transform, appearance, actorSettings.portraitMaterialPolicyName);
            }

            return null;
        }

        public void UpdateOverheadUI(int actorId)
        {
            Assert.IsTrue(actors.ContainsKey(actorId));

            ICharacterActor actor;
            if (actors.TryGetValue(actorId, out actor))
            {
                UpdateOverheadUI(actor);
            }
        }
        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("CharacterStateChangedEvent")]
        void OnCharacterStateChangedEvent(int id, PersonState oldState, PersonState newState)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.personInfo is IPersonTrainerInfo)
                {
                    if (oldState == PersonState.Refused || newState == PersonState.Refused)
                    {
                        UpdateOverheadUI(actor);
                    }
                }
            }
        }

        [BindBridgeEvent("CharacterEnteredRoomEvent")]
        private void OnCharacterEnteredRoomEvent(int id)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                // Play sound at 1/4 of volume if it's happening not in active room.
                Sound.instance.NewVisitor(actor.personInfo.roomName == clubInteractionProvider.activeRoomName ? 1.0f : 0.25f);

                //Debug.LogWarningFormat("Sending Greeting. Id: {0}, Name: '{1}'.", id, actor.personInfo.name);
                actor.QueuePlayEmoteAnimation(AnimationSocial.Greeting, 1, true, null);
                UpdateOverheadUI(actor);

                if (actor.personInfo is IPersonTrainerInfo)
                {
                    OnTrainerEnter(id);
                }
            }
        }

        [BindBridgeEvent("CharacterLeavingRoomEvent")]
        private void OnCharacterLeavingRoomEvent(int id, bool kicked)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                if (actor.personInfo is IPersonVisitorInfo)
                {
                    if (kicked)
                    {
                        actor.RemoveOverheadUIIcon();
                    }
                    else
                    {
                        ShowHintOverheadUIIcon
                            ( actor
                            , DataTables.instance.personsPhrases[UnityEngine.Random.Range(0, DataTables.instance.personsPhrases.Length)].phrase_text
                            , 2.5f);
                    }
                }
                else
                {
                    UpdateOverheadUI(actor);
                }
            }
        }

        [BindBridgeEvent("CharacterTypeChangedEvent")]
        private void OnCharacterTypeChangedEvent(int id, IPersonInfo personInfo)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.UpdateCharacterType(personInfo);

                UpdateOverheadUI(actor);
            }
        }

        [BindBridgeEvent("CharacterAppearanceChangedEvent")]
        private void OnCharacterAppearanceChangedEvent(int id)
        {
            ICharacterActor actor;
            if (actors.TryGetValue(id, out actor))
            {
                actor.UpdateAppearance();
            }
        }
        #endregion

        #region Protected virtual API.
        protected virtual void OnCharacterClick(int actorId, PointerEventData eventData)
        {
        }

        protected virtual void UpdateOverheadUI(ICharacterActor actor)
        {
        }

        protected virtual void OnTrainerEnter(int id)
        {
        }
        #endregion

        #region Protected API.
        protected void SendCharacterSelectedEvent (IPersonInfo personInfo, bool focus)
        {
            onCharacterSelected?.Invoke(personInfo, focus);
        }

        protected void SendCharacterDeselectedEvent (IPersonInfo personInfo)
        {
            onCharacterDeselected?.Invoke(personInfo);

            gui.hud.personPanel.OnCharacterDeselected();
        }

        protected void SendCharacterCreatedEvent (IPersonInfo personInfo)
        {
            onCharacterCreated?.Invoke(personInfo);
        }

        protected void SendCharacterRemovedEvent (int id)
        {
            onCharacterRemoved?.Invoke(id);
        }

        protected void SendStartExerciseEvent (IPersonInfo personInfo, int equipmentId, int exerciseAnimation, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed)
        {
            onStartExercise?.Invoke(personInfo, equipmentId, exerciseAnimation, initiator, duration, flags, speed);
        }

        protected void SendSwitchExerciseEvent (IPersonInfo personInfo, int exerciseAnimation, float speed)
        {
            onSwitchExercise?.Invoke(personInfo, exerciseAnimation, speed);
        }

        protected void SendEndExerciseEvent (IPersonInfo personInfo, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            onEndExercise?.Invoke(personInfo, equipmentId, exercise, initiator, flags);
        }

        protected IEnumerator UpdateOverheadUIAfterDelayJob(int actorId, float delay)
        {
            yield return new WaitForSeconds(delay);

            UpdateOverheadUI(actorId);
        }

        protected void ShowHintOverheadUIIcon(ICharacterActor actor, string text, float autoHideDelay = -1.0f, Action onAutoHide = null)
        {
            var hintTextIcon = new OverheadIconInstanceHolder<HintTextIcon>(hintTextIconPrefab);

            if (autoHideDelay >= 0)
            {
                hintTextIcon.icon.autoHideDelay = autoHideDelay;
            }
            hintTextIcon.instance.txtHint.text = text;
            hintTextIcon.icon.frontIcon = (actor.id == logic.selectedCharacterId);
            hintTextIcon.icon.onAutoHide = onAutoHide;
            actor.SetOverheadUIIcon(hintTextIcon);
        }
        #endregion

        #region Private functions.
        void DoCreateCharacter(int id, IPersonInfo personInfo, IRoomView room, Vector3 worldPosition, Quaternion rotation)
        {
            var runtimeCharacterSetupData = FindRuntimeCharacterSetupData(personInfo.appearance, personInfo.GetType().Name);
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            Assert.IsNotNull(runtimeCharacterSetupData.csd.characterPrefab);

            var actorObject = Instantiate(runtimeCharacterSetupData.csd.characterPrefab);
            Assert.IsNotNull(actorObject);

            if (actorObject != null)
            {
                actorObject.name = string.Format(nameFormatString, id, personInfo.name.Trim(nameTrimChars));

                var actor = actorObject.GetComponent<ICharacterActor>();
                if (actor != null)
                {
                    actor.onCharacterActorPositionUpdated += OnCharacterActorPositionUpdated;

                    actor.pointerClickEvent.AddListener(OnCharacterClick);

                    actor.Init(this, id, personInfo, room.roomConductor, room.actorsDisplayMode, runtimeCharacterSetupData, actorSettings, worldPosition, rotation);
                    actors.Add(id, actor);

                    if (personInfo.state == PersonState.ExerciseComplete)
                    {
                        var questStage = logic.GetQuestStage(id);
                        if (questStage != null && questStage.Value == QuestStage.Completed)
                        {
                            actor.QueuePlayEmoteAnimation(AnimationSocial.VictoryJump, -1, true, null);
                        }
                    }

                    UpdateOverheadUI(actor);

                    onCharacterCreated?.Invoke(personInfo);
                }
            }
        }

        RuntimeCharacterSetupData FindRuntimeCharacterSetupData(PersonAppearance appearance, string typeName)
        {
            Assert.IsTrue(charactersSetupDataSet.charactersSetupData.Length == runtimeCharacterSetupData.Length);

            for (var i = 0; i < charactersSetupDataSet.charactersSetupData.Length; i++)
            {
                if (charactersSetupDataSet.charactersSetupData[i].characterTypeKey.MatchPerson(appearance, typeName))
                {
                    return runtimeCharacterSetupData[i];
                }
            }

            return null;
        }

        void OnRoomActivatedEvent(string roomName)
        {
            UpdateActorsDisplayMode(roomName);
        }

        void OnRoomDeactivatedEvent(string roomName)
        {
            UpdateActorsDisplayMode(roomName);
        }

        void OnCharacterActorPositionUpdated(int id, RoomCoordinates position, float heading)
        {
            onCharacterPositionUpdated?.Invoke(id, position, heading);
        }

        void UpdateActorsDisplayMode(string roomName)
        {
            var room = clubInteractionProvider.GetRoom(roomName);
            Assert.IsNotNull(room);
            var mode = room.actorsDisplayMode;

            foreach (var actor in actors.Values)
            {
                if (actor.personInfo.roomName == roomName)
                {
                    actor.SetDisplayMode(mode);
                }
            }
        }
        #endregion

        #region Private static functions.
        private static RuntimeCharacterSetupData[] PrepareRuntimeCharactersSetupData(CharacterSetupData[] charactersSetupData)
        {
            if (charactersSetupData.Length > 0)
            {
                var runtimeCharacterSetupData = new RuntimeCharacterSetupData[charactersSetupData.Length];

                for (var i = 0; i < charactersSetupData.Length; i++)
                {
                    var runtimeCharacterSetup = PrepareRuntimeCharacterSetupData(charactersSetupData[i], i);
                    if (runtimeCharacterSetup != null)
                    {
                        runtimeCharacterSetupData[i] = runtimeCharacterSetup;
                    }
                    else
                    {
                        Debug.LogError(string.Format("Character Manager: Failed to initialize runtime character data for character at position {0}.", i));
                    }
                }

                return runtimeCharacterSetupData;
            }
            else
            {
                Debug.LogWarning("Character Manager: No character setup data present.");
            }
            return null;
        }

        private static RuntimeCharacterSetupData PrepareRuntimeCharacterSetupData(CharacterSetupData csd, int characterIdx)
        {
            RuntimeCharacterSetupData rcsd = new RuntimeCharacterSetupData(csd);

            Assert.IsTrue(csd.animationCategoryParameter.Length > 0, string.Format("Character Manager: Wrong Character setup for character at position {0}: Empty 'Animation Category Parameter' field.", characterIdx));
            if (csd.animationCategoryParameter.Length > 0)
            {
                rcsd.animationCategoryParameterHash = Animator.StringToHash(csd.animationCategoryParameter);
            }

            Assert.IsTrue(csd.animationSelectorParameter.Length > 0, string.Format("Character Manager: Wrong Character setup for character at position {0}: Empty 'Animation Selector Parameter' field.", characterIdx));
            if (csd.animationSelectorParameter.Length > 0)
            {
                rcsd.animationSelectorParameterHash = Animator.StringToHash(csd.animationSelectorParameter);
            }

            Assert.IsTrue(csd.animationForceSwitchParameter.Length > 0, string.Format("Character Manager: Wrong Character setup for character at position {0}: Empty 'Animation Force Switch Parameter' field.", characterIdx));
            if (csd.animationForceSwitchParameter.Length > 0)
            {
                rcsd.animationForceSwitchParameterHash = Animator.StringToHash(csd.animationForceSwitchParameter);
            }

            if (csd.bodyParts != null && csd.bodyParts.Length > 0)
            {
                rcsd.bodyParts = new RuntimeCharacterSetupData.RuntimeBodyPartData[csd.bodyParts.Length];

                for (var bpIdx = 0; bpIdx < csd.bodyParts.Length; bpIdx++)
                {
                    rcsd.bodyParts[bpIdx] = PrepareAllRuntimeBodyPartData(csd, characterIdx, (PersonAppearance.BodyPart.PartType) bpIdx);
                }
            }

            if (csd.animationsSetup != null && csd.animationsSetup.Length > 0)
            {
                rcsd.animations = new AnimationClip[csd.animationsSetup.Length][];
                for (var i = 0; i < csd.animationsSetup.Length; i++)
                {
                    rcsd.animations[i] = GetAnimationsForCategory(csd, i, characterIdx);
                }
            }

            rcsd.normalWalkAreaMask = RuntimeCharacterSetupData.GetNavigationAreaMask(csd.normalWalkNavigationAreas);
            rcsd.transferAreaMask = RuntimeCharacterSetupData.GetNavigationAreaMask(csd.transferNavigationAreas);

            return rcsd;
        }

        private static RuntimeCharacterSetupData.RuntimeBodyPartData PrepareAllRuntimeBodyPartData(CharacterSetupData csd, int characterIdx, PersonAppearance.BodyPart.PartType bodyPartType)
        {
            if (csd.bodyParts != null && (int) bodyPartType < csd.bodyParts.Length)
            {
                var bp = csd.bodyParts[(int) bodyPartType];
                var rbpd = new RuntimeCharacterSetupData.RuntimeBodyPartData();

                if (bp.appearanceSets != null && bp.appearanceSets.Length > 0)
                {
                    rbpd.partSets = new GameObject[bp.appearanceSets.Length][];
                    for (var i = 0; i < bp.appearanceSets.Length; i++)
                    {
                        rbpd.partSets[i] = PrepareRuntimeBodyPartData(bp, bodyPartType, characterIdx, i);
                    }
                }

                if (bp.texturesList != null)
                {
                    rbpd.partTextures = bp.texturesList.array;
                }

                return rbpd;
            }

            return new RuntimeCharacterSetupData.RuntimeBodyPartData();
        }

        private static GameObject[] PrepareRuntimeBodyPartData(CharacterSetupData.BodyPart bp, PersonAppearance.BodyPart.PartType bodyPartType, int characterIdx, int idx)
        {
            if (idx < bp.appearanceSets.Length)
            {
                var objectsArrayObj = bp.appearanceSets[idx];
                Assert.IsNotNull(objectsArrayObj, string.Format("Character Manager: Wrong Character setup for character at position {0}: missing '{1}' data - null reference in array at index {2}.", characterIdx, bodyPartType.ToString(), idx));
                if (objectsArrayObj != null)
                {
                    return objectsArrayObj.array;
                }
            }
            return null;
        }

        private static AnimationClip[] GetAnimationsForCategory(CharacterSetupData csd, int categoryIdx, int idx)
        {
            Assert.IsTrue(categoryIdx < csd.animationsSetup.Length);
            if (categoryIdx < csd.animationsSetup.Length)
            {
                var animationsSetup = csd.animationsSetup[categoryIdx];

                Assert.IsTrue(animationsSetup.mode != CharacterSetupData.AnimationsSetupData.Mode.OverrideAnimationClip || animationsSetup.overrideClipName.Length > 0, string.Format("Character Manager: Wrong Character setup for character at position {0}: Animation setup has mode {1} and empty 'overrideClipName' field.", idx, animationsSetup.mode.ToString()));

                if (animationsSetup.overrideAnimations != null)
                {
                    return animationsSetup.overrideAnimations.array;
                }
            }
            return null;
        }
        #endregion
    }
}