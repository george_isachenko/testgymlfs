﻿using BridgeBinder;
using Core.Timer;
using Data;
using Data.Room;
using Logic.Facades;
using Logic.PlayerProfile.Events;
using Logic.Quests.Events;
using Logic.Quests.SportQuest.SportLaneQuest;
using Logic.Rooms.Facade;
using Presentation.Helpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Rooms;
using View.UI.OverheadUI;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Club Game")]
    public class PresentationClubGame : PresentationClubBase
    {
        #region Public (Inspector) data.
        public LayerMask expandPreparationRaycastMask;
        #endregion

        #region Private data.
        protected LayerMask? originalRaycasterMask;
        protected IRoomView currentExpandPrepareRoom = null;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected new void Subscribe(ILogicRoomManager logic)
        {
            base.Subscribe(logic);

            logic.onRoomsExpandAvailabilityUpdatedEvent += OnRoomsExpandAvailabilityUpdatedEvent;
            logic.onRoomUnlockCompleteEvent += OnRoomUnlockCompleteEvent;
        }

        [BindBridgeUnsubscribe]
        protected new void Unsubscribe(ILogicRoomManager logic)
        {
            logic.onRoomsExpandAvailabilityUpdatedEvent -= OnRoomsExpandAvailabilityUpdatedEvent;
            logic.onRoomUnlockCompleteEvent -= OnRoomUnlockCompleteEvent;

            base.Unsubscribe(logic);
        }
        #endregion

        #region Unity API.
        protected new void Start()
        {
            base.Start();

            foreach (var room in rooms.Values)
            {
                room.transitionToRoomCheckRequest   += OnTransitionToRoomCheckRequest;
                room.roomUnlockRequest              += OnRoomUnlockRequest;
                room.startExpandRequest             += OnStartExpandRequest;
                room.prepareToExpandRequest         += OnPrepareToExpandRequest;

                room.onCannotExpand                 += OnCannotExpand;
            }

            LevelUpEvent.Event          += OnLevelUp;
            LaneQuestClaimedEvent.Event += LaneQuestClaimed;
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            Assert.IsNotNull(rooms);
            foreach (var room in rooms.Values)
            {
                room.transitionToRoomCheckRequest   -= OnTransitionToRoomCheckRequest;
                room.roomUnlockRequest              -= OnRoomUnlockRequest;
                room.startExpandRequest             -= OnStartExpandRequest;
                room.prepareToExpandRequest         -= OnPrepareToExpandRequest;

                room.onCannotExpand                 -= OnCannotExpand;
            }

            LevelUpEvent.Event         -= OnLevelUp;
            LaneQuestClaimedEvent.Event -= LaneQuestClaimed;
        }
        #endregion

        #region Public API.
        public override void ShowGymUnlockMessage(string title, string message, string roomName, TimerFloat timer)
        {
            var viewData = new GymUnlockViewData(roomName, title, message, timer);
            gui.gymUnlock.UpdateView(viewData, OnGymUnlockPressed);
            gui.gymUnlock.Show();
        }

        public override void ShowGymUnlockMessage(string title, string message, string roomName, Cost cost)
        {
            var viewData = new GymUnlockViewData(roomName, title, message, cost, playerProfileInteractionProvider);
            gui.gymUnlock.UpdateView(viewData, OnGymUnlockPressed);
            gui.gymUnlock.Show();
        }
        #endregion

        #region Bridge events.
        protected override void OnRoomDeactivatedEvent(string roomName)
        {
            CancelExpandPrepare();

            base.OnRoomDeactivatedEvent(roomName);

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                gui.hud.SetHUDMode(View.UI.HUD.HUD.HUDMode.None);

                if (gui.windows.GetCurrentType() != View.UI.WindowTypes.BuyWithResources)
                    gui.windows.ShowDefault();
            }
        }

        protected override void OnRoomActivatedEvent(string roomName)
        {
            CancelExpandPrepare();

            base.OnRoomActivatedEvent(roomName);

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                var roomType = roomView.roomConductor.roomSetupDataInfo.roomType;

                if (roomType == RoomType.SportClub)
                {
                    gui.hud.SetHUDMode(View.UI.HUD.HUD.HUDMode.SportClub);
                }
                else if (roomType == RoomType.Club)
                {
                    gui.hud.SetHUDMode(View.UI.HUD.HUD.HUDMode.Club);
                }
                else if (roomType == RoomType.JuiceBar)
                {
                    gui.hud.SetHUDMode(View.UI.HUD.HUD.HUDMode.JuiceBar);
                }
            }
        }

        private void OnRoomsExpandAvailabilityUpdatedEvent()
        {
            foreach (var roomView in rooms)
            {
                ExpandAvailabilityState state = ExpandAvailabilityState.None;
                float timeLeft = 0;

                var unlockTimer = logic.GetUnlockTimer(roomView.Key);
                if (unlockTimer != null)
                {
                    if (unlockTimer.secondsToFinish <= 0)
                    {
                        state = ExpandAvailabilityState.ExpandComplete;
                    }
                    else
                    {
                        state = ExpandAvailabilityState.ExpandPending;
                        timeLeft = unlockTimer.secondsToFinish;
                    }
                }
                else
                {
                    var expandStage = logic.GetRoomExpandStage(roomView.Key);

                    if (expandStage == 0)
                    {
                        var nextExpandUnlockLevel = logic.GetNextExpandUnlockLevel(roomView.Key);
                        if (nextExpandUnlockLevel > 0 && playerProfileInteractionProvider.level >= nextExpandUnlockLevel)
                        {
                            state = ExpandAvailabilityState.ExpandAvailable;
                        }
                    }
                    else
                    {
                        var expandCost = logic.GetExpandCost(roomView.Key);
                        if (expandCost != null /* && playerProfileInteractionProvider.CanSpend(expandCost, false)*/)
                            state = ExpandAvailabilityState.ExpandAvailable;
                    }
                }

                roomView.Value.SetExpandAvailableState(state, timeLeft);
            }
        }

        private void OnRoomUnlockCompleteEvent(string roomName)
        {
            if (_activeRoomName != roomName && activeRoomCandidates.Contains(roomName))
            {
                logic.ActivateRoomRequest(roomName);
            }
            else
            {
                IRoomView roomView;
                if (rooms.TryGetValue(roomName, out roomView))
                {
                    roomView.TransitionToUnlockedRoom();
                }
            }
        }

        #endregion

        #region Private functions.
        private void EndExpandPrepareState()
        {
            gui.hud.ShowAll();
            gui.windows.states.inEditMode = false;
            OverheadUIManager.instance.SetDefaultDisplayMode(true);
            if (originalRaycasterMask != null)
            {
                var raycaster = Camera.main.gameObject.GetComponent<PhysicsRaycaster>();
                if (raycaster != null)
                {
                    raycaster.eventMask = originalRaycasterMask.Value;
                }
                originalRaycasterMask = null;
            }
        }

        private void ShowExpandConfirm(string roomName, Cost cost, int blockIndex)
        {
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView) &&
                    (roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.BlockGrid ||
                     roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.SideBySide))
            {
                var newSize = logic.GetRoomSizeInCells(roomName, logic.GetRoomExpandStage(roomName));

                if (newSize > 0)
                {
                    var view = PresentationBuyHelper.BuyWithResources
                        ( gui
                        , playerProfileInteractionProvider
                        , cost
                        , delegate ()
                        {
                            BuyExpand(roomView, roomName, blockIndex);
                        });

                    Assert.IsNotNull(view);

                    if (view != null)
                    {
                        view.header = Loc.Get("expandHeader");
                        view.OnClosedByUserClick_Callback = roomView.ExpandEnded;

                        var description = view.IsolateHeader("expandGym");

                        int newStyle = logic.roomCapacity.GetStylePointsNextExpandLevel(roomName);

                        description.SetText("newSize", newSize.ToString());
                        description.SetText("newStyle", newStyle.ToString());

                        description.SetText("expandNewGymSize", Loc.Get("expandNewGymSize"));
                        description.SetText("expandStyleReward", Loc.Get("expandStyleReward"));
                
                        view.actionButtonPrefixText = Loc.Get("idExpand");
                    }
                }
            }
        }

        private void BuyExpand(IRoomView room, string roomName, int blockIndex)
        {
            if (OnExpandConfirmed(roomName, blockIndex))
            {
                gui.buyWithResources.Hide();
            }
            else
            {
                room.ExpandEnded();
            }
        }

        private void OnCannotExpand()
        {
            if (!logic.isEditMode)
                gui.confirmWnd.OnCantExpandConfirm(logic.GetNextExpandUnlockLevel("Club"));
        }

        private void CancelExpandPrepare()
        {
            if (currentExpandPrepareRoom != null)
            {
                var tmpRoom = currentExpandPrepareRoom;
                currentExpandPrepareRoom = null;
                EndExpandPrepareState();
                tmpRoom.ExpandEnded();
            }
        }

        #endregion

        #region Rooms events handlers.
        private bool OnPrepareToExpandRequest(string roomName)
        {
            if (!logic.isEditMode)
            {
                IRoomView roomView;
                if (rooms.TryGetValue(roomName, out roomView))
                {
                    currentExpandPrepareRoom = roomView;

                    if (characterInteractionProvider.selectedCharacterId != Data.Person.PersonData.InvalidId)
                        characterInteractionProvider.DeselectCharacter(characterInteractionProvider.selectedCharacterId);

                    gui.hud.ShowBackButton(CancelExpandPrepare);

                    // Begin Expand Prepare State.
                    {
                        gui.windows.states.inEditMode = true;
                        OverheadUIManager.instance.SetDefaultDisplayMode(false);
                        var raycaster = Camera.main.gameObject.GetComponent<PhysicsRaycaster>();
                        if (raycaster != null)
                        {
                            originalRaycasterMask = raycaster.eventMask;
                            raycaster.eventMask = expandPreparationRaycastMask;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private void OnStartExpandRequest(string roomName, int blockIndex)
        {
            currentExpandPrepareRoom = null;
            EndExpandPrepareState();

            if (!logic.isEditMode)
            {
                IRoomView roomView;
                if (rooms.TryGetValue(roomName, out roomView))
                {
                    //var expandCostX = logicClub.GetExpandCostX(roomId);
                    //var expandCostY = logicClub.GetExpandCostY(roomId);

                    var expandCost = logic.GetExpandCost(roomName);

                    bool canExpandX = logic.CanExpandX (roomName);
                    bool canExoandY = logic.CanExpandY (roomName);

                    if (expandCost != null)
                    {
                        if (roomView.CheckForExpandStart(canExpandX, canExoandY, blockIndex))
                        {
                            ShowExpandConfirm(roomName, expandCost, blockIndex);
                        }
                    }
                    else
                    {
                        // TODO: GUI Message to user.
                        //Debug.Log("Cannot expand - at max room size!"

                        if (!logic.isEditMode)
                            gui.confirmWnd.OnCantExpandConfirm(logic.GetNextExpandUnlockLevel(roomName));
                    }
                }
            }
        }

        private bool OnExpandConfirmed(string roomName, int blockIndex)
        {
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                if (logic.TryExpandRequest(roomName, blockIndex))
                {
                    roomView.UpdateExpand();
                    UpdateStyle();
                    return true;
                }
                else
                {
                    roomView.ExpandEnded();
                }
            }
            return false;
        }

        private bool OnTransitionToRoomCheckRequest(string roomName)
        {
            if (questsInteractionProvider.isTutorialRunning)
                return false;

            if (logic.GetRoomExpandStage(roomName) > 0)
                return true;

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
//                 if (roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.Linear ||
//                     roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.Fixed)
                {
                    var nextExpandUnlockLevel = logic.GetNextExpandUnlockLevel(roomName);
                    if (nextExpandUnlockLevel < 0)
                        return false;

                    if (playerProfileInteractionProvider.level >= nextExpandUnlockLevel)
                    {
                        var unlockTimer = logic.GetUnlockTimer(roomName);
                        if (unlockTimer != null && unlockTimer.secondsToFinish <= 0)
                        {
                            return logic.CompleteUnlockRequest(roomName);
                        }
                    }
                    else
                    {
                        if (!logic.isEditMode)
                        {
                            if (playerProfileInteractionProvider.level >= nextExpandUnlockLevel)
                            {
                                roomView.ShowLockedView (Loc.Get(string.Format("unlock{0}AvailableMessage", roomName)));
                            }
                            else
                            {
                                if (!logic.isAnyTutorialRunning)
                                {
                                    roomView.ShowLockedView (Loc.Get(string.Format("unlock{0}Message", roomName), nextExpandUnlockLevel));
                                }
                            }
                        }
                    }
                }
//                 else
//                 {
//                     return true;
//                 }
            }

            return false;
        }

        private void OnRoomUnlockRequest(string roomName, int desiredExpandStage)
        {
            if (questsInteractionProvider.isTutorialRunning)
                return;

            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView) && roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.Fixed ||
                rooms.TryGetValue(roomName, out roomView) && roomView.roomConductor.roomSetupDataInfo.roomExpandMode == RoomExpandMode.Linear)
            {
                var nextExpandUnlockLevel = logic.GetNextExpandUnlockLevel(roomName);
                if (nextExpandUnlockLevel < 0)
                {   // Already at max expand?
                    gui.confirmWnd.OnCustom
                        ( Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, desiredExpandStage))
                        , Loc.Get(string.Format("unlock{0}Room{1}InavailableMessage", roomName, desiredExpandStage), logic.GetExpandUnlockLevel(roomName, desiredExpandStage)));

                    return;
                }

                var currentExpandStage = logic.GetRoomExpandStage(roomName);
                if (desiredExpandStage < 0)
                    desiredExpandStage = currentExpandStage + 1;

                if (playerProfileInteractionProvider.level >= nextExpandUnlockLevel && desiredExpandStage <= currentExpandStage + 1)
                {
                    var cost = logic.GetExpandCost(roomName);
                    if (cost == null)
                        return; // Already at max expand?

                    var unlockTimer = logic.GetUnlockTimer(roomName);

                    if (unlockTimer == null)
                    {
                        if (!logic.isEditMode)
                        {
                            if (currentExpandStage == 0)
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Header", roomName))
                                    , Loc.Get(string.Format("unlock{0}AvailableMessage", roomName))
                                    , roomName
                                    , cost);
                            }
                            else
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, desiredExpandStage))
                                    , Loc.Get(string.Format("unlock{0}Room{1}Message", roomName, desiredExpandStage))
                                    , roomName
                                    , cost);
                            }
                        }
                    }
                    else if (unlockTimer.secondsToFinish <= 0)
                    {
                        if (logic.CompleteUnlockRequest(roomName))
                        {
                        }
                        gui.gymUnlock.Hide();
                    }
                    else
                    {
                        if (!logic.isEditMode)
                        {
                            if (currentExpandStage == 0)
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Header", roomName))
                                    , Loc.Get(string.Format("unlock{0}Waiting", roomName))
                                    , roomName
                                    , unlockTimer);
                            }
                            else
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, desiredExpandStage))
                                    , Loc.Get(string.Format("unlock{0}Room{1}WaitMessage", roomName, desiredExpandStage))
                                    , roomName
                                    , unlockTimer);
                            }
                        }
                    }
                }
                else
                {
                    if (!logic.isEditMode)
                    {
                        if (currentExpandStage == 0)
                        {
                            if (playerProfileInteractionProvider.level >= nextExpandUnlockLevel)
                            {
                                roomView.ShowLockedView (Loc.Get(string.Format("unlock{0}AvailableMessage", roomName)));
                            }
                            else
                            {
                                if (!logic.isAnyTutorialRunning)
                                {
                                    roomView.ShowLockedView (Loc.Get(string.Format("unlock{0}Message", roomName), nextExpandUnlockLevel));
                                }
                            }
                        }
                        else
                        {
                            int unlockLvl = logic.GetExpandUnlockLevel(roomName, desiredExpandStage);

                            string descript = unlockLvl > 0 ? Loc.Get(string.Format("unlock{0}RoomInavailableMessage", roomName)) : Loc.Get(string.Format("unlockSportClubRoom4InavailableMessage"));

                            gui.confirmWnd.OnCustom
                                ( Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, desiredExpandStage))
                                , descript
                                ,null, false, null, unlockLvl);
                        }
                    }
                }
            }
        }

        private void OnGymUnlockPressed(string roomName)
        {
            IRoomView roomView;
            if (rooms.TryGetValue(roomName, out roomView))
            {
                var unlockTimer = logic.GetUnlockTimer(roomName);
                var expandStage = logic.GetRoomExpandStage(roomName);

                if (unlockTimer == null)
                {
                    if (logic.TryUnlockRequest(roomName))
                    {
                        unlockTimer = logic.GetUnlockTimer(roomName);

                        if (unlockTimer != null)
                        {
                            if (expandStage == 0)
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Header", roomName))
                                    , Loc.Get(string.Format("unlock{0}Waiting", roomName))
                                    , roomName
                                    , unlockTimer);
                            }
                            else
                            {
                                ShowGymUnlockMessage
                                    ( Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, expandStage + 1))
                                    , Loc.Get(string.Format("unlock{0}Room{1}Message", roomName, expandStage + 1))
                                    , roomName
                                    , unlockTimer);
                            }
                        }
                        else
                        {
                            // Instant unlock.
                            gui.gymUnlock.Hide();
                            roomView.TransitionToUnlockedRoom();
                        }
                    }
                    else
                    {
                        /*
                            gui.gymUnlock.Hide();
                        */
                    }
                }
                else
                {
                    if (logic.CompleteUnlockRequest(roomName))
                    {
                        gui.gymUnlock.Hide();
                    }

                }
            }
        }
        #endregion

        #region Player profile event handlers.
        private void OnLevelUp(int level)
        {
            UpdateStyle();
        }

        private void LaneQuestClaimed(LaneQuestLogic laneQuestLogic, int i)
        {
            UpdateFitPoints();
        }
        #endregion
    }
}