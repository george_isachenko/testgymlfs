﻿using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Data;
using Data.Estate;
using Logic.PlayerProfile;
using Logic.PlayerProfile.Facade;
using Presentation.Helpers;
using UnityEngine;
using View.UI;
using View.UI.OverheadUI.Components;
using Core;
using Core.SocialAPI;
using DataProcessing.CSV;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Player Profile Game")]
    public class PresentationPlayerProfileGame : PresentationPlayerProfileBase
    {
        #region IPlayerProfileInteractionProvider properties.
        public override LogicStorage storage
        {
            get
            {
                return logic.storage;
            }
        }
        #endregion

        #region Public (Inspector) data.
        public SingleActionIcon.Holder storageOverheadIcon;
        #endregion

        #region Private data.
        PresentationStorage storagePresenter;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected new void Subscribe(ILogicPlayerProfile playerProfile)
        {
            base.Subscribe(playerProfile);

            storagePresenter = new PresentationStorage(logic.storage, this, ref storageOverheadIcon);
            logic.storage.presenter = storagePresenter;

            gui.hud.OnScoresClick = ShowLeaderboards;
        }

        [BindBridgeUnsubscribe]
        protected new void Unsubscribe(ILogicPlayerProfile playerProfile)
        {
            base.Unsubscribe(playerProfile);
        }
        #endregion

        #region Bridge events.
        [BindBridgeEvent("onAddExpEvent")]
        void OnAddExp(int valueDelta, bool useFx)
        {
            if (valueDelta != 0)
            {
                LeanTween.value
                    ( gameObject
                    , delegate(float v)
                    {
                        gui.hud.profileExpTxt = logic.expLevels.GetExpRatioStr((int)v, logic.expLevels.GetLevelByTable((int)v));

                        float tRatio = logic.expLevels.GetExpRatio(v, logic.expLevels.GetLevelByTable((int)v));
                        gui.hud.profileExpProgress = tRatio;
                    }
                    , (float)logic.experience - valueDelta
                    , (float)(logic.experience), 1.0f);

                if (useFx)
                {
                    gui.fx.ShowHeader(UiFxSprite.Exps, valueDelta);
                    gui.fx.AddFx(UiFxSprite.Exps, valueDelta);
                }
            }
        }

        [BindBridgeEvent("onAddCoinsEvent")]
        void OnAddCoinsEvent(int valueDelta, bool useFx)
        {
            if (valueDelta != 0)
            {
                if (useFx)
                {
                    gui.fx.ShowHeader(UiFxSprite.Coins, valueDelta);
                    gui.fx.AddFx(UiFxSprite.Coins, valueDelta);
                }
            }
        }

        [BindBridgeEvent("onAddFitBucksEvent")]
        void OnAddFitBucksEvent(int valueDelta, bool useFx)
        {
            if (valueDelta != 0)
            {
                if (useFx)
                {
                    gui.fx.ShowHeader(UiFxSprite.Bucks, valueDelta);
                    gui.fx.AddFx(UiFxSprite.Bucks, valueDelta);
                }
            }
        }

        [BindBridgeEvent("onLevelUpEvent")]
        void OnLevelUpEvent()
        {
            OnLevelUp();
        }

        [BindBridgeEvent("onPostLoadEvent")]
        void OnPostLoadEvent()
        {
            gui.hud.UpdateHUD();
        }

        [BindBridgeEvent("onPlayerProfileUpdatedEvent")]
        void OnPlayerProfileUpdatedEvent()
        {
            gui.shopUltimate.coins = logic.coins;
            gui.shopUltimate.bucks = logic.fitBucks;
            gui.hud.coins = logic.coins;
            gui.hud.fitBucks = logic.fitBucks;
            gui.hud.profileLevel = logic.level.ToString();
            gui.hud.profileExpTxt = logic.profileExpTxt;
            gui.hud.profileExpProgress = logic.profileExpProgress;

#if DEBUG
            gui.dbgView.profileLvl.text = "Profile Lvl = " + logic.level.ToString();
            gui.dbgView.fatigue.text = "Fatigue = " + logic.fatigue.ratio.ToString("N");
#endif
        }

        [BindBridgeEvent("onNotEnoughMoneyEvent")]
        void OnNotEnoughMoneyEvent(Data.Cost price, bool spendBucksIfNecessary)
        {
            gui.NeedMoneyMsg(price, spendBucksIfNecessary);
        }
        #endregion

        #region Unity API.
        protected new void Start()
        {
            base.Start();

            gui.hud.OnAddCoins = delegate{ OnAddCoins(); Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "HUD"); };
            gui.hud.OnAddFitBucks = delegate{ OnAddFitBucks(); Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "HUD"); };
            gui.shopUltimate.OnAddCoins = delegate{ OnAddCoins(); Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "Shop"); };
            gui.shopUltimate.OnAddFitBucks = delegate{ OnAddFitBucks(); Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "Shop"); };

#if DEBUG
            gui.dbgView.btnUnlocks.onClick.AddListener(OnLevelUp);
#endif

            gui.windows.OnWindowCloseEvent += OnWindowClose;

            gui.rateUs.btnRateUs.onClick.AddListener(OnRateUsClick);

            storagePresenter.Start();

            gui.hud.enablerFriends.CanEnabled_Callback = () => { return logic.friendsAvailable; };
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            if (gui != null && gui.windows != null)
                gui.windows.OnWindowCloseEvent -= OnWindowClose;

            storagePresenter.Destroy();
        }
        #endregion

        #region IPlayerProfileInteractionProvider interface.
        public override void WarnStorageIsFull(bool showWarningFirst)
        {
            //gui.confirmWnd.OnCustom("Storage Full", "The Storage is full! Get more space by upgrading your storage.", "UPGRADE", true);
            if (showWarningFirst)
            {
                gui.confirmWnd.OnStorageNotEnough();
                gui.confirmWnd.OnConfirm = ShowUpgradeStorageWindow;
            }
            else
            {
                ShowUpgradeStorageWindow();
            }
        }
        #endregion

        #region Private functions.
        void ShowUpgradeStorageWindow ()
        {
            if (!storage.canUpgrade)
            {
                Debug.LogError("PresentationStorage : upgrade not available");
                return;
            }

            var view = PresentationBuyHelper.BuyWithResources(gui, this, storage.capacityUpgradeCost, storage.BuyNextUpgrade);
            view.header = Loc.Get("storageExpandHeader");

            var desciption = view.IsolateHeader("expandStorage");

            desciption.SetText("from", storage.capacity.ToString());
            desciption.SetText("to", storage.capacityNext.ToString());
            desciption.SetText("diff", "+" + (storage.capacityNext - storage.capacity).ToString());
            desciption.SetText("description", Loc.Get("storageExpandIncreaseCap"));

            view.actionButtonPrefixText = Loc.Get("idUpgrade");

            Camera.main.GetComponent<View.CameraHelpers.CameraController>().AutoMoveTo(-3.6f, 0.2f);
        }

        private void OnLevelUp()
        {
            gui.levelUp.level = logic.level;
            //gui.levelUp.rewardCoins = logic.levelUpReward.value;
            gui.levelUp.Show();

            var newEquips = GetUnlockedEquipment();
            var newPaints = GetUnlockedWallFloor();

            // TOTAL ITEMS COUNT
            var itemsCount = newEquips.Count +
                             newPaints.Count +
                             NotifyAboutCapacity() +
                             NotificationAboutExpand() +
                             logic.levelUpReward.resources.Count +
                             1; // for coins

            gui.levelUp.UpdateItemsCount(itemsCount);

            var offsetIdx = 0;

            offsetIdx += PushCoins(offsetIdx, logic.levelUpReward.value);
            offsetIdx += PushEquipsToUnlocksView(offsetIdx, newEquips);
            offsetIdx += PushWallFloorToUnlocksView(newPaints, offsetIdx);
            offsetIdx += PushCapacityUnlock(offsetIdx);
            offsetIdx += PushExpandUnlock(offsetIdx);
            offsetIdx += PushResources(offsetIdx);
        }

        private void OnAddFitBucks()
        {
            gui.hud.btnBack.onClick.Invoke();

            gui.shopUltimate.Show();
            gui.shopUltimate.ShowBankBucks();
            //gui.shop.Show();
            //gui.shop.SetCategory(Data.ShopItemCategory.BANK, 5);
        }

        private void OnAddCoins()
        {
            gui.hud.btnBack.onClick.Invoke();

            gui.shopUltimate.Show();
            gui.shopUltimate.ShowBankCoins();
            //gui.shop.Show();
            //gui.shop.SetCategory(Data.ShopItemCategory.BANK);
        }

        private int NotifyAboutCapacity()
        {
            if (logic.level <= 3)
                return 1;
            else
                return 0;
        }

        private int NotificationAboutExpand()
        {
            // TODO: DIRTY HACK!
            return Data.DataTables.instance.expand.Any (x => (x.Key.roomName == "Club" && x.Value.unlockLevel == logic.level)) ? 1 : 0;

/*
            
            ColumnToArrayOfInt("LvL unlock");

            foreach (var lvl in levels)
            {
                if (lvl == logic.level)
                    return 1;
            }

            return 0;
*/
        }

        private List<EstateType> GetUnlockedEquipment()
        {
            var estateTypes = Data.DataTables.instance.estateTypes;
            var newEquips = new List<EstateType>();

            foreach (var equip in estateTypes)
            {
                if (equip.Value.levelRequirement == logic.level)
                    newEquips.Add(equip.Value);
            }

            return newEquips;
        }

        private List<Data.WallFloorShopItem> GetUnlockedWallFloor()
        {
            var wallFloorShopItems = Data.DataTables.instance.wallFloorShopItems;
            var newPaints = new List<Data.WallFloorShopItem>();
            foreach (var paint in wallFloorShopItems)
            {
                if (paint.Value.price.level == logic.level)
                    newPaints.Add(paint.Value);
            }

            return newPaints;
        }

		private int PushCoins(int offsetIdx, int count)
		{
			if (count > 0)
            {
                var viewItem = gui.levelUp.items[offsetIdx];
                viewItem.coins = count;
            }

            return count > 0 ? 1 : 0;
        }

		private int PushEquipsToUnlocksView(int offsetIdx, List<EstateType> list)
		{
			for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                var viewItem = gui.levelUp.items[i + offsetIdx];
                DataRow row = null;

                if (item.itemType == EstateType.Subtype.Training)
                    row = DataTables.instance.equipmentShop.Where(x => x.id == item.id).FirstOrDefault();
                else if (item.itemType == EstateType.Subtype.Decor)
                    row = DataTables.instance.decorShop.Where(x => x.id == item.id).FirstOrDefault();

                if (row != null)
                    viewItem.prefabName = row.GetString("mesh_prefab");
            }

            return list.Count;
        }

        private int PushWallFloorToUnlocksView(List<Data.WallFloorShopItem> list, int offsetIdx)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                var viewItem = gui.levelUp.items[i + offsetIdx];

                viewItem.wallFloorIdx = item.id;
            }

            return list.Count;
        }

        private int PushCapacityUnlock(int offsetIdx)
        {
            if (NotifyAboutCapacity() > 0)
            {
                var viewItem = gui.levelUp.items[offsetIdx];
                viewItem.capacityOn = true;
            }

            return NotifyAboutCapacity();
        }

        private int PushExpandUnlock(int offsetIdx)
        {
            if (NotificationAboutExpand() > 0)
            {
                var viewItem = gui.levelUp.items[offsetIdx];
                viewItem.expandOn = true;

                return 1;
            }

            return 0;
        }

        private int PushResources(int offsetIdx)
        {
            var resources = logic.levelUpReward.resources;
            if (resources.Count > 0)
            {
                int i = 0;
                foreach (var res in resources)
                {
                    var viewItem = gui.levelUp.items[offsetIdx + i];
                    viewItem.SetResource(Data.Cost.GetSprite(res.Key),1);
                    i++;
                }
            }

            return logic.levelUpReward.resources.Count;
        }

        private void OnWindowClose(WindowTypes type)
        {
            if (type == WindowTypes.LevelUp)
            {
                ShowRateUs();
            }
        }

        void ShowRateUs()
        {
            var bal = DataTables.instance.balanceData;

            if (playerProfileInteractionProvider.level >= bal.Constants.showRateUsLevel 
                && !logic.IsAnyTutorial()
                && !logic.alreadyRateUs)  //!PlayerPrefs.HasKey("AlreadyRate"))
            {
                gui.rateUs.Show();
            }
        }

        void ShowLeaderboards()
        {
            #if UNITY_IOS
            var leaderBordID = "com.tatemgames.dreamgym2.SportPoints_1";
            #elif UNITY_ANDROID
            var leaderBordID = "CgkIgbCzsoAJEAIQMw";
            #else
            var leaderBordID = "bla-bla-bla";
            #endif

            var socialManager = PersistentCore.instance.socialManager;

            foreach (var network in socialManager.GetAvailableNetworks(NetworkFeatures.SupportsScores, true))
            {
                network.ShowLeaderboardWindow(leaderBordID);
            }
        }

        void OnRateUsClick()
        {
#if UNITY_IOS
            Application.OpenURL("itms-apps://itunes.apple.com/app/id1143516053");
#elif UNITY_ANDROID
    #if UNITY_5_6_OR_NEWER
            Application.OpenURL(string.Format("http://play.google.com/store/apps/details?id={0}", WWW.EscapeURL(Application.identifier)));
    #else
            Application.OpenURL(string.Format("http://play.google.com/store/apps/details?id={0}", WWW.EscapeURL(Application.bundleIdentifier)));
    #endif
#endif
            gui.windows.ShowDefault();

            logic.alreadyRateUs = true;  //PlayerPrefs.SetInt("AlreadyRate", 1);
        }
        #endregion
    }
}