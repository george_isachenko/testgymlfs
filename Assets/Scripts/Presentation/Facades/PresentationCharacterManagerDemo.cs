﻿using UnityEngine;

namespace Presentation.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Presentation/Facades/Presentation Character Manager Demo")]
    public class PresentationCharacterManagerDemo : PresentationCharacterManagerBase
    {
        #region Inspector fields.
        #endregion

        #region Private data.
        #endregion

        #region Bridge interface.
        #endregion

        #region ICharacterInteractionProvider properties.
        #endregion

        #region Unity API.
        #endregion

        #region IPresentationCharacterManager interface.
        #endregion

        #region ICharacterActorSharedDataProvider interface.
        #endregion

        #region ICharacterInteractionProvider interface.
        #endregion

        #region Bridge event handlers.
        #endregion

        #region Private functions.
        #endregion

        #region Private static functions.
        #endregion
    }
}