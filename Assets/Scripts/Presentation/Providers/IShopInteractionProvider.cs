﻿using System;
using Data;

namespace Presentation.Providers
{
    public delegate bool RewardedVideoAvaialbleEvent ();

    public interface IShopInteractionProvider
    {
        bool IsRewardedVideoAvailable();
        bool IsRewardedVideoAvailableForTV();
        bool ShowRewardedAd(Action<bool, Cost> callback);
        void ConfirmAdReward(Cost reward);
    }
}