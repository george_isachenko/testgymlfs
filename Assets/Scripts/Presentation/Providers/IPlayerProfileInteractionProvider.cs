﻿using Core;
using Core.Timer;
using Data;
using Logic.PlayerProfile;
using Core.Analytics;

namespace Presentation.Providers
{
    public delegate void LevelUpEvent();

    public interface IPlayerProfileInteractionProvider
    {
        #region Events.
        event LevelUpEvent  onLevelUp;
        #endregion

        #region Properties.
        int                 coins               { get; }
        int                 fitBucks            { get; }
        int                 level               { get; }
        Cost                levelUpReward       { get; }
        int                 experience          { get; }
        TimerFloat          fatigue             { get; }
        LogicStorage        storage             { get; }
        #endregion

        #region Functions.
        bool                SpendMoney          (Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false);
        bool                CanSpend            (Cost cost, bool forceFitBucks = false);
        Cost                GetShortageCost     (Cost cost);
        void                WarnStorageIsFull   (bool showWarningFirst);
        #endregion
    }
}