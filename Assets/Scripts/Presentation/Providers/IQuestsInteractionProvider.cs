﻿using Logic.Quests;
using Logic.Quests.Visitor;
using Logic.Quests.Juicer;

namespace Presentation.Providers
{
    public interface IQuestsInteractionProvider
    {
        VisitorQuestLogic GetVisitorQuest(int charactedId);
        JuiceQuestLogic GetJuiceQuest(int charactedId);

        bool isTutorialRunning
        {
            get;
        }
    }
}