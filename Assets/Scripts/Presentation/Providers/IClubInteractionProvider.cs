﻿using System.Collections.Generic;
using Data.Room;
using View.Rooms;
using System;

namespace Presentation.Providers
{
    public delegate void RoomResizedEvent(string roomName, RoomCoordinates newSize);
    public delegate void RoomDeactivatedEvent(string roomName); 
    public delegate void RoomActivatedEvent(string roomName); 

    public interface IClubInteractionProvider
    {
        event FloorPointerClickEvent onFloorPointerClick;
        event FloorPointerDownEvent onFloorPointerDown;
        event FloorPointerUpEvent onFloorPointerUp;

        event RoomResizedEvent onRoomResizedEvent;
        event RoomDeactivatedEvent onRoomDeactivatedEvent;
        event RoomActivatedEvent onRoomActivatedEvent;

        IDictionary<string, IRoomView> rooms
        {
            get;
        }

        IRoomView activeRoom
        {
            get;
        }

        string activeRoomName
        {
            get;
        }

        bool isInEditMode
        {
            get;
        }

        IRoomView GetRoom(string roomName);
        IRoomView GetRoomOfType(RoomType roomType);

        bool TransitionToRoomRequest(string roomName);
    }
}
