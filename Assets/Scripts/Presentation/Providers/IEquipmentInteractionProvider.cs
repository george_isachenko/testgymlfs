﻿using Data.Estate;
using View.Actors;
using Data.Estate.Info;

namespace Presentation.Providers
{
    public delegate void OnEquipmentSelected (int id, IEquipmentInfo equipmentInfo);
    public delegate void OnEquipmentDeselected (int id);
    public delegate void OnEquipmentDestroyed(int id);
    public delegate void OnEquipmentStateChanged(int id, EquipmentState oldState, EquipmentState newState);

    public interface IEquipmentInteractionProvider
    {
        event OnEquipmentSelected onEquipmentSelected;
        event OnEquipmentDeselected onEquipmentDeselected;
        event OnEquipmentDestroyed onEquipmentDestroyed;
        event OnEquipmentStateChanged onEquipmentStateChanged;

        int selectedEquipmentId { get; }

        IEquipmentActor GetEquipmentActor (int equipmentId);
    
        void SelectEquipmentRequest(int equipmentId, bool allowDeselectionIfSelected = false);
        void DeselectEquipmentRequest();
        void DeselectEquipmentIfSelectedRequest(int equipmentId);
    }
}