﻿using Data;
using Data.Person;
using Data.Person.Info;
using Logic.Persons;
using System;
using View.Actors;

namespace Presentation.Providers
{
    public delegate void OnCharacterSelected (IPersonInfo personInfo, bool focus);
    public delegate void OnCharacterDeselected (IPersonInfo personInfo);
    public delegate void OnCharacterCreated (IPersonInfo personInfo);
    public delegate void OnCharacterRemoved (int id);
    public delegate void OnStartExercise (IPersonInfo personInfo, int equipmentId, int exerciseAnimation, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed);
    public delegate void OnSwitchExercise (IPersonInfo personInfo, int exerciseAnimation, float speed);
    public delegate void OnEndExercise (IPersonInfo personInfo, int equipmentId, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags);

    public interface ICharacterInteractionProvider
    {
        event OnCharacterSelected onCharacterSelected;
        event OnCharacterDeselected onCharacterDeselected;
        event OnCharacterCreated onCharacterCreated;
        event OnCharacterRemoved onCharacterRemoved;
        event OnStartExercise onStartExercise;
        event OnSwitchExercise onSwitchExercise;
        event OnEndExercise onEndExercise;

        int selectedCharacterId { get; }

        ICharacterActor GetCharacterActor(int id);
        bool SelectCharacter(int id);
        void DeselectCharacter(int id);
        void KickVisitor(int id);
        void ClaimQuest(int id);
        void UpdateOverheadUI(int id);

        CharacterPortrait GetPortrait(int id);
        CharacterPortrait GetPortrait(PersonAppearance appearance, string typeName);
    }
}
