﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BridgeBinder;
using Presentation.Base;
using View.UI;
using UnityEngine.Assertions;
using Logic.Persons;
using Logic;
using View.CameraHelpers;
using Logic.Trainers.Facade;
using Data.Person.Info;
using System.Linq;
using View.UI.Base;
using Data;
using View.Actors;
using View.Actors.Helpers;
using Data.Person;

namespace Presentation.Facades
{
    [BindBridgeInterface(typeof(LogicTrainers))]
    public class PresentationTrainers : BasePresentationFacade<LogicTrainers>
    {
        TrainersView    view;
        TrainerInfoView viewTrainerInfo;

        CharacterPortrait[] portraits;

        [BindBridgeSubscribe]
        private void Subscribe(LogicTrainers logic)
        {
            this.logic = logic;   
        }

        void Awake()
        {
            gui.hud.OnShowTrainers = gui.trainers.Show;
            gui.hud.enablerTrainers.CanEnabled_Callback = () => { return logic.canUseMyFeature.canUse;};

            viewTrainerInfo = gui.trainerInfo;

            view = gui.trainers;
            view.OnUpdateContentOnShow_Callback = UpdateTrainersView;
            viewTrainerInfo.ShowTrainer = UpdateTrainerInfoView;
            viewTrainerInfo.OnHide_Callback = () => {UpdateTrainerPanelInfo(null);};

            gui.windows.OnWindowOpenedEvent += OnWindowsOpening;
            gui.windows.OnWindowCloseEvent += OnWindowClose;

            Logic.Persons.Events.CharacterLevelUpEvent.Event += CharacterLevelUpEvent;
        }

        void OnDestroy()
        {
            Logic.Persons.Events.SetSelectionEvent.Event -= OnSetSelectionEvent;
            Logic.Persons.Events.CharacterLevelUpEvent.Event -= CharacterLevelUpEvent;
            gui.windows.OnWindowOpenedEvent -= OnWindowsOpening;
            gui.windows.OnWindowCloseEvent -= OnWindowClose;
        }

        void Start()
        {
            gui.hq.onStaffClick = view.Show;
            gui.hud.personPanel.visitor.OnAssignTrainer = () => 
            { 
                logic.RememberSelectedVisitor();
                view.Show(); 
            };
            gui.hud.personPanel.trainer.OnOpenTrainersView = view.Show;

            var maxCount = logic.maxTrainersCount;

            portraits = new CharacterPortrait[maxCount];

            for(int i = 0;  i < maxCount; i++)
            {
                var appearance = logic.GetTrainerAppearance(i);
                portraits[i] = characterInteractionProvider.GetPortrait(appearance, typeof(IPersonTrainerInfo).Name);
                portraits[i].Hide();
            }

            Logic.Persons.Events.SetSelectionEvent.Event += OnSetSelectionEvent;
        }

        public void UpdateTrainersView()
        {
            var maxCount = logic.maxTrainersCount;
            var items = logic.trainers.ToArray();
            view.list.UpdateItemsCount(maxCount);
            var viewItems = view.list.items;

            for(int i = 0;  i < maxCount; i++)
            {
                var trainerViewItem = viewItems[i];

                trainerViewItem.portrait = portraits[i];

                if ( i < items.Length)
                {
                    var trainer = items[i] as PersonTrainer;

                    trainerViewItem.idx            = i;
                    trainerViewItem.mode           = TrainersViewItem.ViewMode.Info;
                    trainerViewItem.level          = Loc.Get("idLevel") + " " + trainer.level.ToString();
                    trainerViewItem.nameOfPerson   = trainer.name;
                    trainerViewItem.expProgress    = trainer.expProgress;
                    trainerViewItem.expTxt         = trainer.expTxt;
                    trainerViewItem.levelUpOn      = trainer.canLevelUp;
                    trainerViewItem.actionLevelUp  = trainer.LevelUp;
                    trainerViewItem.isFull         = !trainer.canAssign && !trainer.canLevelUp;
                    trainerViewItem.actionShowInfo = UpdateTrainerInfoView;

                    if (logic.goingToAssign && !trainer.canLevelUp)
                    {
                        trainerViewItem.canAssign = trainer.canAssign;
                        trainerViewItem.actionAssign = () => 
                        { 
                            var trainerClient = logic.AssignVisitorToTrainer(trainerViewItem.idx); 
                            Logic.Persons.Events.SetSelectionEvent.Send(trainerClient, true, false);
                            logic.DelayedInvoke(UpdateTrainersView);
                            view.Show(false);
                        };
                    }
                    else
                        trainerViewItem.canAssign = false;

                    Assert.IsTrue(trainer.clients.Length == trainerViewItem.clients.Length);

                    for(int j = 0; j < trainer.clients.Length; j++)
                    {
                        var clientView = trainerViewItem.clients[j];
                        var client = trainer.clients[j];

                        clientView.state = client.state;
                        clientView.lvlUnlock = client.lvlUnlock;
                        if (client.state == Logic.Trainers.TrainerClientType.InProgress)
                        {
                            clientView.iterationsDone = client.iterationsDone;
                            clientView.iterationsGoal = client.iterationsGoal;
                        }
                    }
                }
                else
                {
                    var isNext = (i == items.Length);
                    trainerViewItem.hireBtnOn = isNext;
                    trainerViewItem.mode = TrainersViewItem.ViewMode.Hire;
                    if (isNext)
                    {
                        trainerViewItem.hirePrice = logic.hireCost;
                        trainerViewItem.actionHire = () =>
                        {
                            if (logic.HireTrainer())
                            {
                                view.Hide();
                            }
                        };
                    }
                }
            }
        }

        void UpdateTrainerInfoView(IPersonTrainerInfo trainer)
        {
            if (trainer == null)
            {
                Debug.LogError("UpdateTrainerInfoView trainer is NULL");
                return;
            }

            var unlock = trainer.nextUnlock;

            viewTrainerInfo.Show();
            viewTrainerInfo.nameOfPerson    = trainer.name;
            viewTrainerInfo.level           = trainer.level;
            viewTrainerInfo.expProgress     = trainer.expProgress;
            viewTrainerInfo.expTxt          = trainer.expTxt;
            viewTrainerInfo.maxClients      = trainer.clientsMax;
            viewTrainerInfo.portrait        = characterInteractionProvider.GetPortrait(trainer.id);
            viewTrainerInfo.bestWorkstation = logic.BestWorkstationForTrainer(trainer);
            viewTrainerInfo.coinsBonus      = trainer.coinsBonus;
            viewTrainerInfo.nextUnlockSprite= unlock.sprite;
            viewTrainerInfo.nextUnlockText  = unlock.text;
        }

        void UpdateTrainerInfoView(int idx)
        {
            var items = logic.trainers.ToArray();

            if (idx >= 0 && idx < items.Length)
            {
                UpdateTrainerInfoView(items[idx]);
            }
        }

        void OnSetSelectionEvent(Person person, bool selected, bool focus)
        {
            if (selected)
                UpdateTrainerPanelInfo(person);
        }


        static PersonTrainer _lastTrainer;
        void UpdateTrainerPanelInfo(Person person)
        {
            var trainer = person as PersonTrainer;
            var view = gui.hud.personPanel.trainer;

            if (trainer == null)
            {
                if (_lastTrainer != null & view.isActiveAndEnabled)
                    view.portrait = characterInteractionProvider.GetPortrait(_lastTrainer.id);
                
                return;
            }

            _lastTrainer = trainer;

            gui.hud.personPanel.ShowTrainer();

            view.personName   = person.name;
            gui.trainerInfo.OnNameEditFinishCallback = () => { OnNameChanged(person); };

            view.portrait = characterInteractionProvider.GetPortrait(person.id);

            view.level          = Loc.Get("idLevel") + " " + trainer.level.ToString();
            view.expProgress    = trainer.expProgress;
            view.expTxt         = trainer.expTxt;
            view.levelUpOn      = trainer.canLevelUp;
            view.OnLevelUpClick = trainer.LevelUp;
            view.OnInfoClick    = () => { UpdateTrainerInfoView(trainer); };

            Assert.IsTrue(trainer.clients.Length == view.clients.Length);
            for(int j = 0; j < trainer.clients.Length; j++)
            {
                var clientView = view.clients[j];
                var client = trainer.clients[j];

                clientView.state = client.state;
                clientView.lvlUnlock = client.lvlUnlock;
                if (client.state == Logic.Trainers.TrainerClientType.InProgress)
                {
                    clientView.iterationsDone = client.iterationsDone;
                    clientView.iterationsGoal = client.iterationsGoal;
                }
            }
        }

        void OnNameChanged(Person person)
        {
            var view = gui.trainerInfo;
            person.name = view.trainerName.text;
        }

        void CharacterLevelUpEvent(Person person, int newLevel)
        {
            var trainer = person as PersonTrainer;

            if (trainer != null)
            {
                gui.windows.ShowDefault();
                characterInteractionProvider.SelectCharacter(person.id);
                var actor = characterInteractionProvider.GetCharacterActor(person.id);

                if (actor != null)
                {
                    CameraController.instance.AutoMoveTo(actor.gameObject);
                }

                UpdateTrainerPanelInfo(trainer);
            }
        }

        void OnWindowsOpening(WindowTypes type, UIBaseView wnd)
        {
            if (type == WindowTypes.HQ)
            {
                var data = DataTables.instance.trainerUnlocks;
                bool isLock = logic.playerProfile.level < data[0].level;
                gui.hq.trainersLock = isLock;

                if(isLock)
                    gui.hq.trainersLockCaption = Loc.Get("shopUnlockAt") + " " + data[0].level.ToString();
            }

        }

        void OnWindowClose(WindowTypes type)
        {
            if (type == WindowTypes.TrainersInfoView)
            {
                var person = logic.SelectedTrainer();
                if (person != null)
                    UpdateTrainerPanelInfo(person);

                foreach (var portrait in portraits)
                {
                    portrait.Hide();
                }
            }
        }
    }
}