﻿using Data;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Linq;

namespace DataProcessing.CSV
{
    public class ParseException : Exception
    {
        public ParseException(string message)
            : base (message)
        {

        }
    }

    public static class Parser
    {
        #region Public static API.
        public static IEnumerable<DataRow> ParseString(string text, bool useNull = false)
        {
            if (text == null)
                throw new ArgumentNullException("text");

            using (var reader = new StringReader(text))
            {
                using (var csv = new CsvReader
                    ( reader
                    , true
                    , CsvReader.DefaultDelimiter
                    , CsvReader.DefaultQuote
                    , CsvReader.DefaultEscape
                    , CsvReader.DefaultComment
                    , ValueTrimmingOptions.All
                    , useNull ? null : string.Empty))
                {
                    int fieldCount = csv.FieldCount;
                    var keys = csv.GetFieldHeaders();

                    if (keys == null)
                        throw new ParseException("Not enough lines in file, missing headers line");

                    int idx = 0;
                    while (csv.ReadNextRecord())
                    {
                        var entries = new string[fieldCount];
                        
                        for (int i = 0; i < fieldCount; i++)
                        {
                            entries[i] = Unescape(csv[i]);
                        }

                        yield return new DataRow(idx, keys, entries);
                        idx++;
                    }
                }
            }
        }

        public static Dictionary<KeyT, T> ParseStringToDictionary<KeyT, T> (string text, string indexKeyName = "id")
            where T : new()
        {
            if (indexKeyName == null)
                throw new ArgumentNullException("indexKeyName");

            if (text == null)
                throw new ArgumentNullException("text");

            var type = typeof(T);

            var fieldsInfo = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            var methodsInfo = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if ((fieldsInfo == null || fieldsInfo.Length <= 0) && (methodsInfo == null || methodsInfo.Length <= 0))
                throw new ParseException(string.Format("No fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

            int expectedLineCount = text.Count(x => x == '\n'); // Quick'n'Dirty method to count number of lines. Can be not correct, used to reserve capacity only!

            Dictionary<KeyT, T> result = null;

            using (var reader = new StringReader(text))
            {
                using (var csv = new CsvReader
                    ( reader
                    , true
                    , CsvReader.DefaultDelimiter
                    , CsvReader.DefaultQuote
                    , CsvReader.DefaultEscape
                    , CsvReader.DefaultComment
                    , ValueTrimmingOptions.All
                    , null))
                {
                    int fieldCount = csv.FieldCount;
                    var keys = csv.GetFieldHeaders();
                    if (keys == null)
                        throw new ParseException("Not enough lines in a file, missing headers line");

                    var keyIdx = indexKeyName != string.Empty ? Array.IndexOf(keys, indexKeyName) : 0; // If no 'indexKeyName' is set - use first field as a key.
                    if (keyIdx < 0)
                        throw new ParseException(string.Format("No key fields with a name '{0}' in a file", indexKeyName));

                    result = new Dictionary<KeyT, T>(expectedLineCount); // -1 first 'headers' line but account that last line can be without new-line at the end.

                    int[] fieldsMapping = (fieldsInfo == null) ? null : PrepareFieldsMapping(keys, fieldsInfo);
                    int[] methodsMapping = (methodsInfo == null) ? null : PrepareMethodsMapping(keys, methodsInfo);

                    if (!(fieldsMapping != null && fieldsMapping.Any(x => x >= 0) || methodsMapping != null && methodsMapping.Any(x => x >= 0)))
                        throw new ParseException(string.Format("No mapped fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

                    var entries = new string[fieldCount];

                    int lineNumber = 1;
                    while (csv.ReadNextRecord())
                    {
                        for (int i = 0; i < fieldCount; i++)
                        {
                            entries[i] = Unescape(csv[i]);
                        }

                        KeyT keyValue = (KeyT)ParseValue(lineNumber, 0, entries[keyIdx], typeof(KeyT));
                        result[keyValue] = ParseLineToObject<T>(lineNumber, entries, fieldsInfo, fieldsMapping, methodsInfo, methodsMapping) ; // Can overwrite already existing key.

                        lineNumber++;
                    }
                }
            }

            return result;
        }

        public static Dictionary<KeyT, T> ParseStringToDictionaryWithCompositeKey<KeyT, T> (string text)
            where T : new()
            where KeyT : new()
        {
            if (text == null)
                throw new ArgumentNullException("text");

            var type = typeof(T);
            var keyType = typeof(KeyT);

            if (!keyType.IsClass && !(keyType.IsValueType && !keyType.IsPrimitive && !keyType.IsEnum))
            {
                throw new ParseException(string.Format("Key type {0}.{1} must be either class type, or struct. For primitive or enum key types use ParseStringToDictionary().",
                    keyType.Namespace, keyType.Name));
            }

            var fieldsInfo = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            var methodsInfo = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if ((fieldsInfo == null || fieldsInfo.Length <= 0) && (methodsInfo == null || methodsInfo.Length <= 0))
                throw new ParseException(string.Format("No fields or methods in a value type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

            var keyFieldsInfo = keyType.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            var keyMethodsInfo = keyType.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if ((keyFieldsInfo == null || keyFieldsInfo.Length <= 0) && (keyMethodsInfo == null || keyMethodsInfo.Length <= 0))
                throw new ParseException(string.Format("No fields or methods in a key type '{0}.{1}', type cannot be parsed", keyType.Namespace, keyType.Name));

            int expectedLineCount = text.Count(x => x == '\n'); // Quick'n'Dirty method to count number of lines. Can be not correct, used to reserve capacity only!

            Dictionary<KeyT, T> result = null;

            using (var reader = new StringReader(text))
            {
                using (var csv = new CsvReader
                    ( reader
                    , true
                    , CsvReader.DefaultDelimiter
                    , CsvReader.DefaultQuote
                    , CsvReader.DefaultEscape
                    , CsvReader.DefaultComment
                    , ValueTrimmingOptions.All
                    , null))
                {
                    int fieldCount = csv.FieldCount;
                    var keys = csv.GetFieldHeaders();

                    if (keys == null)
                        throw new ParseException("Not enough lines in a file, missing headers line");

                    result = new Dictionary<KeyT, T>(expectedLineCount); // -1 first 'headers' line but account that last line can be without new-line at the end.

                    int[] fieldsMapping = (fieldsInfo == null) ? null : PrepareFieldsMapping(keys, fieldsInfo);
                    int[] methodsMapping = (methodsInfo == null) ? null : PrepareMethodsMapping(keys, methodsInfo);

                    if (!(fieldsMapping != null && fieldsMapping.Any(x => x >= 0) || methodsMapping != null && methodsMapping.Any(x => x >= 0)))
                        throw new ParseException(string.Format("No mapped fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

                    int[] keyFieldsMapping = (keyFieldsInfo == null) ? null : PrepareFieldsMapping(keys, keyFieldsInfo);
                    int[] keyMethodsMapping = (keyMethodsInfo == null) ? null : PrepareMethodsMapping(keys, keyMethodsInfo);

                    if (!(keyFieldsMapping != null && keyFieldsMapping.Any(x => x >= 0) || keyMethodsMapping != null && keyMethodsMapping.Any(x => x >= 0)))
                        throw new ParseException(string.Format("No mapped fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

                    var entries = new string[fieldCount];

                    int lineNumber = 1;
                    while (csv.ReadNextRecord())
                    {
                        for (int i = 0; i < fieldCount; i++)
                        {
                            entries[i] = Unescape(csv[i]);
                        }

                        KeyT keyValue = ParseLineToObject<KeyT>(lineNumber, entries, keyFieldsInfo, keyFieldsMapping, keyMethodsInfo, keyMethodsMapping);
                        result[keyValue] = ParseLineToObject<T>(lineNumber, entries, fieldsInfo, fieldsMapping, methodsInfo, methodsMapping); // Can overwrite already existing key.

                        lineNumber++;
                    }
                }
            }

            return result;
        }

        public static IEnumerable<T> ParseStringToObjects<T> (string text)
            where T : new()
        {
            if (text == null)
                throw new ArgumentNullException("text");

            var type = typeof(T);

            var fieldsInfo = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            var methodsInfo = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if ((fieldsInfo == null || fieldsInfo.Length <= 0) && (methodsInfo == null || methodsInfo.Length <= 0))
                throw new ParseException(string.Format("No fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

            using (var reader = new StringReader(text))
            {
                using (var csv = new CsvReader
                    ( reader
                    , true
                    , CsvReader.DefaultDelimiter
                    , CsvReader.DefaultQuote
                    , CsvReader.DefaultEscape
                    , CsvReader.DefaultComment
                    , ValueTrimmingOptions.All
                    , null))
                {
                    int fieldCount = csv.FieldCount;
                    var keys = csv.GetFieldHeaders();
                    if (keys == null)
                        throw new ParseException("Not enough lines in a file, missing headers line");

                    int[] fieldsMapping = (fieldsInfo == null) ? null : PrepareFieldsMapping(keys, fieldsInfo);
                    int[] methodsMapping = (methodsInfo == null) ? null : PrepareMethodsMapping(keys, methodsInfo);

                    if (!(fieldsMapping != null && fieldsMapping.Any(x => x >= 0) || methodsMapping != null && methodsMapping.Any(x => x >= 0)))
                        throw new ParseException(string.Format("No mapped fields or methods in a type '{0}.{1}', type cannot be parsed", type.Namespace, type.Name));

                    var entries = new string[fieldCount];

                    int lineNumber = 1;
                    while (csv.ReadNextRecord())
                    {
                        for (int i = 0; i < fieldCount; i++)
                        {
                            entries[i] = Unescape(csv[i]);
                        }

                        yield return ParseLineToObject<T>(lineNumber, entries, fieldsInfo, fieldsMapping, methodsInfo, methodsMapping);

                        lineNumber++;
                    }
                }
            }
        }

        public static IEnumerable<DataRow> ParseResource(string resourceName)
        {
            // NOTE: All this hackery allows us to catch exceptions both on parse initialization and iteration
            // and work around issue that yield is not allowed inside try-catch blocks.

            IEnumerator<DataRow> parseResult = null;

            try
            {
                var res = Resources.Load<TextAsset>(resourceName);
                if (res != null && res.text != null && res.text != string.Empty)
                {
                    parseResult = ParseString(res.text).GetEnumerator();
                }
                else
                {
                    throw new ParseException("Resource file not exists, cannot be loaded or empty.");
                }
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                throw;
            }

            while (true)
            {
                DataRow ret = default(DataRow);

                try
                {
                    if (!parseResult.MoveNext())
                    {
                        break;
                    }
                    ret = parseResult.Current;
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                    throw;
                }

                yield return ret;
            }
        }

        public static Dictionary<KeyT, T> ParseResourceToDictionary<KeyT, T> (string resourceName, string indexKeyName = "id")
            where T : new()
        {
            try
            {
                var res = Resources.Load<TextAsset>(resourceName);
                if (res != null && res.text != null && res.text != string.Empty)
                {
                    var result = ParseStringToDictionary<KeyT, T>(res.text, indexKeyName);
                    return result; // Do it separately, so we got the exception here.
                }
                else
                {
                    throw new ParseException("Resource file not exists, cannot be loaded or empty.");
                }
            }
            catch (Exception ex)
            {
            	Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                throw;
            }
        }

        public static Dictionary<KeyT, T> ParseResourceToDictionaryWithCompositeKey<KeyT, T> (string resourceName)
            where T : new()
            where KeyT : new()
        {
            try
            {
                var res = Resources.Load<TextAsset>(resourceName);
                if (res != null && res.text != null && res.text != string.Empty)
                {
                    var result = ParseStringToDictionaryWithCompositeKey<KeyT, T>(res.text);
                    return result; // Do it separately, so we got the exception here.
                }
                else
                {
                    throw new ParseException("Resource file not exists, cannot be loaded or empty.");
                }
            }
            catch (Exception ex)
            {
            	Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                throw;
            }
        }

        public static IEnumerable<T> ParseResourceToObjects<T> (string resourceName)
            where T : new()
        {
            // NOTE: All this hackery allows us to catch exceptions both on parse initialization and iteration
            // and work around issue that yield is not allowed inside try-catch blocks.

            IEnumerator<T> parseResult = null;

            try
            {
                var res = Resources.Load<TextAsset>(resourceName);
                if (res != null && res.text != null && res.text != string.Empty)
                {
                    parseResult = ParseStringToObjects<T>(res.text).GetEnumerator();
                }
                else
                {
                    throw new ParseException("Resource file not exists, cannot be loaded or empty.");
                }
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                throw;
            }

            while (true)
            {
                T ret = default(T);

                try
                {
                    if (!parseResult.MoveNext())
                    {
                        break;
                    }
                    ret = parseResult.Current;
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Got exception while parsing resource '{0}': {1}.", resourceName, ex);
                    throw;
                }

                yield return ret;
            }
        }
        #endregion

        #region Private methods.
        private static int[] PrepareFieldsMapping (string[] keys, FieldInfo[] fieldsInfo)
        {
            var fieldsMapping = new int[fieldsInfo.Length];
            for (int i = 0; i < fieldsInfo.Length; i++)
            {
                CSVNameAttribute csvNameAttr = (CSVNameAttribute)Attribute.GetCustomAttribute(fieldsInfo[i], typeof(CSVNameAttribute), true);
                var mappedName = (csvNameAttr == null ? fieldsInfo[i].Name : csvNameAttr.CSVName);
                fieldsMapping[i] = Array.IndexOf(keys, mappedName); // Can fill it with -1.
            }
            return fieldsMapping;
        }

        private static int[] PrepareMethodsMapping (string[] keys, MethodInfo[] methodsInfo)
        {
            var methodsMapping = new int[methodsInfo.Length];
            for (int i = 0; i < methodsInfo.Length; i++)
            {
                CSVNameAttribute csvNameAttr = (CSVNameAttribute)Attribute.GetCustomAttribute(methodsInfo[i], typeof(CSVNameAttribute), true);
                if (csvNameAttr != null)
                {
                    methodsMapping[i] = Array.IndexOf(keys, csvNameAttr.CSVName); // Can fill it with -1.
                }
                else
                {
                    methodsMapping[i] = -1;
                }
            }
            return methodsMapping;
        }

        private static T ParseLineToObject<T>
            ( int lineNumber
            , string[] lineEntries
            , FieldInfo[] fieldsInfo
            , int[] fieldsMapping
            , MethodInfo[] methodsInfo
            , int[] methodsMapping)
                where T: new()
        {
            object valueObj = new T(); // 'object' is used to box structs, so you can use FieldInfo.SetValue() on them. It will be unboxed on return.

            if (fieldsMapping != null)
            {
                for (int j = 0; j < fieldsMapping.Length; j++)
                {
                    var mappedFieldIdx = fieldsMapping[j];

                    if (mappedFieldIdx >= 0 && mappedFieldIdx < lineEntries.Length)
                    {
                        var fieldInfo = fieldsInfo[j];
                        var entryValue = lineEntries[mappedFieldIdx];

                        var val = ParseValue(lineNumber, mappedFieldIdx, entryValue, fieldInfo.FieldType);

                        if (val != null)
                            fieldInfo.SetValue(valueObj, val);
                    }
                }
            }

            if (methodsMapping != null)
            {
                for (int j = 0; j < methodsMapping.Length; j++)
                {
                    var mappedFieldIdx = methodsMapping[j];

                    if (mappedFieldIdx >= 0 && mappedFieldIdx < lineEntries.Length)
                    {
                        var methodInfo = methodsInfo[j];
                        var entryValue = lineEntries[mappedFieldIdx];
                        var methodParameters = methodInfo.GetParameters();
                        if (methodParameters.Length == 1)
                        {
                            var val = ParseValue(lineNumber, mappedFieldIdx, entryValue, methodParameters[0].ParameterType);

                            if (val != null)
                                methodInfo.Invoke (valueObj, new object[] { val } );
                        }
                    }
                }
            }

            return (T)valueObj;
        }

        private static object ParseValue (int lineNumber, int idx, string value, Type t)
        {
            if (value == null || value == string.Empty)
                return null;
            
            if (t.IsEnum)
            {
                return Enum.Parse (t, value, true);
            }
            else if (t == typeof(string))
            {
                return value;
            }
//          else if (t == typeof(int))
//          {
//              fieldInfo.SetValue(valueObj, Convert.ToInt32(entryValue));
//          }
//          else if (t == typeof(uint))
//          {
//              fieldInfo.SetValue(valueObj, Convert.ToUInt32(entryValue));
//          }
            else if (t == typeof(bool))
            {
                bool val = false;
                if (!bool.TryParse(value, out val))
                {
                    int intVal = Convert.ToInt32(value);
                    val = (intVal > 0);
                }

                return val;
            }
            else if (t == typeof(TimeSpan))
            {
                int days = 0, hours = 0, minutes = 0, seconds = 0;

                Regex regex = new Regex(@"^((?<days>\d+)d)?\s?((?<hours>\d+)h)?\s?((?<minutes>\d+)m)?\s?((?<seconds>\d+)s)?$");
                var match = regex.Match(value);
                if (match != null && match.Success && match.Groups.Count > 0)
                {
                    var daysGroup = regex.GroupNumberFromName("days");
                    if (daysGroup >= 0 && daysGroup < match.Groups.Count && match.Groups[daysGroup].Value != string.Empty)
                    {
                        days = Convert.ToInt32(match.Groups[daysGroup].Value);
                    }

                    var hoursGroup = regex.GroupNumberFromName("hours");
                    if (hoursGroup >= 0 && hoursGroup < match.Groups.Count && match.Groups[hoursGroup].Value != string.Empty)
                    {
                        hours = Convert.ToInt32(match.Groups[hoursGroup].Value);
                    }

                    var minutesGroup = regex.GroupNumberFromName("minutes");
                    if (minutesGroup >= 0 && minutesGroup < match.Groups.Count && match.Groups[minutesGroup].Value != string.Empty)
                    {
                        minutes = Convert.ToInt32(match.Groups[minutesGroup].Value);
                    }

                    var secondsGroup = regex.GroupNumberFromName("seconds");
                    if (secondsGroup >= 0 && secondsGroup < match.Groups.Count && match.Groups[secondsGroup].Value != string.Empty)
                    {
                        seconds = Convert.ToInt32(match.Groups[secondsGroup].Value);
                    }

                    return new TimeSpan(days, hours, minutes, seconds);
                }

                // Try to parse value as raw seconds.
                seconds = Convert.ToInt32(value);
                return TimeSpan.FromSeconds(seconds);
            }
            else
            {   // Use generic conversion for other types.
                return Convert.ChangeType(value, t);
            }
        }

        private static string Unescape (string text)
        {
            return (text.IndexOf('\\') >= 0) ? Regex.Unescape(text) : text;
        }
        #endregion
    }
}