﻿using FileSystem;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;

namespace DataProcessing.CSV
{
    public class DataRow : IDictionary<string, string>
    {
        #region ICollection fields.
        public ICollection<string> Keys
        {
            get
            {
                return keys_;
            }
        }

        public ICollection<string> Values
        {
            get
            {
                return values_;
            }
        }

        public int Count
        {
            get
            {
                return keys_.Length;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region IDictionary fields.
        public string this[string key]
        {
            get
            {
                if (key == null)
                    throw new ArgumentNullException("key");

                var idx = Array.IndexOf(keys_, key);
                if (idx >= 0)
                {
                    return values_[idx];
                }

                throw new KeyNotFoundException(string.Format("The given key ({0}) was not present in the dictionary.", key));
            }

            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region Constructor.
        public DataRow(int idx, string[] keys, string[] values)
        {
            idx_ = idx;
            keys_ = keys;
            values_ = values;
        }

        #endregion

        #region Public API.
        public int id
        {
            get
            {
                return GetInt("id");
            }
        }

        public int idx
        {
            get
            {
                return idx_;
            }
        }

        public string this[int idx]
        {
            get
            {
                if (idx >= 0 && idx < values_.Length)
                {
                    return values_[idx];
                }
                else
                {
                    throw new ArgumentOutOfRangeException("idx");
                }
            }
        }

        public int GetInt(string key, int defaultValue = -1)
        {
            string value = string.Empty;
            if (TryGetValue(key, out value))
            {
                if (value == string.Empty)
                    return defaultValue;

                return int.Parse(value);
            }
            return defaultValue;
        }

        public bool GetBool(string key, bool defaultValue = false)
        {
            string value = string.Empty;
            if (TryGetValue(key, out value))
            {
                if (value == string.Empty)
                    return defaultValue;

                return (int.Parse(value) != 0);
            }
            return defaultValue;
        }

        public string GetString(string key, string defaultValue = "")
        {
            string value = string.Empty;
            if (TryGetValue(key, out value))
            {
                return value;
            }
            return defaultValue;
        }

        public void InitObject<T>(ref T obj)
        {
            var type = obj.GetType();
            object boxedValueType = null;
            if (type.IsValueType)
            {
                boxedValueType = obj;
            }

            foreach (FieldInfo fieldinfo in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                CSVNameAttribute csvNameAttr = (CSVNameAttribute)Attribute.GetCustomAttribute(fieldinfo, typeof(CSVNameAttribute), true);

                if (fieldinfo.FieldType == typeof(int))
                {
                    fieldinfo.SetValue(boxedValueType != null ? boxedValueType : obj
                        , GetInt(csvNameAttr == null ? fieldinfo.Name : csvNameAttr.CSVName));
                }
                else if (fieldinfo.FieldType == typeof(string))
                {
                    fieldinfo.SetValue(boxedValueType != null ? boxedValueType : obj
                        , GetString(csvNameAttr == null ? fieldinfo.Name : csvNameAttr.CSVName));
                }
                else if (fieldinfo.FieldType == typeof(bool))
                {
                    fieldinfo.SetValue(boxedValueType != null ? boxedValueType : obj
                        , GetBool(csvNameAttr == null ? fieldinfo.Name : csvNameAttr.CSVName));
                }
            }

            if (boxedValueType != null)
            {
                obj = (T)boxedValueType;
            }
        }
        #endregion

        #region IDictionary<string, string> public API.
        public void Add (string key, string value)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey (string key)
        {
            return Array.IndexOf(keys_, key) >= 0;
        }

        public bool Remove (string key)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue (string key, out string value)
        {
            if (key == null)
                throw new ArgumentNullException("key");

            var idx = Array.IndexOf(keys_, key);
            if (idx >= 0)
            {
                value = values_[idx];
                return true;
            }

            value = default(string);
            return false;
        }
        #endregion

        #region ICollection<KeyValuePair<string, string>> public API.
        public void Add (KeyValuePair<string, string> item)
        {
            throw new NotImplementedException();
        }

        public void Clear ()
        {
            throw new NotImplementedException();
        }

        public bool Contains (KeyValuePair<string, string> item)
        {
            int minLength = Math.Min(keys_.Length, values_.Length);
            for (int i = 0; i < minLength; i++)
            {
                if (keys_[i] == item.Key && values_[i] == item.Value)
                    return true;
            }

            return false;
        }

        public void CopyTo (KeyValuePair<string, string>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove (KeyValuePair<string, string> item)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IEnumerator<KeyValuePair<string, string>> public API.
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator ()
        {
            for (int i = 0; i < keys_.Length; i++)
            {
                yield return new KeyValuePair<string, string>(keys_[i], i < values_.Length ? values_[i] : null);
            }
        }
        #endregion

        #region IEnumerator interface API.
        IEnumerator IEnumerable.GetEnumerator ()
        {
            for (int i = 0; i < keys_.Length; i++)
            {
                yield return new KeyValuePair<string, string>(keys_[i], i < values_.Length ? values_[i] : null);
            }
        }
        #endregion

        #region Private data.
        private string[] keys_; // This array is shared among all DataRow's parsed from one file.
        private string[] values_; // Personal array.
        private int idx_;
        #endregion
    }
}