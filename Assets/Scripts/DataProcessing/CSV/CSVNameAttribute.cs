﻿using System;

namespace DataProcessing.CSV
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class CSVNameAttribute : Attribute
    {
        public CSVNameAttribute(string csvName)
        {
            this.csvName = csvName;
        }

        public string CSVName
        {
            get { return csvName; }
        }

        private string csvName;
    }
}