﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace DataProcessing.JSON
{
    /// <summary>
    /// Converts an <see cref="Enum"/> to and from its name string value.
    /// </summary>
    public class StringTimeSpanConverter : JsonConverter
    {
        /// <summary>
        /// Gets or sets a value indicating whether the written enum text should be camel case.
        /// </summary>
        /// <value><c>true</c> if the written enum text will be camel case; otherwise, <c>false</c>.</value>
        public bool CamelCaseText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether integer values are allowed.
        /// </summary>
        /// <value><c>true</c> if integers are allowed; otherwise, <c>false</c>.</value>
        public bool AllowIntegerValues { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringEnumConverter"/> class.
        /// </summary>
        public StringTimeSpanConverter ()
        {
            AllowIntegerValues = true;
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>

        public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
        {
            /*
            if (value == null)
            {
	            writer.WriteNull();
	            return;
            }

            Enum e = (Enum)value;

            string enumName = e.ToString("G");

            if (char.IsNumber(enumName[0]) || enumName[0] == '-')
            {
	            // enum value has no name so write number
	            writer.WriteValue(value);
            }
            else
            {
	            Type enumType = e.GetType();

	            string finalName = EnumUtils.ToEnumName(enumType, enumName, CamelCaseText);

	            writer.WriteValue(finalName);
            }
            */
        }


        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                //if (!ReflectionUtils.IsNullableType(objectType))
                //{
                //	throw JsonSerializationException.Create(reader, "Cannot convert null value to {0}.".FormatWith(CultureInfo.InvariantCulture, objectType));
                //}

                return null;
            }

            //bool isNullable = ReflectionUtils.IsNullableType(objectType);
            //Type t = isNullable ? Nullable.GetUnderlyingType(objectType) : objectType;

            try
            {
                if (reader.TokenType == JsonToken.String)
                {
                    string timeText = reader.Value.ToString();
                    return GetTmeSpan(timeText);
                }

                if (reader.TokenType == JsonToken.Integer)
                {
                    return new TimeSpan(0, 0, 0, Convert.ToInt32(reader.Value));
                }
            }
            catch (Exception)
            {
                //throw JsonSerializationException.Create(reader, "Error converting value {0} to type '{1}'.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.FormatValueForPrint(reader.Value), objectType), ex);
            }

            return null;
            // we don't actually expect to get here.
            //throw JsonSerializationException.Create(reader, "Unexpected token {0} when parsing enum.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>
        /// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        ///

        public override bool CanConvert (Type objectType)
        {
            /*
			Type t = (ReflectionUtils.IsNullableType(objectType))
				? Nullable.GetUnderlyingType(objectType)
				: objectType;

			return t.IsEnum();
			*/

            return true;
        }

        TimeSpan GetTmeSpan (string value)
        {
            int days = 0, hours = 0, minutes = 0, seconds = 0;

            Regex regex = new Regex (@"^((?<days>\d+)d)?\s?((?<hours>\d+)h)?\s?((?<minutes>\d+)m)?\s?((?<seconds>\d+)s)?$");
            var match = regex.Match (value);
            if (match != null && match.Success && match.Groups.Count > 0)
            {
                var daysGroup = regex.GroupNumberFromName ("days");
                if (daysGroup >= 0 && daysGroup < match.Groups.Count && match.Groups[daysGroup].Value != string.Empty)
                {
                    days = Convert.ToInt32(match.Groups[daysGroup].Value);
                }

                var hoursGroup = regex.GroupNumberFromName ("hours");
                if (hoursGroup >= 0 && hoursGroup < match.Groups.Count && match.Groups[hoursGroup].Value != string.Empty)
                {
                    hours = Convert.ToInt32(match.Groups[hoursGroup].Value);
                }

                var minutesGroup = regex.GroupNumberFromName ("minutes");
                if (minutesGroup >= 0 && minutesGroup < match.Groups.Count && match.Groups[minutesGroup].Value != string.Empty)
                {
                    minutes = Convert.ToInt32(match.Groups[minutesGroup].Value);
                }

                var secondsGroup = regex.GroupNumberFromName ("seconds");
                if (secondsGroup >= 0 && secondsGroup < match.Groups.Count && match.Groups[secondsGroup].Value != string.Empty)
                {
                    seconds = Convert.ToInt32(match.Groups[secondsGroup].Value);
                }

                return new TimeSpan(days, hours, minutes, seconds);
            }

            seconds = Convert.ToInt32(value);
            return TimeSpan.FromSeconds(seconds);
        }
    }
}
