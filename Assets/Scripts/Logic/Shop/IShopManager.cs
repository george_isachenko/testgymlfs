﻿using System;
using Data;
using Data.Estate;

namespace Logic.Shop
{
    public interface IShopManager
    {
        bool IsItemOwned(ShopItemData item);
        void RegisterOwnership(ShopItemData item);
        void UnregisterOwnership(ShopItemData item);
        bool HasOwnerhsip(ShopItemData item);
        ShopItemData FindShopItemForEstateType(EstateType estateType);

        bool ShowRewardedAd(Action<bool, Cost> callback);
        void ConfirmRewardedAd(Cost reward);
        void OpenAdTab();
    }
}