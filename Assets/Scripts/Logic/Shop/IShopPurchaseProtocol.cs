﻿using Data;
using System;

namespace Logic.Shop
{
    public interface IShopPurchaseProtocol
    {
        ShopItemData itemToBuy { get; }

        bool InitPreview(ShopItemData item, Action<bool> OnFinishedPreview = null);
        bool ConfirmPurchase();
        void CancelPurchase();
        void RestoreOwnedItem(ShopItemData item);
        void BuyIAP(string iapProductId);
        //ShopItemData FindShopItemByIAPProductId(string iapProductId);
    }
}