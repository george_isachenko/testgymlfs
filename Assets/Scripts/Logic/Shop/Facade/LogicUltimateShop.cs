﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Advertisement;
using Core.Analytics;
using Core.IAP;
using Core.Timer;
using Data;
using Data.Estate;
using Data.PlayerProfile;
using Data.Room;
using DataProcessing.CSV;
using Logic.FacadesBase;
using Logic.PlayerProfile.Events;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI;

namespace Logic.Shop.Facade
{
    [AddComponentMenu("Kingdom/Logic/Shop/Facade/Logic Ultimate Shop")]
    [BindBridgeInterface(typeof(PresentationUltimateShop))]
    [DisallowMultipleComponent]
    public class LogicUltimateShop : BaseLogicFacade 
    {
        PresentationUltimateShop presenter;
        public IShopPurchaseProtocol      buyEquipmentDecor     { get { return gameCore.equipmentDecorShopPurchaseProtocol; } }
        public IShopPurchaseProtocol      buyInterior           { get { return gameCore.wallFloorShopPurchaseProtocol; } }
        public IShopPurchaseProtocol      buyIAP                { get { return gameCore.iapShopPurchaseProtocol; } }


        public List<UltimateShopItemData> itemsEquip            = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsInterior         = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsDecor            = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsSportEquip       = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsSportResources   = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsBankCoins        = new List<UltimateShopItemData>();
        public List<UltimateShopItemData> itemsBankBucks        = new List<UltimateShopItemData>();

        public bool lockSportFeatures                   { get { return LockSportFeatures(); } }
        public bool lockDecor                           { get { return decorStartLevel > playerProfile.level; } }
        public int  decorStartLevel                     { get; private set; }
        public bool isPromoPurchaseEnable               { get{ return data.promoInfo != null && data.promoInfo.promoPurchaseId >= 0;}}
        public int currentPromoId                       { get{ return data.promoInfo != null ? data.promoInfo.promoPurchaseId : 0;}}
        public bool promoPurchaseWasShow                { get { return data.promoInfo != null && data.promoInfo.wasShow; } } //set { if (data.promoInfo != null) data.promoInfo.wasShow = value;}}

        LogicShop.PersistentData data    { get { return gameCore.shop.persistentData; } }

        public bool forceBuySportResource
        {
            get { return isAnyTutorialRunning; }
        }

        public bool isClubInEditMode
        {
            get { return gameCore.equipmentManager.isInEditMode; }
        }

        [BindBridgeSubscribe]
        void Subscribe(PresentationUltimateShop presenter)
        {
            this.presenter = presenter;
        }

        void Start()
        {
            decorStartLevel = DataTables.instance.estateTypes.Min( d => d.Value.itemType == EstateType.Subtype.Decor ? d.Value.levelRequirement : int.MaxValue);
            LevelUpEvent.Event += OnLevelUp;
            gameCore.shop.onIAPProcessPurchase += OnIAPPurchase;
        }

        void OnDestroy()
        {
            LevelUpEvent.Event -= OnLevelUp;
            gameCore.shop.onIAPProcessPurchase -= OnIAPPurchase;
        }

        void Awake()
        {
            InitItems(itemsEquip,       DataTables.instance.equipmentShop, UltimateShopItemType.Equip);
            InitItems(itemsDecor,       DataTables.instance.decorShop, UltimateShopItemType.Decor);
            InitInteriorItems();

            InitSportEquip();
            InitSportResources();
            InitBankCoins();
            InitBankBucks();

            foreach (var item in itemsEquip)
            {
                item.trainerLevel = DataTables.instance.estateTypes[item.id].trainerLevelRequirement;
            }
        }

        void InitItems(List<UltimateShopItemData> items, DataRow[] table, UltimateShopItemType type)
        {
            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var row = table[i];

                var level = row.GetInt("level");
                if (level == -1)
                    level = DataTables.instance.estateTypes[row.id].levelRequirement;

                item.id = row.id;
                item.name = Loc.Get(row.GetString("name"));

                item.cost = new Cost(row.GetString("price_type"), row.GetInt("price"), level);
                item.meshPrefab = row.GetString("mesh_prefab");

                item.type = type;

                items.Add(item);
            }
        }

        void InitEquipItems()
        {
            var table = DataTables.instance.shopEquip;
            var items = itemsEquip;

            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];
                var level = DataTables.instance.estateTypes[tableItem.id].levelRequirement;

                item.name = Loc.Get(tableItem.name);
                item.id = tableItem.id;
                item.cost = new Cost(tableItem.price_type, tableItem.price, level);
                item.meshPrefab = tableItem.mesh_prefab;
                item.type = UltimateShopItemType.Equip;

                items.Add(item);
            }
        }

        void InitInteriorItems()
        {
            var table = DataTables.instance.shopInterior;
            var items = itemsInterior;

            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];

                item.name =Loc.Get(tableItem.name);
                item.id = tableItem.id;
                item.cost = new Cost(tableItem.price_type, tableItem.price, tableItem.level);
                item.type = UltimateShopItemType.Interior;

                items.Add(item);
            }
        }

        void InitDecorItems()
        {
            var table = DataTables.instance.shopDecor;
            var items = itemsDecor;

            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];
                var level = DataTables.instance.estateTypes[tableItem.id].levelRequirement;

                item.name = Loc.Get(tableItem.name);
                item.id = tableItem.id;
                item.cost = new Cost(tableItem.price_type, tableItem.price, level);
                item.meshPrefab = tableItem.mesh_prefab;
                item.type = UltimateShopItemType.Decor;

                items.Add(item);
            }
        }

        void InitSportEquip()
        {
            var table = DataTables.instance.sportEquipment;
            var items = itemsSportEquip;

            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];

                item.name = Loc.Get(tableItem.name);
                item.id = tableItem.id;
                item.cost = tableItem.cost;
                item.meshPrefab = tableItem.meshPrefab;
                item.type = UltimateShopItemType.SportEquip;

                items.Add(item);
            }
        }

        void InitSportResources()
        {
            var table = DataTables.instance.sportResources;
            var items = itemsSportResources;

            for (int i = 0; i < table.Length; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];

                item.name = Loc.Get(tableItem.name);
                item.id = (int)tableItem.id;
                item.cost = tableItem.cost;
                item.icon = GUICollections.instance.storageIcons.items[item.id].sprite;
                item.type = UltimateShopItemType.SportResources;

                items.Add(item);
            }
        }

        void InitBankCoins()
        {
            var table = DataTables.instance.bankShop;
            var items = itemsBankCoins;

            for (int i = 0; i < 6; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i];

                item.type = UltimateShopItemType.Bank;
                item.name = Loc.Get(tableItem.GetString("name"));
                item.id = i;
                item.cost = new Cost(Cost.CostType.CoinsOnly, tableItem.GetInt("coins"), 0);
                item.icon = GUICollections.instance.shopBankTextures.array[i];
                item.badgeType = (BankBadgeType) Enum.Parse(typeof(BankBadgeType), tableItem.GetString("badgeType"), true);
                item.percent = tableItem.GetInt("percent", 0);

                items.Add(item);
            }
        }

        void InitBankBucks()
        {
            var table = DataTables.instance.bankShop;
            var items = itemsBankBucks;

            var offset = 6;

            for (int i = 0; i < 6; i++)
            {
                var item = new UltimateShopItemData();
                var tableItem = table[i + offset];

                item.type = UltimateShopItemType.Bank;
                item.name = Loc.Get(tableItem.GetString("name"));
                item.id = i + offset;
                item.cost = new Cost(Cost.CostType.BucksOnly, tableItem.GetInt("bucks"), 0);
                item.icon = GUICollections.instance.shopBankTextures.array[i + offset];
                item.badgeType = (BankBadgeType) Enum.Parse(typeof(BankBadgeType), tableItem.GetString("badgeType"), true);
                item.percent = tableItem.GetInt("percent", 0);

                items.Add(item);
            }
        }

        public EstateType BestEquipmentForTrainerLevel(int level)
        {
            for (int i = itemsEquip.Count - 1; i >= 0; i--)
            {
                var item = itemsEquip[i];

                if (item.trainerLevel <= level)
                    return DataTables.instance.estateTypes[item.id];
            }

            // default
            return DataTables.instance.estateTypes[itemsEquip.First().id];
        }

        public UltimateShopItemData BankItemByIdx(int idx)
        {
            foreach (var item in itemsBankBucks)
            {
                if (item.id == idx)
                    return item;
            }

            foreach (var item in itemsBankCoins)
            {
                if (item.id == idx)
                    return item;
            }

            return null;
        }

        bool LockSportFeatures()
        {
            return gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) <= 0; 
        }

        public ShopItemData GetOldShopItemData(UltimateShopItemData data)
        {
            var oldItem = new ShopItemData();

            switch(data.type)
            {
            case UltimateShopItemType.Decor:
                oldItem.category = ShopItemCategory.DECORS;
                oldItem.estateTypeData = DataTables.instance.estateTypes[data.id];
                break;

            case UltimateShopItemType.Equip:
                oldItem.category = ShopItemCategory.EQUIPMENTS;
                oldItem.estateTypeData = DataTables.instance.estateTypes[data.id];
                break;

            case UltimateShopItemType.Interior:
                oldItem.category = ShopItemCategory.PAINTS;
                break;

            case UltimateShopItemType.Bank:
                oldItem.category = ShopItemCategory.BANK;
                var tmp = new BankItemData();
                tmp.productInfo = GetIapInfo(data.id);
                tmp.iapProductId = DataTables.instance.bankShop[data.id].GetString("iap_id");
                oldItem.bankItemData = tmp;
                break;

            case UltimateShopItemType.SportEquip:
                oldItem.category = ShopItemCategory.SPORT_EQUIPMENTS;
                oldItem.estateTypeData = DataTables.instance.estateTypes[data.id];
                break;

            default:
                oldItem.category = ShopItemCategory.UNKNOWN;
                break;
            }

            oldItem.name = data.name;
            oldItem.price = data.cost;
            oldItem.id = data.id;

            return oldItem;
        }

        public string GetItemsInStorage(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.SportResources)
            {
                return playerProfile.storage.GetResourcesCount((ResourceType)item.id).ToString();
            }
            else if (item.type == UltimateShopItemType.SportEquip)
            {
                return gameCore.equipmentManager.equipment.Count(p => p.Value.estateType.id == item.id && !p.Value.isLocked).ToString();
            }
            
            return string.Empty;
        }

        public string GetItemsInStock(UltimateShopItemData item, out bool outOfStock)
        {
            outOfStock = false;

            if (item.type == UltimateShopItemType.Equip)
            {
                var total = gameCore.equipmentManager.GetEquipmentsCount(item.id);
                var max = gameCore.equipmentManager.maxSameEquipmentCount;

                outOfStock = (total == max);

                return total.ToString() + "/" + max.ToString();
            }
            else if (item.type == UltimateShopItemType.SportEquip)
            {
                outOfStock = gameCore.equipmentManager.FindAvailableLockedStaticEquipment(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub), item.id) == null;
            }

            return string.Empty;
        }

        public bool TryBuy(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.SportResources)
                return TryBuySportRes(item);
//             else if (item.type == UltimateShopItemType.SportEquip)
//                 return TryBuySportEquip(item);

            return false;
        }

        bool TryBuySportRes(UltimateShopItemData item)
        {
            /*            if (GetCooldownTime(itemLogic.id) > 0)
            {
                fail("Need to wait cooldown finish...");
                return;
            }*/
            var money = new Analytics.MoneyInfo("SportResource", ((ResourceType)item.id).ToString());

            if (gameCore.playerProfile.SpendMoney(item.cost, money))
            {
                if (gameCore.playerProfile.storage.AddResource((ResourceType)item.id, 1, forceBuySportResource, new Analytics.MoneyInfo("Shop",((ResourceType)item.id).ToString())))
                {
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    Achievements.BuySportResource.Send();
                    return true;
                }
                else
                {
                    Debug.LogError("Can't add buyed resource " + item.id + " to storage");
                }
            }

            return false;
        }

        public ShopUltimateViewItem.LockType GetLock(UltimateShopItemData item, int idx)
        {
            if (item.cost.level > playerProfile.level)
                return ShopUltimateViewItem.LockType.Level;

            if (item.type == UltimateShopItemType.SportEquip)
            {
                var table = DataTables.instance.sportEquipment;
                var isLocked = table[idx].requiredUpgradeLevel > gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub));

                if (isLocked)
                    return ShopUltimateViewItem.LockType.GymExpandSeparator + table[idx].requiredUpgradeLevel;
            }
            else if (item.type == UltimateShopItemType.SportResources)
            {
                
                var sportExercises = DataTables.instance.sportExercises;
                var sportEquipment = DataTables.instance.sportEquipment;
                for (int i = 0; i < sportExercises.Length; i++)
                {
                    if (sportExercises[i].needResource == (ResourceType)item.id)
                    {
                        var equip = sportEquipment.FirstOrDefault(e => e.id == sportExercises[i].equipmentId);

                        if (equip != null)
                        {
                            var isLocked = equip.requiredUpgradeLevel > gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub));

                            if (isLocked)
                                return ShopUltimateViewItem.LockType.GymExpandSeparator + equip.requiredUpgradeLevel;
                        }
                    }
                }

                if (data.timersSportRes.ContainsKey(item.id))
                    return ShopUltimateViewItem.LockType.Time;
                else if (item.cost.level > playerProfile.level)
                    return ShopUltimateViewItem.LockType.Level;
            }
            else if (item.type == UltimateShopItemType.Equip)
            {
                if (GetNeedSizeForEquip(item) > 0)
                    return ShopUltimateViewItem.LockType.GymSize;
            }

            return ShopUltimateViewItem.LockType.None;
        }

        /*
        public bool IsLocked(UltimateShopItemData item, int idx)
        {
            if (item.type == UltimateShopItemType.SportEquip)
            {
                var table = DataTables.instance.sportEquipment;
                return table[idx].requiredUpgradeLevel > gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub));
            }
            else if (item.type == UltimateShopItemType.SportResources)
            {
                if (data.timersSportRes.ContainsKey(item.id))
                    return true;
                else
                    return item.cost.level > playerProfile.level;
            }
            else
                return item.cost.level > playerProfile.level;
        }*/

        public bool IsItemOwned(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.Interior)
                return data.purchasedPaints.Exists(i => i == item.id);
            else
                return false;
        }

        public void RegisterOwnership(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.Interior)
            {
                if (!data.purchasedPaints.Exists(i => i == item.id))
                {
                    data.purchasedPaints.Add(item.id);
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                }
            }
        }

        public void AddTimerForItem(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.SportResources)
            {
                if (data.timersSportRes.ContainsKey(item.id))
                {
                    Debug.LogError("ACHTUNG. Can't add timer second time");
                    return;
                }

                var table = DataTables.instance.sportResources;

                var unlockTime = 0f;
                foreach (var tableItem in table)
                {
                    if ((int)tableItem.id == item.id)
                        unlockTime = tableItem.unlockTimeSeconds;
                }

                if (unlockTime == 0f)
                    unlockTime = 60;

                data.timersSportRes.Add(item.id, new TimerFloat(unlockTime));
                
                return;
            }

            Debug.LogError("ACHTUNG. wrong item type");

        }

        public TimerFloat GetTimeForItem(UltimateShopItemData item)
        {
            if (data.timersSportRes.ContainsKey(item.id))
            {
                var timer = data.timersSportRes[item.id];

                if (timer.secondsToFinish == 0)
                    data.timersSportRes.Remove(item.id);

                return timer;
            }

            return null;
        }

        public void RemoveTimerForItem(UltimateShopItemData item)
        {
            if (data.timersSportRes.ContainsKey(item.id))
            {
                data.timersSportRes.Remove(item.id);
            }
            else
            {
                Debug.LogError("Can't remove timer");
            }
        }

        public ProductInfo GetIapInfo(int idx)
        {
            var iap_id = DataTables.instance.bankShop[idx].GetString("iap_id");
            ProductInfo info;
            if (!PersistentCore.instance.iapManager.GetProductInfo(iap_id, out info))
                Debug.LogError("Can't get product info");
                
            return info;
        }

        public string GetIapStringPrice(int idx)
        {
            var info = GetIapInfo(idx);
            if (info.availableToPurchase)
            {
                return info.metaData.localizedPriceString;
            }
            else
                return "not available";
        }

        public int GetStyle(UltimateShopItemData item)
        {
            if (item.type == UltimateShopItemType.Decor)
            {
                return DataTables.instance.estateTypes[item.id].style;
            }

            return -1;
        }

        public int GetNeedSizeForEquip (UltimateShopItemData item) // -1 - тренажеров максимально количество
        {
            if (item.type == UltimateShopItemType.Equip)
            {
                Data.Estate.EstateType estatetype;
                if (DataTables.instance.estateTypes.TryGetValue (item.id, out estatetype)) 
                {
                    bool canBuy = gameCore.equipmentManager.IsCanBuyThis (item.id);
                    if (!canBuy) 
                    {
                        return gameCore.equipmentManager.SquareNeedForThis (item.id);
                    } 
                }
            }

            return 0;
        }
            
        public string GetRoomNameForLock(ShopUltimateViewItem.LockType lockType)
        {
            var requiredExpandLevel = lockType - ShopUltimateViewItem.LockType.GymExpandSeparator;
            if (requiredExpandLevel < 0)
                return "";
            var targetEquip =
                DataTables.instance.sportEquipment.Where(
                    e => e.requiredUpgradeLevel == requiredExpandLevel).ToArray();
            if (targetEquip.Any())
                return Loc.Get(targetEquip.First().sportEquipRoomLocId);
            return "";
        }

        void OnLevelUp(int lvl)
        {
            DetectNewItems(itemsEquip);
            DetectNewItems(itemsDecor);
            DetectNewItems(itemsInterior);

            gameCore.shop.DetectNewPromo();

            Advertisements.instance.RequestRewardedVideo();

            presenter.UpdateNewItems();
        }

        public List<UltimateShopNewInfo> GetNew()
        {
            UltimateShopNewInfo info;
            var list = new List<UltimateShopNewInfo>();

            foreach (var item in data.newItems)
            {
                info.id = item.id;
                info.type = OldShopToNew.Category(item.category);
                list.Add(info);
            }

            return list;
        }

        public void MarkNewItemsAsSeen(UltimateShopItemType category)
        {
            var oldCat = NewShopToOld.Category(category);

            var removedCount = data.newItems.RemoveAll(x => x.category == oldCat);

            if (removedCount > 0)
                gameCore.ScheduleSave(ScheduledSaveType.SaveSoon);
        }

        void DetectNewItems(List<UltimateShopItemData> items)
        {
            NewItemType newItem;

            var lvl = gameCore.playerProfile.level;

            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];

                if (item.cost.level == lvl)
                {
                    newItem.id = item.id;
                    newItem.category = GetOldShopItemData(item).category;

                    if (newItem.category != ShopItemCategory.UNKNOWN)
                    {
                        data.newItems.Add(newItem);
                    }
                    else
                    {
                        Debug.LogError("DetectNewItems : UNKNOWN category");
                    }
                }
            }            
        }

        public bool IsRewardedVideoAvailable()
        {
            var allowed     = (gameCore.playerProfile.level >= DataTables.instance.balanceData.Advertisement.startLevel);
            var available   = Advertisements.instance.IsRewardedVideoAvailable();

            return allowed && available;
        }

        public bool IsRewardedVideoAvailableForTV()
        {
            return IsRewardedVideoAvailable() && (gameCore.playerProfile.level >= DataTables.instance.balanceData.Advertisement.enableOnTVLevel);
        }

        public Cost.CostType                advertisementRewardType = Cost.CostType.BucksOnly;
        public int                          advertisementRewardCount = 1;

        public bool ShowRewardedAd(Action<bool, Cost> callback)
        {
            return Advertisements.instance.ShowRewardedAd
                (   delegate(ShowResult result, int count_, string currencyName)
                    { 
                        advertisementRewardType = Cost.CostType.BucksOnly;

                        if(currencyName != null)
                        {
                            //Debug.Log("CURRENCY: " + currencyName + " " + currencyName.Contains("Coins").ToString());

                            if (currencyName.Contains("FitBucks"))
                                advertisementRewardType = Cost.CostType.BucksOnly;
                            else if (currencyName.Contains("Coins"))
                                advertisementRewardType = Cost.CostType.CoinsOnly;
                        }

                        callback (result == ShowResult.Finished, new Cost (advertisementRewardType, count_, 1));
                    },
                    gameCore.playerProfile.fitBucks
                );
        }
            
        public void ConfirmRewardedAd(Cost reward)
        {
            Assert.IsNotNull(reward);

            if (reward.type != Cost.CostType.None)
            {
                gameCore.playerProfile.AddReward(reward, new Analytics.MoneyInfo("RewardedVideo", "RewardedVideo"), true);
            }
            else
            {
                Debug.LogWarning("LogicShop:ConfirmRewardedAd: trying to claim reward for advertisement without it being set to something.");
            }
        }

        public ShopItemData FindShopItemByIAPProductId(string iapProductId)
        {
            return gameCore.shop.FindShopItemByIAPProductId(iapProductId);
        }

        void OnIAPPurchase(ShopItemData item)
        {
            if (item.bankItemData != null && item.bankItemData.Value.iapProductId.Contains("promo"))
                data.promoInfo.promoPurchaseId = -1;
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            presenter.OnPurchaseIAP();
        }

        #region Promo Purchase
        public TimerFloat GetTimerForPromoPurchase()
        {
            if (data.promoInfo != null && data.promoInfo.promoPurchaseId >= 0)
            {
                var timer = data.promoInfo.promoTimeLeft;

                if (timer.secondsToFinish == 0)
                    gameCore.shop.persistentData.promoInfo.promoPurchaseId = -1;

                return timer;
            }

            return null;
        }

        public void InitPromoPurchase()
        {
            if (data.promoInfo != null && data.promoInfo.promoPurchaseId >= 0 && !promoPurchaseWasShow)
            {
                PromoPurchases ppInfo = DataTables.instance.promoPurchases[currentPromoId];
                data.promoInfo.promoTimeLeft = new TimerFloat((float)ppInfo.duration.TotalSeconds);
                data.promoInfo.wasShow = true;
            }
        }
        #endregion

    }
}