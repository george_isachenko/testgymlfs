﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Core.Advertisement;
using Core.IAP;
using Core.Timer;
using Data;
using Data.IAP;
using Logic.FacadesBase;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;
using Core.Analytics;
using DataProcessing.CSV;
using Data.Estate;

namespace Logic.Shop.Facade
{
    [AddComponentMenu("Kingdom/Logic/Shop/Facade/Logic Shop")]
    [DisallowMultipleComponent]
    public class LogicShop
        : BaseLogicFacade
        , ILogicShop
        , IPersistent
        , IShopManager
        , IShopPurchaseProtocol
    {
        public struct PurchaseHistoryItem
        {
            public string inAppId;
            public string platform;
            public string store;
            public DateTime purchaseDate;
            public string transactionId;
            public string appVersion;
            public decimal moneySpent;

            public PurchaseHistoryItem(string inAppId, DateTime purchaseDate, string transactionId, string appVersion, decimal moneySpent, string platform, string store)
            {
                this.inAppId = inAppId;
                this.purchaseDate = purchaseDate;
                this.transactionId = transactionId;
                this.appVersion = appVersion;
                this.moneySpent = moneySpent;
                this.platform = platform;
                this.store = store;
            }
        }

        private static readonly int initialPurchasedItemsCount = 16;
        private static readonly int initialPurchaseHistoryCount = 32;

        #region Events
        public event IAPInitializedEvent            onIAPInitialized;
        public event IAPPurchaseFailedEvent         onIAPPurchaseFailed;
        public event IAPProcessPurchaseEvent        onIAPProcessPurchase;
        //public event RewardedVideoAvaialbleEvent    onRewardedVideoAvaialble;
        #endregion

        #region Properties
        IShopPurchaseProtocol ILogicShop.wallFloorShopPurchaseProtocol
        {
            get { return gameCore.wallFloorShopPurchaseProtocol; }
        }

        IShopPurchaseProtocol ILogicShop.equipmentShopPurchaseProtocol
        {
            get { return gameCore.equipmentDecorShopPurchaseProtocol; }
        }

        IShopPurchaseProtocol ILogicShop.bankShopPurchaseProtocol
        {
            get { return this; }
        }
        #endregion

        #region Public data
        public string                       googlePublicKey;
        [Tooltip("Valid only in developers builds.")]
        public FakeStoreUIMode              fakeStoreUIMode;

        public float                        initializationDelay;
        public float                        initializationDelayFreshInstall;

        public Cost.CostType                advertisementRewardType = Cost.CostType.BucksOnly;
        public int                          advertisementRewardCount = 1;
        #endregion

        public class PromoInfo
        {
            public int            promoPurchaseId;
            public TimerFloat     promoTimeLeft;
            public bool           wasShow;

            public PromoInfo()
            {
                promoPurchaseId = -1;
                promoTimeLeft = new TimerFloat(0);
                wasShow = false;
            }

        }

        #region Persistent data
        public struct PersistentData
        {
            public List<int>                        purchasedPaints;
            public List<PurchaseHistoryItem>        purchaseHistory;
            public List<NewItemType>                newItems;
            public Dictionary<int, TimerFloat>      timersSportRes;
            public PromoInfo                        promoInfo; 
        }
        #endregion

        #region IPersistent API
        string IPersistent.propertyName
        {
            get
            {
                return "Shop";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(PersistentData);
            }
        }

        ShopItemData IShopPurchaseProtocol.itemToBuy
        {
            get
            {
                return pendingBankItem;
            }
        }
        #endregion

        #region Private data.
        bool iapInitialized = false;
        public PersistentData persistentData;
        private ShopItemData pendingBankItem;
        #endregion

        #region Unity API

        private void Awake()
        {
            ReinitPersistentData();

            gameCore.onGameStarted += OnGameStarted;
            gameCore.onSaveGameCreated += OnSaveGameCreated;
        }

        void Start()
        {
            var iapManager = PersistentCore.instance.iapManager;
            Assert.IsNotNull(iapManager);

            if (iapManager != null)
            {
                iapManager.onProcessPurchase += OnIAPProcessPurchase;
                iapManager.onPurchaseFailed += OnIAPPurchaseFailed;
            }
                
            //Advertisements.instance.onRewardedVideoAvailable += OnRewardedVideoAvailable;
        }

        private void OnDestroy()
        {
            //Advertisements.instance.onRewardedVideoAvailable -= OnRewardedVideoAvailable;

            var iapManager = PersistentCore.instance.iapManager;
            if (iapManager != null)
            {
                iapManager.onProcessPurchase -= OnIAPProcessPurchase;
                iapManager.onPurchaseFailed -= OnIAPPurchaseFailed;
            }
        }
        #endregion

        #region ILogicShop interface.
        public void OpenAdTab()
        {
        }

        public int GetItemCount(ShopItemCategory category)
        {
            switch (category)
            {
                case ShopItemCategory.EQUIPMENTS:
                return DataTables.instance.equipmentShop.Length;

                case ShopItemCategory.PAINTS:
                return DataTables.instance.wallFloorShopItems.Count;

                case ShopItemCategory.BANK:
                return DataTables.instance.bankShop.Length;

                case ShopItemCategory.DECORS:
                return DataTables.instance.decorShop.Length;

                case ShopItemCategory.ADVERTISEMENT:
                return 1;
            }
            return -1;
        }

        public bool GetItem(ShopItemCategory category, int idx, out ShopItemData item)
        {
            if (idx >= 0)
            {
                switch (category)
                {
                    case ShopItemCategory.EQUIPMENTS:
                    {
                        var equipmentShopTable = DataTables.instance.equipmentShop;
                        if (equipmentShopTable != null && idx < equipmentShopTable.Length)
                        {
                            item = new ShopItemData();
                            item.category = category;

                            DataRow shopItemRow = equipmentShopTable[idx];
                            if (DataTables.instance.estateTypes.TryGetValue(shopItemRow.id, out item.estateTypeData))
                            {
                                shopItemRow.InitObject(ref item);
                                item.idx = shopItemRow.idx;

                                item.price = new Cost();
                                item.price.level = DataTables.instance.estateTypes[shopItemRow.id].levelRequirement;
                                item.price.value = shopItemRow.GetInt("price");
                                if (shopItemRow.GetString("price_type") == "Coins")
                                    item.price.type = Cost.CostType.CoinsOnly;
                                else
                                    item.price.type = Cost.CostType.BucksOnly;
                                //row
                                return true;
                            }
                            else
                            {
                                Debug.LogError("WRONG ID " + shopItemRow.id);
                            }
                        }
                    }; break;

                    case ShopItemCategory.DECORS:
                    {
                        var decorShopTable = DataTables.instance.decorShop;
                        if (decorShopTable != null && idx < decorShopTable.Length)
                        {
                            item = new ShopItemData();
                            item.category = category;

                            DataRow shopItemRow = decorShopTable [idx];
                            DataTables.instance.estateTypes.TryGetValue(shopItemRow.id, out item.estateTypeData);
                            shopItemRow.InitObject(ref item);

                            item.idx = shopItemRow.idx;

                            item.price = new Cost();
                            item.price.level = DataTables.instance.estateTypes[shopItemRow.id].levelRequirement;
                            item.price.value = shopItemRow.GetInt("price");
                            if (shopItemRow.GetString("price_type") == "Coins")
                                item.price.type = Cost.CostType.CoinsOnly;
                            else
                                item.price.type = Cost.CostType.BucksOnly;

                            return true;
                        };
                    }; break;

                    case ShopItemCategory.PAINTS:
                    {
                        var wallFloorShopItems = DataTables.instance.wallFloorShopItems;
                        if (wallFloorShopItems != null && idx < wallFloorShopItems.Count)
                        {
                            var wallFloorShopItem = wallFloorShopItems[idx];

                            item = new ShopItemData();
                            item.category = category;
                            item.idx = idx;

                            item.price = wallFloorShopItem.price;
                            item.name = wallFloorShopItem.name;
                            item.id = wallFloorShopItem.id;

                            return true;
                        }
                    }; break;

                    case ShopItemCategory.BANK:
                    {
                        var bankShopTable = DataTables.instance.bankShop;
                        var iapManager = PersistentCore.instance.iapManager;
                        Assert.IsNotNull(iapManager);

                        TryInit();

                        if (bankShopTable != null && iapManager != null && idx < bankShopTable.Length)
                        {
                            item = new ShopItemData();
                            item.category = category;

                            {
                                BankItemData bankItemData = new BankItemData();

                                DataRow shopItemRow = bankShopTable [idx];
                                shopItemRow.InitObject(ref item);
                                shopItemRow.InitObject(ref bankItemData);

                                ProductInfo productInfo;
                                Assert.IsNotNull(bankItemData.iapProductId);
                                if (iapManager.GetProductInfo(bankItemData.iapProductId, out productInfo))
                                {
                                    bankItemData.productInfo = productInfo;
                                }

                                item.bankItemData = bankItemData;
                            }

                            item.idx = idx;
                            item.price = new Cost();
                            item.price.level = 0;
                            item.price.value = item.bankItemData.Value.coins + item.bankItemData.Value.bucks;
                            if (item.bankItemData.Value.coins > 0)
                                item.price.type = Cost.CostType.CoinsOnly;
                            else if (item.bankItemData.Value.bucks > 0)
                                item.price.type = Cost.CostType.BucksOnly;
                            else
                                item.price.type = Cost.CostType.None;

                            return true;
                        }
                    }; break;

                    case ShopItemCategory.ADVERTISEMENT:
                    {
                        item = new ShopItemData();
                        item.category = category;
                        item.name = "Get FREE FitBucks";
                        item.price.type = Cost.CostType.None;

                        item.idx = idx;
                        return true;
                    }
                }
            }

            item = null;
            return false;
        }

        public int GetShopIndex(ShopItemCategory category, int id)
        {
            DataRow[] table;

            switch (category)
            {
                case ShopItemCategory.EQUIPMENTS:
                {
                    table = DataTables.instance.equipmentShop;
                    foreach (var row in table)
                    {
                        if (row.id == id)
                            return row.idx;
                    }
                }; break;

                case ShopItemCategory.DECORS:
                {
                    table = DataTables.instance.decorShop;
                    foreach (var row in table)
                    {
                        if (row.id == id)
                            return row.idx;
                    }

                }; break;

                case ShopItemCategory.PAINTS:
                {
                    var tTable = DataTables.instance.wallFloorShopItems;

                    for (int i = 0; i < tTable.Count; ++i)
                        if (tTable[i].id == id)
                            return i;
                }; break;

                case ShopItemCategory.BANK:
                {
                    table = DataTables.instance.bankShop;
                    foreach (var row in table)
                    {
                        if (row.id == id)
                            return row.idx;
                    }
                }; break;
            }

            return 0;
        }

        bool ILogicShop.IsItemLocked(ShopItemCategory category, int idx)
        {
            if (idx < 0)
                return false;

            ShopItemData item;
            if (GetItem(category, idx, out item))
            {
                if (category == ShopItemCategory.ADVERTISEMENT)
                {
                    return !IsRewardedVideoAvailable();
                }
                else if (category == ShopItemCategory.BANK)
                {
                    return !(item.bankItemData != null &&
                             item.bankItemData.Value.productInfo.availableToPurchase &&
                             item.bankItemData.Value.productInfo.metaData != null);
                }
                else
                {
                    return gameCore.playerProfile.level < item.price.level;
                }
            }

            return false;
        }

        int ILogicShop.GetNeedSizeForEquip(int idx) // -1 - тренажеров максимально количество
        {
            Data.Estate.EstateType estatetype;
            if (DataTables.instance.estateTypes.TryGetValue(idx, out estatetype))
            {
                bool canBuy = gameCore.equipmentManager.IsCanBuyThis (idx);
                if (!canBuy)
                {
                    return gameCore.equipmentManager.SquareNeedForThis(idx);
                }
            }

            return 0;
        }

        int ILogicShop.GetNeedLevelForDecor(int idx)
        {
            Data.Estate.EstateType estatetype;
            if (DataTables.instance.estateTypes.TryGetValue(idx, out estatetype))
            {
                int count = gameCore.equipmentManager.GetEquipmentsCount(idx);

                int[] levelsReqIncrement = {0, 3, 7, 12};

                if (count < 4)
                {
                    int levelReq = estatetype.levelRequirement + levelsReqIncrement[count];
                    return gameCore.playerProfile.level >= levelReq ? 0 : levelReq;
                }
                else
                {
                    return -1;
                }
            }

            return 0;
        }

        int ILogicShop.GetEquipmentsCount(int equipmentType)
        {
            return gameCore.equipmentManager.GetEquipmentsCount(equipmentType);
        }

        void ILogicShop.AddCoins(int value, Analytics.MoneyInfo info)
        {
            gameCore.playerProfile.AddCoins(value, info);
        }

        void ILogicShop.AddFitBucks(int value, Analytics.MoneyInfo info)
        {
            gameCore.playerProfile.AddFitBucks(value, info);
        }

        public bool IsRewardedVideoAvailable()
        {
            return (RewardedVideoDisplayingAllowed() && Advertisements.instance.IsRewardedVideoAvailable());
        }

        List<NewItemType> ILogicShop.GetNewItems()
        {
            return persistentData.newItems;
        }

        #endregion

        #region IShopManager interface.
        ShopItemData IShopManager.FindShopItemForEstateType(EstateType estateType)
        {
            for (int i = 0; i < GetItemCount(ShopItemCategory.EQUIPMENTS); i++)
            {
                ShopItemData item;
                if (GetItem(ShopItemCategory.EQUIPMENTS, i, out item))
                {
                    if (item.estateTypeData != null && item.estateTypeData == estateType)
                        return item;
                }
            }
            return null;
        }
        #endregion

        #region Common shared interface
        public bool IsItemOwned(ShopItemData item)
        {
            if (item.category == ShopItemCategory.PAINTS)
            {
                return persistentData.purchasedPaints.Exists(i => i == item.id);
            }
            else
                return false;
        }

        public void RegisterOwnership(ShopItemData item)
        {
            if (!persistentData.purchasedPaints.Exists(i => i == item.id))
            {
                persistentData.purchasedPaints.Add(item.id);
                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            }
        }

        public void UnregisterOwnership(ShopItemData item)
        {
            if (persistentData.purchasedPaints.Exists(i => i == item.id))
            {
                persistentData.purchasedPaints.Remove(item.id);
                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            }
        }

        public bool HasOwnerhsip(ShopItemData item)
        {
            return persistentData.purchasedPaints.Exists(i => i == item.id);
        }

        public bool ShowRewardedAd(Action<bool, Cost> callback)
        {
            return Advertisements.instance.ShowRewardedAd
                (delegate (ShowResult result, int count_, string currencyName)
                 {
                    if (currencyName.Contains("FitBucks"))
                        advertisementRewardType = Cost.CostType.BucksOnly;
                    else if (currencyName.Contains("Coins"))
                        advertisementRewardType = Cost.CostType.CoinsOnly;
                    else
                        advertisementRewardType = Cost.CostType.BucksOnly;

                    callback(result == ShowResult.Finished, new Cost(advertisementRewardType, /*advertisementRewardCount*/count_, 1));
                 }, 
                    gameCore.playerProfile.fitBucks
                );
        }

        public void ConfirmRewardedAd(Cost reward)
        {
            Assert.IsNotNull(reward);

            if (reward.type != Cost.CostType.None)
            {
                gameCore.playerProfile.AddReward(reward, new Analytics.MoneyInfo("RewardedVideo", "RewardedVideo"), true);
            }
            else
            {
                Debug.LogWarning("LogicShop:ConfirmRewardedAd: trying to claim reward for advertisement without it being set to something.");
            }
        }

        public void DetectNewPromo()
        {
            var lvl = gameCore.playerProfile.level;

            foreach (var it in persistentData.purchaseHistory)
                Debug.Log(it.inAppId);

            foreach (var item in DataTables.instance.promoPurchases)
            {
                if (item.startLevel == lvl && !persistentData.purchaseHistory.Exists(x => x.inAppId == item.newPriceId))
                {
                    persistentData.promoInfo.promoPurchaseId = item.id;
                    persistentData.promoInfo.promoTimeLeft = new TimerFloat((float)item.duration.TotalSeconds); //.secondsToFinish = item.duration.Seconds;  
                    persistentData.promoInfo.wasShow = false;
                    break;
                }
            }
                
        }
        #endregion

        #region Private API
        private void TryInit ()
        {
            if (!iapInitialized)
            {
                var iapManager = PersistentCore.instance.iapManager;
                Assert.IsNotNull(iapManager);
                if (iapManager != null)
                {
                    iapManager.Init(googlePublicKey, fakeStoreUIMode, OnIAPInitialized);
                    iapInitialized = true;
                }
            }
        }

        void OnGameStarted (bool isFreshInstall)
        {
            if (!isFreshInstall)
            {
                StartCoroutine(TryInitJob(initializationDelay));
            }
        }

        void OnSaveGameCreated()
        {
            StartCoroutine(TryInitJob(initializationDelayFreshInstall));
        }

        private IEnumerator TryInitJob (float initializationDelay)
        {
            yield return null;

            if (initializationDelay > 0)
            {
                yield return new WaitForSeconds(initializationDelay);
            }

            TryInit();
        }

        bool RewardedVideoDisplayingAllowed()
        {
            return (gameCore.playerProfile.level >= DataTables.instance.balanceData.Advertisement.startLevel);
        }

        private void ReinitPersistentData()
        {
            if (persistentData.purchasedPaints == null)
                persistentData.purchasedPaints = new List<int>(initialPurchasedItemsCount);
            else
                persistentData.purchasedPaints.Clear();

            if (persistentData.purchaseHistory == null)
                persistentData.purchaseHistory = new List<PurchaseHistoryItem>(initialPurchaseHistoryCount);
            else
                persistentData.purchaseHistory.Clear();

            if (persistentData.newItems == null)
                persistentData.newItems = new List<NewItemType>();
            else
                persistentData.newItems.Clear();

            if (persistentData.timersSportRes == null)
                persistentData.timersSportRes = new Dictionary<int, TimerFloat>();
            else
                persistentData.timersSportRes.Clear();

            persistentData.promoInfo = new PromoInfo();
        }

        /*
        bool OnRewardedVideoAvailable()
        {
            if (RewardedVideoDisplayingAllowed())
            {
                if (onRewardedVideoAvaialble != null)
                    return onRewardedVideoAvaialble();
            }
            return false;
        }
        */

        void OnLevelUp(int lvl)
        {
            DetectNewsFromCategory(ShopItemCategory.EQUIPMENTS, lvl);
            DetectNewsFromCategory(ShopItemCategory.DECORS, lvl);
            DetectNewsFromCategory(ShopItemCategory.PAINTS, lvl);

            Advertisements.instance.RequestRewardedVideo();
        }

        void DetectNewsFromCategory(ShopItemCategory category, int profileLevel)
        {
            NewItemType newItem;
            ShopItemData item;

            for (int i = 0; i < GetItemCount(category); i++)
            {
                if (GetItem(category, i, out item))
                {
                    if (item.price.level == profileLevel)
                    {
                        newItem.id = item.id;
                        newItem.category = category;
                        persistentData.newItems.Add(newItem);
                    }
                }
            }
        }
        #endregion

        #region IAP callbacks.
        void OnIAPInitialized(bool initialized, InitializationFailureReason? initializationFailureReason)
        {
            onIAPInitialized?.Invoke(initialized, initializationFailureReason);
        }

        void OnIAPPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Assert.IsNotNull(product);

            var shopItem = FindShopItemByIAPProductId(product.definition.id);

            Debug.LogFormat("LogicShop: Purchase failed. IAP Product ID: {0}, Item: {1} ({2}), Failure reason: {3}."
                , product.definition.id
                , (shopItem != null) ? shopItem.id : -1
                , (shopItem != null) ? shopItem.name : "?"
                , failureReason);

            if (shopItem != null)
            {
                if (failureReason != PurchaseFailureReason.UserCancelled)
                {
                    PersistentCore.instance.analytics.GameEvent("In-App purchase Failed", "Reason", failureReason.ToString());

                    PersistentCore.instance.QueueModalConfirmMessage
                        ( Loc.Get(string.Format("InApp{0}{1}Header", typeof(PurchaseFailureReason).Name, failureReason.ToString()))
                        , Loc.Get( "InAppPurchaseMessageFormat" // "Failed to purchase '{0}'.\n{1}"
                                    , shopItem.name
                                    , Loc.Get(string.Format("InApp{0}{1}Message", typeof(PurchaseFailureReason).Name, failureReason.ToString())))
                        , delegate { }
                        , Loc.Get("idClose"));
                }

                onIAPPurchaseFailed?.Invoke(shopItem, failureReason);
            }
        }

        bool OnIAPProcessPurchase(Product product, string storeID)
        {
            Assert.IsNotNull(product);

            var shopItem = FindShopItemByIAPProductId(product.definition.id);

            Debug.LogFormat("LogicShop: Purchase completed. IAP Product ID: {0}, Item: {1} ({2})."
                , product.definition.id
                , (shopItem != null) ? shopItem.id : -1
                , (shopItem != null) ? shopItem.name : "?");

            if (shopItem != null && shopItem.bankItemData != null)
            {
                IAPEntryData iapEntry;
                DataTables.instance.iapEntries.TryGetValue(product.definition.id, out iapEntry);

                var purchaseHistoryItem = new PurchaseHistoryItem
                    ( product.definition.id
                    , DateTime.UtcNow
                    , product.transactionID != null ? product.transactionID : string.Empty
                    , Application.version
                    , (iapEntry != null) ? iapEntry.defaultCost : 0
                    , Application.platform.ToString()
                    , storeID);

                var performAction = new Func<bool>(delegate()
                    {
                        persistentData.purchaseHistory.Add(purchaseHistoryItem);
                        return true;
                    });

                var rollbackAction = new Action(delegate()
                    {
                        persistentData.purchaseHistory.Remove(purchaseHistoryItem);
                    });

                if (gameCore.playerProfile.PurchaseBankItemTransaction(shopItem, performAction, rollbackAction))
                {
                    try
                    {
                        string itemTypeStr = string.Empty;

                        if (shopItem.bankItemData.Value.bucks != 0)
                        {
                            PersistentCore.instance.analytics.AddMoneyEvent("FitBucks", shopItem.bankItemData.Value.bucks, "InAppPurchase", product.definition.id, true);
                            //PersistentCore.instance.analytics.BusinessEvent (product.metadata.isoCurrencyCode, (int)(product.metadata.localizedPrice * 100), "FitBucks", shopItem.bankItemData.Value.iapProductId, "Shop", product.receipt);//String.Empty, String.Empty);
                            itemTypeStr = "FitBucks";
                        }

                        if (shopItem.bankItemData.Value.coins != 0)
                        {
                            PersistentCore.instance.analytics.AddMoneyEvent("Gold", shopItem.bankItemData.Value.coins, "InAppPurchase", product.definition.id, true);
                            //PersistentCore.instance.analytics.BusinessEvent (product.metadata.isoCurrencyCode, (int)(product.metadata.localizedPrice * 100), "Gold", shopItem.bankItemData.Value.iapProductId, "Shop", product.receipt);//String.Empty, String.Empty);
                            itemTypeStr = "Gold";
                        }

                        if (itemTypeStr != string.Empty)
                            PersistentCore.instance.analytics.BusinessEvent(product.metadata.isoCurrencyCode, (int)(product.metadata.localizedPrice * 100), itemTypeStr, shopItem.bankItemData.Value.iapProductId, "Shop", product.receipt, product.transactionID);

                        onIAPProcessPurchase?.Invoke(shopItem);

                        if (Advertisements.instance != null)
                        {
                            Advertisements.instance.SetupInAppsCount(persistentData.purchaseHistory.Count);
                        }


                    }
                    catch (Exception)
                    {
                    	// Any exceptions that happens in this block are not critical, just ignore them.
                    }

                    return true;
                }
            }
            return false;
        }
        #endregion

        #region IShopPurchaseProtocol interface.
        bool IShopPurchaseProtocol.InitPreview(ShopItemData item, Action<bool> OnFinishedPreview)
        {
            Assert.IsNull(pendingBankItem); // Must finish with previous.
            var iapManager = PersistentCore.instance.iapManager;
            Assert.IsNotNull(iapManager);

            TryInit();

            if (item.category == ShopItemCategory.BANK &&
                pendingBankItem == null &&
                iapManager != null &&
                iapManager.initializationState == IAPManager.InitializationState.Initialized)
            {
                pendingBankItem = item;
                return true;
            }
            return false;
        }

        bool IShopPurchaseProtocol.ConfirmPurchase()
        {
            var iapManager = PersistentCore.instance.iapManager;
            Assert.IsNotNull(iapManager);
            Assert.IsNotNull(pendingBankItem);

            TryInit();

            if (pendingBankItem != null && iapManager != null)
            {
                var tmpItem = pendingBankItem;
                pendingBankItem = null; // InitiatePurchaseRequest() can be synchronous in testing environment, so reset it soon.
                return iapManager.InitiatePurchaseRequest(tmpItem.bankItemData.Value.iapProductId);
            }

            pendingBankItem = null;
            return false;
        }

        void IShopPurchaseProtocol.BuyIAP(string iapProductId)
        {
            var iapManager = PersistentCore.instance.iapManager;
            Assert.IsNotNull(iapManager);
            //Assert.IsNotNull(pendingBankItem);

            TryInit();

            if (pendingBankItem == null &&
                iapManager != null &&
                iapManager.initializationState == IAPManager.InitializationState.Initialized)
            {
                iapManager.InitiatePurchaseRequest(iapProductId);
            } 
        }

        void IShopPurchaseProtocol.CancelPurchase()
        {
            pendingBankItem = null;
        }

        void IShopPurchaseProtocol.RestoreOwnedItem(ShopItemData item)
        {
            Debug.LogError("IMPOSIBRU : IShopPurchaseProtocol.RestoreOwnedItem you can't call this method.");

            /*Assert.IsNotNull(pendingBankItem);
            if (pendingBankItem != null)
            {
                var iapManager = PersistentCore.instance.iapManager;
                Assert.IsNotNull(iapManager);
                iapManager.ConfirmPendingPurchase (pendingBankItem.bankItemData.Value.iapProductId);

                pendingBankItem = null;
            }*/
        }
        #endregion

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersistentData);
            if (obj is PersistentData)
            {
                persistentData = (PersistentData)obj;
                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad()
        {
            ReinitPersistentData();
        }

        void IPersistent.OnLoaded(bool success)
        {
            if (!success)
            {
                ReinitPersistentData();
            }
            else
            {
                if (persistentData.promoInfo == null)
                    persistentData.promoInfo = new PromoInfo();

                if (persistentData.purchasedPaints == null)
                    persistentData.purchasedPaints = new List<int>(initialPurchasedItemsCount);

                if (persistentData.purchaseHistory == null)
                    persistentData.purchaseHistory = new List<PurchaseHistoryItem>(initialPurchaseHistoryCount);

                if (persistentData.newItems == null)
                    persistentData.newItems = new List<NewItemType>();

                if (persistentData.timersSportRes == null)
                    persistentData.timersSportRes = new Dictionary<int, TimerFloat>();
            }
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (persistentData.timersSportRes != null)
            {
                foreach (var timer in persistentData.timersSportRes)
                {
                    timer.Value.FastForward((float)timePassedSinceSave.TotalSeconds);
                }
            }

            if (persistentData.promoInfo != null && persistentData.promoInfo.promoPurchaseId >= 0 && persistentData.promoInfo.wasShow)
                persistentData.promoInfo.promoTimeLeft.FastForward((float)timePassedSinceSave.TotalSeconds);
            else
                DetectNewPromo();

        }

        void IPersistent.OnLoadSkipped()
        {
            // Give user paints with id == 0 on startup.
            persistentData.purchasedPaints.Add(0);

        }
        #endregion

        #region Public API
        public ShopItemData FindShopItemByIAPProductId(string iapProductId)
        {
            for (int i = 0; i < GetItemCount(ShopItemCategory.BANK); i++)
            {
                ShopItemData item;
                if (GetItem(ShopItemCategory.BANK, i, out item))
                {
                    if (item.bankItemData != null && item.bankItemData.Value.iapProductId == iapProductId)
                        return item;
                }
            }
            return null;
        }
        #endregion
    }
}