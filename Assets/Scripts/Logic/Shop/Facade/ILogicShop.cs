﻿using System;
using System.Collections.Generic;
using Core.Analytics;
using Data;
using UnityEngine.Purchasing;

namespace Logic.Shop.Facade
{
    public struct NewItemType
    {
        public ShopItemCategory category;
        public int id;
    }

    public delegate void IAPInitializedEvent (bool initialized, InitializationFailureReason? initializationFailureReason);
    public delegate void IAPPurchaseFailedEvent (ShopItemData item, PurchaseFailureReason failureReason);
    public delegate void IAPProcessPurchaseEvent (ShopItemData item);
    public delegate bool RewardedVideoAvaialbleEvent ();

    public interface ILogicShop
    {
        event IAPInitializedEvent onIAPInitialized;
        event IAPPurchaseFailedEvent onIAPPurchaseFailed;
        event IAPProcessPurchaseEvent onIAPProcessPurchase;
        //event RewardedVideoAvaialbleEvent onRewardedVideoAvaialble;

        IShopPurchaseProtocol wallFloorShopPurchaseProtocol { get; }
        IShopPurchaseProtocol equipmentShopPurchaseProtocol { get; }
        IShopPurchaseProtocol bankShopPurchaseProtocol { get; }

        int GetItemCount(ShopItemCategory category);
        bool GetItem(ShopItemCategory category, int idx, out ShopItemData item);
        bool IsItemLocked(ShopItemCategory category, int idx);
		int GetNeedSizeForEquip (int idx);
		int GetNeedLevelForDecor (int idx);
		int GetEquipmentsCount (int equipmentType);
        bool IsItemOwned(ShopItemData item);
//      bool SpendMoney(ItemPrice price);
        void AddCoins(int value, Analytics.MoneyInfo info);
        void AddFitBucks(int value, Analytics.MoneyInfo info);
        //bool IsItemCanShow(ShopItemCategory category, int idx);

        bool IsRewardedVideoAvailable();
        bool ShowRewardedAd(Action<bool, Cost> callback);
        void ConfirmRewardedAd(Cost reward);

        List<NewItemType> GetNewItems();

        int GetShopIndex(ShopItemCategory category, int id);
    }
}