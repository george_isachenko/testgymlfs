﻿using System;
using BridgeBinder;
using Core;
using Core.Analytics;
using Data;
using Data.Estate;
using Data.Estate.Upgrades;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using InspectorHelpers;
using Logic.Estate.Events;
using Logic.Persons;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using View;
using System.Linq;

namespace Logic.Estate.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Estate/Facade/Logic Equipment Manager Game")]
    public class LogicEquipmentManagerGame : LogicEquipmentManagerBase
    {
        #region Interfaces: properties
        public override ShopItemData itemToBuy
        {
            get
            {
                return itemToBuy_;
            }
        }

        public override bool hasSelection
        {
            get
            {
                return selectedEquipmentId_ != EquipmentData.InvalidId;
            }
        }

        public override int selectedEquipmentId
        {
            get
            {
                return selectedEquipmentId_;
            }
        }

        public override bool isInEditMode
        {
            get
            {
                return pendingEquipment != null || editingEquipmentId_ != EquipmentData.InvalidId;
            }
        }

        public override int editingEquipmentId
        {
            get
            {
                return editingEquipmentId_;
            }
        }

        public override Cost repairCost
        {
            get
            {
                return repairCostCached;
            }
        }

        public override int repairResourcesAvailable
        {
            get
            {
                return gameCore.playerProfile.storage.GetResourcesCount(repairCostCached.resourceType);
            }
        }

        public override int maxSameEquipmentCount
        {
            get
            {
                return maxSameCount;
            }
        }

        public override EquipmentUseHPCosts[] useHPCosts
        {
            get
            {
                return equipmentUseHPCosts;
            }
        }

        public override int initialHP
        {
            get
            {
                return equipmentInitialHP;
            }
        }

        public override int minHP
        {
            get
            {
                return equipmentMinHP;
            }
        }

        public override int maxHP
        {
            get
            {
                return equipmentMaxHP;
            }
        }
        #endregion

        #region Public data.
        [Header("Equipment limits")]
        public int maxSameCount = 4;

        [Header("Equipment repair")]
        public int equipmentInitialHP = EquipmentData.equipmentDefaultInitialHP;
        public int equipmentMinHP = EquipmentData.equipmentDefaultMinHP;
        public int equipmentMaxHP = EquipmentData.equipmentDefaultMaxHP;
        [ReorderableList]
        public EquipmentUseHPCosts[] equipmentUseHPCosts;
        [Tooltip("Equipment repair time (in seconds).")]
        public float equipmentRepairTime = 30.0f;
        public ResourceType equipmentRepairResource = ResourceType.Equip_01;
        public int equipmentRepairCost = 1;
        #endregion

        #region Private data.
        protected ShopItemData itemToBuy_;
        protected int selectedEquipmentId_ = EquipmentData.InvalidId;   // Not selected.
        protected int editingEquipmentId_ = EquipmentData.InvalidId;
        protected Equipment pendingEquipment = null;
        protected Action<bool> equipmentPreviewCallback = null;
        protected bool equipmentSelectionEnabled = true;
        protected Cost repairCostCached;

        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("onEquipmentPlacementUpdated")]
        protected void OnEquipmentPlacementUpdated (int id, EquipmentPlacement placement)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                if (equipment.placement != placement)
                {
                    equipment.SetPlacement(placement);
                    
                    // gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                }
            }
            else
            {
                Debug.LogErrorFormat("Cannot find equipment with id {0} to set placement.", id);
            }
        }
        #endregion

        #region Unity API
        protected new void Awake()
        {
            base.Awake();

            repairCostCached = new Cost(equipmentRepairResource, equipmentRepairCost);

            Persons.Events.SetSelectionEvent.Event += OnPersonSetSelectionEvent;

            Rooms.Events.RoomDeactivatedEvent.Event += OnRoomDeactivatedEvent;
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            Persons.Events.SetSelectionEvent.Event -= OnPersonSetSelectionEvent;

            Rooms.Events.RoomDeactivatedEvent.Event -= OnRoomDeactivatedEvent;
        }

#if DEBUG
        protected /*new*/ void Update()
        {
            // Debug Auto-play stuff.
            if (gameCore.isGameStarted && DebugHacks.visitorsAutoPlay && gameCore.quests.tutorial != null && !gameCore.quests.tutorial.isRunning)
            {
                var equipmentCandidates = equipments.Values.Where
                    ( e => (e.state == EquipmentState.Assembling || e.state == EquipmentState.AssemblyComplete || e.state ==  EquipmentState.Broken || e.state == EquipmentState.Repairing || e.state == EquipmentState.ShopMode));
                
                foreach (var candidate in equipmentCandidates)
                {
                    if (candidate.state == EquipmentState.Assembling)
                    {
                        candidate.SkipAssembly();
                    }
                    else if (candidate.state == EquipmentState.AssemblyComplete)
                    {
                        candidate.ConfirmAssemblyComplete();
                    }
                    else if (candidate.state == EquipmentState.Broken)
                    {
                        candidate.RepairEquipmentRequest(new Cost(), repairResourcesAvailable, equipmentRepairTime);
                    }
                    else if (candidate.state == EquipmentState.Repairing)
                    {
                        candidate.SkipRepairEquipmentRequest();
                    }
                    else if (candidate.state == EquipmentState.ShopMode)
                    {

                    }
                }

                if (itemToBuy_ != null && pendingEquipment != null && equipmentPreviewCallback !=  null)
                {
                    DelayedInvoke(() =>
                    {
                        if (itemToBuy_ != null && pendingEquipment != null && equipmentPreviewCallback !=  null)
                        {
                            equipmentPreviewCallback.Invoke(true);
                        }
                    });
                }
            }

            // base.Update();
        }
#endif
        #endregion

        #region Public API
        public override bool SelectEquipmentRequest(int id)
        {
            if (!equipmentSelectionEnabled)
                return false;

            if (pendingEquipment != null)
            {
                return false; // Cannot change selection if we have "Pending equipment".
            }
            else if (editingEquipmentId_ != EquipmentData.InvalidId && id != EquipmentData.InvalidId)
            {
                EditEquipmentRequest(id);
                return true;
            }

            if (id != selectedEquipmentId_)
            {
                int oldId = selectedEquipmentId_;

                Equipment oldEquipment = null;
                Equipment newEquipment = null;

                if (oldId != EquipmentData.InvalidId)
                    equipments.TryGetValue(oldId, out oldEquipment);

                if (id == EquipmentData.InvalidId)
                {
                    selectedEquipmentId_ = EquipmentData.InvalidId;
                }
                else
                {
                    if (pendingEquipment == null && equipments.TryGetValue(id, out newEquipment) && (newEquipment.isSelectable))
                    {
                        selectedEquipmentId_ = id;
                    }
                    else
                    {
                        return false;
                    }
                }

                if (oldEquipment != null)
                {
                    EquipmentSelectionEvent.Send(oldEquipment, false);
                }

                if (newEquipment != null)
                {
                    EquipmentSelectionEvent.Send(newEquipment, true);
                    PersistentCore.instance.analytics.DetailGameEvent("Equipment Select", "Equipment Name", newEquipment.estateType.locNameId);
                    Sound.instance.EquipmentSelect();
                }
            }
            return true;
        }

        public override bool IsEditableEquipment(int id)
        {
            Assert.IsTrue(id != EquipmentData.InvalidId);
            if (id != EquipmentData.InvalidId)
            {
                Equipment equipment;
                return (equipments.TryGetValue(id, out equipment) && equipment.isEditable);
            }
            return false;
        }

        public override bool EditEquipmentRequest(int id)
        {
            if (pendingEquipment != null)
                return false; // Cannot change edit mode if we have "Pending equipment".

            if (!canEditEquip)
                return false; // can't edit if we in can edit state

            if (id != EquipmentData.InvalidId)
            {
                if (id != editingEquipmentId_)
                {
                    Equipment equipment;
                    if (equipments.TryGetValue(id, out equipment) && equipment.isEditable)
                    {
                        if (selectedEquipmentId_ != EquipmentData.InvalidId && selectedEquipmentId_ != id)
                            SelectEquipmentRequest(EquipmentData.InvalidId);
                        gameCore.personManager.SelectCharacterRequest(PersonData.InvalidId);
                        editingEquipmentId_ = id;
                        EquipmentEditModeEvent.Send(equipment, equipment.canSellOrStore);
                        return true;
                    }
                }
            }
            else if (editingEquipmentId_ != EquipmentData.InvalidId)
            {
                editingEquipmentId_ = EquipmentData.InvalidId;
                EquipmentEditModeEvent.Send(null, false);
                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                return true;
            }

            return false;
        }

        public override void SkipAssemblyRequest(int id)
        {
            Equipment equipment;
            if (id != editingEquipmentId_ && equipments.TryGetValue(id, out equipment))
            {
                equipment.SkipAssembly();
                ShowPopularityHint(id);
            }
        }

        public override void ConfirmAssemblyCompleteRequest(int id)
        {
            Equipment equipment;
            if (id != editingEquipmentId_ && equipments.TryGetValue(id, out equipment))
            {
                equipment.ConfirmAssemblyComplete();
                ShowPopularityHint(id);
            }
        }

        public override bool UseEquipmentRequest(int id, int characterId, int exerciseTierIdx)
        {
            return UseEquipmentRequest(id, characterId, exerciseTierIdx, ExerciseInitiator.Player);
        }

        public override bool UseEquipmentRequest(int id, int characterId, int exerciseTierIdx, ExerciseInitiator initiator)
        {
            if (editingEquipmentId_ != EquipmentData.InvalidId)
                return false;

            Equipment equipment;
            Person person;
            if (id != EquipmentData.InvalidId &&
                characterId != PersonData.InvalidId &&
                exerciseTierIdx >= 0 &&
                equipments.TryGetValue(id, out equipment) &&
                gameCore.personManager.persons.TryGetValue(characterId, out person) &&
                person.isPlayerControllable &&
                person.canBeTrainedByPlayer)
            {
                if (equipment.state == EquipmentState.Idle && !equipment.isOccupied)
                {
                    EstateType estateType;
                    if (DataTables.instance.estateTypes.TryGetValue(equipment.estateType.id, out estateType))
                    {
                        if (exerciseTierIdx < estateType.exercisesIds.Length)
                        {
                            if (gameCore.personManager.UseEquipmentRequest(person, equipment, estateType.exercises[exerciseTierIdx], initiator))
                            {
                                SelectEquipmentRequest(EquipmentData.InvalidId);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public override void SellEditedEquipment()
        {
            if (editingEquipmentId_ == EquipmentData.InvalidId)
                return;

            Equipment equipment;
            if (equipments.TryGetValue(editingEquipmentId_, out equipment))
            {
                var price = GetSellPrice (editingEquipmentId_);
                if (price != null && price.type != Cost.CostType.None)
                {
                    if (price.value == 0)
                    {
                        DestroyEquipment(editingEquipmentId_);
                    }
                    else
                    {
                        if (equipment.estateType.itemType == EstateType.Subtype.Training)
                            gameCore.playerProfile.AddReward(price, new Analytics.MoneyInfo("Sell Equipment", equipment.estateType.locNameId), false);
                        else
                            gameCore.playerProfile.AddReward(price, new Analytics.MoneyInfo(string.Format("Sell {0}", equipment.estateType.itemType), equipment.estateType.locNameId), false);

                        DestroyEquipment(editingEquipmentId_);
                    }

                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                }
            }
        }

        public override void UnlockEquipmentRequest(int id)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                if (equipment.UnlockRequest())
                {
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                }
            }
        }

        public override Cost GetSellPrice(int equipmentId)
        {
            Cost price;

            Equipment equipment;
            if (equipments.TryGetValue(editingEquipmentId, out equipment))
            {
                if (equipment.estateType.price != null)
                {
                    price = new Cost(equipment.estateType.price);

                    price.value = Math.Max(1, Mathf.FloorToInt(DataTables.instance.balanceData.Equipment.sellMultiplier * price.value));

                    for (int i = 1; i <= equipment.upgradeLevel; i++)
                    {
                        EquipmentUpgradeInfo upgradeInfo;
                        DataTables.instance.equipmentUpgrades.TryGetValue
                            (new EquipmentUpgradeInfo.Key(equipment.estateType.id, i), out upgradeInfo);
                        if (upgradeInfo == null)
                            break;

                        price.value += Mathf.FloorToInt(upgradeInfo.costAmount * DataTables.instance.balanceData.Equipment.sellUpgradeMultiplier);
                    }

                    return price;
                }
            }

            Debug.LogError("Can't find price for equipment id " + equipmentId.ToString());
            return null;
        }

        public override bool RepairEquipmentRequest(int id)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                Assert.IsNotNull(repairCostCached);
                if (equipment.RepairEquipmentRequest(repairCostCached, repairResourcesAvailable, equipmentRepairTime))
                {
                    SelectEquipmentRequest(id);
                    return true;
                }
            }
            return false;
        }

        public override void SkipRepairEquipmentRequest(int id)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                equipment.SkipRepairEquipmentRequest();
            }
        }

        public override void SetEquipmentSelectionMode(bool allowSelection)
        {
            if (!allowSelection && selectedEquipmentId_ != PersonData.InvalidId)
                SelectEquipmentRequest(EquipmentData.InvalidId);
            equipmentSelectionEnabled = allowSelection;
        }

        public override bool UpgradeEquipment(int id, bool canSpendFitBucks)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                EquipmentUpgradeInfo upgradeInfo;

                if (!DataTables.instance.equipmentUpgrades.TryGetValue(new EquipmentUpgradeInfo.Key(equipment.estateType.id, equipment.upgradeLevel + 1), out upgradeInfo))
                {
                    Debug.LogWarningFormat("Equipment {0} (Estate type: {1}) is already at max upgrade level ({2})."
                        , id, equipment.estateType.id, equipment.upgradeLevel);
                    return false;   // Already at max upgrade level.
                }

                if (playerProfile.level < upgradeInfo.unlockLevel)
                {
                    Debug.LogWarningFormat("Cannot upgrade equipment {0} (Estate type: {1}) to level {2}, your profile level ({3}) is too low."
                        , id, equipment.estateType.id, equipment.upgradeLevel + 1, playerProfile.level);
                    return false;
                }

                if (!playerProfile.SpendMoney(upgradeInfo.cost, new Analytics.MoneyInfo("UpgradeEquipment", equipment.estateType.locNameId), canSpendFitBucks))
                {
                    return false;
                }

                equipment.Upgrade();

                return true;
            }
            return false;
        }
        #endregion

        #region Person events handlers
        private void OnPersonSetSelectionEvent(Person person, bool selected, bool focus)
        {
            if (person.workoutEquipmentId != EquipmentData.InvalidId)
            {
                if (selected && person.workoutEquipmentId != selectedEquipmentId_)
                {
                    SelectEquipmentRequest(person.workoutEquipmentId);
                }
                else if (!selected && person.workoutEquipmentId == selectedEquipmentId_)
                {
                    SelectEquipmentRequest(EquipmentData.InvalidId);
                }
            }
            else /* if (selectedEquipmentId_ != EquipmentData.InvalidEquipmentId)*/
            {
                SelectEquipmentRequest(EquipmentData.InvalidId);
                //                 Equipment equipment;
                //                 if (equipments.TryGetValue(selectedEquipmentId, out equipment))
                //                 {
                //                     gameCore.eventsDispatcher.SendUpdateOverheadUIEvent(selectedEquipmentId, CreateEquipmentOverheadUIData(equipment, true));
                //                 }
            }
        }
        #endregion

        #region IShopPurchaseProtocol API
        public override bool InitPreview(ShopItemData item, Action<bool> onFinishedPreview)
        {
            // Assert.IsNull(itemToBuy_);
            if (itemToBuy_ != null)
            {
                equipmentPreviewCallback = null;
                if (pendingEquipment != null)
                    CancelEquipmentPreview();
                itemToBuy_ = null;
            }

            if (item.category == ShopItemCategory.EQUIPMENTS ||
                item.category == ShopItemCategory.DECORS)
            {
                /*
                if(item.category == Data.ShopItemCategory.EQUIPMENTS)
                {
                    if (!IsCanBuyThis (item.estateTypeData.id)) 
                    {
                        int gymSize = gameCore.roomManager.GetRoomSize (gameCore.roomManager.activeRoomName).Square();
                        int SquareNeed = SquareNeedForThis (item.estateTypeData.id);

                        Debug.Log ("Ты не можеш купить это. Площадь = " + gymSize.ToString() + " Площадь нужна = " + SquareNeed);
                        return false;
                    }
                }            
                */

                if (RequestEquipmentPreview(item.id, onFinishedPreview))
                {
                    itemToBuy_ = item;
                    return true;
                }
            }
            else if (item.category == ShopItemCategory.SPORT_EQUIPMENTS)
            {
                var staticEquipmentId = FindAvailableLockedStaticEquipment(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub), item.id);
                if (staticEquipmentId != null)
                {
                    var equipmentIdToUnlock = FindStaticEquipmentInRoom(staticEquipmentId.Value);
                    if (equipmentIdToUnlock != EquipmentData.InvalidId)
                    {
                        if (RequestEquipmentUnlockPreview(equipmentIdToUnlock, onFinishedPreview))
                        {
                            itemToBuy_ = item;
                            return true;
                        }
                    }
                    else
                    {
                        Debug.LogError("Cannot find static equipment.");
                        return false;
                    }
                }
                else
                {
                    Debug.LogError("Cannot find available locked static equipment.");
                    return false;
                }
            }
            return false;
        }

        public override bool ConfirmPurchase()
        {
            Assert.IsNotNull(itemToBuy_);
            if (itemToBuy_ != null)
            {
                if (itemToBuy_.category == ShopItemCategory.EQUIPMENTS ||
                    itemToBuy_.category == ShopItemCategory.DECORS)
                {
                    if (DebugHacks.visitorsAutoPlay ||
                        gameCore.playerProfile.SpendMoney
                            ( itemToBuy_.price
                            , new Analytics.MoneyInfo(itemToBuy_.GetCategoryString(), itemToBuy_.estateTypeData.locNameId)))
                    {
                        presentationEquipmentManager?.EquipmentEndPreviewModeRequest();

                        EquipmentPreviewModeChangedEvent.Send(null);

                        PersistentCore.instance.analytics.GameEvent(itemToBuy_.GetCategoryString() + " Purchase",
                            new GameEventParam("Equipment Name", itemToBuy_.estateTypeData.locNameId),
                            new GameEventParam("Level", gameCore.playerProfile.level),
                            new GameEventParam("Already bought count", GetEquipmentsCount(itemToBuy_.estateTypeData.id)));

                        CreateEquipment(pendingEquipment.estateType.id, pendingEquipment.placement);

                        Achievements.EquipDecorPurchase.Send(pendingEquipment.estateType);

                        pendingEquipment = null;
                        itemToBuy_ = null;

                        return true;
                    }
                }
                else if (itemToBuy_.category == ShopItemCategory.SPORT_EQUIPMENTS)
                {
                    if (DebugHacks.visitorsAutoPlay ||
                        gameCore.playerProfile.SpendMoney
                            ( itemToBuy_.price
                            , new Analytics.MoneyInfo(itemToBuy_.GetCategoryString(), itemToBuy_.estateTypeData.locNameId)))
                    {
                        presentationEquipmentManager?.EquipmentEndPreviewModeRequest();

                        EquipmentPreviewModeChangedEvent.Send(null);

                        UnlockEquipmentRequest(pendingEquipment.id);

                        pendingEquipment = null;
                        itemToBuy_ = null;

                        return true;
                    }
                }
            }
            return false;
        }

        public override void CancelPurchase()
        {
            if (itemToBuy_ != null)
            {
                equipmentPreviewCallback = null;
                if (pendingEquipment != null)
                    CancelEquipmentPreview();
                itemToBuy_ = null;
            }
        }

        public override void RestoreOwnedItem(ShopItemData item)
        {
        }
        #endregion

        #region Protected virtual API.
        protected override void Reinit()
        {
            base.Reinit();

            selectedEquipmentId_ = editingEquipmentId_ = EquipmentData.InvalidId;
        }
        #endregion

        #region Private API
        void DestroyEquipment(int id)
        {
            Equipment equipment;
            if (equipments.TryGetValue(editingEquipmentId, out equipment))
            {
                equipment.Destroy();

                if (editingEquipmentId != EquipmentData.InvalidId)
                    EditEquipmentRequest(EquipmentData.InvalidId);

                equipments.Remove(id);

                persistentData.equipmentData.RemoveAll(eq => eq.id == id);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                SelectEquipmentRequest(EquipmentData.InvalidId);
                EquipmentDeathEvent.Send(id);
            }
        }

        private bool RequestEquipmentPreview(int equipmentType, Action<bool> OnFinishedPreview)
        {
            if (pendingEquipment != null)   // There's already pending equipment - just finish with it, bitch!
            {
                Debug.LogError("There's already pending equipment!");
                return false;
            }

            if (editingEquipmentId != EquipmentData.InvalidId)   // There's already editing equipment - just finish with it, bitch!
            {
                Debug.LogError("Cannot preview equipment while editing other equipment!");
                return false;
            }

            EstateType estateType;
            if (!DataTables.instance.estateTypes.TryGetValue(equipmentType, out estateType))
            {
                Debug.LogErrorFormat("Unknown Estate type {0}.", equipmentType);
                return false;
            }

            var equipmentData = new EquipmentData(EquipmentData.InvalidId, estateType, gameCore.roomManager.GetRoomNameOfType(RoomType.Club), equipmentInitialHP);
            equipmentData.state = EquipmentState.ShopMode;
            var equipment = new Equipment(this, equipmentData);

            equipment.SetPlacement(new EquipmentPlacement(new RoomCoordinates(-1, -1), 0));

            SelectEquipmentRequest(EquipmentData.InvalidId);
            gameCore.personManager.SelectCharacterRequest(PersonData.InvalidId);

            if (presentationEquipmentManager.EquipmentStartPreviewModeRequest(equipment, OnEquipmentPreviewFinished))
            {
                pendingEquipment = equipment;
                equipmentPreviewCallback = OnFinishedPreview;

                EquipmentPreviewModeChangedEvent.Send(equipment);
                return true;
            }

            return false;
        }

        private bool RequestEquipmentUnlockPreview(int equipmentId, Action<bool> OnFinishedPreview)
        {
            if (pendingEquipment != null)   // There's already pending equipment - just finish with it, bitch!
            {
                Debug.LogError("There's already pending equipment!");
                return false;
            }

            if (editingEquipmentId != EquipmentData.InvalidId)   // There's already pending equipment - just finish with it, bitch!
            {
                Debug.LogError("Cannot preview equipment while editing other equipment!");
                return false;
            }

            Equipment equipment;
            if (equipments.TryGetValue(equipmentId, out equipment))
            {
                SelectEquipmentRequest(EquipmentData.InvalidId);
                gameCore.personManager.SelectCharacterRequest(PersonData.InvalidId);

                if (presentationEquipmentManager.EquipmentStartPreviewModeRequest(equipment, OnEquipmentPreviewFinished))
                {
                    pendingEquipment = equipment;
                    equipmentPreviewCallback = OnFinishedPreview;

                    EquipmentPreviewModeChangedEvent.Send(equipment);
                    return true;
                }
            }
            return false;
        }

        private void CancelEquipmentPreview()
        {
            Assert.IsNotNull(pendingEquipment);
            if (pendingEquipment != null)
            {
                pendingEquipment = null;

                presentationEquipmentManager?.EquipmentEndPreviewModeRequest();

                EquipmentPreviewModeChangedEvent.Send(null);

                if (equipmentPreviewCallback != null)
                {
                    var tmpCallback = equipmentPreviewCallback;
                    equipmentPreviewCallback = null;
                    tmpCallback.Invoke(false);
                }
            }
        }

        private void OnEquipmentPreviewFinished(EquipmentPlacement? placement)
        {
            Assert.IsNotNull(pendingEquipment);

            if (equipmentPreviewCallback != null)
            {
                var tmpCallback = equipmentPreviewCallback;
                equipmentPreviewCallback = null;
                if (placement.HasValue)
                {
                    pendingEquipment.SetPlacement(placement.Value);
                }
                tmpCallback(placement.HasValue);
            }
        }

        void ShowPopularityHint(int id)
        {
            Equipment equipment;
            if (equipments.TryGetValue(id, out equipment))
            {
                if (equipment.estateType.style > 0)
                {
                    presentationEquipmentManager.ShowStyleHintOverheadUIIcon(id, "+" + equipment.estateType.style.ToString());
                }
            }
        }

        private void OnRoomDeactivatedEvent(string roomName)
        {
            if (pendingEquipment != null)
            {
                CancelEquipmentPreview();
            }

            EditEquipmentRequest(EquipmentData.InvalidId);

            SelectEquipmentRequest(EquipmentData.InvalidId);
        }
        #endregion

        #region Unity Editor API
#if UNITY_EDITOR
        private void OnValidate()
        {
            if (equipmentMinHP > equipmentMaxHP)
                equipmentMinHP = equipmentMaxHP;
        }
#endif
        #endregion
    }
}