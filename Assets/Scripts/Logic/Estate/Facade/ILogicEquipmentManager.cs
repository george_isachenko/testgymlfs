﻿using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Estate.Static;
using Data.Estate.Upgrades;

namespace Logic.Estate.Facade
{
    public delegate void SetSelectionEvent(int id, bool selected);
    public delegate void SetEditModeEvent(int id, bool canSellOrStore);
    public delegate void EquipmentStateChangedEvent(int id, EquipmentState oldState, EquipmentState newState);
    public delegate void EquipmentAssemblyStartEvent(int id);
    public delegate void EquipmentAssemblyCompleteEvent(int id, bool skipped);
    public delegate void EquipmentAssemblyCompleteConfirmedEvent(int id);
    public delegate void EquipmentStartPendingExerciseEvent(int id);
    public delegate void EquipmentSwitchPendingExerciseEvent(int id);
    public delegate void EquipmentCancelPendingExerciseEvent(int id);
    public delegate void EquipmentRepairRequiredEvent(int id);
    public delegate void EquipmentStartRepairEvent(int id);
    public delegate void EquipmentEndRepairEvent(int id);
    public delegate void EquipmentStartUnlockingEvent(int id);
    public delegate void EquipmentUpgradedEvent(int id, int upgradeLevel);

    public interface ILogicEquipmentManager
    {
        bool hasSelection
        {
            get;
        }

        int selectedEquipmentId
        {
            get;
        }

        bool isInEditMode
        {
            get;
        }

        int editingEquipmentId
        {
            get;
        }

        Cost repairCost
        {
            get;
        }

        int repairResourcesAvailable
        {
            get;
        }

        bool canEditEquip
        {
            get;
            set;
        }

        // Events.
        event SetSelectionEvent SetSelectionEvent;
        event SetEditModeEvent SetEditModeEvent;
        event EquipmentStateChangedEvent EquipmentStateChangedEvent;
        event EquipmentAssemblyStartEvent EquipmentAssemblyStartEvent;
        event EquipmentAssemblyCompleteEvent EquipmentAssemblyCompleteEvent;
        event EquipmentAssemblyCompleteConfirmedEvent EquipmentAssemblyCompleteConfirmedEvent;
        event EquipmentStartPendingExerciseEvent EquipmentStartPendingExerciseEvent;
        event EquipmentSwitchPendingExerciseEvent EquipmentSwitchPendingExerciseEvent;
        event EquipmentCancelPendingExerciseEvent EquipmentCancelPendingExerciseEvent;
        event EquipmentRepairRequiredEvent EquipmentRepairRequiredEvent;
        event EquipmentStartRepairEvent EquipmentStartRepairEvent;
        event EquipmentEndRepairEvent EquipmentEndRepairEvent;
        event EquipmentStartUnlockingEvent EquipmentStartUnlockingEvent;
        event EquipmentUpgradedEvent EquipmentUpgradedEvent;

        int GetEquipmentsCount(int estateTypeID = -1, EstateType.Subtype? equipmentType = null, string roomName = null, StaticEquipmentID? staticEquipmentID = null, params EquipmentState[] states);
        Equipment FindEquipment(int estateTypeID = -1, EstateType.Subtype? equipmentType = null, string roomName = null, StaticEquipmentID? staticEquipmentID = null, params EquipmentState[] states);

        bool SelectEquipmentRequest(int id);
        bool UseEquipmentRequest(int id, int characterId, int exerciseIdx);
        bool IsEditableEquipment(int id);
        bool EditEquipmentRequest(int id);
        void SkipAssemblyRequest(int id);
        void ConfirmAssemblyCompleteRequest(int id);
        bool RepairEquipmentRequest(int id);
        void SkipRepairEquipmentRequest(int id);
        void UnlockEquipmentRequest(int id);
        void SellEditedEquipment();
        Cost GetSellPrice(int equipmentId);
        void SetEquipmentSelectionMode(bool allowSelection);
        int FindStaticEquipmentInRoom(StaticEquipmentID staticEquipmentID);

        bool UpgradeEquipment(int id, bool canSpendFitBucks);
        UpgradeAvailabilityState GetEquipmentUpgradeInfo (int id, out EquipmentUpgradeInfo currentUpgradeInfo, out EquipmentUpgradeInfo nextUpgradeInfo);
    }
}