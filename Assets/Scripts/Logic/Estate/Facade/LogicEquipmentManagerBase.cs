﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Collections;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Estate.Static;
using Data.Estate.Upgrades;
using Data.Person;
using Data.Room;
using Logic.Core;
using Logic.Estate.Events;
using Logic.FacadesBase;
using Logic.Persons;
using Logic.Serialization;
using Logic.Shop;
using Presentation.Facades.Interfaces.EquipmentManager;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Estate.Facade
{
    [BindBridgeInterface(typeof(IPresentationEquipmentManager))]
    public abstract class LogicEquipmentManagerBase
        : BaseLogicFacade
        , ILogicEquipmentManager
        , IEquipmentManager
        , IShopPurchaseProtocol
        , IPersistent
    {
        #region static data
        protected static readonly int equipmentsInitialCapacity = 128;
        protected static readonly int equipmentsInAssemblyOrRepairInitialCapacity = 16;
        #endregion

        #region Interfaces: events
        public event SetSelectionEvent SetSelectionEvent;
        public event SetEditModeEvent SetEditModeEvent;
        public event EquipmentStateChangedEvent EquipmentStateChangedEvent;
        public event EquipmentAssemblyStartEvent EquipmentAssemblyStartEvent;
        public event EquipmentAssemblyCompleteEvent EquipmentAssemblyCompleteEvent;
        public event EquipmentAssemblyCompleteConfirmedEvent EquipmentAssemblyCompleteConfirmedEvent;
        public event EquipmentStartPendingExerciseEvent EquipmentStartPendingExerciseEvent;
        public event EquipmentSwitchPendingExerciseEvent EquipmentSwitchPendingExerciseEvent;
        public event EquipmentCancelPendingExerciseEvent EquipmentCancelPendingExerciseEvent;
        public event EquipmentRepairRequiredEvent EquipmentRepairRequiredEvent;
        public event EquipmentStartRepairEvent EquipmentStartRepairEvent;
        public event EquipmentEndRepairEvent EquipmentEndRepairEvent;
        public event EquipmentStartUnlockingEvent EquipmentStartUnlockingEvent;
        public event EquipmentUpgradedEvent EquipmentUpgradedEvent;
        #endregion

        #region Interfaces: properties
        public virtual ShopItemData itemToBuy
        {
            get
            {
                return null;
            }
        }

        IGameCore IEquipmentManager.gameCore
        {
            get
            {
                return gameCore;
            }
        }

        IShopPurchaseProtocol IEquipmentManager.purchaseProtocol
        {
            get
            {
                return this;
            }
        }

        public virtual bool hasSelection
        {
            get
            {
                return false;
            }
        }

        public virtual int selectedEquipmentId
        {
            get
            {
                return EquipmentData.InvalidId;
            }
        }

        public virtual bool isInEditMode
        {
            get
            {
                return false;
            }
        }

        public virtual int editingEquipmentId
        {
            get
            {
                return EquipmentData.InvalidId;
            }
        }

        public virtual Cost repairCost
        {
            get
            {
                return null;
            }
        }

        public virtual int repairResourcesAvailable
        {
            get
            {
                return 0;
            }
        }

        public virtual int maxSameEquipmentCount
        {
            get
            {
                return int.MaxValue;
            }
        }

        IDictionary<int, Equipment> IEquipmentManager.equipment
        {
            get
            {
                return equipments;
            }
        }

        public virtual EquipmentUseHPCosts[] useHPCosts
        {
            get
            {
                return null;
            }
        }

        public virtual int initialHP
        {
            get
            {
                return EquipmentData.equipmentDefaultInitialHP;
            }
        }

        public virtual int minHP
        {
            get
            {
                return EquipmentData.equipmentDefaultMinHP;
            }
        }

        public virtual int maxHP
        {
            get
            {
                return EquipmentData.equipmentDefaultMaxHP;
            }
        }

        public virtual bool canEditEquip
        {
            get;
            set;
        }
        #endregion

        #region IPersistent properties.
        string IPersistent.propertyName
        {
            get
            {
                return "Equipment";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(EquipmentPersistentData);
            }
        }
        #endregion

        #region Public (Inspector) data.
        #endregion

        #region Private data.
        protected IPresentationEquipmentManager presentationEquipmentManager;
        protected AutoKeyIntDictionary<Equipment> equipments;
        protected int minimumUpgradeAvailabilityLevel = 0;
        #endregion

        #region Persistent data.
        protected EquipmentPersistentData persistentData;
        #endregion

        #region Bridge
        [BindBridgeSubscribe]
        protected void Subscribe(IPresentationEquipmentManager presentationEquipmentManager)
        {
            this.presentationEquipmentManager = presentationEquipmentManager;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(IPresentationEquipmentManager presentationEquipmentManager)
        {
            this.presentationEquipmentManager = null;
        }
        #endregion

        #region Bridge event handlers.
        #endregion

        #region Unity API
        protected void Awake()
        {
            persistentData.equipmentData = new List<EquipmentData>(equipmentsInitialCapacity);
            equipments = new AutoKeyIntDictionary<Equipment>(equipmentsInitialCapacity);
            canEditEquip = true;

            Events.EquipmentBirthEvent.Event += OnEquipmentBirthEvent;
            Events.EquipmentDeathEvent.Event += OnEquipmentDeathEvent;
            Events.EquipmentSelectionEvent.Event += OnEquipmentSelectionEvent;
            Events.EquipmentStateChangedEvent.Event += OnEquipmentStateChangedEvent;
            Events.EquipmentStartPendingExerciseEvent.Event += OnEquipmentStartPendingExerciseEvent;
            Events.EquipmentSwitchPendingExerciseEvent.Event += OnEquipmentSwitchPendingExerciseEvent;
            Events.EquipmentCancelPendingExerciseEvent.Event += OnEquipmentCancelPendingExerciseEvent;
            Events.EquipmentStartExerciseEvent.Event += OnEquipmentStartExerciseEvent;
            Events.EquipmentSwitchExerciseEvent.Event += OnEquipmentSwitchExerciseEvent;
            Events.EquipmentEndExerciseEvent.Event += OnEquipmentEndExerciseEvent;
            Events.EquipmentEditModeEvent.Event += OnEquipmentEditModeEvent;
            Events.EquipmentAssemblyStartEvent.Event += OnEquipmentAssemblyStartEvent;
            Events.EquipmentAssemblyCompleteEvent.Event += OnEquipmentAssemblyCompleteEvent;
            Events.EquipmentAssemblyCompleteConfirmedEvent.Event += OnEquipmentAssemblyCompleteConfirmedEvent;
            Events.EquipmentRepairRequiredEvent.Event += OnEquipmentRepairRequiredEvent;
            Events.EquipmentStartRepairEvent.Event += OnEquipmentStartRepairEvent;
            Events.EquipmentEndRepairEvent.Event += OnEquipmentEndRepairEvent;
            Events.EquipmentStartUnlockingEvent.Event += OnEquipmentUnlockedEvent;
            Events.EquipmentUpgradedEvent.Event += OnEquipmentUpgradedEvent;
        }

        protected void OnDestroy()
        {
            Events.EquipmentBirthEvent.Event -= OnEquipmentBirthEvent;
            Events.EquipmentDeathEvent.Event -= OnEquipmentDeathEvent;
            Events.EquipmentSelectionEvent.Event -= OnEquipmentSelectionEvent;
            Events.EquipmentStateChangedEvent.Event -= OnEquipmentStateChangedEvent;
            Events.EquipmentStartPendingExerciseEvent.Event -= OnEquipmentStartPendingExerciseEvent;
            Events.EquipmentSwitchPendingExerciseEvent.Event -= OnEquipmentSwitchPendingExerciseEvent;
            Events.EquipmentCancelPendingExerciseEvent.Event -= OnEquipmentCancelPendingExerciseEvent;
            Events.EquipmentStartExerciseEvent.Event -= OnEquipmentStartExerciseEvent;
            Events.EquipmentSwitchExerciseEvent.Event -= OnEquipmentSwitchExerciseEvent;
            Events.EquipmentEndExerciseEvent.Event -= OnEquipmentEndExerciseEvent;
            Events.EquipmentEditModeEvent.Event -= OnEquipmentEditModeEvent;
            Events.EquipmentAssemblyStartEvent.Event -= OnEquipmentAssemblyStartEvent;
            Events.EquipmentAssemblyCompleteEvent.Event -= OnEquipmentAssemblyCompleteEvent;
            Events.EquipmentAssemblyCompleteConfirmedEvent.Event -= OnEquipmentAssemblyCompleteConfirmedEvent;
            Events.EquipmentRepairRequiredEvent.Event -= OnEquipmentRepairRequiredEvent;
            Events.EquipmentStartRepairEvent.Event -= OnEquipmentStartRepairEvent;
            Events.EquipmentEndRepairEvent.Event -= OnEquipmentEndRepairEvent;
            Events.EquipmentStartUnlockingEvent.Event -= OnEquipmentUnlockedEvent;
            Events.EquipmentUpgradedEvent.Event -= OnEquipmentUpgradedEvent;
        }

        protected void Start ()
        {
            minimumUpgradeAvailabilityLevel = DataTables.instance.equipmentUpgrades.Min(x => (x.Value.unlockLevel > 0) ? x.Value.unlockLevel : int.MaxValue);
        }

        protected void OnApplicationPause (bool pauseStatus)
        {
            /*
            if (!pauseStatus && !Application.isEditor)
            {
                Reset();
            }
            */
        }
        #endregion

        #region Public API
        public int CreateEquipment(int equipmentType, EquipmentPlacement placement)
        {
            EstateType estateType;
            if (!DataTables.instance.estateTypes.TryGetValue(equipmentType, out estateType))
            {
                Debug.LogErrorFormat("Unknown Estate type {0}.", equipmentType);
                return EquipmentData.InvalidId;
            }

            int id = equipments.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Equipment>.invalidId);
            var equipmentData = new EquipmentData(id, estateType, gameCore.roomManager.GetRoomNameOfType(RoomType.Club), initialHP);
            persistentData.equipmentData.Add(equipmentData);

            var equipment = new Equipment(this, equipmentData);

            equipment.SetPlacement(placement);

            equipments.Add(id, equipment);

            equipment.OnPostCreate();

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return id;
        }

        public virtual bool SelectEquipmentRequest(int id)
        {
            return false;
        }

        public virtual bool IsEditableEquipment(int id)
        {
            return false;
        }

        public virtual bool EditEquipmentRequest(int id)
        {
            return false;
        }

        public virtual void SkipAssemblyRequest(int id)
        {
        }

        public virtual void ConfirmAssemblyCompleteRequest(int id)
        {
        }

        public virtual bool UseEquipmentRequest(int id, int characterId, int exerciseTierIdx)
        {
            return false;
        }

        public virtual bool UseEquipmentRequest(int id, int characterId, int exerciseTierIdx, ExerciseInitiator initiator)
        {
            return false;
        }

        public virtual void SellEditedEquipment()
        {
        }

        public virtual void UnlockEquipmentRequest(int id)
        {
        }

        public virtual Cost GetSellPrice(int equipmentId)
        {
            return null;
        }

        public virtual bool RepairEquipmentRequest(int id)
        {
            return false;
        }

        public virtual void SkipRepairEquipmentRequest(int id)
        {
        }

        public virtual void SetEquipmentSelectionMode(bool allowSelection)
        {
        }

        public int FindStaticEquipmentInRoom(StaticEquipmentID staticEquipmentID)
        {
            var equipment = equipments.Values.FirstOrDefault(eq => eq.staticEquipmentId == staticEquipmentID);

            return (equipment != null) ? equipment.id : EquipmentData.InvalidId;
        }

        public StaticEquipmentID? FindAvailableLockedStaticEquipment(string roomName, int estateTypeID)
        {
            var equipment = equipments.Values.FirstOrDefault
                ( eq => eq.isStatic &&
                  eq.isLocked &&
                  eq.roomName == roomName &&
                  eq.estateType.id == estateTypeID );

            return (equipment != null) ? new StaticEquipmentID?(equipment.staticEquipmentId) : null;
        }

        public virtual bool UpgradeEquipment(int id, bool canSpendFitBucks)
        {
            return false;
        }

        public UpgradeAvailabilityState GetEquipmentUpgradeInfo
            (int id, out EquipmentUpgradeInfo currentUpgradeInfo, out EquipmentUpgradeInfo nextUpgradeInfo)
        {
            currentUpgradeInfo = nextUpgradeInfo = null;
            if (gameCore.playerProfile.level >= minimumUpgradeAvailabilityLevel)
            {
                Equipment equipment;
                if (equipments.TryGetValue(id, out equipment))
                {
                    var currentEstateType = equipment.estateType;

                    DataTables.instance.equipmentUpgrades.TryGetValue
                        (new EquipmentUpgradeInfo.Key(currentEstateType.id, equipment.upgradeLevel), out currentUpgradeInfo);

                    DataTables.instance.equipmentUpgrades.TryGetValue
                        (new EquipmentUpgradeInfo.Key(currentEstateType.id, equipment.upgradeLevel + 1), out nextUpgradeInfo);

                    if (nextUpgradeInfo != null)
                    {
                        if (gameCore.playerProfile.level >= nextUpgradeInfo.unlockLevel)
                        {
                            return UpgradeAvailabilityState.EquipmentAvailable;
                        }
                        else if (equipment.upgradeLevel == 0)
                        {
                            return UpgradeAvailabilityState.FeatureAvailable;
                        }
                        else
                        {
                            return UpgradeAvailabilityState.EquipmentLocked;
                        }
                    }
                    else
                    {
                        return UpgradeAvailabilityState.EquipmentFull;
                    }
                }
                else
                {
                    return UpgradeAvailabilityState.EquipmentLocked;
                }
            }
            else
            {
                return UpgradeAvailabilityState.FeatureUnavailable;
            }
        }
        #endregion

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {
            InitStaticEquipment();
        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is EquipmentPersistentData);
            if (obj is EquipmentPersistentData)
            {
                persistentData = (EquipmentPersistentData)obj;

                foreach (var equipmentData in persistentData.equipmentData)
                {
                    if (!equipmentData.isStatic) // Static equipment will be bound later.
                    {
                        var equipment = new Equipment(this, equipmentData);
                        equipments.Add(equipmentData.id, equipment);
                    }
                }

                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad()
        {
            Reinit();
        }

        void IPersistent.OnLoaded(bool success)
        {
            if (success)
            {
            }
            else
            {
                equipments.Clear(); // Don't want to send death events!
                Reinit();
                PostLoadCleanup();
            }

            InitStaticEquipment();  // Call it first, so usual equipment doesn't meddle with static.
            SpawnEquipment();
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            PostLoadCleanup();

            foreach (var equipment in equipments.Values)
            {
                equipment.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }
        }

        void IPersistent.OnLoadSkipped()
        {
            SpawnEquipment();
        }

        #endregion

        #region IEquipmentManager API
        public int GetEquipmentsCount
            (int estateTypeID = -1
            , EstateType.Subtype? equipmentType = null
            , string roomName = null
            , StaticEquipmentID? staticEquipmentID = null
            , params EquipmentState[] states)
        {
            int count = 0;

            foreach (var equipment in equipments.Values)
            {
                if ((estateTypeID < 0 || estateTypeID == equipment.estateType.id) &&
                     (equipmentType == null || equipmentType.Value == equipment.estateType.itemType) &&
                     (!equipment.isStatic || staticEquipmentID == null || equipment.staticEquipmentId == staticEquipmentID) &&
                     (roomName == null || roomName == equipment.roomName) &&
                     (states == null || states.Length == 0 || states.Contains(equipment.state)))
                {
                    count++;
                }
            }

            return count;
        }

        public Equipment FindEquipment
            (int estateTypeID = -1
            , EstateType.Subtype? equipmentType = null
            , string roomName = null
            , StaticEquipmentID? staticEquipmentID = null
            , params EquipmentState[] states)
        {
            foreach (var equipment in equipments.Values)
            {
                if ((estateTypeID < 0 || estateTypeID == equipment.estateType.id) &&
                     (equipmentType == null || equipmentType.Value == equipment.estateType.itemType) &&
                     (!equipment.isStatic || staticEquipmentID == null || equipment.staticEquipmentId == staticEquipmentID) &&
                     (roomName == null || roomName == equipment.roomName) &&
                     (states == null || states.Length == 0 || states.Contains(equipment.state)))
                {
                    return equipment;
                }
            }

            return null;
        }

        public int GetEquipmentsCount(int equipmentType)
        {
            int count = 0;

            foreach (var equipmentData in persistentData.equipmentData)
            {
                if (equipmentData.estateType.id == equipmentType)
                    count++;
            }

            return count;

        }

        public bool IsCanBuyThis(int equipmentType)
        {
            EstateType estateType = DataTables.instance.estateTypes [equipmentType];

            if (estateType != null)
            {
                // TODO: What? This check seems wrong.
                int gymSize = gameCore.roomManager.GetRoomSizeInCells(gameCore.roomManager.GetRoomNameOfType(RoomType.Club)); //(gameCore.roomManager.activeRoomName);
                int needSize = SquareNeedForThis (equipmentType);
                if (needSize > -1 && gymSize >= needSize)
                    return true;
            }

            return false;
        }

        public int SquareNeedForThis(int equipmentType)
        {
            EstateType estateType = DataTables.instance.estateTypes [equipmentType];

            if (estateType != null)
            {
                int tCount = GetEquipmentsCount (equipmentType);

                if (tCount <= 0)
                    return 0;

                if ((tCount - 1) >= estateType.countForGymSize.Length)
                    return -1;

                return estateType.countForGymSize[tCount - 1];
            }

            return -1;
        }
        #endregion

        #region IShopPurchaseProtocol API
        public virtual bool InitPreview(ShopItemData item, Action<bool> onFinishedPreview)
        {
            return false;
        }

        public virtual bool ConfirmPurchase()
        {
            return false;
        }

        public virtual void CancelPurchase()
        {
        }

        public virtual void RestoreOwnedItem(ShopItemData item)
        {
        }

        public virtual void BuyIAP(string iapProductId)
        {
        }
        #endregion

        #region Protected virtual API.
        protected virtual void Reinit()
        {
            foreach (var equipment in equipments)
            {
                if (!equipment.Value.isStatic)
                    EquipmentDeathEvent.Send(equipment.Key);
            }

            persistentData.equipmentData.Clear();
            equipments.Clear();
        }
        #endregion

        #region Private functions.
        void Reset()
        {
            SelectEquipmentRequest(EquipmentData.InvalidId); 
        }

        void PostLoadCleanup()
        {
            foreach (var equipmentData in persistentData.equipmentData)
            {
                if (equipmentData.occupantCharacterId != PersonData.InvalidId)
                {
                    Person occupant;
                    if (!gameCore.personManager.persons.TryGetValue(equipmentData.occupantCharacterId, out occupant) ||
                        (occupant.state != PersonState.Workout || occupant.workoutEquipmentId != equipmentData.id))
                    {
                        Debug.LogWarning("Cleaning up wrong (not found) occupant character id on equipment.");

                        equipmentData.occupantCharacterId = PersonData.InvalidId;
                        if (equipmentData.state == EquipmentState.Working)
                            equipmentData.state = EquipmentState.Idle;
                    
                    }
                }

                if (equipmentData.state == EquipmentState.WorkPending)
                {
                    if (gameCore.personManager.persons.Values.Count(x => x.workoutEquipmentId == equipmentData.id && x.state == PersonState.WorkoutPending) == 0)
                    {
                        Debug.LogWarning("Cleaning up wrong equipment state (WorkPending) on equipment because corresponding character not found.");
                        equipmentData.state = EquipmentState.Idle;
                        equipmentData.occupantCharacterId = PersonData.InvalidId;
                    }
                }
            }
        }

        void InitStaticEquipment()
        {
            var staticEquipment = presentationEquipmentManager.GetStaticEquipmentList();

            // Bind existing equipment, remove equipment from list if it has no corresponding static equipment actor.
            foreach (var equipmentData in persistentData.equipmentData.ToList())    // Work on a copy, because we can modify original container.
            {
                if (equipmentData.isStatic)
                {
                    if (staticEquipment.Any(eq => eq.Key == equipmentData.staticEquipmentId))
                    {
                        var staticEq = staticEquipment.First (eq => eq.Key == equipmentData.staticEquipmentId);
                        if (staticEq.Value.estateType == equipmentData.estateType.id)
                        {
                            if (!staticEq.Value.isEditable)
                            {   // For non-editable equipment we always force designer-set placement in case it was moved between patches.
                                equipmentData.placement = staticEq.Value.placement;
                            }

                            var equipment = new Equipment(this, equipmentData, staticEq.Value.showAtLevel);
                            equipments.Add(equipmentData.id, equipment);
                        }
                        else
                        {
                            // Estate type changed: equipment must be recreated.
                            persistentData.equipmentData.RemoveAll(eq => eq.id == equipmentData.id);
                        }
                    }
                    else
                    {
                        // Static equipment was removed.
                        persistentData.equipmentData.RemoveAll(eq => eq.id == equipmentData.id);
                    }
                }
            }

            // Create Equipment's for new equipment actors (that was not bound on previous step).
            foreach (var se in staticEquipment)
            {
                if (!persistentData.equipmentData.Any(eq => eq.isStatic && eq.staticEquipmentId == se.Key))
                {
                    EstateType estateType;
                    if (DataTables.instance.estateTypes.TryGetValue(se.Value.estateType, out estateType))
                    {
                        int id = equipments.ProposeKey();
                        Assert.IsTrue(id != AutoKeyIntDictionary<Equipment>.invalidId);
                        var equipmentData = new EquipmentData(id, estateType, se.Key, se.Value);
                        persistentData.equipmentData.Add(equipmentData);

                        var equipment = new Equipment(this, equipmentData, se.Value.showAtLevel);

                        equipments.Add(id, equipment);
                    }
                }
            }
        }

        private void SpawnEquipment()
        {
            foreach (var equipment in equipments.Values)
            {
                if (equipment.isStatic)
                {
                    presentationEquipmentManager?.BindStaticEquipmentRequest(equipment.id, equipment.staticEquipmentId, equipment);
                }
                else
                {
                    EquipmentBirthEvent.Send(equipment, true);
                }
            }
        }
        #endregion

        #region Equipment events proxy functions.
        void OnEquipmentBirthEvent(Equipment equipment, bool restored)
        {
            Assert.IsNotNull(equipment);
            if (presentationEquipmentManager != null)
            {
                presentationEquipmentManager.CreateEquipmentRequest(equipment.id, equipment);
            }
        }

        void OnEquipmentDeathEvent(int id)
        {
            if (presentationEquipmentManager != null)
            {
                presentationEquipmentManager.DestroyEquipmentRequest(id);
            }
        }

        void OnEquipmentSelectionEvent(Equipment equipment, bool selected)
        {
            Assert.IsNotNull(equipment);
            SetSelectionEvent?.Invoke(equipment.id, selected);
        }

        private void OnEquipmentStateChangedEvent(Equipment equipment, EquipmentState oldState, EquipmentState newState)
        {
            Assert.IsNotNull(equipment);
            EquipmentStateChangedEvent?.Invoke(equipment.id, oldState, newState);
        }

        void OnEquipmentStartPendingExerciseEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentStartPendingExerciseEvent?.Invoke(equipment.id);
        }

        void OnEquipmentSwitchPendingExerciseEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentSwitchPendingExerciseEvent?.Invoke(equipment.id);
        }

        void OnEquipmentCancelPendingExerciseEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentCancelPendingExerciseEvent?.Invoke(equipment.id);
        }

        void OnEquipmentStartExerciseEvent(Equipment equipment, int exerciseAnimation, float speed)
        {
            Assert.IsNotNull(equipment);
            if (presentationEquipmentManager != null)
            {
                presentationEquipmentManager.EquipmentStartExerciseRequest(equipment.id, exerciseAnimation, speed);
            }
        }

        void OnEquipmentSwitchExerciseEvent (Equipment equipment, int exerciseAnimation, float speed)
        {
            Assert.IsNotNull(equipment);
            if (presentationEquipmentManager != null)
            {
                presentationEquipmentManager.EquipmentSwitchExerciseRequest(equipment.id, exerciseAnimation, speed);
            }
        }

        void OnEquipmentEndExerciseEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            if (presentationEquipmentManager != null)
            {
                presentationEquipmentManager.EquipmentEndExerciseRequest(equipment.id);
            }
        }

        void OnEquipmentEditModeEvent(Equipment equipment, bool canSellOrStore)
        {
            SetEditModeEvent?.Invoke(equipment != null ? equipment.id : EquipmentData.InvalidId, canSellOrStore);
        }

        void OnEquipmentAssemblyStartEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentAssemblyStartEvent?.Invoke(equipment.id);
        }

        void OnEquipmentAssemblyCompleteEvent(Equipment equipment, bool skipped)
        {
            Assert.IsNotNull(equipment);
            EquipmentAssemblyCompleteEvent?.Invoke(equipment.id, skipped);
        }

        void OnEquipmentAssemblyCompleteConfirmedEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentAssemblyCompleteConfirmedEvent?.Invoke(equipment.id);
        }

        void OnEquipmentRepairRequiredEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentRepairRequiredEvent?.Invoke(equipment.id);
        }

        void OnEquipmentStartRepairEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentStartRepairEvent?.Invoke(equipment.id);
        }

        void OnEquipmentEndRepairEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentEndRepairEvent?.Invoke(equipment.id);
        }

        void OnEquipmentUnlockedEvent(Equipment equipment)
        {
            Assert.IsNotNull(equipment);
            EquipmentStartUnlockingEvent?.Invoke(equipment.id);
        }

        void OnEquipmentUpgradedEvent (Equipment equipment, int upgradeLevel)
        {
            Assert.IsNotNull(equipment);
            EquipmentUpgradedEvent?.Invoke(equipment.id, upgradeLevel);
        }
        #endregion
    }
}