﻿using UnityEngine;

namespace Logic.Estate.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Estate/Facade/Logic Equipment Manager Demo")]
    public class LogicEquipmentManagerDemo : LogicEquipmentManagerBase
    {
        #region Interfaces: properties
        #endregion

        #region Public data.
        #endregion

        #region Private data.
        #endregion

        #region Bridge
        #endregion

        #region Bridge event handlers.
        #endregion

        #region Unity API
        #endregion

        #region Public API
        #endregion

        #region Private API
        #endregion
    }
}