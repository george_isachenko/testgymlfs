﻿using System;
using System.Collections;
using Core;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Estate.Static;
using Data.Person;
using Data.Room;
using Logic.Estate.Events;
using Logic.Persons;
using UnityEngine;
using UnityEngine.Assertions;
using Core.Analytics;
using Logic.Serialization;

namespace Logic.Estate
{
    public class Equipment : IEquipmentInfo
    {
        #region Public properties
        public IEquipmentManager equipmentManager { get; private set; }

        public int id
        {
            get
            {
                return data.id;
            }
        }

        public EstateType estateType
        {
            get
            {
                return data.estateType;
            }
        }

        public EquipmentPlacement placement
        {
            get
            {
                return data.placement;
            }
        }

        public EquipmentState state
        {
            get
            {
                return data.state;
            }
        }

        public string roomName
        {
            get
            {
                return data.roomName;
            }
        }

        public string staticEquipmentName
        {
            get
            {
                return data.staticEquipmentName;
            }
        }

        public int occupantCharacterId
        {
            get
            {
                return data.occupantCharacterId;
            }
        }

        public float assemblyTotalTime
        {
            get
            {
                return data.assemblyTotalTime;
            }
        }

        public DateTime assemblyEndTime
        {
            get
            {
                return data.assemblyEndTime;
            }
        }

        public int health
        {
            get
            {
                return data.health;
            }
        }

        public int upgradeLevel
        {
            get
            {
                return data.upgradeLevel;
            }
        }

        public bool isOccupied
        {
            get
            {
                return data.isOccupied;
            }
        }

        public bool isUsable
        {
            get
            {
                return data.isUsable;
            }
        }

        public bool isPotentiallyAvailableForUse
        {
            get
            {
                return data.isPotentiallyAvailableForUse;
            }
        }

        public bool isSelectable
        {
            get
            {
                return data.isSelectable;
            }
        }

        public bool isEditable
        {
            get
            {
                return data.isEditable;
            }
        }

        public bool canSellOrStore
        {
            get
            {
                return data.canSellOrStore;
            }
        }

        public bool isStatic
        {
            get
            {
                return data.isStatic;
            }
        }

        public bool isBroken
        {
            get
            {
                return data.isBroken;
            }
        }

        public bool isRepairing
        {
            get
            {
                return data.isRepairing;
            }
        }

        public bool isInDelivery
        {
            get
            {
                return data.isInDelivery;
            }
        }

        public bool affectsStyle
        {
            get
            {
                return data.affectsStyle;
            }
        }

        public bool isLocked
        {
            get
            {
                return data.isLocked;
            }
        }

        public StaticEquipmentID staticEquipmentId
        {
            get
            {
                return data.staticEquipmentId;
            }
        }

        public bool meetsLevelRequirement
        {
            get
            {
                return (!isStatic || (showAtLevel <= 0 || equipmentManager.gameCore.playerProfile.level >= showAtLevel));
            }
        }

        internal void SetPlacement (EquipmentPlacement placement)
        {
            if (data.placement != placement)
            {
                data.placement = placement;
            }
        }
        #endregion

        #region Protected properties.
        protected EquipmentData data { get; private set; }
        #endregion

        #region Private data.
        private IEnumerator currentJob;
        private int showAtLevel; // Non-Serialized: Used only for static equip and set on every static equip binding.
        #endregion

        #region Constructors.
        public Equipment(IEquipmentManager equipmentManager, EquipmentData data)
        {
            Assert.IsNotNull(equipmentManager);
            Assert.IsNotNull(data);
            this.equipmentManager = equipmentManager;
            this.data = data;
            showAtLevel = 0;
            currentJob = null;
        }

        public Equipment(IEquipmentManager equipmentManager, EquipmentData data, int showAtLevel)
        {
            Assert.IsNotNull(equipmentManager);
            Assert.IsNotNull(data);
            this.equipmentManager = equipmentManager;
            this.data = data;
            this.showAtLevel = showAtLevel;
            currentJob = null;
        }
        #endregion

        #region Public virtual API.
        public virtual void OnPostCreate()
        {
            data.state = EquipmentState.Assembling; // A hack to set state before equipment birth without sending callbacks.
            StartAssembly(() => EquipmentBirthEvent.Send(this, false));
        }

        public virtual void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (data.state == EquipmentState.Assembling)
            {
                if (data.assemblyTotalTime == 0)
                {   // Workaround for old saves (SerializationVersion < 11), when there was no assemblyTotalTime: calculate to the same value as time left.
                    // If time is already passed do another workaround: use 1 second.
                    data.assemblyTotalTime = Math.Max(1.0f, (float)(data.assemblyEndTime - loadTimeUTC).TotalSeconds);
                }

                if (data.assemblyEndTime == DateTime.MinValue || loadTimeUTC >= data.assemblyEndTime)
                {
                    AssemblyComplete(false);
                }
                else
                {
                    Assert.IsNull(currentJob);
                    currentJob = AssemblyJob();
                    equipmentManager.gameCore.StartCoroutine(currentJob);
                }
            }
            else if (data.state == EquipmentState.Repairing)
            {
                if (data.assemblyTotalTime == 0)
                {   // Workaround for old saves (SerializationVersion < 11), when there was no assemblyTotalTime: calculate to the same value as time left.
                    // If time is already passed do another workaround: use 1 second.
                    data.assemblyTotalTime = Math.Max(1.0f, (float)(data.assemblyEndTime - loadTimeUTC).TotalSeconds);
                }

                if (data.assemblyEndTime == DateTime.MinValue || loadTimeUTC >= data.assemblyEndTime)
                {
                    RepairComplete();
                }
                else
                {
                    Assert.IsNull(currentJob);
                    currentJob = RepairJob();
                    equipmentManager.gameCore.StartCoroutine(currentJob);
                }
            }
            else if (data.state == EquipmentState.Broken)
            {
                EquipmentRepairRequiredEvent.Send(this);
            }
        }

        public virtual void Destroy()
        {
            if (currentJob != null)
            {
                equipmentManager.gameCore.StopCoroutine(currentJob);
                currentJob = null;
            }
        }

        public virtual void SkipAssembly()
        {
            if (data.state == EquipmentState.Assembling &&
                data.assemblyEndTime != DateTime.MinValue &&
                data.assemblyEndTime - DateTime.UtcNow > TimeSpan.Zero)
            {
                var timeLeftNoFractions = Mathf.RoundToInt((float)((data.assemblyEndTime - DateTime.UtcNow).TotalSeconds));

                var skipCost = DynamicPrice.GetSkipCost(timeLeftNoFractions);
                if (skipCost <= 0 ||
                    DebugHacks.visitorsAutoPlay ||
                    equipmentManager.gameCore.playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, skipCost, 1), new Analytics.MoneyInfo("Skip", "Assembly")))
                {
                    if (currentJob != null)
                    {
                        equipmentManager.gameCore.StopCoroutine(currentJob);
                        currentJob = null;
                    }

                    AssemblyComplete(true);
                }
            }
        }

        public virtual bool RepairEquipmentRequest(Cost repairCost, int repairResourcesAvailable, float timeSeconds)
        {
            Assert.IsNotNull(repairCost);

            if (isBroken)
            {
                var costUsed = (repairCost.value <= 0 || repairCost.value <= repairResourcesAvailable)
                    ? repairCost
                    : new Cost (Cost.CostType.BucksOnly, Cost.RecourcePriceInFitbucks(repairCost.resourceType), 1);

                if (costUsed.value <= 0 ||
                    DebugHacks.visitorsAutoPlay ||
                    equipmentManager.gameCore.playerProfile.SpendMoney(costUsed, new Analytics.MoneyInfo("Repair", data.estateType.locNameId)))
                {
                    data.assemblyTotalTime = timeSeconds;
                    data.assemblyEndTime = DateTime.UtcNow + TimeSpan.FromSeconds(timeSeconds);

                    Assert.IsNull(currentJob);
                    currentJob = RepairJob();
                    equipmentManager.gameCore.StartCoroutine(currentJob);

                    ChangeState(EquipmentState.Repairing);
                    return true;
                }
            }
            return false;
        }

        public virtual void SkipRepairEquipmentRequest()
        {
            if (isRepairing)
            {
                if (currentJob != null)
                {
                    equipmentManager.gameCore.StopCoroutine(currentJob);
                    currentJob = null;
                }
                RepairComplete();
            }
        }

        public virtual void ConfirmAssemblyComplete()
        {
            Assert.IsTrue(data.state == EquipmentState.AssemblyComplete);

            ChangeState(EquipmentState.Idle);
            EquipmentAssemblyCompleteConfirmedEvent.Send(this);
        }

        public virtual bool UnlockRequest()
        {
            Assert.IsTrue(isLocked);

            if (isLocked)
            {
                StartAssembly(() => EquipmentStartUnlockingEvent.Send(this));
                return true;
            }
            return false;
        }
        #endregion

        #region Public API.
        public void Upgrade()
        {
            data.upgradeLevel += 1;

            equipmentManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            EquipmentUpgradedEvent.Send(this, upgradeLevel);
        }

        public bool SchedulePendingExercise(Person person, Exercise exercise)
        {
            Assert.IsTrue(data.state == EquipmentState.Idle);

            if (data.state == EquipmentState.Idle)
            {
                ChangeState(EquipmentState.WorkPending);
                EquipmentStartPendingExerciseEvent.Send(this);
                return true;
            }
            return false;
        }

        public bool StartPendingExercise(Person person, float speed)
        {
            Assert.IsTrue(data.state == EquipmentState.WorkPending ||
                (state == EquipmentState.Working && !equipmentManager.gameCore.isGameStarted));

            if (data.state == EquipmentState.WorkPending ||
                (state == EquipmentState.Working && !equipmentManager.gameCore.isGameStarted))
            {
                data.occupantCharacterId = person.id;
                ChangeState(EquipmentState.Working);

                EquipmentStartExerciseEvent.Send(this, person.workoutExercise.equipmentSubAnimation, speed);
                return true;
            }
            return true;
        }

        public bool SwitchExercise(Exercise newExercise, float speed)
        {
            Assert.IsTrue(data.state == EquipmentState.WorkPending || data.state == EquipmentState.Working);

            if (data.state == EquipmentState.WorkPending)
            {
                EquipmentSwitchPendingExerciseEvent.Send(this);
                return true;
            }
            else if (data.state == EquipmentState.Working)
            {
                EquipmentSwitchExerciseEvent.Send(this, newExercise.equipmentSubAnimation, speed);
                return true;
            }
            return false;
        }

        public bool EndExercise(Exercise exercise)
        {
            Assert.IsTrue(data.state == EquipmentState.Working ||
                (state == EquipmentState.WorkPending && !equipmentManager.gameCore.isGameStarted));

            if (data.state == EquipmentState.Working ||
                (state == EquipmentState.WorkPending && !equipmentManager.gameCore.isGameStarted))
            {
                ChangeState(EquipmentState.Working); // In case it' still in WorkPending state, is possible when completing not started but already finished task on load.

                data.occupantCharacterId = PersonData.InvalidId;

                if (data.estateType.itemType == EstateType.Subtype.Training)
                {
                    var tier = data.estateType.GetExerciseTier(exercise);
                    var equipmentUseHPCosts = equipmentManager.useHPCosts;
                    Assert.IsNotNull(equipmentUseHPCosts);
                    if (equipmentUseHPCosts.Length == 0)
                    {
                        Debug.LogErrorFormat("Missing Equipments usage HP costs in Equipment Manager!");
                    }

                    if (tier >= 0 && tier < equipmentUseHPCosts.Length)
                    {
                        var cost = equipmentUseHPCosts[tier];

#if DEBUG
                        if (DebugHacks.fastEquipmentBreakage && tier < DebugHacks.fastEquipmentBreakageCosts.Length)
                        {
                            cost = DebugHacks.fastEquipmentBreakageCosts[tier];
                        }
#endif // DEBUG

                        if (cost.minHPCost >= cost.maxHPCost)
                        {
                            data.health -= cost.minHPCost;
                        }
                        else if (cost.minHPCost < cost.maxHPCost)
                        {
                            data.health -= UnityEngine.Random.Range(cost.minHPCost, cost.maxHPCost + 1);
                        }
                    }
                }

                EquipmentEndExerciseEvent.Send(this);

                if (data.health <= 0)
                {
                    data.health = 0;
                    ChangeState(EquipmentState.Broken);
                }
                else
                {
                    ChangeState(EquipmentState.Idle);
                }

                return true;
            }
            return false;
        }

        public bool CancelPendingExercise()
        {
            Assert.IsTrue(data.state == EquipmentState.WorkPending);
            if (data.state == EquipmentState.WorkPending)
            {
                ChangeState(EquipmentState.Idle);
                EquipmentCancelPendingExerciseEvent.Send(this);
                return true;
            }
            return false;
        }

        public void AbortExercise()
        {
            Assert.IsTrue(data.state == EquipmentState.WorkPending || data.state == EquipmentState.Working);
            Assert.IsFalse(equipmentManager.gameCore.isGameStarted);

            if (data.state == EquipmentState.WorkPending || data.state == EquipmentState.Working)
            {
                data.occupantCharacterId = PersonData.InvalidId;

                ChangeState(EquipmentState.Idle);
            }
        }
        #endregion

        #region Protected API.
        protected void ChangeState(EquipmentState newState)
        {
            if (state != newState)
            {
                var oldState = state;

                data.state = newState;

                equipmentManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                if (state == EquipmentState.Broken)
                {
                    PersistentCore.instance.analytics.GameEvent("EqBreak", "equpmentName", estateType.locNameId);

                    EquipmentRepairRequiredEvent.Send(this);
                }
                else if (state == EquipmentState.Repairing)
                {
                    PersistentCore.instance.analytics.GameEvent("StartRepair", "equpmentName", estateType.locNameId);

                    EquipmentStartRepairEvent.Send(this);
                }

                EquipmentStateChangedEvent.Send(this, oldState, newState);
            }
        }
        #endregion

        #region Private methods.
        private IEnumerator AssemblyJob()
        {
            yield return null;

            yield return new WaitUntil(() => (DateTime.UtcNow >= data.assemblyEndTime));

            currentJob = null;
            AssemblyComplete(false);
        }

        private IEnumerator RepairJob()
        {
            yield return null;

            yield return new WaitUntil(() => (DateTime.UtcNow >= data.assemblyEndTime));

            currentJob = null;
            RepairComplete();
        }

        private void StartAssembly(Action intermediateDelegate)
        {
            data.assemblyTotalTime = (float)data.estateType.assemblyTime.TotalSeconds;
            data.assemblyEndTime = DateTime.UtcNow + data.estateType.assemblyTime;

            ChangeState(EquipmentState.Assembling);

            intermediateDelegate?.Invoke();

            if (data.estateType.assemblyTime > TimeSpan.Zero)
            {
                Assert.IsNull(currentJob);
                currentJob = AssemblyJob();
                equipmentManager.gameCore.StartCoroutine(currentJob);
            }

            EquipmentAssemblyStartEvent.Send(this);

            if (data.estateType.assemblyTime <= TimeSpan.Zero)
            {
                AssemblyComplete(false);
            }
        }

        private void AssemblyComplete(bool skipped)
        {
            Assert.IsTrue(data.state == EquipmentState.Assembling);
            Assert.IsNull(currentJob);

            data.assemblyTotalTime = 0;
            data.assemblyEndTime = DateTime.MinValue;
            ChangeState(EquipmentState.AssemblyComplete);
            EquipmentAssemblyCompleteEvent.Send(this, skipped);
        }

        private void RepairComplete()
        {
            Assert.IsTrue(data.state == EquipmentState.Repairing);
            Assert.IsNull(currentJob);

            var minHP = equipmentManager.minHP;
            var maxHP = equipmentManager.maxHP;

            if (minHP == maxHP)
            {
                data.health = maxHP;
            }
            if (minHP < maxHP)
            {
                data.health = UnityEngine.Random.Range(minHP, maxHP + 1);
            }
            else
            {
                data.health = equipmentManager.initialHP;
            }

            data.assemblyTotalTime = 0;
            data.assemblyEndTime = DateTime.MinValue;
            ChangeState(EquipmentState.Idle);
            EquipmentEndRepairEvent.Send(this);
        }
        #endregion
    }
}