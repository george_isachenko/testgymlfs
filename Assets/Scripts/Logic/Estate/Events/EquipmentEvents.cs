﻿using Core.EventSystem;
using Data.Estate;
using Data.Estate.Static;

namespace Logic.Estate.Events
{
    public class EquipmentBirthEvent : StaticEvent<EquipmentBirthEvent.Delegate, Equipment, bool>
    {
        public delegate void Delegate (Equipment equipment, bool restored);
    }

    public class EquipmentDeathEvent : StaticEvent<EquipmentDeathEvent.Delegate, int>
    {
        public delegate void Delegate(int id);
    }

    public class EquipmentStateChangedEvent : StaticEvent<EquipmentStateChangedEvent.Delegate, Equipment, EquipmentState, EquipmentState>
    {
        public delegate void Delegate(Equipment equipment, EquipmentState oldState, EquipmentState newState);
    }

    public class EquipmentSelectionEvent : StaticEvent<EquipmentSelectionEvent.Delegate, Equipment, bool>
    {
        public delegate void Delegate(Equipment equipment, bool selected);
    }

    public class EquipmentStartPendingExerciseEvent : StaticEvent<EquipmentStartPendingExerciseEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentSwitchPendingExerciseEvent : StaticEvent<EquipmentSwitchPendingExerciseEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentCancelPendingExerciseEvent : StaticEvent<EquipmentStartPendingExerciseEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentStartExerciseEvent : StaticEvent<EquipmentStartExerciseEvent.Delegate, Equipment, int, float>
    {
        public delegate void Delegate(Equipment equipment, int exerciseAnimation, float speed);
    }

    public class EquipmentSwitchExerciseEvent : StaticEvent<EquipmentSwitchExerciseEvent.Delegate, Equipment, int, float>
    {
        public delegate void Delegate(Equipment equipment, int exerciseAnimation, float speed);
    }

    public class EquipmentEndExerciseEvent : StaticEvent<EquipmentEndExerciseEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentPreviewModeChangedEvent : StaticEvent<EquipmentPreviewModeChangedEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentEditModeEvent : StaticEvent<EquipmentEditModeEvent.Delegate, Equipment, bool>
    {
        public delegate void Delegate(Equipment equipment, bool canSellOrStore);
    }

    public class EquipmentAssemblyStartEvent : StaticEvent<EquipmentAssemblyStartEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentAssemblyCompleteEvent : StaticEvent<EquipmentAssemblyCompleteEvent.Delegate, Equipment, bool>
    {
        public delegate void Delegate(Equipment equipment, bool skipped);
    }

    public class EquipmentAssemblyCompleteConfirmedEvent : StaticEvent<EquipmentAssemblyCompleteConfirmedEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentRepairRequiredEvent : StaticEvent<EquipmentRepairRequiredEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentStartRepairEvent : StaticEvent<EquipmentStartRepairEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentEndRepairEvent : StaticEvent<EquipmentEndRepairEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentStartUnlockingEvent : StaticEvent<EquipmentStartUnlockingEvent.Delegate, Equipment>
    {
        public delegate void Delegate(Equipment equipment);
    }

    public class EquipmentUpgradedEvent : StaticEvent<EquipmentUpgradedEvent.Delegate, Equipment, int>
    {
        public delegate void Delegate(Equipment equipment, int newUpgradeLevel);
    }

}