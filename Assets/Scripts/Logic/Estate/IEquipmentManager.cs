﻿using System;
using System.Collections.Generic;
using Data.Estate;
using Data.Estate.Static;
using Data.Estate.Upgrades;
using Data.Room;
using Logic.Core;
using Logic.Shop;
using Data.Person;

namespace Logic.Estate
{
    [Serializable]
    public struct EquipmentUseHPCosts
    {
        public int minHPCost;
        public int maxHPCost;

        public EquipmentUseHPCosts(int minHPCost, int maxHPCost)
        {
            this.minHPCost = minHPCost;
            this.maxHPCost = maxHPCost;
        }
    }

    public interface IEquipmentManager
    {
        IGameCore gameCore
        {
            get;
        }

        IShopPurchaseProtocol purchaseProtocol
        {
            get;
        }

        IDictionary<int, Equipment> equipment
        {
            get;
        }

        EquipmentUseHPCosts[] useHPCosts
        {
            get;
        }

        int initialHP
        {
            get;
        }

        int minHP
        {
            get;
        }

        int maxHP
        {
            get;
        }

        int maxSameEquipmentCount
        {
            get;
        }

        int CreateEquipment(int equipmentType, EquipmentPlacement placement);

        int GetEquipmentsCount(int estateTypeID = -1, EstateType.Subtype? equipmentType = null, string roomName = null, StaticEquipmentID? staticEquipmentID = null, params EquipmentState[] states);
        Equipment FindEquipment(int estateTypeID = -1, EstateType.Subtype? equipmentType = null, string roomName = null, StaticEquipmentID? staticEquipmentID = null, params EquipmentState[] states);

        bool IsCanBuyThis(int equipmentType);
        int SquareNeedForThis(int equipmentType);

        // Common API with ILogicEquipmentManager
        bool hasSelection
        {
            get;
        }

        int selectedEquipmentId
        {
            get;
        }

        bool isInEditMode
        {
            get;
        }

        int editingEquipmentId
        {
            get;
        }

        bool canEditEquip
        {
            get;
            set;
        }

        bool SelectEquipmentRequest(int id);
        bool UseEquipmentRequest(int id, int characterId, int exerciseTierIdx, ExerciseInitiator initiator);
        bool IsEditableEquipment(int id);
        bool EditEquipmentRequest(int id);
        bool RepairEquipmentRequest(int id);
        void UnlockEquipmentRequest(int id);
        void SkipRepairEquipmentRequest(int id);
        void SetEquipmentSelectionMode(bool allowSelection);
        int FindStaticEquipmentInRoom(StaticEquipmentID staticEquipmentID);
        StaticEquipmentID? FindAvailableLockedStaticEquipment(string roomName, int estateTypeID);

        bool UpgradeEquipment(int id, bool canSpendFitBucks);
        UpgradeAvailabilityState GetEquipmentUpgradeInfo (int id, out EquipmentUpgradeInfo currentUpgradeInfo, out EquipmentUpgradeInfo nextUpgradeInfo);
    }
} // namespace Logic.Estate
