﻿using System;
using Logic.PlayerProfile.Events;
using UnityEngine;

namespace Logic
{
    public class NextDayDetector
    {
        private DateTime _yesterday = DateTime.Today;

        public void Update()
        {
            var today = DateTime.Today;
            if (_yesterday != today)
            {
                var diff = today - _yesterday;
                var daysCount = (int)diff.TotalDays;
                if (daysCount > 0)
                {
                    _yesterday = today;

                    Debug.LogFormat("NextDayDetector: Next Day detected in runtime, days passed: {0}.", daysCount);
                    NewDayEvent.Send(daysCount);
                }
                else
                {
                    Debug.LogWarningFormat("NextDayDetector: zero or negative days passed: {0}. This is wrong, ignored!", daysCount);
                }
            }
        }

        public void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            var prevSessionTime = DateTime.Now - timePassedSinceSave;
            var diff = DateTime.Today.Date - prevSessionTime.Date;

            var daysCount = (int)diff.TotalDays;

            Debug.LogFormat("NextDayDetector: days passed {0}.", daysCount);

            if (daysCount > 0)
            {
                NewDayEvent.Send(daysCount);
            }
        }

        public TimeSpan TimeToNextDay()
        {
            return new TimeSpan(1,0,0,0) - DateTime.Now.TimeOfDay;
        }
    }
}