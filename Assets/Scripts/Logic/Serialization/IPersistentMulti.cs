﻿using System;

namespace Logic.Serialization
{
    public interface IPersistentMulti : IPersistent
    {
        bool MoveNext();
        void Reset();
    }
}