﻿using System.Globalization;
using Core.Collections;
using Newtonsoft.Json.Serialization;

namespace Logic.Serialization
{
    public class SimpleReferenceResolver : IReferenceResolver
    {
        private int _referenceCount;

        BiDictionaryOneToOne<string, object> mappings;

        public SimpleReferenceResolver ()
        {
            mappings = new BiDictionaryOneToOne<string, object>();
        }

        public object ResolveReference(object context, string reference)
        {
            object value;
            mappings.TryGetByFirst(reference, out value);
            return value;
        }

        public string GetReference(object context, object value)
        {
            string reference;
            if (!mappings.TryGetBySecond(value, out reference))
            {
                _referenceCount++;
                reference = _referenceCount.ToString(CultureInfo.InvariantCulture);
                mappings.Add(reference, value);
            }

            return reference;
        }

        public void AddReference(object context, string reference, object value)
        {
            mappings.Add(reference, value);
        }

        public bool IsReferenced(object context, object value)
        {
            string reference;
            return mappings.TryGetBySecond(value, out reference);
        }
    }
}