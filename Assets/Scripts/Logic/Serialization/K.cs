﻿namespace Logic.Serialization
{
    // Encryption system data stuff class.
    public static class K
    {
        // SaveGame key salt.
        public static readonly byte[] sgs = new byte[] { 0x4F, 0xFB, 0x01, 0x89, 0x57, 0x3C, 0xEF, 0x31 };
    }
}
