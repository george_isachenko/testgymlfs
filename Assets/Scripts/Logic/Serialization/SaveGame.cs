﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using Core;
using Core.Encryption;
using Core.Network;
using FileSystem;
using Logic.Core;
using LZ4;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Converters;
using UnityEngine;
using UnityEngine.Assertions;
using Data;
using Core.Analytics;

namespace Logic.Serialization
{
    public struct SaveGameDataHeader
    {
        public DateTime saveDate;
        public int serializationVersion;

        public SaveGameDataHeader(DateTime saveDate, int serializationVersion)
        {
            this.saveDate = saveDate;
            this.serializationVersion = serializationVersion;
        }
    }

    public class SaveGame : ISaveGameConverterProvider
    {
        #region Types.
        public enum LoadResult
        {
            Success = 0,
            Failed,
            Skipped
        }

        enum SaveType
        {
            FrameSave = 0,
            AutoSave,
            Immediate,
            ImmediateOptional
        }

        enum CurrentOperation
        {
            None = 0,
            Loading,
            Saving,
        }
        #endregion

        #region Static data.
        static readonly int encKeySize = 128; // In bits.
        static readonly float saveSoonDelay = 3.0f;
        static readonly float firstSaveDelay = 3.0f;

        static readonly string tempFileNameTemplate = @"{0}.tmp";
        static readonly string backupFileNameTemplate = @"{0}.bak";
#if DEBUG
        static readonly string debugFileNameTemplate = @"{0}.debug.json";
        static readonly string debugLoadedFileNameTemplate = @"{0}.loaded.json";
#endif
        #endregion

        #region Debug Static properties.
#if DEBUG
        public static bool dbgNoSave
        {
            get
            {
                return (PlayerPrefs.GetInt("dbgNoSave") != 0);
            }

            set
            {
                PlayerPrefs.SetInt("dbgNoSave", value ? 1 : 0);
                PlayerPrefs.Save();
            }
        }
#endif
        #endregion

        #region Fields.
        string saveFileName;
        IPersistent[] persistentList;
        IsoDateTimeConverter isoDateTimeConverter;
        StringEnumConverter stringEnumConverter;
        byte[] ekBytes;
        IGameCore gameCore;
        bool saveForbiddenOnCriticalError = false;
        bool saveDisabled = true;
        bool saveOnThisFrame = false;
        bool saveGameExists = false;
        float nextAutoSaveTime = 0;
        int dbgSaveIteration = 0;
        int lastSaveFrame = 0;
        CurrentOperation currentOperation = CurrentOperation.None;
        #endregion

        #region ISaveGameConverterProvider properties.
        IExerciseProvider ISaveGameConverterProvider.exerciseProvider
        {
            get
            {
                return gameCore.exerciseProvider;
            }
        }

        IEstateTypeProvider ISaveGameConverterProvider.estateTypeProvider
        {
            get
            {
                return gameCore.estateTypeProvider;
            }
        }

        JsonConverter ISaveGameConverterProvider.defaultEnumConverter
        {
            get
            {
                return stringEnumConverter;
            }
        }
        #endregion

        #region Events.
        public event Action onSaveGameCreated;
        #endregion

        #region Public static API.
        public static bool Exists (string saveGameFullPath)
        {
            return (File.Exists(saveGameFullPath) && new FileInfo(saveGameFullPath).Length > 0);
        }
        #endregion

        #region Public API.
        public bool isSaveGameExists
        {
            get { return saveGameExists; }
        }

        public bool isSaveEnabled
        {
            get { return !saveDisabled; }
        }

        public SaveGame(string fileName, IPersistent[] persistentList, IGameCore gameCore)
        {
            this.saveFileName = fileName;
            this.persistentList = persistentList;
            this.gameCore = gameCore;

            Assert.IsNotNull(persistentList);
            if (persistentList == null)
                throw new NullReferenceException("Argument 'persistentList' is null.");

            isoDateTimeConverter = new IsoDateTimeConverter();
            isoDateTimeConverter.DateTimeStyles = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;
            isoDateTimeConverter.DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";

            stringEnumConverter = new StringEnumConverter();

            var ekFileName = ResourcePaths.Persistent.keysResFolder + ResourcePaths.Persistent.saveGameKeyResource;
            ekBytes = EKBytes(ekFileName);
            if (ekBytes != null)
            {
                ekBytes = EH.GK(ekBytes, K.sgs, encKeySize / 8);
            }
            else
            {
#if DEBUG
                Debug.LogErrorFormat("Cannot read Savegame encryption key file resource '{1}'. Savegame save and loading will fail.", ekFileName);
#endif
            }
        }

        public bool TrySave()
        {
            if (saveForbiddenOnCriticalError)
            {
                return false;
            }

            if (currentOperation != CurrentOperation.None)
            {
                Debug.LogFormat("Cannot save game: other operation ({0}) is in progress!", currentOperation);
                return false;
            }

            if (saveDisabled)
            {
                return true;
            }

            SaveType? saveType = null;
            if (saveOnThisFrame)
            {
                if (Time.frameCount > lastSaveFrame)
                    saveType = SaveType.FrameSave;
            }
            else if (nextAutoSaveTime > 0 && Time.realtimeSinceStartup >= nextAutoSaveTime)
            {
                saveType = SaveType.AutoSave;
            }

            if (saveType != null)
            {
                saveOnThisFrame = false; // Always turn off.
                nextAutoSaveTime = 0;
                currentOperation = CurrentOperation.Saving;
                var saveResult = DoSave(saveType.Value);
                currentOperation = CurrentOperation.None;

                if (saveResult)
                {
                    if (!saveGameExists)
                    {
                        saveGameExists = true;

                        PersistentCore.instance.NotifySaveGameCreated();
                        onSaveGameCreated?.Invoke();
                    }

                    PersistentCore.instance.networkController.NotifySaveGameUpdated(false);

                    return true;
                }
                else
                {
                    nextAutoSaveTime = Time.realtimeSinceStartup + saveSoonDelay; // Try later, using auto-save.
                }
            }
            return false;
        }

        public LoadResult Load(bool loadFromResource, bool ignoreTimePassed = false, NetworkFileTimeStamps networkFileTimeStamps = null)
        {
            if (currentOperation != CurrentOperation.None)
            {
                Debug.LogFormat("Cannot load game: other operation ({0}) is in progress! Game paused.", currentOperation);
                Debug.Break();
                return LoadResult.Failed;
            }

            var prevSaveDisabled = saveDisabled;
            saveDisabled = false; 

            if (ekBytes == null)
                return LoadResult.Failed;

            currentOperation = CurrentOperation.Loading;

            var result = (loadFromResource ? LoadFromResource() : LoadFromFile(networkFileTimeStamps, ignoreTimePassed));

            currentOperation = CurrentOperation.None;

            if (result != LoadResult.Success)
                saveDisabled = prevSaveDisabled; 

            if (result == LoadResult.Skipped)
            {
                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    persistent.InitDefaultData();
                }

                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    persistent.OnLoadSkipped();
                }
            }

            saveGameExists = (result == LoadResult.Success);

            return result;
        }

#if DEBUG
        public void DebugDeleteSave()
        {
            File.Delete(saveFileName);
        }
#endif

        public void ChangeSaveGameBehaviour(SaveGameBehaviour saveGameBehaviour)
        {
            switch (saveGameBehaviour)
            {
                case SaveGameBehaviour.SaveDisabled:
                if (!saveDisabled)
                {
                    Debug.Log("Savegame is disabled.");
                    saveDisabled = true;
                }
                break;

                case SaveGameBehaviour.SaveEnabled:
                if (saveDisabled)
                {
                    Debug.Log("Savegame is enabled.");
                    saveDisabled = false;

                    // First save hacks - do delayed save instead of immediate.
                    if (!saveGameExists)
                    {
                        Debug.Log("First savegame, delaying save.");

                        saveOnThisFrame = false;
                        nextAutoSaveTime = Time.realtimeSinceStartup + firstSaveDelay;
                    }
                }
                break;

                case SaveGameBehaviour.SaveForbidden:
                if (!saveForbiddenOnCriticalError)
                {
                    Debug.LogWarning("Disabling Savegame in this session!");
                    saveOnThisFrame = false;
                    saveForbiddenOnCriticalError = true;
                }
                break;
            }
        }

        public void ScheduleSave(ScheduledSaveType scheduledSaveType)
        {
#if DEBUG
            if (dbgNoSave)
            {
                Debug.LogFormat("> Debug NO-SAVE option is set, savegame save request is ignored!");
                return;
            }
#endif

            switch (scheduledSaveType)
            {
                case ScheduledSaveType.SaveOnThisFrame:
                if (!saveForbiddenOnCriticalError && saveGameExists)
                {
                    saveOnThisFrame = true;
                }
                break;

                case ScheduledSaveType.SaveSoon:
                if (!saveForbiddenOnCriticalError && nextAutoSaveTime == 0)
                {
                    nextAutoSaveTime = Time.realtimeSinceStartup + saveSoonDelay;
                }
                break;
            }
        }

        public bool ImmediateSaveTransaction(bool onlyIfSaveQueued, Func<bool> performAction, Action rollbackAction = null)
        {
#if DEBUG
            if (dbgNoSave && !onlyIfSaveQueued)
            {
                Debug.LogFormat("> Debug NO-SAVE option is set, savegame save request is ignored!");
                return false;
            }
#endif

            if (saveForbiddenOnCriticalError)
            {
                Debug.Log("Cannot save game: game saving is forbidden in this session, this cannot be undone! Game paused.");
                return false;
            }

            if (currentOperation != CurrentOperation.None)
            {
                Debug.LogFormat("Cannot save game: other operation ({0}) is in progress!", currentOperation);
                return false;
            }

            if (saveDisabled && !onlyIfSaveQueued)
            {
                Debug.Log("Cannot perform immediate game saving: game saving is disabled now! Game paused.");
                Debug.Break();
                return false;
            }

            if ((performAction == null || performAction()))
            {
                if (onlyIfSaveQueued)
                {
                    if (saveDisabled || (!saveOnThisFrame && nextAutoSaveTime == 0))
                    {
                        return true;
                    }
                }

                currentOperation = CurrentOperation.Saving;
                var saveResult = DoSave(onlyIfSaveQueued ? SaveType.ImmediateOptional : SaveType.Immediate);
                currentOperation = CurrentOperation.None;
                if (saveResult)
                {
                    saveOnThisFrame = false;
                    nextAutoSaveTime = 0;

                    if (!saveGameExists)
                    {
                        saveGameExists = true;

                        PersistentCore.instance.NotifySaveGameCreated();
                        onSaveGameCreated?.Invoke();
                    }

                    PersistentCore.instance.networkController.NotifySaveGameUpdated(true);

                    return true;
                }
                else
                {
                    if (rollbackAction != null)
                        rollbackAction();
                    return false;
                }
            }
            return false;
        }
        #endregion

        #region Private functions.
        private byte[] EKBytes (string resourceFileName)
        {
            var saveKeyResource = Resources.Load(resourceFileName) as TextAsset;
            return (saveKeyResource == null || saveKeyResource.bytes == null || saveKeyResource.bytes.Length < 8) ? null : saveKeyResource.bytes;
        }

        private LoadResult LoadFromFile(NetworkFileTimeStamps networkFileTimeStamps = null, bool ignoreTimePassed = false)
        {
            if (saveFileName == null || saveFileName == string.Empty)
                return LoadResult.Failed;

#if DEBUG
            if (Debug.isDebugBuild)
            {
                string debugFileName = string.Format(debugLoadedFileNameTemplate, saveFileName);

                try
                {
                    var fi = new FileInfo(saveFileName);
                    if (fi.Exists && fi.Length > encKeySize / 8)
                    {
                        using (var fileStream = File.OpenRead(saveFileName))
                        {
                            using (var cStream = EH.CD(ekBytes, fileStream, encKeySize))
                            {
                                using (var lz4Stream = new LZ4Stream(cStream, LZ4StreamMode.Decompress, LZ4StreamFlags.Default))
                                {
                                    using (var jsonReader = new BsonReader(lz4Stream, false, DateTimeKind.Utc))
                                    {
                                        DebugDumpJsonReaderToFile(jsonReader, debugFileName);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogWarningFormat("Failed to dump loaded file in JSON format to file '{0}', Exception:\n\n{1}", debugFileName, ex);
                }
            }
#endif

            try
            {
                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    persistent.OnPreLoad();
                }

                var saveFileInfo = new FileInfo(saveFileName);
                var saveTimeStamp = saveFileInfo.LastWriteTimeUtc.ToUniversalTime();
                if (!saveFileInfo.Exists || saveFileInfo.Length <= encKeySize / 8)
                    return LoadResult.Skipped;

                if (networkFileTimeStamps != null && saveFileInfo.LastWriteTimeUtc != networkFileTimeStamps.fileTimeStamp)
                {
                    // File modification time doesn't match time stamps, so it's possibly updated.
                    Debug.LogWarningFormat("File modification time doesn't match provided network time stamps, so it's possibly updated outside of game session! File modification time: {0}, Time stamp: {1}"
                        , saveFileInfo.LastWriteTimeUtc, networkFileTimeStamps.fileTimeStamp);
                }
                    
                using (var fileStream = saveFileInfo.OpenRead())
                {
                    using (var cStream = EH.CD(ekBytes, fileStream, encKeySize))
                    {
                        using (var lz4Stream = new LZ4Stream(cStream, LZ4StreamMode.Decompress, LZ4StreamFlags.Default))
                        {
                            using (var jsonReader = new BsonReader(lz4Stream, false, DateTimeKind.Utc))
                            {
                                return DoLoad(jsonReader, saveTimeStamp, saveFileName, networkFileTimeStamps, ignoreTimePassed);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (Application.isEditor)
                {
                    saveForbiddenOnCriticalError = true;
                    Debug.LogErrorFormat("Got exception while loading SaveGame from file '{0}'!\nGAME SAVING IS FORBIDDEN IN THIS PLAY SESSION!!! See exception below and fix errors or remove your save game manually!\n\n{1}"
                        , saveFileName, ex);
                }
                else
                {
                    Debug.LogErrorFormat("Got exception while loading SaveGame from file '{0}'!\nSee exception below and fix errors or remove your save game manually!\n\n{1}"
                        , saveFileName, ex);
                }
            }

            return LoadResult.Failed;
        }

        private LoadResult LoadFromResource()
        {
            var saveResName = ResourcePaths.Persistent.saveGamesResFolder + ResourcePaths.Persistent.saveGameDemoResource;

            try
            {
                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    persistent.OnPreLoad();
                }

                var saveResource = Resources.Load(saveResName) as TextAsset;

                if (saveResource == null)
                {
                    Debug.LogErrorFormat("Cannot load SaveGame from resource '{0}', missing TextAsset resource."
                        , saveResName);

                    return LoadResult.Failed;
                }

                using (var stringReader = new StringReader(saveResource.text))
                {
                    using (var jsonReader = new JsonTextReader(stringReader))
                    {
                        return DoLoad(jsonReader, DateTime.UtcNow, saveResName, null, true);
                    }
                }
            }
            catch (Exception ex)
            {
                if (Application.isEditor)
                {
                    saveForbiddenOnCriticalError = true;
                    Debug.LogErrorFormat("Got exception while loading SaveGame from resource '{0}'!\nGAME SAVING IS FORBIDDEN IN THIS PLAY SESSION!!! See exception below and fix errors or remove your save game manually!\n\n{1}"
                        , saveResName, ex);
                }
                else
                {
                    Debug.LogErrorFormat("Got exception while loading SaveGame from resource '{0}'!\nSee exception below and fix errors or remove your save game manually!\n\n{1}"
                        , saveResName, ex);
                }
            }

            return LoadResult.Failed;
        }

        private LoadResult DoLoad
            ( JsonReader jsonReader
            , DateTime saveModificationTime
            , string fileOrResourceName
            , NetworkFileTimeStamps networkFileTimeStamps
            , bool ignoreTimePassed )
        {
            if (persistentList == null || persistentList.Length == 0)
                return LoadResult.Failed;

            SaveGameDataHeader saveHeader;

            bool validSaveVersion = false;
            bool deserializationOK = true;

            var serializer = JsonSerializer.Create
                    (new JsonSerializerSettings
                    {
                        ContractResolver = SaveGameContractResolver.Instance,
                        Context = new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence, this)
                    });

            SetCommonSerializerOptions(serializer);

            if (jsonReader.Read() && jsonReader.TokenType == JsonToken.StartObject)
            {
                if (jsonReader.Read() && jsonReader.TokenType == JsonToken.PropertyName && (string)(jsonReader.Value) == "header" && jsonReader.Read())
                {
                    saveHeader = (SaveGameDataHeader)serializer.Deserialize(jsonReader, typeof(SaveGameDataHeader));

                    validSaveVersion = (saveHeader.serializationVersion >= Data.Serialization.minSupportedSerializationVersion &&
                                        saveHeader.serializationVersion <= Data.Serialization.currentSerializationVersion);

                    if (validSaveVersion)
                    {
                        while (jsonReader.Read() && jsonReader.TokenType == JsonToken.PropertyName)
                        {
                            string propertyName = (string)jsonReader.Value;

                            bool foundHandler = false;
                            foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                            {
                                var persistentPropertyName = persistent.propertyName;
                                var persistentPropertyType = persistent.propertyType;

                                if (persistentPropertyType != null && propertyName == persistent.propertyName)
                                {
                                    foundHandler = true;
                                    if (jsonReader.Read())
                                    {
                                        var obj = serializer.Deserialize(jsonReader, persistent.propertyType);
                                        if (obj != null)
                                        {
                                            /*
                                            if (obj.GetType().IsInstanceOfType(persistent.propertyType))
                                            {
                                                Debug.LogErrorFormat("SaveGame deserialize type inconsistency : {0}, {1}", obj.GetType().ToString(), persistent.propertyType.ToString());
                                            }*/

                                            if (!persistent.Deserialize(obj))
                                            {
                                                deserializationOK = false;
                                            }
                                        }
                                        else
                                        {
                                            persistent.InitDefaultData();
                                        }
                                    }
                                    else
                                    {
                                        deserializationOK = false;
                                    }
                                    break;
                                }
                            }

                            if (!deserializationOK)
                                break;

                            if (!foundHandler)
                            {
                                jsonReader.Skip();
                            }
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat("Loading game from file/resource '{0}' failed. Unsupported savegame version (minimum supported version is {1}, current version is {2}, got {3})."
                            , fileOrResourceName
                            , Data.Serialization.minSupportedSerializationVersion
                            , Data.Serialization.currentSerializationVersion
                            , saveHeader.serializationVersion);
                    }

                    var loadOK = (validSaveVersion && deserializationOK);
                    foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                    {
                        if (!loadOK)
                            persistent.InitDefaultData();
                        persistent.OnLoaded(loadOK);
                    }
                }
                else
                {
                    Debug.LogErrorFormat("Loading game from file/resource '{0}' failed. Failed to read savegame header data.", fileOrResourceName);
                    return LoadResult.Failed;
                }
            }
            else
            {
                Debug.LogErrorFormat("Loading game from file/resource '{0}' failed. Invalid start of savegame file.", fileOrResourceName);
                return LoadResult.Failed;
            }

            if (validSaveVersion && deserializationOK)
            {
                var timePassed = TimeSpan.Zero;

                var timeUTCNow = DateTime.UtcNow;

                if (!ignoreTimePassed)
                {
                    if (networkFileTimeStamps != null && networkFileTimeStamps.fileTimeStamp != saveModificationTime)
                    {
                        Debug.LogWarningFormat("Network file time stamps present but modification time is different, will be ignored. Stamp time: {0}, file modification time: {1}"
                            , networkFileTimeStamps.fileTimeStamp, saveModificationTime);
                    }

                    if (networkFileTimeStamps != null && networkFileTimeStamps.fileTimeStamp == saveModificationTime)
                    {
                        var remoteTimeDeltaUpload = networkFileTimeStamps.uploadClientTimeStamp - saveHeader.saveDate;
                        if (remoteTimeDeltaUpload < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat("Remote file 'save to upload' time delta is negative, using zero time difference! Upload client's time: {0}, save game client's time: {1}, delta: {2}."
                                , networkFileTimeStamps.uploadClientTimeStamp, saveHeader.saveDate, remoteTimeDeltaUpload);
                            remoteTimeDeltaUpload = TimeSpan.Zero;
                        }

                        var serverTimeDelta = networkFileTimeStamps.downloadServerTimeStamp - networkFileTimeStamps.uploadServerTimeStamp;
                        if (serverTimeDelta < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat("Remote file 'server upload to download' time delta is negative, using zero time difference! Upload server's time: {0}, Download server's time: {1}, delta: {2}."
                                , networkFileTimeStamps.uploadServerTimeStamp, networkFileTimeStamps.downloadServerTimeStamp, serverTimeDelta);

                            serverTimeDelta = TimeSpan.Zero;
                        }

                        var remoteTimeDeltaDownload = timeUTCNow - networkFileTimeStamps.downloadClientTimeStamp;
                        if (remoteTimeDeltaDownload < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat("Remote file 'download to now' time delta is negative, using zero time difference! Download local time: {0}, Time now: {1}, delta: {2}."
                                , networkFileTimeStamps.downloadClientTimeStamp, timeUTCNow, remoteTimeDeltaDownload);
                            remoteTimeDeltaDownload = TimeSpan.Zero;
                        }

                        timePassed = remoteTimeDeltaUpload + serverTimeDelta + remoteTimeDeltaDownload;

                        Debug.LogFormat("Using remote file (downloaded) time differences. Save to upload delta: {0}, Server upload to download delta: {1}, Download to now delta: {2}, Total delta: {3}."
                            , remoteTimeDeltaUpload, serverTimeDelta, remoteTimeDeltaDownload, timePassed);
                    }
                    else
                    {
                        timePassed = timeUTCNow - saveHeader.saveDate;

                        if (timePassed < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat("Time delta is negative, using zero time difference! Time now: {0}, save game time: {1}, delta: {2}."
                                , timeUTCNow, saveHeader.saveDate, timePassed);
                            timePassed = TimeSpan.Zero;
                        }
                        else
                        {
                            Debug.LogFormat("Using local time difference. Time now: {0}, save game time: {1}, delta: {2}."
                                , timeUTCNow, saveHeader.saveDate, timePassed);
                        }
                    }
                }
                else
                {
                    Debug.Log("Time passed ignored for this save, it's zero.");
                }

                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    persistent.OnPostLoad(timePassed, timeUTCNow);
                }
            }

            var loaded = (validSaveVersion && deserializationOK);
            if (loaded)
            {
                Debug.LogFormat("Game Loaded from file/resource '{0}'.", fileOrResourceName);
            }
            else
            {
                Debug.LogErrorFormat("Loading game from file/resource '{0}' failed. Some error occurred on deserialization.", fileOrResourceName);
            }

            lastSaveFrame = Time.frameCount;
            return loaded ? LoadResult.Success : LoadResult.Failed;
        }

        private bool DoSave(SaveType saveType)
        {
#if DEBUG
            if (dbgNoSave)
            {
                Debug.LogFormat("> Debug NO-SAVE option is set, savegame save request is ignored!");
                return true;
            }
#endif

            if (saveFileName == null || saveFileName == string.Empty || persistentList == null || persistentList.Length == 0)
                return false;

            if (ekBytes == null)
                return false;

            var timeUtcNow = DateTime.UtcNow;

            SaveGameDataHeader saveHeader = new SaveGameDataHeader (timeUtcNow, Data.Serialization.currentSerializationVersion);

            // Save BSON (Binary JSON) version as a main save file.
            try
            {
                SynchronousSave(saveHeader, saveFileName);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Saving game to file '{0}' failed.\n{1}", saveFileName, ex);

                if (saveType == SaveType.FrameSave)
                {   // On error replace frame save with "AutoSave" so it doesn't retry each frame.
                    saveOnThisFrame = false;
                    nextAutoSaveTime = Time.realtimeSinceStartup + saveSoonDelay;
                }

                var prevSaveDisabledState = saveDisabled;
                saveDisabled = true;

                PersistentCore.instance.analytics.GameEvent("SaveGame Saving Failed", "Error", ex.Message);

                PersistentCore.instance.QueueModalConfirmMessage
                    ( Loc.Get("SaveGameSavingFailedHeader")
                    , Loc.Get("SaveGameSavingFailedMessage")
                    , () =>
                    {
                        saveDisabled = prevSaveDisabledState;

                        // Do nothing, we will try next time.
                    }
                    , Loc.Get("idRetry")
                    , () =>
                    {
                        PersistentCore.Quit(true);
                    }
                    , Loc.Get("idQuitGame"));

                return false;
            }

#if DEBUG
            // Also save text JSON version for debug purposes on debug builds. No backups, no error handling.
            if (Debug.isDebugBuild)
            {
                string debugFileName = string.Format(debugFileNameTemplate, saveFileName);

                try
                {
                    using (var writer = File.CreateText(debugFileName))
                    {
                        using (var jsonWriter = new JsonTextWriter(writer))
                        {
                            DoSerialize(jsonWriter, this, saveHeader);
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
#endif

            lastSaveFrame = Time.frameCount;
            nextAutoSaveTime = 0;

            Debug.LogFormat ("Game Saved: '{0}'. Iteration: {1}, Frame: {2}, Time spent: {3}, Save type: {4}."
                , saveFileName
                , ++dbgSaveIteration
                , Time.frameCount
                , (DateTime.UtcNow - timeUtcNow).TotalSeconds
                , saveType);
            return true;
        }

        private void CommitFiles(string fileName, string tmpFileName)
        {
            string backupFileName = string.Format(backupFileNameTemplate, fileName);

            File.Delete(backupFileName);
            if (File.Exists(fileName))
                File.Move(fileName, backupFileName);

            try
            {
                File.Move(tmpFileName, fileName);
            }
            catch (Exception)
            {
                if (File.Exists(backupFileName))
                    File.Move(backupFileName, fileName); // Try to move old save file back.
                throw;
            }
        }

        private void SynchronousSave(SaveGameDataHeader saveHeader, string fileName)
        {
            string tmpFileName = string.Format(tempFileNameTemplate, fileName);

            using (var fileStream = File.Create(tmpFileName))
            {
                using (var cStream = EH.CE(ekBytes, fileStream, encKeySize))
                {
                    using (var lz4Stream = new LZ4Stream(cStream, LZ4StreamMode.Compress, LZ4StreamFlags.HighCompression))
                    {
                        using (var jsonWriter = new BsonWriter(lz4Stream))
                        {
                            jsonWriter.DateTimeKindHandling = DateTimeKind.Utc;

                            DoSerialize(jsonWriter, this, saveHeader);
                        }
                    }
                }
            }

            CommitFiles(fileName, tmpFileName);
        }

        private void DoSerialize(JsonWriter jsonWriter, object contextObject, SaveGameDataHeader saveHeader)
        {
            jsonWriter.Formatting = Formatting.Indented;

            var serializer = JsonSerializer.Create
                (new JsonSerializerSettings
                {
                    ContractResolver = SaveGameContractResolver.Instance,
                    Context = new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence, contextObject)
                });

            SetCommonSerializerOptions(serializer);

            jsonWriter.WriteStartObject();
            {
                jsonWriter.WritePropertyName("header");
                serializer.Serialize(jsonWriter, saveHeader);

                foreach (var persistent in PersistentMultiIterator.Iterator(persistentList))
                {
                    var persistentPropertyName = persistent.propertyName;
                    var persistentPropertyType = persistent.propertyType;

                    if (persistentPropertyType != null && persistentPropertyName != null && persistentPropertyName != string.Empty)
                    {
                        var serializeData = persistent.Serialize();
                        if (serializeData != null)
                        {
                            jsonWriter.WritePropertyName(persistentPropertyName);
                            serializer.Serialize(jsonWriter, serializeData);
                        }
                    }
                }
            }
            jsonWriter.WriteEndObject();
        }

        private void SetCommonSerializerOptions(JsonSerializer serializer)
        {
            serializer.ReferenceResolver = new SimpleReferenceResolver();
            serializer.Converters.Add(isoDateTimeConverter);
            serializer.Converters.Add(stringEnumConverter);
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.DefaultValueHandling = DefaultValueHandling.Include | DefaultValueHandling.Populate;
            serializer.MissingMemberHandling = MissingMemberHandling.Ignore;
            // serializer.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            serializer.TypeNameHandling = TypeNameHandling.Auto;
            serializer.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            serializer.Culture = CultureInfo.InvariantCulture;
            serializer.FloatParseHandling = FloatParseHandling.Double;
        }

#if DEBUG
        private bool DebugDumpJsonReaderToFile(JsonReader jsonReader, string fileName)
        {
            Assert.IsNotNull(jsonReader);

            try
            {
                using (var writer = File.CreateText(fileName))
                {
                    using (var jsonWriter = new JsonTextWriter(writer))
                    {
                        jsonWriter.Formatting = Formatting.Indented;
                        jsonWriter.WriteToken(jsonReader);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Dumping JSON reader to file '{0}' failed.\n{2}", fileName, ex);
                return false;
            }
        }
#endif
        #endregion
    }
}