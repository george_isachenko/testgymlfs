﻿using System;

namespace Logic.Serialization
{
    public enum SaveGameBehaviour
    {
        SaveDisabled,
        SaveEnabled,
        SaveForbidden       // Save forbidden in this session, cannot be reset after setting!
    }

    public enum ScheduledSaveType
    {
        SaveOnThisFrame,    // Save on this frame on LateUpdate() if not saved already on this frame by other means: immediate or save soon.
        SaveSoon,           // Save after some short cooldown (3-5 seconds) if not saved already by other means before this time passes.
    }

    public interface IPersistent
    {
        string propertyName
        {
            get;
        }

        Type propertyType
        {
            get;
        }

        bool Deserialize(object obj);
        object Serialize();

        void OnPreLoad ();
        void OnLoaded (bool success);
        void OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC);
        void OnLoadSkipped ();
        void InitDefaultData();
    }
}