﻿using System;

namespace Logic.Serialization
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class EnumAliasAttribute : Attribute
    {
        public EnumAliasAttribute(string alias)
        {
            Alias = alias;
        }

        public string Alias { get; set; }
    }
}