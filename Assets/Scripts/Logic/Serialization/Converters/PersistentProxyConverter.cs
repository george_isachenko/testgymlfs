﻿using System;
using System.Reflection;
using Newtonsoft.Json;

namespace Logic.Serialization.Converters
{
    public class PersistentProxyConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Type objectType = value.GetType();
            var ppa = Attribute.GetCustomAttribute(objectType, typeof(PersistentProxyAttribute), true) as PersistentProxyAttribute;
            if (ppa != null && ppa.persistentFieldName != null && ppa.persistentFieldName != string.Empty)
            {
                var persistentField = objectType.GetField(ppa.persistentFieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
                if (persistentField != null)
                {
                    var serializedData = persistentField.GetValue(value);
                    if (serializedData != null || serializer.NullValueHandling == NullValueHandling.Include)
                    {
                        serializer.Serialize (writer, serializedData );
                    }
                }
                else
                {
                    throw new InvalidOperationException
                        (string.Format(@"{0}: Object of type ""{1}"" is missing field named ""{2}""."
                            , GetType().Name, objectType.Name, ppa.persistentFieldName));
                }
            }
            else
            {
                throw new InvalidOperationException
                    (string.Format(@"{0}: Missing [PersistentData] attribute or PersistentDataAttribute.persistentFieldName field is empty string."
                        , GetType().Name));
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var ppa = Attribute.GetCustomAttribute(objectType, typeof(PersistentProxyAttribute), true) as PersistentProxyAttribute;
            if (ppa != null && ppa.persistentFieldName != null && ppa.persistentFieldName != string.Empty)
            {
                var persistentField = objectType.GetField(ppa.persistentFieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
                if (persistentField != null)
                {
                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        var serializedData = serializer.Deserialize (reader, objectType);

                        var obj = (existingValue != null) ? existingValue : Activator.CreateInstance(objectType);
                        persistentField.SetValue (obj, serializedData);

                        return obj;
                    }
                    else
                    {
                        throw new InvalidOperationException
                            (string.Format(@"{0}: Object expected as a value of property named ""{1}"" when reading object of type ""{2}""."
                                , GetType().Name, ppa.persistentFieldName, objectType.Name));
                    }
                }
                else
                {
                    throw new InvalidOperationException
                        (string.Format(@"{0}: Object of type ""{1}"" is missing field named ""{2}""."
                            , GetType().Name, objectType.Name, ppa.persistentFieldName));
                }
            }
            else
            {
                throw new InvalidOperationException
                    (string.Format(@"{0}: Missing [PersistentData] attribute or PersistentDataAttribute.persistentFieldName field is empty string."
                        , GetType().Name));
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return Attribute.IsDefined(objectType, typeof(PersistentProxyAttribute));
        }
    }
}