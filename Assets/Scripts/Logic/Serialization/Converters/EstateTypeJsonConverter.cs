﻿using System;
using Data;
using Data.Estate;
using Newtonsoft.Json;
using UnityEngine;

namespace Logic.Serialization.Converters
{
    public class EstateTypeJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            EstateType estateType = value as EstateType;

            if (estateType != null)
            {
                writer.WriteValue (estateType.id);
            }
            else
            {
                writer.WriteNull();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Integer)
            {
                int estateTypeId = Convert.ToInt32 (reader.Value);
                
                if (serializer.Context.Context != null && serializer.Context.Context is ISaveGameConverterProvider)
                {
                    var saveGameProvider = serializer.Context.Context as ISaveGameConverterProvider;
                    var result = saveGameProvider.estateTypeProvider.GetEstateType(estateTypeId);
                    if (result != null)
                        return result;

                    throw new JsonReaderException(string.Format("Cannot find Estate Type with id: {0}.", estateTypeId));
                }
                else
                {
                    throw new JsonReaderException(string.Format("Wrong serializer context: {0}.", serializer.Context.Context));
                }
            }
            else if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }
            else
            {
                throw new JsonReaderException(string.Format("Unexpected token type: {0}.", reader.TokenType));
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(EstateType);
        }
    }
}