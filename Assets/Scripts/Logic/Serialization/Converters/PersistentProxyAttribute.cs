﻿using System;

namespace Logic.Serialization.Converters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
    public class PersistentProxyAttribute : Attribute
    {
        public string persistentFieldName { get; set; }

        public PersistentProxyAttribute(string persistentFieldName)
        {
            this.persistentFieldName = persistentFieldName;
        }
    }
}