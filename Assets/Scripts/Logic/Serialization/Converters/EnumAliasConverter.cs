﻿using UnityEngine;
using System;
using Newtonsoft.Json;

namespace Logic.Serialization.Converters
{
    public class EnumAliasConverter : JsonConverter
    {
        #region Public virtual API.
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (serializer.Context.Context != null &&
                serializer.Context.Context is ISaveGameConverterProvider)
            {
                if (value.GetType().IsEnum)
                {
                    var saveGameProvider = serializer.Context.Context as ISaveGameConverterProvider;
                    saveGameProvider.defaultEnumConverter.WriteJson(writer, value, serializer);
                }
                else
                {
                    throw new JsonWriterException(string.Format("Passed value is not an enum type: {0}", value.GetType().FullName));
                }
            }
            else
            {
                throw new JsonWriterException("JsonSerializer context object is not an ISaveGameConverterProvider.");
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (serializer.Context.Context != null &&
                serializer.Context.Context is ISaveGameConverterProvider)
            {
                if (reader.TokenType != JsonToken.String)
                    throw new JsonReaderException(string.Format("Unexpected JSON token type: {0}.", reader.TokenType));
                    
                if (!objectType.IsEnum)
                    throw new JsonReaderException(string.Format("Object type '{0}' is not an enum type!", objectType.FullName));


                var enumFields = objectType.GetFields();

                foreach (var enumField in enumFields)
                {
                    var enumAliasAttribute = (EnumAliasAttribute)Attribute.GetCustomAttribute(enumField, typeof(EnumAliasAttribute), true);
                    if (enumAliasAttribute != null && enumAliasAttribute.Alias != null && enumAliasAttribute.Alias != string.Empty)
                    {
                        if ((reader.Value as string) == enumAliasAttribute.Alias)
                        {
                            var result = Enum.Parse (objectType, enumField.Name, false);
                            return result;
                        }
                    }
                }

                // Alias not found - use default enum converter.
                var saveGameProvider = serializer.Context.Context as ISaveGameConverterProvider;
                return saveGameProvider.defaultEnumConverter.ReadJson(reader, objectType, existingValue, serializer);
            }
            else
            {
                throw new JsonReaderException("JsonSerializer context object is not an ISaveGameConverterProvider.");
            }
        }

        public override bool CanConvert(Type objectType)
        {
            if (objectType.IsEnum)
            {
                // Check that at least one enum field has EnumAliasAttribute assigned.
                var enumFields = objectType.GetFields();

                foreach (var enumField in enumFields)
                {
                    var enumAliasAttribute = (EnumAliasAttribute)Attribute.GetCustomAttribute(enumField, typeof(EnumAliasAttribute), true);
                    if (enumAliasAttribute != null && enumAliasAttribute.Alias != null && enumAliasAttribute.Alias != string.Empty)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion
    }
}