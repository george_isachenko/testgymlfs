﻿using Newtonsoft.Json;

namespace Logic.Serialization
{
    public interface ISaveGameConverterProvider
    {
        IExerciseProvider exerciseProvider { get; }
        IEstateTypeProvider estateTypeProvider { get; }
        JsonConverter defaultEnumConverter { get; }
    }
}