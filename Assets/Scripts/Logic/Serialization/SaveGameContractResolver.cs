﻿using System;
using Data;
using Logic.Serialization.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Logic.Serialization
{
    public class SaveGameContractResolver : DefaultContractResolver
    {
        private static SaveGameContractResolver _instance;
        public static IContractResolver Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SaveGameContractResolver();
                return _instance;
            }
        }

        public SaveGameContractResolver()
            : base()
        {
        }

        protected override JsonContract CreateContract (Type objectType)
        {
            if (Attribute.IsDefined(objectType, typeof(PersistentProxyAttribute), true) &&
                !Attribute.IsDefined(objectType, typeof(JsonConverterAttribute), true))
            {
                JsonContract contract = base.CreateContract(objectType);

                contract.Converter = new PersistentProxyConverter();

                return contract;
            }
            else
                return base.CreateContract(objectType);
        }
    }
}