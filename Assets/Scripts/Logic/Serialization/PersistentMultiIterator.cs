﻿using System.Collections.Generic;

namespace Logic.Serialization
{
    public static class PersistentMultiIterator
    {
        public static IEnumerable<IPersistent> Iterator (IPersistent[] persistentList)
        {
            if (persistentList != null)
            {
                foreach (var persistent in persistentList)
                {
                    if (persistent is IPersistentMulti)
                    {
                        var persistentMulti = persistent as IPersistentMulti;
                        persistentMulti.Reset();

                        do
                        {
                            yield return persistentMulti;
                        } while (persistentMulti.MoveNext());
                    }
                    else
                        yield return persistent;
                }
            }
            else
                yield break;
        }
    }
}