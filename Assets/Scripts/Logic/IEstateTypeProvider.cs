﻿using Data.Estate;

namespace Logic
{
    public interface IEstateTypeProvider
    {
        EstateType GetEstateType (int id);
    }
}