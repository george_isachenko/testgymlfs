﻿using System;
using System.Linq;
using Core;
using Data;
using Data.Estate;
using Data.Room;
using Logic.Persons;
using Logic.Quests.SportQuest.SportLaneQuest;
using Presentation.Facades;
using UnityEngine;
using Logic.FacadesBase;
using BridgeBinder;
using Data.Person;

namespace Logic.Notifications.Facade
{
    [BindBridgeInterface(typeof (PresentationNotifications))]
    [AddComponentMenu("Kingdom/Logic/LogicNotifications")]
    public class LogicNotifications :
        BaseLogicFacade
    {
        #region Private data.
        PresentationNotifications presenter;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected virtual void Subscribe(PresentationNotifications presenter)
        {
            this.presenter = presenter;
        }

        [BindBridgeUnsubscribe]
        protected virtual void Unsubscribe(PresentationNotifications presenter)
        {
            this.presenter = null;
        }
        #endregion

        #region Unity API.
        void Start()
        {
            presenter.ClearLocalNotifications();
        }

/*
// For debugging only.
#if UNITY_EDITOR
        private void OnApplicationFocus (bool focus)
        {
            OnApplicationPause(!focus);
        }
#endif
*/

        void OnApplicationPause (bool pause)
        {
            if (PersistentCore.globalState == GlobalState.Game && gameCore != null && gameCore.isGameStarted)
            {
                presenter.ClearLocalNotifications();

                if (pause)
                {
                    UpdateVisitorsFinishedExercises();
                    UpdateEquipmentAssemblyComplete();
                    UpdateDailyQuest();
                    UpdateSportResourcesCooldownFinished();
                    UpdateSportsmenFinishedExercises();
                    UpdateLaneQuestCooldownFinished();
                    UpdateLaneQuestAlmostFinished();
                    UpdateSportClubUnlock();
                    UpdateJuiceBarUnlock();
                    UpdateJuiceBarOrders();
                    UpdateDailyActive();
                }
            }
        }
        #endregion

        #region Private methods.
        private void UpdateVisitorsFinishedExercises ()
        {
            try
            {
                if (gameCore.personManager != null && gameCore.personManager.persons != null)
                {
                    var visitorsExerciseEndTimes = gameCore.personManager.persons.Values
                        .Where(p => p is PersonVisitor)
                        .Select(p => p.exerciseEndTime);

                    if (visitorsExerciseEndTimes.Any())
                    {
                        var maxVisitorQuestFinishTime = visitorsExerciseEndTimes.Max();

                        if (maxVisitorQuestFinishTime > DateTime.UtcNow)
                        {
                            presenter.SetNotification("VisitorsFinishedExercises", maxVisitorQuestFinishTime, true);
                            return;
                        }
                    }
                }

                presenter.CancelLocalNotification("VisitorsFinishedExercises", true, true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'VisitorsFinishedExercises' notification: {0}.", ex);
            }
        }

        private void UpdateEquipmentAssemblyComplete ()
        {
            try
            {
                if (gameCore.equipmentManager != null && gameCore.equipmentManager.equipment != null)
                {
                    var equipmentAssemblingEndTimes = gameCore.equipmentManager.equipment.Values
                        .Where(e => e.estateType.itemType == EstateType.Subtype.Training && e.state == EquipmentState.Assembling)
                        .Select(e => e.assemblyEndTime);

                    if (equipmentAssemblingEndTimes.Any())
                    {
                        var maxEquipAssemblyTime = equipmentAssemblingEndTimes.Max();

                        if (maxEquipAssemblyTime > DateTime.UtcNow)
                        {
                            presenter.SetNotification("EquipmentAssemblyComplete", maxEquipAssemblyTime, true);
                            return;
                        }
                    }
                }

                presenter.CancelLocalNotification("EquipmentAssemblyComplete", true, true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'EquipmentAssemblyComplete' notification: {0}.", ex);
            }
        }

        private void UpdateDailyQuest ()
        {
            try
            {
                if (gameCore.quests != null && gameCore.quests.dailyQuests != null && gameCore.quests.dailyQuests.quests != null)
                {
                    var dailyQuestsTimers = gameCore.quests.dailyQuests.quests
                        .Where (e => e.timer != null)
                        .Select(e => e.timer);

                    if (dailyQuestsTimers.Any())
                    {
                        var maxDailyQuestFinishTime = dailyQuestsTimers.Max(t => t.secondsToFinish);

                        if (maxDailyQuestFinishTime > 0)
                        {
                            presenter.SetNotification("DailyQuest", DateTime.UtcNow + TimeSpan.FromSeconds(maxDailyQuestFinishTime), true);
                            return;
                        }
                    }
                }

                presenter.CancelLocalNotification("DailyQuest", true, true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'DailyQuest' notification: {0}.", ex);
            }
        }

        private void UpdateSportResourcesCooldownFinished ()
        {
            try
            {
                if (gameCore.shopUltimate != null && gameCore.shopUltimate.itemsSportResources != null)
                {
                    var sportResourcesOnCooldownTimers = gameCore.shopUltimate.itemsSportResources
                        .Select (r => gameCore.shopUltimate.GetTimeForItem(r))
                        .Where  (t => t != null);

                    if (sportResourcesOnCooldownTimers.Any())
                    {
                        var maxSportResourceCooldownTime = sportResourcesOnCooldownTimers.Max(t => t.secondsToFinish);

                        if (maxSportResourceCooldownTime > 0)
                        {
                            presenter.SetNotification("SportResourcesCooldownFinished", DateTime.UtcNow + TimeSpan.FromSeconds(maxSportResourceCooldownTime), true);
                            return;
                        }
                    }
                }

                presenter.CancelLocalNotification("SportResourcesCooldownFinished", true, true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'SportResourcesCooldownFinished' notification: {0}.", ex);
            }
        }

        private void UpdateSportsmenFinishedExercises ()
        {
            try
            {
                if (gameCore.personManager != null && gameCore.personManager.sportsmen != null && gameCore.personManager.sportsmen.members != null)
                {
                    var sportsmenDoingExercise = gameCore.personManager.sportsmen.members
                        .Where(s => s.state == PersonState.Workout)
                        .OrderByDescending(s => s.exerciseEndTime);

                    if (sportsmenDoingExercise.Any())
                    {
                        var sportsmenWithMaxExerciseEndTime = sportsmenDoingExercise.First();

                        if (sportsmenWithMaxExerciseEndTime.exerciseEndTime > DateTime.UtcNow)
                        {
                            presenter.SetNotification("SportsmenFinishedExercises"
                                    , sportsmenWithMaxExerciseEndTime.exerciseEndTime
                                    , true
                                    , Loc.Get(DataTables.instance.sportsmen[sportsmenWithMaxExerciseEndTime.currentOrNextSportsmanType].locNameId));
                            return;
                        }
                    }
                }

                presenter.CancelLocalNotification("SportsmenFinishedExercises", true, true);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'SportsmenFinishedExercises' notification: {0}.", ex);
            }
        }

        private void UpdateLaneQuestCooldownFinished ()
        {
            if (gameCore.sportLaneQuest != null && gameCore.sportLaneQuest.currentLaneQuest != null)
            {
                var currentLaneQuest = gameCore.sportLaneQuest.currentLaneQuest;

                try
                {
                    if (currentLaneQuest.state == LaneQuestState.WaitForNewQuests && currentLaneQuest.currentStateTimer.secondsToFinish > 0)
                    {
                        presenter.SetNotification("LaneQuestCooldownFinished", DateTime.UtcNow + currentLaneQuest.currentStateTimer.timeSpan, true);
                        return;
                    }

                    presenter.CancelLocalNotification("LaneQuestCooldownFinished", true, true);
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Exception while trying to set 'LaneQuestCooldownFinished' notification: {0}.", ex);
                }
            }
        }

        private void UpdateLaneQuestAlmostFinished ()
        {
            if (gameCore.sportLaneQuest != null && gameCore.sportLaneQuest.currentLaneQuest != null)
            {
                var currentLaneQuest = gameCore.sportLaneQuest.currentLaneQuest;

                try
                {
                    // make prepare lane quest finish
                    var laneQuestAlmostFinishedTime = TimeSpan.FromHours(2.0f);
                    if (currentLaneQuest.state == LaneQuestState.ProgressAccumulation && currentLaneQuest.currentStateTimer.timeSpan > laneQuestAlmostFinishedTime)
                    {
                        presenter.SetNotification("LaneQuestAlmostFinished", DateTime.UtcNow + currentLaneQuest.currentStateTimer.timeSpan - laneQuestAlmostFinishedTime, true);
                        return;
                    }

                    presenter.CancelLocalNotification("LaneQuestAlmostFinished", true, true);
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Exception while trying to set 'LaneQuestAlmostFinished' notification: {0}.", ex);
                }
            }
        }

        private void UpdateSportClubUnlock ()
        {
            if (gameCore.roomManager != null)
            {
                // Sport Club or Sport Club room part unlocking finished
                try
                {
                    var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub);

                    if (roomName != null)
                    {
                        var roomUnlockTimer = gameCore.roomManager.GetUnlockTimer(roomName);

                        if (roomUnlockTimer != null && roomUnlockTimer.secondsToFinish > 0)
                        {
                            var expandStage = gameCore.roomManager.GetRoomExpandStage(roomName);
                            if (expandStage > 0)
                            {
                                presenter.SetNotification(string.Format("Unlocked{0}Room", roomName), DateTime.UtcNow + roomUnlockTimer.timeSpan, true
                                    , Loc.Get(string.Format("unlock{0}Room{1}Header", roomName, expandStage)));
                                return;
                            }
                            else
                            {
                                presenter.SetNotification(string.Format("Unlocked{0}", roomName), DateTime.UtcNow + roomUnlockTimer.timeSpan, true
                                    , Loc.Get(string.Format("unlock{0}Header", roomName)));
                                return;
                            }
                        }

                        presenter.CancelLocalNotification(string.Format("Unlocked{0}Room", roomName), true, true);
                        presenter.CancelLocalNotification(string.Format("Unlocked{0}", roomName), true, true);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Exception while trying to set Sport Club unlock notification: {0}.", ex);
                }
            }
        }

        private void UpdateJuiceBarUnlock ()
        {
            if (gameCore.roomManager != null)
            {
                // TODO: Awaits localization: DGUNITY-1852.
/*
                var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);

                try
                {
                    if (roomName != null)
                    {
                        var roomUnlockTimer = gameCore.roomManager.GetUnlockTimer(roomName);

                        if (roomUnlockTimer != null && roomUnlockTimer.secondsToFinish > 0)
                        {
                            presenter.SetNotification(string.Format("Unlocked{0}", roomName), DateTime.UtcNow + roomUnlockTimer.timeSpan, true
                                , Loc.Get(string.Format("unlock{0}Header", roomName)) );
                            return;
                        }

                        presenter.CancelLocalNotification(string.Format("Unlocked{0}", roomName), true, true);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Exception while trying to set '{0}' notification: {1}."
                        , string.Format("Unlocked{0}", roomName), ex);
                }
*/
            }
        }

        private void UpdateJuiceBarOrders ()
        {
            // TODO: Awaits localization: DGUNITY-1852.
/*
            try
            {
                if (gameCore.juiceBar != null && gameCore.juiceBar.canUseMyFeature.canUse &&
                    gameCore.juiceBar.orders != null && gameCore.juiceBar.orders.Count > 0)
                {
                    var juiceBarActiveOrderTimers = gameCore.juiceBar.orders
                        .Where(x => x.time != null)
                        .Select(x => x.time);

                    if (juiceBarActiveOrderTimers.Any())
                    {
                        var juiceBarActiveOrdersMaxEndTime = juiceBarActiveOrderTimers.Max(x => x.secondsToFinish);

                        if (juiceBarActiveOrdersMaxEndTime > 0)
                        {
                            presenter.SetNotification("JuiceBarAllOrdersComplete", DateTime.UtcNow + TimeSpan.FromSeconds(juiceBarActiveOrdersMaxEndTime), true);
                            return;
                        }

                        presenter.CancelLocalNotification("JuiceBarAllOrdersComplete", true, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'JuiceBarAllOrdersComplete' notification: {0}.", ex);
            }
*/
        }

        private void UpdateDailyActive ()
        {
            try
            {
                presenter.CancelLocalNotification("DailyActive", true, true);

                //presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromHours(2), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromHours(8), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(1), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(2), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(3), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(5), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(7), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(10), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(14), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(21), false);
                presenter.SetNotification("DailyActive", DateTime.UtcNow + TimeSpan.FromDays(30), false);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Exception while trying to set 'DailyActive' notification: {0}.", ex);
            }
        }
        #endregion
    }
}