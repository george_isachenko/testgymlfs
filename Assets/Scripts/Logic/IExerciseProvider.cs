﻿using Data;

namespace Logic
{
    public interface IExerciseProvider
    {
        Exercise GetExercise (int id);
    }
}