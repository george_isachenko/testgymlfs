﻿using System;
using System.Collections;
using Logic.Persons;
using Logic.Estate;
using Logic.Facades;
using Logic.PlayerProfile;
using Logic.Rooms;
using Logic.Serialization;
using Logic.Shop;
using Logic.Statistics;
using Logic.Shop.Facade;
using Logic.Quests.Facade;
using Logic.SportAgency.Facade;
using Logic.Quests.SportQuest.SportLaneQuest.Facade;
using Logic.JuiceBar.Facade;
using Logic.Trainers.Facade;

namespace Logic.Core
{
    public delegate void SaveGameCreatedEvent ();
    public delegate void WillRestartGameEvent (global::Core.GameMode mode, global::Core.DemoLevelSource demoLevelSource);
    public delegate void GameStartedEvent (bool isFreshInstall);

    public interface IGameCore
    {
        IPlayerProfileManager       playerProfile                   { get; }
        IPersonManager              personManager                   { get; }
        IEquipmentManager           equipmentManager                { get; }
        IRoomManager                roomManager                     { get; }
        IShopManager                shopManager                     { get; }
        LogicSportShop              sportShop                       { get; }
        LogicSportAgency            sportAgency                     { get; }
        LogicSportsmen              sportsmen                       { get; }
        LogicQuests                 quests                          { get; }
        LogicJuiceBar               juiceBar                        { get; }
        StatisticsLogic             statistics                      { get; }
        IEstateTypeProvider         estateTypeProvider              { get; }
        IExerciseProvider           exerciseProvider                { get; }
        IShopPurchaseProtocol       wallFloorShopPurchaseProtocol     { get; }
        IShopPurchaseProtocol       equipmentDecorShopPurchaseProtocol { get; }
        IShopPurchaseProtocol       iapShopPurchaseProtocol         { get; }
        SportLaneQuestLogic         sportLaneQuest                  { get; }
        SportTimeQuestLogic         sportTimeQuest                  { get; }
        SportsmanTrainingLogic      sportsmanTraining               { get; }
        LogicShop                   shop                            { get; } 
        LogicUltimateShop           shopUltimate                    { get; }
        LogicTrainers               trainers                        { get; }

        bool isSaveEnabled                                          { get; }
        bool isGameStarted                                          { get; }

        event                       SaveGameCreatedEvent                 onSaveGameCreated;
        event                       WillRestartGameEvent                 onWillRestartGame;
        event                       GameStartedEvent                     onGameStarted;

        void                        StartCoroutine (IEnumerator coroutine);
        void                        StopCoroutine (IEnumerator coroutine);
        void                        DelayedInvoke (Action action);

        void                        ChangeSaveGameBehaviour(SaveGameBehaviour saveGameBehaviour);
        void                        ScheduleSave(ScheduledSaveType scheduledSaveType = ScheduledSaveType.SaveOnThisFrame);
        bool                        ImmediateSaveTransaction(bool onlyIfSaveQueued, Func<bool> performAction, Action rollbackAction = null);

#if DEBUG
        void                        DebugDeleteDataAndStop(bool isCriticalError = false);
#endif
    }
}