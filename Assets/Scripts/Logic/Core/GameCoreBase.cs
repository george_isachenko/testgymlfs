﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Data;
using Data.Estate;
using Logic.Estate;
using Logic.Facades;
using Logic.JuiceBar.Facade;
using Logic.Persons;
using Logic.PlayerProfile;
using Logic.Quests.Facade;
using Logic.Quests.SportQuest.SportLaneQuest.Facade;
using Logic.Rooms;
using Logic.Serialization;
using Logic.Shop;
using Logic.Shop.Facade;
using Logic.SportAgency.Facade;
using Logic.Statistics;
using UnityEngine;
using UnityEngine.Assertions;
using View.CameraHelpers;
using Logic.Trainers.Facade;

namespace Logic.Core
{
    public abstract class GameCoreBase
        : MonoBehaviour
        , IGameCore
        , IExerciseProvider
        , IEstateTypeProvider
    {
        protected static GameCoreBase   instance;

        public IPlayerProfileManager    playerProfile                       { get; protected set; }
        public IPersonManager           personManager                       { get; protected set; }
        public IEquipmentManager        equipmentManager                    { get; protected set; }
        public IRoomManager             roomManager                         { get; protected set; }
        public IShopManager             shopManager                         { get; protected set; }
        public LogicSportShop           sportShop                           { get; protected set; }
        public LogicSportAgency         sportAgency                         { get; protected set; }
        public LogicSportsmen           sportsmen                           { get; protected set; }
        public LogicQuests              quests                              { get; protected set; }
        public LogicJuiceBar            juiceBar                            { get; protected set; }
        public SportLaneQuestLogic      sportLaneQuest                      { get; protected set; }
        public SportTimeQuestLogic      sportTimeQuest                      { get; protected set; }
        public SportsmanTrainingLogic   sportsmanTraining                   { get; protected set; }
        public LogicShop                shop                                { get; protected set; } 
        public LogicUltimateShop        shopUltimate                        { get; protected set; } 
        public LogicTrainers            trainers                            { get; protected set; } 

        public IEstateTypeProvider      estateTypeProvider                  { get { return this; } }
        public IExerciseProvider        exerciseProvider                    { get { return this; } }

        public IShopPurchaseProtocol    wallFloorShopPurchaseProtocol       { get { return roomManager.purchaseProtocol; } }
        public IShopPurchaseProtocol    equipmentDecorShopPurchaseProtocol  { get { return equipmentManager.purchaseProtocol; } }
        public IShopPurchaseProtocol    iapShopPurchaseProtocol             { get; protected set; }

        public StatisticsLogic          statistics                          { get; protected set; }

        public bool                     isGameStarted                       { get; protected set; }

        #region Public events.
        public event                    SaveGameCreatedEvent                    onSaveGameCreated;
        public event                    WillRestartGameEvent                    onWillRestartGame;
        public event                    GameStartedEvent                        onGameStarted;
        #endregion

        #region Private data.
        protected List<IEnumerator>     pendingJobs = new List<IEnumerator>(64);
        protected SaveGame              saveGame;
        #endregion

        #region Unity API
        protected void Awake()
        {
            instance = this;

//          Application.runInBackground = false;

            playerProfile = GetComponent<IPlayerProfileManager>();
            Assert.IsNotNull(playerProfile);

            personManager = GetComponent<IPersonManager>();
            Assert.IsNotNull(personManager);

            equipmentManager = GetComponent<IEquipmentManager>();
            Assert.IsNotNull(equipmentManager);

            roomManager = GetComponent<IRoomManager>();
            Assert.IsNotNull(roomManager);

            PersistentCore.instance.onWillRestartGame += OnWillRestartGame;
        }

        protected void OnDestroy()
        {
            PersistentCore.instance.onWillRestartGame -= OnWillRestartGame;
        }

        protected void OnApplicationQuit ()
        {
            Cleanup();
        }

        protected void Start()
        {
//          Application.runInBackground = false;
        }

        protected void LateUpdate()
        {
            Settings.TrySave();
        }
        #endregion

        #region Public static API.
        public static int GetEquipCount(int id, EstateType.Subtype type = EstateType.Subtype.Training)
        {
            if (id == -1)
                return instance.equipmentManager.GetEquipmentsCount(id, type);

            var table = DataTables.instance.estateTypes;

            if (table.ContainsKey(id))
            {
                var count = instance.equipmentManager.GetEquipmentsCount(id, type);
                return count;
            }

            return 0;
        }

        public static int GetRoomExpandStage(Data.Room.RoomType room)
        {
            if (instance != null)
                return instance.roomManager.GetRoomExpandStage(instance.roomManager.GetRoomNameOfType(room));

            Debug.LogError("GameCore::GetRoomExpandStage called while not initialized");

            return 0;
        }

        public static int GetRoomStyle()
        {
            if (instance != null)
            {
                var room = instance.roomManager.roomCapacity;
                var style = room.GetStyleLevel(room.GetTotalStylePoints(instance.roomManager.GetRoomNameOfType(Data.Room.RoomType.Club)));
                return style;
            }

            Debug.LogError("GameCore::GetRoomExpandStage called while not initialized");

            return 0;
        }

        public static int GetPlayerLevel()
        {
            if (instance != null)
                return instance.playerProfile.level;

            Debug.LogError("GameCore::GetPlayerLevel called while not initialized");

            return 1;
        }

        public static int GetDailyBonusDay()
        {
            if (instance != null)
            {
                var dailyBonus = instance.quests.dailyBonusNew;

                if (dailyBonus.IsAvailableByLevel())
                {
                    if (dailyBonus.accepedByUser)
                        return dailyBonus.currentDay + 1;
                    else
                        return dailyBonus.currentDay;
                }
                else
                    return 0;
            }

            Debug.LogError("GameCore::GetDailyBonusDay called while not initialized");

            return 1;
        }

        public static int GetStorageCapacity()
        {
            if (instance != null)
                return instance.playerProfile.storage.capacity;

            Debug.LogError("GameCore::GetStorageCapacity called while not initialized");

            return 1;
        }

        public static int GetSportsmenCapacity()
        {
            if (instance != null)
                return instance.sportsmen.GetSportsmenCapacity();

            Debug.LogError("GameCore::GetSportsmenCapacity called while not initialized");

            return 1;
        }

        public static bool GetTreeAvailability(int idx)
        {
            if (instance != null)
                return instance.juiceBar.IsTreeAvailable(idx);

            Debug.LogError("GameCore::GetTreeAvailability called while not initialized");

            return false;
        }
        #endregion

        #region IExerciseProvider API
        Exercise IExerciseProvider.GetExercise(int id)
        {
            Exercise result;
            return DataTables.instance.exercises.TryGetValue(id, out result) ? result : null;
        }
        #endregion

        #region IEstateTypeProvider API
        EstateType IEstateTypeProvider.GetEstateType(int id)
        {
            EstateType result;
            return DataTables.instance.estateTypes.TryGetValue(id, out result) ? result : null;
        }
        #endregion

        #region IGameCore API
        public abstract bool isSaveEnabled
        {
            get;
        }

        public abstract void ChangeSaveGameBehaviour(SaveGameBehaviour saveGameBehaviour);
        public abstract void ScheduleSave(ScheduledSaveType scheduledSaveType);
        public abstract bool ImmediateSaveTransaction(bool onlyIfSaveQueued, Func<bool> performAction, Action rollbackAction);

        void IGameCore.StartCoroutine(IEnumerator coroutine)
        {
            if (Application.isPlaying)
            {
                if (isGameStarted)
                {
                    this.StartCoroutine(coroutine);
                }
                else
                {
                    pendingJobs.Add(coroutine);
                }
            }
        }

        void IGameCore.StopCoroutine(IEnumerator coroutine)
        {
            if (isGameStarted)
            {
                this.StopCoroutine(coroutine);
            }
            else
            {
                pendingJobs.Remove(coroutine);
            }
        }

        void IGameCore.DelayedInvoke(Action action)
        {
            if (Application.isPlaying)
            {
                StartCoroutine(DelayedInvokeCoroutine(action));
            }
        }

#if DEBUG
        public void DebugDeleteDataAndStop(bool isCriticalError = false)
        {
            if (saveGame != null)
                saveGame.DebugDeleteSave();

            PersistentCore.instance.networkController.DebugResetNetworkData();
            PersistentCore.Quit(isCriticalError);
        }
#endif
        #endregion

        #region Protected API.
        protected void OnSaveGameCreated ()
        {
            onSaveGameCreated?.Invoke();
        }

        protected IEnumerator GameStartedJob (bool isFreshInstall)
        {
            yield return null;

            try
            {
                isGameStarted = true;

                foreach (var job in pendingJobs)
                {
                    this.StartCoroutine(job);
                }
                pendingJobs.Clear();

                onGameStarted?.Invoke(isFreshInstall);

                PersistentCore.instance.NotifyGameStarted(isFreshInstall, () =>
                {
                    if (CameraController.instance != null)
                    {
                        CameraController.instance.enabled = true;
                    }
                    else
                    {
                        Debug.LogError("No CameraController instance found at the start of game!");
                    }
                });
            }
            catch (Exception ex)
            {
            	Debug.LogException(ex);
            }
        }
        #endregion

        #region Private API
        private IEnumerator DelayedInvokeCoroutine(Action action)
        {
            yield return null;

            action?.Invoke();
        }

        private void OnWillRestartGame (GameMode mode, DemoLevelSource demoLevelSource)
        {
            onWillRestartGame?.Invoke(mode, demoLevelSource);

            Cleanup();
        }

        private void Cleanup ()
        {
            StopAllCoroutines(); // Cancel all "delayed start" tasks.
            pendingJobs.Clear();
        }
        #endregion
    }
}
