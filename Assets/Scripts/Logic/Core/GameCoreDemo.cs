﻿using System;
using Core;
using Data;
using FileSystem;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Core
{
    [AddComponentMenu("Kingdom/Logic/Core/Game Core Demo")]
    public class GameCoreDemo
        : GameCoreBase
    {
/*
        #region Public events.
        public event                    OnSaveGameCreated                   onSaveGameCreated = delegate () { };
        public event                    OnGameStarted                       onGameStarted;
        #endregion
*/

        #region Private data.
        #endregion

        #region Unity API
        protected new void Awake()
        {
            base.Awake();

            var persistentList = GetComponents<IPersistent>();
            saveGame = new SaveGame(ResourcePaths.Persistent.saveGameFriendFullPath, persistentList, this);
        }

        protected new void Start()
        {
            base.Start();

            Debug.LogFormat("App persistent data path is: '{0}'", Application.persistentDataPath);

            var loadSaveGameResult = LoadSaveGame();

            if (loadSaveGameResult == SaveGame.LoadResult.Skipped || loadSaveGameResult == SaveGame.LoadResult.Success)
            {
                StartCoroutine(GameStartedJob(false));
            }
        }

        protected new void LateUpdate()
        {
            base.LateUpdate();
        }
        #endregion

        #region IGameCore API
        public override bool isSaveEnabled
        {
            get { return false; }
        }

        public override void ChangeSaveGameBehaviour(SaveGameBehaviour saveGameBehaviour)
        {
        }

        public override void ScheduleSave(ScheduledSaveType scheduledSaveType)
        {
        }

        public override bool ImmediateSaveTransaction(bool onlyIfSaveQueued, Func<bool> performAction, Action rollbackAction)
        {
            return false;
        }

        #endregion

        #region Private API

        SaveGame.LoadResult LoadSaveGame()
        {
            if (saveGame != null)
            {
                Assert.IsNotNull(PersistentCore.instance);
                Assert.IsNotNull(PersistentCore.instance.networkController);
                var loadResult = saveGame.Load(PersistentCore.demoLevelSource == DemoLevelSource.FromResource, true);

                switch (loadResult)
                {
                    case SaveGame.LoadResult.Success:
                    {
                        Debug.Log("Demo mode Savegame loading OK.");
                        isGameStarted = true;
                    }; break;

                    case SaveGame.LoadResult.Failed:
                    {
                        Debug.LogError("Demo mode Savegame loading Failed!");

                        PersistentCore.instance.analytics.GameEvent("DemoMode SaveGame Load Failed", "Source", PersistentCore.demoLevelSource.ToString());

                        PersistentCore.instance.QueueModalConfirmMessage
                            ( Loc.Get("SaveGameLoadingFailedHeader")
                            , Loc.Get("SaveGameLoadingFailedMessage")
                            , () =>
                            {
                                PersistentCore.ScheduleGameModeSwitching(GameMode.MainGame);
                            }
                            , Loc.Get("idReturn"));
                    }; break;

                    case SaveGame.LoadResult.Skipped:
                    {
                        Debug.Log("Demo mode Savegame loading skipped.");
                        isGameStarted = true;
                    }; break;
                }

                return loadResult;
            }
            else
            {
                return SaveGame.LoadResult.Failed;
            }
        }
        #endregion
    }
}