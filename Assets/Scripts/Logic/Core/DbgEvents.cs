﻿using Core.EventSystem;

namespace Logic.DbgEvents
{
#if DEBUG
    public class OneMinuteLeftForQuestTimers : StaticEvent<OneMinuteLeftForQuestTimers.Delegate>
    {
        public delegate void Delegate();
    }
#endif
}
