﻿using System;
using System.Collections;
using Core;
using Core.Advertisement;
using Core.Analytics;
using Data;
using FileSystem;
using Logic.Facades;
using Logic.JuiceBar.Facade;
using Logic.Quests.Facade;
using Logic.Quests.SportQuest.SportLaneQuest.Facade;
using Logic.Serialization;
using Logic.Shop;
using Logic.Shop.Facade;
using Logic.SportAgency.Facade;
using Logic.Statistics;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.Trainers.Facade;

namespace Logic.Core
{
    [AddComponentMenu("Kingdom/Logic/Core/Game Core Game")]
    public class GameCoreGame
        : GameCoreBase
    {
/*
        #region Public events.
        public event                    OnSaveGameCreated                   onSaveGameCreated;
        public event                    OnGameStarted                       onGameStarted;
        #endregion
*/

        #region Private data.
        #endregion

        #region Unity API
        protected new void Awake()
        {
            base.Awake();

            var persistentList = GetComponents<IPersistent>();
            saveGame = new SaveGame(ResourcePaths.Persistent.saveGameFullPath, persistentList, this);

            saveGame.onSaveGameCreated += OnSaveGameCreated;

            statistics = new StatisticsLogic(this);

            shopManager = GetComponent<IShopManager>();
            Assert.IsNotNull(shopManager);

            sportShop = GetComponent<LogicSportShop>();
            Assert.IsNotNull(sportShop);

            sportAgency = GetComponent<LogicSportAgency>();
            Assert.IsNotNull(sportAgency);

            sportsmen = GetComponent<LogicSportsmen>();
            Assert.IsNotNull(sportsmen);

            quests = GetComponent<LogicQuests>();
            Assert.IsNotNull(quests);

            juiceBar = GetComponent<LogicJuiceBar>();
            Assert.IsNotNull(juiceBar);

            sportLaneQuest = GetComponent<SportLaneQuestLogic>();
            Assert.IsNotNull(sportLaneQuest);

            sportTimeQuest = GetComponent<SportTimeQuestLogic>();
            Assert.IsNotNull(sportTimeQuest);

            sportsmanTraining = GetComponent<SportsmanTrainingLogic>();
            Assert.IsNotNull(sportsmanTraining);

            shop = GetComponent<LogicShop>();
            Assert.IsNotNull(shop);

            shopUltimate = GetComponent<LogicUltimateShop>();
            Assert.IsNotNull(shopUltimate);

            trainers = GetComponent<LogicTrainers>();
            Assert.IsNotNull(trainers);

            iapShopPurchaseProtocol = shop;
        }

        protected new void OnDestroy()
        {
            saveGame.onSaveGameCreated -= OnSaveGameCreated;

            base.OnDestroy();
        }

        protected new void Start()
        {
            base.Start();

//          Application.runInBackground = false;

            Debug.LogFormat("App persistent data path is: '{0}'", Application.persistentDataPath);

            var loadSaveGameResult = LoadSaveGame();

            if (loadSaveGameResult == SaveGame.LoadResult.Skipped || loadSaveGameResult == SaveGame.LoadResult.Success)
            {
                if (saveGame.isSaveGameExists)
                {
                    Debug.Log("SaveGame exists, normal startup.");

                    PostLoadOrCreateSave();

                    StartCoroutine(GameStartedJob(false));
                }
                else
                {
                    Debug.Log("SaveGame NOT exists, fast startup.");

                    PersistentCore.instance.analytics.SetCustomDimension(0, Analytics.CustomDimension.Level, playerProfile.level);

                    StartCoroutine(GameStartedJob(true));
                }
            }
        }

        protected new void LateUpdate()
        {
            base.LateUpdate();

#if DEBUG
            if (DebugHacks.saveThisFrame)
            {
                DebugHacks.saveThisFrame = false;
                saveGame.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            }
#endif

            if (saveGame != null)
            {
                saveGame.TrySave();
            }
        }
        #endregion

        #region IGameCore API
        public override bool isSaveEnabled
        {
            get { return saveGame.isSaveEnabled; }
        }

        public override void ChangeSaveGameBehaviour(SaveGameBehaviour saveGameBehaviour)
        {
            saveGame.ChangeSaveGameBehaviour(saveGameBehaviour);
        }

        public override void ScheduleSave(ScheduledSaveType scheduledSaveType)
        {
            saveGame.ScheduleSave(scheduledSaveType);
        }

        public override bool ImmediateSaveTransaction(bool onlyIfSaveQueued, Func<bool> performAction, Action rollbackAction)
        {
            return saveGame.ImmediateSaveTransaction(onlyIfSaveQueued, performAction, rollbackAction);
        }

        #endregion

        #region Protected API.
        protected new void OnSaveGameCreated ()
        {
            Debug.Log(">>> SaveGame created.");

            PostLoadOrCreateSave();

            base.OnSaveGameCreated();
        }
        #endregion

        #region Private API
        IEnumerator AdvertisementsUpdate()
        {
            while (Application.isPlaying)
            {
                yield return new WaitForSeconds(1.0f);

                if (Advertisements.instance != null)
                    Advertisements.instance.UpdateAds();
                else
                    yield break;
            }
        }

        SaveGame.LoadResult LoadSaveGame()
        {
            DebugHacks.gamePassedLoading = false;

            if (saveGame != null)
            {
                Assert.IsNotNull(PersistentCore.instance);
                Assert.IsNotNull(PersistentCore.instance.networkController);
                var loadResult = saveGame.Load(false, false, PersistentCore.instance.networkController.downloadedFileTimeStamps);

                switch (loadResult)
                {
                    case SaveGame.LoadResult.Success:
                    {
                        DebugHacks.gameLoaded = true;
                        DebugHacks.gamePassedLoading = true;

                        Debug.Log("Savegame loading OK.");
                    }; break;

                    case SaveGame.LoadResult.Failed:
                    {
                        Debug.LogError("Savegame loading Failed!");

                        PersistentCore.instance.analytics.GameEvent("SaveGame Load Failed", "ProfileID", PersistentCore.instance.networkController.profileId);

#if UNITY_EDITOR
                        PersistentCore.instance.QueueModalConfirmMessage
                            ( Loc.Get("SaveGameLoadingFailedHeader")
                            , Loc.Get("SaveGameLoadingFailedMessage")
                            , () =>
                            {
                                PersistentCore.Quit(true);
                            }
                            , Loc.Get("idStop")
                            , () =>
                            {
                                DebugDeleteDataAndStop(true);
                            }
                            , "Delete Save");
#else
                        PersistentCore.instance.QueueModalConfirmMessage
                            ( Loc.Get("SaveGameLoadingFailedHeader")
                            , Loc.Get("SaveGameLoadingFailedMessage")
                            , () =>
                            {
                                var profileId = PersistentCore.instance.networkController.profileId;
                                string subject = string.Format("{0} save game loading failed (Player id: {1})"
                                    , Application.productName
                                    , profileId != string.Empty ? profileId : "<none>" );
                                string body = string.Format("Hello {0}!\n\n", Application.companyName);

                                PersistentCore.instance.ContactSupport(subject, body);

                                PersistentCore.Quit(true);
                            }
                            , Loc.Get("ContactSupportButton")
                            , () =>
                            {
                                PersistentCore.Quit(true);
                            }
                            , Loc.Get("idQuitGame"));
#endif
                    }; break;

                    case SaveGame.LoadResult.Skipped:
                    {
                        DebugHacks.gamePassedLoading = true;

                        Debug.Log("Demo mode Savegame loading skipped.");
                    }; break;
                }

                return loadResult;
            }
            else
            {
                DebugHacks.gamePassedLoading = true;
                return SaveGame.LoadResult.Failed;
            }
        }

        private void PostLoadOrCreateSave ()
        {
            PersistentCore.instance.analytics.SetCustomDimension(0, Analytics.CustomDimension.Level, playerProfile.level);

            if (Advertisements.instance != null)
            {
                Advertisements.instance.SetupCurLevel(playerProfile.level);
                Advertisements.instance.SetupInAppsCount(shop.persistentData.purchaseHistory.Count);
                Advertisements.instance.Init(playerProfile.adverticementsData);

                StartCoroutine(AdvertisementsUpdate());
            }
        }
        #endregion
    }
}
