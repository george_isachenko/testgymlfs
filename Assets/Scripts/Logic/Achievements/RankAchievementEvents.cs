﻿using Core.EventSystem;
using Data.Estate;

namespace Logic.Achievements
{
    public class EquipDecorPurchase : StaticEvent<EquipDecorPurchase.Delegate, EstateType>
    {
        public delegate void Delegate(EstateType estateType);
    }

    public class EarnCoins : StaticEvent<EarnCoins.Delegate, int>
    {
        public delegate void Delegate(int coins);
    }

    public class CompleteDailyActivity : StaticEvent<CompleteDailyActivity.Delegate>
    {
        public delegate void Delegate();
    }

    public class GetDailyBonus : StaticEvent<GetDailyBonus.Delegate>
    {
        public delegate void Delegate();
    }
        
    public class TrainVisitor : StaticEvent<TrainVisitor.Delegate>
    {
        public delegate void Delegate();
    }

    public class WatchAd : StaticEvent<WatchAd.Delegate>
    {
        public delegate void Delegate();
    }

    public class CompleteLaneQuest : StaticEvent<CompleteLaneQuest.Delegate>
    {
        public delegate void Delegate();
    }
        
    public class BuySportsman : StaticEvent<BuySportsman.Delegate>
    {
        public delegate void Delegate();
    }

    public class SellSportsman : StaticEvent<SellSportsman.Delegate>
    {
        public delegate void Delegate();
    }

    public class BuySportResource : StaticEvent<BuySportResource.Delegate>
    {
        public delegate void Delegate();
    }

    public class MakeJuice : StaticEvent<MakeJuice.Delegate, int>
    {
        public delegate void Delegate(int id);
    }

    public class HarvestFruits : StaticEvent<HarvestFruits.Delegate, int, int>
    {
        public delegate void Delegate(int id, int count);
    }

    public class CompleteJuiceQuest : StaticEvent<CompleteJuiceQuest.Delegate>
    {
        public delegate void Delegate();
    }
}
