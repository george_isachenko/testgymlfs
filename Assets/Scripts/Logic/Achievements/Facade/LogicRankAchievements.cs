﻿using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Data;
using Data.Estate;
using Data.Person;
using Logic.Persons;
using Logic.FacadesBase;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.Persons.Events;
using Core.Analytics;
using Data.RankAchievement;
using Core;
using Data.PlayerProfile;
using Core.SocialAPI;

namespace Logic.Achievements.Facade
{
    [AddComponentMenu("Kingdom/Logic/Achievements/Facade/Logic Rank Achievements")]
    [DisallowMultipleComponent]
    [BindBridgeInterface(typeof(PresentatoinRankAchievements))]
    public class LogicRankAchievements : BaseLogicFacade, IPersistent 
    {
        PresentatoinRankAchievements presenter;

        RankAchievementsData    data;
        int maxRank = 0;
        bool bannerOn           { set { SetBannerOn(value); } }

        public List<LogicRankAchievementsTask> tasks = new List<LogicRankAchievementsTask>();

        public string   completedRatio      { get { return GetCompletedRatio(); } }
        public int      completedCount      { get { return tasks.Count(t => t.completed == true); } }
        public int      claimedCount        { get { return tasks.Count(t => t.claimed == true); } }
        public string   rankName            { get { return GetRankName(); } }
        public int      currentRank         { get { return data == null ? 0 : data.currentRank; } }
        
        [BindBridgeSubscribe]
        private void Subscribe(PresentatoinRankAchievements presenter)
        {
            this.presenter = presenter;
        }

        void Awake()
        {
            Achievements.EquipDecorPurchase.Event += PurchaseEquipDecor;
            Achievements.EarnCoins.Event += EarnCoins;
            Achievements.CompleteDailyActivity.Event += CompleteDailyActivity;
            Achievements.GetDailyBonus.Event += GetDailyBonus;
            Achievements.TrainVisitor.Event += TrainVisitor;
            Achievements.WatchAd.Event += WatchAd;
            Achievements.CompleteLaneQuest.Event += CompleteLaneQuest;
            Achievements.BuySportsman.Event += BuySportsman;
            Achievements.SellSportsman.Event += SellSportsman;
            Achievements.BuySportResource.Event += BuySportResource;
            Achievements.MakeJuice.Event += MakeJuice;
            Achievements.HarvestFruits.Event += HarvestFruit;
            Achievements.CompleteJuiceQuest.Event += CompleteJuiceQuest;

            SportsmanTypeChangedEvent.Event += TrainSportsman;
        }

        void OnDestroy()
        {
            Achievements.EquipDecorPurchase.Event -= PurchaseEquipDecor;
            Achievements.EarnCoins.Event -= EarnCoins;
            Achievements.CompleteDailyActivity.Event -= CompleteDailyActivity;
            Achievements.GetDailyBonus.Event -= GetDailyBonus;
            Achievements.TrainVisitor.Event -= TrainVisitor;
            Achievements.WatchAd.Event -= WatchAd;
            Achievements.CompleteLaneQuest.Event -= CompleteLaneQuest;
            Achievements.BuySportsman.Event -= BuySportsman;
            Achievements.SellSportsman.Event -= SellSportsman;
            Achievements.BuySportResource.Event -= BuySportResource;
            Achievements.MakeJuice.Event -= MakeJuice;
            Achievements.HarvestFruits.Event -= HarvestFruit;
            Achievements.CompleteJuiceQuest.Event -= CompleteJuiceQuest;

            SportsmanTypeChangedEvent.Event -= TrainSportsman;
        }

        void Start()
        {
            var table = DataTables.instance.rankAchievements;
            maxRank = table.Max(item => item.rank);
        }

        void SetBannerOn(bool value)
        {
#if UNITY_IOS
            UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowDefaultAchievementCompletionBanner(value);
#endif
        }

        string GetRankName()
        {
            if (data.currentRank < Data.DataTables.instance.rankReward.Count)
            {
                return Loc.Get("AchievementsTrainerRankName") + " " + Loc.Get(Data.DataTables.instance.rankReward[data.currentRank].name);
            }

            return "";
        }

        string GetCompletedRatio()
        {
            return claimedCount.ToString() + "/" + tasks.Count.ToString();
        }

        void UpdateTasks()
        {
            if (tasks.Count == 0)
            {
                AddTaskForCurrentRank();
            }
        }

        void AddTaskForCurrentRank()
        {
            var table = DataTables.instance.rankAchievements;

            foreach (var row in table)
            {
                if (row.rank == data.currentRank)
                {
                    var taskData = new RankAchievementsTaskData();
                    taskData.task = row;
                    tasks.Add( new LogicRankAchievementsTask(taskData, OnComlete) );
                }
            }
        }

        List<int> GetAllIndicesOfCompletedTasks()
        {
            List<int> result = new List<int>();
            var table = DataTables.instance.rankAchievements;

            for (int i = 0; i < table.Length; i++)
            {
                var task = table[i];
                if (task.rank < currentRank)
                    result.Add(i);
            }

            foreach (var task in tasks)
            {
                if (task.completed)
                {
                    var idx =  GetIdxOfTask(task);
                    if (idx >= 0)
                        result.Add(idx);
                }
            }

            return result;
        }

        int GetIdxOfTask(LogicRankAchievementsTask task)
        {
            var table = DataTables.instance.rankAchievements;

            var currentRankItems = table.Where( item => item.rank == currentRank).ToArray();

            if (currentRankItems == null || currentRankItems.Length == 0)
            {
                Debug.LogError("GetIdxOfTask : no tasks for current rank – " + currentRank.ToString());  
                return -1;
            }
                
            var itemsOfType = currentRankItems.Where( item => item.type == task.type).ToArray();

            if (itemsOfType.Length == 0)
            {
                Debug.LogErrorFormat("GetIdxOfTask : IMPOSIBRU task not found in table rank - {0}, type - {1}", currentRank, task.type.ToString());
                return  -1;
            }

            //////////////////////////////////////////////////////////////////
            /// itemsOfType.Length == 1     <=== THIS IS THE MOST PROBABLE CASE
            //////////////////////////////////////////////////////////////////
            if (itemsOfType.Length == 1) // <=== THIS IS THE MOST PROBABLE CASE
            {
                return GetIdxOfTaskData(itemsOfType[0]);
            }
            else
            {
                for( int i = 0; i < itemsOfType.Length; i++)
                {
                    var itemSrc = itemsOfType[i];
                    var itemDst = task.data.task;

                    if (itemSrc.type == itemDst.type 
                        && itemSrc.id == itemDst.id 
                        && itemSrc.goal == itemDst.goal)
                    {
                        return GetIdxOfTaskData(itemSrc);
                    }

                }
            }

            return -1;
        }

        int GetIdxOfTaskData(RankAchievementTableData taskData)
        {
            var table = DataTables.instance.rankAchievements;

            for( int i = 0; i < table.Length; i++)
            {
                if (table[i] == taskData)
                    return i;
            }

            return -1;
        }

        void OnComlete(LogicRankAchievementsTask task)
        {
            PersistentCore.instance.analytics.GameEvent("Achievement Completed",
                new GameEventParam("Level", gameCore.playerProfile.level),
                new GameEventParam("Achievement id",  data.currentRank.ToString() + "." + task.id.ToString()));

            var idx = GetIdxOfTask(task);

            Debug.LogFormat("Reporting achive : {0} {1}", task.title, task.subTitle);
            bannerOn = true;
            SocialNetworkAchievement(idx);
            presenter.UpdateBtn();
        }

        void GoNextRank()
        {
            if (data.currentRank < maxRank)
            {
                tasks.Clear();
                data.currentRank++;
                AddTaskForCurrentRank();

                var table = DataTables.instance.rankReward;
                var rankReward = table[data.currentRank].reward;

                presenter.RankLevelUp(rankReward, data.currentRank);
                presenter.UpdateBtn();
            }
            else
                presenter.UpdateIfVisible();
        }

        void IncrementTaskWithId(RankAchievementType type, int id, int value)
        {
            var selectedTasks = tasks.Where ( task => task.type == type && task.id == id).ToArray();

            foreach(var task in selectedTasks)
                task.CompleteSteps(value);
        }

        void IncrementTask(RankAchievementType type, int value)
        {
            var selectedTasks = tasks.Where ( task => task.type == type).ToArray();

            foreach(var task in selectedTasks)
                task.CompleteSteps(value);
        }

        #if DEBUG
        public void FakeCompleteTask(int idx)
        {
            if (idx < tasks.Count)
            {
                var task = tasks[idx];

                if (task.data.type == RankAchievementsTaskData.TaskType.Iteration)
                {
                    if (0 == task.data.task.goal - 1 - task.data.stepsCompleted)
                        task.CompleteSteps(1);
                    else
                        task.CompleteSteps(task.data.task.goal - 1 - task.data.stepsCompleted);
                }
                else
                {
                    task.data.stepsCompleted = int.MaxValue;
                    task.CompleteSteps(1);
                }
            }
        }
        #endif

        #if DEBUG
/*
        bool IsLoggedIn()
        {
            var socialManager = PersistentCore.instance.socialManager;

#if UNITY_IOS
            return socialManager.GetNetwork(NetworkType.GameCenter).GetNetworkState() == NetworkState.LoggedIn;
#elif UNITY_ANDROID
            return socialManager.GetNetwork(NetworkType.GooglePlay).GetNetworkState() == NetworkState.LoggedIn;
#else
            return false;
#endif
        }
*/

        public void DropAllAchievements()
        {
            var socialManager = PersistentCore.instance.socialManager;

            foreach (var network in socialManager.GetAvailableNetworks(NetworkFeatures.SupportsAllAchievementsReset, true))
            {
                network.ResetAllAchievements();
            }
        }

        void ResetAchievementsCallback(bool result)
        {
            if (result)
                Debug.Log("Achievements reset successfully");
            else
                Debug.LogError("Can't reset achievements");
        }
        #endif

        public void SyncAllAchievements()
        {
            var completedAchievements = GetAllIndicesOfCompletedTasks();
            
            bannerOn = false;
            foreach (var idx in completedAchievements)
                SocialNetworkAchievement(idx, 100);
                
        }

        void SocialNetworkAchievement(int idx, float percent = 100.0f)
        {
            var socialManager = PersistentCore.instance.socialManager;
            var table = DataTables.instance.rankAchievementsSocial;

            if (idx < 0)
            {
                Debug.LogError("AchievementProgress : idx < 0");
                return;
            }

            if (idx < table.Length)
            {
                #if UNITY_IOS
                var achievementID = table[idx].gameCenterID;
                #else
                var achievementID = table[idx].googlePlayID;
                #endif

                var info =  Data.DataTables.instance.rankAchievements[idx];

                Debug.LogFormat("Going to report achievement id {0}, type {1}, id {2}, goal {3}", achievementID, info.type, info.id, info.goal);

/*
                #if DEBUG
                if (!IsLoggedIn())
                {
                    Debug.Log("Can't report achievement: not logged in");
                    return;
                }
                #endif
*/

                foreach (var network in socialManager.GetAvailableNetworks(NetworkFeatures.SupportsAchievements, true))
                {
                    network.ReportAchievement(achievementID, percent);
                }
            }
            else
            {
                Debug.LogError("AchievementProgress : idx out of bounds");
            }
        }

        #if DEBUG
        public void OnDebugItemClick(int idx)
        {
            FakeCompleteTask(idx);

            presenter.UpdateIfVisible();
        }
        #endif

        public void GoNextRunkIfNecessary()
        {
            try
            {
                if (tasks.Count == claimedCount)
                {
                    GoNextRank();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("GoNextRunkIfNecessary exception : " + e.Message);
            }
        }

        public void OnCliam(int idx)
        {
            if (idx < tasks.Count)
            {
                var task = tasks[idx];
                task.claimed = true;

                playerProfile.AddFitBucks(task.reward, new Analytics.MoneyInfo("Achievement Complete", "task_" + idx.ToString()), true);

                if (tasks.Count == claimedCount)
                {
                    GoNextRank();
                }
            }
            else
                Debug.LogError("LogicRankAchievement::OnCliam wrong idx");

            presenter.UpdateIfVisible();
            presenter.UpdateBtn();
        }

        public void OnCliamLevelUp()
        {
            var table = DataTables.instance.rankReward;
            if (table.ContainsKey(data.currentRank))
            {        
                var rankReward = table[data.currentRank].reward;

                playerProfile.AddFitBucks(rankReward, new Analytics.MoneyInfo("Achievement Rank Reached", "rank_" +  data.currentRank.ToString()), true);
            }
        }

        /// EVENTS

        void PurchaseEquipDecor(EstateType estateType)
        {
            if (estateType.itemType == EstateType.Subtype.Training)
            {
                IncrementTaskWithId(RankAchievementType.BuyEquip, estateType.id, 1);
                IncrementTask(RankAchievementType.BuyEquipTotal, 1);

            }
            else if (estateType.itemType == EstateType.Subtype.Decor)
            {
                IncrementTask(RankAchievementType.BuyDecorTotal, 1);
            }
        }

        void EarnCoins(int coins)
        {
            IncrementTask(RankAchievementType.EarnCoins, coins);
        }

        void CompleteDailyActivity()
        {
            IncrementTask(RankAchievementType.CompleteDailyActivitiesTotal, 1);
        }

        void TrainVisitor()
        {
            IncrementTask(RankAchievementType.TrainVisitors, 1);
        }

        void GetDailyBonus()
        {
            IncrementTask(RankAchievementType.GetDailyBonus, 1);
        }

        void WatchAd()
        {
            IncrementTask(RankAchievementType.WatchAd, 1);
        }

        void CompleteLaneQuest()
        {
            IncrementTask(RankAchievementType.CompleteLaneQuest, 1);
        }

        void TrainSportsman(PersonSportsman person, SportsmanType type)
        {
            IncrementTask(RankAchievementType.TrainSportsmen, 1);
        }

        void BuySportsman()
        {
            IncrementTask(RankAchievementType.BuySportsmen, 1);
        }

        void SellSportsman()
        {
            IncrementTask(RankAchievementType.SellSportsmen, 1);
        }

        void BuySportResource()
        {
            IncrementTask(RankAchievementType.BuySportResource, 1);
        }

        void MakeJuice(int id)
        {
            IncrementTaskWithId(RankAchievementType.MakeJuice, id - (int)ResourceType.Juice_01, 1);
        }

        void HarvestFruit(int id, int count)
        {
            IncrementTaskWithId(RankAchievementType.HarvestFruits, id, count);
        }

        void CompleteJuiceQuest()
        {
            IncrementTask(RankAchievementType.CompleteJuiceQuest, 1);
        }

        #region IPersistent API
        string IPersistent.propertyName
        {
            get
            {
                return "RankAchievements";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(RankAchievementsData);
            }
        }

        void IPersistent.InitDefaultData()
        {
            data = new RankAchievementsData();
            UpdateTasks();
        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is RankAchievementsData);
            if (obj is RankAchievementsData)
            {
                data = (RankAchievementsData)obj;

                foreach(var taskData in data.tasksData)
                    tasks.Add( new LogicRankAchievementsTask(taskData, OnComlete) );

                return true;
            }
            else
            {
                
            }

            return false;
        }

        object IPersistent.Serialize()
        {
            data.tasksData.Clear();

            foreach( var task in tasks)
                data.tasksData.Add(task.data);
            
            return data;
        }

        void IPersistent.OnPreLoad ()
        {
            
        }

        void IPersistent.OnLoaded (bool success)
        {
            if (!success)
            {

            }
            else
            {

            }

            if (data == null)
            {
                data = new RankAchievementsData();
            }

            UpdateTasks();
        }

        void IPersistent.OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            
        }

        void IPersistent.OnLoadSkipped ()
        {
            //presenter.UpdateView();
        }

        #endregion
    }
}