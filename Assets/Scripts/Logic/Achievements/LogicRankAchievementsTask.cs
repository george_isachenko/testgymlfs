﻿using System;
using Data;
using Data.RankAchievement;
using UnityEngine;

namespace Logic.Achievements
{
    public class LogicRankAchievementsTask
    {
        public RankAchievementsTaskData data    { private set; get; } // = new RankAchievementsTaskData();

        public Action<LogicRankAchievementsTask> OnComplete_Callbak;

        public bool completed                   { get { return data.completed; } }
        public bool claimed                     { get { return data.claimed; } set { data.claimed = true; } }
        public bool completedAndClaimed         { get { return completed && claimed; } }
        public RankAchievementType type         { get { return data.task.type; } }
        public int id                           { get { return data.task.id; } }
        public int reward                       { get { return Math.Max(data.task.reward, 1); } }

        public string               title       { get { return data.GetTitle(); } }
        public string               subTitle    { get { return data.GetSubTitle(); } }
        public string               steps       { get { return data.steps; } }
        public float                progress    { get { return data.progress; } }

        public string               hintTitle   { get { return title; } }
        public string               hintDesc    { get { return GetHint(); } }


        public LogicRankAchievementsTask(RankAchievementsTaskData data, Action<LogicRankAchievementsTask> OnComplete)
        {
            this.data = data;
            OnComplete_Callbak = OnComplete;

            //Debug.LogError("Adding task " + task.type.ToString());
        }

        public void CompleteSteps(int steps)
        {
            if (data.type == RankAchievementsTaskData.TaskType.Iteration)
                data.stepsCompleted += steps;

            if (completed)
                OnComplete();
        }

        void OnComplete()
        {
            if (completed)
            {
                if (OnComplete_Callbak != null)
                    OnComplete_Callbak(this);
                else
                    Debug.LogError("LogicRankAchievementsTask OnComplete_Callbak == null");
            }
            else
                Debug.LogError("trying to comple uncompleted rank achievement task");
        }

        string GetHint()
        {
            if (data.task.type == RankAchievementType.OpenSportClub)
                return Loc.Get("OpenSportClubHint");
            else if (data.task.type == RankAchievementType.BuySportsmen)
                return Loc.Get("BuySportsmenHint");
            else if (data.task.type == RankAchievementType.SellSportsmen)
                return Loc.Get("SellSportsmenHint");
            else if (data.task.type == RankAchievementType.ReachStyle)
                return Loc.Get("ReachStyleHint");
            else if (data.task.type == RankAchievementType.WatchAd)
                return Loc.Get("WatchAdHint");
            else 
                return Loc.Get("GeneralAchievementHint");
        }
    }
}