﻿using UnityEngine;
using System.Collections;
using Data;
using Data.Person;
using System.Collections.Generic;
using System.Linq;

namespace Logic.SportAgency
{
    public static class SportAgencyRndSlots
    {

        class RndItem
        {
            public SportsmanType   type;
            public int             probability;
        }

        static List<RndItem>       items = new List<RndItem>();
        static int                 sumProbability = 0;

        public static void Init (Dictionary<SportsmanType, SportsmanData> members)
        {
            if (sumProbability != 0)
            {
                return;
            }

            if (members == null || members.Count == 0)
            {
                Debug.LogError("Can't init SportAgencyRndSlots bcs of void table");
                members = new Dictionary<SportsmanType, SportsmanData>
            {
                {SportsmanType.Sportsman_0, DataTables.instance.sportsmen[SportsmanType.Sportsman_0]}
            };
                //return;
            }

            var min = members.Aggregate(int.MaxValue, (current, mem) => Mathf.Min(current, mem.Value.coins));
            var selected = members.Select(i => new RndItem
            {
                type = i.Key,
                probability = (int) ((float) min/i.Value.coins*1000f)
            }).ToArray();

            items.AddRange(selected);
            sumProbability = selected.Sum(i => i.probability);
        }

        public static SportsmanType GetRndType ()
        {
            var value = UnityEngine.Random.Range(0, sumProbability);

            var sum = 0;

            foreach (var item in items)
            {
                if (value > sum && value < sum + item.probability)
                {
                    return item.type;
                }
                sum += item.probability;
            }

            var itm = items[items.Count - 1];
            if (value > sum && value < sum + itm.probability)
            {
                return itm.type;
            }

            Debug.LogError("ACHTUNG! SportAgencyRndSlots");

            return SportsmanType.Sportsman_0;
        }
    }
}