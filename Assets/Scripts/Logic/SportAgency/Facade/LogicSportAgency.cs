﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core.Analytics;
using Core.Timer;
using Data;
using Data.Person;
using Data.Room;
using Data.SportAgency;
using Logic.FacadesBase;
using Logic.PlayerProfile.Events;
using Logic.Quests.Tutorial;
using Logic.Rooms.Events;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.SportAgency.Facade
{
    [AddComponentMenu("Kingdom/Logic/Sport Agency/Facade/Logic Sport Agency")]
    [DisallowMultipleComponent]
    [BindBridgeInterface(typeof (PresentationSportAgency))]
    public class LogicSportAgency: BaseLogicFacade, IPersistent
    {
        public event Action<string, RoomData, IRoomSetupDataInfo> onRoomSizeUpdated;
        public event Action<int> onLevelUp;

        private SportAgencyPersistentData data;
        private PresentationSportAgency presenter;
        private IEnumerator timerCoroutine = null;
        private IEnumerator delayedUpdateViewJob;

        public int                          slotsSellAvailable  { get { return GetSlotsSellAvailable(); } }
        public Cost                         unlockSlotCost      { get; private set; }
        public TimerFloat                   timerBuySlots       { get { return data.timerBuySlots; } }
        public List<SportAgencySellSlot>    slotsSell           { get { return data.slotsSell; } }
        public List<SportAgencyBuySlot>     slotsBuy            { get { return data.slotsBuy; } }

        public CanUseFeatureResult canUseMyFeature
        {
            get
            {
                if (gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) == 0)
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.DependencyLocked);
                if (playerProfile.level < DataTables.instance.balanceData.Agency.startLevel)
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.LockedNeededLevel, DataTables.instance.balanceData.Agency.startLevel);
                return new CanUseFeatureResult(CanUseFeatureResult.ResultType.CanUse);
            }
        }
            
        void dbgOneMinuteLeft()
        {
            timerBuySlots.secondsToFinish = 60;
        }

        [BindBridgeSubscribe]
        private void Subscribe(PresentationSportAgency presentationAgency)
        {
            this.presenter = presentationAgency;
            unlockSlotCost = new Cost(Cost.CostType.BucksOnly, 20, 0);
        }

        void Start()
        {
#if DEBUG
            DbgEvents.OneMinuteLeftForQuestTimers.Event += dbgOneMinuteLeft;
#endif
            RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;
            LevelUpEvent.Event += OnLevelUp;
        }

        void OnDestroy()
        {
#if DEBUG
            DbgEvents.OneMinuteLeftForQuestTimers.Event -= dbgOneMinuteLeft;
#endif
            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;
            LevelUpEvent.Event -= OnLevelUp;
        }

        private void OnLevelUp(int level)
        {
            if (data == null && canUseMyFeature.level <= level)
                ((IPersistent)(this)).InitDefaultData();
            
            if (onLevelUp != null)
                onLevelUp(level);
        }

        int GetSlotsSellAvailable()
        {
            for (int i = 0; i < data.slotsSell.Count; i++)
            {
                if (data.slotsSell[i].state == SportAgencySellSlot.State.Locked)
                    return Mathf.Min( i + 1, data.slotsSell.Count);
            }

            return data.slotsSell.Count;
        }

        public void CleanUpSellSlots()
        {
            foreach (var slot in data.slotsSell)
            {
                if (slot.isFree)
                    slot.state = SportAgencySellSlot.State.Empty;
            }
        }

        public bool SendSportsmanToSlot(SportsmanType type)
        {
            foreach (var slot in data.slotsSell)
            {
                if (slot.isFree)
                {
                    var member = sportsmen.GetSportsman
                        ( type
                        , gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)
                        , PersonState.Idle, PersonState.ExerciseComplete);

                    if (member != null)
                    {
                        slot.sportsmanId = member.id;
                        slot.state = SportAgencySellSlot.State.Sell;

                        slot.sportsmanType = member.sportsmanType;
                        slot.name = member.name;

                    return true;
                }
            }
            }

            return false;
        }

        void DelayedUpdateView()
        {
            if (delayedUpdateViewJob == null)
            {
                delayedUpdateViewJob = DelayedUpdateViewJob();
                gameCore.StartCoroutine(delayedUpdateViewJob);
            }
        }

        private IEnumerator DelayedUpdateViewJob()
        {
            yield return 0; // new WaitForSeconds(0);

            delayedUpdateViewJob = null;
            presenter.UpdateView();
        }

        public void Sell_Click(int idx)
        {
            var slot = data.slotsSell[idx];
            slot.Sell();

            presenter.DropSelection();

            //sportsmen.KillCharacterRequest(slot.sportsmanId, new Analytics.MoneyInfo("SportAgency", slot.sportsmanType.ToString()));
            sportsmen.RemoveSportsmen(slot.sportsmanType,1,new Analytics.MoneyInfo("SportAgency", slot.sportsmanType.ToString()));

            DelayedUpdateView();
        }

        public void UnlockSell_Click(int idx)
        {
            if (playerProfile.SpendMoney(unlockSlotCost, new Analytics.MoneyInfo("SportAgency", "Unlock_Sell")))
            {
                var slot = data.slotsSell[idx];
                slot.Unlock();

                presenter.UpdateView();
            }
        }

        public int SportsmanPrice(SportsmanType type)
        {
			return Mathf.CeilToInt(DataTables.instance.sportsmen[type].coins * 1.5f);
        }

        public void SellClaim_Click(int idx)
        {
            var slot = data.slotsSell[idx];
            var coinsPrice = SportsmanPrice(slot.sportsmanType);

            playerProfile.AddCoins(coinsPrice, new Analytics.MoneyInfo("SportAgency", slot.sportsmanType.ToString()), true);
            Achievements.SellSportsman.Send();

            slot.Empty();

            presenter.UpdateView();
        }

        public void SellTimeOut(int idx)
        {
            if (data.slotsSell.Count > idx)
            {
                var slot = data.slotsSell[idx];
                slot.timer = null;
                slot.complete = true;

                presenter.UpdateView();
            }
            else
                Debug.LogError("SellTimeOut : wrong idx");
        }

        public void BuyTimeOut(int idx)
        {
            if (data.slotsBuy.Count > idx)
            {
                var slot = data.slotsBuy[idx];
                slot.isSold = false;

                data.RandomizeBuySlot(idx, gameCore.personManager.GenerateRandomName, availableToBuySportsmans);

                presenter.UpdateView();
            }
            else
                Debug.LogError("BuyTimeOut : wrong idx");
        }

        public void Buy_Click(int idx)
        {
            var slot = data.slotsBuy[idx];

            if (sportsmen.CanAdd())
            {
                if (playerProfile.CanSpend(slot.cost))
                {
                    if (playerProfile.SpendMoney(slot.cost, new Analytics.MoneyInfo("SportAgency", slot.type.ToString())))
                    {
                        slot.isSold = true;
                        sportsmen.CreateSportsman(slot.type, slot.gender, new Analytics.MoneyInfo("SportAgency", "SportAgency"),slot.name);
                        Achievements.BuySportsman.Send();
                    }
                }
                else
                {
                    var diffCost = new Cost();
                    diffCost.type = Cost.CostType.CoinsOnly;
                    diffCost.value = slot.cost.value - playerProfile.coins;

                    presenter.BuyCoinsForBucks(diffCost, new Analytics.MoneyInfo("SportAgency", slot.type.ToString()), 
                        () => { 
                            Buy_Click(idx); 
                        });
                }
            }
            else
                presenter.CantAddSportsmanMsg();
        }

        void SellFromSlot(SportAgencySellSlot slot)
        {
            if (slot.state == SportAgencySellSlot.State.Sell)
            {
                slot.Empty();
                gameCore.playerProfile.AddCoins(1000, new Analytics.MoneyInfo("SportAgency", "SportAgency"), false);
                //Achievements.SellSportsman.Send();
            }
            else
            {
                Debug.LogError("trying to sell from wrong slot state");
            }

            presenter.UpdateView();
        }

        public void RefreshBuySlots()
        {
            data.RefreshBuySlots(gameCore.personManager.GenerateRandomName,availableToBuySportsmans);
        }
        private void UpdateTimerCoroutine()
        {
            if (timerCoroutine != null)
            {
                gameCore.StopCoroutine(timerCoroutine);
                timerCoroutine = null;
            }

            timerCoroutine = TimerIenumerator();
            gameCore.StartCoroutine(timerCoroutine);
        }

        private IEnumerator TimerIenumerator()
        {
            while (timerBuySlots != null && timerBuySlots.secondsToFinish > 0)
            {
                yield return null;
            }
            RefreshBuySlots();

            UpdateTimerCoroutine();
        }


        public string propertyName { get { return "SportAgencyData"; } }
        public Type propertyType { get { return typeof(SportAgencyPersistentData); } }

        bool IPersistent.Deserialize(object obj)
        {
            data = obj as SportAgencyPersistentData;
            Assert.IsNotNull(data);

            UpdateTimerCoroutine();
            return data != null;
        }

        object IPersistent.Serialize()
        {
            return data;
        }

        void IPersistent.OnPreLoad()
        {
            
        }

        void IPersistent.InitDefaultData()
        {
            if (canUseMyFeature.level <= playerProfile.level)
            {
                data = new SportAgencyPersistentData();
                data.InitDefaultItems(gameCore.personManager.GenerateRandomName, availableToBuySportsmans);
                UpdateTimerCoroutine();
            }
        }

        public Dictionary<SportsmanType, SportsmanData> availableToBuySportsmans
        {
            get
            {
                var availableSportmanTypes = DataTables.instance.sportExercises
                    .Where(e => e.leverRequired <= gameCore.playerProfile.level)
                    .Select(e => e.trainTo)
                    .Distinct().ToList();
                availableSportmanTypes.Add(SportsmanType.Sportsman_0);
                return
                    DataTables.instance.sportsmen.Where(p => availableSportmanTypes.Contains(p.Key))
                        .ToDictionary(i => i.Key, i => i.Value);
            }
        }

        public bool isClubInEditMode
        {
            get { return gameCore.equipmentManager.isInEditMode; }
        }
            
        public bool isSportAgencyTutorialRunning { get
        {
            return gameCore.quests.tutorial.IsTutorialRunning<TutorSportAgencyQuest>();
        } }

        void IPersistent.OnLoaded(bool success)
        {
            if (success)
            {
                if (canUseMyFeature.canUse)
                {
                    if (data == null)
                    {
                        Debug.LogError("LogicSportAgency : loaded with success, but data not inited. Going to init right now");
                        (this as IPersistent).InitDefaultData();
                    }
                }
            }
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (canUseMyFeature.canUse)
            {
                if (data != null)
                {
                    var seconds = (float)timePassedSinceSave.TotalSeconds;

                    if (timerBuySlots != null)
                        timerBuySlots.FastForward(seconds);

                    foreach (var slot in data.slotsSell)
                    {
                        if (slot.timer != null)
                            slot.timer.FastForward(seconds);
                    }

                    while(slotsBuy.Count > 4)
                    {
                        slotsBuy.RemoveAt(slotsBuy.Count - 1);
                    }

                    UpdateTimerCoroutine();
                }
                else
                {
                    Debug.LogError("LogicSportAgency.OnPostLoad : data is NULL, creating default");
                    ((IPersistent)(this)).InitDefaultData();
                }
            }
        }

        void IPersistent.OnLoadSkipped()
        {
        }

        void OnRoomSizeUpdated (string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupDataInfo)
        {
            onRoomSizeUpdated?.Invoke(roomName, roomData, roomSetupDataInfo);
        }
    }
}

