﻿using Logic.Estate;

namespace Logic
{
    public static class DebugHacks
    {
        public static bool visitorCanUseAnyEquipment    = false;
        public static int  exerciseOverrideTime         = 0;
        public static bool gameLoaded                   = false;
        public static bool gamePassedLoading            = false;
        public static bool tutorialRunning              = false;
        public static bool questsBooster                = false;
        public static bool fastEquipmentBreakage        = false;
        public static bool saveThisFrame                = false;
        public static bool disableTutorialInit          = false;
        public static bool disablePasserbys             = false;
        public static bool disableTrainers              = false;
        public static bool visitorsAutoPlay             = false;
        public static int forcedTrainerLevel            = 0; // 15; // 0 - Do not force trainer level.

        public static EquipmentUseHPCosts[] fastEquipmentBreakageCosts = new EquipmentUseHPCosts[]
        {
            new EquipmentUseHPCosts(30, 40),
            new EquipmentUseHPCosts(40, 50),
            new EquipmentUseHPCosts(50, 60)
        };
    }
}
