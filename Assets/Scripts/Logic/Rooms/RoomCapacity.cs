﻿using System;
using System.Collections.Generic;
using Data;
using Data.Room;
using Logic.Core;
using UnityEngine.Assertions;

namespace Logic.Rooms
{
    public class RoomCapacity
    {
        IGameCore gameCore;
        RoomsPersistentData persistentData;  // Shared data.
        
        public RoomCapacity(IGameCore gameCore, RoomsPersistentData persistentData)
        {
            Assert.IsNotNull(gameCore);
            Assert.IsNotNull(persistentData);

            this.gameCore = gameCore;
            this.persistentData = persistentData;
        }

        public int GetTotalStylePoints(string roomName, int expandLevel = -1)
        {
            if (persistentData.roomsData == null)
                return 0;

            int value = 0;
            foreach (var eq in gameCore.equipmentManager.equipment)
            {
                if (eq.Value.roomName == roomName)
                {
                    if ((eq.Value.estateType.itemType == Data.Estate.EstateType.Subtype.Decor) ||
                        (eq.Value.estateType.itemType == Data.Estate.EstateType.Subtype.Door)
                    )
                    {
                        if (eq.Value.affectsStyle)
                        {
                            value += eq.Value.estateType.style;
                        }
                    }
                }
            }

            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                var expandData = DataTables.instance.expand;
                int expandLvl = expandLevel > 0 ? expandLevel : roomData.expandLevel;

                for(int i = 0; i < expandLvl; ++i)
                {
                    value += GetStyleAtExpandLevel(roomName, i);
                }
            }

            return value;
        }

        int GetStyleAtExpandLevel(string roomName, int expandLevel)
        {
            if (persistentData.roomsData == null)
                return 0;

            if (roomName == null)
                return 0;

            var expandData = DataTables.instance.expand;
            RoomExpandInfo roomExpandInfo = null;
            if (expandData.TryGetValue(RoomExpandInfo.CreateKey(expandLevel, roomName), out roomExpandInfo))
                return roomExpandInfo.addStyle;           

            return 0;
        }

        public int GetStylePointsNextExpandLevel(string roomName) //--- v
        {
            if (persistentData.roomsData == null)
                return 0;

            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                return GetStyleAtExpandLevel(roomName, roomData.expandLevel);
            }

            return 0;
        }

        public int GetTotalExpandStylePoints(string roomName)
        {
            if (persistentData.roomsData == null)
                return 0;

            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                var count = 0;
                for (var i = 0; i < roomData.expandLevel; i++)
                {
                    count += GetStyleAtExpandLevel(roomName, i);
                }
                return count;
            }

            return 0;
        }


        public int GetMaxStylePoints(string roomName, int expandLevel = -1) //---v
        {
            if (persistentData.roomsData == null)
                return 0;
            
            int level = GetStyleLevel(GetTotalStylePoints(roomName));
            int maxStylePoints = GetMaxStylePointsForLevel(level);

            return maxStylePoints;
        }

        public int GetNormalStylePoints(string roomName, int overloadStylePoints = -1)
        {
            int stylePoints = overloadStylePoints < 0 ? GetTotalStylePoints (roomName) : overloadStylePoints;
            var styleTable = DataTables.instance.styleCapacity;

            int curMin = 0;

            foreach (var t in styleTable)
            {
                if (stylePoints < t.stylePoints)
                    break;

                curMin = t.stylePoints;
            }

            return stylePoints - curMin;
        }

        public int GetNormalStylePointsMax(string roomName, int overloadStylePoints = -1)
        {
            if (persistentData.roomsData == null)
                return 0;

            int stylePoints = overloadStylePoints < 0 ? GetTotalStylePoints (roomName) : overloadStylePoints;

            int curMin = 0;
            int curMax = 0;

            var styleTable = DataTables.instance.styleCapacity;
            foreach (var t in styleTable)
            {
                curMax = t.stylePoints;

                if (stylePoints < t.stylePoints)
                    break;

                curMin = t.stylePoints;
            }

            return curMax - curMin;
        }

        public int GetStylePointsMax(string roomName, int overloadStylePoints = -1)
        {
            if (persistentData.roomsData == null)
                return 0;

            int stylePoints = overloadStylePoints < 0 ? GetTotalStylePoints (roomName) : overloadStylePoints;

            int curMax = 0;

            var styleTable = DataTables.instance.styleCapacity;
            foreach (var t in styleTable)
            {
                curMax = t.stylePoints;

                if (stylePoints < t.stylePoints)
                    break;
            }

            return curMax;
        }
           
        public int GetStyleCapacity(string roomName)
        {
            if (persistentData.roomsData == null)
                return 0;

            if (persistentData.roomsData.ContainsKey(roomName))
            {
                if (gameCore.playerProfile.level == 1)
                    return 1;
                else if (gameCore.playerProfile.level == 2)
                    return 2;

                int stylePoints = GetTotalStylePoints (roomName);

                int level = GetStyleLevel(stylePoints);
                return GetCapacityByStyleLevel(level);
            }
            return 0;
        }


        public int GetStyleLevelForNextVisitor(int curStylePoints, string roomName) //---v
        {
            int sLevel = GetStyleLevel(curStylePoints);
            int cap = GetCapacityByStyleLevel(sLevel);

            var capacityTable = DataTables.instance.capacityLevels;

            foreach (var t in capacityTable)
            {
                if (t.level > sLevel && t.capacity > cap)
                    return t.level;
            }

            return 0;
        }

        /*
        public int GetMaxStyleLevel(string roomName, int expandLevel = -1) // ---new
        {
            if (persistentData.roomsData == null)
                return 0;

            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                int expandLvl = expandLevel > 0 ? expandLevel : roomData.expandLevel;

                RoomExpandInfo roomExpandInfo = null;

                // Get the last expand level, starting from current and counting to 0.
                while (!DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(expandLvl, roomName), out roomExpandInfo) && expandLvl >= 0)
                    expandLvl--; 

                if (roomExpandInfo != null)
                {
                    return roomExpandInfo.maxStyleLevel;
                }
            }

            return 0; 
        }
        */

        public int GetMaxStylePointsForLevel(int level) // ---new
        {
            int result = 0;

            foreach (var i in DataTables.instance.styleCapacity)
            {
                /*
                if (i.level == level)
                    return i.stylePoints;
                else
                */

                if (i.level > level)
                    return result;

                result = i.stylePoints;
            }

            return result;
        }

        public int GetStyleLevel(int stylePoints)
        {
            int level = 0;
            var styleTable = DataTables.instance.styleCapacity;
            foreach (var t in styleTable)
            {
                level = t.level;

                if (t.stylePoints > stylePoints)
                    break;
            }           

            return level;
        }

        public int GetCapacityByStyleLevel(int level)
        {
            int cap = 0;
            var capTable = DataTables.instance.capacityLevels;
            foreach (var t in capTable)
            {
                if (level < t.level)
                    break;

                cap = t.capacity;
            }           

            return cap;
        }

        public int GetFirstExpandLevel(string roomName)
        {
            if(roomName == null || gameCore == null)
                return 0; 

            var expandData = DataTables.instance.expand;

            if (expandData == null)
                return 0;

            RoomExpandInfo roomExpandInfo = null;
            if (expandData.TryGetValue(RoomExpandInfo.CreateKey(1, roomName), out roomExpandInfo))
            {
                return roomExpandInfo.unlockLevel;
            }

            return 0;
        }

    }
}

