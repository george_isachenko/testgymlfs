﻿using Core.EventSystem;
using Data.Room;

namespace Logic.Rooms.Events
{
    public class RoomSizeUpdatedEvent : StaticEvent<RoomSizeUpdatedEvent.Delegate, string, RoomData, IRoomSetupDataInfo>
    {
        public delegate void Delegate (string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupDataInfo);
    }

    public class RoomDeactivatedEvent : StaticEvent<RoomDeactivatedEvent.Delegate, string>
    {
        public delegate void Delegate(string roomName);
    }

    public class RoomActivatedEvent : StaticEvent<RoomActivatedEvent.Delegate, string>
    {
        public delegate void Delegate(string roomName);
    }

    public class WallSetEvent : StaticEvent<WallSetEvent.Delegate, string, int>
    {
        public delegate void Delegate(string roomName, int idx);
    }

    public class FloorSetEvent : StaticEvent<FloorSetEvent.Delegate, string, int>
    {
        public delegate void Delegate(string roomName, int idx);
    }
}