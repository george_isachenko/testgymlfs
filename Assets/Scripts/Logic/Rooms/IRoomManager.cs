﻿using Core.Timer;
using Data.Room;
using Data;
using Logic.Shop;
using Logic.Persons;

namespace Logic.Rooms
{
    public delegate void OnRoomUnlockComplete (string roomName);

    public interface IRoomManager
    {
        event OnRoomUnlockComplete onRoomUnlockComplete;

        // Common API with ILogicClub
        string activeRoomName
        {
            get;
        }

        IShopPurchaseProtocol purchaseProtocol
        {
            get;
        }

        RoomCapacity roomCapacity
        {
            get;
        }

        string GetRoomNameOfType(RoomType roomType);
        bool TransitionToRoomRequest(string roomName);
        void AllowRoomsActivation(bool allow);
        int GetRoomSizeInCells(string roomName);
        RoomCoordinates GetRoomDimensionsInCells(string roomName);
        RoomCoordinates GetRoomDimensionsNative(string roomName);

        bool IsRoomAtInitialSize(string roomName);
        int GetNextExpandUnlockLevel(string roomName);
        int GetExpandUnlockLevel(string roomName, int expandStage);
        Cost GetExpandCost(string roomName, int expandStage);
        Cost GetExpandCost(string roomName);
        int GetRoomExpandStage(string roomName);
        TimerFloat GetUnlockTimer(string roomName);
        int GetRoomZonesCount (string roomName, string zoneNamePrefix);
        bool IsRoomCoordinatesApproximatelyWithinRoom(string roomName, RoomCoordinates coordinates);

        bool GetPersonQueueCandidateZoneName (Person person, out string queueZoneName, out bool run);
        int GetNumberOfPersonsInQueue(string roomName);
        int GetRoomCurrentQueueMaxPlaces(string roomName, bool autoFillLimit = true);
    }
}