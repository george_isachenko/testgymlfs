﻿using System;
using System.Collections;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Timer;
using Data;
using Data.Room;
using Logic.PlayerProfile.Events;
using Logic.Rooms.Events;
using Logic.Serialization;
using Presentation.Facades.Interfaces.Club;
using UnityEngine;
using UnityEngine.Assertions;
using Core.Analytics;
using Logic.Core;
using Logic.Persons;
using Data.Person;
using System.Collections.Generic;

namespace Logic.Rooms.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Rooms/Facade/Logic Room Manager Game")]
    public class LogicRoomManagerGame : LogicRoomManagerBase
    {
        #region Static readonly data.
        private static readonly string queueZoneNamePrefix = "queue_";
        #endregion

        #region Properties.
        public override bool isEditMode
        {
            get
            {
                return gameCore.equipmentManager.isInEditMode;
            }
        }
        #endregion

        #region IShopPurchaseProtocol properties
        public override ShopItemData itemToBuy
        {
            get
            {
                return itemToBuy_;
            }
        }
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected float queueUpdateRate = 2.0f;
        #endregion

        #region Private data.
        protected ShopItemData itemToBuy_;
        protected KeyValuePair<string, RoomData>? wallFloorPreviewRoom;
        protected IEnumerator roomsExpandAvailabilityUpdatedJob;
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected override void Subscribe(IPresentationClub presentationClub)
        {
            base.Subscribe(presentationClub);

            RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;
            LevelUpEvent.Event += OnLevelUp;
            PlayerProfileUpdatedEvent.Event += OnPlayerProfileUpdatedEvent;
        }

        [BindBridgeUnsubscribe]
        protected override void Unsubscribe(IPresentationClub presentationClub)
        {
            base.Unsubscribe(presentationClub);

            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;
            LevelUpEvent.Event -= OnLevelUp;
            PlayerProfileUpdatedEvent.Event -= OnPlayerProfileUpdatedEvent;
        }
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            gameCore.onGameStarted += OnGameStarted;
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            if (roomsExpandAvailabilityUpdatedJob != null)
            {
                StopCoroutine(roomsExpandAvailabilityUpdatedJob);
                roomsExpandAvailabilityUpdatedJob = null;
            }
        }
        #endregion

        #region Public API.
        public override void FinishEditMode()
        {
            gameCore.equipmentManager.EditEquipmentRequest(Data.Estate.EquipmentData.InvalidId);
        }

        public override int GetExpandStep(string roomName)
        {
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi))
            {
                return rsdi.expandStep;
            }

            return 0;
        }

        public override bool CanExpandX(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(roomData.expandLevel, roomName), out roomExpandInfo))
                {
                    return (roomExpandInfo.expandWidth > 0);
                }
            }
            return false;
        }

        public override bool CanExpandY(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(roomData.expandLevel, roomName), out roomExpandInfo))
                {
                    return (roomExpandInfo.expandHeight > 0);
                }
            }
            return false;
        }

        public override Cost GetExpandCost(string roomName, int expandStage)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(expandStage - 1, roomName), out roomExpandInfo))
                {
                    return CreateCost(roomExpandInfo);
                }
            }
            return null;
        }

        public override Cost GetExpandCost(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(roomData.expandLevel, roomName), out roomExpandInfo))
                {
                    if (gameCore.playerProfile.level >= roomExpandInfo.unlockLevel)
                    {
                        return CreateCost(roomExpandInfo);
                    }
                }
            }
            return null;
        }

        public override TimerFloat GetUnlockTimer(string roomName)
        {
            RoomData roomData;
            return persistentData.roomsData.TryGetValue(roomName, out roomData) ? roomData.expandUnlockTimer : null;
        }

        public override int GetNextExpandUnlockLevel(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(roomData.expandLevel, roomName), out roomExpandInfo))
                {
                    return roomExpandInfo.unlockLevel;
                }
            }
            return -1;
        }

        public override int GetExpandUnlockLevel(string roomName, int expandStage)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo roomExpandInfo;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(expandStage - 1, roomName), out roomExpandInfo))
                {
                    return roomExpandInfo.unlockLevel;
                }
            }
            return -1;
        }

        public override bool TryExpandRequest(string roomName, int blockIndex)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                if (rsdi.roomExpandMode == RoomExpandMode.SideBySide)
                {
                    bool canX = (roomData.currentSize.x + rsdi.expandStep <= rsdi.maxSizeInCells.x);
                    bool canY = (roomData.currentSize.y + rsdi.expandStep <= rsdi.maxSizeInCells.y);

                    if (!canX && !canY)
                    {   
                        Debug.LogErrorFormat("Cannot expand room, already reached maximum size (Room name: {0})."
                            , roomName);
                        return false;   // Already reached max size.
                    }

                    var cost = GetExpandCost(roomName);
                    if (gameCore.playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Expand", "ExpandLevel " + roomData.expandLevel.ToString()), true))
                    {
                        if (CanExpandX(roomName))
                            roomData.currentSize.x += rsdi.expandStep;
                        if (CanExpandY(roomName))
                            roomData.currentSize.y += rsdi.expandStep;

                        PersistentCore.instance.analytics.GameEvent(roomName + "Expand",
                            new GameEventParam("Level", gameCore.playerProfile.level),
                            new GameEventParam("Expand level", roomData.expandLevel.ToString()));

                        ExpandOrUnlockComplete(roomData, rsdi);
                        return true;
                    }
                }
                else if (rsdi.roomExpandMode == RoomExpandMode.BlockGrid)
                {
                    if (roomData.expandLevel >= rsdi.maxSizeInBlocks.GetSquare() + 1)
                    {   // Already reached max size.
                        Debug.LogErrorFormat("Cannot expand room, already reached maximum size (Room name: {0}).", roomName);
                        return false;
                    }

                    if (blockIndex < 0 || blockIndex >= rsdi.maxSizeInBlocks.GetSquare())
                    {   // Already reached max size.
                        Debug.LogErrorFormat("Cannot expand room, requested expand block index is wrong (Room name: {0}).", roomName);
                        return false;
                    }

                    if (roomData.blocks == null || blockIndex >= roomData.blocks.Length)
                    {
                        Debug.LogErrorFormat("Cannot expand room, wrong setup of room blocks!!! Probable data corruption. (Room name: {0}).", roomName);
                        return false;
                    }

                    if (roomData.blocks[blockIndex].unlocked)
                    {
                        Debug.LogErrorFormat("Cannot expand room, block is already unlocked!!! Probable data corruption. (Room name: {0}).", roomName);
                        return false;
                    }

                    var cost = GetExpandCost(roomName);
                    if (gameCore.playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Expand", "ExpandLevel " + roomData.expandLevel.ToString()), true))
                    {
                        roomData.blocks[blockIndex].unlocked = true;

                        PersistentCore.instance.analytics.GameEvent(roomName + "Expand",
                            new GameEventParam("Level", gameCore.playerProfile.level),
                            new GameEventParam("Expand level", roomData.expandLevel.ToString()));

                        ExpandOrUnlockComplete(roomData, rsdi);
                        return true;
                    }
                }
                else
                {
                    Debug.LogErrorFormat("Expand request is unsupported for expand mode {0} (Room name: {1})."
                        , rsdi.roomExpandMode, roomName);
                    return false;
                }
            }

            return false;
        }

        public override bool TryUnlockRequest(string roomName)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                RoomExpandInfo info;
                if (DataTables.instance.expand.TryGetValue(RoomExpandInfo.CreateKey(roomData.expandLevel, roomName), out info))
                {
                    if (roomData.expandUnlockTimer == null)
                    {
                        var cost = GetExpandCost(roomName);
                        if (gameCore.playerProfile.SpendMoney(cost, new Analytics.MoneyInfo(roomName, "Unlock " + roomData.expandLevel.ToString()), true))
                        {
                            if (info.duration > TimeSpan.Zero)
                            {
                                roomData.expandUnlockTimer = new TimerFloat((float) info.duration.TotalSeconds);

                                Debug.LogFormat("Set unlock timer of {0} for expand level {1} (Room name: {2})."
                                    , info.duration.TotalSeconds
                                    , roomData.expandLevel
                                    , roomName);
                                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                                PersistentCore.instance.analytics.GameEvent(roomName + "Expand",
                                    new GameEventParam("Level", gameCore.playerProfile.level),
                                    new GameEventParam("Expand level", roomData.expandLevel.ToString()));

                                if (rsdi.roomType == RoomType.JuiceBar)
                                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.JuiceBarUnlock);//Start Juice Bar Unlock
                            }
                            else
                            {   // Immediate expand.
                                ExpandOrUnlockComplete(roomData, rsdi);
                            }

                            return true;
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat("Invalid request, unlock is already pending for this room (Room name: {0})."
                            , roomName);
                        return false;
                    }
                }
                else
                {
                    Debug.LogErrorFormat("Cannot get expand info from Expand data table for expand level {0}, probably size reached maximum (Room name: {1})."
                        , roomData.expandLevel, roomName);
                    return false;
                }
            }

            return false;
        }

        public override bool CompleteUnlockRequest(string roomName)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsd;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsd) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                if (roomData.expandUnlockTimer != null)
                {
                    if (roomData.expandUnlockTimer.secondsToFinish > 0)
                    {
                        var skipCostAmount = DynamicPrice.GetSkipCost((int) roomData.expandUnlockTimer.secondsToFinish);
                        if (skipCostAmount > 0)
                        {
                            var skipCost = new Cost(Cost.CostType.BucksOnly, skipCostAmount, 0);
                            if (!playerProfile.SpendMoney(skipCost, new Analytics.MoneyInfo("Skip", string.Format("{0}UnlockDuration", roomName))))
                                return false;
                        }
                    }

                    roomData.expandUnlockTimer = null;
                    ExpandOrUnlockComplete(roomData, rsd);

                    SendOnRoomUnlockCompleteEvent(roomName);
                    return true;
                }
                else
                {
                    Debug.LogErrorFormat("There is no pending unlock request for this room (Room name: {0})."
                        , roomName);
                    return false;
                }
            }
            return false;
        }

        public override bool GetPersonQueueCandidateZoneName (Person person, out string queueZoneName, out bool run)
        {
            Assert.IsNotNull(person);

            if (person != null && person.state == PersonState.Entering) // It must be in initial (Entering) state, not in queue already.
            {
                IRoomSetupDataInfo rsdi;
                if (persistentData.roomsData.ContainsKey(person.roomName) && roomsSetupDataInfo.TryGetValue(person.roomName, out rsdi))
                {
                    var numberOfPersonsInQueue = GetNumberOfPersonsInQueue(rsdi);
                    if (numberOfPersonsInQueue < GetRoomCurrentQueueMaxPlaces(rsdi, false))
                    {
                        queueZoneName = queueZoneNamePrefix + numberOfPersonsInQueue.ToString();
                        run = (numberOfPersonsInQueue < rsdi.queueRunThreshold);

                        if (rsdi.autoFillUpQueueLimit == 0 && rsdi.autoFillUpRoomLimit == 0)
                        {   // There's no Queue job for this room yet, so start it now.
                            StartCoroutine(QueueFillAndEntranceJob(rsdi));
                        }

                        return true;
                    }
                }
            }

            queueZoneName = null;
            run = false;
            return false;
        }

        public override int GetNumberOfPersonsInQueue(string roomName)
        {
            IRoomSetupDataInfo rsdi;
            if (persistentData.roomsData.ContainsKey(roomName) && roomsSetupDataInfo.TryGetValue(roomName, out rsdi))
            {
                return GetNumberOfPersonsInQueue(rsdi);
            }
            return 0;
        }

        public override int GetRoomCurrentQueueMaxPlaces(string roomName, bool autoFillLimit)
        {
            IRoomSetupDataInfo rsdi;
            if (persistentData.roomsData.ContainsKey(roomName) && roomsSetupDataInfo.TryGetValue(roomName, out rsdi))
            {
                return GetRoomCurrentQueueMaxPlaces(rsdi, autoFillLimit);
            }
            return 0;
        }
        #endregion

        #region IShopPurchaseProtocol interface.
        public override bool InitPreview(ShopItemData item, Action<bool> OnFinishedPreview)
        {
            if (_activeRoom != null && item.category == ShopItemCategory.PAINTS && !gameCore.shopManager.IsItemOwned(item))
            {
                itemToBuy_ = item;
                wallFloorPreviewRoom = _activeRoom;

                presenter.SetWall(_activeRoom.Value.Key, item.id);
                presenter.SetFloor(_activeRoom.Value.Key, item.id);
                return true;
            }
            else
                return false;
        }

        public override bool ConfirmPurchase()
        {
            if (itemToBuy_ != null && _activeRoom != null)
            {
                if (!gameCore.shopManager.HasOwnerhsip(itemToBuy_))
                {
                    if (gameCore.playerProfile.SpendMoney(itemToBuy_.price, new Analytics.MoneyInfo("Paint", "paint_" + itemToBuy_.id.ToString())))
                    {
                        gameCore.shopManager.RegisterOwnership(itemToBuy_);

                        if (presenter.IsIdxWall(_activeRoom.Value.Key, itemToBuy_.id))
                        {
                            _activeRoom.Value.Value.wallId = itemToBuy_.id;
                            WallSetEvent.Send(_activeRoom.Value.Key, _activeRoom.Value.Value.wallId);
                            presenter.SetWall(_activeRoom.Value.Key, _activeRoom.Value.Value.wallId);
                        }

                        if (presenter.IsIdxFloor(_activeRoom.Value.Key, itemToBuy_.id))
                        {
                            _activeRoom.Value.Value.floorId = itemToBuy_.id;
                            FloorSetEvent.Send(_activeRoom.Value.Key, _activeRoom.Value.Value.floorId);
                            presenter.SetFloor(_activeRoom.Value.Key, _activeRoom.Value.Value.floorId);
                        }

                        itemToBuy_ = null;
                        wallFloorPreviewRoom = null;
                        return true;
                    }
                }
                else
                {
                    Debug.LogErrorFormat("LogicClub: IShopPurchaseProtocol.ConfirmPurchase: item '{0}' is already purchased.", itemToBuy_.name);
                }
            }
            else
            {
                Debug.LogError("LogicClub: IShopPurchaseProtocol.ConfirmPurchase: itemToBuy is null.");
            }
            return false;
        }

        public override void CancelPurchase()
        {
            Assert.IsNotNull(itemToBuy_);
            if (wallFloorPreviewRoom != null)
            {
                presenter.SetWall(wallFloorPreviewRoom.Value.Key, wallFloorPreviewRoom.Value.Value.wallId);
                presenter.SetFloor(wallFloorPreviewRoom.Value.Key, wallFloorPreviewRoom.Value.Value.floorId);
            }
            itemToBuy_ = null;
        }

        public override void RestoreOwnedItem(ShopItemData item)
        {
            if (item.category == ShopItemCategory.PAINTS)
            {
                if (presenter.IsIdxWall(_activeRoom.Value.Key, item.id))
                    _activeRoom.Value.Value.wallId = item.id;
                if (presenter.IsIdxFloor(_activeRoom.Value.Key, item.id))
                    _activeRoom.Value.Value.floorId = item.id;

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                presenter.SetWall(_activeRoom.Value.Key, _activeRoom.Value.Value.wallId);
                presenter.SetFloor(_activeRoom.Value.Key, _activeRoom.Value.Value.floorId);
            }
        }
        #endregion

        #region Protected virtual API.
        protected override void RoomsPostInit()
        {
            base.RoomsPostInit();

            ScheduleRoomsExpandAvailabilityUpdatedEvent();
        }
        #endregion

        #region Private functions.
        void ExpandOrUnlockComplete(RoomData roomData, IRoomSetupDataInfo rsdi)
        {
            roomData.expandLevel++;

            bool[] unlockedBlocks = roomData.blocks != null
                    ? roomData.blocks.Select(x => x.unlocked).ToArray()
                    : null;

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            RoomSizeUpdatedEvent.Send(rsdi.name, roomData, rsdi);

            presenter.SetSize(rsdi.name, roomData.expandLevel, roomData.currentSize, unlockedBlocks, true);

            ScheduleRoomsExpandAvailabilityUpdatedEvent();
        }

        void OnRoomSizeUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo rsdi)
        {
            if (rsdi.roomType == RoomType.SportClub && roomData.expandLevel == 1)
            {
                for (int i = 0; i < 1; i++)
                {
                    gameCore.sportsmen.CreateSportsman
                        (SportsmanType.Sportsman_0
                        , UnityEngine.Random.value < 0.5f
                            ? PersonAppearance.Gender.Male
                            : PersonAppearance.Gender.Female
                        ,new Analytics.MoneyInfo("", ""));
                }

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            }
        }

        void OnLevelUp(int level)
        {
            ScheduleRoomsExpandAvailabilityUpdatedEvent();
        }

        void OnPlayerProfileUpdatedEvent()
        {
            ScheduleRoomsExpandAvailabilityUpdatedEvent();
        }

        void ScheduleRoomsExpandAvailabilityUpdatedEvent()
        {
            // The idea is to collect all requests made in one frame (there can be really many)
            // into one and send only one event via one-shot Coroutine (between Update() and LateUpdate()).
            // We also want to send event after all of them are made, not after first, that's why Coroutine instead of bool flags.
            if (roomsExpandAvailabilityUpdatedJob == null)
            {
                roomsExpandAvailabilityUpdatedJob = SendRoomsExpandAvailabilityUpdatedEventJob();
                StartCoroutine(roomsExpandAvailabilityUpdatedJob);
            }
        }

        IEnumerator SendRoomsExpandAvailabilityUpdatedEventJob()
        {
            yield return 0; // Skip first immediate call.

            SendOnRoomsExpandAvailabilityUpdatedEvent();

            roomsExpandAvailabilityUpdatedJob = null;
            yield break;
        }

        void OnGameStarted(bool isFreshInstall)
        {
            gameCore.onGameStarted -= OnGameStarted;

            foreach (var rsdi in roomsSetupDataInfo.Values)
            {
                if (persistentData.roomsData.ContainsKey(rsdi.name) && (rsdi.autoFillUpQueueLimit > 0 || rsdi.autoFillUpRoomLimit > 0))
                {
                    StartCoroutine(QueueFillAndEntranceJob(rsdi));
                }
            }
        }

        private IEnumerator QueueFillAndEntranceJob (IRoomSetupDataInfo rsdi)
        {
            Debug.LogFormat("Starting queue fill and entrance job for room '{0}'.", rsdi.name);

            Assert.IsNotNull(rsdi);

            float lastSpawnTime = -1.0f;

            yield return null;

            // Initial queue update.
            UpdateQueue(rsdi);

            if (rsdi.delayQueueStart > 0)
                yield return new WaitForSeconds(rsdi.delayQueueStart);
            else
                yield return null; // Wait another frame.

            while (true)
            {
                var personInQueue = GetFirstPersonInQueue(rsdi);
                if (personInQueue != null && IsRoomNeedsPerson(rsdi))
                {
                    personInQueue.EnterRoom(Person.EntranceMode.ImmediateEntranceFast);
                    UpdateQueue(rsdi);
                }
                else if (!rsdi.noSpawnWhenTutorialRunning || !gameCore.quests.tutorial.isRunning)
                {
                    if (IsRoomNeedsPersonAutoFillUp(rsdi))
                    {
                        SpawnPerson(rsdi, Person.EntranceMode.ImmediateEntranceFast);
                    }
                    else
                    {
                        var numberOfPersonsInQueue = GetNumberOfPersonsInQueue(rsdi);
                        var maxNumberOfSlots = GetRoomCurrentQueueMaxPlaces(rsdi, true);
                        if (numberOfPersonsInQueue < maxNumberOfSlots)
                        {
                            if (lastSpawnTime < 0 || numberOfPersonsInQueue == 0 ||
                                gameCore.playerProfile.level < rsdi.adaptiveSpawnRateStartLevel ||
                                UnityEngine.Random.value < (Time.time - lastSpawnTime) * (rsdi.queueSpawnChancePerFreeSlotPerMinute / 60.0f) * (maxNumberOfSlots - numberOfPersonsInQueue))
                            {
                                lastSpawnTime = Time.time;

                                SpawnPerson(rsdi, Person.EntranceMode.GoToQueue);
                            }
                        }
                    }
                }

                yield return new WaitForSeconds(queueUpdateRate);
            }
        }

        private int GetRoomCurrentQueueMaxPlaces(IRoomSetupDataInfo rsdi, bool autoFillLimit)
        {
            switch (rsdi.roomType)
            {
                case RoomType.Club:
                {
                    var potentialPlaces = GetRoomZonesCount(rsdi.name, queueZoneNamePrefix);
                    if (potentialPlaces <= 0)
                        return 0;

                    return Math.Max(0, Math.Min(autoFillLimit ? rsdi.autoFillUpQueueLimit : int.MaxValue, Math.Min(potentialPlaces, gameCore.quests.visitorQuests.quests.Count)));
                }

                case RoomType.JuiceBar:
                {
                    var potentialPlaces = GetRoomZonesCount(rsdi.name, queueZoneNamePrefix);
                    if (potentialPlaces <= 0)
                        return 0;

                    return Math.Max(0, Math.Min(autoFillLimit ? rsdi.autoFillUpQueueLimit : int.MaxValue, Math.Min(potentialPlaces, gameCore.juiceBar.GetMaxClientsCount())));
                }
            }
            return 0;
        }

        private bool IsRoomNeedsPersonAutoFillUp(IRoomSetupDataInfo rsdi)
        {
            switch (rsdi.roomType)
            {
                case RoomType.Club:
                {
                    return (rsdi.autoFillUpRoomLimit > 0)
                        ? gameCore.quests.visitorQuests.quests.Count < Math.Min(rsdi.autoFillUpRoomLimit, roomCapacity.GetStyleCapacity(rsdi.name))
                        : false;
                }

                case RoomType.JuiceBar:
                {
                    return (rsdi.autoFillUpRoomLimit > 0)
                        ? gameCore.juiceBar.GetCurrentClientsCount() < Math.Min(rsdi.autoFillUpRoomLimit, gameCore.juiceBar.GetMaxClientsCount())
                        : false;
                }
            }
            return false;
        }

        private bool IsRoomNeedsPerson(IRoomSetupDataInfo rsdi)
        {
            switch (rsdi.roomType)
            {
                case RoomType.Club:
                {
                    return gameCore.quests.visitorQuests.quests.Count < roomCapacity.GetStyleCapacity(rsdi.name);
                }

                case RoomType.JuiceBar:
                {
                    return gameCore.juiceBar.GetEnteredClientsCount() < gameCore.juiceBar.GetMaxClientsCount();
                }
            }
            return false;
        }

        private Person SpawnPerson(IRoomSetupDataInfo rsdi, Person.EntranceMode entranceMode)
        {
            switch (rsdi.roomType)
            {
                case RoomType.Club:
                {
                    return gameCore.personManager.CreateVisitor(rsdi.name, entranceMode);
                }

                case RoomType.JuiceBar:
                {
                    return gameCore.personManager.CreateJuicer(rsdi.name, entranceMode);
                }
            }
            return null;
        }

        private int GetNumberOfPersonsInQueue(IRoomSetupDataInfo rsdi)
        {
            return gameCore.personManager.persons.Values.Where(x => x.state == PersonState.InQueue && x.roomName == rsdi.name).Count();
        }

        private Person GetFirstPersonInQueue(IRoomSetupDataInfo rsdi)
        {
            return gameCore.personManager.persons.Values.Where(x => x.state == PersonState.InQueue && x.roomName == rsdi.name).OrderBy(x => x.spawnTime).FirstOrDefault();
        }

        private void UpdateQueue (IRoomSetupDataInfo rsdi)
        {
            var personsInQueue = gameCore.personManager.persons.Values.Where(x => x.state == PersonState.InQueue && x.roomName == rsdi.name).OrderBy(x => x.spawnTime);
            if (personsInQueue != null)
            {
                var counter = 0;
                foreach (var person in personsInQueue)
                {
                    var queueZoneName = queueZoneNamePrefix + counter.ToString();

                    person.MoveInQueue(queueZoneName);

                    counter++;
                }
            }
        }
        #endregion
    }
}