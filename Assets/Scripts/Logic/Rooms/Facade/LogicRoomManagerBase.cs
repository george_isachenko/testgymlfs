﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core.Timer;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.FacadesBase;
using Logic.Persons;
using Logic.Serialization;
using Logic.Shop;
using Presentation.Facades.Interfaces.Club;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Rooms.Facade
{
    [BindBridgeInterface(typeof(IPresentationClub))]
    public abstract class LogicRoomManagerBase
        : BaseLogicFacade
        , ILogicRoomManager
        , IRoomManager
        , IShopPurchaseProtocol
        , IPersistent
    {
        #region Static/readonly data.
        static readonly int initialRoomDataCapacity = 8;
        #endregion

        #region ILogicRoomManager events.
        public event RoomDeactivatedEvent onRoomDeactivatedEvent;
        public event RoomActivatedEvent onRoomActivatedEvent;
        public event RoomsExpandAvailabilityUpdatedEvent onRoomsExpandAvailabilityUpdatedEvent;
        public event RoomUnlockCompleteEvent onRoomUnlockCompleteEvent;
        #endregion

        #region IRoomManager events.
        public event OnRoomUnlockComplete onRoomUnlockComplete;
        #endregion

        #region Properties.
        public string activeRoomName
        {
            get
            {
                return (_activeRoom != null) ? _activeRoom.Value.Key : string.Empty;
            }
        }

        IShopPurchaseProtocol IRoomManager.purchaseProtocol
        {
            get
            {
                return this;
            }
        }

        public virtual bool isEditMode
        {
            get
            {
                return false;
            }
        }

        public RoomCapacity roomCapacity
        {
            get;
            private set;
        }
        #endregion

        #region IShopPurchaseProtocol properties
        public virtual ShopItemData itemToBuy
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region IPersistent properties.
        string IPersistent.propertyName
        {
            get
            {
                return "Rooms";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(RoomsPersistentData);
            }
        }
        #endregion

        #region Private data.
        protected IPresentationClub presenter;
        protected Dictionary<string, IRoomSetupDataInfo> roomsSetupDataInfo;
        protected string initialActiveRoomName;
        protected KeyValuePair<string, RoomData>? _activeRoom;
        protected bool allowRoomsActivation = true;
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected float passerbySpawnCheckRate = 5.0f;

        [SerializeField]
        protected float passerbySpawnWarmupCheckRate = 0.5f;
        #endregion

        #region Persistent Data.
        protected RoomsPersistentData persistentData = new RoomsPersistentData();
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        protected virtual void Subscribe(IPresentationClub presentationClub)
        {
            presenter = presentationClub;
        }

        [BindBridgeUnsubscribe]
        protected virtual void Unsubscribe(IPresentationClub presentationClub)
        {
            presenter = null;
        }
        #endregion

        #region Unity API.
        protected void Awake()
        {
            roomCapacity = new RoomCapacity(gameCore, persistentData); 

            gameCore.onGameStarted += OnGameStarted;

            Reinit();
        }

        protected void OnDestroy()
        {
        }
        #endregion

        #region Public API.
        public string GetRoomNameOfType(RoomType roomType)
        {
            return (from rsd in roomsSetupDataInfo
                    where rsd.Value.roomType == roomType
                    select rsd.Key).FirstOrDefault();
        }

        public virtual void FinishEditMode()
        {
        }

        public int GetPlayerFitPointsCount()
        {
            return playerProfile.GetFitPointsCount();
        }

        public void RegisterRooms(IList<IRoomSetupDataInfo> roomsSetupDataInfoList)
        {
            //Debug.Log("LogicClub.RegisterRooms:");
            Assert.IsNotNull(roomsSetupDataInfoList);
            Assert.IsNull(roomsSetupDataInfo);

            roomsSetupDataInfo = new Dictionary<string, IRoomSetupDataInfo>(roomsSetupDataInfoList.Count);

            foreach (var rsdi in roomsSetupDataInfoList)
            {
                roomsSetupDataInfo.Add(rsdi.name, rsdi); 
            }

/*
            this.roomsSetupDataInfo = roomsSetupDataInfoList.ToDictionary
                ((KeyValuePair<string, IRoomSetupDataInfo> rsd1) => rsd1.name, (KeyValuePair<string, IRoomSetupDataInfo> rsd2) => rsd2);
*/
            initialActiveRoomName = roomsSetupDataInfoList[0].name;

            // BindRooms();
        }

        public bool TransitionToRoomRequest(string roomName)
        {
            if (persistentData.roomsData.ContainsKey(roomName))
            {
                return presenter.TransitionToRoomRequest(roomName);
            }
            return false;
        }

        public bool DeactivateRoomRequest(string roomName)
        {
            if (_activeRoom == null || roomName != _activeRoom.Value.Key)
            {
                // Debug.LogFormat("Invalid request to deactivate room '{0}'.", roomName);
                return false; // Invalid request.
            }

            if (!allowRoomsActivation)
                return false;

            _activeRoom = null;

            Events.RoomDeactivatedEvent.Send(roomName);

            onRoomDeactivatedEvent?.Invoke(roomName);

            return true;
        }

        public bool ActivateRoomRequest(string roomName)
        {
            if (_activeRoom != null && roomName == _activeRoom.Value.Key)
            {
                // Debug.LogWarningFormat("Room '{0}' is already active.", roomName);
                return true;
            }

            if (!allowRoomsActivation)
                return false;

            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                if (roomData.expandLevel > 0)
                {
                    var previousActiveRoomName = (_activeRoom != null) ? _activeRoom.Value.Key : null;
                    _activeRoom = new KeyValuePair<string, RoomData>(roomName, roomData);

                    if (previousActiveRoomName != null)
                    {
                        Events.RoomDeactivatedEvent.Send(previousActiveRoomName);
                        onRoomDeactivatedEvent?.Invoke(previousActiveRoomName);
                    }

                    Events.RoomActivatedEvent.Send(roomName);
                    onRoomActivatedEvent?.Invoke(roomName);

                    return true;
                }
                else
                {
                    Debug.LogFormat("Room '{0}' is locked and cannot be activated yet.", roomName);
                }
            }
            return false;
        }

        public void AllowRoomsActivation(bool allow)
        {
            allowRoomsActivation = allow;
        }

        public int GetRoomSizeInCells(string roomName)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                if (rsdi.roomExpandMode == RoomExpandMode.BlockGrid)
                {
                    return Math.Max(0, roomData.expandLevel - 1) * rsdi.blockSize * rsdi.blockSize;
                }
                else
                {
                    return roomData.currentSize.x * roomData.currentSize.y;
                }
            }

            return 0;
        }

        public int GetRoomSizeInCells(string roomName, int atExpandLevel)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                if (rsdi.roomExpandMode == RoomExpandMode.BlockGrid)
                {
                    return atExpandLevel * rsdi.blockSize * rsdi.blockSize;
                }
                else
                {
                    return 0; // TODO: Calculate this!
                }
            }

            return 0;
        }

        public RoomCoordinates GetRoomDimensionsInCells(string roomName)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsdi;
            if (roomsSetupDataInfo.TryGetValue(roomName, out rsdi) && persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                return roomData.currentSize;
            }

            return RoomCoordinates.zero;
        }

        public RoomCoordinates GetRoomDimensionsNative(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                return roomData.currentSize;
            }

            return RoomCoordinates.zero;
        }

        public virtual int GetExpandStep(string roomName)
        {
            return 0;
        }

        public virtual bool CanExpandX(string roomName)
        {
            return false;
        }

        public virtual bool CanExpandY(string roomName)
        {
            return false;
        }

        public virtual int GetRoomExpandStage(string roomName)
        {
            RoomData roomData;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData))
            {
                return roomData.expandLevel;
            }
            return -1;
        }

        public virtual Cost GetExpandCost(string roomName, int expandStage)
        {
            return null;
        }

        public virtual Cost GetExpandCost(string roomName)
        {
            return null;
        }

        public virtual TimerFloat GetUnlockTimer(string roomName)
        {
            return null;
        }

        public virtual int GetNextExpandUnlockLevel(string roomName)
        {
            return -1;
        }

        public virtual int GetExpandUnlockLevel(string roomName, int expandStage)
        {
            return -1;
        }

        public virtual bool TryExpandRequest(string roomName, int blockIndex)
        {
            return false;
        }

        public virtual bool TryUnlockRequest(string roomName)
        {
            return false;
        }

        public virtual bool CompleteUnlockRequest(string roomName)
        {
            return false;
        }

        public virtual bool IsRoomAtInitialSize(string roomName)
        {
            RoomData roomData;
            IRoomSetupDataInfo rsd;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData) && roomsSetupDataInfo.TryGetValue(roomName, out rsd))
            {
                return (roomData.expandLevel <= GetInitialExpandLevel(roomName));
            }
            return true;
        }

        public int GetRoomZonesCount (string roomName, string zoneNamePrefix)
        {
            if (persistentData.roomsData.ContainsKey(roomName))
            {
                return presenter.GetRoomZonesCount(roomName, zoneNamePrefix);
            }
            else
            {
                return -1;
            }
        }

        public bool IsRoomCoordinatesApproximatelyWithinRoom(string roomName, RoomCoordinates coordinates)
        {
            RoomData roomData;
            // IRoomSetupDataInfo rsd;
            if (persistentData.roomsData.TryGetValue(roomName, out roomData)/* && roomsSetupDataInfo.TryGetValue(roomName, out rsd)*/)
            {
                return (coordinates.isValid && coordinates.x < roomData.currentSize.x && coordinates.y < roomData.currentSize.y);
            }
            else
                return false;
        }

        public virtual bool GetPersonQueueCandidateZoneName (Person person, out string queueZoneName, out bool run)
        {
            queueZoneName = null;
            run = false;
            return false;
        }

        public virtual int GetNumberOfPersonsInQueue(string roomName)
        {
            return 0;
        }

        public virtual int GetRoomCurrentQueueMaxPlaces(string roomName, bool autoFillLimit = true)
        {
            return 0;
        }

        #endregion

        #region IShopPurchaseProtocol interface.
        public virtual bool InitPreview(ShopItemData item, Action<bool> OnFinishedPreview)
        {
            return false;
        }

        public virtual bool ConfirmPurchase()
        {
            return false;
        }

        public virtual void CancelPurchase()
        {
        }

        public virtual void RestoreOwnedItem(ShopItemData item)
        {
        }

        public virtual void BuyIAP(string iapProductId)
        {
        }
        #endregion

        #region Protected virtual API.
        protected virtual void Reinit()
        {
            persistentData.roomsData = new Dictionary<string, RoomData>(initialRoomDataCapacity);
        }

        protected virtual void RoomsPostInit()
        {
            //Debug.Log("LogicClub.RoomsPostInit:");

            _activeRoom = null;
            ActivateRoomRequest(initialActiveRoomName);
        }
        #endregion

        #region Protected API.
        protected void SendOnRoomsExpandAvailabilityUpdatedEvent()
        {
            onRoomsExpandAvailabilityUpdatedEvent?.Invoke();
        }

        protected void SendOnRoomUnlockCompleteEvent(string roomName)
        {
            onRoomUnlockComplete?.Invoke(roomName);

            onRoomUnlockCompleteEvent?.Invoke(roomName);
        }
        #endregion

        #region Protected static API.
        protected static Cost CreateCost (RoomExpandInfo roomExpandInfo)
        {
            var cost = new Cost(Cost.CostType.ResourcesAndCoins, roomExpandInfo.cost, 0);

            if (roomExpandInfo.resource1 > 0)
                cost.resources.Add(ResourceType.Expand_01, roomExpandInfo.resource1);

            if (roomExpandInfo.resource2 > 0)
                cost.resources.Add(ResourceType.Expand_02, roomExpandInfo.resource2);

            if (roomExpandInfo.resource3 > 0)
                cost.resources.Add(ResourceType.Expand_03, roomExpandInfo.resource3);

            if (roomExpandInfo.resource4 > 0)
                cost.resources.Add(ResourceType.Expand_04, roomExpandInfo.resource4);

            return cost;
        }
        #endregion

        #region Private functions.
        int GetInitialExpandLevel(string roomName)
        {
            return  (from expandEntry in DataTables.instance.expand
                    where expandEntry.Key.roomName == roomName && expandEntry.Value.cost > 0
                    orderby expandEntry.Key.expandLevel ascending
                    select expandEntry.Key.expandLevel).FirstOrDefault();
        }

        void BindRoomsData()
        {
            //Debug.Log("LogicClub.BindRoomsData:");
            Assert.IsNotNull(roomsSetupDataInfo);
            if (roomsSetupDataInfo.Count > 0)
            {
                // Check for removed rooms.
                // Note: We're currently not deleting removed rooms from persistent data, in case they will be restored back.
                foreach (var room in persistentData.roomsData)
                {
                    IRoomSetupDataInfo rsd;
                    if (!roomsSetupDataInfo.TryGetValue(room.Key, out rsd))
                    {
                        Debug.LogWarningFormat("LogicClub.BindRoomsData: There's a room with name '{0}' in savegame, but we don't have such room in a world. Ignoring it."
                            , room.Key );
                    }
                }

                // Bind new data.
                foreach (var rsdi in roomsSetupDataInfo)
                {
                    RoomData roomData;
                    if (!persistentData.roomsData.TryGetValue(rsdi.Key, out roomData))
                    {
                        roomData = new RoomData(rsdi.Value, GetInitialExpandLevel(rsdi.Key));
                        persistentData.roomsData.Add(rsdi.Key, roomData);
                    }

                    // If blocks array is empty for a room with GridBlock expand format
                    // then this is old save format where Room Blocks where not present.
                    //
                    // Migrate data:
                    // - Create blocks array.
                    // - Give user square room of the same or bigger size in blocks that correspond to old currentSize in cells.
                    // - Update currentSize, it's now in blocks instead of cells.

                    if (rsdi.Value.roomExpandMode == RoomExpandMode.BlockGrid && roomData.blocks == null)
                    {
                        roomData.blocks = new RoomData.RoomBlock[rsdi.Value.maxSizeInBlocks.GetSquare()];
                        for(int i = 0; i < roomData.blocks.Length; i++)
                            roomData.blocks[i] = new Data.Room.RoomData.RoomBlock();

                        var maxSide = Mathf.Max(roomData.currentSize.x, roomData.currentSize.y) + 1;
                        var sizeInBlocks = (maxSide / rsdi.Value.blockSize) + ((maxSide % rsdi.Value.blockSize > 0) ? 1 : 0);
                        sizeInBlocks = sizeInBlocks * sizeInBlocks;
                        var unlockedBlocksCount = Math.Max(GetInitialExpandLevel(rsdi.Key) - 1, sizeInBlocks);

                        for (int i = 0; i < unlockedBlocksCount; i++)
                        {
                            roomData.blocks[i] = new RoomData.RoomBlock();
                            roomData.blocks[i].unlocked = true;
                        }

                        roomData.expandLevel = unlockedBlocksCount + 1;
                        roomData.currentSize = rsdi.Value.maxSizeInCells;
                    }

                    // Check if size of BlockGrid mode room increased - add additional blocks, if needed.
                    if (rsdi.Value.roomExpandMode == RoomExpandMode.BlockGrid &&
                        roomData.blocks.Length < rsdi.Value.maxSizeInBlocks.GetSquare())
                    {
                        Array.Resize(ref roomData.blocks, rsdi.Value.maxSizeInBlocks.GetSquare());

                        for(int i = 0; i < roomData.blocks.Length; i++)
                        {
                            if (roomData.blocks[i] == null)
                                roomData.blocks[i] = new RoomData.RoomBlock();
                        }
                    }

                    if (rsdi.Value.roomExpandMode == RoomExpandMode.BlockGrid && roomData.currentSize != rsdi.Value.maxSizeInCells)
                    {
                        roomData.currentSize = rsdi.Value.maxSizeInCells;
                    }
                }

                if (roomsSetupDataInfo.Count > 0)
                {
                    RoomData room;
                    if (persistentData.roomsData.TryGetValue(initialActiveRoomName, out room))
                    {
                        _activeRoom = new KeyValuePair<string, RoomData> (initialActiveRoomName, room);
                    }
                    Assert.IsTrue(_activeRoom != null);
                }
            }

            foreach (var roomData in persistentData.roomsData)
            {
                if (roomsSetupDataInfo.ContainsKey(roomData.Key))
                {
                    bool[] unlockedBlocks = roomData.Value.blocks != null
                            ? roomData.Value.blocks.Select(x => x.unlocked).ToArray()
                            : null;

                    presenter.SetSize(roomData.Key, roomData.Value.expandLevel, roomData.Value.currentSize, unlockedBlocks, false);
                    presenter.SetWall(roomData.Key, roomData.Value.wallId);
                    presenter.SetFloor(roomData.Key, roomData.Value.floorId);
                }
            }
        }

        void OnGameStarted(bool isFreshInstall)
        {
            gameCore.onGameStarted -= OnGameStarted;

            if (isFreshInstall)
            {
                gameCore.onSaveGameCreated += OnSaveGameCreated;
            }
            else
            {
                StartPasserbySpawn();
            }
        }

        void OnSaveGameCreated ()
        {
            gameCore.onSaveGameCreated -= OnSaveGameCreated;

            StartPasserbySpawn();
        }

        private void StartPasserbySpawn ()
        {
            if (!DebugHacks.disablePasserbys)
            {
                var cityRoomName = GetRoomNameOfType(RoomType.City);
                if (cityRoomName != null)
                {
                    RoomData roomData;
                    IRoomSetupDataInfo rsdi;
                    if (roomsSetupDataInfo.TryGetValue(cityRoomName, out rsdi) && persistentData.roomsData.TryGetValue(cityRoomName, out roomData))
                    {
                        if (rsdi.autoFillUpRoomLimit > 0 && rsdi.queueSpawnChancePerFreeSlotPerMinute > 0)
                        {
                            StartCoroutine(PasserbySpawnJob(rsdi));
                        }
                    }
                }
            }
        }

        private IEnumerator PasserbySpawnJob (IRoomSetupDataInfo rsdi)
        {
            Debug.LogFormat("Starting passerby spawn job for room '{0}'.", rsdi.name);

            Assert.IsNotNull(rsdi);

            float lastSpawnTime = -1.0f;

            yield return null;

            while (true)
            {
                var numberOfPersonsSpawned = gameCore.personManager.persons.Values.Where
                    (x => x is PersonPasserby && x.roomName == rsdi.name).Count();
                
                if (numberOfPersonsSpawned < rsdi.autoFillUpRoomLimit &&
                    (lastSpawnTime < 0 || numberOfPersonsSpawned < rsdi.autoFillUpQueueLimit ||
                        UnityEngine.Random.value < (Time.time - lastSpawnTime) * (rsdi.queueSpawnChancePerFreeSlotPerMinute / 60.0f) * (rsdi.autoFillUpRoomLimit - numberOfPersonsSpawned)))
                {
                    lastSpawnTime = Time.time;

                    gameCore.personManager.CreatePasserby(rsdi.name, Person.EntranceMode.ImmediateEntrance);
                    numberOfPersonsSpawned++;
                }

                yield return new WaitForSeconds((numberOfPersonsSpawned < rsdi.autoFillUpQueueLimit) ? passerbySpawnWarmupCheckRate : passerbySpawnCheckRate);
            }
        }
        #endregion

        #region IPersistent API
        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is RoomsPersistentData);
            if (obj is RoomsPersistentData)
            {
                persistentData = (RoomsPersistentData)obj;
                roomCapacity = new RoomCapacity(gameCore, persistentData); 
                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.InitDefaultData()
        {

        }

        void IPersistent.OnPreLoad()
        {
            Reinit();
        }

        void IPersistent.OnLoaded(bool success)
        {
            if (success)
            {
                BindRoomsData();
            }
            else
            {
                Reinit();
                BindRoomsData();
            }
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (var room in persistentData.roomsData)
            {
                if (room.Value.expandUnlockTimer != null)
                {
                    room.Value.expandUnlockTimer.FastForward((float) timePassedSinceSave.TotalSeconds);
                } 
            }

            RoomsPostInit();

            presenter.UpdateStyle();
            presenter.UpdateFitPoints();
        }

        void IPersistent.OnLoadSkipped()
        {
            BindRoomsData();
            RoomsPostInit();
            presenter.UpdateStyle();
            presenter.UpdateFitPoints();
        }
        #endregion
    }
}