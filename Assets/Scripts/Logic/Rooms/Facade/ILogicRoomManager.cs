﻿using System.Collections.Generic;
using Core.Timer;
using Data;
using Data.Room;

namespace Logic.Rooms.Facade
{
    public delegate void RoomDeactivatedEvent(string roomName);
    public delegate void RoomActivatedEvent(string roomName);
    public delegate void RoomsExpandAvailabilityUpdatedEvent();
    public delegate void RoomUnlockCompleteEvent (string roomName);

    public interface ILogicRoomManager
    {
        event RoomDeactivatedEvent onRoomDeactivatedEvent;
        event RoomActivatedEvent onRoomActivatedEvent;
        event RoomsExpandAvailabilityUpdatedEvent onRoomsExpandAvailabilityUpdatedEvent;
        event RoomUnlockCompleteEvent onRoomUnlockCompleteEvent;


        string  activeRoomName          { get; }
        bool    isEditMode              { get; }
        bool    isAnyTutorialRunning    { get; }

        RoomCapacity roomCapacity
        {
            get;
        }

        string GetRoomNameOfType(RoomType roomType);
        void RegisterRooms(IList<IRoomSetupDataInfo> roomsSetupDataInfo);
        bool DeactivateRoomRequest(string roomName);
        bool ActivateRoomRequest(string roomName);
        int GetRoomSizeInCells(string roomName);
        int GetRoomSizeInCells(string roomName, int atExpandLevel);
        RoomCoordinates GetRoomDimensionsInCells(string roomName);
        RoomCoordinates GetRoomDimensionsNative(string roomName);

        bool IsRoomAtInitialSize(string roomName);
        bool TryExpandRequest(string roomName, int blockIndex);
        int GetNextExpandUnlockLevel(string roomName);
        int GetExpandUnlockLevel(string roomName, int expandStage);
        Cost GetExpandCost(string roomName, int expandStage);
        Cost GetExpandCost(string roomName);
        int GetExpandStep(string roomName);
        bool CanExpandX(string roomName);
        bool CanExpandY(string roomName);
        int GetRoomExpandStage(string roomName);

        bool TryUnlockRequest(string roomName);
        TimerFloat GetUnlockTimer(string roomName);
        bool CompleteUnlockRequest(string roomName);

        void FinishEditMode();
        int GetPlayerFitPointsCount();
    }
}