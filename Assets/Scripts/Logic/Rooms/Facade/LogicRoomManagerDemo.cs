﻿using UnityEngine;

namespace Logic.Rooms.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Rooms/Facade/Logic Room Manager Demo")]
    public class LogicRoomManagerDemo : LogicRoomManagerBase
    {
        #region Properties.
        #endregion

        #region IShopPurchaseProtocol properties
        #endregion

        #region Private data.
        #endregion

        #region Persistent Data.
        #endregion

        #region Bridge.
        #endregion

        #region Unity API.
        #endregion

        #region Public API.
        #endregion

        #region Private functions.
        #endregion

        #region IPersistent API
        #endregion
    }
}