﻿using Core.EventSystem;

namespace Logic.ResolveEvents
{
    public class StorageIsFull : StaticEvent<StorageIsFull.Delegate, bool>
    {
        public delegate void Delegate(bool showWarningFirst);
    }
}
