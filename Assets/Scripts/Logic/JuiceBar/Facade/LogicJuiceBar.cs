﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Analytics;
using Core.Timer;
using Data;
using Data.JuiceBar;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Estate;
using Logic.FacadesBase;
using Logic.Persons;
using Logic.Persons.Events;
using Logic.PlayerProfile.Events;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.Quests.Tutorial;
using Logic.Quests.Juicer;

namespace Logic.JuiceBar.Facade
{
    public delegate void OnJuicerTransfer (int personId);
    public delegate void OnJuiceConsumeStarted (int personId, int equipmentId, ResourceType juiceType);
    public delegate void OnJuiceConsumeEnded (int personId, int equipmentId);

    [AddComponentMenu("Kingdom/Logic/Juice Bar/Facade/Logic Juice Bar")]
    [DisallowMultipleComponent]
    [BindBridgeInterface(typeof (PresentationJuiceBarBase))]
    public class LogicJuiceBar: BaseLogicFacade, IPersistent
    {
        #region Public events;
        public event OnJuicerTransfer onJuicerTransfer;
        public event OnJuiceConsumeStarted onJuiceConsumeStarted;
        public event OnJuiceConsumeEnded onJuiceConsumeEnded;
        public event Action onJuiceAddInOrder;
        public event Action onJuiceOrderClaim;
        public event Action<string, int> onFruitCollected;
        #endregion

        #region Public properties.
        public string                   propertyName        { get { return "JuiceBarData"; } }
        public Type                     propertyType        { get { return typeof(JuiceBarPersistentData); } }

        public int                      levelMin            { get; private set; }
        public List<ResourceType>       availableJuices     { get { return GetAvailableJuices(); } }

        public List<JuiceOrder>         orders              { get { return data.orders; } }
        public int                      orderSlotsAvailable { get { return data.orderSlotsAvailable; } }

        public CanUseFeatureResult      canUseMyFeature     { get { return GetCanUseMyFeature(); } }

        public JuiceQuestManager        questManager        { get { return gameCore.quests.juiceQuests; } }
        #endregion

        #region Private data.
        private PresentationJuiceBarBase    presenter;
        private JuiceRecipesTableData[]     juiceRecipes;
        #endregion

        #region Persistent data.
        private JuiceBarPersistentData  data = new JuiceBarPersistentData();
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        private void Subscribe(PresentationJuiceBarBase presentationJuiceBar)
        {
            this.presenter = presentationJuiceBar;

            juiceRecipes = DataTables.instance.juiceRecipes;
            levelMin = juiceRecipes.Min(item => item.level);

            gameCore.onGameStarted += OnGameStarted;
            gameCore.roomManager.onRoomUnlockComplete += OnRoomUnlockComplete;

            LevelUpEvent.Event += OnLevelUpEvent;

            CharacterTransferEvent.Event += OnCharacterTransferEvent;
            CharacterSwitchExerciseEvent.Event += OnCharacterSwitchExerciseEvent;
            // CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent;
        }

        [BindBridgeUnsubscribe]
        private void Unsubscribe(PresentationJuiceBarBase presentationJuiceBar)
        {
            this.presenter = null;

            gameCore.onGameStarted -= OnGameStarted;
            gameCore.roomManager.onRoomUnlockComplete -= OnRoomUnlockComplete;

            LevelUpEvent.Event -= OnLevelUpEvent;

            CharacterTransferEvent.Event -= OnCharacterTransferEvent;
            CharacterSwitchExerciseEvent.Event -= OnCharacterSwitchExerciseEvent;
            // CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
        }
        #endregion

        #region Unity API.
        /*
                void Start ()
                {
                }

                void OnDestroy()
                {
                }
        */
        #endregion

        #region Public API.
        public CanUseFeatureResult GetCanUseMyFeature()
        {
            var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
            if (roomName != null)
            {
                // TODO: Optimize this, make single API in Room Manager that returns all properties of the room in one call.
                if (gameCore.roomManager.GetRoomExpandStage(roomName) == 0)
                {
                    if (gameCore.roomManager.GetUnlockTimer(roomName) != null)
                    {
                        return new CanUseFeatureResult(CanUseFeatureResult.ResultType.Pending);
                    }
                    else
                    {
                        var unlockLevel = gameCore.roomManager.GetNextExpandUnlockLevel(roomName);
                        return (gameCore.playerProfile.level < unlockLevel)
                            ? new CanUseFeatureResult(CanUseFeatureResult.ResultType.LockedNeededLevel, unlockLevel)
                            : new CanUseFeatureResult(CanUseFeatureResult.ResultType.Locked);
                    }
                }
                else
                {
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.CanUse);
                }
            }
            else
            {
                return new CanUseFeatureResult(CanUseFeatureResult.ResultType.Locked);
            }
        }

        public List<ResourceType> GetRandomJuices(int count = 1)
        {
            var tmpAvailableJuices = availableJuices;
            var resultJuices = new List<ResourceType>();

            for(int i = 0; i < count; i++)
            {
                if (tmpAvailableJuices.Count > 0)
                {
                    var rndElement = tmpAvailableJuices[UnityEngine.Random.Range(0, tmpAvailableJuices.Count)];
                    resultJuices.Add(rndElement);
                    tmpAvailableJuices.Remove(rndElement);
                }
                else
                {
                    return resultJuices;
                }
            }

            return resultJuices;
        }

        public bool IsJuiceNeeded(ResourceType juiceId)
        {
            var countInStorage = playerProfile.storage.GetResourcesCount(juiceId);
            var countInQuests = questManager.CountJuiceInQuests(juiceId);
            var countInOrders = CountJuiceInOrders(juiceId);

            //Debug.LogErrorFormat("{0} - countInStorage {1}, countInQuests {2}, countInOrders {3}", juiceId, countInStorage, countInQuests, countInOrders);

            return countInQuests > countInStorage + countInOrders;
        }

        int CountJuiceInOrders(ResourceType juiceId)
        {
            int count = 0;
            foreach(var order in orders)
            {
                if (order.id == juiceId)
                    count++;
            }

            return count;
        }

        public bool Make(JuiceRecipesTableData recipe)
        {
            if (recipe == null)
            {
                Debug.LogError("recipe is NULL");
                return false;
            }

            if (orders.Count >= data.orderSlotsAvailable)
            {
                return false;
            }
            
            var cost = recipe.cost;

            if (playerProfile.CanSpend(cost))
            {
                playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Juice",recipe.loc_id));
                AddToOrders(recipe);
                return true;
            }
            else
            {
                presenter.BuyShortageOfFruits(recipe);
                return false;
            }
        }

        public bool BuyShortageOfFruits(JuiceRecipesTableData recipe)
        {
            var cost = recipe.cost;
            var costShortage = playerProfile.GetShortageCost(cost);
            var costFitbucks = playerProfile.storage.GetExtraBucksPrice(costShortage);

            if (playerProfile.SpendMoney(costFitbucks, new Analytics.MoneyInfo("Fruits","BuyWithBucks")))
            {
                playerProfile.AddReward(costShortage, new Analytics.MoneyInfo("Fruits","BuyWithBucks"), true);
                var result = playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Juice",recipe.loc_id));
                Assert.IsTrue(result);

                AddToOrders(recipe);

                return true;
            }

            return false;
        }

        public void OnOrderSlotUnlockClick()
        {
            if (gameCore.quests.tutorial.isAnyTutorialRunning)
                return;

            var cost = new Cost(Cost.CostType.BucksOnly, /*DataTables.instance.balanceData.JuiceBar.unlockOrderSlotStartPrice*/ GetOrderUnlockCost(), 0);

            if (playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("JuiceBarSlot","Slot_" + (data.orderSlotsAvailable + 1).ToString())))
            {
                data.orderSlotsAvailable++;
                presenter.UpdateMakeJuice();
            }
        }

        public bool ClaimOrder(int idx)
        {
            if (playerProfile.storage.isFull && !gameCore.quests.tutorial.isAnyTutorialRunning)
            {
                presenter.GetPlayerProfileInteractionProvider().WarnStorageIsFull(true);
                return false;
            }
            
            if (idx < orders.Count)
            {
                var order = orders[idx];

                var bucks = GetOrderSkipCost((int)order.time.secondsToFinish); //DynamicPrice.GetSkipCostNoFree((int)order.time.secondsToFinish);

                if (bucks > 0)
                {
                    var cost = new Cost(Cost.CostType.BucksOnly, bucks, 0);
                
                    if (playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Skip","JuiceOrder")))
                    {
                        ClaimOrder(order);
                        return true;
                    }
                }
                else
                {
                    ClaimOrder(order);
                    return true;
                }
            }
            else
            {
                Debug.LogError("LogicJuiceBar : OnOrderBtnClick wrong idx " + idx.ToString());
            }

            return false;
        }

        public int GetEnteredClientsCount()
        {
            if (canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse)
            {
                var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
                Assert.IsNotNull(roomName);

                return gameCore.personManager.persons.Values.Count (x => x is PersonJuicer && x.roomName == roomName && x.isNotLeavingOrDead && x.state != PersonState.InQueue);
            }
            else
            {
                return 0;
            }
        }

        public int GetCurrentClientsCount()
        {
            if (canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse)
            {
                var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
                Assert.IsNotNull(roomName);

                return gameCore.personManager.persons.Values.Count (x => x is PersonJuicer && x.roomName == roomName && x.isNotLeavingOrDead);
            }
            else
            {
                return 0;
            }
        }

        public int GetMaxClientsCount()
        {
            if (canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse)
            {
                var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
                Assert.IsNotNull(roomName);

                var level = gameCore.playerProfile.level;
                return gameCore.equipmentManager.equipment.Values.Count
                    (x => x.roomName == roomName &&
                     x.meetsLevelRequirement &&
                     x.isPotentiallyAvailableForUse);
            }
            else
            {
                return 0;
            }
        }

        public bool TryUnlockTree (string fruitTreeName, bool spendBucksIfNecessary)
        {
            if (canUseMyFeature.canUse)
            {
                FruitTree fruitTree;
                if (data.fruitTrees.TryGetValue(fruitTreeName, out fruitTree) && DataTables.instance.fruitTrees.Any(x => x.name == fruitTreeName))
                {
                    if (!fruitTree.unlocked)
                    {
                        var fruitTreeDefinition = DataTables.instance.fruitTrees.FirstOrDefault(x => x.name == fruitTreeName);
                        Assert.IsNotNull(fruitTreeDefinition);

                        if (fruitTreeDefinition != null)
                        {
                            if (fruitTreeDefinition.unlockLevel <= gameCore.playerProfile.level)
                            {
                                var cost = fruitTreeDefinition.cost;

                                if (cost.isFree || playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("BuyFruitTree", fruitTreeName), spendBucksIfNecessary))
                                {
                                    PerformTreeUnlock(fruitTreeDefinition, ref fruitTree);
                                    return true;
                                }
                            }
                            else
                            {
                                Debug.LogWarningFormat("Logic error: Cannot unlock Fruit tree '{0}', required level is: {1}, current: {2}!"
                                    , fruitTreeName, fruitTreeDefinition.unlockLevel, gameCore.playerProfile.level);
                            }
                        }
                        else
                        {
                            Debug.LogErrorFormat("Cannot unlock Fruit tree '{0}', it's definition not found in Fruit Trees table!" , fruitTreeName);
                        }
                    }
                    else
                    {
                        Debug.LogWarningFormat("Logic error: Cannot unlock Fruit tree '{0}', it's already unlocked!", fruitTreeName);
                    }
                }
                else
                {
                    Debug.LogWarningFormat("Logic error: Cannot find Fruit tree (or its definition) with name '{0}'.", fruitTreeName);
                }
            }
            return false;
        }

        public int CollectFruitsFromTree(string fruitTreeName)
        {
            if (canUseMyFeature.canUse)
            {
                FruitTree fruitTree;
                if (data.fruitTrees.TryGetValue(fruitTreeName, out fruitTree) && DataTables.instance.fruitTrees.Any(x => x.name == fruitTreeName))
                {
                    if (fruitTree.unlocked && fruitTree.numFruits > 0)
                    {
                        var fruitTreeDefinition = DataTables.instance.fruitTrees.FirstOrDefault(x => x.name == fruitTreeName);
                        Assert.IsNotNull(fruitTreeDefinition);

                        var treeIdx = Array.IndexOf(DataTables.instance.fruitTrees, fruitTreeDefinition);

                        if (fruitTreeDefinition != null)
                        {
                            var collected = gameCore.playerProfile.storage.AddResourcePartially
                                ( fruitTreeDefinition.type
                                , fruitTree.numFruits
                                , new Analytics.MoneyInfo("FruitTree", fruitTreeName));

                            if (collected > 0)
                            {
                                Assert.IsTrue(collected <= fruitTree.numFruits);
                                fruitTree.numFruits = fruitTree.numFruits - collected;

                                Achievements.HarvestFruits.Send(treeIdx, collected);

                                UpdateFruitTreeTimerAfterCollection(fruitTreeDefinition, ref fruitTree, collected);

                                gameCore.ScheduleSave();

                                FruitTreeUpdated(fruitTreeDefinition, fruitTree);

                                PersistentCore.instance.analytics.GameEvent("Collect Fruit", 
                                    new GameEventParam("Level", gameCore.playerProfile.level),
                                    new GameEventParam("Fruit Name", fruitTreeDefinition.type.ToString()),
                                    new GameEventParam("Amount", collected)
                                );

                                onFruitCollected?.Invoke(fruitTreeName, collected);
                            }
                            return collected;
                        }
                    }
                }
            }
            return -1;
        }

        public bool SkipFruitTreeRegen(string fruitTreeName)
        {
            if (canUseMyFeature.canUse)
            {
                FruitTree fruitTree;
                if (data.fruitTrees.TryGetValue(fruitTreeName, out fruitTree) && DataTables.instance.fruitTrees.Any(x => x.name == fruitTreeName))
                {
                    var fruitTreeDefinition = DataTables.instance.fruitTrees.FirstOrDefault(x => x.name == fruitTreeName);
                    Assert.IsNotNull(fruitTreeDefinition);

                    if (fruitTreeDefinition != null && fruitTree.unlocked)
                    {
                        var nextTimeLeft = (float)CalculateRegenNextTimeLeft(fruitTreeDefinition, fruitTree).TotalSeconds;
                        var timeLeftNoFractions = Mathf.FloorToInt(nextTimeLeft);
                        
                        if (timeLeftNoFractions > 0)
                        {
                            var skipCost = DynamicPrice.GetSkipCostNoFree(timeLeftNoFractions);
                            if (skipCost <= 0 || gameCore.playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, skipCost, 1), new Analytics.MoneyInfo("Skip", "FruitTree")))
                            {
                                fruitTree.numFruits += 1;
                                Assert.IsTrue(fruitTree.numFruits <= fruitTreeDefinition.maxFruits);

                                if (fruitTree.numFruits < fruitTreeDefinition.maxFruits && fruitTreeDefinition.regenTime > TimeSpan.Zero)
                                {
                                    var newTimeLeft = fruitTree.maxFruitsRegenTimer.secondsToFinish - (float)fruitTreeDefinition.regenTime.TotalSeconds;

                                    if (newTimeLeft > 0)
                                    {
                                        fruitTree.maxFruitsRegenTimer = new TimerFloat(newTimeLeft);
                                    }
                                    else
                                    {
                                        fruitTree.maxFruitsRegenTimer = null;
                                    }
                                }
                                else
                                {
                                    fruitTree.maxFruitsRegenTimer = null;
                                }

                                gameCore.ScheduleSave();

                                FruitTreeUpdated(fruitTreeDefinition, fruitTree);

                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public void CalcOrders()
        {
            if (orders.Count < 1)
                return;

            bool needSort = false;
            float prevSecondsValue = orders[0].time.secondsToFinish;
            for (int i = 1; i < orders.Count; ++i)
            {
                var tOrder = orders[i];

                if ((prevSecondsValue > tOrder.time.secondsToFinish) || (prevSecondsValue == 0 && tOrder.time.secondsToFinish > 0))
                {
                    needSort = true;
                    break;
                }

                prevSecondsValue = tOrder.time.secondsToFinish;
            }

            if (needSort)
            {
                data.orders = orders.OrderBy(s => s.sortValue).ToList<JuiceOrder>();
                //orders.Sort(JuiceOrder.JuiceOrderCompare);
            }

        }

        public int GetOrderUnlockCost()
        {
            const int UnlockSlotCost = 15;
            int normalCount = Mathf.Max(1, orderSlotsAvailable - (JuiceBarPersistentData.DefaultSlotsCount - 1)); 
            return normalCount * UnlockSlotCost;
        }

        public int GetOrderSkipCost(int totalSeconds)
        {
            if (gameCore.quests.tutorial.IsTutorialRunning<TutorSequenceJuiceBar>())
                return 0;

            return DynamicPrice.GetSkipCostNoFree(totalSeconds);
        }

        public void SelectFruitTreeByIndex(int index)
        {
            presenter.SelectFruitTreeByIndex(index);
        }

        public TimerFloat GetUnlockTimer()
        {
            var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
            if (roomName != null)
            {
                return gameCore.roomManager.GetUnlockTimer(roomName);
            }
            return null;
        }
        
        public void ShowMakeJuiceUI()
        {
            presenter.ShowMakeJuiceUI();
        }
        #endregion

        #region IPersistent interface.
        bool IPersistent.Deserialize(object obj)
        {
            data = obj as JuiceBarPersistentData;

            return data != null;
        }

        object IPersistent.Serialize()
        {
            return data;
        }

        void IPersistent.OnPreLoad()
        {
        }

        void IPersistent.InitDefaultData()
        {
            data = new JuiceBarPersistentData();
        }
       
        void IPersistent.OnLoaded(bool success)
        {
            if (data == null)
                data = new JuiceBarPersistentData();

            if (data.fruitTrees == null)
                data.fruitTrees = new Dictionary<string, FruitTree>(8);

            if (data.orders == null)
                data.orders = new List<JuiceOrder>(16);

            if (success)
            {
                PrepareTreesData();
            }
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (var order in orders)
            {
                if (order.time != null)
                    order.time.FastForward((float)timePassedSinceSave.TotalSeconds);
            }
                
            UpdateTreesTimersAfterLoad(timePassedSinceSave);
        }

        void IPersistent.OnLoadSkipped()
        {
            if (data == null)
                data = new JuiceBarPersistentData();

            PrepareTreesData();
        }
        #endregion

        #region Event handlers.
        void OnRoomUnlockComplete (string roomName)
        {
            if (roomName == gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar))
            {
                // Automatically unlock trees that have zero unlock cost and can be unlocked already because of level.
                foreach (var fruitTreeDefinition in DataTables.instance.fruitTrees)
                {
                    var fruitTree = data.fruitTrees[fruitTreeDefinition.name];
                    Assert.IsNotNull(fruitTree);

                    if (!fruitTree.unlocked && fruitTreeDefinition.unlockLevel <= gameCore.playerProfile.level && fruitTreeDefinition.cost.isFree)
                    {
                        PerformTreeUnlock(fruitTreeDefinition, ref fruitTree);
                    }
                    else
                    {
                        FruitTreeUpdated (fruitTreeDefinition, fruitTree);
                    }
                }

                PersistentCore.instance.analytics.TutorialStep(TutorialStepId.FinishJuiceBarUnlock);//Finish Bar Unlock
    
                PersistentCore.instance.analytics.GameEvent("Unlock Bar", new GameEventParam("Level", gameCore.playerProfile.level)); 

                StartCoroutine(RegenFruitsJob());
            }
        }

        void OnGameStarted (bool isFreshInstall)
        {
            // Update trees on presentation side.
            var roomUnlocked = canUseMyFeature.canUse;

            foreach (var fruitTreeDefinition in DataTables.instance.fruitTrees)
            {
                var fruitTree = data.fruitTrees[fruitTreeDefinition.name];
                Assert.IsNotNull(fruitTree);

                presenter.InitTree
                    ( fruitTreeDefinition.name
                    , fruitTree.unlocked
                    , fruitTree.unlocked ? false : (roomUnlocked && fruitTreeDefinition.unlockLevel <= gameCore.playerProfile.level)
                    , fruitTreeDefinition.unlockLevel
                    , fruitTreeDefinition.cost
                    , fruitTree.numFruits
                    , fruitTreeDefinition.maxFruits
                    , fruitTreeDefinition.regenTime
                    , CalculateRegenNextTimeLeft(fruitTreeDefinition, fruitTree));
            }

            {
                var persons = gameCore.personManager.persons;
                foreach (var person in persons.Values)
                {
                    if (person is PersonJuicer && person.state == PersonState.Workout)
                    {
                        var juicer = person as PersonJuicer;
                        var quest = juicer.juiceQuest;

                        if (quest != null)
                        {
                            var exerciseId = person.workoutExercise.id;
                            if (quest.exerciseId == exerciseId)
                            {
                                StartJuiceConsume(juicer);
                            }
                        }
                    }
                }

                if (roomUnlocked)
                {
                    StartCoroutine(RegenFruitsJob());
                }
            }
        }

        void OnLevelUpEvent (int level)
        {
            if (canUseMyFeature.canUse)
            {
                // Update trees on presentation side that can be unlocked on this level, also auto-unlock trees that has free unlock cost.
                foreach (var fruitTreeDefinition in DataTables.instance.fruitTrees)
                {
                    if (fruitTreeDefinition.unlockLevel == level)
                    {
                        var fruitTree = data.fruitTrees[fruitTreeDefinition.name];
                        Assert.IsNotNull(fruitTree);

                        if (!fruitTree.unlocked && fruitTreeDefinition.unlockLevel <= gameCore.playerProfile.level && fruitTreeDefinition.cost.isFree)
                        {
                            PerformTreeUnlock(fruitTreeDefinition, ref fruitTree);
                        }
                        else
                        {
                            FruitTreeUpdated (fruitTreeDefinition, fruitTree);
                        }
                    }
                }
            }
        }

        private void OnCharacterTransferEvent (Person person, string roomName)
        {
            if (person is PersonJuicer && roomName == gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar))
            {
                onJuicerTransfer?.Invoke(person.id);
            }
        }

        private void OnCharacterSwitchExerciseEvent (Person person, Exercise exercise, float speed)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(exercise);

            if (person is PersonJuicer)
            {
                var juicer = person as PersonJuicer;
                var quest = juicer.juiceQuest;

                if (quest != null)
                {
                    var exerciseId = person.workoutExercise.id;
                    if (quest.exerciseId == exerciseId)
                    {
                        StartJuiceConsume(juicer);
                    }
                    else if (quest.exerciseCompleteId == exerciseId)
                    {
                        Equipment equipment = person.workoutEquipment;
                        if (equipment != null)
                        {
                            EndJuiceConsume(juicer, equipment);
                        }
                    }
                }
            }
        }

/*
        private void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, bool skipped)
        {
            Assert.IsNotNull(person);

            if (person is PersonJuicer)
            {
                var juicer = person as PersonJuicer;
                var quest = juicer.juiceQuest;

                if (quest != null && quest.exerciseId == person.workoutExercise.id)
                {
                    EndJuiceConsume(person, equipment);
                }
            }
        }
*/
        #endregion

        #region Private methods.
        private void PrepareTreesData ()
        {
            if (data.fruitTrees == null)
                data.fruitTrees = new Dictionary<string, FruitTree>(16);

            foreach (var fruitTreeDefinition in DataTables.instance.fruitTrees)
            {
                if (!data.fruitTrees.ContainsKey(fruitTreeDefinition.name))
                    data.fruitTrees.Add(fruitTreeDefinition.name, new FruitTree());
            }

            // TODO: Remove trees from persistent data that are not in definitions table? I don't know yet...
        }

        private void UpdateTreesTimersAfterLoad (TimeSpan timePassedSinceSave)
        {
            var timePassedSeconds = (float)timePassedSinceSave.TotalSeconds;

            foreach (var fruitTreeDefinition in DataTables.instance.fruitTrees)
            {
                var fruitTree = data.fruitTrees[fruitTreeDefinition.name];

                if (fruitTree.maxFruitsRegenTimer != null)
                {
                    fruitTree.maxFruitsRegenTimer.FastForward(timePassedSeconds);

                    UpdateTreeTimerAndFruits(fruitTreeDefinition, ref fruitTree);
                }
            }
        }

        private bool UpdateTreeTimerAndFruits (FruitTreeDefinition fruitTreeDefinition, ref FruitTree fruitTree)
        {
            Assert.IsNotNull(fruitTree.maxFruitsRegenTimer);

            if (fruitTree.maxFruitsRegenTimer.secondsToFinish <= 0)
            {
                fruitTree.maxFruitsRegenTimer = null;
                fruitTree.numFruits = fruitTreeDefinition.maxFruits;
                return true;
            }
            else
            {
                if (fruitTreeDefinition.regenTime > TimeSpan.Zero)
                {
                    var cyclesPassed = Math.Min(fruitTreeDefinition.maxFruits - fruitTree.numFruits, fruitTree.maxFruitsRegenTimer.secondsPassed / (float)fruitTreeDefinition.regenTime.TotalSeconds);
                    var numRegenerated = Math.Min(fruitTreeDefinition.maxFruits - fruitTree.numFruits, Mathf.FloorToInt (cyclesPassed));
                    if (numRegenerated > 0)
                    {
                        fruitTree.numFruits += numRegenerated;

                        if (fruitTree.numFruits == fruitTreeDefinition.maxFruits)
                        {
                            fruitTree.maxFruitsRegenTimer = null;
                            return true;
                        }
                        else
                        {
                            var newTimeLeft = (fruitTreeDefinition.maxFruits - fruitTree.numFruits) * (float)fruitTreeDefinition.regenTime.TotalSeconds -
                                                (cyclesPassed - numRegenerated) * (float)fruitTreeDefinition.regenTime.TotalSeconds;
                            if (newTimeLeft > 0)
                            {
                                fruitTree.maxFruitsRegenTimer = new TimerFloat(newTimeLeft);
                            }
                            else
                            {
                                fruitTree.maxFruitsRegenTimer = null;
                            }
                            return true;
                        }
                    }
                }
                else
                {
                    fruitTree.maxFruitsRegenTimer = null;
                    return true;
                }
            }

            return false;
        }

        private void UpdateFruitTreeTimerAfterCollection (FruitTreeDefinition fruitTreeDefinition, ref FruitTree fruitTree, int collected)
        {
            if (fruitTreeDefinition.regenTime > TimeSpan.Zero && fruitTree.numFruits < fruitTreeDefinition.maxFruits)
            {
                if (fruitTree.maxFruitsRegenTimer != null)
                {
                    fruitTree.maxFruitsRegenTimer = new TimerFloat(fruitTree.maxFruitsRegenTimer.secondsToFinish + collected * (float)fruitTreeDefinition.regenTime.TotalSeconds);
                }
                else
                {
                    fruitTree.maxFruitsRegenTimer = new TimerFloat((fruitTreeDefinition.maxFruits - fruitTree.numFruits) * (float)fruitTreeDefinition.regenTime.TotalSeconds);
                }
            }
            else
            {
                fruitTree.maxFruitsRegenTimer = null;
            }
        }

        private IEnumerator RegenFruitsJob ()
        {
            yield return null;

            while (true)
            {
                // Perform fruits regeneration.
                foreach (var fruitTreePair in data.fruitTrees)
                {
                    if (fruitTreePair.Value.unlocked && fruitTreePair.Value.maxFruitsRegenTimer != null)
                    {
                        var fruitTreeDefinition = DataTables.instance.fruitTrees.FirstOrDefault(x => x.name == fruitTreePair.Key);
                        Assert.IsNotNull(fruitTreeDefinition);

                        if (fruitTreeDefinition != null)
                        {
                            var fruitTree = fruitTreePair.Value;

                            if (UpdateTreeTimerAndFruits(fruitTreeDefinition, ref fruitTree))
                            {
                                FruitTreeUpdated(fruitTreeDefinition, fruitTree);

                                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                            }
                        }
                    }
                }

                yield return null; // Run on next frame.
            }
        }

        private TimeSpan CalculateRegenNextTimeLeft (FruitTreeDefinition fruitTreeDefinition, FruitTree fruitTree)
        {
            // Calculate time left to regen one next fruit.

            if (fruitTree.maxFruitsRegenTimer != null && fruitTreeDefinition.regenTime > TimeSpan.Zero && fruitTree.numFruits < fruitTreeDefinition.maxFruits)
            {
                return TimeSpan.FromSeconds(Math.Max(0.0f, fruitTree.maxFruitsRegenTimer.secondsToFinish - Math.Max(0, fruitTreeDefinition.maxFruits - fruitTree.numFruits - 1) * (float)fruitTreeDefinition.regenTime.TotalSeconds));
            }
            else
            {
                return TimeSpan.Zero;
            }
        }

        public bool IsTreeAvailable(int idx)
        {
            var table = DataTables.instance.fruitTrees;
            if (idx < table.Length)
            {
                var treeDefinition = DataTables.instance.fruitTrees[idx];
                var treeInstance = data.fruitTrees[treeDefinition.name];

                return treeInstance.unlocked;
            }

            Debug.LogError("IsTreeAvailable : wrong idx");
            return false;
        }

        private void PerformTreeUnlock (FruitTreeDefinition fruitTreeDefinition, ref FruitTree fruitTree)
        {
            fruitTree.unlocked = true;
            fruitTree.numFruits = fruitTreeDefinition.maxFruits;

            FruitTreeUpdated(fruitTreeDefinition, fruitTree);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        private void FruitTreeUpdated (FruitTreeDefinition fruitTreeDefinition, FruitTree fruitTree)
        {
            presenter.UpdateFruitTree
                ( fruitTreeDefinition.name
                , fruitTree.unlocked
                , fruitTree.unlocked ? false : (canUseMyFeature.canUse && fruitTreeDefinition.unlockLevel <= gameCore.playerProfile.level)
                , fruitTree.numFruits
                , fruitTreeDefinition.regenTime
                , CalculateRegenNextTimeLeft(fruitTreeDefinition, fruitTree));
        }

        private void StartJuiceConsume (PersonJuicer juicer)
        {
            Assert.IsNotNull(juicer);

            var quest =  juicer.juiceQuest;

            if (quest != null && quest.objectives != null && quest.objectives.Count > 0)
            {
                onJuiceConsumeStarted?.Invoke(juicer.id, juicer.workoutEquipmentId, quest.objectives[0].juice);
            }
        }

        private void EndJuiceConsume (PersonJuicer juicer, Equipment equipment)
        {
            Assert.IsNotNull(juicer);

            var quest = juicer.juiceQuest;

            if (quest != null && quest.objectives != null && quest.objectives.Count > 0)
            {
                onJuiceConsumeEnded?.Invoke(juicer.id, equipment.id);
            }
        }

        private void ClaimOrder(JuiceOrder order)
        {
            var orderTime = order.time.secondsToFinish;

            //--- update other order timers
            foreach (var o in orders)
                o.time.FastForward(orderTime);

            playerProfile.AddResource(order.id, 1, new Analytics.MoneyInfo("Juice", "ClaimOrder"), false);
            Achievements.MakeJuice.Send((int)order.id);
            orders.Remove(order);
            //presenter.UpdateMakeJuice();

            onJuiceOrderClaim?.Invoke();
        }

        private void AddToOrders(JuiceRecipesTableData recipe)
        {
            PersistentCore.instance.analytics.GameEvent("Make Juice", new GameEventParam("Level", gameCore.playerProfile.level), new GameEventParam("Juice Name", recipe.loc_id));

            var maxTime = orders.Count > 0 ? orders.Max(s => s.time) : new TimerFloat(0);
            var order = new JuiceOrder(recipe.id, recipe.make_time + maxTime.timeSpan);

            orders.Add(order);
            orders.Sort(JuiceOrder.JuiceOrderCompare);

            onJuiceAddInOrder?.Invoke();
        }

        private List<ResourceType> GetAvailableJuices()
        {
            if (gameCore.quests.tutorial.IsTutorialRunning<TutorSequenceJuiceBar>())
            {
                return new List<ResourceType> { juiceRecipes[0].id };
            }

            var result = juiceRecipes.Where(item => item.level <= playerProfile.level).Select(item => item.id).ToList();

            result.Sort();

            return result;
        }

        #endregion
    }
}
