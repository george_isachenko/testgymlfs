﻿using System;
using Data;
using Data.Person;
using Data.Quests;
using Logic.Persons.Tasks;
using Logic.Estate;
using UnityEngine.Assertions;
using Data.Estate;
using UnityEngine;
using Core;
using Data.Room;
using Data.Person.Info;
using Logic.Persons.Events;
using Core.Analytics;
using System.Linq;
using System.Reflection;

namespace Logic.Persons
{
    [Flags]
    public enum ExerciseFlags
    {
        None = 0,
        StartsInOffline = 1,
        EndsInOffline = 2,      // Used both on start and end.
        Restored = 4,           // Used only on start.
        Skipped = 8,            // Used only when ending exercise.
    }

    public abstract class Person : IPersonInfo
    {
        #region Static/readonly data.
        private static readonly string instantiateMethodName = "Instantiate";
        private static readonly Type[] instantiateMethodParameterTypes = new Type[] { typeof(IPersonManager), typeof(PersonData), typeof(bool) };
        #endregion

        #region Static data.
        private static Type[] personTypesCache;
        #endregion

        #region Types.
        public enum IdleType
        {
            Idle = 0,
            IdleWalking,
            IdleInteraction,
        }

        public enum EntranceMode
        {
            ImmediateEntrance = 0,
            ImmediateEntranceFast,
            GoToQueue,
            NoMove
        }
        #endregion

        #region Public Properties.
        public IPersonManager personManager                 { get; private set; }

        public int                  id                      { get { return data.id; } }
        public string               name                    { get { return data.name; } set { SetName(value); } }
        public PersonState          state                   { get { return data.state; } }
        public int                  level                   { get { return data.level; } }
        public RoomCoordinates      position                { get { return data.position; } }
        public float                heading                 { get { return data.heading; } }
        public PersonAppearance     appearance              { get { return data.appearance; } } 
        public string               roomName                { get { return data.roomName; } }
        public int                  workoutEquipmentId      { get { return data.workoutEquipmentId; } }
        public Exercise             workoutExercise         { get { return data.workoutExercise; } }
        public float                exerciseTotalTime       { get { return data.exerciseTotalTime; } }
        public DateTime             exerciseEndTime         { get { return data.exerciseEndTime; } }
        public bool                 isInfiniteExerciseTime  { get { return data.exerciseEndTime == DateTime.MaxValue.ToUniversalTime(); } } 
        public DateTime             spawnTime               { get { return data.spawnTime; } }
        public bool                 isTraining              { get { return data.isTraining; } }
        public bool                 isNotLeavingOrDead      { get { return data.isNotLeavingOrDead; } } 

        public IdleType             idleType                { get; set; }
        public int                  followingPersonId       { get; private set; }
        public MovementSettings     followMovementSettings  { get; private set; }

        public Equipment            workoutEquipment
        {
            get
            {
                Equipment equipment = null;
                if (data.workoutEquipmentId != EquipmentData.InvalidId &&
                    personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment))
                {
                    return equipment;
                }

                return null;
            }
        }
        #endregion

        #region Protected properties.
        protected PersonData data                           { get; private set; }
        #endregion

        #region Public virtual properties.
        public virtual bool isSelectable
        {
            get
            {
                return data.isSelectable;
            }
        }

        public virtual bool isPlayerControllable
        {
            get
            {
                return data.isPlayerControllable;
            }
        }

        public virtual bool canBeTrainedByPlayer
        {
            get
            {
                return false;
            }
        }

        public virtual bool useExtendedIdleAnimation
        {
            get
            {
                return (state == PersonState.InQueue ? false : true);
            }
        }

        public virtual int stoppedAvoidanceDelta
        {
            get
            {
                return 0;
            }
        }

        public virtual bool canSetTask
        {
            get
            {
                return (data.state == PersonState.Idle || data.state == PersonState.ExerciseComplete) &&
                    (task == null || task.interruptable);
            }
        }

        public virtual bool canPerformIdleRoutines
        {
            get
            {
                return (state == PersonState.Idle || state == PersonState.InQueue);
            }
        }

        public virtual bool canPerformFullIdle
        {
            get
            {
                return (state == PersonState.Idle);
            }
        }

        public virtual BalanceData.CharactersBalance.IdleSettingsBalance idleSettings
        {
            get
            {
                BalanceData.CharactersBalance.IdleSettingsBalance result = null;
                var type = GetType();
                do 
                {
                    if (DataTables.instance.balanceData.Characters.IdleSettings.TryGetValue(type.Name, out result))
                        break;

                    type = type.BaseType;
                } while (type != null && type != typeof(object));

                Assert.IsNotNull(result, string.Format("Please fill-in IdleSettings for character type '{0}'.", GetType().Name));
                return result;
            }
        }

        public abstract string spawnRoomZoneName
        {
            get;
        }

        public abstract string despawnRoomZoneName
        {
            get;
        }
        
        public abstract string lobbyRoomZoneName
        {
            get;
        }

        public abstract string idleZoneName
        {
            get;
        }
        #endregion

        #region Private properties.
        private static Type[] personTypes
        {
            get
            {
                if (personTypesCache == null)
                {
                    var thisType = typeof(Person);
                    personTypesCache = thisType.Assembly.GetTypes().Where(t => t.IsClass && !t.IsAbstract && thisType.IsAssignableFrom(t)).ToArray();
                }

                return personTypesCache;
            }
        }
        #endregion

        #region Private data.
        protected Task task;
        #endregion

        #region Constructors.
        protected Person(IPersonManager personManager, PersonData personData)
        {
            this.personManager = personManager;
            this.data = personData;

            followingPersonId = PersonData.InvalidId;
        }

        protected Person(IPersonManager personManager, PersonData personData, Person convertFrom)
        {
            this.personManager = personManager;
            data = personData;

            followingPersonId = PersonData.InvalidId;
            
            data.spawnTime = DateTime.UtcNow; // Update spawn time to current, so order in queue will be correct.

            // Transfer task ownership.
            task = convertFrom.task;
            convertFrom.task = null;

            if (task != null)
            {
                task.AssignPerson(this); // Reassign tasks to self.
            }
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData)
        {
            Assert.IsNotNull(personData);

            if (personData != null)
            {
                var thisType = typeof(Person);

                // Find all Person-derived classes that are not this class.
                foreach (var type in personTypes)
                {
                    if (type != thisType)
                    {
                        // Get static public non-inherited Instantiate() method that matches instantiateMethodParameterTypes parameters signature.
                        var instantiateMethod = type.GetMethod
                            ( instantiateMethodName
                            , BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly
                            , null
                            , instantiateMethodParameterTypes
                            , null);
                        if (instantiateMethod != null)
                        {
                            var result = instantiateMethod.Invoke(null, new object[] { personManager, personData, true });
                            if (result != null && result is Person)
                                return (Person)result;
                        }
                    }
                }
            }

            return null;
        }
        #endregion

        #region Public API.
        public bool SetTask(Task newTask)
        {
            //Debug.LogFormat("SetTask: id: {0}, Task: {1}.", id, newTask.GetType().Name);
            if (task != null)
            {
                return task.SetTask(this, newTask);
            }

            newTask.AssignPerson(this);
            if (newTask.Start())
            {
                task = newTask;
            }

            return (task != null);
        }

        public void AddTask(Task newTask)
        {
            //Debug.LogFormat("AddTask: id: {0}, Task: {1}.", id, newTask.GetType().Name);
            if (task != null)
            {
                task.AddTask(this, newTask);
            }
            else
            {
                task = newTask;
                newTask.AssignPerson(this);
                newTask.Start();
            }
        }

        public void UpdateTask(float dt)
        {
            if (task != null)
            {
                task.Update(dt);
            }
        }

        public void ResetTasks()
        {
            //Debug.LogFormat("ResetTasks: id: {0}.", id);
            if (task != null)
            {
                task.Abort();
                task.Start();
            }
        }

        public T FindTask<T>(bool recursive = true) where T : Task
        {
            return task != null ? task.FindTask<T>(recursive) : null;
        }

        public void OnCreated(string spawnZoneName = null)
        {
            CharacterBirthEvent.Send(this, false, new CharacterDestination(spawnZoneName != null ? spawnZoneName : spawnRoomZoneName));
        }

        public void OnRestored()
        {
            CharacterBirthEvent.Send(this, true, new CharacterDestination(data.position, data.heading));
        }

        public void UpdatePosition (RoomCoordinates position, float heading)
        {
            if (data.position != position || data.heading != heading)
            {
                data.position = position;
                data.heading = heading;

                //gameCore_.ScheduleSave(ScheduledSaveType.SaveSoon);
            }
        }

        public void Kill ()
        {
            ChangeState(PersonState.Sentenced);
        }
        
        public void Follow(int id, MovementSettings movementSettings = null)
        {
            followingPersonId = id;
            followMovementSettings = movementSettings;
        }

        public bool DoOfflineExercise
            ( out bool refusedToEnd
            , Equipment equipment
            , Exercise exercise
            , DateTime startDate
            , DateTime loadTimeUTC
            , ExerciseInitiator initiator
            , TimeSpan? customDuration = null)
        {
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            Assert.IsTrue(state == PersonState.Idle || state == PersonState.ExerciseComplete);
            Assert.IsFalse(personManager.gameCore.isGameStarted);

            refusedToEnd = false;

            if (personManager.gameCore.isGameStarted)
            {
                Debug.LogErrorFormat("Cannot use Person's 'offline' APIs when game is already running.");
                return false;
            }

            if (startDate >= loadTimeUTC)
            {
                Debug.LogErrorFormat("Start date is past Load Time date, cannot use this API.");
                return false; // Start date is past loadTimeUTC (is in "online"), cannot use this function.
            }

            var duration = customDuration != null ? customDuration.Value : exercise.duration;

            if ((state == PersonState.Idle || state == PersonState.ExerciseComplete) && equipment.SchedulePendingExercise(this, exercise))
            {
                data.workoutEquipmentId = equipment.id;
                data.workoutExercise = exercise;
                data.exerciseInitiator = initiator;

                SetExerciseTimes(startDate, duration);

                ChangeState(PersonState.WorkoutPending);

                CharacterSchedulePendingExerciseEvent.Send(this, equipment, exercise, initiator, duration);

                var speed = CalculateExerciseAnimationSpeed();

                if (equipment.StartPendingExercise(this, speed))
                {
                    var endsInOffline = data.exerciseEndTime < loadTimeUTC;

                    ExerciseFlags flags = ExerciseFlags.StartsInOffline;
                    if (endsInOffline)
                        flags |= ExerciseFlags.EndsInOffline;

                    ChangeState(PersonState.Workout);

                    CharacterStartExerciseEvent.Send
                        (this, equipment, data.workoutExercise, data.exerciseInitiator, duration, flags, speed);

                    if (endsInOffline)
                    {
                        if (TryToEndExercise(out refusedToEnd, false))
                        {
                            return true;
                        }
                    }
                    else
                    {   // Reschedule task.
                        SetTask(new TaskExercise(this, equipment));
                        return true;
                    }
                }
            }

            Debug.LogErrorFormat("Failed to do offline exercise. Wrong state or workout equipment refused request. Person id: {0}, State: {1}, Exercise id: {2}, Equipment id: {3}."
                , id, state, exercise.id, equipment.id);
            return false;
        }

        public bool SchedulePendingExercise
            ( Equipment equipment
            , Exercise exercise
            , ExerciseInitiator initiator
            , TimeSpan? customDuration = null)
        {
            Assert.IsTrue(data.state == PersonState.Idle);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);

            if (data.state == PersonState.Idle && equipment.SchedulePendingExercise(this, exercise))
            {
                var duration = customDuration != null ? customDuration.Value : exercise.duration;
#if DEBUG
                if (DebugHacks.exerciseOverrideTime > 0)
                    duration = TimeSpan.FromSeconds(DebugHacks.exerciseOverrideTime);
#endif

                data.workoutEquipmentId = equipment.id;
                data.workoutExercise = exercise;
                data.exerciseInitiator = initiator;
                SetExerciseTimes(DateTime.UtcNow, duration);

                ChangeState(PersonState.WorkoutPending);

                CharacterSchedulePendingExerciseEvent.Send(this, equipment, exercise, initiator, duration);
                return true;
            }
            else
            {
                Debug.LogErrorFormat("Failed to schedule pending exercise. Wrong state or workout equipment refused request. Person id: {0}, State: {1}, Exercise id: {2}, Equipment id: {3}."
                    , id, state, exercise.id, equipment.id);
            }

            return false;
        }

        public void StartExercise (TimeSpan? customDuration = null)
        {
            Assert.IsTrue(data.state == PersonState.WorkoutPending);
            Assert.IsTrue(data.workoutEquipmentId != EquipmentData.InvalidId);
            Assert.IsTrue(data.exerciseEndTime != DateTime.MinValue);
            Assert.IsNotNull(data.workoutExercise);

            var speed = CalculateExerciseAnimationSpeed();

            Equipment equipment;
            if (data.state == PersonState.WorkoutPending &&
                personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment) &&
                equipment.StartPendingExercise(this, speed))
            {
                var duration = customDuration != null ? customDuration.Value : data.workoutExercise.duration;

                SetExerciseTimes(DateTime.UtcNow, duration);   // This updates exercise end times to count from this point.

                ChangeState(PersonState.Workout);

                CharacterStartExerciseEvent.Send
                    ( this
                    , equipment
                    , data.workoutExercise
                    , data.exerciseInitiator
                    , duration
                    , personManager.gameCore.isGameStarted ? ExerciseFlags.None : ExerciseFlags.StartsInOffline
                    , speed);
            }
            else
            {
                Debug.LogErrorFormat("Failed to start pending exercise. Wrong state or workout equipment id or its state. Person id: {0}, State: {1}, Equipment id: {2}."
                    , id, state, data.workoutEquipmentId);
            }
        }

        public bool SwitchExercise(Exercise exercise, TimeSpan? customDuration)
        {
            Assert.IsTrue(data.state == PersonState.WorkoutPending || data.state == PersonState.Workout);
            Assert.IsTrue(data.workoutEquipmentId != EquipmentData.InvalidId);

            var speed = CalculateExerciseAnimationSpeed();

            Equipment equipment;
            if ((data.state == PersonState.WorkoutPending || data.state == PersonState.Workout) &&
                personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment) &&
                equipment.SwitchExercise(exercise, speed))
            {
                data.workoutExercise = exercise;

                if (data.state == PersonState.WorkoutPending)
                {
                    var duration = customDuration != null ? customDuration.Value : exercise.duration;
#if DEBUG
                    if (DebugHacks.exerciseOverrideTime > 0)
                        duration = TimeSpan.FromSeconds(DebugHacks.exerciseOverrideTime);
#endif
                    SetExerciseTimes(DateTime.UtcNow, duration);

                    CharacterSwitchPendingExerciseEvent.Send(this, exercise, duration);
                    return true;
                }
                else if (data.state == PersonState.Workout)
                {
                    Assert.IsTrue(data.exerciseEndTime != DateTime.MinValue);

                    if (customDuration != null)
                    {
                        SetExerciseTimes(DateTime.UtcNow, customDuration.Value);
                    }

                    CharacterSwitchExerciseEvent.Send(this, data.workoutExercise, speed);
                    return true;
                }
            }
            else
            {
                Debug.LogErrorFormat("Failed to switch exercise. Wrong state or workout equipment id or its state. Person id: {0}, State: {1}, Equipment id: {2}."
                    , id, state, data.workoutEquipmentId);
            }

            return false;
        }

        public bool TryToEndExercise(out bool refused, bool skip = false)
        {
            Assert.IsTrue(data.state == PersonState.Workout ||
                (state == PersonState.WorkoutPending && !personManager.gameCore.isGameStarted));

            if (data.state == PersonState.Workout ||
                (state == PersonState.WorkoutPending && !personManager.gameCore.isGameStarted))
            {
                Equipment equipment = workoutEquipment;
                if (equipment != null)
                {
                    Assert.IsNotNull(equipment);

                    var exercise = data.workoutExercise;
                    Assert.IsNotNull(exercise);

                    ExerciseFlags flags = ExerciseFlags.None;
                    if (!personManager.gameCore.isGameStarted)
                        flags |= ExerciseFlags.EndsInOffline;

                    if (skip)
                        flags |= ExerciseFlags.Skipped;

                    if (AboutToEndExercise(flags))
                    {
                        data.workoutEquipmentId = EquipmentData.InvalidId;
                        data.workoutExercise = null;
                        data.exerciseTotalTime = 0.0f;
                        data.exerciseEndTime = DateTime.MinValue;

                        CharacterFinishingExerciseEvent.Send(this, equipment, exercise, data.exerciseInitiator, flags);

                        ChangeState(PersonState.ExerciseComplete);

                        CharacterEndedExerciseEvent.Send(this, equipment, exercise, data.exerciseInitiator, flags);

                        equipment.EndExercise(exercise);
                        refused = false;
                        return true;
                    }
                    else
                    {
                        // Rescheduled another exercise, skip ending.
                        refused = true;
                        return true;
                    }
                }
            }

            Debug.LogErrorFormat("Failed to end exercise. Wrong state or workout equipment id or its state. Person id: {0}, State: {1}, Equipment id: {2}."
                , id, state, data.workoutEquipmentId);

            refused = false;
            return false;
        }

        public void ChangeExerciseDuration(TimeSpan newDuration)
        {
            Assert.IsTrue(newDuration >= TimeSpan.Zero);
            Assert.IsTrue(data.state == PersonState.WorkoutPending || data.state == PersonState.Workout);

            if (newDuration >= TimeSpan.Zero && (data.state == PersonState.WorkoutPending || data.state == PersonState.Workout))
            {
                SetExerciseTimes(DateTime.UtcNow, newDuration);
            }
        }

        public bool CancelPendingExercise()
        {
            Assert.IsTrue(data.state == PersonState.WorkoutPending);

            if (data.state == PersonState.WorkoutPending)
            {
                Equipment equipment;
                if (personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment))
                {
                    Assert.IsNotNull(equipment);

                    data.workoutEquipmentId = EquipmentData.InvalidId;
                    data.workoutExercise = null;
                    data.exerciseTotalTime = 0.0f;
                    data.exerciseEndTime = DateTime.MinValue;

                    ChangeState(PersonState.Idle);

                    equipment.CancelPendingExercise();

                    CharacterCancelPendingExerciseEvent.Send(this, equipment);
                    return true;
                }
            }

            Debug.LogErrorFormat("Failed to cancel pending exercise. Wrong state or workout equipment id or its state. Person id: {0}, State: {1}, Equipment id: {2}."
                , id, state, data.workoutEquipmentId);

            return false;
        }

        public void SkipExercise()
        {
            if (data.state == PersonState.Workout &&
                data.exerciseEndTime != DateTime.MinValue &&
                data.exerciseEndTime - DateTime.UtcNow > TimeSpan.Zero)
            {
                Assert.IsTrue(data.workoutEquipmentId != EquipmentData.InvalidId);
                Assert.IsNotNull(data.workoutExercise);

                switch (data.workoutExercise.interruptType)
                {
                    case Exercise.InterruptType.Uninterruptable:
                        {
                            Debug.LogErrorFormat("Logic error: trying to skip uninterruptible exercise. Person id: {0}, Equipment id: {1}, Exercise id: {2}."
                                , id, data.workoutEquipmentId, data.workoutExercise.id);
                        }; break;

                    case Exercise.InterruptType.ForBucks:
                    case Exercise.InterruptType.ForBucksNoFree:
                        {
                            if (personManager.gameCore.quests.tutorial.isRunning)
                            {
                                PerformSkipExercise();
                            }
                            else
                            {
                                if (data.exerciseEndTime != DateTime.MaxValue.ToUniversalTime())
                                {
                                    var timeLeftNoFractions = Mathf.RoundToInt((float)((data.exerciseEndTime - DateTime.UtcNow).TotalSeconds));
                                    var skipCost = data.workoutExercise.interruptType == Exercise.InterruptType.ForBucksNoFree ? DynamicPrice.GetSkipCostNoFree(timeLeftNoFractions) : DynamicPrice.GetSkipCost(timeLeftNoFractions);

                                    if (skipCost <= 0 ||
                                        data.exerciseInitiator == ExerciseInitiator.DebugAutoPlay ||
                                        personManager.gameCore.playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, skipCost, 1), new Analytics.MoneyInfo("Skip", "Exercise")))
                                    {
                                        PerformSkipExercise();
                                    }
                                }
                                else
                                {   // Cannot skip infinite exercise for bucks - it's to costly! ;-) So, issue warning and skip for free.
                                    Debug.LogErrorFormat("Logic warning: trying to skip infinite exercise for bucks, skipping it for free! Person id: {0}, Equipment id: {1}, Exercise id: {2}."
                                        , id, data.workoutEquipmentId, data.workoutExercise.id);
                                    PerformSkipExercise();
                                }
                            }
                        }; break;

                    case Exercise.InterruptType.ForFree:
                        {
                            PerformSkipExercise();
                        }; break;
                }
            }
        }
        #endregion

        #region Debug API.
#if DEBUG
        public string GetDebugInfo (bool verbose)
        {
            if (task != null)
            {
                return task.GetDebugInfo(verbose);
            }

            return null;
        }
#endif
        #endregion

        #region Public virtual API.
        public virtual PersonData Destroy()
        {
            if (task != null)
            {
                task.Abort();
                task = null;
            }

            CharacterDeathEvent.Send(id);
            personManager = null;

            return data;
        }

        public virtual void OnStateChanged(PersonState oldState, PersonState newState)
        {
            idleType = IdleType.Idle; // Reset idle type on ANY state change.
        }

        public virtual void OnPostCreate(EntranceMode entranceMode)
        {
            if (entranceMode != EntranceMode.NoMove)
                EnterOrQueue(entranceMode);
        }

        public abstract void OnLoaded();

        public virtual void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            Assert.IsFalse(personManager.gameCore.isGameStarted);

            switch (data.state)
            {
                case PersonState.Entering:
                {
                    EnterRoom(EntranceMode.ImmediateEntranceFast);
                }; break;

                case PersonState.Idle:
                break;

                case PersonState.WorkoutPending:
                case PersonState.Workout:
                {
                    ProcessOfflineExercisePostLoad(timePassedSinceSave, loadTimeUTC);
                }; break;

                case PersonState.Leaving:
                {   // All leaving persons are marked dead (as already left) on load.
                    ChangeState(PersonState.Sentenced);
                }; break;

                case PersonState.ExerciseComplete:
                {
                }; break;

                case PersonState.InQueue:
                {
                    // Do nothing, it will be updated from Room Manager when game start signaled.
                }; break;
            }

            if (task != null)
            {
                task.PostLoad((float)timePassedSinceSave.TotalSeconds);
            }
        }

        public virtual void Quit()
        {
        }

        public virtual void OnConverted(string newRoomName, EntranceMode entranceMode)
        {
            if (data.roomName != newRoomName)
            {
                ChangeState(PersonState.Entering);

                data.roomName = newRoomName;
                CharacterTransferEvent.Send(this, newRoomName);

                EnterOrQueue(entranceMode);
            }
        }

        public virtual bool SelectRequest()
        {
            return isSelectable;
        }

        public virtual void OnSelected()
        {
            if (canSetTask)
                ResetTasks();
        }

        public virtual void OnDeselected()
        {
        }

        public virtual void EnterRoom (EntranceMode entranceMode)
        {
            ChangeState(PersonState.Entering);

            var activatorTask = new TaskDelegate
                ( new TaskDelegate.Settings
                {
                    isInterruptable = (task) => false,
                    // onAborted = activatorAction
                    onActivated = (task) =>
                    {
                        // Debug.LogFormat(">>> Activating {0} ({1})", id, data.name);

                        ChangeState(PersonState.Idle);
                        CharacterEnteredRoomEvent.Send(task.person);
                    }
                });

            activatorTask.AddTask
                ( this
                , new TaskGoto
                    ( new CharacterDestination(lobbyRoomZoneName)
                    , DataTables.instance.balanceData.Characters.Movement.EnteringRoom
                    , (entranceMode == EntranceMode.ImmediateEntranceFast) ? MovementMode.Running : MovementMode.WalkingFast
                    , true)
                );

            SetTask(activatorTask);
        }

        public virtual void MoveInQueue(string queueZoneName)
        {
            Assert.IsTrue(state == PersonState.InQueue);
            if (state == PersonState.InQueue)
            {
                var destination = new CharacterDestination(queueZoneName);
                var existingGotoTask = FindTask<TaskGoto>();
                if (existingGotoTask != null)
                {
                    existingGotoTask.UpdateDestination(destination);
                }
                else
                {
                    SetTask(new TaskGoto
                        ( destination
                        , DataTables.instance.balanceData.Characters.Movement.MoveInQueue));
                }
            }
        }

        public virtual bool MoveRequest(CharacterDestination destination)
        {
            if (canSetTask)
            {
                var existingGotoTask = FindTask<TaskGoto>();
                if (existingGotoTask != null)
                {
                    existingGotoTask.UpdateDestination(destination);
                }
                else
                {
                    SetTask(new TaskGoto
                            ( destination
                            , DataTables.instance.balanceData.Characters.Movement.Move));
                }

                return true;
            }

            return false;
        }

        public virtual bool KickRequest()
        {
            return false;
        }

        public virtual bool CanUseEquipment(Equipment equipment, int tier)
        {
            return false;
        }

        public virtual bool CanUseEquipment(Equipment equipment)
        {
            return false;
        }

        public virtual bool TryToUseEquipment
            ( Equipment equipment
            , Exercise exercise
            , ExerciseInitiator initiator
            , TimeSpan? customDuration = null
            , MovementSettings customMovementSettings = null)
        {
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);

            if (equipment == null)
            {
                Debug.LogError("Equipment is null!");
                return false;
            }

            if (exercise == null)
            {
                Debug.LogError("Exercise is null!");
                return false;
            }
            
            if (equipment.isUsable && canSetTask)
            {
                if (CanUseEquipment(equipment, exercise.tier))
                {
                    if (data.state == PersonState.ExerciseComplete)
                    {
                        ChangeState(PersonState.Idle);
                    }

                    var task = new TaskExercise(this, equipment, exercise, initiator, customDuration, customMovementSettings);
                    SetTask(task);

                    personManager.DeselectCharacterIfSelectedRequest(id);
                    return true;
                }
                else
                {
                    CharacterRefusedExerciseEvent.Send(this, equipment, exercise);
                }
            }

            return false;
        }

        public virtual QuestStage? GetQuestStage()
        {
            return null;
        }

        public virtual bool ClaimQuestRequest()
        {
            return false;
        }

        public virtual bool AboutToEndExercise(ExerciseFlags flags)
        {
            return true; // True - allow ending. False - another exercise rescheduled.
        }

#if DEBUG
        public virtual void DebugAutoPlay ()
        {

        }
#endif
        #endregion

        #region Protected API.
        protected void ChangeState(PersonState newState)
        {
            if (state != newState)
            {
                Assert.IsTrue(state != PersonState.Sentenced); // You cannot cancel 'Sentenced' state and go to another!
                if (state != PersonState.Sentenced)
                {
                    var oldState = state;
                    data.state = newState;

                    var cachedNewState = newState;

                    OnStateChanged(oldState, state);

                    if (cachedNewState == data.state)   // Send only if it's not changed again in a OnStateChanged() callback.
                        CharacterStateChangedEvent.Send(this, oldState, cachedNewState);

                    if (state == PersonState.Sentenced)
                        CharacterSentencedEvent.Send(this);
                }
            }
        }
        #endregion

        #region Protected virtual API.
        protected virtual void EnterRoomQueue (string roomNameQueueZone, bool run)
        {
            Assert.IsTrue(state == PersonState.Entering);

            ChangeState(PersonState.InQueue);

            SetTask(new TaskGoto
                ( new CharacterDestination(roomNameQueueZone)
                , DataTables.instance.balanceData.Characters.Movement.MoveInQueue
                , run ? MovementMode.Running : MovementMode.WalkingFast));
        }

        protected virtual void LeaveRoom(bool kicked = false)
        {
            if (state != PersonState.Leaving)
            {
                ChangeState(PersonState.Leaving);
                CharacterLeavingRoomEvent.Send(this, kicked);
            }

            AddTask
                ( new TaskGoto
                    ( new CharacterDestination(despawnRoomZoneName)
                    , DataTables.instance.balanceData.Characters.Movement.LeavingRoom
                    , true)
                );
            AddTask(new TaskKill());
        }

        protected virtual void PerformSkipExercise ()
        {
            personManager.DeselectCharacterIfSelectedRequest(id);
//          personManager.SelectCharacterRequest(id);
            ResetTasks();
        }

        protected virtual void SetName(string name)
        {
            data.name = name;
        }
        #endregion

        #region Private methods.
        private void EnterOrQueue (EntranceMode entranceMode)
        {
            string queueZoneName;
            bool run;
            if (entranceMode == EntranceMode.GoToQueue && personManager.gameCore.roomManager.GetPersonQueueCandidateZoneName(this, out queueZoneName, out run))
            {
                EnterRoomQueue(queueZoneName, run);
            }
            else
            {
                EnterRoom(entranceMode == EntranceMode.GoToQueue ? EntranceMode.ImmediateEntranceFast : entranceMode);
            }
        }

        private void SetExerciseTimes (DateTime startDate, TimeSpan duration)
        {
            data.exerciseTotalTime = (duration == TimeSpan.Zero) ? 0.0f : (float)duration.TotalSeconds;
            data.exerciseEndTime = (duration == TimeSpan.Zero) ? DateTime.MaxValue.ToUniversalTime() : startDate + duration;
        }

        private bool ProcessOfflineExercisePostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            Assert.IsTrue(state == PersonState.WorkoutPending || state == PersonState.Workout);

            if (personManager.gameCore.isGameStarted)
            {
                Debug.LogErrorFormat("Cannot use Person's 'offline' APIs when game is already running.");
                return false;
            }

            if (data.exerciseTotalTime == 0)
            {   // Workaround for old saves (SerializationVersion < 11), when there was no exerciseTotalTime: calculate to the same value as time left.
                // If time is already passed do another workaround: use 1 second.
                data.exerciseTotalTime = (data.workoutExercise != null)
                    ? (float)data.workoutExercise.duration.TotalSeconds
                    : Math.Max(1.0f, (float)(data.exerciseEndTime - loadTimeUTC).TotalSeconds);
            }

            if (state == PersonState.WorkoutPending || state == PersonState.Workout)
            {
                if (data.exerciseEndTime != DateTime.MinValue)  // Compatibility with old saves, when "WorkoutPending" gets canceled after loading game and there was no exerciseEndTime set while in WorkoutPending.
                {
                    // Check that all parameters and states are still valid.
                    Equipment equipment;
                    if (data.workoutExercise != null &&
                        data.workoutEquipmentId != EquipmentData.InvalidId &&
                        personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment))
                    {
                        // Reschedule.
                        var speed = CalculateExerciseAnimationSpeed();

                        if (equipment.StartPendingExercise(this, speed))
                        {
                            ChangeState(PersonState.Workout); // In case it's still in WorkoutPending state.

                            CharacterStartExerciseEvent.Send
                                ( this
                                , equipment
                                , data.workoutExercise
                                , data.exerciseInitiator
                                , TimeSpan.FromSeconds(data.exerciseTotalTime) // Not sure what value to use here: full duration or duration left till end.
                                , ExerciseFlags.Restored
                                , speed);

                            SetTask(new TaskExercise(this, equipment));

                            if (data.exerciseEndTime < loadTimeUTC) // Already finished while was in offline? 
                            {
                                bool refused = false;
                                if (TryToEndExercise(out refused, false))
                                {
                                    if (!refused)
                                    {
                                        Assert.IsTrue(state != PersonState.Workout);

                                        ResetTasks(); // It should immediately gracefully abort TaskExercise without duplicate attempts to end exercise.
                                    }

                                    return true;
                                }
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }

                // Failed to restore - reset to Idle.
                AbortExercise();
            }
            else
            {
                Debug.LogErrorFormat("Trying to end exercise for person {0} ({1}) when he at wrong state: {2}."
                    , id, name, state);
            }

            return false;
        }

        private float CalculateExerciseAnimationSpeed()
        {
            return Math.Max
                ( Mathf.Epsilon
                , UnityEngine.Random.Range
                    (DataTables.instance.balanceData.Characters.exerciseSpeedMultiplierMin,
                        DataTables.instance.balanceData.Characters.exerciseSpeedMultiplierMax));
        }

        private void AbortExercise()
        {
            Assert.IsTrue(data.state == PersonState.WorkoutPending || data.state == PersonState.Workout);
            Assert.IsFalse(personManager.gameCore.isGameStarted);

            if (data.state == PersonState.WorkoutPending || data.state == PersonState.Workout)
            {
                Equipment equipment;
                if (data.workoutEquipmentId != EquipmentData.InvalidId &&
                    personManager.gameCore.equipmentManager.equipment.TryGetValue(data.workoutEquipmentId, out equipment))
                {
                    equipment.AbortExercise();
                }

                data.workoutEquipmentId = EquipmentData.InvalidId;
                data.workoutExercise = null;
                data.exerciseTotalTime = 0.0f;
                data.exerciseEndTime = DateTime.MinValue;

                ChangeState(PersonState.Idle);
            }
        }
        #endregion
    }
}