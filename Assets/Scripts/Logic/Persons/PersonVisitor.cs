﻿using System.Linq;
using Core;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.Quests;
using Data.Quests.Juicer;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Events;
using Logic.Persons.Tasks;
using Logic.Quests.Base;
using Logic.Quests.Visitor;
using Logic.Serialization;
using UnityEngine.Assertions;

namespace Logic.Persons
{
    public class PersonVisitor : Person, IPersonVisitorInfo
    {
        #region Static/readonly data.
        private static readonly string visitorSpawnRoomZoneName             = "visitor_spawn";
        private static readonly string visitorDespawnRoomZoneName           = "visitor_despawn";
        private static readonly string visitorLobbyRoomZoneName             = "visitor_lobby";
        // private static readonly string visitorQueueZoneNamePrefix           = "visitor_queue_";
        private static readonly string visitorIdleZoneName                  = CharacterDestination.wholeRoom;
        #endregion

        #region Public virtual properties.
        public override bool canBeTrainedByPlayer
        {
            get
            {
                return true;
            }
        }

        protected new PersonVisitorData data { get { return (PersonVisitorData)base.data; } }
        #endregion

        #region Properties.
        public VisitorQuestData     visitorQuestData    { get { return data.visitorQuestData; } } 
        public override string      spawnRoomZoneName   { get { return visitorSpawnRoomZoneName; } }
        public override string      despawnRoomZoneName { get { return visitorDespawnRoomZoneName; } }
        public override string      lobbyRoomZoneName   { get { return visitorLobbyRoomZoneName; } }
        public override string      idleZoneName        { get { return visitorIdleZoneName; } }
        public bool                 canAssignTrainer    { get { return GetCanAssignTrainer(); } }

/*
        public override string queueZoneNamePrefix
        {
            get
            {
                return visitorQueueZoneNamePrefix;
            }
        }
*/

        public VisitorQuestLogic    visitorQuest    { get; protected set; }
        #endregion

        #region Constructor.
        public PersonVisitor(IPersonManager personManager, PersonVisitorData personVisitorData)
            : base(personManager, personVisitorData)
        {
            visitorQuest = null;
        }

        protected PersonVisitor(IPersonManager personManager, PersonVisitorData personVisitorData, PersonVisitor convertFrom)
            : base(personManager, personVisitorData, convertFrom)
        {
            visitorQuest = convertFrom.visitorQuest;
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (personData.GetType() == typeof(PersonVisitorData)) // Exact match needed.
            {
                var personVisitorData = personData as PersonVisitorData;

                return new PersonVisitor(personManager, personVisitorData);
            }

            return null;
        }
        #endregion

        #region Public API.
        public VisitorQuestType? GetVisitorQuestCurrentObjectiveType()
        {
            return (visitorQuest != null && visitorQuest.currentObjective != null)
                ? new VisitorQuestType?(visitorQuest.currentObjective.type)
                : null;
        }

        public VisitorQuestObjectiveData GetVisitorQuestCurrentObjectiveData()
        {
            return (visitorQuest != null && visitorQuest.currentObjective != null)
                ? visitorQuest.currentObjective.GetData()
                : null;
        }

        public PersonSportsman BecameSportsman(SportsmanType sportsmanType, out PersonVisitorData oldData, out PersonSportsmanData newData)
        {
            oldData = data;

            newData = new PersonSportsmanData(data, sportsmanType);
            newData.appearance.bodyType = PersonAppearance.BodyType.Strong;

            return new PersonSportsman(personManager, newData, this);
        }

        public PersonJuicer BecameJuicer(out PersonVisitorData oldData, out PersonJuicerData newData)
        {
            oldData = data;

            var questData = new JuiceQuestData(id);

            newData = new PersonJuicerData(data, questData);

            // TODO: Uncomment, when you decide to change appearance.
            // UpdateJuicerAppearance(ref personJuicerData.appearance);

            return new PersonJuicer(personManager, newData, this);
        }

        public PersonTrainerClient BecameTrainerClient(out PersonVisitorData oldData, out PersonTrainerClientData newData)
        {
            oldData = data;

            newData = new PersonTrainerClientData(data);

            return new PersonTrainerClient(personManager, newData, this);
        }
        #endregion

        #region Public Debug API.
#if DEBUG
        public override void DebugAutoPlay ()
        {
            if (DebugHacks.visitorsAutoPlay && state != PersonState.Sentenced && this is PersonVisitor) // Trainer Clients cannot do auto-play now.
            {
                personManager.gameCore.DelayedInvoke(() =>
                {
                    if (personManager != null &&
                        personManager.gameCore != null &&
                        personManager.gameCore.quests != null &&
                        personManager.gameCore.quests.tutorial != null &&
                        !personManager.gameCore.quests.tutorial.isRunning &&
                        (state == PersonState.Idle || state == PersonState.ExerciseComplete || state == PersonState.Workout) &&
                        visitorQuest != null)
                    {
                        if (state == PersonState.ExerciseComplete)
                        {
                            if (visitorQuest.completed)
                            {
                                personManager.ClaimQuestRequest(this);
                            }
                            else
                            {
                                personManager.SelectCharacterRequest(id);
                            }
                        }
                        else if (state == PersonState.Workout)
                        {
                            personManager.SkipExerciseRequest(id);
                        }
                        else if (state == PersonState.Idle)
                        {
                            if (visitorQuest.completed)
                            {
                                personManager.ClaimQuestRequest(this);
                            }
                            else
                            {
                                var equipmentCandidate = personManager.gameCore.equipmentManager.equipment.Values
                                    .Where ( e =>
                                                    e.state == Data.Estate.EquipmentState.Idle &&
                                                    e.roomName == roomName &&
                                                    CanUseEquipment(e))
                                    .FirstOrDefault();

                                if (equipmentCandidate != null)
                                {
                                    int tierIdx = -1;
                                    for (int i = 1; i <= 3; i++)
                                    {
                                        if (CanUseEquipment(equipmentCandidate, i))
                                        {
                                            tierIdx = i - 1;
                                            break;
                                        }
                                    }

                                    if (tierIdx >= 0)
                                        personManager.gameCore.equipmentManager.UseEquipmentRequest(equipmentCandidate.id, id, tierIdx, ExerciseInitiator.DebugAutoPlay);
                                }
                                else if (!personManager.gameCore.equipmentManager.isInEditMode)
                                {
                                    var estateTypeCandidate = 
                                        visitorQuest.objectives
                                            .Where ( o => !o.completed && o.type == VisitorQuestType.Exercise)
                                            .Select ( o => o.exercise.equipment )
                                            .FirstOrDefault();

                                    if (estateTypeCandidate != null)
                                    {
                                        var shopItem = ((Shop.IShopManager)personManager.gameCore.shop).FindShopItemForEstateType(estateTypeCandidate);

                                        if (shopItem != null)
                                        {
                                            var shpp = personManager.gameCore.equipmentManager as Shop.IShopPurchaseProtocol;

                                            shpp.InitPreview (shopItem, (result) =>
                                                {
                                                    if (result)
                                                    {
                                                        shpp.ConfirmPurchase();
                                                    }
                                                    else
                                                    {
                                                        shpp.CancelPurchase();
                                                    }
                                                });
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
#endif
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetTask(new TaskIdle());

            base.OnPostCreate(entranceMode);
        }

        public override void OnLoaded()
        {
            if (state != PersonState.InQueue)
            {
                SetupVisitorQuest();
            }

            SetTask(new TaskIdle());
        }

        public override bool CanUseEquipment(Equipment equipment, int tier)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif
            return  equipment.meetsLevelRequirement &&
                    visitorQuest != null &&
                    !visitorQuest.completed &&
                    visitorQuest.CanUseEquipment(equipment.estateType, tier);
        }

        public override bool CanUseEquipment(Equipment equipment)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif

            return  equipment.meetsLevelRequirement &&
                    visitorQuest != null &&
                    !visitorQuest.completed &&
                    visitorQuest.CanUseEquipment(equipment.estateType);
        }

        public override QuestStage? GetQuestStage()
        {
            return (visitorQuest != null)
                ? new QuestStage?(visitorQuest.questStage)
                : null;
        }

        public override bool ClaimQuestRequest()
        {
            if (visitorQuest != null && visitorQuest.completed)
            {
                if (visitorQuest.reward.rewardType == QuestRewardType.BecameSportsmen)
                {
                    if (!personManager.gameCore.sportsmen.CanAdd())
                    {
                        personManager.gameCore.sportsmen.WarnSportRoomFull();
                        return false;
                    }
                }

                if (visitorQuest.Claim())
                {
                    // WARNING! We can be converted to another Person type here and dropped from persons list.

                    personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    return true;
                }
            }
            return false;
        }

        public override void OnStateChanged(PersonState oldState, PersonState newState)
        {
            base.OnStateChanged(oldState, newState);

            if (newState != PersonState.Idle)
            {
                personManager.DeselectCharacterIfSelectedRequest(id);
            }

            if (newState == PersonState.ExerciseComplete)
            {
                if (data.appearance.bodyType != PersonAppearance.BodyType.Strong && visitorQuest != null)
                {
                    if (visitorQuest.completed)
                    {
                        data.appearance.bodyType = PersonAppearance.BodyType.Strong;
                        CharacterAppearanceChangedEvent.Send(this);
                    }
                    else if (data.appearance.bodyType == PersonAppearance.BodyType.Fat &&
                             visitorQuest.objectivesCompleted >= visitorQuest.objectivesCount / 2)
                    {
                        data.appearance.bodyType = PersonAppearance.BodyType.Normal;
                        CharacterAppearanceChangedEvent.Send(this);
                    }
                }
            }

            personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        public override void EnterRoom (EntranceMode entranceMode)
        {
            base.EnterRoom (entranceMode);

            SetupVisitorQuest();
        }

        public override bool SelectRequest()
        {
            return isSelectable;
        }

        public override void OnSelected()
        {
            base.OnSelected();

            if (visitorQuest != null)
            {
                visitorQuest.WasSeen();

                if (data.state == PersonState.ExerciseComplete && !visitorQuest.completed)
                {
                    ChangeState(PersonState.Idle);
                }
            }
        }

        public override bool KickRequest()
        {
            if (visitorQuest != null && ((data.state == PersonState.Idle && canSetTask) ||
                                         (data.state == PersonState.ExerciseComplete && visitorQuest.reward.rewardType == QuestRewardType.BecameSportsmen)))
            {
                LeaveRoom(true);

                visitorQuest.OnVisitorKick();
                visitorQuest = null;
                data.visitorQuestData = null;

                PersistentCore.instance.analytics.GameEvent("Kick Visitor");
                return true;
            }

            return false;
        }

        public override void Quit()
        {
            Assert.IsNotNull(visitorQuest);

            var rewardType = visitorQuest.reward.rewardType;

            personManager.gameCore.quests.visitorQuests.RemoveQuest(visitorQuest);
            visitorQuest = null;
            data.visitorQuestData = null;

            if (rewardType == QuestRewardType.BecameSportsmen)
            {
                // Simulate room leaving.
                ChangeState(PersonState.Leaving);
                CharacterLeavingRoomEvent.Send(this, false);

                var sportsmen = personManager.gameCore.sportsmen.BecameSportsman ( this, SportsmanType.Sportsman_0 );
                if (sportsmen == null)
                {   // Failed to became a sportsmen (some other conditions not valid) - just leave room.
                    LeaveRoom();
                }
            }
            else
            {
                var juiceBarRoomName = personManager.gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
                var roomNeedsClients = personManager.gameCore.juiceBar.GetCurrentClientsCount() < personManager.gameCore.juiceBar.GetMaxClientsCount();
                var queueNeedsClients = personManager.gameCore.roomManager.GetNumberOfPersonsInQueue(juiceBarRoomName) < personManager.gameCore.roomManager.GetRoomCurrentQueueMaxPlaces(juiceBarRoomName, false);

                if (roomNeedsClients || queueNeedsClients)
                {
                    // Simulate room leaving.
                    ChangeState(PersonState.Leaving);
                    CharacterLeavingRoomEvent.Send(this, false);

                    var juicer = personManager.gameCore.personManager.BecameJuicer
                        ( this
                        , personManager.gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar)
                        , roomNeedsClients ? EntranceMode.ImmediateEntrance : EntranceMode.GoToQueue );

                    if (juicer == null)
                    {   // Failed to became a juicer (some other conditions not valid) - just leave room.
                        LeaveRoom();
                    }
                }
                else
                {
                    LeaveRoom();
                }
            }
        }
        #endregion

        #region Protected virtual API.

        #endregion

        #region Protected API
        protected void SetupVisitorQuest()
        {
            if (visitorQuest == null && visitorQuestData != null && personManager.gameCore.quests != null) // Quests system can be null in Demo mode.
            {
                visitorQuest = personManager.gameCore.quests.visitorQuests.GetVisitorQuest(visitorQuestData.visitorId);
                if (visitorQuest == null)
                {
                    visitorQuest = personManager.gameCore.quests.visitorQuests.NewVisitorQuest(visitorQuestData);
                }
                visitorQuest.visitorName = data.name;
            }
        }
        #endregion

        #region Private methods.
        bool GetCanAssignTrainer()
        {
            if (this is PersonTrainerClient)
                return false;
            
            if (visitorQuest == null || visitorQuest.completed)
                return false;
            
            return canSetTask;
        }
        #endregion
    }
}
