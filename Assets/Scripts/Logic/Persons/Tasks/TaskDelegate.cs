﻿using System;
using Data.Person;

namespace Logic.Persons.Tasks
{
    public class TaskDelegate : Task
    {
        #region Types.
        public struct Settings
        {
            public Func<Task, bool>                     onStart;
            public Action<Task>                         onActivated;
            public Action<Task>                         onDeactivated;
            public Func<Task, float, UpdateResult>      onUpdate;
            public Action<Task>                         onAborted;
            public Func<Task, bool>                     onPreviousSiblingCanceled;
            public Func<Task, bool>                     onChildCanceled;
            public Func<Task, bool>                     isInterruptable;

            public float                                updateDelay;
        }
        #endregion

        #region Private data.
        Settings settings;
        float updateDelay;
        #endregion

        #region Constuctor.
        public TaskDelegate(Settings settings)
        : base()
        {
            this.settings = settings;
        }
        #endregion

        #region Protected virtual API.
        protected override bool isInterruptable
        {
            get
            {
                return (settings.isInterruptable != null) ? settings.isInterruptable(this) : false;
            }
        }

        protected override bool OnStart()
        {
            return (settings.onStart != null)
                ? settings.onStart(this)
                : true;
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            if (updateDelay > 0)
                updateDelay -= dt;

            if (updateDelay <= 0)
            {
                return (settings.onUpdate != null)
                    ? settings.onUpdate(this, dt)
                    : UpdateResult.Complete;
            }
            else
            {
                return UpdateResult.Running;
            }
        }

        protected override void OnAborted()
        {
            settings.onAborted?.Invoke(this);
        }

        protected override void OnActivated()
        {
            updateDelay = settings.updateDelay;

            settings.onActivated?.Invoke(this);
        }

        protected override void OnDeactivated()
        {
            settings.onDeactivated?.Invoke(this);
        }

        protected override bool OnPreviousSiblingCanceled()
        {
            return (settings.onPreviousSiblingCanceled != null)
                ? settings.onPreviousSiblingCanceled(this)
                : true;
        }

        protected override bool OnChildCanceled()
        {
            return (settings.onChildCanceled != null)
                ? settings.onChildCanceled(this)
                : true;
        }
        #endregion
    }
}