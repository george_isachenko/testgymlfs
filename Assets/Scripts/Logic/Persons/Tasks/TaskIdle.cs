﻿using System;
using System.Linq;
using Data;
using Data.Person;
using Data.Room;
using Logic.Persons.Events;
using MathHelpers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons.Tasks
{
    public class TaskIdle : Task
    {
        #region Private data.
        float timeIdleNoSelect;
        float timeIdleSelected;
        BalanceData.CharactersBalance.IdleSettingsBalance idleSettings;
        #endregion

        #region Constructor.
        public TaskIdle()
        : base ()
        {
        }
        #endregion

        #region Protected virtual properties.
        protected override bool isInterruptable
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Protected virtual interface.
        protected override void OnActivated()
        {
            idleSettings = person.idleSettings; // Cache it, this accessor can be costly.

            timeIdleNoSelect = idleSettings.timeIdleUnselectedInitial;
            timeIdleSelected = idleSettings.timeIdleSelectedInitial;
            person.idleType = Person.IdleType.Idle;
        }

        protected override void OnDeactivated()
        {
            timeIdleNoSelect = 0;
            timeIdleSelected = 0;
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            var personState = person.state;

            var canPerformIdleRoutines = person.canPerformIdleRoutines;

            if (canPerformIdleRoutines && person.followingPersonId == PersonData.InvalidId)
            {
                if (person.personManager.selectedCharacterId == person.id)
                {
                    timeIdleNoSelect = 0;
                    timeIdleSelected += dt;
                }
                else
                {
                    timeIdleNoSelect += dt;
                    timeIdleSelected = 0;
                }
            }
            else
            {
                timeIdleNoSelect = 0;
                timeIdleSelected = 0;
            }

            if (person.idleType == Person.IdleType.Idle)
            {
                if (person.followingPersonId != PersonData.InvalidId)
                {
                    var followingPerson = person.personManager.GetPerson(person.followingPersonId);
                    if (followingPerson != null)
                    {
                        if (person.canSetTask &&
                            RoomCoordinates.Distance(followingPerson.position, person.position) >= 
                                DataTables.instance.balanceData.Characters.Movement.followDistanceThreshold)
                        {
                            var movementSettings = (person.followMovementSettings != null)
                                            ? person.followMovementSettings
                                            : DataTables.instance.balanceData.Characters.Movement.IdleDefaultFollow;

                            person.idleType = Person.IdleType.IdleWalking;
                            AddTask ( person
                                    , new TaskGoto
                                        ( new CharacterDestination (followingPerson)
                                        , movementSettings));

                            if (movementSettings.numRetries >= 0)   // One time follow - unfollow after first try!
                            {
                                person.Follow(PersonData.InvalidId);
                            }
                        }

                        return UpdateResult.Running;
                    }
                    else
                    {
                        person.Follow(PersonData.InvalidId); // Invalid id - unfollow person!
                    }
                }

                if (timeIdleNoSelect >= idleSettings.timeIdleUnselectedToDoSomething)
                {
                    var selector = (person.canPerformFullIdle)
                        ? RandomSelector.SelectFrom
                            ( idleSettings.idleInteractionChancePerSecond * dt
                            , idleSettings.idleGreetingEmoteChancePerSecond * dt
                            , idleSettings.idleWalkChancePerSecond * dt )
                        : RandomSelector.SelectFrom
                            ( idleSettings.idleInteractionChanceInQueuePerSecond * dt );

                    switch (selector)
                    {
                        case 0:
                        {
                            var persons = person.personManager.persons;
                            Assert.IsNotNull(persons);

                            var candidates = persons.Values
                                .Where(p => 
                                    !ReferenceEquals(p, person) && 
                                    p.idleType == Person.IdleType.Idle &&
                                    (p.state == PersonState.InQueue || p.canSetTask) &&
                                    TaskInteraction.IsInteractionPossible(person, p)).ToArray(); // It's better to create an array here, than iterate lazily two times.

                            var count = candidates.Length;
                            if (count > 0)
                            {
                                var otherPerson = (count == 1) ? candidates.First() : candidates.ElementAt(UnityEngine.Random.Range(0, count));

                                AddTask ( person
                                        , new TaskInteraction
                                            ( otherPerson
                                            , AnimationSocial.Dialog_Random
                                            , UnityEngine.Random.Range(Math.Max(Mathf.Epsilon, idleSettings.idleInteractionMinTime), idleSettings.idleInteractionMaxTime)));

                            timeIdleNoSelect = idleSettings.timeIdleUnselectedToDoSomething - idleSettings.timeIdleUnselectedToDoNextAction;
                            }
                        }; break;

                        case 1:
                        {
                            //Debug.LogWarningFormat("Sending Greeting. Id: {0}, Name: '{1}'.", person.id, person.name);

                            PlayEmoteAnimationEvent.Send(person, AnimationSocial.Greeting, 1, true, null);
                            timeIdleNoSelect = idleSettings.timeIdleUnselectedToDoSomething - idleSettings.timeIdleUnselectedToDoNextAction;
                        }; break;

                        case 2:
                        {
                            person.idleType = Person.IdleType.IdleWalking;
                            var idleZoneName = person.idleZoneName;
                            AddTask ( person
                                    , new TaskGoto
                                        ( new CharacterDestination (idleZoneName == string.Empty ? CharacterDestination.wholeRoom : idleZoneName)
                                        , DataTables.instance.balanceData.Characters.Movement.IdleMove));
                                    timeIdleNoSelect = idleSettings.timeIdleUnselectedToDoSomething - idleSettings.timeIdleUnselectedToDoNextAction;
                        }; break;
                    }
                }
                else if (timeIdleSelected >= idleSettings.timeIdleSelectedToDoSomething)
                {
                }
            }

            return UpdateResult.Running;    // Kill task never ends, disallowing other tasks to run!
        }
        #endregion

        #region Debug API.
#if DEBUG
        public override string ToString(bool verbose)
        {
            return verbose
                ? string.Format("IdleType: {0}, IdleNoSelect: {1:N2}, IdleSelect: {2:N2}, Following: {3}"
                    , person.idleType, timeIdleNoSelect, timeIdleSelected, person.followingPersonId)
                : string.Empty;
        }
#endif
        #endregion
    }
}