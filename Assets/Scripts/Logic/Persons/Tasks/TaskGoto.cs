﻿using System;
using Data.Person;
using Data.Room;
using Logic.Persons.Events;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons.Tasks
{
    public class TaskGoto : Task , IMovementSettingsInfo
    {
        #region Private fields.
        CharacterDestination destination;
        IMovementSettingsInfo movementSettingsInfo;
        bool noInterrupt;

        MovementMode initialMovementMode;
        MovementMode currentMovementMode;
        int numRetriesLeft;
        bool needsUpdate;
        bool complete;
        MovementResult result;
        float retryLeft;
        float returnBackModeTimeLeft;
        #endregion

        #region IMovementSettingsInfo interface properties.
        MovementMode IMovementSettingsInfo.mode
        {
            get
            {
                return currentMovementMode;
            }
        }

        bool IMovementSettingsInfo.blendAnimations
        {
            get
            {
                return movementSettingsInfo.blendAnimations;
            }
        }

        int IMovementSettingsInfo.priorityDelta
        {
            get
            {
                return movementSettingsInfo.priorityDelta;
            }
        }

        int IMovementSettingsInfo.numRetries
        {
            get
            {
                return numRetriesLeft;
            }
        }

        float IMovementSettingsInfo.retryDelay
        {
            get
            {
                return movementSettingsInfo.retryDelay;
            }
        }
        #endregion

        #region Constructors.
        public TaskGoto
            ( CharacterDestination destination
            , MovementMode mode
            , bool blendAnimations = false
            , int priorityDelta = 0
            , int numRetries = -1
            , float retryDelay = 1.0f
            , bool noInterrupt = false)
        : base ()
        {
            this.destination = destination;
            movementSettingsInfo = new MovementSettings(mode, blendAnimations, priorityDelta, numRetries, retryDelay);
            this.noInterrupt = noInterrupt;

            returnBackModeTimeLeft = 0;

            Prepare();
        }

        public TaskGoto
            ( CharacterDestination destination
            , IMovementSettingsInfo movementSettingsInfo
            , bool noInterrupt = false)
        : base ()
        {
            this.destination = destination;
            this.movementSettingsInfo = movementSettingsInfo;
            this.noInterrupt = noInterrupt;

            returnBackModeTimeLeft = 0;

            Prepare();
        }

        public TaskGoto
            ( CharacterDestination destination
            , IMovementSettingsInfo movementSettingsInfo
            , MovementMode mode
            , bool noInterrupt = false)
        : base ()
        {
            this.destination = destination;
            this.movementSettingsInfo = new MovementSettings(movementSettingsInfo);
            this.noInterrupt = noInterrupt;

            returnBackModeTimeLeft = 0;

            Prepare();

            currentMovementMode = mode; // 'mode' from movementSettings is ignored.
        }

        public TaskGoto
            ( CharacterDestination destination
            , IMovementSettingsInfo movementSettingsInfo
            , MovementMode mode
            , int numRetries
            , bool noInterrupt = false)
        : base ()
        {
            this.destination = destination;
            this.movementSettingsInfo = new MovementSettings(movementSettingsInfo);
            this.noInterrupt = noInterrupt;

            returnBackModeTimeLeft = 0;

            Prepare();

            currentMovementMode = mode; // 'mode' from movementSettings is ignored.
            numRetriesLeft = numRetries; // // 'numRetries' from movementSettings is ignored.
        }

        public TaskGoto
            ( CharacterDestination destination
            , IMovementSettingsInfo movementSettingsInfo
            , int numRetries
            , bool noInterrupt = false)
        : base ()
        {
            this.destination = destination;
            this.movementSettingsInfo = new MovementSettings(movementSettingsInfo);
            this.noInterrupt = noInterrupt;

            returnBackModeTimeLeft = 0;

            Prepare();

            numRetriesLeft = numRetries; // // 'numRetries' from movementSettings is ignored.
        }
        #endregion

        #region Task API.
        protected override bool isInterruptable
        {
            get
            {
                if (noInterrupt)
                    return false;

                switch (destination.mode)
                {
                    case CharacterDestination.Mode.Coordinates:
                    {
                        return true;
                    }

                    case CharacterDestination.Mode.Equipment:
                    {
                        return hasChildren ? children[0].interruptable : true;  // Potentially Interruptable only if there's no sub-tasks or sub-tasks interruptable too.
                    }

                    case CharacterDestination.Mode.Zone:
                    {
//                         if (person.state == PersonState.InQueue || person.state == PersonState.Entering || person.state == PersonState.Leaving)
//                         {
//                             return person.personManager.gameCore.roomManager.IsRoomCoordinatesApproximatelyWithinRoom(person.roomName, person.position);
//                         }
//                         else
                        {
                            return true;
                        }
                    }

                    case CharacterDestination.Mode.Person:
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        protected override void OnAborted()
        {
            Log("OnAborted", "id : {0}, name: '{1}', complete: {2}.", person.id, person.name, complete);

            if (!complete)
                StopCharacterEvent.Send(person);
        }

        protected override void OnActivated()
        {
            Prepare();
            SendCharacter();
            needsUpdate = false;
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            if (needsUpdate)
            {
                needsUpdate = false;
                complete = false;
                numRetriesLeft = movementSettingsInfo.numRetries;
                retryLeft = movementSettingsInfo.retryDelay;
                SendCharacter();
            }

            if (complete)
            {
                switch (result)
                {
                    case MovementResult.Failed:
                    case MovementResult.Stuck:
                    {
                        if (numRetriesLeft != 0)
                        {
                            if (retryLeft <= 0.0f)
                            {
                                if (numRetriesLeft > 0)
                                    numRetriesLeft--;

                                Prepare();
                                SendCharacter();
                            }
                            else
                            {
                                retryLeft -= dt;
                            }
                        }
                        else
                        {
                            if (children != null)
                                children.Clear();
                            Log("OnUpdate", "Canceled. id : {0}, name: '{1}'", person.id, person.name);
                            return UpdateResult.Canceled;
                        }
                    }; break;

                    case MovementResult.Complete:
                    {
                        Log("OnUpdate", "Complete. id : {0}, name: '{1}'", person.id, person.name);
                        return UpdateResult.Complete;
                    }

                    case MovementResult.Stopped:
                    {
                        return UpdateResult.Canceled;
                    }

                    case MovementResult.UpdatedDestination:
                    {
                        Debug.LogError("Unexpected result!"); // It should be ignored in a OnMoveComplete() callback.
                    }; break;
                }
            }
            else
            {
                if (returnBackModeTimeLeft > 0)
                {
                    returnBackModeTimeLeft -= dt;

                    if (returnBackModeTimeLeft <= 0)
                    {
                        ChangeMovementMode(initialMovementMode, 0);
                    }
                }
            }

            return UpdateResult.Running;
        }


#if DEBUG
        public override string ToString(bool verbose)
        {
            return verbose
                ? string.Format("To: <{0}>, mode: {1}, retr: {2}, noInt.: {3}, prioDelta: {4}"
                    , destination, currentMovementMode, numRetriesLeft, noInterrupt, movementSettingsInfo.priorityDelta)
                : string.Format("To: <{0}>", destination);
        }
#endif
        #endregion

        #region Public API.
        public void UpdateDestination(CharacterDestination destination, MovementMode mode)
        {
            if (this.destination != destination)
            {
                this.destination = destination;
                this.currentMovementMode = mode;

                if (active)
                {
                    needsUpdate = true;
                    complete = false;
                    result = MovementResult.Failed;
                }
            }
        }

        public void UpdateDestination(CharacterDestination destination)
        {
            if (this.destination != destination)
            {
                this.destination = destination;

                if (active)
                {
                    needsUpdate = true;
                    complete = false;
                    result = MovementResult.Failed;
                }
            }
        }

        public void ChangeMovementMode(MovementMode mode, float returnBackTime = 0.0f)
        {
            if (currentMovementMode != mode && !complete)
            {
                currentMovementMode = mode;
                returnBackModeTimeLeft = returnBackTime;

                ChangeCharacterMovementModeEvent.Send(person, currentMovementMode);
            }
        }
        #endregion

        #region Private methods.
        private void OnMoveComplete(int personId, MovementResult result)
        {
            Assert.IsTrue(personId == person.id);
            Log("OnMoveComplete", "id : {0}, name: '{1}', result: {2}", person.id, person.name, result);

            if (!needsUpdate && result != MovementResult.UpdatedDestination)
            {
                complete = true;
                this.result = result;
            }
        }

        private void Prepare()
        {
            complete = false;
            result = MovementResult.Failed;
            numRetriesLeft = movementSettingsInfo.numRetries;
            retryLeft = movementSettingsInfo.retryDelay;
            currentMovementMode = initialMovementMode = movementSettingsInfo.mode;
        }

        private void SendCharacter()
        {
            MoveCharacterEvent.Send(person, destination, this, OnMoveComplete);
        }

        private void Log(string function, string format, params object[] args)
        {
            // Debug.LogFormat("TaskGoto.{0}: {1}", function, string.Format(format, args));
        }
        #endregion
    }
}