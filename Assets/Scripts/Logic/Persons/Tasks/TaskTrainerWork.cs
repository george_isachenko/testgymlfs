﻿using System;
using Data;
using Data.Person;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Events;
using MathHelpers;
using UnityEngine;
using UnityEngine.Assertions;
using Data.Estate;

namespace Logic.Persons.Tasks
{
    public class TaskTrainerWork : Task
    {
        #region Types.
        enum State
        {
            Initial = 0,
            WalkToClient,
            Conversation,
            WalkToEquipment,
            AwaitClientNearEquipment,
            QueueTraining,
            WaitForTrainingStart,
            RotatingTowardsClientForSupport,
            TrainSupport,
            SupportCooldown,
            Finished
        }
        #endregion

        #region Private data.
        State state;
        int equipmentCandidateId;
        Exercise exerciseCandidate;
        int trainingPersonId;
        int firstFailedToTrainCandidateId;
        float timeout;
        #endregion

        #region Protected properties.
        protected new PersonTrainer person
        {
            get
            {
                return (PersonTrainer)base.person;
            }
        }
        #endregion

        #region Constructor.
        public TaskTrainerWork()
        : base ()
        {
            Reset();
            firstFailedToTrainCandidateId = PersonData.InvalidId;
        }
        #endregion

        #region Protected virtual properties.
        protected override bool isInterruptable
        {
            get
            {
                return GetCurrentTrainingClient() == null;
            }
        }
        #endregion

        #region Protected virtual interface.
        protected override UpdateResult OnUpdate(float dt)
        {
            PersonTrainerClient currentTrainingPerson = GetCurrentTrainingClient();

            if (currentTrainingPerson != null)
            {
                if (trainingPersonId == PersonData.InvalidId)
                {
                    trainingPersonId = currentTrainingPerson.id;
                }
                else if (trainingPersonId != currentTrainingPerson.id)
                {
                    Reset();
                    person.SetClientsFollow(false);
                    currentTrainingPerson = null; // Will be updated at the end of function.
                }
            }

            if (currentTrainingPerson != null)
            {
                switch (state)
                {
                    case State.Initial:
                    {
                        if (currentTrainingPerson.isTraining)
                        {
                            WalkToTrainingClient(currentTrainingPerson);
                            state = State.WalkToClient;
                        }
                        else
                        {
                            currentTrainingPerson.ResetTasks();
                            WalkToTrainingClient(currentTrainingPerson);
                            state = State.WalkToClient;
                        }
                        return UpdateResult.Running;
                    }

                    case State.WalkToClient:
                    {
                        if (currentTrainingPerson.isTraining)
                        {
                            StopCharacterEvent.Send(person);

                            SupportTrainingClient(currentTrainingPerson);
                            return UpdateResult.Running;
                        }
                        else if (RoomCoordinates.Distance(person.position, currentTrainingPerson.position) <=
                                DataTables.instance.balanceData.Characters.Trainers.interactionCloseEnoughDistance)
                        {
                            StopCharacterEvent.Send(person);

                            StopCharacterEvent.Send(currentTrainingPerson);

                            CharacterLookAtEvent.Send(person, new CharacterDestination(currentTrainingPerson), null);
                            CharacterLookAtEvent.Send(currentTrainingPerson, new CharacterDestination(person), null);

                            PlayEmoteAnimationEvent.Send(person, AnimationSocial.Dialog_Random, -1, false, null);
                            PlayEmoteAnimationEvent.Send(currentTrainingPerson, AnimationSocial.Dialog_Random, -1, false, null);

                            timeout = UnityEngine.Random.Range(Math.Max(Mathf.Epsilon, DataTables.instance.balanceData.Characters.Trainers.prepareInteractionMinTime), DataTables.instance.balanceData.Characters.Trainers.prepareInteractionMaxTime);
                            state = State.Conversation;
                            return UpdateResult.Running;
                        }
                        else
                        {
                            Debug.LogWarningFormat("Not close enough to start conversation with a client (training: {0}), skipping it.", currentTrainingPerson.isTraining);
                            timeout = 0;
                            state = State.Conversation;
                        }
                    }; return UpdateResult.Running;

                    case State.Conversation:
                    {
                        if (timeout <= 0)
                        {
                            // Stops also emotes (interaction).
                            StopCharacterEvent.Send(person);
                            StopCharacterEvent.Send(currentTrainingPerson);

                            var equipmentCandidate = currentTrainingPerson.SelectTrainingEquipmentCandidateAndExercise
                                    ( person.personManager.gameCore.equipmentManager.equipment.Values
                                    , out exerciseCandidate);

                            if (equipmentCandidate != null && trainingPersonId == currentTrainingPerson.id)
                            {
                                equipmentCandidateId = equipmentCandidate.id;

                                state = State.WalkToEquipment; // Will return to task when walk is complete.

                                WalkEveryoneToEquipment(equipmentCandidate, currentTrainingPerson);
                            }
                            else
                            {
                                if (firstFailedToTrainCandidateId == currentTrainingPerson.id)
                                {
                                    // Looped through all clients and returned back to the same that failed before.
                                    // That means that we cannot train any client now - cancel task, report to owner, it will go to 'Refused' state.
                                    person.FailedToTrainAnyClient();
                                    return UpdateResult.Canceled; 
                                }
                                else if (firstFailedToTrainCandidateId == PersonData.InvalidId)
                                {
                                    firstFailedToTrainCandidateId = currentTrainingPerson.id;
                                }

                                state = State.Finished;
                            }
                        }
                        else
                        {
                            timeout -= dt;
                        }
                        return UpdateResult.Running;
                    };

                    case State.WalkToEquipment:
                    {
                        CharacterLookAtEvent.Send(person, new CharacterDestination(currentTrainingPerson), null);
                        timeout = DataTables.instance.balanceData.Characters.Trainers.awaitNearEquipmentFallbackTime;
                        state = State.AwaitClientNearEquipment;
                        return UpdateResult.Running;
                    };

                    case State.AwaitClientNearEquipment:
                    {
                        if (timeout <= 0 ||
                            RoomCoordinates.Distance(person.position, currentTrainingPerson.position) <=
                                Math.Max(DataTables.instance.balanceData.Characters.Movement.followDistanceThreshold, DataTables.instance.balanceData.Characters.Trainers.trainerAwaitsClientCloseEnoughDistance))
                        {
                            state = State.QueueTraining;
                        }
                        else
                        {
                            if (currentTrainingPerson.FindTask<TaskGoto>() == null)
                            {
                                timeout -= dt;
                            }
                        }
                        return UpdateResult.Running;
                    };

                    case State.QueueTraining:
                    {
                        // If initial candidate is missing or moved, or cannot be used - try to select another, if possible, and walk to it again.
                        // If cannot select new - abandon client for now and try to train another.

                        person.SetClientsFollow(false);

                        Assert.IsTrue(equipmentCandidateId != EquipmentData.InvalidId);
                        Equipment equipmentCandidate;
                        if (person.personManager.gameCore.equipmentManager.equipment.TryGetValue(equipmentCandidateId, out equipmentCandidate))
                        {
                            if (currentTrainingPerson.TryToUseEquipment
                                    ( equipmentCandidate
                                    , exerciseCandidate
                                    , ExerciseInitiator.Trainer
                                    , exerciseCandidate.duration
                                    , DataTables.instance.balanceData.Characters.Trainers.ClientMoveToEquipmentToWork))
                            {
                                state = State.WaitForTrainingStart;
                                firstFailedToTrainCandidateId = PersonData.InvalidId;
                                timeout = DataTables.instance.balanceData.Characters.Trainers.supportBeginCooldownTime; 
                                return UpdateResult.Running;
                            }
                            else
                            {
                                // TODO: Comment out this log after debug!
                                Debug.LogFormat("Client ({0}) refused to do an exercise '{1}' on equipment of type {2} with a trainer {3}."
                                    , currentTrainingPerson.id, exerciseCandidate.id, equipmentCandidate.estateType.id, person.id);
                                state = State.Finished;
                                break;
                            }
                        }

                        equipmentCandidate = currentTrainingPerson.SelectTrainingEquipmentCandidateAndExercise
                                ( person.personManager.gameCore.equipmentManager.equipment.Values
                                , out exerciseCandidate);

                        if (equipmentCandidate != null)
                        {   // Equipment changed, selected another equipment - walk to it again.
                            equipmentCandidateId = equipmentCandidate.id;

                            state = State.WalkToEquipment;

                            WalkEveryoneToEquipment(equipmentCandidate, currentTrainingPerson);

                        }
                        else
                        {
                            state = State.Finished;
                        }
                        return UpdateResult.Running;
                    }; 

                    case State.WaitForTrainingStart:
                    {
                        if (currentTrainingPerson.state == PersonState.Workout)
                        {
                            if (timeout <= 0)
                            {
                                // SupportTrainingClient(currentTrainingPerson);

                                WalkToTrainingClient(currentTrainingPerson);
                                state = State.WalkToClient;
                            }
                            else
                            {
                                timeout -= dt;
                            }
                        }
                        else if (currentTrainingPerson.state != PersonState.WorkoutPending)
                        {
                            Debug.LogWarningFormat("Something wrong, client ({0}) is not in Workout or WorkoutPending state, skipping. State: {1}"
                                , currentTrainingPerson.id, currentTrainingPerson.state);

                            state = State.Finished;
                        }
                        return UpdateResult.Running;
                    };

                    case State.RotatingTowardsClientForSupport:
                    {
                        // Nothing to do here, wait until it finishes rotating towards client, submits support animation via callback and switches to TrainSupport state (emote).
                        if (currentTrainingPerson.id != trainingPersonId || !currentTrainingPerson.isTraining)
                        {
                            EndSupport(currentTrainingPerson);
                        }

                        return UpdateResult.Running;
                    };

                    case State.TrainSupport:
                    {
                        if (currentTrainingPerson.id != trainingPersonId || !currentTrainingPerson.isTraining)
                        {
                            EndSupport(currentTrainingPerson);
                        }
                        return UpdateResult.Running;
                    };

                    case State.SupportCooldown:
                    {
                        if (timeout <= 0)
                        {
                            state = State.Finished;
                        }
                        else
                        {
                            timeout -= dt;
                        }
                        return UpdateResult.Running;
                    };

                    case State.Finished:
                    {
                        StopCharacterEvent.Send(person);
                        if (!currentTrainingPerson.isTraining)
                            StopCharacterEvent.Send(currentTrainingPerson);
                    }; break;
                }
            }

            if (person.SelectNextTrainingClient())
            {
                currentTrainingPerson = GetCurrentTrainingClient();

                Reset();

                if (currentTrainingPerson != null)
                {
                    trainingPersonId = currentTrainingPerson.id;
                    return UpdateResult.Running;
                }
            }

            // Should exit here only when finished training all clients.
            return UpdateResult.Complete;
        }
        #endregion

        #region Public Debug API.
#if DEBUG
        public override string ToString(bool verbose)
        {
            if (verbose)
            {
                if (equipmentCandidateId != EquipmentData.InvalidId)
                {
                    return string.Format("State: {0}, Client: {1}, Timeout: {2:N2}, Equipment: {3}, Exercise: {4}"
                        , state, trainingPersonId, timeout, equipmentCandidateId, exerciseCandidate != null ? exerciseCandidate.id.ToString() : "<null>");
                }
                else
                {
                    return string.Format("State: {0}, Client: {1}, Timeout: {2:N2},", state, trainingPersonId, timeout);
                }
            }
            else
            {
                return string.Format("State: {0}, Client: {1}", state, trainingPersonId);
            }
        }
#endif
        #endregion

        #region Private methods.
        private void Reset ()
        {
            state = State.Initial;
            equipmentCandidateId = EquipmentData.InvalidId;
            exerciseCandidate = null;
            trainingPersonId = PersonData.InvalidId;
        }

        private PersonTrainerClient GetCurrentTrainingClient()
        {
            var trainingClientLogic = person.currentTrainingClient;

            return (trainingClientLogic != null && trainingClientLogic.state == Trainers.TrainerClientType.InProgress)
                        ? trainingClientLogic.client : null;
        }

        private void WalkEveryoneToEquipment (Equipment equipmentCandidate, PersonTrainerClient trainingPerson)
        {
/*
            // Should keep it from going back to Idle and run somewhere if it reached equipment before trainer.
            var keeperTask = new TaskDelegate
                ( trainingPerson
                , new TaskDelegate.Actions
                    { onUpdate = (task, dt) =>
                        { return (state == State.WalkToEquipment || state == State.WalkToClientNearEquipment)
                            ? UpdateResult.Running
                            : UpdateResult.Complete;
                        }
                    }
                );

            trainingPerson.AddTask(keeperTask);

            keeperTask.AddTask(new TaskGoto
                ( trainingPerson
                , new CharacterDestination(equipmentCandidate)
                , DataTables.instance.balanceData.Characters.Movement.trainerClientMove
                , noInterrupt: true));
*/

            person.SetClientsFollow(true, DataTables.instance.balanceData.Characters.Trainers.ClientFollowOneTime);
            trainingPerson.Follow(person.id, DataTables.instance.balanceData.Characters.Trainers.ClientFollow); // For person that will be trained follow mode is mandatory, not optional (one time)!

            AddTask ( person
                    , new TaskGoto
                        ( new CharacterDestination(equipmentCandidate, "support_point")
                        , DataTables.instance.balanceData.Characters.Trainers.TrainerMoveToEquipment
                        , noInterrupt: true));
        }

        private void WalkToTrainingClient (PersonTrainerClient trainingPerson)
        {
            person.SetClientsFollow(true, DataTables.instance.balanceData.Characters.Trainers.ClientFollowOneTime);
            trainingPerson.Follow(PersonData.InvalidId); // For a person that will be trained or already training follow is turned off.

            if (trainingPerson.isTraining)
            {
                var equipment = trainingPerson.workoutEquipment;
                if (equipment != null)
                {
                    AddTask(person
                            , new TaskGoto
                                ( new CharacterDestination(equipment, "support_point")
                                , DataTables.instance.balanceData.Characters.Trainers.TrainerMoveToClient
                                , noInterrupt: true));
                    return;
                }
            }

            AddTask ( person
                    , new TaskGoto
                        ( new CharacterDestination(trainingPerson)
                        , DataTables.instance.balanceData.Characters.Trainers.TrainerMoveToClient
                        , noInterrupt: true));
        }

        private void SupportTrainingClient (PersonTrainerClient trainingPerson)
        {
            firstFailedToTrainCandidateId = PersonData.InvalidId; // Reset first failed to train client, because we have someone to support at least.

            var currentSupportingClientId = trainingPerson.id;

            var exerciseTier = (exerciseCandidate != null)
                ? exerciseCandidate.tier
                : (trainingPerson.workoutExercise != null) ? trainingPerson.workoutExercise.tier : 1;

            var animationLoops =  (exerciseTier >= 0 && exerciseTier < DataTables.instance.balanceData.Characters.Trainers.supportAnimationLoops.Count)
                    ? DataTables.instance.balanceData.Characters.Trainers.supportAnimationLoops[exerciseTier] : 1;

            state = State.RotatingTowardsClientForSupport;

            CharacterLookAtEvent.Send(person, new CharacterDestination(trainingPerson), () =>
            {
                if (state == State.RotatingTowardsClientForSupport && currentSupportingClientId == trainingPersonId) // Switch to next state only in case it's same state and client.
                {
                    state = State.TrainSupport;

                    PlayEmoteAnimationEvent.Send
                        ( person
                        , AnimationSocial.Support
                        , animationLoops
                        , false
                        , () =>
                        {
                            if (state == State.TrainSupport && currentSupportingClientId == trainingPersonId) // Switch to next state only in case it's same state and client.
                            {
                                EndSupport(trainingPerson);
                            }
                        });
                }
            });
        }

        private void EndSupport (PersonTrainerClient trainingPerson)
        {
            StopCharacterEvent.Send(person);
            if (!trainingPerson.isTraining)
                StopCharacterEvent.Send(trainingPerson);

            timeout = UnityEngine.Random.Range(Math.Max(Mathf.Epsilon, DataTables.instance.balanceData.Characters.Trainers.supportCooldownMinTime), DataTables.instance.balanceData.Characters.Trainers.supportCooldownMaxTime);
            state = State.SupportCooldown;
        }
#endregion
    }
}