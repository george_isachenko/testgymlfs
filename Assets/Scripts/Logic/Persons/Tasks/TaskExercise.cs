﻿using System;
using Data;
using Data.Estate;
using Data.Person;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Events;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons.Tasks
{
    public class TaskExercise : Task
    {
        #region Private data.
        Equipment equipment;
        TimeSpan initialDuration;
        Exercise exercise;
        ExerciseInitiator initiator;
        MovementSettings customMovementSettings;
        private bool isReadyToSkipInvoked = false;
        #endregion

        #region Protected virtual properties.
        protected override bool isInterruptable
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Constructor.
        public TaskExercise
            ( Person person
            , Equipment equipment
            , Exercise exercise
            , ExerciseInitiator initiator
            , TimeSpan? customDuration = null
            , MovementSettings customMovementSettings = null)
        : base()
        {
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            Assert.IsTrue(person.state == PersonState.Idle || person.state == PersonState.Entering);

            this.equipment = equipment;
            this.exercise = exercise;
            this.initiator = initiator;
            this.customMovementSettings = customMovementSettings;
            initialDuration = SetupInitialDuration(person, exercise, customDuration);
        }

        public TaskExercise
            (Person person, Equipment equipment)  // Used when restoring Task by PersonData on loading.
        : base()
        {
            Assert.IsNotNull(equipment);
            Assert.IsTrue(person.workoutEquipmentId == equipment.id);
            Assert.IsNotNull(person.workoutExercise);
            Assert.IsTrue(person.exerciseEndTime != DateTime.MinValue);
            Assert.IsTrue(person.state == PersonState.Workout);

            this.equipment = equipment;
            this.initialDuration = (person.exerciseEndTime == DateTime.MaxValue.ToUniversalTime())
                ? TimeSpan.Zero
                : person.exerciseEndTime - DateTime.UtcNow; // Can be negative, it's ok.
            this.exercise = person.workoutExercise;
        }
        #endregion

        #region Public API.
        public void UpdateDuration(TimeSpan newDuration)
        {
            var state = person.state;
            Assert.IsTrue(newDuration >= TimeSpan.Zero);

            if (state == PersonState.WorkoutPending || state == PersonState.Workout)
            {
                person.ChangeExerciseDuration(newDuration);
            }

            this.initialDuration = newDuration; 
        }

        public bool SwitchExercise(Exercise newExercise, TimeSpan? customDuration = null)
        {
            var state = person.state;
            Assert.IsTrue(state == PersonState.WorkoutPending || state == PersonState.Workout);

            var duration = SetupInitialDuration(person, newExercise, customDuration);

            if (person.SwitchExercise(newExercise, duration))
            {
                exercise = newExercise;
                initialDuration = duration;

                if (state == PersonState.WorkoutPending)
                {
                    if (person.FindTask<TaskGoto>() != null)
                    {
                        SetTask(person
                                , new TaskGoto
                                    (new CharacterDestination(equipment, newExercise.tier)
                                    , DataTables.instance.balanceData.Characters.Movement.MoveToEquipmentToWork
                                    , true));
                    }
                }

                return true;
            }

            return false;
        }
        #endregion

        #region Protected virtual API.
        protected override void OnActivated()
        {
            var state = person.state;
            Assert.IsTrue(state == PersonState.Idle || state == PersonState.WorkoutPending || state == PersonState.Workout);

            if (state == PersonState.Idle)
            {
                person.SchedulePendingExercise(equipment, exercise, initiator, initialDuration);

                AddTask ( person
                        , new TaskGoto
                            ( new CharacterDestination(equipment, exercise.tier)
                            , customMovementSettings != null
                                ? customMovementSettings
                                : DataTables.instance.balanceData.Characters.Movement.MoveToEquipmentToWork
                            , true));
            }
            else if (state == PersonState.WorkoutPending)
            {
                person.StartExercise(initialDuration);
            }
            else if (state == PersonState.Workout)
            {
                // Nothing to do, it is offine started or restored Exercise, so it's already started.
            }
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            var timeToFinish = (person.exerciseEndTime == DateTime.MaxValue.ToUniversalTime())
                ? TimeSpan.MaxValue
                : person.exerciseEndTime - DateTime.UtcNow;

            if (!isReadyToSkipInvoked &&
                (exercise.interruptType == Exercise.InterruptType.ForBucks || exercise.interruptType == Exercise.InterruptType.ForFree) &&
                timeToFinish.TotalSeconds < DataTables.instance.balanceData.Equipment.exerciseFreeSkipTime)
            {
                CharacterReadyToSkipExerciseEvent.Send(person, equipment, exercise);
                isReadyToSkipInvoked = true;
            }

            if (timeToFinish.TotalSeconds <= 0)
            {
                EndComplete(false);

                // Check again, can be switched to another exercise in same task.
                timeToFinish = (person.exerciseEndTime == DateTime.MaxValue.ToUniversalTime())
                    ? TimeSpan.MaxValue
                    : person.exerciseEndTime - DateTime.UtcNow;

                if (timeToFinish.TotalSeconds <= 0) 
                    return UpdateResult.Complete;
            }

            return UpdateResult.Running;
        }

        protected override void OnAborted()
        {
            // NOTE: Person can be also already in a ExerciseComplete or Idle states here when loading game and attempting to end offline exercises.
            if (person.state == PersonState.Workout)
            {
                EndComplete(true);
            }
            else if (person.state == PersonState.WorkoutPending)
            {
                person.CancelPendingExercise();
            }
        }
        #endregion

        #region Private methods.
        private void EndComplete(bool skip)
        {
            Assert.IsTrue(person.state == PersonState.Workout);

            bool refused;
            person.TryToEndExercise(out refused, skip);
        }
        #endregion

        #region Private static methods.
        private static TimeSpan SetupInitialDuration (Person person, Exercise exercise, TimeSpan? customDuration)
        {
            return (person.personManager.gameCore.playerProfile.level == 1)
                ? (exercise.duration > TimeSpan.FromSeconds(30) ? TimeSpan.FromSeconds(30) : exercise.duration)
                : ((customDuration != null) ? customDuration.Value : exercise.duration);
        }
        #endregion

        #region Public debug API.
#if DEBUG
        public override string ToString(bool verbose)
        {
            return verbose
                ? string.Format("Equipment: {0}, Duration: {1}, Exercise: {2}", equipment.id, Mathf.RoundToInt((float)initialDuration.TotalSeconds), exercise.id)
                : string.Format("Equipment: {0}", equipment.id);
        }
#endif
        #endregion
    }
}