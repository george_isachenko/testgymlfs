﻿using System;
using System.Collections.Generic;

namespace Logic.Persons.Tasks
{
    public abstract class Task
    {
        #region Types.
        public enum UpdateResult
        {
            Running = 0,
            Complete,
            Canceled
        }
        #endregion

        #region Private static data.
        private static readonly int childsCapacity = 8;
        #endregion

        #region Private data.
        private bool _active;
        private bool _aborted;
        #endregion

        #region Protected data.
        protected List<Task> children;
        #endregion

        #region Public properties.
        public Person person { get; private set; }

        public bool interruptable
        {
            get
            {
                return hasChildren ? children[0].interruptable : isInterruptable;
            }
        }

        public bool hasChildren
        {
            get
            {
                return (children != null && children.Count > 0);
            }
        }

        public bool active
        {
            get
            {
                return _active;
            }
        }
        #endregion

        #region Protected virtual properies.
        protected virtual bool isInterruptable
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Constructors.
        public Task()
        {
        }
        #endregion

        #region Public API.
        public void AssignPerson(Person person)
        {
            if (!ReferenceEquals(this.person, person))
            {
                this.person = person;

                if (hasChildren)
                {
                    foreach (var child in children)
                    {
                        child.AssignPerson(person);
                    }
                }
            }
        }

        public bool SetTask(Person person, Task newTask)
        {
            if (hasChildren)
            {
                children[0].Abort();
                children.Clear();
            }
            else
            {
                Deactivate();
            }

            if (newTask != null)
            {
                newTask.AssignPerson(person);

                if (newTask.Start())
                {
                    if (hasChildren)
                    {
                        children[children.Count - 1] = newTask;
                    }
                    else
                    {
                        if (children == null)
                            children = new List<Task>(childsCapacity);
                        children.Add(newTask);
                    }
                }
            }

            return hasChildren;
        }

        public void AddTask(Person person, Task newTask)
        {
            if (newTask != null)
            {
                newTask.AssignPerson(person);

                if (hasChildren)
                {
                    children.Add(newTask);
                }
                else
                {
                    Deactivate();
                    if (newTask.Start())
                    {
                        if (children == null)
                            children = new List<Task>(childsCapacity);

                        children.Add(newTask);
                    }
                }
            }
        }

        public T FindTask<T>(bool recursive) where T : Task
        {
            if (this is T)
                return this as T;

            if (recursive && children != null)
            {
                foreach (var child in children)
                {
                    var childTask = child.FindTask<T>(recursive);
                    if (childTask != null)
                        return childTask;
                }
            }

            return null;
        }

        public void PostLoad(float timePassed)
        {
            if (hasChildren)
            {
                foreach (var child in children)
                {
                    child.PostLoad(timePassed);
                }
            }

            OnPostLoad(timePassed);
        }

        public UpdateResult Update(float dt)
        {
            if (hasChildren)
            {
                UpdateResult result = children[0].Update(dt);
                if (result == UpdateResult.Complete)
                {
                    children.RemoveAt(0);

                    if (hasChildren)
                    {
                        return children[0].Start() ? UpdateResult.Running : UpdateResult.Canceled;
                    }
                    else
                    {
                        return Start() ? UpdateResult.Running : UpdateResult.Canceled;
                    }
                }
                else if (result == UpdateResult.Canceled)
                {
                    children.RemoveAt(0);

                    while (hasChildren && !children[0].OnPreviousSiblingCanceled())
                    {
                        children.RemoveAt(0);
                    }

                    if (hasChildren)
                    {
                        return children[0].Start() ? UpdateResult.Running : UpdateResult.Canceled;
                    }
                    else
                    {
                        if (!OnChildCanceled())
                        {
                            return UpdateResult.Canceled;
                        }
                        else
                        {
                            return Start() ? UpdateResult.Running : UpdateResult.Canceled;
                        }
                    }
                }
                return result;
            }
            else
            {
                return _aborted ? UpdateResult.Canceled : OnUpdate(dt);
            }
        }

        public bool Start()
        {
            if (hasChildren)
            {
                return children[0].Start();
            }
            else
            {
                if (OnStart())
                {
                    Activate();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Abort()
        {
            if (hasChildren)
            {
                children[0].Abort();
                children.Clear();
            }
            else
            {
                Deactivate();
                _aborted = true;
            }

            OnAborted();
        }
        #endregion

        #region Protected API.
        protected void Activate()
        {
            if (hasChildren)
            {
                children[0].Activate();
            }
            else
            {
                //if (!_active)
                {
                    _active = true;
                    _aborted = false;
                    OnActivated();
                }
            }
        }

        protected void Deactivate()
        {
            if (hasChildren)
            {
                children[0].Deactivate();
            }
            else
            {
                if (_active)
                {
                    _active = false;
                    OnDeactivated();
                }
            }
        }
        #endregion

        #region Protected virtual API.
        protected virtual bool OnStart()
        {
            return true;
        }

        protected virtual void OnPostLoad(float timePassed)
        {
        }

        protected virtual UpdateResult OnUpdate(float dt)
        {
            return UpdateResult.Running;
        }

        protected virtual void OnAborted()
        {
        }

        protected virtual void OnActivated()
        {
        }

        protected virtual void OnDeactivated()
        {
        }

        protected virtual bool OnPreviousSiblingCanceled()
        {
            return true;
        }

        protected virtual bool OnChildCanceled()
        {
            return true;
        }
        #endregion

        #region Public Debug API.
#if DEBUG
        public string GetDebugInfo (bool verbose)
        {
            if (hasChildren)
            {
                return string.Format("{0} > {1}", GetType().Name, children[0].GetDebugInfo(verbose));
            }
            else
            {
                return string.Format ("{0}: {1}", GetType().Name, ToString(verbose));
            }
        }

        public virtual string ToString(bool verbose)
        {
            return string.Empty;
        }
#endif
        #endregion
    }
}
