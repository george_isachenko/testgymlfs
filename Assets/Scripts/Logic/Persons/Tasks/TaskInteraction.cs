﻿using System;
using Data.Person;
using Data.Room;
using Logic.Persons.Events;
using UnityEngine.Assertions;
using Data;

namespace Logic.Persons.Tasks
{
    public class TaskInteraction : Task
    {
        #region Private data.
        int otherPersonId;  // We're using ID instead of Person reference because don't want to lock other person if it gets removed.
        TimeSpan emoteTime;
        AnimationSocial emote;
        bool walking = false;
        #endregion

        #region Constructor.
        public TaskInteraction(Person otherPerson, AnimationSocial emote, float emoteTime)
        : base ()
        {
            otherPersonId = otherPerson.id;
            this.emote = emote;
            this.emoteTime = TimeSpan.FromSeconds(emoteTime);

            // Debug.LogFormat("TaskInteraction: Constructor: {0} => {1}, time: {2}", person.id, otherPerson.id, this.emoteTime);
        }
        #endregion

        #region Protected virtual properties.
        protected override bool isInterruptable
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Public static API.
        public static bool IsInteractionPossible(Person person, Person otherPerson)
        {
            var selectedCharacterId = person.personManager.selectedCharacterId;
            var personState = person.state;
            var otherPersonState = otherPerson.state;

            // Interaction is potentially possible if all condition met:
            // 
            // 1. Both are in either Idle or InQueue states, both in a same state (Idle can interact with Idle, InQueue with InQueue)
            // 2. Both are in same room.
            // 3. Neither is a selected person.
            //
            // In addition: if persons are in a InQueue sate they must be already close enough to talk with each other
            // without much running and messing with a queue.
            // 
            // Even if all these condition met - interaction can be canceled either before running or after
            // if secondary conditions are failed (like Idle type changes) but they're
            // checked outside of this function.

            return ((personState != PersonState.Idle && personState != PersonState.InQueue) ||
                    person.id == selectedCharacterId ||
                    (otherPersonState != PersonState.Idle && otherPersonState != PersonState.InQueue) ||
                    otherPerson.id == selectedCharacterId ||
                    personState != otherPersonState ||
                    (otherPersonState == PersonState.Idle && !otherPerson.canSetTask) ||
                    person.roomName != otherPerson.roomName)
                ? false
                : person.canPerformFullIdle ? true : IsCloseEnough(person, otherPerson);
        }

        public static bool IsCloseEnough(Person person, Person otherPerson)
        {
            return RoomCoordinates.Distance(person.position, otherPerson.position) <=
                    DataTables.instance.balanceData.Characters.Movement.interactionCloseEnoughDistance;
        }
        #endregion

        #region Protected virtual interface.
        protected override bool OnStart()
        {
            //Debug.LogFormat("TaskInteraction.OnStart: {0} => {1}", person.id, otherPersonId);

            Person otherPerson;
            if (otherPersonId != PersonData.InvalidId && (otherPerson = person.personManager.GetPerson(otherPersonId)) != null)
            {
                if (!person.canPerformFullIdle)
                {
                    // When cannot perform full idle you do not walk to other character, interact only if there's character nearby.
                    if (TryBeginInteraction(otherPerson))
                    {
                        person.idleType = Person.IdleType.IdleInteraction;

                        return true;
                    }
                }
                else
                {
                    if (IsInteractionPossible(person, otherPerson) &&
                        person.idleType == Person.IdleType.Idle &&
                        otherPerson.idleType == Person.IdleType.Idle)
                    {
                        person.idleType = Person.IdleType.IdleInteraction;

                        walking = true;
                        MoveCharacterEvent.Send
                            ( person
                            , new CharacterDestination(otherPerson)
                            , DataTables.instance.balanceData.Characters.Movement.IdleInteractionMove
                            , OnMoveComplete);

                        return true;
                    }
                }
            }

            //Debug.LogFormat("TaskInteraction.OnStart: {0} => {1}: Start failed!", person.id, otherPersonId);
            otherPersonId = PersonData.InvalidId;
            person.idleType = Person.IdleType.Idle;

            return false;
        }

        protected override void OnDeactivated()
        {
            //Debug.LogFormat("TaskInteraction.OnDeactivated: {0} => {1}", person.id, otherPersonId);

            Cleanup(otherPersonId != PersonData.InvalidId ? person.personManager.GetPerson(otherPersonId) : null);
        }

        protected override void OnAborted()
        {
//             Debug.LogFormat("TaskInteraction: OnAborted: {0} => {1}", person.id, otherPersonId);

            Cleanup(otherPersonId != PersonData.InvalidId ? person.personManager.GetPerson(otherPersonId) : null);
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            //Debug.LogFormat("TaskInteraction.OnUpdate: {0} => {1}: walking: {2}", person.id, otherPersonId, walking);

            if (!walking)
            {
                Person otherPerson;
                if (otherPersonId != PersonData.InvalidId && (otherPerson = person.personManager.GetPerson(otherPersonId)) != null)
                {
                    if (!IsInteractionPossible(person, otherPerson) ||
                        person.idleType != Person.IdleType.IdleInteraction ||
                        otherPerson.idleType != Person.IdleType.IdleInteraction ||
                        !IsCloseEnough(person, otherPerson))
                    {
                        //Debug.LogFormat("TaskInteraction.OnUpdate: {0} => {1}: Canceled because of criteria violations!", person.id, otherPersonId);
                        Cleanup(otherPerson);
                        return UpdateResult.Canceled;
                    }

                    emoteTime -= TimeSpan.FromSeconds(dt);

                    if (emoteTime <= TimeSpan.Zero)
                    {
                        //Debug.LogFormat("TaskInteraction.OnUpdate: {0} => {1}: Completed!", person.id, otherPersonId);
                        Cleanup(otherPerson);
                        return UpdateResult.Complete;
                    }
                }
                else
                {
                    //Debug.LogFormat("TaskInteraction.OnUpdate: {0} => {1}: Canceled because of missing other person!", person.id, otherPersonId);
                    Cleanup(null);
                    return UpdateResult.Canceled;
                }
            }

            return UpdateResult.Running;
        }

        protected override bool OnChildCanceled()
        {
            return false; // TaskGoto failed, Cancel us too.
        }

#if DEBUG
        public override string ToString(bool verbose)
        {
            return verbose
                ? string.Format("Other: {0}, eTime: {1:N2}, walk: {2}", otherPersonId, emoteTime.TotalSeconds, walking)
                : string.Format("Other: {0}", otherPersonId);
        }
#endif
        #endregion

        #region Private methods.
        private bool TryBeginInteraction (Person otherPerson)
        {
            if (IsInteractionPossible(person, otherPerson) &&
                otherPerson.idleType == Person.IdleType.Idle &&
                IsCloseEnough(person, otherPerson))
            {
                otherPerson.idleType = Person.IdleType.IdleInteraction;

                CharacterLookAtEvent.Send(person, new CharacterDestination(otherPerson), null);
                CharacterLookAtEvent.Send(otherPerson, new CharacterDestination(person), null);

                PlayEmoteAnimationEvent.Send(person, emote, -1, false, null);
                PlayEmoteAnimationEvent.Send(otherPerson, emote, -1, false, null);

                return true;
            }

            return false;
        }

        void OnMoveComplete (int personId, MovementResult result)
        {
            //Debug.LogFormat("TaskInteraction.OnMoveComplete: {0} => {1}, result: {2}", person.id, otherPersonId, result);

            Assert.IsTrue(personId == person.id);
            Assert.IsTrue(walking);

            walking = false;

            if (result == MovementResult.Complete)
            {
                Person otherPerson;
                if (otherPersonId != PersonData.InvalidId && (otherPerson = person.personManager.GetPerson(otherPersonId)) != null)
                {
                    if (TryBeginInteraction(otherPerson))
                        return;
                    }
                }

            //Debug.LogFormat("TaskInteraction.OnMoveComplete: {0} => {1}: aborted!", person.id, otherPersonId);
            Abort();
        }

        void Cleanup (Person otherPerson)
        {
            //Debug.LogFormat("TaskInteraction.Cleanup: {0} => {1}", person.id, otherPersonId);

            if (otherPerson != null)
            {
                if ((otherPerson.state == PersonState.Idle || otherPerson.state == PersonState.InQueue) &&
                    otherPerson.idleType == Person.IdleType.IdleInteraction)
                {
                    StopCharacterEvent.Send(otherPerson);
                    otherPerson.idleType = Person.IdleType.Idle;
                }
            }

            if ((person.state == PersonState.Idle || person.state == PersonState.InQueue) &&
                person.idleType == Person.IdleType.IdleInteraction)
            {
                StopCharacterEvent.Send(person);
                person.idleType = Person.IdleType.Idle;
            }

            otherPersonId = PersonData.InvalidId;
        }
        #endregion
    }
}