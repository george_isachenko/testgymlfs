﻿namespace Logic.Persons.Tasks
{
    public class TaskKill : Task
    {
        public TaskKill()
        : base ()
        {
        }

        protected override bool isInterruptable
        {
            get
            {
                return false;
            }
        }

        protected override void OnActivated()
        {
            person.personManager.KillCharacterRequest(person.id);
        }

        protected override UpdateResult OnUpdate(float dt)
        {
            return UpdateResult.Running;    // Kill task never ends, disallowing other tasks to run!
        }
    }
}