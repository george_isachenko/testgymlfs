﻿using System;
using Data;
using Data.Person;
using Data.Room;
using Logic.Persons.Events;
using Logic.Persons.Tasks;

namespace Logic.Persons
{
    public class PersonPasserby : Person
    {
        #region Static/readonly data.
        public static readonly string passerbySpawnRoomZoneName = "passerby_spawn";
        public static readonly string passerbyDespawnRoomZoneName = "passerby_despawn";
        #endregion

        #region Properties.
        public override string spawnRoomZoneName
        {
            get
            {
                return passerbySpawnRoomZoneName;
            }
        }

        public override string despawnRoomZoneName
        {
            get
            {
                return passerbyDespawnRoomZoneName;
            }
        }
        
        public override string lobbyRoomZoneName
        {
            get
            {
                return string.Empty;
            }
        }

        public override string idleZoneName
        {
            get
            {
                return string.Empty;
            }
        }

/*
        public override string queueZoneNamePrefix
        {
            get
            {
                return null;
            }
        }
*/
        #endregion

        #region Public virtual properties.
        public override bool isSelectable
        {
            get
            {
                return false;
            }
        }

        public override bool isPlayerControllable
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Constructor.
        public PersonPasserby(IPersonManager personManager, PersonData personData)
            : base(personManager, personData)
        {
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (!strict && personData.GetType() == typeof(PersonData)) // Exact match needed.
            {
                return new PersonPasserby(personManager, personData);
            }

            return null;
        }
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetTask(new TaskIdle());

            EnterRoom(entranceMode);
        }

        public override void OnLoaded()
        {
            SetTask(new TaskIdle());
        }

        public override void EnterRoom (EntranceMode entranceMode)
        {
            ChangeState(PersonState.Entering);

            Action<Task> activatorAction = (Task task) =>
            {
                personManager.KillCharacterRequest(id);
            };

            var activatorTask = new TaskDelegate
                ( new TaskDelegate.Settings
                {
                    isInterruptable = (task) => true,
                    // onAborted = activatorAction
                    onActivated = activatorAction,
                });

            activatorTask.AddTask
                ( this
                , new TaskGoto
                    ( new CharacterDestination(passerbyDespawnRoomZoneName)
                    , DataTables.instance.balanceData.Characters.Movement.PasserbyWalking
                    , (entranceMode == EntranceMode.ImmediateEntranceFast) ? MovementMode.Running : MovementMode.WalkingFast
                    , true)
                );

            SetTask(activatorTask);
        }
        #endregion
    }
}