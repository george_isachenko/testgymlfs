﻿using System;
using System.Collections;
using System.Linq;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.Quests;
using Data.Quests.Juicer;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Tasks;
using Logic.Quests.Juicer;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons
{
    public class PersonJuicer : Person, IPersonJuicerInfo
    {
        #region Static/readonly data.
        private static readonly string juicerSpawnRoomZoneName          = "juicer_spawn";
        private static readonly string juicerDespawnRoomZoneName        = "juicer_despawn";
        private static readonly string juicerLobbyRoomZoneName          = "juicer_lobby";
        // private static readonly string juicerQueueZoneNamePrefix     = "juicer_queue_";
        private static readonly string juicerIdleZoneName               = CharacterDestination.wholeRoom;
        #endregion

        #region Overriden properties.
        protected new PersonJuicerData data { get { return (PersonJuicerData)base.data; } }
        #endregion

        #region Public virtual properties.
        public override bool canPerformIdleRoutines
        {
            get
            {
                return false;
            }
        }

        public override string  spawnRoomZoneName   { get { return juicerSpawnRoomZoneName; } }
        public override string  despawnRoomZoneName { get { return juicerDespawnRoomZoneName; } }
        public override string  lobbyRoomZoneName   { get { return juicerLobbyRoomZoneName; } }
        public override string  idleZoneName        { get { return juicerIdleZoneName; } }
        #endregion

        #region Public properties.
        public JuiceQuestData   juiceQuestData      { get { return data.juiceQuestData; } }
        public JuiceQuestLogic      juiceQuest      { get; protected set; }
        #endregion

        #region Constructor.
        public PersonJuicer(IPersonManager personManager, PersonJuicerData personJuicerData)
            : base(personManager, personJuicerData)
        {
            juiceQuest = null;
        }

        public PersonJuicer(IPersonManager personManager, PersonJuicerData personJuicerData, Person convertFrom)
            : base(personManager, personJuicerData, convertFrom)
        {
            juiceQuest = null;
        }

        #endregion

        #region Public API.
        public bool Drink (Exercise exercise, float drinkSeconds)
        {
            Assert.IsNotNull(exercise);
            Assert.IsTrue(state == PersonState.Workout);

            var drinkTime = TimeSpan.FromSeconds(drinkSeconds);
            
            Assert.IsTrue(drinkTime > TimeSpan.Zero && drinkTime < TimeSpan.MaxValue);

            if (state == PersonState.Workout)
            {
                var task = FindTask<TaskExercise>();
                if (task != null)
                {
                    task.SwitchExercise(exercise, drinkTime);

                    return true;
                }
            }

            return false;
        }

        public void RemoveQuest()
        {
            juiceQuest = null;
            data.juiceQuestData = null;
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (personData.GetType() == typeof(PersonJuicerData)) // Exact match needed.
            {
                var personJuicerData = personData as PersonJuicerData;

                return new PersonJuicer(personManager, personJuicerData);
            }

            return null;
        }
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetTask(new TaskIdle());

            base.OnPostCreate(entranceMode);
        }

        public override void OnLoaded()
        {
            if (state != PersonState.InQueue)
            {
                SetupJuiceQuest();
            }

            SetTask(new TaskIdle());
        }

        public override void EnterRoom (EntranceMode entranceMode)
        {
            base.EnterRoom (entranceMode);

            SetupJuiceQuest();
        }

        public override bool SelectRequest()
        {
            if (state == PersonState.Entering)
            {
                var taskGoto = FindTask<TaskGoto>();
                if (taskGoto != null)
                {
                    taskGoto.ChangeMovementMode(MovementMode.Running, DataTables.instance.balanceData.Characters.Movement.temporarySpeedUpDuration);
                }
            }

            return (state == PersonState.Workout || state == PersonState.ExerciseComplete);
        }

        public override void OnSelected()
        {
            base.OnSelected();

            if (juiceQuest != null)
            {
                juiceQuest.WasSeen();
            }
        }

        public override void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            base.OnPostLoad(timePassedSinceSave, loadTimeUTC);

            if (state == PersonState.Idle)
            {
                TryToSitOnStool();
            }
            else if (state == PersonState.ExerciseComplete)
            {
                Quit();
            }
        }

        public override bool AboutToEndExercise(ExerciseFlags flags)
        {
            Assert.IsTrue(data.state == PersonState.Workout);

            if (data.state == PersonState.Workout)
            {
                Assert.IsNotNull(data.workoutExercise);

                if (juiceQuest != null && data.workoutExercise.id == juiceQuest.exerciseId)
                {
                    return ! SwitchToDrinkCompleteExercise();
                }
            }

            return true;
        }

        public override void Quit()
        {
            personManager.gameCore.DelayedInvoke(() =>
            {
                LeaveRoom(); 
            });
        }

        public override bool KickRequest()
        {
            return false;
        }

        public override bool CanUseEquipment(Equipment equipment, int tier)
        {
            return (equipment.meetsLevelRequirement);
        }

        public override bool CanUseEquipment(Equipment equipment)
        {
            return (equipment.meetsLevelRequirement);
        }

        public override QuestStage? GetQuestStage()
        {
            return (juiceQuest != null)
                ? new QuestStage?(juiceQuest.questStage)
                : null;
        }

        public override bool ClaimQuestRequest()
        {
            if (juiceQuest != null && juiceQuest.completed)
            {
                if (juiceQuest.Claim())
                {
                    var task = FindTask<TaskExercise>();
                    if (task != null)
                    {
                        SkipExercise();
                    }

                    personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    return true;
                }
            }
            return false;
        }

        public override void OnStateChanged(PersonState oldState, PersonState newState)
        {
            base.OnStateChanged(oldState, newState);

            if (newState == PersonState.Idle)
            {
                TryToSitOnStool();
            }
            else if (newState == PersonState.ExerciseComplete)
            {
                Quit();
            }
            else
            {
                personManager.DeselectCharacterIfSelectedRequest(id);
            }

            personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }
        #endregion

        #region Protected virtual API.
        protected override void PerformSkipExercise ()
        {
            if (juiceQuest != null && data.workoutExercise.id == juiceQuest.exerciseId)
            {
                if (SwitchToDrinkCompleteExercise())
                {
                    return;
                }
            }
            
            base.PerformSkipExercise();
        }
        #endregion

        #region Private methods.
        private void TryToSitOnStool ()
        {
            personManager.gameCore.DelayedInvoke(() =>
            {
                var equipment = personManager.gameCore.equipmentManager.equipment.Values.Where
                    ( x =>  x.roomName == roomName &&
                            x.isUsable &&
                            x.meetsLevelRequirement).FirstOrDefault();
                
                if (equipment != null)
                {
                    personManager.UseEquipmentRequest(this, equipment, equipment.estateType.exercises[0], ExerciseInitiator.Auto);
                }
                else
                {
                    Debug.LogWarning("Logic warning: Juicer cannot find usable and free equipment upon reaching room. Will quit now!");

                    Quit();
                }
            });
        }

        private void SetupJuiceQuest()
        {
            if (juiceQuest == null && juiceQuestData != null && personManager.gameCore.quests != null)
            {
                juiceQuest = personManager.gameCore.quests.juiceQuests.GetQuest(juiceQuestData.juicerId);

                if (juiceQuest == null)
                {
                    juiceQuest = personManager.gameCore.quests.juiceQuests.NewQuest(juiceQuestData);
                }
            }
        }

        private bool SwitchToDrinkCompleteExercise ()
        {
            Assert.IsTrue(state == PersonState.Workout);

            Exercise drinkCompleteExercise;
            if (juiceQuest != null &&
                DataTables.instance.exercises.TryGetValue(juiceQuest.exerciseCompleteId, out drinkCompleteExercise))
            {
                var task = FindTask<TaskExercise>();
                if (task != null)
                {
                    return task.SwitchExercise(drinkCompleteExercise, null);
                }
            }

            Debug.LogErrorFormat("Failed to switch to 'Drink complete exercise'. Not running TaskExercise task or juiceQuest is null, or required exercise in exercises table. Person id: {0}, Complete exercise id: {1}."
                , id, juiceQuest != null ? juiceQuest.exerciseCompleteId : -1);
            return false;
        }
        #endregion
    }
}
