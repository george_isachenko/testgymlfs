﻿using Data.Person;
using Data.Room;
using Logic.Estate;
using System;
using Data;
using Core.EventSystem;
using Data.Person.Info;

namespace Logic.Persons.Events
{
    public class CharacterBirthEvent : StaticEvent<CharacterBirthEvent.Delegate, Person, bool, CharacterDestination>
    {
        public delegate void Delegate(Person person, bool restored, CharacterDestination spawnDestination);
    }

    public class CharacterDeathEvent : StaticEvent<CharacterDeathEvent.Delegate, int>
    {
        public delegate void Delegate(int id);
    }

    public class CharacterEnteredRoomEvent : StaticEvent<CharacterEnteredRoomEvent.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class CharacterLeavingRoomEvent : StaticEvent<CharacterLeavingRoomEvent.Delegate, Person, bool>
    {
        public delegate void Delegate(Person person, bool kicked);
    }

    public class CharacterSentencedEvent : StaticEvent<CharacterSentencedEvent.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class CharacterTypeChangedEvent : StaticEvent<CharacterTypeChangedEvent.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class CharacterTransferEvent : StaticEvent<CharacterTransferEvent.Delegate, Person, string>
    {
        public delegate void Delegate(Person person, string roomName);
    }

    public class MoveCharacterEvent : StaticEvent<MoveCharacterEvent.Delegate, Person, CharacterDestination, IMovementSettingsInfo, Action<int, MovementResult>>
    {
        public delegate void Delegate(Person person, CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo, Action<int, MovementResult> onComplete);
    }

    public class ChangeCharacterMovementModeEvent : StaticEvent<ChangeCharacterMovementModeEvent.Delegate, Person, MovementMode>
    {
        public delegate void Delegate(Person person, MovementMode mode);
    }

    public class SetSelectionEvent : StaticEvent<SetSelectionEvent.Delegate, Person, bool, bool>
    {
        public delegate void Delegate(Person person, bool selected, bool focus);
    }

    public class StopCharacterEvent : StaticEvent<StopCharacterEvent.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class CharacterSchedulePendingExerciseEvent : StaticEvent<CharacterSchedulePendingExerciseEvent.Delegate, Person, Equipment, Exercise, ExerciseInitiator, TimeSpan>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration);
    }

    public class CharacterSwitchPendingExerciseEvent : StaticEvent<CharacterSwitchPendingExerciseEvent.Delegate, Person, Exercise, TimeSpan>
    {
        public delegate void Delegate(Person person, Exercise exercise, TimeSpan duration);
    }

    public class CharacterCancelPendingExerciseEvent : StaticEvent<CharacterCancelPendingExerciseEvent.Delegate, Person, Equipment>
    {
        public delegate void Delegate(Person person, Equipment equipment);
    }

    public class CharacterStartExerciseEvent : StaticEvent<CharacterStartExerciseEvent.Delegate, Person, Equipment, Exercise, ExerciseInitiator, TimeSpan, ExerciseFlags, float>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed);
    }

    public class CharacterSwitchExerciseEvent : StaticEvent<CharacterSwitchExerciseEvent.Delegate, Person, Exercise, float>
    {
        public delegate void Delegate(Person person, Exercise exercise, float speed);
    }

    public class CharacterFinishingExerciseEvent : StaticEvent<CharacterFinishingExerciseEvent.Delegate, Person, Equipment, Exercise, ExerciseInitiator, ExerciseFlags>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags);
    }

    public class CharacterEndedExerciseEvent : StaticEvent<CharacterEndedExerciseEvent.Delegate, Person, Equipment, Exercise, ExerciseInitiator, ExerciseFlags>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags);
    }

    public class PlayEmoteAnimationEvent : StaticEvent<PlayEmoteAnimationEvent.Delegate, Person, AnimationSocial, int, bool, Action>
    {
        public delegate void Delegate(Person person, AnimationSocial animationId, int loopCount, bool rotateTowardsCamera, Action onComplete);
    }

    public class CharacterLookAtEvent : StaticEvent<CharacterLookAtEvent.Delegate, Person, CharacterDestination, Action>
    {
        public delegate void Delegate(Person person, CharacterDestination lookAtDestination, Action onComplete);
    }

    public class CharacterAppearanceChangedEvent : StaticEvent<CharacterAppearanceChangedEvent.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class VisitorQuestClaimedRequest : StaticEvent<VisitorQuestClaimedRequest.Delegate, Person>
    {
        public delegate void Delegate(Person person);
    }

    public class CharacterRefusedExerciseEvent : StaticEvent<CharacterRefusedExerciseEvent.Delegate, Person, Equipment, Exercise>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise);
    }

    public class SportsmanTypeChangedEvent : StaticEvent<SportsmanTypeChangedEvent.Delegate, PersonSportsman, SportsmanType>
    {
        public delegate void Delegate(PersonSportsman sportsman, SportsmanType newSportsmanType);
    }

    public class CharacterReadyToSkipExerciseEvent : StaticEvent<CharacterReadyToSkipExerciseEvent.Delegate, Person, Equipment, Exercise>
    {
        public delegate void Delegate(Person person, Equipment equipment, Exercise exercise);
    }

    public class CharacterShowReadyToSkipOverhead : StaticEvent<CharacterShowReadyToSkipOverhead.Delegate, IPersonInfo>
    {
        public delegate void Delegate(IPersonInfo person);
    }

    public class CharacterLevelUpEvent : StaticEvent<CharacterLevelUpEvent.Delegate, Person, int>
    {
        public delegate void Delegate(Person person, int newLevel);
    }

    public class CharacterStateChangedEvent : StaticEvent<CharacterStateChangedEvent.Delegate, Person, PersonState, PersonState>
    {
        public delegate void Delegate(Person person, PersonState oldState, PersonState newState);
    }
}
