﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logic.Estate;
using Data.Estate;
using Data.Person;
using Data;
using UnityEngine.Assertions;

namespace Logic.Persons
{
    public class OfflineTrainer
    {
        #region Types.
        private class OfflineEquipment
        {
            public readonly Equipment equipment;
            public DateTime availableSince;

            public OfflineEquipment(Equipment equipment, DateTime availableSince)
            {
                this.equipment = equipment;
                this.availableSince = availableSince;
            }
        }

        private class OfflineTrainerClient
        {
            public readonly PersonTrainerClient person;
            public DateTime availableSince;

            public OfflineTrainerClient(PersonTrainerClient person, DateTime availableSince)
            {
                this.person = person;
                this.availableSince = availableSince;
            }
        }
        #endregion

        #region Private data.
//      private readonly string                             roomName;
        private readonly DateTime                           loadTimeUTC;
//      private readonly TimeSpan                           timePassedSinceSave;
        private readonly OfflineTrainerClient[]             clients;
        private readonly OfflineEquipment[]                 equipment;
        private DateTime                                    currentTimePosition;
        #endregion

        #region Constructors.
        public OfflineTrainer
            ( string roomName
            , IDictionary<int, Equipment> allEquipment
            , IDictionary<int, Person> allPersons
            , DateTime loadTimeUTC
            , TimeSpan timePassedSinceSave)
        {
//          this.roomName = roomName;
            this.loadTimeUTC = loadTimeUTC;
//          this.timePassedSinceSave = timePassedSinceSave;

            currentTimePosition = loadTimeUTC - timePassedSinceSave;

            equipment = allEquipment.Values
                .Where( e => 
                        e.isPotentiallyAvailableForUse &&
                        e.roomName == roomName)
                .Select(e => new OfflineEquipment(e, GetInitialEquipmentAvailability(allPersons, e, loadTimeUTC, timePassedSinceSave)))
                .Where(oe => oe.availableSince < loadTimeUTC)
                .ToArray();

            clients = allPersons.Values
                .Where( p =>
                        p.isNotLeavingOrDead &&
                        p is PersonTrainerClient &&
                        p.roomName == roomName)
                .Cast<PersonTrainerClient>()
                .Where( p =>
                        p.trainer != null &&
                        p.visitorQuest != null &&
                        !p.visitorQuest.completed &&
                        !p.visitorQuest.claimed)
                .Select(p => new OfflineTrainerClient(p, GetInitialPersonAvailability(p, loadTimeUTC, timePassedSinceSave)))
                .Where(op => op.availableSince < loadTimeUTC)
                .ToArray();
        }

        public void Simulate()
        {
            bool anyScheduled = false;
            IEnumerable<IGrouping<DateTime, OfflineTrainerClient>> groupedAvailableClients = null;

            do
            {
                anyScheduled = false;
                groupedAvailableClients = OrderAndGroupAvailableClients();
                if (groupedAvailableClients != null)
                {
                    foreach (var clientsGroup in groupedAvailableClients)
                    {
                        foreach (var offlineClient in clientsGroup)
                        {
                            var groupedAvailableEquipment = OrderAndGroupAvailableEquipment();
                            if (groupedAvailableEquipment != null)
                            {
                                foreach (var equipmentGroup in groupedAvailableEquipment)
                                {
                                    Exercise exercise;
                                    var equipment = offlineClient.person.SelectTrainingEquipmentCandidateAndExercise
                                        ( equipmentGroup.Select(eg => eg.equipment)
                                        , out exercise);

                                    if (equipment != null && exercise != null)
                                    {
                                        var offlineEquipment = equipmentGroup.First(e => ReferenceEquals(e.equipment, equipment));
                                        Assert.IsNotNull(offlineEquipment);

                                        var delay = CalculateStartupDelay(exercise.duration);
                                        var startDate = Max(offlineEquipment.availableSince, offlineClient.availableSince);
                                        var realStartDate = startDate + delay;

                                        bool refusedToEnd = false;

                                        if (realStartDate < loadTimeUTC && // Never start past load time!
                                            offlineClient.person.DoOfflineExercise
                                                ( out refusedToEnd
                                                , equipment
                                                , exercise
                                                , realStartDate
                                                , loadTimeUTC
                                                , ExerciseInitiator.Trainer))
                                        {
                                            // Person can refuse to end exercise if it's switched to another exercise while ending. 
                                            // We do not handle this situations well yet and are not expecting it for trainers and trainer clients, really.
                                            // But in case this happens - mark both client and equipment as busy till loading, sorry.

                                            var endDate = refusedToEnd
                                                ? loadTimeUTC
                                                : realStartDate + exercise.duration;

                                            offlineClient.availableSince = endDate;
                                            offlineEquipment.availableSince = endDate;

                                            anyScheduled = true;

                                            break; // All regrouping must be repeated again!
                                        }
                                    }
                                }
                            }

                            // Do not break here if any scheduled, because we can submit many clients that are available at the same time without regrouping.
                        }

                        currentTimePosition = clientsGroup.Key; // Any clients who were available before currentTimePosition but failed to find free equipment at first try will be ignored on next passes.

                        if (anyScheduled)
                            break; // All regrouping must be repeated again!
                    }
                }
            } while (anyScheduled);
        }
        #endregion

        #region Private static methods.
        public static T Max<T> (T x, T y) where T : struct, IComparable<T>
        {
            // See https://stackoverflow.com/a/1906572 .
            return (Comparer<T>.Default.Compare(x, y) > 0) ? x : y;
        }

        static DateTime GetInitialEquipmentAvailability
            (IDictionary<int, Person> allPersons, Equipment equipment, DateTime loadTimeUTC, TimeSpan timePassedSinceSave)
        {
            if (equipment.state == EquipmentState.Working)
            {
                Person workingPerson;
                if (equipment.isOccupied &&
                    allPersons.TryGetValue(equipment.occupantCharacterId, out workingPerson) &&
                    workingPerson.state == PersonState.Workout)
                {
                    return workingPerson.exerciseEndTime;
                }
            }
            else if (equipment.state == EquipmentState.WorkPending)
            {
                var workingPerson = allPersons.Values
                    .Where(p => p.state == PersonState.WorkoutPending &&
                                p.roomName == equipment.roomName &&
                                p.workoutEquipmentId == equipment.id)
                    .FirstOrDefault();

                if (workingPerson != null)
                {
                    if (workingPerson.exerciseEndTime == DateTime.MinValue)
                    {   // Compatibility with old saves, when "WorkoutPending" gets canceled after loading game.
                        return loadTimeUTC - timePassedSinceSave;
                    }
                    else
                    {
                        return workingPerson.exerciseEndTime;
                    }
                }
            }
            else if (equipment.isUsable)
            {
                return loadTimeUTC - timePassedSinceSave;
            }

            return loadTimeUTC; // Fallback default value. Means that equipment cannot be used really.
        }

        static DateTime GetInitialPersonAvailability
            (Person person, DateTime loadTimeUTC, TimeSpan timePassedSinceSave)
        {
            if (person.state == PersonState.Workout)
            {
                return person.exerciseEndTime;
            }
            else if (person.state == PersonState.WorkoutPending)
            {
                if (person.exerciseEndTime == DateTime.MinValue)
                {   // Compatibility with old saves, when "WorkoutPending" gets canceled after loading game.
                    return loadTimeUTC - timePassedSinceSave;
                }
                else
                {
                    return person.exerciseEndTime;
                }
            }

            return loadTimeUTC; // Fallback default value. Means that person cannot be used really.
        }

        static TimeSpan CalculateStartupDelay
            (TimeSpan duration)
        {
            // Simple "Multiply then Add" operation, just both multiply and add operands are defined as random ranges.
            TimeSpan delay = TimeSpan.Zero;

            var multiplierMin = DataTables.instance.balanceData.Characters.Trainers.offlineExerciseMultiplyTimeMin;
            var multiplierMax = DataTables.instance.balanceData.Characters.Trainers.offlineExerciseMultiplyTimeMax;
            if (duration > TimeSpan.Zero && multiplierMin > 0 && multiplierMax >= multiplierMin)
            {
                delay = TimeSpan.FromSeconds(duration.TotalSeconds * UnityEngine.Random.Range(multiplierMin, multiplierMax)) - duration;
            }

            var addMin = DataTables.instance.balanceData.Characters.Trainers.offlineExerciseAddTimeMin;
            var addMax = DataTables.instance.balanceData.Characters.Trainers.offlineExerciseAddTimeMax;

            if (addMin > TimeSpan.Zero && addMax >= addMin)
            {
                return delay + TimeSpan.FromSeconds(UnityEngine.Random.Range((float)addMin.TotalSeconds, (float)addMax.TotalSeconds));
            }
            else
            {
                return delay;
            }
        }
        #endregion

        #region Private methods.
        private bool CanTrainOffline(OfflineTrainerClient client)
        {
            return (client.availableSince < loadTimeUTC &&
                    client.person.visitorQuest != null &&
                    !client.person.visitorQuest.completed &&
                    !client.person.visitorQuest.claimed);
        }

        private IEnumerable<IGrouping<DateTime, OfflineTrainerClient>> OrderAndGroupAvailableClients()
        {
            return clients
                .Where(p => p.availableSince >= currentTimePosition && CanTrainOffline(p))
                .OrderBy(p => p.availableSince)
                .GroupBy(p => p.availableSince, p => p);
        }

        private bool CanBeUsedOffline(OfflineEquipment equipment)
        {
            return (equipment.availableSince < loadTimeUTC &&
                    equipment.equipment.isPotentiallyAvailableForUse);
        }

        private IEnumerable<IGrouping<DateTime, OfflineEquipment>> OrderAndGroupAvailableEquipment()
        {
            return equipment
                .Where(e => CanBeUsedOffline(e))
                .OrderBy(e => e.availableSince)
                .GroupBy(e => e.availableSince, e => e);
        }
        #endregion
    }
}