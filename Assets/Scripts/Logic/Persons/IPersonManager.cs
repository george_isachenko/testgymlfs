﻿using System;
using System.Collections.Generic;
using Data;
using Data.Person;
using Data.Quests;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Core;
using Logic.Estate;
using Logic.Facades;
using UnityEngine;

namespace Logic.Persons
{
    public interface IPersonManager
    {
        IGameCore gameCore
        {
            get;
        }

        LogicSportsmen sportsmen   { get; }

        IDictionary<int, Person> persons
        {
            get;
        }

        int sportsmenCapacityLevel { get; set; }

        PersonVisitor CreateVisitor(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null);
        PersonPasserby CreatePasserby(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null);
        PersonSportsman CreateSportsman(string roomName, Person.EntranceMode entranceMode, SportsmanType sportsmanType, PersonAppearance.Gender gender, string name = null, string spawnZoneName = null);
        PersonSportsman CreateSportsman(string roomName, Person.EntranceMode entranceMode, SportsmanType sportsmanType, PersonAppearance appearance, string name = null, string spawnZoneName = null);
        PersonJuicer CreateJuicer(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null);
        PersonTrainer CreateTrainer(string roomName, Person.EntranceMode entranceMode, string name, string spawnZoneName = null);

        PersonSportsman BecameSportsman(PersonVisitor visitor, SportsmanType sportsmanType, string newRoomName, Person.EntranceMode entranceMode);
        PersonJuicer BecameJuicer(PersonVisitor visitor, string newRoomName, Person.EntranceMode entranceMode);
        PersonTrainerClient BecameTrainerClient(PersonVisitor visitor, string newRoomName);

        bool UseEquipmentRequest(Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator);

        // Common API with ILogicCharacterManager. 
        bool hasSelection
        {
            get;
        }

        int selectedCharacterId
        {
            get;
        }

        string GenerateRandomName(PersonAppearance.Gender gender, bool unique = true);
        void SetCharactersSelectionMode(bool allowSelection);

        Person GetPerson(int id);
        int GetCharactersCount(Type personType = null, string roomName = null, params PersonState[] states);
        void DeselectCharacterIfSelectedRequest(int id);
        bool SelectCharacterRequest(int id);
        bool MoveCharacterRequest(int id, RoomCoordinates coords);
        QuestStage? GetQuestStage(int id);
        VisitorQuestObjectiveData GetVisitorQuestCurrentObjectiveData(int id);
        bool KickRequest(int id);
        void KillCharacterRequest(int id);
        void SkipExerciseRequest(int id);

        bool ClaimQuestRequest(int id);
        bool ClaimQuestRequest(Person person);
        void OnTrainerXpUp(int id);

        IEnumerable<PersonSportsman> GetSportsmen();
        int GetSportsmanCount(SportsmanType? sportsmanType, string roomName = null, params PersonState[] states);
        PersonSportsman GetSportsman(SportsmanType? sportsmanType, string roomName = null, params PersonState[] states);
        int RemoveSportsmen(SportsmanType sportsmanType, int count);
        void UpdateSportsmanAppearance(ref PersonAppearance appearance, SportsmanType sportsmanType);

        PersonAppearance GetTrainerAppearance(int trainerIdx);
    }
}