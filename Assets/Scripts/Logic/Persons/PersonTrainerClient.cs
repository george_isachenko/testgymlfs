using System;
using Data.Person;
using Data.Person.Info;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Tasks;
using Data;
using Data.Quests.Visitor;
using Data.Estate;
using UnityEngine;
using System.Linq;
using UnityEngine.Assertions;
using System.Collections.Generic;

namespace Logic.Persons
{
    public class PersonTrainerClient : PersonVisitor, IPersonTrainerClientInfo
    {
        #region Static/readonly data.
        #endregion

        #region Overriden properties.
        protected new PersonTrainerClientData data { get { return (PersonTrainerClientData)base.data; } }
        #endregion

        #region Public virtual properties.
        public override bool isPlayerControllable { get { return false; } }
        public override bool canBeTrainedByPlayer { get { return false; } }
        public override bool canPerformFullIdle { get { return false; } }
        public override bool canPerformIdleRoutines { 
            get 
            {
                return (state == PersonState.Idle || state == PersonState.InQueue) &&
                        (visitorQuest == null || (visitorQuest.completed && !visitorQuest.claimed));
            }
        }
        #endregion

        #region Public properties.
        public PersonTrainer trainer
        {
            get
            {
                if (_cachedTrainer == null)
                {
                    _cachedTrainer = personManager.persons.Values
                            .Where(p => p is PersonTrainer && (p as PersonTrainer).IsTrainingClient(this))
                            .Cast<PersonTrainer>()
                            .FirstOrDefault();
                }
                return _cachedTrainer;
            }
        }
        #endregion

        #region Private data.
        private PersonTrainer _cachedTrainer;
        #endregion

        #region Constructors.
        public PersonTrainerClient(IPersonManager personManager, PersonTrainerClientData personData)
            : base(personManager, personData)
        {
        }

        public PersonTrainerClient(IPersonManager personManager, PersonTrainerClientData personTrainerClientData, PersonVisitor convertFrom)
            : base(personManager, personTrainerClientData, convertFrom)
        {
        }
        #endregion

        #region Public static API.
        public static new Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (personData.GetType() == typeof(PersonTrainerClientData)) // Exact match needed.
            {
                var personTrainerClientData = personData as PersonTrainerClientData;

                return new PersonTrainerClient(personManager, personTrainerClientData);
            }

            return null;
        }
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetTask(new TaskIdle());

            base.OnPostCreate(entranceMode);
        }

        public override bool KickRequest()
        {
            return false;
        }

        public override bool ClaimQuestRequest()
        {
            var trainer = this.trainer; // Cache it, accessor is costly!

            Assert.IsNotNull(trainer);
            Assert.IsNotNull(visitorQuest);

            if (trainer != null)
            {
                if (visitorQuest != null && visitorQuest.completed)
                {
                    visitorQuest.coinsBonus = ((float)trainer.coinsBonus) * 0.01f;

                    var exp = visitorQuest.trainerExp; // VISITOR QUEST WILL BE GONE AFTER base.ClaimQuestRequest!
                    if (base.ClaimQuestRequest())
                    {
                        trainer.AddExp(exp);
                        trainer.UnassignClient(this);
                        return true;
                    }
                }
            }
            else
            {
                Debug.LogError("Trainer is unassigned while trying to claim quest!");
            }

            return false;
        }
        #endregion

        #region Public API.
        public Equipment SelectTrainingEquipmentCandidateAndExercise
            (IEnumerable<Equipment> equipment, out Exercise exercise)
        {
            if (visitorQuest != null && !visitorQuest.completed && !visitorQuest.claimed)
            {
                foreach (var objective in visitorQuest.objectives)
                {
                    if (!objective.completed)
                    {
                        if (objective.type == VisitorQuestType.Exercise)
                        {
                            var candidate = equipment
                                            .Where(e => e.roomName == roomName &&
                                                        e.state == EquipmentState.Idle &&
                                                        e.estateType.trainerLevelRequirement <= trainer.level &&
                                                        objective.CanUseEquipment(e.estateType))
                                            .OrderByDescending(e => e.upgradeLevel)
                                            .FirstOrDefault();

                            if (candidate != null)
                            {
                                exercise = objective.exercise;
                                return candidate;
                            }
                        }
                        else if (objective.type == VisitorQuestType.Stats)
                        {
                            var candidate = equipment
                                            .Where(e => e.roomName == roomName &&
                                                        e.state == EquipmentState.Idle &&
                                                        e.estateType.trainerLevelRequirement <= trainer.level &&
                                                        objective.CanUseEquipment(e.estateType))
                                            .OrderByDescending(e => e.upgradeLevel)
                                            .ThenByDescending(e => e.estateType.stats.Get(objective.exerciseType))
                                            .FirstOrDefault();

                            if (candidate != null)
                            {
                                exercise = candidate.estateType.exercises
                                                .OrderByDescending(e => e.stats.Get(objective.exerciseType))
                                                .First();
                                return candidate;
                            }
                        }
                    }
                }
            }

            exercise = null;
            return null;
        }

        public override bool CanUseEquipment(Equipment equipment, int tier)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif
            return equipment.meetsLevelRequirement &&
                    equipment.estateType.trainerLevelRequirement <= trainer.level &&
                    visitorQuest != null &&
                    !visitorQuest.completed &&
                    visitorQuest.CanUseEquipment(equipment.estateType, tier);
        }

        public override bool CanUseEquipment(Equipment equipment)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif

            return equipment.meetsLevelRequirement &&
                    equipment.estateType.trainerLevelRequirement <= trainer.level &&
                    visitorQuest != null &&
                    !visitorQuest.completed &&
                    visitorQuest.CanUseEquipment(equipment.estateType);
        }
        #endregion
    }
}