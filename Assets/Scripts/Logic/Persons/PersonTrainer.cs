using System;
using System.Linq;
using Data;
using Data.Person;
using Data.Person.Info;
using Data.PlayerProfile;
using Data.Room;
using Logic.Estate;
using Logic.Persons.Events;
using Logic.Persons.Tasks;
using Logic.Trainers;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.Estate.Events;
using View.UI;
using Data.Estate;

namespace Logic.Persons
{
    public class PersonTrainer : Person, IPersonTrainerInfo
    {
        #region Types.
        public class TrainerUnlockInfo
        {
            public string text;
            public Sprite sprite;
        }
        #endregion

        #region Static/readonly data.
        private static readonly string trainerSpawnRoomZoneName = "trainer_spawn";
        private static readonly string trainerDespawnRoomZoneName = "trainer_despawn";
        private static readonly string trainerLobbyRoomZoneName = "trainer_lobby";
        private static readonly string trainerWorkZoneName = CharacterDestination.wholeRoom;

        private static ExperienceLevels expLevels;
        #endregion

        #region Overriden properties.
        protected new PersonTrainerData data { get { return (PersonTrainerData)base.data; } }
        #endregion

        #region Public virtual properties.
        public override bool isPlayerControllable   { get { return canSetTask; } }
        public override bool canBeTrainedByPlayer   { get { return false; } }

        public override bool canSetTask
        {
            get
            {
                return (data.state == PersonState.Idle || data.state == PersonState.ExerciseComplete || data.state == PersonState.Refused) &&
                    (task == null || task.interruptable);
            }
        }

        public override bool canPerformIdleRoutines
        {
            get
            {
                return (state == PersonState.Idle || state == PersonState.InQueue) && currentTrainingClientIdx < 0;
            }
        }

        public override bool useExtendedIdleAnimation
        {
            get
            {
                return (state == PersonState.InQueue || state == PersonState.Entering ? false : canSetTask);
            }
        }

        public override int stoppedAvoidanceDelta
        {
            get
            {
                // When training client - boost our avoidance priority to disallow moving of us by another clients.
                return (currentTrainingClient != null && currentTrainingClient.state == TrainerClientType.InProgress)
                    ? 15 : 11;  // TODO: Move this tunables to balance!
            }
        }

        public override string  spawnRoomZoneName    { get { return trainerSpawnRoomZoneName; } }
        public override string  despawnRoomZoneName  { get { return trainerDespawnRoomZoneName; } }
        public override string  lobbyRoomZoneName    { get { return trainerLobbyRoomZoneName; } }
        public override string  idleZoneName         { get { return trainerWorkZoneName; } }
        #endregion

        #region IPersonTrainerInfo properties.
        public string                   expTxt              { get { return GetExpText(); } }
        public float                    expProgress         { get { return GetExpProgress(); } }
        public int                      clientsMax          { get { return GetMaxClients(); } }
        public bool                     canAssign           { get { return GetFirstFreeSlot() != null; } }
        public bool                     canLevelUp          { get { return CanLevelUp(); } }
        public TrainingClientLogic[]    clients             { get { return clients_; } }
        public int                      coinsBonus          { get { return GetCoinsBonus(); } }
        public TrainerUnlockInfo        nextUnlock          { get { return GetNextUnlock(); } }
        #endregion

        #region Public properties.
        public TrainingClientLogic      currentTrainingClient   { get { return currentTrainingClientIdx >= 0 ? clients_[currentTrainingClientIdx] : null; } }
        #endregion

        #region Private data.
        private TrainingClientLogic[] clients_;
        private int currentTrainingClientIdx = -1;
        #endregion

        #region Constructors.
        public PersonTrainer(IPersonManager personManager, PersonTrainerData personData)
            : base(personManager, personData)
        {
#if DEBUG
            if (DebugHacks.forcedTrainerLevel > 0)
                data.level = DebugHacks.forcedTrainerLevel;
#endif

            if (expLevels == null)
            {
                var table = DataTables.instance.trainerLevels;
                var levels = table.Select( x => x.exp).ToArray();
                expLevels = new ExperienceLevels(levels);
            }
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (personData.GetType() == typeof(PersonTrainerData)) // Exact match needed.
            {
                var personTrainerData = personData as PersonTrainerData;

                return new PersonTrainer(personManager, personTrainerData);
            }

            return null;
        }
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetupClients();

            SetTask(new TaskIdle()); // Base root task.

            base.OnPostCreate(entranceMode);
        }

        public override void OnLoaded()
        {
            SetupClients();

            if (state == PersonState.Refused)
                ChangeState(PersonState.Idle); // Try to start again from idle!

            SetTask(new TaskIdle()); // Base root task.
        }

        public override void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            base.OnPostLoad(timePassedSinceSave, loadTimeUTC);

            TryToStartWork();
        }

        public override PersonData Destroy()
        {
            if (state == PersonState.Refused)
                EquipmentStateChangedEvent.Event -= OnEquipmentStateChangedEvent;

            return base.Destroy();
        }

        public override bool KickRequest()
        {
            return false;
        }

        public override bool CanUseEquipment(Equipment equipment, int tier)
        {
            return false; // Trainers cannot use equipments now.
        }

        public override bool CanUseEquipment(Equipment equipment)
        {
            return false; // Trainers cannot use equipments now.
        }

        public override bool TryToUseEquipment
            ( Equipment equipment
            , Exercise exercise
            , ExerciseInitiator initiator
            , TimeSpan? customDuration = null
            , MovementSettings customMovementSettings = null)
        {
/*
            if (canSetTask)
            {
                CharacterRefusedExerciseEvent.Send(this, equipment, exercise);
            }
*/
            return false;
        }

        public override void OnStateChanged(PersonState oldState, PersonState newState)
        {
            base.OnStateChanged(oldState, newState);

            if (oldState == PersonState.Refused)
            {
                EquipmentStateChangedEvent.Event -= OnEquipmentStateChangedEvent;
            }

            if (newState != PersonState.Idle)
            {
                personManager.DeselectCharacterIfSelectedRequest(id);
            }

            if (newState == PersonState.Idle)
            {
                if (personManager.gameCore.isGameStarted) // Do not start too early.
                    TryToStartWork();
            }
            else if (newState == PersonState.Refused)
            {
                EquipmentStateChangedEvent.Event += OnEquipmentStateChangedEvent;
            }
        }
        #endregion

        #region Protected virtual API.
        protected override void SetName(string name)
        {
            base.SetName(name);
        }
        #endregion

        #region Public API.
        public void AddExp(int exp)
        {
            if (exp > 0)
            {
                data.experience += exp;
                personManager.OnTrainerXpUp(id);
            }
        }

        public void LevelUp()
        {
            var oldLevel = data.level;
            var newLevel = expLevels.GetLevelByTable(data.experience);

            if (newLevel == -1)
            {
                newLevel = expLevels.levelUpsData.Length;
            }

            if (newLevel > oldLevel)
            {
                data.level = newLevel;

                if (canSetTask)
                    ResetTasks();

                PlayEmoteAnimationEvent.Send(this, AnimationSocial.VictoryJump, 1, true, null);

                CharacterLevelUpEvent.Send(this, newLevel);

                TryToStartWork();
            }
            else
            {
                Debug.LogError("PersonTrainer : can't level up");
            }
        }

        public PersonTrainerClient AssignClient (Person client)
        {
            Assert.IsNotNull(client);

            if (client is PersonVisitor && client.canSetTask)
            {
                var firstFreeSlot = GetFirstFreeSlot();

                if (firstFreeSlot != null)
                {
                    var trainerClient = personManager.BecameTrainerClient(client as PersonVisitor, client.roomName);
                    Assert.IsNotNull(trainerClient);
                    client = null;

                    firstFreeSlot.client = trainerClient;

                    TryToStartWork();

/*
                    var startWork = ((currentTrainingClient == null || currentTrainingClient.state != TrainerClientType.InProgress)
                        ? SelectNextTrainingClient() : true) && canSetTask && FindTask<TaskTrainerWork>() == null;

                    if (startWork)
                    {
                        SetTask(new TaskTrainerWork());
                    }
*/

                    personManager.gameCore.ScheduleSave(Serialization.ScheduledSaveType.SaveOnThisFrame);
                    return trainerClient;
                }
                else
                {
                    Debug.LogError("Cannot assign client: no free slots.");
                }
            }
            else
            {
                Debug.LogWarningFormat("Client is not a visitor or in wrong state. Type: {0}, state: {1}.", client.GetType().Name, client.state);
            }

            return null;
        }

        public bool UnassignClient(PersonTrainerClient client)
        {
            foreach(var slot in clients_)
            {
                if (slot.client.id == client.id)
                {
                    slot.client = null;
                    ReorderClientsInSlots();

                    personManager.gameCore.ScheduleSave(Serialization.ScheduledSaveType.SaveOnThisFrame);
                    return true;
                }
            }

            return false;
        }

        public void ResetCurrentTrainingClient()
        {
            currentTrainingClientIdx = -1;
        }

        public bool SelectNextTrainingClient()
        {
            Assert.IsNotNull(clients_);
            Assert.IsTrue(clients_.Length > 0);

            var startingPosition = currentTrainingClientIdx + 1;

            currentTrainingClientIdx = -1;

            var firstAlreadyWorkingCandidateIdx = -1;

            for (int i = 0; i < clients_.Length; i++)
            {
                var adjustedIdx = (i + startingPosition) % clients_.Length;

                var candidate = clients_[adjustedIdx];
                if (candidate.state == TrainerClientType.InProgress)
                {
                    var clientPerson = candidate.client;

                    if (clientPerson.isTraining)
                    {
                        firstAlreadyWorkingCandidateIdx = adjustedIdx;
                    }
                    else if (clientPerson.canSetTask &&
                             clientPerson.visitorQuest != null &&
                             !clientPerson.visitorQuest.completed)
                    {
                        currentTrainingClientIdx = adjustedIdx;
                        return true;
                    }
                }
            }

            // If cannot find a candidate that can train - select first that is already training, so trainer can walk to support him.
            if (currentTrainingClientIdx < 0)
            {
                currentTrainingClientIdx = firstAlreadyWorkingCandidateIdx;
            }

            return currentTrainingClientIdx >= 0;
        }

        public bool IsTrainingClient(int id)
        {
            return data.clientIds.Contains(id);
        }
        
        public bool IsTrainingClient(Person person)
        {
            Assert.IsNotNull(person);

            return data.clientIds.Contains(person.id);
        }

        public void SetClientsFollow(bool enabled, MovementSettings movementSettings = null)
        {
            var followId = enabled ? id : PersonData.InvalidId;

            foreach (var slot in clients_)
            {
                if (slot.assigned)
                {
                    slot.client.Follow(followId, movementSettings);
                }
            }
        }

        public void FailedToTrainAnyClient()
        {
            if (clients.Any(c => c.state == TrainerClientType.InProgress))
                ChangeState(PersonState.Refused);
        }
        #endregion

        #region Private methods.
        private void OnEquipmentStateChangedEvent(Equipment equipment, EquipmentState oldState, EquipmentState newState)
        {
            Assert.IsTrue(state == PersonState.Refused);

            if (newState == EquipmentState.Idle &&
                equipment.isUsable &&
                canSetTask &&
                clients.Any(c => c.assigned && !c.completed && c.client.CanUseEquipment(equipment)))
            {
                ChangeState(PersonState.Idle); // Will start task on going to idle.
            }
        }

        private void TryToStartWork()
        {
            if (state != PersonState.Idle)
            {
                ChangeState(PersonState.Idle); // It will call us again.
            }
            else
            {
                if (FindTask<TaskTrainerWork>() == null && canSetTask && SelectNextTrainingClient())
                {
                    SetTask(new TaskTrainerWork());
                }
            }
        }

        bool CanLevelUp()
        {
            var oldLevel = data.level;
            var newLevel = expLevels.GetLevelByTable(data.experience);

            if (newLevel == -1)
            {
                newLevel = expLevels.levelUpsData.Length;
            }

            return newLevel > oldLevel;
        }

        int GetCoinsBonus()
        {
            var table = DataTables.instance.trainerLevels;

            var levelIdx = level - 1;

            if (levelIdx >= 0 && levelIdx < table.Length)
            {
                return table[levelIdx].coinsBonus;
            }
            else
                Debug.LogError("GetCoinsBonus wrong level index " + levelIdx.ToString());

            return 0;
        }

        TrainingClientLogic GetFirstFreeSlot()
        {
            return clients_.FirstOrDefault(x => x.state == TrainerClientType.Empty);
        }

        int GetUnlockLevelForClientSlot(int idx)
        {
            var table = DataTables.instance.trainerLevels;

            for (int i = 0; i < table.Length; i++)
            {
                var item = table[i];

                if (item.clients > idx)
                    return i + 1;
            }

            return 1;
        }

        bool IsObjectiveLocked(int idx)
        {
            return idx + 1 > GetMaxClients();
        }

        int GetMaxClients()
        {
            var table = DataTables.instance.trainerLevels;
            var idx = level - 1;

            if (idx < table.Length)
            {
                return table[idx].clients;
            }
            else
            {
                return table[table.Length -1].clients;
            }
        }

        void SetupClients()
        {
            Assert.IsNotNull(data.clientIds);

            clients_ = new TrainingClientLogic[data.clientIds.Length];
            for (int i = 0; i < clients_.Length; i++)
            {
                Person clientPerson = null;
                var clientId = data.clientIds[i];
                if (clientId != PersonData.InvalidId)
                    personManager.persons.TryGetValue(clientId, out clientPerson);

                clients_[i] = new TrainingClientLogic(data, i, clientPerson as PersonTrainerClient, IsObjectiveLocked, GetUnlockLevelForClientSlot(i));
            }
        }

        string GetExpText()
        {
            if (canLevelUp)
            {
                var threshold = expLevels.GetExpThreshold(level).ToString();
                return threshold + "/" + threshold;
            }
            
            return expLevels.GetExpRatioStr(data.experience, level);
        }

        float GetExpProgress()
        {
            if (canLevelUp)
                return 1;
            return expLevels.GetExpRatio(data.experience, level);
        }

        void ReorderClientsInSlots()
        {
            for (int i = 0; i < clients_.Length; i++)
            {
                if (!clients_[i].assigned)
                {
                    if (currentTrainingClientIdx == i)
                        currentTrainingClientIdx = -1;

                    if (i + 1 < clients_.Length)
                    {
                        if (currentTrainingClientIdx == i + 1)
                            currentTrainingClientIdx--;

                        if (clients_[i+1].assigned)
                        {
                            clients_[i].client = clients_[i+1].client;
                            clients_[i+1].client = null;
                        }
                    }
                }
            }
        }

        TrainerUnlockInfo GetNextUnlock()
        {
            // look for max clients
            var table = DataTables.instance.trainerLevels;

            if (level >= table.Length)
                return null;

            var info = new TrainerUnlockInfo();

            var currentLvlInfo = table[level - 1];
            var nextLvlInfo = table[level];

            if (currentLvlInfo.clients < nextLvlInfo.clients)
            {
                //info.sprite = GUICollections.instance.rankAchievementIconList.items[(int)Data.RankAchievement.RankAchievementType.TrainVisitors].sprite;
                info.sprite = GUICollections.instance.commonIcons.array[6];
                info.text = Loc.Get("TrainerNewMaxClients") + " " + nextLvlInfo.clients.ToString();
            }
            else if (currentLvlInfo.coinsBonus < nextLvlInfo.coinsBonus)
            {
                info.sprite = GUICollections.instance.rankAchievementIconList.items[(int)Data.RankAchievement.RankAchievementType.EarnCoins].sprite;
                info.text = Loc.Get("TrainerNewCoinsBonus") +" " + nextLvlInfo.coinsBonus.ToString();
            }
            else
            {
                var equips = DataTables.instance.estateTypes;
                var query = equips.Select(o => o.Value).Where(o => o.trainerLevelRequirement == (level + 1)).ToList();

                if (query.Count > 0)
                {
                    var equip = query.First();
                    info.sprite = GUICollections.instance.rankAchievementIconList.items[(int)Data.RankAchievement.RankAchievementType.BuyEquip].sprite;
                    info.text = Loc.Get("TrainerNewEquipment") + " " + Loc.Get(equip.locNameId);
                }
                else
                {
                    info.text = "TrainerMaxLevel";
                }
            }

            return info;
        }
        #endregion
    }
}
