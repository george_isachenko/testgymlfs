﻿using System;
using BridgeBinder;
using Core;
using Core.Collections;
using Data;
using Data.Person;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Estate;
using Logic.Estate.Events;
using Logic.Persons.Events;
using Logic.Persons.Tasks;
using Logic.Quests.Base;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

namespace Logic.Persons.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Persons/Facade/Logic Person Manager Game")]
    public class LogicPersonManagerGame
        : LogicPersonManagerBase
    {
        #region Interfaces: properties
        public override bool hasSelection
        {
            get { return selectedCharacterId_ != PersonData.InvalidId; }
        }

        public override int selectedCharacterId
        {
            get { return selectedCharacterId_; }
        }
        #endregion

        #region Inspector data
        #endregion

        #region Private data
        private int selectedCharacterId_ = PersonData.InvalidId; // Not selected.
        private bool characterSelectionEnabled = true;
        #endregion

        #region Bridge
        #endregion

        #region Bridge event handlers.
        [BindBridgeEvent("onCharacterPositionUpdated")]
        void OnCharacterPositionUpdated (int id, RoomCoordinates position, float heading)
        {
            var person = GetPerson(id);
            Assert.IsNotNull(person);
            if (person != null)
            {
                person.UpdatePosition(position, heading);
            }
        }
        #endregion

        #region Unity API
        protected new void Awake()
        {
            base.Awake();

            EquipmentSelectionEvent.Event += OnEquipmentSelectionEvent;

            Rooms.Events.RoomDeactivatedEvent.Event += OnRoomDeactivatedEvent;
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            EquipmentSelectionEvent.Event -= OnEquipmentSelectionEvent;

            Rooms.Events.RoomDeactivatedEvent.Event -= OnRoomDeactivatedEvent;
        }

#if DEBUG
        protected new void Update()
        {
            // Debug Auto-play stuff.
            if (gameCore.isGameStarted && DebugHacks.visitorsAutoPlay && gameCore.quests.tutorial != null && !gameCore.quests.tutorial.isRunning)
            {
                var personCandidates = persons.Values.Where
                    ( p => (p.state == PersonState.Idle || p.state == PersonState.ExerciseComplete || p.state == PersonState.Workout));
                
                foreach (var candidate in personCandidates)
                {
                    candidate.DebugAutoPlay();
                }
            }

            base.Update();
        }
#endif
        #endregion

#region Public API
public override PersonVisitor CreateVisitor(string roomName, Person.EntranceMode entranceMode, string spawnZoneName)
        {
            Assert.IsNotNull(roomName);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var questData = new VisitorQuestData(id);

            var personData = new PersonVisitorData(id, 100, roomName, questData);
            persistentData.personsData.Add(personData);

            RandomizeAppearance(ref personData.appearance, PersonAppearance.ViewType.Gym);
            personData.name = GenerateRandomName(personData.appearance.gender, true);
            personData.appearance.bodyType = PersonAppearance.BodyType.Fat;

            var person = new PersonVisitor(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public override bool SelectCharacterRequest(int id)
        {
            if (characterSelectionEnabled)
            {
/*
                // TODO: TEMPORARY HACK.
                if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftShift)) && selectedCharacterId_ != PersonData.InvalidId)
                {
                    Person trainer, client;
                    if (persons.TryGetValue(selectedCharacterId_, out trainer) && trainer is PersonTrainer &&
                        persons.TryGetValue(id, out client) && client is PersonVisitor)
                    {
                        if (((PersonTrainer)trainer).AssignClient(client))
                        {
                            Debug.LogFormat(">>> Assigned client #{0} ({1}) to trainer #{2} ({3}", client.id, client.name, trainer.id, trainer.name);
                            return false;
                        }
                    }
                }

                // TODO: END.
*/

                Person oldPerson = null;
                Person newPerson = null;

                if (selectedCharacterId_ != PersonData.InvalidId)
                    persons.TryGetValue(selectedCharacterId_, out oldPerson);

                if (id == PersonData.InvalidId)
                {
                }
                else if (selectedCharacterId_ != PersonData.InvalidId && id == selectedCharacterId_)
                {
                    return true;
                }
                else if (gameCore.equipmentManager.isInEditMode || !persons.TryGetValue(id, out newPerson))
                {
                    return false;
                }

                if (newPerson == null || newPerson.SelectRequest())
                {
                    selectedCharacterId_ = id;

                    if (oldPerson != null)
                    {
                        oldPerson.OnDeselected();
                        SetSelectionEvent.Send(oldPerson, false, !DebugHacks.visitorsAutoPlay);
                    }

                    if (newPerson != null)
                    {
                        newPerson.OnSelected();
                        SetSelectionEvent.Send(newPerson, true, false);

                        PersistentCore.instance.analytics.DetailGameEvent("Client Select");
                    }
                    return true;
                }
            }

            return false;
        }

        public override void DeselectCharacterIfSelectedRequest(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                DeselectCharacterIfSelectedRequest(person);
            }
        }

        public override bool KickRequest(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                if (person.KickRequest())
                {
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    return true;
                }
            }

            return false;
        }

        public override bool ClaimQuestRequest(Person person)
        {
            return person.ClaimQuestRequest();
        }

        public override bool ClaimQuestRequest(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                return ClaimQuestRequest(person);
            }

            return false;
        }

        public override void SkipExerciseRequest(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                //------
                // DebugSpawnCharacterWorkingOnEquipment();
                //------

                person.SkipExercise();
            }
        }

        public override void SetCharactersSelectionMode(bool allowSelection)
        {
            if (!allowSelection && selectedCharacterId_ != PersonData.InvalidId)
                SelectCharacterRequest(PersonData.InvalidId);
            characterSelectionEnabled = allowSelection;
        }
        #endregion

        #region Protected virtual API.
        protected override void Reinit()
        {
            base.Reinit();

            selectedCharacterId_ = PersonData.InvalidId;
        }
        #endregion

        #region IPersistent API 
        public override void OnLoaded(bool success)
        {
            if (success)
            {
                persistentData.personsData.RemoveAll(x => x.state == PersonState.Sentenced);

                foreach (var personData in persistentData.personsData)
                {
                    var person = Person.Instantiate(this, personData);

                    if (person != null)
                    {
                        persons.Add(person.id, person);
                    }
                    else
                    {
                        // TODO: Decide what to do here: issue warning, do cleanup etc.
                    }
                }

                foreach (var person in persons.Values)
                {
                    if (person.state == PersonState.Sentenced)
                    {
                        if (!sentencedPersons.Contains(person.id))
                            sentencedPersons.Add(person.id);
                    }
                    else
                    {
                        person.OnLoaded();
                    }
                }
            }
            else
            {
                persons.Clear(); // Do not want to send Death events!
                Reinit();
            }
        }
        #endregion

        #region Private API
        private void OnEquipmentSelectionEvent(Equipment equipment, bool selected)
        {
            Assert.IsNotNull(equipment);
            if (equipment != null)
            {
                if (equipment.isOccupied)
                {
                    if (selected && equipment.occupantCharacterId != selectedCharacterId_)
                    {
                        SelectCharacterRequest(equipment.occupantCharacterId);
                    }
                    else if (!selected && equipment.occupantCharacterId == selectedCharacterId_)
                    {
                        DeselectCharacterIfSelectedRequest(equipment.occupantCharacterId);
                    }
                }
                else if (selected && equipment.isUsable)
                {
                    Person person;
                    if (selectedCharacterId_ != PersonData.InvalidId &&
                        persons.TryGetValue(selectedCharacterId_, out person) &&
                        person.canBeTrainedByPlayer &&
                        person.canSetTask)
                    {
                        person.SetTask ( new TaskGoto
                            ( new CharacterDestination(equipment)
                            , DataTables.instance.balanceData.Characters.Movement.MoveToEquipment));
                    }
                }
            }
        }

        private void DeselectCharacterIfSelectedRequest(Person person)
        {
            Assert.IsNotNull(person);
            if (person.id == selectedCharacterId_)
            {
                selectedCharacterId_ = PersonData.InvalidId;

                person.OnDeselected();
                SetSelectionEvent.Send(person, false, false);
            }
        }

        private void OnRoomDeactivatedEvent(string obj)
        {
            gameCore.personManager.DeselectCharacterIfSelectedRequest(gameCore.personManager.selectedCharacterId);
            //            SelectCharacterRequest(PersonData.InvalidId);
        }

        public override void OnTrainerXpUp(int id)
        {
            presentationCharacterManager.UpdateOverheadUI(id);
        }

#if DEBUG
/*
        private void DebugSpawnCharacterWorkingOnEquipment ()
        {
            var equipmentTypes = DataTables.instance.estateTypes.Where(x => x.Value.itemType == Data.Estate.EstateType.Subtype.Training).Select(x => x.Value).ToArray();

            var equipmentId = gameCore.equipmentManager.CreateEquipment(equipmentTypes[Random.Range(0, equipmentTypes.Length)].id, new EquipmentPlacement(Random.Range(3, 18), Random.Range(3, 18), Random.Range(0, 4)));
            var equipment = gameCore.equipmentManager.equipment[equipmentId];
            //                         gameCore.equipmentManager.equipment.Where
            //                     (x => x.Value.estateType.itemType == Data.Estate.EstateType.Subtype.Training &&
            //                      x.Value.state == Data.Estate.EquipmentState.Idle).Select(x => x.Value).FirstOrDefault();

            if (equipment != null)
            {
                gameCore.playerProfile.AddCoins(10000, new Analytics.MoneyInfo("Debug", "BuyWithBucks"));
                gameCore.playerProfile.AddFitBucks(10000, new Analytics.MoneyInfo("Debug", "BuyWithBucks"));

                equipment.SkipAssembly();
                if (equipment.state == Data.Estate.EquipmentState.AssemblyComplete)
                {
                    equipment.ConfirmAssemblyComplete();
                }

                int visitorId = persons.ProposeKey();
                Assert.IsTrue(visitorId != AutoKeyIntDictionary<Person>.invalidId);

                var visitorQuestData = new VisitorQuestData(visitorId);
                var quest = gameCore.quests.NewVisitorQuest(visitorQuestData);

                // Core::get().tables.members.generate_name(person.apperance.isMale());
                var personVisitorData = new PersonVisitorData(visitorId, 100, gameCore.roomManager.GetRoomNameOfType(RoomType.Club), visitorQuestData);
                persistentData.personsData.Add(personVisitorData);

                RandomizeAppearance(ref personVisitorData.appearance, PersonAppearance.ViewType.Gym);
                personVisitorData.name = GenerateRandomName(personVisitorData.appearance.gender, true);
                personVisitorData.appearance.bodyType = PersonAppearance.BodyType.Fat;

                var visitor = new PersonVisitor(this, personVisitorData, quest);

                persons.Add(visitorId, visitor);

                // visitor.OnPostCreate();
                visitor.SetTask(new TaskIdle(visitor, visitor.idleZoneName));

                visitor.data.workoutExercise = equipment.estateType.exercises[0];
                visitor.data.workoutEquipmentId = equipment.data.id;
                visitor.data.state = PersonState.Workout;
                visitor.data.exerciseTotalTime = 60.0f;
                visitor.data.exerciseEndTime = System.DateTime.UtcNow + System.TimeSpan.FromMinutes(1.0);

                equipment.data.state = Data.Estate.EquipmentState.Working;
                equipment.data.occupantCharacterId = visitor.data.id;

                CharacterBirthEvent.Send(visitor, false, new CharacterDestination(equipment.placement.position));

                visitor.SetTask(new TaskExercise(visitor, equipment));

                equipment.StartExercise(visitor, true, visitor.data.workoutExercise);
                visitor.StartExercise(equipment, true, null);
            }
        }
*/
#endif

        #endregion
    }
}