﻿using System;
using Core.Collections;
using Data.Person;
using Data.Room;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Persons/Facade/Logic Person Manager Demo")]
    public class LogicPersonManagerDemo
        : LogicPersonManagerBase
    {
        #region Interfaces: properties
        #endregion

        #region Inspector data
        #endregion

        #region Private data
        #endregion

        #region Bridge
        #endregion

        #region Bridge event handlers.
        #endregion

        #region Unity API
        #endregion

        #region Public API
        public override PersonVisitor CreateVisitor(string roomName, Person.EntranceMode entranceMode, string spawnZoneName)
        {
            Assert.IsNotNull(roomName);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var personData = new PersonVisitorData(id, 100, roomName, null);
            persistentData.personsData.Add(personData);

            RandomizeAppearance(ref personData.appearance, PersonAppearance.ViewType.Gym);
            personData.name = GenerateRandomName(personData.appearance.gender, true);
            personData.appearance.bodyType = PersonAppearance.BodyType.Fat;

            var person = new PersonVisitor(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }
        #endregion

        #region Protected virtual API.
        #endregion

        #region IPersistent API 
        public override void OnLoaded(bool success)
        {
            if (success)
            {
                persistentData.personsData.RemoveAll(x => x.state == PersonState.Sentenced);

                foreach (var personData in persistentData.personsData)
                {
                    personData.exerciseEndTime = DateTime.MaxValue.ToUniversalTime(); // TODO: Hack to make infinite exercise time. Improve this!

                    var person = Person.Instantiate(this, personData);

                    if (person != null)
                    {
                        persons.Add(person.id, person);
                    }
                    else
                    {
                        // TODO: Decide what to do here: issue warning, do cleanup etc.
                    }
                }

                foreach (var person in persons.Values)
                {
                    if (person.state == PersonState.Sentenced)
                    {
                        if (!sentencedPersons.Contains(person.id))
                            sentencedPersons.Add(person.id);
                    }
                    else
                    {
                        person.OnLoaded();
                    }
                }
            }
            else
            {
                persons.Clear(); // Do not want to send Death events!
                Reinit();
            }
        }

        public override void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (var person in persons.Values)
            {
                person.OnRestored();
            }

            foreach (var person in persons.Values)
            {
                person.OnPostLoad(TimeSpan.Zero, loadTimeUTC);
            }
        }
        #endregion

        #region PrivateAPI
        #endregion
    }
}