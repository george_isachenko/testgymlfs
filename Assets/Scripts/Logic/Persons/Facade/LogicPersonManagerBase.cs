﻿using BridgeBinder;
using Core;
using Core.Analytics;
using Core.Collections;
using Data;
using Data.Person;
using Data.Quests;
using Data.Quests.Juicer;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Core;
using Logic.Estate;
using Logic.FacadesBase;
using Logic.Serialization;
using Presentation.Facades.Interfaces.CharacterManager;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons.Facade
{
    [BindBridgeInterface(typeof (IPresentationCharacterManager))]
    public abstract class LogicPersonManagerBase
        : BaseLogicFacade
        , ILogicPersonManager
        , IPersonManager
        , IPersistent
    {
        #region Static data
        protected static readonly int personsInitialCapacity = 64;
        #endregion

        #region Interfaces: events
        public event CharacterSelectionEvent CharacterSelectionEvent;
        public event CharacterStateChangedEvent CharacterStateChangedEvent;
        public event CharacterEnteredRoomEvent CharacterEnteredRoomEvent;
        public event CharacterLeavingRoomEvent CharacterLeavingRoomEvent;
        public event CharacterTypeChangedEvent CharacterTypeChangedEvent;
        public event CharacterRefusedExerciseEvent CharacterRefusedExerciseEvent;
        public event CharacterReadySkipExerciseEvent CharacterReadyToSkipExerciseEvent;
        public event CharacterSchedulePendingExerciseEvent CharacterSchedulePendingExerciseEvent;
        public event CharacterSwitchPendingExerciseEvent CharacterSwitchPendingExerciseEvent;
        public event CharacterCancelPendingExerciseEvent CharacterCancelPendingExerciseEvent;
        public event CharacterAppearanceChangedEvent CharacterAppearanceChangedEvent;
        public event SportsmanTypeChangedEvent SportsmanTypeChangedEvent;
        public event CharacterLevelUpEvent CharacterLevelUpEvent;
        #endregion

        #region Interfaces: properties
        public virtual bool hasSelection
        {
            get { return false; }
        }

        public virtual int selectedCharacterId
        {
            get { return PersonData.InvalidId; }
        }

        IGameCore IPersonManager.gameCore
        {
            get { return gameCore; }
        }

        IDictionary<int, Person> IPersonManager.persons
        {
            get { return persons; }
        }

        int IPersonManager.sportsmenCapacityLevel
        {
            get { return persistentData.sportsmenStorageLevel; }
            set { persistentData.sportsmenStorageLevel = value; } 
        }
        #endregion

        #region IPersistent properties.
        string IPersistent.propertyName
        {
            get { return "Characters"; }
        }

        Type IPersistent.propertyType
        {
            get { return typeof (PersonsPersistentData); }
        }
        #endregion

        #region Inspector data
        #endregion

        #region Protected data
        protected IPresentationCharacterManager presentationCharacterManager;
        protected AutoKeyIntDictionary<Person> persons;
        #endregion

        #region Private data.
        protected List<int> sentencedPersons = new List<int>(16);
        #endregion

        #region Persistent data
        protected PersonsPersistentData persistentData;
        #endregion

        #region Bridge
        [BindBridgeSubscribe]
        protected void Subscribe(IPresentationCharacterManager presentationCharacterManager)
        {
            this.presentationCharacterManager = presentationCharacterManager;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(IPresentationCharacterManager presentationCharacterManager)
        {
            this.presentationCharacterManager = null;
        }
        #endregion

        #region Bridge event handlers.
        #endregion

        #region Unity API
        protected void Awake()
        {
            persistentData.personsData = new List<PersonData>(personsInitialCapacity);
            persistentData.sportsmenStorageLevel = 0;
            persons = new AutoKeyIntDictionary<Person>(personsInitialCapacity);

            Events.SetSelectionEvent.Event += OnSetSelectionEvent;
            Events.CharacterStateChangedEvent.Event += OnCharacterStateChangedEvent;
            Events.CharacterBirthEvent.Event += OnCharacterBirthEvent;
            Events.CharacterDeathEvent.Event += OnCharacterDeathEvent;
            Events.CharacterEnteredRoomEvent.Event += OnCharacterEnteredRoomEvent;
            Events.CharacterLeavingRoomEvent.Event += OnCharacterLeavingRoomEvent;
            Events.CharacterTypeChangedEvent.Event += OnCharacterTypeChangedEvent;
            Events.CharacterSentencedEvent.Event += OnCharacterSentencedEvent;
            Events.CharacterTransferEvent.Event += OnCharacterTransferEvent;
            Events.MoveCharacterEvent.Event += OnMoveCharacterEvent;
            Events.StopCharacterEvent.Event += OnStopCharacterEvent;
            Events.ChangeCharacterMovementModeEvent.Event += OnChangeCharacterMovementModeEvent;
            Events.CharacterSchedulePendingExerciseEvent.Event += OnCharacterSchedulePendingExerciseEvent;
            Events.CharacterSwitchPendingExerciseEvent.Event += OnCharacterSwitchPendingExerciseEvent;
            Events.CharacterCancelPendingExerciseEvent.Event += OnCharacterCancelPendingExerciseEvent;
            Events.CharacterStartExerciseEvent.Event += OnCharacterStartExerciseEvent;
            Events.CharacterSwitchExerciseEvent.Event += OnCharacterSwitchExerciseEvent;
            Events.CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent;
            Events.PlayEmoteAnimationEvent.Event += OnPlayEmoteAnimationEvent;
            Events.CharacterLookAtEvent.Event += OnCharacterLookAtEvent;
            Events.CharacterRefusedExerciseEvent.Event += OnCharacterRefusedExerciseEvent;
            Events.CharacterReadyToSkipExerciseEvent.Event += OnCharacterReadyToSkipExerciseEvent;
            Events.CharacterAppearanceChangedEvent.Event += OnCharacterAppearanceChangedEvent;
            Events.SportsmanTypeChangedEvent.Event += OnSportsmanTypeChangedEvent;
            Events.CharacterLevelUpEvent.Event += OnCharacterLevelUpEvent;
        }

        protected void Start()
        {
        }

        protected void OnDestroy()
        {
            Events.SetSelectionEvent.Event -= OnSetSelectionEvent;
            Events.CharacterStateChangedEvent.Event -= OnCharacterStateChangedEvent;
            Events.CharacterBirthEvent.Event -= OnCharacterBirthEvent;
            Events.CharacterDeathEvent.Event -= OnCharacterDeathEvent;
            Events.CharacterEnteredRoomEvent.Event -= OnCharacterEnteredRoomEvent;
            Events.CharacterLeavingRoomEvent.Event -= OnCharacterLeavingRoomEvent;
            Events.CharacterTypeChangedEvent.Event -= OnCharacterTypeChangedEvent;
            Events.CharacterSentencedEvent.Event -= OnCharacterSentencedEvent;
            Events.CharacterTransferEvent.Event -= OnCharacterTransferEvent;
            Events.MoveCharacterEvent.Event -= OnMoveCharacterEvent;
            Events.StopCharacterEvent.Event -= OnStopCharacterEvent;
            Events.ChangeCharacterMovementModeEvent.Event -= OnChangeCharacterMovementModeEvent;
            Events.CharacterSchedulePendingExerciseEvent.Event -= OnCharacterSchedulePendingExerciseEvent;
            Events.CharacterSwitchPendingExerciseEvent.Event -= OnCharacterSwitchPendingExerciseEvent;
            Events.CharacterCancelPendingExerciseEvent.Event -= OnCharacterCancelPendingExerciseEvent;
            Events.CharacterStartExerciseEvent.Event -= OnCharacterStartExerciseEvent;
            Events.CharacterSwitchExerciseEvent.Event -= OnCharacterSwitchExerciseEvent;
            Events.CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
            Events.PlayEmoteAnimationEvent.Event -= OnPlayEmoteAnimationEvent;
            Events.CharacterLookAtEvent.Event -= OnCharacterLookAtEvent;
            Events.CharacterRefusedExerciseEvent.Event -= OnCharacterRefusedExerciseEvent;
            Events.CharacterReadyToSkipExerciseEvent.Event -= OnCharacterReadyToSkipExerciseEvent;
            Events.CharacterAppearanceChangedEvent.Event -= OnCharacterAppearanceChangedEvent;
            Events.SportsmanTypeChangedEvent.Event -= OnSportsmanTypeChangedEvent;
            Events.CharacterLevelUpEvent.Event -= OnCharacterLevelUpEvent;
        }

        protected void OnApplicationPause (bool pauseStatus)
        {
            /*
            if (!pauseStatus && !Application.isEditor)
            {
                Reset();
            }
            */
        }

        protected void Update()
        {
            if (gameCore.isGameStarted)
            {
                var dt = Time.deltaTime;
                foreach (var person in persons)
                {
                    person.Value.UpdateTask(dt);
                }

            }
        }

        protected void LateUpdate ()
        {
            if (gameCore.isGameStarted)
            {
                CleanupSentenced();
            }
        }
        #endregion

        #region Public API
        public abstract PersonVisitor CreateVisitor(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null);

        public virtual PersonJuicer CreateJuicer(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null)
        {
            Assert.IsNotNull(roomName);

            var juicesResources = DataTables.instance.balanceData.Storage.Resources.Where(x => !x.Value.disabled && x.Value.category == "Juice");
            Assert.IsTrue(juicesResources != null && juicesResources.Count() > 0);
            if (juicesResources != null && juicesResources.Count() > 0)
            {
                int id = persons.ProposeKey();
                Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

                var questData = new JuiceQuestData(id);

                var personData = new PersonJuicerData(id, 100, roomName, questData);
                persistentData.personsData.Add(personData);

                RandomizeAppearance(ref personData.appearance, PersonAppearance.ViewType.Gym);
                personData.name = GenerateRandomName(personData.appearance.gender, true);
                // personJuicerData.appearance.bodyType = PersonAppearance.BodyType.Fat;

                var person = new PersonJuicer(this, personData);

                persons.Add(id, person);

                person.OnCreated(spawnZoneName);
                person.OnPostCreate(entranceMode);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                return person;
            }
            else
            {
                return null;
            }
        }

        public virtual PersonPasserby CreatePasserby(string roomName, Person.EntranceMode entranceMode, string spawnZoneName = null)
        {
            Assert.IsNotNull(roomName);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var personData = new PersonData(id, 1, roomName);
            
            // Passerby's are not saved into persistent data!
            // persistentData.personsData.Add(personData);

            RandomizeAppearance(ref personData.appearance, PersonAppearance.ViewType.Street);
            personData.name = GenerateRandomName(personData.appearance.gender, false);
            //personData.appearance.bodyType = PersonAppearance.BodyType.Normal;

            var person = new PersonPasserby(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            // gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public virtual PersonSportsman CreateSportsman
            ( string roomName
            , Person.EntranceMode entranceMode
            , SportsmanType sportsmanType
            , PersonAppearance.Gender gender
            , string name = null
            , string spawnZoneName = null)
        {
            Assert.IsNotNull(roomName);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var personData = new PersonSportsmanData(id, 100, roomName, sportsmanType);
            persistentData.personsData.Add(personData);

            RandomizeAppearanceForSportsman(ref personData.appearance, gender, PersonAppearance.ViewType.SportClub, sportsmanType);

            personData.name = (name != null) ? name : GenerateRandomName(gender, true);
            personData.appearance.bodyType = PersonAppearance.BodyType.Strong;

            var person = new PersonSportsman(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public virtual PersonSportsman CreateSportsman
            ( string roomName
            , Person.EntranceMode entranceMode
            , SportsmanType sportsmanType
            , PersonAppearance appearance
            , string name = null
            , string spawnZoneName = null)
        {
            Assert.IsNotNull(roomName);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var personData = new PersonSportsmanData(id, 100, roomName, sportsmanType);
            personData.appearance = appearance;
            personData.name = (name != null) ? name : GenerateRandomName(personData.appearance.gender, true);
            UpdateSportsmanAppearance(ref personData.appearance, sportsmanType);
            persistentData.personsData.Add(personData);

            var person = new PersonSportsman(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public virtual PersonTrainer CreateTrainer(string roomName, Person.EntranceMode entranceMode, string name, string spawnZoneName = null)
        {
            Assert.IsNotNull(roomName);
            Assert.IsNotNull(name);

            int id = persons.ProposeKey();
            Assert.IsTrue(id != AutoKeyIntDictionary<Person>.invalidId);

            var trainersCount = persons.Values.Count(x => x is PersonTrainer);

            var personData = new PersonTrainerData(id, 1, roomName); // TODO: TEMPORARY HACK!

            SelectAppearanceForTrainer(ref personData.appearance, trainersCount);
            personData.appearance.bodyType = PersonAppearance.BodyType.Strong;
            personData.name = name;

            persistentData.personsData.Add(personData);

            var person = new PersonTrainer(this, personData);

            persons.Add(id, person);

            person.OnCreated(spawnZoneName);
            person.OnPostCreate(entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public virtual PersonSportsman BecameSportsman
            ( PersonVisitor visitor
            , SportsmanType sportsmanType
            , string roomName
            , Person.EntranceMode entranceMode)
        {
            PersonVisitorData oldData;
            PersonSportsmanData newData;

            var person = visitor.BecameSportsman(sportsmanType, out oldData, out newData);

            persistentData.personsData.Remove(oldData);
            persistentData.personsData.Add(newData);

            persons.Remove(visitor.id);
            persons.Add(person.id, person);

            visitor = null;

            Events.CharacterTypeChangedEvent.Send (person);

            person.OnConverted(roomName, entranceMode);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public virtual PersonJuicer BecameJuicer(PersonVisitor visitor, string roomName, Person.EntranceMode entranceMode)
        {
            var juicesResources = DataTables.instance.balanceData.Storage.Resources.Where(x => !x.Value.disabled && x.Value.category == "Juice");
            Assert.IsTrue(juicesResources != null && juicesResources.Count() > 0);
            if (juicesResources != null && juicesResources.Count() > 0)
            {
                PersonVisitorData oldData;
                PersonJuicerData newData;

                var person = visitor.BecameJuicer(out oldData, out newData);

                persistentData.personsData.Remove(oldData);
                persistentData.personsData.Add(newData);

                persons.Remove(visitor.id);
                persons.Add(person.id, person);

                visitor = null;

                Events.CharacterTypeChangedEvent.Send (person);

                person.OnConverted(roomName, entranceMode);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                return person;
            }
            else
            {
                return null;
            }
        }

        public virtual PersonTrainerClient BecameTrainerClient(PersonVisitor visitor, string newRoomName)
        {
            PersonVisitorData oldData;
            PersonTrainerClientData newData;

            var person = visitor.BecameTrainerClient(out oldData, out newData);

            RandomizeCostume(ref newData.appearance, PersonAppearance.ViewType.TrainerClient, false);

            persistentData.personsData.Remove(oldData);
            persistentData.personsData.Add(newData);

            persons.Remove(visitor.id);
            persons.Add(person.id, person);

            visitor = null;

            Events.CharacterTypeChangedEvent.Send (person);

            person.OnConverted(person.roomName, Person.EntranceMode.ImmediateEntrance);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return person;
        }

        public Person GetPerson(int id)
        {
            Person person;
            return persons.TryGetValue(id, out person) ? person : null;
        }

        public int GetCharactersCount(Type personType = null, string roomName = null, params PersonState[] states)
        {
            int count = 0;

            foreach (var person in persons.Values)
            {
                if ((personType == null || personType == person.GetType()) &&
                    (roomName == null || roomName == person.roomName) &&
                    (states == null || states.Length == 0 || states.Contains(person.state)))
                {
                    count++;
                }
            }

            return count;
        }

        public virtual bool SelectCharacterRequest(int id)
        {
            return false;
        }

        public virtual void DeselectCharacterIfSelectedRequest(int id)
        {
        }

        public QuestStage? GetQuestStage(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                return person.GetQuestStage();
            }

            return null;
        }

        public VisitorQuestObjectiveData GetVisitorQuestCurrentObjectiveData(int id)
        {
            var q = gameCore.quests.visitorQuests.GetVisitorQuest(id);
            if (q != null && q.currentObjective != null)
                return q.currentObjective.GetData();
            return null;
        }

        public virtual bool KickRequest(int id)
        {
            return false;
        }

        public virtual bool ClaimQuestRequest(Person person)
        {
            return false;
        }

        public virtual bool ClaimQuestRequest(int id)
        {
            return false;
        }

        public virtual bool MoveCharacterRequest(int id, RoomCoordinates coords)
        {
            Person person;
            if (persons.TryGetValue(id, out person) && person.isPlayerControllable)
            {
                if (person.MoveRequest(new CharacterDestination(coords)))
                {
                    gameCore.ScheduleSave(ScheduledSaveType.SaveSoon);
                    return true;
                }
            }

            return false;
        }

        public virtual void KillCharacterRequest(int id)
        {
            Person person;
            if (persons.TryGetValue(id, out person))
            {
                Assert.IsTrue(person.state != PersonState.Sentenced
                    , "Person is already sentenced to death, you necrophiliac!");
                Assert.IsTrue(person.state != PersonState.WorkoutPending && person.state != PersonState.Workout
                    , "Person is training while you're trying to kill him! So cruel.");

                person.Kill();
            }
        }

        public virtual void SkipExerciseRequest(int id)
        {
        }

        public virtual bool UseEquipmentRequest
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator)
        {
            Assert.IsNotNull(person);
            return person.TryToUseEquipment(equipment, exercise, initiator);
        }

        public virtual void SetCharactersSelectionMode(bool allowSelection)
        {
        }

        public IEnumerable<PersonSportsman> GetSportsmen()
        {
            var result = (from person in persons.Values
                where person is PersonSportsman
                select person as PersonSportsman);

            return result;
        }

        public int GetSportsmanCount(SportsmanType? sportsmanType, string roomName = null, params PersonState[] states)
        {
            int count = 0;

            foreach (var person in persons.Values)
            {
                var sportsman = person as PersonSportsman;
                if (sportsman != null)
                {
                    if ((sportsmanType == null || sportsmanType == sportsman.sportsmanType) &&
                        (roomName == null || roomName == person.roomName) &&
                        (states == null || states.Length == 0 || states.Contains(person.state)))
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public PersonSportsman GetSportsman
            (SportsmanType? sportsmanType, string roomName = null, params PersonState[] states)
        {
            foreach (var person in persons.Values)
            {
                var sportsman = person as PersonSportsman;
                if (sportsman != null)
                {
                    if ((sportsmanType == null || sportsmanType == sportsman.sportsmanType) &&
                        (roomName == null || roomName == person.roomName) &&
                        (states == null || states.Length == 0 || states.Contains(person.state)))
                    {
                        return sportsman;
                    }
                }
            }

            return null;
        }

        public int RemoveSportsmen(SportsmanType sportsmanType, int count)
        {
            var needRemove = Mathf.Min(count, GetSportsmanCount(sportsmanType));

            if (needRemove > 0)
            {
                var sportsmentoRemove = persons.Values
                    .Where(p => p is PersonSportsman && p.state != PersonState.Sentenced && p.state != PersonState.Leaving && p.state != PersonState.Workout && p.state != PersonState.WorkoutPending)
                    .Select(p => p)
                    .Cast<PersonSportsman>()
                    .Where(s => s.sportsmanType == sportsmanType)
                    .Select(s => s)
                    .Take(needRemove);

                foreach (var person in sportsmentoRemove)
                {
                    person.Quit();
                }

                return sportsmentoRemove.Count();
            }
            return 0;
        }
            
        public string GenerateRandomName(PersonAppearance.Gender gender, bool unique = true)
        {
            string result;
            do
            {
                var firstNames = (gender == PersonAppearance.Gender.Male)
                    ? DataTables.instance.personFirstNamesMale
                    : DataTables.instance.personFirstNamesFemale;
                Assert.IsNotNull(firstNames);

                var lastNames = (gender == PersonAppearance.Gender.Male)
                    ? DataTables.instance.personLastNamesMale
                    : DataTables.instance.personLastNamesFemale;
                Assert.IsNotNull(lastNames);

                var firstName = firstNames.Values.ElementAt(UnityEngine.Random.Range(0, firstNames.Count));
                var lastName = lastNames.Values.ElementAt(UnityEngine.Random.Range(0, lastNames.Count));

                result = string.Format("{0} {1}", firstName.name, lastName.name);

            } while (unique && persons.Values.Any((person) => person.name == result));

            return result;
        }

        public void UpdateSportsmanAppearance(ref PersonAppearance appearance, SportsmanType sportsmanType)
        {
            SelectCostumeForSportClub(ref appearance, sportsmanType);
        }

        public void SetPersonName(int id, string name)
        {
            var p = GetPerson(id);     
            p.name = name;
        }

        public PersonAppearance GetTrainerAppearance(int trainerIdx)
        {
            var appearance = new PersonAppearance();
            SelectAppearanceForTrainer(ref appearance, trainerIdx);
            return appearance;
        }
        #endregion

        #region IPersistent API 
        public virtual bool Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersonsPersistentData);
            if (obj is PersonsPersistentData)
            {
                persistentData = (PersonsPersistentData) obj;
                return true;
            }
            return false;
        }

        public virtual object Serialize()
        {
            return persistentData;
        }

        public virtual void InitDefaultData()
        {
            
        }

        public virtual void OnPreLoad()
        {
            Reinit();
        }

        public virtual void OnLoaded(bool success)
        {   // See overrides.
        }

        public virtual void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (var person in persons.Values)
            {
                person.OnRestored();
            }

            // NOTE: Must construct offlineTrainer before OnPostLoad() called on persons and their initial state is lost.
            var clubRoomName = gameCore.roomManager.GetRoomNameOfType(RoomType.Club);

            var offlineTrainer = new OfflineTrainer
                ( clubRoomName
                , gameCore.equipmentManager.equipment
                , persons
                , loadTimeUTC
                , timePassedSinceSave );

            foreach (var person in persons.Values)
            {
                person.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }

            offlineTrainer.Simulate();
        }

        public virtual void OnLoadSkipped()
        {
        }

        public virtual void OnTrainerXpUp(int id)
        {

        }
        #endregion

        #region Protected virtual API.
        protected virtual void Reinit()
        {
            foreach (var person in persons.Values)
            {
                person.Destroy();
            }

            persons.Clear();
            sentencedPersons.Clear();

            persistentData.personsData.Clear();
            persistentData.sportsmenStorageLevel = 0;
        }
        #endregion

        #region Protected API.
        protected void Reset()
        {
            SelectCharacterRequest(PersonData.InvalidId);
        }
        #endregion

        #region Private functions.
        protected void CleanupSentenced()
        {
            // Remove 'sentenced' characters.
            if (sentencedPersons.Count > 0)
            {
                foreach (var id in sentencedPersons)
                {
                    Person person;
                    if (persons.TryGetValue(id, out person))
                    {
                        if (selectedCharacterId == person.id)
                            DeselectCharacterIfSelectedRequest(selectedCharacterId);

                        var personData = person.Destroy();
                        persistentData.personsData.Remove(personData);

                        persons.Remove(id);
                    }
                }
                sentencedPersons.Clear();

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            }
        }

        protected void RandomizeAppearance
            ( ref PersonAppearance appearance
            , PersonAppearance.ViewType viewType
            , bool generateHead = true
            , bool strictViewType = false)
        {
            appearance.visibleMood = PersonAppearance.VisibleMood.Good;
            appearance.gender = (PersonAppearance.Gender)UnityEngine.Random.Range(0, Enum.GetNames(typeof (PersonAppearance.Gender)).Length);
            appearance.race = (PersonAppearance.Race)UnityEngine.Random.Range(0, Enum.GetNames(typeof (PersonAppearance.Race)).Length);
            appearance.bodyType = (PersonAppearance.BodyType)UnityEngine.Random.Range(0, Enum.GetNames(typeof (PersonAppearance.BodyType)).Length);
            appearance.viewType = viewType;

            appearance.bodyParts = new PersonAppearance.BodyPart[3];

            RandomizeCostume(ref appearance, viewType, generateHead, strictViewType);
        }

        protected void RandomizeAppearanceForSportsman
            (ref PersonAppearance appearance
            , PersonAppearance.Gender gender
            , PersonAppearance.ViewType viewType
            , SportsmanType sportsmanType)
        {
            appearance.visibleMood = PersonAppearance.VisibleMood.Sad;
            appearance.gender = gender;
            appearance.race =
                (PersonAppearance.Race)UnityEngine.Random.Range(0, Enum.GetNames(typeof (PersonAppearance.Race)).Length);
            appearance.viewType = viewType;

            appearance.bodyParts = new PersonAppearance.BodyPart[3];

            RandomizeCostume(ref appearance, PersonAppearance.ViewType.Gym);
            SelectCostumeForSportClub(ref appearance, sportsmanType);
        }

        protected void RandomizeCostume
            ( ref PersonAppearance appearance
            , PersonAppearance.ViewType viewType
            , bool generateHead = true
            , bool strictViewType = false)
        {
            Assert.IsTrue(appearance.bodyParts.Length >=
                          Enum.GetNames(typeof (PersonAppearance.BodyPart.PartType)).Length);

            var head = new PersonAppearance.BodyPart();
            var torso = new PersonAppearance.BodyPart();
            var legs = new PersonAppearance.BodyPart();

            var gender = appearance.gender;
            var race = appearance.race;

            var costumesCandidates = (from costume in DataTables.instance.costumes
                where ((strictViewType == false && costume.anyViewType) || costume.viewType == viewType) &&
                      costume.gender == gender &&
                      (costume.anyRace || costume.race == race)
                select costume).ToArray();  // Better to get array once and then query from it again multiple times. 

            if (costumesCandidates != null && costumesCandidates.Length > 0)
            {
                if (generateHead)
                {
                    var headsCostumes = (from costume in costumesCandidates
                        where costume.partType == PersonAppearance.BodyPart.PartType.Head
                        select costume).ToArray();

                    if (headsCostumes != null && headsCostumes.Length > 0)
                    {
                        head.meshIdx = headsCostumes.ElementAt(UnityEngine.Random.Range(0, headsCostumes.Length)).meshId;
                    }
                }

                {
                    var torsoCostumes = (from costume in costumesCandidates
                        where costume.partType == PersonAppearance.BodyPart.PartType.Torso
                        select costume).ToArray();

                    if (torsoCostumes != null && torsoCostumes.Length > 0)
                    {
                        torso.clothesIdx = torsoCostumes.ElementAt(UnityEngine.Random.Range(0, torsoCostumes.Length)).textureId;
                    }
                }

                {
                    var legsCostumes = (from costume in costumesCandidates
                        where costume.partType == PersonAppearance.BodyPart.PartType.Legs
                        select costume).ToArray();

                    if (legsCostumes != null && legsCostumes.Length > 0)
                    {
                        legs.clothesIdx = legsCostumes.ElementAt(UnityEngine.Random.Range(0, legsCostumes.Length)).textureId;
                    }
                }
            }

            appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Head] = head;
            appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Torso] = torso;
            appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Legs] = legs;
        }

        protected void SelectCostumeForSportClub(ref PersonAppearance appearance, SportsmanType sportsmanType)
        {
            var gender = appearance.gender;
            var torsoCostume = DataTables.instance.sportCostumes.FirstOrDefault
                (  i =>
                    i.sportsmanType == sportsmanType && i.gender == gender &&
                    i.bodyPart == PersonAppearance.BodyPart.PartType.Torso);
            
            var legsCostume = DataTables.instance.sportCostumes.FirstOrDefault
                (  i =>
                    i.sportsmanType == sportsmanType && i.gender == gender &&
                    i.bodyPart == PersonAppearance.BodyPart.PartType.Legs);

            if (torsoCostume != null)
            {
                appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Torso].clothesIdx = torsoCostume.costumeId;
            }
            else
            {
                Debug.LogErrorFormat
                    ( "Can't find sport costume for {sportsmanType={0}; gender={1}; bodyPart=Torso}, skip setting appearance for this"
                    , sportsmanType, gender);
            }

            if (legsCostume != null)
            {
                appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Legs].clothesIdx = legsCostume.costumeId;
            }
            else
            {
                Debug.LogErrorFormat
                    ( "Can't find sport costume for {sportsmanType={0}; gender={1}; bodyPart=Legs}, skip setting appearance for this"
                    ,  sportsmanType, gender);
            }
        }

        protected void SelectAppearanceForTrainer (ref PersonAppearance appearance, int viewTypeIndex)
        {
            appearance.visibleMood = PersonAppearance.VisibleMood.Good;
            appearance.bodyType = PersonAppearance.BodyType.Strong;
            appearance.viewType = PersonAppearance.ViewType.Trainer;

            appearance.bodyParts = new PersonAppearance.BodyPart[3];

            var head = new PersonAppearance.BodyPart();
            var torso = new PersonAppearance.BodyPart();
            var legs = new PersonAppearance.BodyPart();

            {
                var headPartCostume = (from costume in DataTables.instance.costumes
                    where costume.viewType == PersonAppearance.ViewType.Trainer &&
                          costume.partType == PersonAppearance.BodyPart.PartType.Head &&
                          costume.viewTypeIndex == viewTypeIndex
                    select costume).FirstOrDefault();

                if (headPartCostume != null)
                {
                    appearance.gender = headPartCostume.gender;
                    appearance.race = headPartCostume.race;

                    head.meshIdx = headPartCostume.meshId;
                }
                else
                {
                    Debug.LogErrorFormat("Cannot customize head body part for a trainer with View Type Index: {0}.", viewTypeIndex);
                }
            }

            {
                var torsoPartCostume = (from costume in DataTables.instance.costumes
                    where costume.viewType == PersonAppearance.ViewType.Trainer &&
                          costume.partType == PersonAppearance.BodyPart.PartType.Torso &&
                          costume.viewTypeIndex == viewTypeIndex
                    select costume).FirstOrDefault();

                if (torsoPartCostume != null)
                {
                    torso.clothesIdx = torsoPartCostume.textureId;
                }
                else
                {
                    Debug.LogErrorFormat("Cannot customize torso body part for a trainer with View Type Index: {0}.", viewTypeIndex);
                }
            }

            {
                var legsPartCostume = (from costume in DataTables.instance.costumes
                    where costume.viewType == PersonAppearance.ViewType.Trainer &&
                          costume.partType == PersonAppearance.BodyPart.PartType.Legs &&
                          costume.viewTypeIndex == viewTypeIndex
                    select costume).FirstOrDefault();

                if (legsPartCostume != null)
                {
                    legs.clothesIdx = legsPartCostume.textureId;
                }
                else
                {
                    Debug.LogErrorFormat("Cannot customize legs body part for a trainer with View Type Index: {0}.", viewTypeIndex);
                }
            }

            appearance.bodyParts[(int)PersonAppearance.BodyPart.PartType.Head] = head;
            appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Torso] = torso;
            appearance.bodyParts[(int) PersonAppearance.BodyPart.PartType.Legs] = legs;
        }
        #endregion

        #region Person events proxy functions
        private void OnSetSelectionEvent(Person person, bool selected, bool focus)
        {
            Assert.IsNotNull(person);
            CharacterSelectionEvent?.Invoke(person.id, selected, focus);
        }

        private void OnCharacterStateChangedEvent(Person person, PersonState oldState, PersonState newState)
        {
            Assert.IsNotNull(person);
            CharacterStateChangedEvent?.Invoke(person.id, oldState, newState);
        }

        private void OnCharacterBirthEvent(Person person, bool restored, CharacterDestination spawnDestination)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
                presentationCharacterManager.CreateCharacterRequest(person.id, person, spawnDestination);
        }

        private void OnCharacterDeathEvent(int id)
        {
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.DestroyCharacterRequest(id);
            }
        }

        private void OnCharacterEnteredRoomEvent(Person person)
        {
            Assert.IsNotNull(person);
            CharacterEnteredRoomEvent?.Invoke(person.id);
        }

        private void OnCharacterLeavingRoomEvent(Person person, bool kicked)
        {
            Assert.IsNotNull(person);
            CharacterLeavingRoomEvent?.Invoke(person.id, kicked);
        }

        private void OnCharacterTypeChangedEvent (Person person)
        {
            Assert.IsNotNull(person);
            CharacterTypeChangedEvent?.Invoke(person.id, person);
        }

        private void OnCharacterSentencedEvent (Person person)
        {
            Assert.IsNotNull(person);

            var id = person.id;

            Assert.IsFalse(sentencedPersons.Contains(id));

            if (!sentencedPersons.Contains(id))
                sentencedPersons.Add(id);
        }

        private void OnCharacterTransferEvent (Person person, string newRoomName)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.TransferCharacterRequest(person.id, newRoomName);
            }
        }

        private void OnMoveCharacterEvent(Person person, CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo,
            Action<int, MovementResult> onComplete)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.MoveCharacterRequest(person.id, destination, movementSettingsInfo, onComplete);
            }
        }

        private void OnStopCharacterEvent(Person person)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.StopCharacterRequest(person.id);
            }
        }

        private void OnChangeCharacterMovementModeEvent(Person person, MovementMode mode)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.ChangeCharacterMovementModeRequest(person.id, mode);
            }
        }

        private void OnCharacterSchedulePendingExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            CharacterSchedulePendingExerciseEvent?.Invoke(person.id, equipment.id, exercise.id, initiator, duration);
        }

        private void OnCharacterSwitchPendingExerciseEvent(Person person, Exercise exercise, TimeSpan duration)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(exercise);
            CharacterSwitchPendingExerciseEvent?.Invoke(person.id, exercise.id, duration);
        }

        private void OnCharacterCancelPendingExerciseEvent(Person person, Equipment equipment)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            CharacterCancelPendingExerciseEvent?.Invoke(person.id, equipment.id);
        }

        private void OnCharacterStartExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.StartExerciseRequest
                    (person.id, equipment.id, exercise.tier, exercise.characterAnimation, initiator, duration, flags, speed);

                PersistentCore.instance.analytics.DetailGameEvent
                    ( "Start Exercise"
                    , new GameEventParam("Equipment Name"
                    , equipment.estateType.locNameId)
                    , new GameEventParam("Exercise Name"
                    , exercise.locNameId));
            }
        }

        private void OnCharacterSwitchExerciseEvent (Person person, Exercise exercise, float speed)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(exercise);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.SwitchExerciseRequest
                    (person.id, exercise.tier, exercise.characterAnimation, speed);


                PersistentCore.instance.analytics.DetailGameEvent("Switch Exercise",  new GameEventParam("Exercise Name", exercise.locNameId));
            }
        }

        void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.EndExerciseRequest(person.id, equipment.id, exercise, initiator, flags);
            }
        }

        void OnPlayEmoteAnimationEvent(Person person, AnimationSocial animationId, int loopCount,
            bool rotateTowardsCamera, Action onComplete)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.PlayEmoteAnimationRequest
                    (person.id, animationId, loopCount, rotateTowardsCamera, onComplete);
            }
        }

        void OnCharacterLookAtEvent(Person person, CharacterDestination lookAtDestination, Action onComplete)
        {
            Assert.IsNotNull(person);
            if (presentationCharacterManager != null)
            {
                presentationCharacterManager.PersonLookAtRequest(person.id, lookAtDestination, onComplete);
            }
        }

        void OnCharacterRefusedExerciseEvent(Person person, Equipment equipment, Exercise exercise)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);
            CharacterRefusedExerciseEvent?.Invoke(person.id, equipment.id, exercise.id);
        }

        void OnCharacterReadyToSkipExerciseEvent(Person person, Equipment equipment, Exercise exercise)
        {
            Assert.IsNotNull(person);
            Assert.IsNotNull(equipment);
            Assert.IsNotNull(exercise);

            if(!(person is PersonTrainerClient))
                CharacterReadyToSkipExerciseEvent?.Invoke(person.id, equipment.id, exercise.id);
        }

        void OnCharacterAppearanceChangedEvent(Person person)
        {
            Assert.IsNotNull(person);
            CharacterAppearanceChangedEvent?.Invoke(person.id);
        }

        void OnSportsmanTypeChangedEvent(PersonSportsman sportsman, SportsmanType newSportsmanType)
        {
            Assert.IsNotNull(sportsman);
            SportsmanTypeChangedEvent?.Invoke(sportsman.id, newSportsmanType);
        }

        void OnCharacterLevelUpEvent(Person person, int newLevel)
        {
            Assert.IsNotNull(person);
            CharacterLevelUpEvent?.Invoke(person.id, newLevel);
        }

        #endregion
    }
}