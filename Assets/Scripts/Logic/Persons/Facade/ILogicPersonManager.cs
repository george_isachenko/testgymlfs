﻿using System;
using Data.Person;
using Data.Person.Info;
using Data.Quests;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Facades;

namespace Logic.Persons.Facade
{
    public delegate void CharacterSelectionEvent(int id, bool selected, bool focus);
    public delegate void CharacterStateChangedEvent(int id, PersonState oldState, PersonState newState);
    public delegate void CharacterEnteredRoomEvent(int id);
    public delegate void CharacterLeavingRoomEvent(int id, bool kicked);
    public delegate void CharacterTypeChangedEvent(int id, IPersonInfo personInfo);
    public delegate void CharacterRefusedExerciseEvent(int id, int equipmentId, int exerciseId);
    public delegate void CharacterReadySkipExerciseEvent(int id, int equipmentId, int exerciseId);
    public delegate void CharacterSchedulePendingExerciseEvent(int id, int equipmentId, int exerciseId, ExerciseInitiator initiator, TimeSpan duration);
    public delegate void CharacterSwitchPendingExerciseEvent(int id, int exerciseId, TimeSpan duration);
    public delegate void CharacterCancelPendingExerciseEvent(int id, int equipmentId);
    public delegate void CharacterAppearanceChangedEvent(int id);
    public delegate void SportsmanTypeChangedEvent(int id, SportsmanType newSportsmanType);
    public delegate void CharacterLevelUpEvent(int id, int newLevel);

    public interface ILogicPersonManager
    {
        bool hasSelection
        {
            get;
        }

        int selectedCharacterId
        {
            get;
        }

        LogicSportsmen   sportsmen   { get; }

        event CharacterSelectionEvent CharacterSelectionEvent;
        event CharacterStateChangedEvent CharacterStateChangedEvent;
        event CharacterEnteredRoomEvent CharacterEnteredRoomEvent;
        event CharacterLeavingRoomEvent CharacterLeavingRoomEvent;
        event CharacterRefusedExerciseEvent CharacterRefusedExerciseEvent;
        event CharacterReadySkipExerciseEvent CharacterReadyToSkipExerciseEvent;
        event CharacterSchedulePendingExerciseEvent CharacterSchedulePendingExerciseEvent;
        event CharacterSwitchPendingExerciseEvent CharacterSwitchPendingExerciseEvent;
        event CharacterCancelPendingExerciseEvent CharacterCancelPendingExerciseEvent;
        event CharacterAppearanceChangedEvent CharacterAppearanceChangedEvent;
        event SportsmanTypeChangedEvent SportsmanTypeChangedEvent;
        event CharacterTypeChangedEvent CharacterTypeChangedEvent;
        event CharacterLevelUpEvent CharacterLevelUpEvent;

        int GetCharactersCount(Type personType = null, string roomName = null, params PersonState[] states);
        bool SelectCharacterRequest(int id);
        void DeselectCharacterIfSelectedRequest(int id);
        bool MoveCharacterRequest(int id, RoomCoordinates coords);
        QuestStage? GetQuestStage(int id);
        VisitorQuestObjectiveData GetVisitorQuestCurrentObjectiveData(int id);
        void KillCharacterRequest(int id);
        bool KickRequest(int id);
        bool ClaimQuestRequest(int id);
        void SkipExerciseRequest(int id);
        void SetCharactersSelectionMode(bool allowSelection);
        void SetPersonName(int id, string name);
    }
} 
