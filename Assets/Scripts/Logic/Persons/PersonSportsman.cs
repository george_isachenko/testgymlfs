﻿using System;
using System.Linq;
using Data;
using Data.Estate;
using Data.Person;
using Data.Person.Info;
using Data.Room;
using Data.Sport;
using Logic.Estate;
using Logic.Persons.Events;
using Logic.Persons.Tasks;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Persons
{
    public class PersonSportsman : Person,
        IPersonSportsmanInfo
    {
        #region Static/readonly data.
        private static readonly string sportsmanSpawnRoomZoneName    = "sportsman_spawn";
        private static readonly string sportsmanDespawnRoomZoneName  = "sportsman_despawn";
        private static readonly string sportsmanLobbyRoomZoneName    = "sportsman_lobby";
        // private static readonly string sportsmanQueueZoneNamePrefix    = "sportsman_queue_";
        private static readonly string sportsmanIdleZoneName         = "sportsman_idle";

        #endregion

        #region Public virtual properties.
        public override bool canSetTask
        {
            get
            {
                // Sportsmen can go directly to work from Entering state.
                return (data.state == PersonState.Entering || data.state == PersonState.Idle || data.state == PersonState.ExerciseComplete) && (task == null || task.interruptable);
            }
        }

        protected new PersonSportsmanData data { get { return (PersonSportsmanData)base.data; } }
        #endregion

        #region Properties.

        public SportsmanType sportsmanType
        {
            get { return data.sportsmanType; }
        }

        public int currentSportExerciseId
        {
            get { return data.currentSportExerciseId; }
        }

        public SportsmanType currentOrNextSportsmanType
        {
            get
            {
                if (isTraining && currentSportExerciseId != SportExercise.InvalidId)
                {
                    var sportExercise = DataTables.instance.sportExercises.FirstOrDefault (se => se.id == currentSportExerciseId);
                    Assert.IsNotNull(sportExercise);
                    if (sportExercise != null)
                    {
                        return sportExercise.trainTo;
                    }
                }

                return sportsmanType;
            }
        }

        public override string spawnRoomZoneName
        {
            get { return sportsmanSpawnRoomZoneName; }
        }

        public override string despawnRoomZoneName
        {
            get { return sportsmanDespawnRoomZoneName; }
        }

        public override string lobbyRoomZoneName
        {
            get { return sportsmanLobbyRoomZoneName; }
        }

        public override string idleZoneName
        {
            get { return sportsmanIdleZoneName; }
        }

        /*
                public override string queueZoneNamePrefix
                {
                    get
                    {
                        return null; // Queue for spoprtmen is not supported yet.
                        // return sportsmanQueueZoneNamePrefix;
                    }
                }
        */
        #endregion

        #region Private data.
        bool typeChanged = false;
        #endregion

        #region Constructors.
        public PersonSportsman(IPersonManager personManager, PersonSportsmanData personSportsmanData)
            : base(personManager, personSportsmanData)
        {

        }

        public PersonSportsman(IPersonManager personManager, PersonSportsmanData personSportsmanData, Person convertFrom)
            : base(personManager, personSportsmanData, convertFrom)
        {
        }
        #endregion

        #region Public static API.
        public static Person Instantiate (IPersonManager personManager, PersonData personData, bool strict)
        {
            if (personData.GetType() == typeof(PersonSportsmanData)) // Exact match needed.
            {
                var personSportsmanData = personData as PersonSportsmanData;

                return new PersonSportsman(personManager, personSportsmanData);
            }

            return null;
        }
        #endregion

        #region Public virtual API.
        public override void OnPostCreate(EntranceMode entranceMode)
        {
            SetTask(new TaskIdle());

            base.OnPostCreate(entranceMode);
        }

        public override void OnLoaded()
        {
            SetTask(new TaskIdle());
        }

        public override void EnterRoom (EntranceMode entranceMode)
        {
            ChangeState(PersonState.Entering);

            var activatorTask = new TaskDelegate
                ( new TaskDelegate.Settings
                {
                    isInterruptable = (task) => true,
                    onAborted = (task) =>
                    {
                        // Debug.LogFormat(">>> FORCE Activating {0} ({1})", id, data.name);

                        ChangeState(PersonState.Idle);
                        CharacterEnteredRoomEvent.Send(task.person);
                    },
                    onActivated = (task) =>
                    {
                        // Debug.LogFormat(">>> Activating {0} ({1})", id, data.name);

                        ChangeState(PersonState.Idle);
                        CharacterEnteredRoomEvent.Send(task.person);
                    }
                });

            activatorTask.AddTask
                ( this
                , new TaskGoto
                    ( new CharacterDestination(lobbyRoomZoneName)
                    , DataTables.instance.balanceData.Characters.Movement.EnteringRoom
                    , (entranceMode == EntranceMode.ImmediateEntranceFast) ? MovementMode.Running : MovementMode.WalkingFast
                    , false)
                );

            SetTask(activatorTask);
        }

        public override bool CanUseEquipment(Equipment equipment, int tier)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif
            return true;

            //return visitorQuest.CanUseEquipment(equipment.estateType, tier);
        }

        public override bool CanUseEquipment(Equipment equipment)
        {
#if DEBUG
            if (DebugHacks.visitorCanUseAnyEquipment)
                return true;
#endif
            return true;

            //return visitorQuest.CanUseEquipment(equipment.estateType, tier);
        }

        public override void OnStateChanged(PersonState oldState, PersonState newState)
        {
            base.OnStateChanged(oldState, newState);

            if (newState != PersonState.Idle)
            {
                personManager.DeselectCharacterIfSelectedRequest(id);
            }

            if (newState == PersonState.Idle)
            {
                if (oldState == PersonState.Entering)
                {
                    personManager.UpdateSportsmanAppearance(ref data.appearance, sportsmanType);
                    CharacterAppearanceChangedEvent.Send(this);
                }
            }
            else if (newState == PersonState.ExerciseComplete)
            {
                if (typeChanged)
                {
                    typeChanged = false;
                    SportsmanTypeChangedEvent.Send(this, sportsmanType);
                    CharacterAppearanceChangedEvent.Send(this);
                }
            }

/*
            if (newState == PersonState.Leaving)
            {
                LeaveRoom();
            }
*/

            personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        public override bool SelectRequest()
        {
            return isSelectable;
        }

        public override void OnSelected()
        {
            base.OnSelected();

            if (data.state == PersonState.ExerciseComplete)
            {
                ChangeState(PersonState.Idle);
            }
        }

        public override void Quit()
        {
            LeaveRoom();
        }

        public override bool AboutToEndExercise(ExerciseFlags flags)
        {
            Assert.IsFalse(typeChanged);
            Assert.IsTrue(currentSportExerciseId != SportExercise.InvalidId);
            if (currentSportExerciseId != SportExercise.InvalidId)
            {
                var sportExercise = DataTables.instance.sportExercises.FirstOrDefault
                    (se => se.id == currentSportExerciseId);

                Assert.IsNotNull(sportExercise);
                if (sportExercise != null && sportExercise.trainTo != sportsmanType)
                {
                    /*
                    PersistentCore.instance.analytics.SpendMoneyEvent("Sportsmen",
                        1,
                        data.roomName,
                        sportsmanType.ToString());
                    PersistentCore.instance.analytics.AddMoneyEvent("Sportsmen",
                        1,
                        data.roomName,
                        sportExercise.trainTo.ToString());
                    */
                    data.sportsmanType = sportExercise.trainTo;
                    data.currentSportExerciseId = SportExercise.InvalidId;

                    personManager.UpdateSportsmanAppearance(ref data.appearance, sportsmanType);
                    typeChanged = true;

                    // It's continued in OnStateChanged, see here.
                }
            }

            return true; // True - allow ending. False - another exercise rescheduled.
        }
        #endregion

        #region Public API.
        public bool TryToUseEquipment
            ( Equipment equipment
            , Exercise exercise
            , SportExercise sportExercise
            , TimeSpan? customDuration = null)
        {
            Assert.IsNotNull(sportExercise);
            Assert.IsTrue(data.currentSportExerciseId == SportExercise.InvalidId);

            if (state == PersonState.Entering)
            {
                if (canSetTask)
                {
                    ResetTasks();
                }
                else
                {
                    return false;
                }
            }

            if (base.TryToUseEquipment
                    ( equipment
                    , exercise
                    , ExerciseInitiator.Player
                    , customDuration != null ? customDuration : sportExercise.duration))
            {
                data.currentSportExerciseId = sportExercise.id;

                personManager.gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                return true;
            }
            return false;
        }
        #endregion

        #region Protected API.
        #endregion
    }
}
