﻿using System;
using Core;
using Core.Timer;
using Data;
using Data.PlayerProfile;
using Core.Analytics;

namespace Logic.PlayerProfile.Facade
{
    public delegate void GameWillRestartEvent(GameMode mode, DemoLevelSource demoLevelSource);
    public delegate void PostLoadEvent();
    public delegate void AddExpEvent(int valueDelta, bool useFx);
    public delegate void AddCoinsEvent(int valueDelta, bool useFx);
    public delegate void AddFitBucksEvent(int valueDelta, bool useFx);
    public delegate void LevelUpEvent();
    public delegate void PlayerProfileUpdatedEvent();
    public delegate void NotEnoughMoneyEvent(Cost price, bool spendBucksIfNecessary); 

    public interface ILogicPlayerProfile
    {
        #region Events.
        event GameWillRestartEvent onGameWillRestartEvent;
        event PostLoadEvent onPostLoadEvent;
        event AddExpEvent onAddExpEvent;
        event AddCoinsEvent onAddCoinsEvent;
        event AddFitBucksEvent onAddFitBucksEvent;
        event LevelUpEvent onLevelUpEvent;
        event PlayerProfileUpdatedEvent onPlayerProfileUpdatedEvent;
        event NotEnoughMoneyEvent onNotEnoughMoneyEvent;
        #endregion

        #region Properties
        int                 coins               { get; }
        int                 fitBucks            { get; }
        int                 level               { get; }
        Cost                levelUpReward       { get; }
        int                 experience          { get; }
        string              profileExpTxt       { get; }
        float               profileExpProgress  { get; }
        ExperienceLevels    expLevels           { get; }
        TimerFloat          fatigue             { get; }
        LogicStorage        storage             { get; }
        bool                alreadyRateUs       { get; set;}
        bool                friendsAvailable    { get; }
        #endregion

        #region Public API.
        bool IsAnyTutorial();
        void AddReward(Cost price, Analytics.MoneyInfo info, bool forceResourceAdd, bool fx = false);
        void AddCoins(int coins, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false);
        void AddFitBucks(int bucks, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false);
        void AddResource(ResourceType type, int value, Analytics.MoneyInfo info, bool force);
        bool AddExperience(int exp, bool fx = false);
        void AddFitPoints(int points);
        //string GetExpRatioStr(int expValue = -1);
        //float GetExpRatio(float expValue = -1.0f);
        bool CanSpend(Cost cost, bool forceFitBucks = false);
        Cost GetShortageCost(Cost cost);
        bool SpendMoney(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false);
        bool PurchaseBankItemTransaction(ShopItemData shopItemData, Func<bool> performAction, Action rollbackAction = null);
        void GooglePlayConnect();
        #endregion
    
        #region Public test/debug API.
#if DEBUG
        void PushTest();
        void DebugMoarMoney(int coins, int fitBucks);
        void DebugZeroMoney();
        void DebugIncrementLevel();
        void DebugDecrementLevel();
        void DebugIncreaseFatigue();
        void DebugDecreaseFatigue();
        void DebugAddExperienceToNextLevel();
        void DebugIncreasResource (ResourceType value);
        void DebugDecreaseResource (ResourceType value);
        void DebugDeleteDataAndStop ();
#endif
        #endregion
    }
}