﻿using System;
using BridgeBinder;
using Core;
using Core.Advertisement;
using Core.Timer;
using Data;
using Data.PlayerProfile;
using Logic.FacadesBase;
using Logic.Serialization;
using Presentation.Facades.Interfaces.PlayerProfile;
using UnityEngine;
using UnityEngine.Assertions;
using Core.Analytics;
using Logic.Quests.Tutorial;
using System.Linq;

namespace Logic.PlayerProfile.Facade
{
    [BindBridgeInterface(typeof(IPresentationPlayerProfile))]
    public abstract class LogicPlayerProfileBase
        : BaseLogicFacade
        , ILogicPlayerProfile
        , IPlayerProfileManager
        , IPersistent
    {
        #region Events
        public event GameWillRestartEvent onGameWillRestartEvent;
        public event PostLoadEvent onPostLoadEvent;
        public event AddExpEvent onAddExpEvent;
        public event AddCoinsEvent onAddCoinsEvent;
        public event AddFitBucksEvent onAddFitBucksEvent;
        public event LevelUpEvent onLevelUpEvent;
        public event PlayerProfileUpdatedEvent onPlayerProfileUpdatedEvent;
        public event NotEnoughMoneyEvent onNotEnoughMoneyEvent;
        #endregion

        #region Properties
        public int                  coins               { get { return persistentData.data.coins; } }
        public int                  fitBucks            { get { return persistentData.data.fitBucks; } }
        public int                  level               { get { return persistentData.data.level; } }
        public virtual Cost         levelUpReward       { get { return null; } }
        public int                  experience          { get { return persistentData.data.experience; } }
        private ExperienceLevels    _expLevels;
        public ExperienceLevels     expLevels           { get { return _expLevels; } }
        public string               profileExpTxt       { get { return expLevels.GetExpRatioStr(experience, level);} }
        public float                profileExpProgress  { get { return expLevels.GetExpRatio(experience, level); } }
        public TimerFloat           fatigue             { get { return persistentData.data.fatigue; } }
        public int                  fitPoints           { get { return persistentData.data.fitPoints; } }

        public LogicStorage         storage             { get; private set; }
        public virtual bool         alreadyRateUs       { get{ return false;} set{ }}
        public bool                 wasFBConnected      { get{ return persistentData.data.wasConnectToFacebook;} set{ persistentData.data.wasConnectToFacebook = value; } }
        public AdvertisementsData   adverticementsData  { get{ return persistentData.adverticementsData;} } 
        public virtual bool         friendsAvailable
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region IPersistent properties.
        string IPersistent.propertyName
        {
            get
            {
                return "PlayerProfile";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(PlayerProfilePersistentData);
            }
        }
        #endregion

        #region Public (Inspector) data
        [Tooltip("Fatigue time (in seconds).")]
        public float                fatigueTime = 20.0f * 60;
        #endregion

        #region Private data
        protected IPresentationPlayerProfile  presenter;
        #endregion

        #region Persistent data
        protected PlayerProfilePersistentData persistentData;
        #endregion

        #region Bridge
        [BindBridgeSubscribe]
        protected void Subscribe(IPresentationPlayerProfile playerProfile)
        {
            presenter = playerProfile;
        }

        [BindBridgeUnsubscribe]
        protected void Unsubscribe(IPresentationPlayerProfile playerProfile)
        {
            presenter = null;
        }
        #endregion

        #region Unity API
        protected void Awake()
        {
            _expLevels = new ExperienceLevels(DataTables.instance.experienceLevels);
                
            persistentData.data = new PlayerProfileData(fatigueTime);
            persistentData.storageData = new StorageData();

            storage = new LogicStorage(gameCore, persistentData.storageData);

            gameCore.onWillRestartGame += OnWillRestartGame;
        }

        protected void OnDestroy()
        {
            storage.OnDestroy();
            gameCore.onWillRestartGame -= OnWillRestartGame;
        }

        protected void OnApplicationFocus(bool focused)
        {
        }
        #endregion

        #region Public API
        public virtual bool IsAnyTutorial()
        {
            return isAnyTutorialRunning;
            //return false;
        }

        public virtual void AddReward(Cost price, Analytics.MoneyInfo info, bool forceResourceAdd, bool fx = false)
        {
        }

        public virtual void AddCoins(int coins, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false)
        {
        }

        public virtual void AddFitBucks(int bucks, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false)
        {
        }

        public virtual void AddResource(ResourceType type, int value, Analytics.MoneyInfo info, bool force)
        {
        }

        public virtual bool AddExperience(int exp, bool fx = false)
        {
            return false;
        }

        public virtual void AddFitPoints(int points)
        {
        }

        public virtual int GetFitPointsCount()
        {
            return fitPoints;
        }

        /*
        public virtual string GetExpRatioStr(int expValue_ = -1)
        {
            return string.Empty;
        }

        public virtual float GetExpRatio(float expValue_ = -1.0f)
        {
            return 1.0f;
        }
        */

        public virtual bool CanSpend(Cost cost, bool forceFitBucks = false)
        {
            return false;
        }

        public virtual Cost GetShortageCost(Cost cost)
        {
            Assert.IsNotNull(cost);

            switch (cost.type)
            {
                case Cost.CostType.CoinsOnly:
                {
                    if (cost.value > persistentData.data.coins)
                    {
                        return new Cost(Cost.CostType.CoinsOnly, cost.value - persistentData.data.coins, cost.level);
                    }
                }; break;

                case Cost.CostType.BucksOnly:
                {
                    if (cost.value > persistentData.data.fitBucks)
                    {
                        return new Cost(Cost.CostType.BucksOnly, cost.value - persistentData.data.fitBucks, cost.level);
                    }
                }; break;

                case Cost.CostType.ResourcesOnly:
                {
                    var result = new Cost (cost.type, 0, cost.level);

                    result.resources = storage.GetResourcesShortage(cost.resources);

                    if (result.resources.Count > 0)
                        return result;
                }; break;

                case Cost.CostType.ResourcesAndCoins:
                {
                    var result = new Cost (cost.type, 0, cost.level);

                    if (cost.value > persistentData.data.coins)
                    {
                        result.value = cost.value - persistentData.data.coins;
                    }

                    result.resources = storage.GetResourcesShortage(cost.resources);

                    return result;
                }; 
            }

            return null; // No shortage.
        }

        public virtual bool SpendMoney(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false)
        {
            return false;
        }

        public virtual bool PurchaseBankItemTransaction(ShopItemData shopItemData, Func<bool> performAction, Action rollbackAction = null)
        {
            return false;
        }

        public virtual void GooglePlayConnect()
        {
        }
        #endregion

        #region Public test/debug API.
#if DEBUG
        public void PushTest()
        {
/*
            UM_NotificationController.Instance.ShowNotificationPoup("Hello", "Notification popup test");

            int NotificationId = UM_NotificationController.Instance.ScheduleLocalNotification("Hello Local", "Local Notification Example", 15);
*/
        }

        public virtual void DebugMoarMoney(int coins, int fitBucks)
        {
        }

        public virtual void DebugZeroMoney()
        {
        }

        public virtual void DebugIncrementLevel()
        {
        }

        public virtual void DebugDecrementLevel()
        {
        }

        public virtual void DebugIncreaseFatigue()
        {
        }

        public virtual void DebugDecreaseFatigue()
        {
        }

        public virtual void DebugAddExperienceToNextLevel()
        {
        }

        public virtual void DebugIncreasResource (ResourceType value)
        {
        }

        public virtual void DebugDecreaseResource (ResourceType value)
        {
        }

        void ILogicPlayerProfile.DebugDeleteDataAndStop ()
        {
            gameCore.DebugDeleteDataAndStop();
        }

#endif
        #endregion

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {
        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PlayerProfilePersistentData);
            if (obj is PlayerProfilePersistentData)
            {
                persistentData = (PlayerProfilePersistentData)obj;
                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            persistentData.adverticementsData = Advertisements.instance.data;
            return persistentData;
        }

        void IPersistent.OnPreLoad()
        {
            persistentData.data = new PlayerProfileData(fatigueTime);
            persistentData.storageData = new StorageData();
        }

        void IPersistent.OnLoaded(bool success)
        {
            if (!success)
            {
                persistentData.data = new PlayerProfileData(fatigueTime);
                persistentData.storageData = new StorageData();
            }
            else
            {
                if (persistentData.storageData == null)
                    persistentData.storageData = new StorageData();
            }
            storage.Reinit(persistentData.storageData);
        }

        public virtual void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (fatigue.secondsTotal != fatigueTime)
            {
                persistentData.data.fatigue = new TimerFloat(fatigueTime);
            }
            else
            {
                if (timePassedSinceSave.TotalSeconds < fatigueTime)
                    fatigue.FastForward(-(float)timePassedSinceSave.TotalSeconds);
                else
                    fatigue.FastForward(-fatigueTime);
            }

            storage.PostInit();
            storage.CheckDataIntegrity();

            Events.PlayerProfileUpdatedEvent.Send();

            if(persistentData.adverticementsData != null)
                persistentData.adverticementsData.SpendTime((float)timePassedSinceSave.TotalSeconds);

            onPostLoadEvent?.Invoke();
        }

        public virtual void OnLoadSkipped()
        {
            storage.Reinit(persistentData.storageData);
            storage.PostInit();

            Events.PlayerProfileUpdatedEvent.Send();
        }
        #endregion

        #region Protected API.
        protected void SendPlayerProfileUpdatedEvent()
        {
            onPlayerProfileUpdatedEvent?.Invoke();
        }

        protected void SendAddExpEvent(int valueDelta, bool useFx)
        {
            onAddExpEvent?.Invoke(valueDelta, useFx);
        }

        protected void SendAddCoinsEvent(int valueDelta, bool useFx)
        {
            onAddCoinsEvent?.Invoke(valueDelta, useFx);
        }

        protected void SendAddFitBucksEvent(int valueDelta, bool useFx)
        {
            onAddFitBucksEvent?.Invoke(valueDelta, useFx);
        }

        protected void SendLevelUpEvent()
        {
            // Debug.Log(">>> SendLevelUpEvent!");

            Events.LevelUpEvent.Send(persistentData.data.level);

            onLevelUpEvent?.Invoke();
        }

        public void SendNotEnoughMoneyEvent(Cost price, bool useFitbucks)
        {
            onNotEnoughMoneyEvent?.Invoke(price, useFitbucks);
        }

        protected ResourceType StrToResource(string value)
        {
            var resources = Enum.GetValues( typeof( ResourceType ) ) as ResourceType[];

            foreach (var res in resources)
            {
                if (res.ToString() == value)
                {
                    return res;
                }
            }

            return ResourceType.None;
        }
        #endregion

        #region Private functions.
        private void OnWillRestartGame (GameMode mode, DemoLevelSource demoLevelSource)
        {
            onGameWillRestartEvent?.Invoke(mode, demoLevelSource);
        }
        #endregion
    }
}