﻿using UnityEngine;

namespace Logic.PlayerProfile.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Player Profile/Facade/Logic Player Profile Demo")]
    public class LogicPlayerProfileDemo : LogicPlayerProfileBase
    {
        #region Events
        #endregion

        #region Properties
        #endregion

        #region Public (Inspector) data
        #endregion

        #region Private data
        #endregion

        #region Persistent data
        #endregion

        #region Bridge
        #endregion

        #region Unity API
        #endregion

        #region Public API
        #endregion

        #region Public test/debug API.
#if DEBUG
#endif
        #endregion

        #region Player Profile events proxy functions.
        #endregion

        #region IPersistent API
        #endregion

        #region Private API
        #endregion
    }
}