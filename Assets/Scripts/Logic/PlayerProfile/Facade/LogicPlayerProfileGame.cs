﻿using System;
using Core;
using Data;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Data.PlayerProfile;
using View;
using Core.Analytics;
using System.Linq;
using Core.Advertisement;
using Core.SocialAPI;
using Logic.Quests.Tutorial;

namespace Logic.PlayerProfile.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Player Profile/Facade/Logic Player Profile Game")]
    public class LogicPlayerProfileGame : LogicPlayerProfileBase
    {
        #region Properties
        public override Cost    levelUpReward           { get { return levelUpsReward[persistentData.data.level - 1]; } }
        public override bool    alreadyRateUs
        {
            get
            {
                return persistentData.data.alreadyRateUs;
            }
            set
            {
                persistentData.data.alreadyRateUs = value;
            }
        }
        public override bool     friendsAvailable
        {
            get
            {
                return gameCore.quests.tutorial.IsTutorialRunning<TutorSquenceFriends>() ||
                        gameCore.quests.tutorial.IsTutorialFinished<TutorSquenceFriends>();
            }
        }
        #endregion

        #region Public (Inspector) data
        #endregion

        #region Private data
        //protected int[]             levelUpsData;
        protected Cost[]            levelUpsReward;
        #endregion

        #region Unity API
        protected new void Awake()
        {
            base.Awake();

            Events.PlayerProfileUpdatedEvent.Event += OnPlayerProfileUpdatedEvent;

            var coinsRewards = DataTables.instance.playerLevels
                .Where(x => x.ContainsKey("reward"))
                .Select(x => x.GetInt("reward"))
                .ToArray();
            levelUpsReward = new Cost[coinsRewards.Length];

            for (int i = 0; i < coinsRewards.Length; i++)
            {
                var row = DataTables.instance.playerLevels[i];

                levelUpsReward[i] = new Cost();
                levelUpsReward[i].value = coinsRewards[i];
                levelUpsReward[i].AddResource( StrToResource( row.GetString("resource_1") ) );
                levelUpsReward[i].AddResource( StrToResource( row.GetString("resource_2") ) );

                if (levelUpsReward[i].resources.Count > 0)
                    levelUpsReward[i].type = Cost.CostType.ResourcesAndCoins;
                else
                    levelUpsReward[i].type = Cost.CostType.CoinsOnly;
            }
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();
            Events.PlayerProfileUpdatedEvent.Event -= OnPlayerProfileUpdatedEvent;
        }

        protected new void OnApplicationFocus(bool focused)
        {
            base.OnApplicationFocus(focused);

            if (!focused)
            {
                gameCore.ImmediateSaveTransaction(true, null);
            }
        }
        #endregion

        #region Public API
        public override void AddReward(Cost price, Analytics.MoneyInfo info, bool forceResourceAdd, bool fx = false)
        {
            switch (price.type)
            {
                case Cost.CostType.CoinsOnly:
                    {
                        AddCoins(price.value, info, fx);
                    }; 
                    break;
                    
                case Cost.CostType.BucksOnly:
                    {
                        AddFitBucks(price.value, info, fx);
                    }; 
                    break;
                    
                case Cost.CostType.ResourcesOnly:
                    {
                        foreach (var res in price.resources)
                            AddResource(res.Key, res.Value, info, forceResourceAdd);
                    }; 
                    break;

                case Cost.CostType.ResourcesAndCoins:
                    {
                        AddCoins(price.value, info, fx);

                        foreach (var res in price.resources)
                            AddResource(res.Key, res.Value, info, forceResourceAdd);
                    };
                    break;

                    default:
                    {
                        Debug.LogError("AddMoney wrong price");
                    }; break;
            }
        }

        public override void AddCoins(int coins, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false)
        {
            if (coins > 0)
            {
                persistentData.data.coins += coins;

                Achievements.EarnCoins.Send(coins);

                SendAddCoinsEvent(coins, fx);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                if(info != null)
                    PersistentCore.instance.analytics.AddMoneyEvent("Gold", coins, info.itemType, info.itemId, isInApp);

                Events.PlayerProfileUpdatedEvent.Send();
            }
            else if (coins < 0)
                Debug.LogError("Can't add negative value of coins");
        }

        public override void AddFitBucks(int bucks, Analytics.MoneyInfo info, bool fx = false, bool isInApp = false)
        {
            if (bucks > 0)
            {
                persistentData.data.fitBucks += bucks;

                SendAddFitBucksEvent(bucks, fx);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                if(info != null)
                    PersistentCore.instance.analytics.AddMoneyEvent("FitBucks", bucks, info.itemType, info.itemId, isInApp);

                Events.PlayerProfileUpdatedEvent.Send();
            }
            else if (bucks < 0)
                Debug.LogError("Can't add negative value of bucks");
        }

        public override void AddResource(ResourceType type, int value, Analytics.MoneyInfo info, bool force)
        {
            if (type == ResourceType.None)
            {
                Debug.LogError("Can't add resource None");
                return;
            }
            
            storage.AddResource(type, value, force, info);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override bool AddExperience(int exp, bool fx = false)
        {
            if (exp > 0 && level < expLevels.maxLevel)
            {
                persistentData.data.experience += exp;

                SendAddExpEvent(exp, fx);

                var oldLevel = persistentData.data.level;
                var newLevel = expLevels.GetLevelByTable(persistentData.data.experience);

                if (newLevel > oldLevel)
                {
                    persistentData.data.level = newLevel;

                    SendLevelUpEvent();

                    OnLevelChanged();

                    if (newLevel == 2)
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Level2);//GameEvent("Tutor1:Level Up -> 2");
                    if (newLevel == 3)
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Level3);
                    else if (newLevel == 4)
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Level4);
                    else if (newLevel == 5)
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Level5);
                    else if (newLevel == 6)
                    {
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Level6);
                        PersistentCore.instance.analytics.TutorialFinish();
                    }
                }

                PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                Events.PlayerProfileUpdatedEvent.Send();

                return true;
            }

            return false;
        }

        public override void AddFitPoints(int points)
        {
            persistentData.data.fitPoints += points;

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            Events.PlayerProfileUpdatedEvent.Send();

            ReportScore();
        }
         
        public void ReportScore()
        {
            #if UNITY_IOS
            var leaderBordID = "com.tatemgames.dreamgym2.SportPoints_1";
            #elif UNITY_ANDROID
            var leaderBordID = "CgkIgbCzsoAJEAIQMw";
            #else
            var leaderBordID = "bla-bla-bla";
            #endif

            var socialManager = PersistentCore.instance.socialManager;

            foreach (var network in socialManager.GetAvailableNetworks(NetworkFeatures.SupportsScores, true))
            {
                network.ReportScore(persistentData.data.fitPoints, leaderBordID);
            }
        }



        public override bool CanSpend(Cost cost, bool forceFitBucks = false)
        {
            if (cost.type == Cost.CostType.BucksOnly && forceFitBucks)
            {
                Debug.LogError("Can't user forceFitBucks == true for Cost.CostType.BucksOnly");
                return false;
            }

            if (forceFitBucks)
            {
                return storage.GetExtraBucksPrice(cost, null).value <= persistentData.data.fitBucks;
            }

            if (cost == null)
                return true;

            switch (cost.type)
            {
            case Cost.CostType.CoinsOnly:
                return cost.value <= persistentData.data.coins;

            case Cost.CostType.BucksOnly:
                return cost.value <= persistentData.data.fitBucks;

            case Cost.CostType.ResourcesOnly:
                return storage.CanSpendResources(cost.resources);

            case Cost.CostType.ResourcesAndCoins:
                return (cost.value <= persistentData.data.coins) && storage.CanSpendResources(cost.resources);
            }

            Debug.LogError("Wrong cost type : NONE");
            return false;
        }

        public override bool SpendMoney(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false)
        {
            Assert.IsTrue(cost.value >= 0);
            if (cost.value < 0)
                return false;

            if (!CanSpend(cost, spendBucksIfNecessary))
            {
                SendNotEnoughMoneyEvent(cost, spendBucksIfNecessary);
                
                return false;
            }

            switch (cost.type)
            {

            case Cost.CostType.CoinsOnly:
                return SpendCoins(cost, info, spendBucksIfNecessary);

            case Cost.CostType.BucksOnly:
                return SpendBucks(cost, info);

            case Cost.CostType.ResourcesOnly:
                return SpendResources(cost, spendBucksIfNecessary, info);

            case Cost.CostType.ResourcesAndCoins:
                {
                    if (SpendCoins(cost, info, spendBucksIfNecessary))
                    {
                        if (SpendResources(cost, spendBucksIfNecessary, info))
                            return true;
                        else
                            Debug.LogError("ACHTUNG, IMPOSIBULE!! can't spend resources after coins");
                    }
                    else 
                        return false;

                }
                break;

            default:
                {
                    Debug.LogErrorFormat("Unsupported Cost type: {0}", cost.type);
                }; break;
            }

            Debug.LogError("ACHTUNG, IMPOSIBULE! this place can't be reached");

            return false;
        }

        public override bool PurchaseBankItemTransaction(ShopItemData shopItemData, Func<bool> performAction, Action rollbackAction = null)
        {
            if (shopItemData.category == ShopItemCategory.BANK && shopItemData.bankItemData != null)
            {
                if (performAction == null || performAction())
                {
                    var performSaveAction = new Func<bool>(() =>
                    {
                        persistentData.data.coins += shopItemData.bankItemData.Value.coins;
                        persistentData.data.fitBucks += shopItemData.bankItemData.Value.bucks;
                        return true;
                    });

                    var rollbackSaveAction = new Action(() =>
                    {
                        persistentData.data.coins -= shopItemData.bankItemData.Value.coins;
                        persistentData.data.fitBucks -= shopItemData.bankItemData.Value.bucks;

                        if (rollbackAction != null)
                            rollbackAction();
                    });

                    if (gameCore.ImmediateSaveTransaction(false, performSaveAction, rollbackSaveAction))
                    {
                        PersistentCore.instance.analytics.GameEvent("In-app Purchase", 
                            new GameEventParam("Level", persistentData.data.level),
                            new GameEventParam("Coins Balance", persistentData.data.coins - shopItemData.bankItemData.Value.coins),
                            new GameEventParam("FitBucks Balance", persistentData.data.fitBucks - shopItemData.bankItemData.Value.bucks));

                        Events.PlayerProfileUpdatedEvent.Send();
                        return true;
                    }
                }
            }
            else
            {
                Debug.LogError("Only ShopItemData of category BANK supported.");
            }

            return false;
        }

        #endregion

        #region Public test/debug API.
#if DEBUG
        public override void DebugMoarMoney(int coins, int fitBucks)
        {
            if (coins > 0)
                AddCoins(coins, new Analytics.MoneyInfo(null, null), true);

            if (fitBucks > 0)
                AddFitBucks(fitBucks, new Analytics.MoneyInfo(null, null), true);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        public override void DebugZeroMoney()
        {
            persistentData.data.coins = 0;
            persistentData.data.fitBucks = 0;

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override void DebugIncrementLevel()
        {
            persistentData.data.level++;
            persistentData.data.experience = expLevels.ExpByLvl(persistentData.data.level - 1) + 1;

            PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            SendLevelUpEvent();
            OnLevelChanged();

            Events.PlayerProfileUpdatedEvent.Send();
            //Presenter.OnLevelUp();
        }

        public override void DebugDecrementLevel()
        {
            if (persistentData.data.level > 1)
                persistentData.data.level--;
            persistentData.data.experience = expLevels.ExpByLvl(persistentData.data.level - 1) + 1;

            PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            OnLevelChanged();

            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override void DebugIncreaseFatigue()
        {
            fatigue.dbgInc();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override void DebugDecreaseFatigue()
        {
            fatigue.dbgDec();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override void DebugAddExperienceToNextLevel()
        {
            persistentData.data.experience = expLevels.ExpByLvl(persistentData.data.level) - 1;

            PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            //OnLevelChanged();
            Events.PlayerProfileUpdatedEvent.Send();
        }

        public override void DebugIncreasResource (ResourceType resource)
        {
            BalanceData.StorageBalance.ResourceBalance  resourceBalanceInfo;
            if (DataTables.instance.balanceData.Storage.Resources.TryGetValue(resource, out resourceBalanceInfo) && !resourceBalanceInfo.disabled)
            {
/*
                persistentData.storageData.Add (resource, 1);

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                PlayerProfile.PlayerProfileUpdatedEvent.Send();
*/

                storage.AddResource ( resource, 1, false, Analytics.MoneyInfo.dummy);
            }
            else
            {
                Debug.LogWarningFormat("Resource balance info for resource type '{0}' is missing or resource type is disabled.", resource);
            }
        }

        public override void DebugDecreaseResource (ResourceType resource)
        {
            BalanceData.StorageBalance.ResourceBalance  resourceBalanceInfo;
            if (DataTables.instance.balanceData.Storage.Resources.TryGetValue(resource, out resourceBalanceInfo) && !resourceBalanceInfo.disabled)
            {
                // persistentData.storageData.Remove (resource, 1);

                storage.SpendResource( resource, null, 1);
            }
            else
            {
                Debug.LogWarningFormat("Resource balance info for resource type '{0}' is missing or resource type is disabled.", resource);
            }
        }
#endif
        #endregion

        #region Player Profile events proxy functions.
        void OnPlayerProfileUpdatedEvent()
        {
            SendPlayerProfileUpdatedEvent();
        }
        #endregion

        #region IPersistent API
        public override void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            base.OnPostLoad(timePassedSinceSave, loadTimeUTC);

            PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);
        }

        public override void OnLoadSkipped()
        {
            //PersistentCore.instance.analytics.GameEvent("Profile Created");
            PersistentCore.instance.analytics.TutorialStart();
            PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Begin);

            base.OnLoadSkipped();

            PersistentCore.instance.networkController.SetSaveMetaData(persistentData.data.level, persistentData.data.experience);
        }

        #endregion

        #region Private functions.
        bool SpendCoins(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false)
        {
            Assert.IsTrue(cost.type == Cost.CostType.CoinsOnly || cost.type == Cost.CostType.ResourcesAndCoins);

            if (cost.value <= persistentData.data.coins)
            {
                persistentData.data.coins -= cost.value;

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                Events.PlayerProfileUpdatedEvent.Send();
                PersistentCore.instance.analytics.SpendMoneyEvent("Gold", cost.value, info.itemType, info.itemId);
                return true;
            }
            else
            {
                if (spendBucksIfNecessary)
                {
                    int coinsLack = cost.value - coins;
                    var bucks = Cost.CoinsToFitBucks(coinsLack);
                    if (SpendBucks(new Cost(Cost.CostType.BucksOnly, bucks, 0), /*info*/new Analytics.MoneyInfo("Conversion", "Coins"), true))
                    {
                        PersistentCore.instance.analytics.AddMoneyEvent("Gold", coinsLack, "Conversion", "From Bucks");
                        PersistentCore.instance.analytics.SpendMoneyEvent("Gold"/*"Gold_withConvertion"*/, coinsLack, info.itemType, info.itemId);
                        persistentData.data.coins = 0;
                        gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

                        return true;
                    }
                    return false;
                }
            }

            return false;
        }

        bool SpendBucks(Cost cost, Analytics.MoneyInfo info = null, bool withConvertion = false)
        {
            Assert.IsTrue(cost.type == Cost.CostType.BucksOnly || cost.type == Cost.CostType.ResourcesAndCoins || cost.type == Cost.CostType.ResourcesOnly);

            if (cost.value <= persistentData.data.fitBucks)
            {
                persistentData.data.fitBucks -= cost.value;

                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                Events.PlayerProfileUpdatedEvent.Send();

                if (info != null)
                {
                string strCurrency = "FitBucks";//withConvertion ? "FitBucks_withConvertion" : "FitBucks";
                PersistentCore.instance.analytics.SpendMoneyEvent(strCurrency, cost.value, info.itemType, info.itemId);
                }
                return true;
            }

            return false;
        }

        bool SpendResources(Cost cost, bool spendBucksIfNecessary, Analytics.MoneyInfo info)
        {
            Assert.IsTrue(cost.type == Cost.CostType.ResourcesAndCoins || cost.type == Cost.CostType.ResourcesOnly);

            // compensate lacking resources with FitBucks
            Dictionary<ResourceType, int> lackRes = new Dictionary<ResourceType, int>();
            var extraPrice = storage.GetExtraBucksPrice(cost, lackRes, false);

            if (extraPrice.value > 0 && spendBucksIfNecessary == false)
                return false;

            if (extraPrice.value > 0)
            {
                int platinumCount = lackRes.Where(i => i.Key == ResourceType.Platinum).Sum(i => i.Value);
                int resCount = lackRes.Where(i => i.Key != ResourceType.Platinum).Sum(i => i.Value); 
                int platinumCost = Cost.RecourcePriceInFitbucks(ResourceType.Platinum) * platinumCount;
                
                if (!SpendBucks(extraPrice, null /*info new Analytics.MoneyInfo("Conversion", "Resource")*/, true))
                {
                    return false;
                }
                else
                {
                    if (platinumCount > 0)
                    {
                        PersistentCore.instance.analytics.AddMoneyEvent("Platinum", platinumCount, "Conversion", "From Bucks");
                        PersistentCore.instance.analytics.SpendMoneyEvent("FitBucks", platinumCost, "Conversion", "Platinum");
                    }

                    if (extraPrice.value - platinumCost > 0)
                    {
                        PersistentCore.instance.analytics.AddMoneyEvent("GameRes", resCount, "Conversion", "From Bucks");
                        PersistentCore.instance.analytics.SpendMoneyEvent("FitBucks", extraPrice.value - platinumCost, "Conversion", "GameRes");
                    }
                }
            }

            foreach (var res in cost.resources)
            {
                storage.SpendResource(res.Key, /*info*/null, res.Value, spendBucksIfNecessary, lackRes.ContainsKey(res.Key));
            }

            int paltinumInCost = cost.PlatinumCount();
            int resInCost = cost.ResourceCount();

            if(paltinumInCost > 0)
                PersistentCore.instance.analytics.SpendMoneyEvent("Platinum", paltinumInCost, info.itemType, info.itemId);

            if(cost.resources.Count > paltinumInCost)
                PersistentCore.instance.analytics.SpendMoneyEvent("GameRes", resInCost - paltinumInCost, info.itemType, info.itemId);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            Events.PlayerProfileUpdatedEvent.Send();
            return true;
        }

        void OnLevelChanged()
        {
            Quests.ExerciseEquipSelector.UpdateAvailability(gameCore);
            gameCore.quests.dailyQuests.UpdateItemsCount();
            AddReward(levelUpReward, new Analytics.MoneyInfo("LevelUpReward", "Level " + level.ToString()), true);

            Sound.instance.LevelUp();
            PersistentCore.instance.analytics.ProgressionEvent(persistentData.data.level);
            PersistentCore.instance.analytics.SetCustomDimension(0, Analytics.CustomDimension.Level, persistentData.data.level);
            Advertisements.instance.SetupCurLevel(persistentData.data.level);
            Advertisements.instance.RequestRewardedVideo();

        }
        #endregion
    }
}