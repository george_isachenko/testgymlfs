﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Data;
using Data.PlayerProfile;
using Logic.Core;
using Logic.FacadesBase;
using Logic.PlayerProfile.Events;
using Presentation;
using UnityEngine;
using UnityEngine.Assertions;
using Core.Analytics;

namespace Logic.PlayerProfile
{
    public delegate void StorageUpdated(int countTotal, int capacity, float capacityRatio, bool haveNewItem );

    public class LogicStorage : BaseLogicNested
    {
        #region Public events.
        public event StorageUpdated onStorageUpdated;
        public event Action<int> onLevelUp;
        #endregion

        public PresentationStorage presenter { private get; set; }

        #region Public properties.
        public int                      countTotal          { get { return data.countTotal; } }
        public int                      countJuices         { get { return data.countJuices; } }
        public int                      capacity            { get { return data.capacity; } }
        public string                   capacityRatioStr    { get { return data.capacityRatioStr; } }
        public string                   capacityNextRatioStr{ get { return data.capacityNextRatioStr; } }
        public float                    capacityRatio       { get { return data.capacityRatio; } }
        public int                      capacityNext        { get { return data.capacityNext; } }
        public List<StorageItemInfo>    allItems            { get { return data.items; } }
        public List<StorageItemInfo>    itemsJuice          { get { return data.itemsJuice; } }
        public bool                     isFull              { get { return capacity <= countTotal; } }
        public int                      capacityAvailable   { get { return System.Math.Max(capacity - countTotal, 0); } }
        public Cost                     capacityUpgradeCost { get { return data.capacityUpgradeCost; } }
        public bool                     canUpgrade          { get { return data.canUpdgrade; } }
        #endregion

        #region Private data.
        StorageData data;
        #endregion

        #region Constructors.
        public LogicStorage(IGameCore gameCore, StorageData data)
            : base(gameCore)
        {
            Assert.IsNotNull(data);
            this.data = data;

            LevelUpEvent.Event += OnLevelUp;
        }
        #endregion

        #region Unity API.
        public void OnDestroy()
        {
            LevelUpEvent.Event -= OnLevelUp;
        }
        #endregion

        #region Public API.
        public bool IsAvailable()
        {
            return gameCore.playerProfile.level >= 5;
        }

        public void Reinit(StorageData data)
        {
            Assert.IsNotNull(data);
            this.data = data;
        }

        public void PostInit()
        {
            onStorageUpdated?.Invoke(countTotal, capacity, capacityRatio, data.haveNewItem);
        }

        public bool CanAcceptResourcesCompletely (Dictionary<ResourceType, int> requiredResources)
        {
            int total = 0;

            if (gameCore.quests.tutorial.isRunning)
                return true;

            foreach (var res in requiredResources)
            {
                BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
                if (!DataTables.instance.balanceData.Storage.Resources.TryGetValue(res.Key, out resBalanceMeta) || resBalanceMeta.disabled)
                {
                    Debug.LogWarningFormat("Cannot accept resource to storage: {0}, balance data inavailable or resource is disabled.", res.Key);
                    return false;
                }

                total += (res.Value * resBalanceMeta.capacityCost);
            }

            var capacityLeft = capacity - countTotal;

            return (total <= capacityLeft);
        }

        public bool AddResource(ResourceType id, int value, bool force, Analytics.MoneyInfo info)
        {
            if (value <= 0)
                return true;

            bool isTutor = gameCore.quests.tutorial.isRunning;

            BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
            if (!DataTables.instance.balanceData.Storage.Resources.TryGetValue(id, out resBalanceMeta) || resBalanceMeta.disabled)
            {
                Debug.LogErrorFormat("Trying to add wrong resource to storage: {0}, balance data inavailable or resource is disabled.", id);
                return false;
            }

            if (force || isTutor || resBalanceMeta.capacityCost == 0 )
            {
                data.Add(id, value);
            }
            else
            {
                if (isFull)
                    return false;

                var capacityLeft = capacity - countTotal;

                if (value * resBalanceMeta.capacityCost <= capacityLeft)
                {
                    data.Add(id, value);
                }
                else
                {
                    data.Add(id, Mathf.FloorToInt(capacityLeft * (1.0f / resBalanceMeta.capacityCost)));
                    Debug.LogWarning("Added resource partially");
                }
            }

            gameCore.ScheduleSave(Serialization.ScheduledSaveType.SaveOnThisFrame);

            if (info != null)
            {
                PersistentCore.instance.analytics.AddMoneyEvent(resBalanceMeta.category, value, info.itemType, info.itemId);
            }

            var ratio = capacityRatio;
            StorageUpdatedEvent.Send(countTotal, capacity, ratio);
            onStorageUpdated?.Invoke(countTotal, capacity, ratio, data.haveNewItem);

            if (id == ResourceType.Platinum)
                OnAddPlatinum(value);

            return true;
        }

        public int AddResourcePartially(ResourceType id, int value, Analytics.MoneyInfo info)
        {
            if (value <= 0)
                return 0;

            bool isTutor = gameCore.quests.tutorial.isRunning;

            BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
            if (!DataTables.instance.balanceData.Storage.Resources.TryGetValue(id, out resBalanceMeta) || resBalanceMeta.disabled)
            {
                Debug.LogErrorFormat("Trying to add wrong resource to storage: {0}, balance data inavailable or resource is disabled.", id);
                return 0;
            }

            if (resBalanceMeta.capacityCost == 0 || isTutor)
            {
                data.Add(id, value);
            }
            else
            {
                if (isFull)
                {
                    return 0;
                }
                else
                {
                    var capacityLeft = capacity - countTotal;
                    var canAccept = capacityLeft / resBalanceMeta.capacityCost;

                    if (canAccept > 0)
                    {
                        value = Math.Min(canAccept, value);
                        data.Add(id, value);

                        if (id == ResourceType.Platinum)
                            OnAddPlatinum(value);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }

            gameCore.ScheduleSave(Serialization.ScheduledSaveType.SaveOnThisFrame);

            if (info != null)
            {
                PersistentCore.instance.analytics.AddMoneyEvent(resBalanceMeta.category, value, info.itemType, info.itemId);
            }

            var ratio = capacityRatio;
            StorageUpdatedEvent.Send(countTotal, capacity, ratio);
            onStorageUpdated?.Invoke(countTotal, capacity, ratio, data.haveNewItem);

            return value;
        }

        public bool CanSpendResources(Dictionary<ResourceType, int> resources)
        {
            foreach (var res in resources)
            {
                var count = GetResourcesCount(res.Key);
                if (res.Value > count)
                    return false;
            }

            return true;
        }

        public bool SpendResources(Dictionary<ResourceType, int> resources, Analytics.MoneyInfo info = null)
        {
            foreach (var item in resources)
            {
                var res = item.Key;
                var count = item.Value;

                if ( !SpendResource(res, info, count))
                    return false;
            }

            onStorageUpdated?.Invoke(countTotal, capacity, capacityRatio, data.haveNewItem);

            return true;
        }

        public bool SpendResource(ResourceType id, Analytics.MoneyInfo info = null, int count = 1, bool force = false, bool buyWithConvertion = false)
        {
            Assert.IsTrue(count >= 0);

            string resName = id == ResourceType.Platinum ? "Platinum" : "GameRes";
            string resNameConversion = id == ResourceType.Platinum ? "Platinum" : "GameRes";//"Platinum_withConvertion" : "GameRes_withConvertion";

            if (!data.resources.ContainsKey(id))
            {
                if(buyWithConvertion && info != null)
                    PersistentCore.instance.analytics.SpendMoneyEvent(resNameConversion, count, info.itemType, info.itemId);
                return false;
            }

            var resCountAvailable = data.resources[id];
            var resCountNeeded = count;


            if (resCountAvailable < resCountNeeded && force == false)
                return false;


            data.resources[id] = resCountAvailable - resCountNeeded;

            if (data.resources[id] <= 0)
                data.resources.Remove(id);

            string strCurrency =  buyWithConvertion ? resNameConversion : resName;


            gameCore.ScheduleSave(Logic.Serialization.ScheduledSaveType.SaveOnThisFrame);

            if(info != null)
            PersistentCore.instance.analytics.SpendMoneyEvent(strCurrency, count, info.itemType, info.itemId);
            StorageUpdatedEvent.Send(countTotal, capacity, capacityRatio);
            onStorageUpdated?.Invoke(countTotal, capacity, capacityRatio, data.haveNewItem);

            if (id == ResourceType.Platinum)
                OnSpendPlatinum(count);

            return true;
        }

        public Cost GetExtraBucksPrice(Cost cost, Dictionary<ResourceType, int> outlackRes = null, bool includeCoins = true)
        {
            int bucks = 0;
            foreach (var res in cost.resources)
            {
                var resId = res.Key;
                var resCountNeeded = res.Value;
                var resCountInStorage = GetResourcesCount(resId);

                var resCountLack = resCountNeeded - resCountInStorage;

                if (resCountLack > 0)
                {
                    var sportRes = DataTables.instance.sportResources.FirstOrDefault(r => r.id == resId);

                    if (sportRes != null)
                    {
                        bucks += sportRes.bucks;
                    }
                    else
                    {
                        bucks += Cost.RecourcePriceInFitbucks(resId) * resCountLack;
                    }

                    if (outlackRes != null)
                        outlackRes.Add(res.Key, resCountLack);
                }
            }

            if (cost.type == Cost.CostType.ResourcesAndCoins && includeCoins)
            {
                if (cost.value > gameCore.playerProfile.coins)
                {
                    var coinsDiff = cost.value - gameCore.playerProfile.coins;
                    bucks += Cost.CoinsToFitBucks(coinsDiff);
                }
            }

            if (cost.type == Cost.CostType.BucksOnly)
                bucks += cost.value;

            return new Cost(Cost.CostType.BucksOnly, bucks, 0);
        }

        public Dictionary<ResourceType, int> GetResourcesShortage (Dictionary<ResourceType, int> requiredResources)
        {
            return
                (from requiredResource in requiredResources
                 where !data.resources.ContainsKey(requiredResource.Key) || data.resources[requiredResource.Key] < requiredResource.Value
                 select new KeyValuePair<ResourceType, int>
                    ( requiredResource.Key
                    , data.resources.ContainsKey(requiredResource.Key)
                        ? requiredResource.Value - data.resources[requiredResource.Key]
                        : requiredResource.Value))
            .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public int GetResourcesCount(ResourceType type)
        {
            return data.GetResourcesCount(type);
        }

        public void BuyNextUpgrade()
        {
            if (gameCore.playerProfile.SpendMoney(capacityUpgradeCost, new Analytics.MoneyInfo("Storage", "Storage"), true))
            {
                IncreaseCapacity();
                presenter.OnStorageUpgraded(data.capacityOld, data.capacity);
                presenter.OnStorageUpdated(countTotal, capacity, capacityRatio, data.haveNewItem);
                    
                GameEventParam[] parameters = { new GameEventParam("Level", gameCore.playerProfile.level), new GameEventParam("Expand level", data.capacityIdx) };
                PersistentCore.instance.analytics.GameEvent("Storage Expand", parameters);
            }
        }

        public void OnWinStorageOpen()
        {
            data.haveNewItem = false;
        }

        public void CheckDataIntegrity()
        {
            data.CheckDataIntegrity();
        }

        public int GetPlatinumCount()
        {
            int count = 0;
            data.resources.TryGetValue(ResourceType.Platinum, out count);

            if (count > 0)
                data.wasPlatina = true;

            if (!data.wasPlatina)
                return -1;

            return count;
        }
        #endregion

        #region Private methods.
        private void OnLevelUp(int level)
        {
            onLevelUp?.Invoke(level);
        }

        void IncreaseCapacity()
        {
            if (canUpgrade)
            {
                data.capacityIdx++;
            }
            else
            {
                Debug.LogError("Can't upgrade storage");
            }
        }

        void OnAddPlatinum(int count)
        {
            data.wasPlatina = true;
            //presenter.OnAddPlatinum(count);
        }

        void OnSpendPlatinum(int count)
        {

        }
        #endregion
    }
}