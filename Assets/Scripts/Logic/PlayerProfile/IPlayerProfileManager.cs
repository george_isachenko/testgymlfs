﻿using System;
using Core;
using Core.Timer;
using Data;
using Data.PlayerProfile;
using Core.Advertisement;
using Core.Analytics;

namespace Logic.PlayerProfile
{
    public interface IPlayerProfileManager
    {
        #region Properties
        int                 coins               { get; }
        int                 fitBucks            { get; }
        int                 level               { get; }
        Cost                levelUpReward       { get; }
        int                 experience          { get; }
        TimerFloat          fatigue             { get; }
        LogicStorage        storage             { get; }
        bool                wasFBConnected      { get; set; }
        AdvertisementsData adverticementsData { get; } 
        #endregion

        #region Public API.
		bool IsAnyTutorial();
        void AddReward(Cost price, Analytics.MoneyInfo info, bool forceResourceAdd, bool fx = false);
        void AddCoins(int coins, Analytics.MoneyInfo info = null, bool fx = false, bool isInApp = false);
        void AddFitBucks(int bucks, Analytics.MoneyInfo info = null, bool fx = false, bool isInApp = false);
        void AddResource(ResourceType type, int value, Analytics.MoneyInfo info, bool force);
        bool AddExperience(int exp, bool fx = false);
        bool CanSpend(Cost cost, bool forceFitBucks = false);
        Cost GetShortageCost(Cost cost);
        bool SpendMoney(Cost cost, Analytics.MoneyInfo info, bool spendBucksIfNecessary = false);
        bool PurchaseBankItemTransaction(ShopItemData shopItemData, Func<bool> performAction, Action rollbackAction = null);
        void AddFitPoints(int count);
        int GetFitPointsCount();
        void SendNotEnoughMoneyEvent(Cost price, bool useFitbucks);
        #endregion

    }
}
