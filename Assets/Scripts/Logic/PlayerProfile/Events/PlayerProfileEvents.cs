﻿using Core.EventSystem;

namespace Logic.PlayerProfile.Events
{
    public class PlayerProfileUpdatedEvent : StaticEvent<PlayerProfileUpdatedEvent.Delegate>
    {
        public delegate void Delegate();
    }

    public class LevelUpEvent : StaticEvent<LevelUpEvent.Delegate, int>
    {
        public delegate void Delegate(int lvl);
    }

    public class NewDayEvent : StaticEvent<NewDayEvent.Delegate, int>
    {
        public delegate void Delegate(int days);
    }

    public class DailyBonusClaimed : StaticEvent<DailyBonusClaimed.Delegate>
    {
        public delegate void Delegate();
    }

    public class StorageUpdatedEvent : StaticEvent<StorageUpdatedEvent.Delegate, int, int, float>
    {
        public delegate void Delegate(int countTotal, int capacity, float capacityRatio);
    }

}