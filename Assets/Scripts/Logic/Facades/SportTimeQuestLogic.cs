using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.FacadesBase;
using Logic.PlayerProfile;
using Logic.PlayerProfile.Events;
using Logic.Rooms;
using Logic.Rooms.Events;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;

using Random = UnityEngine.Random;
using Core.Analytics;
using Data.Quests.SportQuest;
using Logic.SportAgency.Facade;
using Logic.Quests.SportQuest;
using Logic.Quests.SportQuest.SportLaneQuest.Facade;
using Logic.ResolveEvents;

namespace Logic.Facades
{
    [AddComponentMenu("Kingdom/Logic/Facades/Sport Time Quest")]
    [BindBridgeInterface(typeof(SportTimeQuestPresenter))]
    public class SportTimeQuestLogic 
        : BaseLogicFacade, IPersistent
    {
        private class TimeQuestPersistentData
        {
            public SportTimeQuestData questsData;
            public SportObjectiveData[] objectivesData;
        }

        public event Action onQuestCompleted;
        public event Action onQuestClaimed;

        public event Action<string, RoomData, IRoomSetupDataInfo> onRoomSizeUpdated;
        public event Action<int> onLevelUp;

        private TimeQuestPersistentData persistent = new TimeQuestPersistentData();
        private SingleSportTimeQuest questBuffer;

        public LogicSportsmen sportsmanManager
        {
            get { return gameCore.personManager.sportsmen; }
        }

        public SingleSportTimeQuest quest
        {
            get
            {
                return questBuffer != null &&
                       (questBuffer.newQuestTimer != null &&
                        (questBuffer != null && questBuffer.newQuestTimer.secondsToFinish <= 0))
                    ? questBuffer = GenerateSportQuest()
                    : questBuffer ?? (questBuffer = GenerateSportQuest());
            }
            private set { questBuffer = value; }
        }

        public CanUseFeatureResult canUseMyFeature
        {
            get
            {
                if (gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) <= 0)
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.DependencyLocked);
                if (playerProfile.level < DataTables.instance.balanceData.Quests.TimeQuests.startLevel)
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.LockedNeededLevel,
                        DataTables.instance.balanceData.Quests.TimeQuests.startLevel);
                return new CanUseFeatureResult(CanUseFeatureResult.ResultType.CanUse);
            }
        }

        void Start()
        {
            RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;
            LevelUpEvent.Event += OnLevelUp;
        }

        void OnDestroy()
        {
            quest.UnSubscribe();
            
            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;
            LevelUpEvent.Event -= OnLevelUp;
        }

        public int GetNeededSportsmensCount (SingleSportTimeQuest quest)
        {
            return
                quest.objectives.Sum
                ( o => Mathf.Max
                    ( 0
                    , (o.iterationsGoal - sportsmanManager.GetSportsmanCount
                            ( o.sportsmanType
                            , null
                            , PersonState.Idle
                            //, PersonState.InQueue
                            , PersonState.Entering
                            , PersonState.ExerciseComplete))));
        }

        public int GetIterationsDone (SportObjectiveLogic<SportObjectiveData> questObjective)
        {
            return sportsmanManager.GetSportsmanCount
                ( questObjective.sportsmanType
                , null
                , PersonState.Idle
                // , PersonState.InQueue
                , PersonState.Entering
                , PersonState.ExerciseComplete);
        }

        private void OnLevelUp(int level)
        {
            if (onLevelUp != null)
                onLevelUp(level);
        }

        private SingleSportTimeQuest GenerateSportQuest()
        {
            persistent.questsData = GenerateRandomQuestData();
            persistent.objectivesData = GenerateRandomQuestObjectivesData();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            
            return InitQuestFromPersistent();
        }

        private SingleSportTimeQuest InitQuestFromPersistent()
        {
            if (persistent.objectivesData == null || persistent.questsData == null)
                return null;
            var sportQuest = new SingleSportTimeQuest(persistent.questsData);
            foreach (var objectiveData in persistent.objectivesData)
            {
                sportQuest.AddObjective(new SportObjectiveLogic<SportObjectiveData>(objectiveData, GetIterationsDone));
            }
            sportQuest.objectives.Sort();
            sportQuest.OnQuestClaimed_Callback += a => { if (onQuestClaimed != null) onQuestClaimed(); };
            sportQuest.OnQuestCompleted_Callback += a => { if (onQuestCompleted != null) onQuestCompleted(); };
            return sportQuest;
        }

        private SportObjectiveData[] GenerateRandomQuestObjectivesData()
        {
            var availableSportmans =
                DataTables.instance.sportExercises.Where(e => e.leverRequired <= playerProfile.level)
                    .Select(e => e.trainTo)
                    .Distinct().ToList();
            availableSportmans.Add(SportsmanType.Sportsman_0);

            var res = new List<SportObjectiveData>();
            var count = Random.Range(4, 7);
            for (var k = 0; k < count && availableSportmans.Any(); k++)
            {
                var objectiveData = new SportObjectiveData()
                {
                    sportsmanType = availableSportmans[Random.Range(0, availableSportmans.Count)],
                    iterationsGoal = Random.Range(1, 6)
                };
                availableSportmans.Remove(objectiveData.sportsmanType);
                res.Add(objectiveData);
            }
            return res.ToArray();
        }

        private SportTimeQuestData GenerateRandomQuestData()
        {
            var rewards = new List<SportQuestReward>();
            var availableRewardTypes = new List<SportQuestRewardType>()
            {
                SportQuestRewardType.Experience,
                SportQuestRewardType.FitPoints,
                SportQuestRewardType.Resource
            };

            var availableResources = DataTables.instance.balanceData.Storage.Resources
                .Where(x => !x.Value.disabled && x.Value.availableAsSportTimeQuestReward).Select(x => x.Key).ToList();

            for (var i = 0; i < Random.Range(3, 6); i++)
            {
                var rewardType = availableRewardTypes[UnityEngine.Random.Range(0, availableRewardTypes.Count)];
                if (rewardType != SportQuestRewardType.Resource)
                    availableRewardTypes.Remove(rewardType);
                var count = Random.Range(1, 4);
                var id = ResourceType.None;
                if (rewardType == SportQuestRewardType.Resource)
                {
                    id = availableResources[Random.Range(0, availableResources.Count)];
                    availableResources.Remove(id);
                }
                rewards.Add(new SportQuestReward()
                {
                    rewardType = rewardType,
                    count = count,
                    itemId = id
                });
            }

            return new SportTimeQuestData
            {
                questTypeId = string.Empty,
                rewards = rewards.ToArray()
            };
        }

        public void BuyCompleteAndClaim()
        {
            // check
            if (quest == null)
                throw new NullReferenceException();

            var neededSportsmensCount = GetNeededSportsmensCount (quest);

            var bucksNeeded = neededSportsmensCount * 10;

            if (bucksNeeded > playerProfile.fitBucks)
                throw new NeedMoreBucksException();

            var resRewards = quest.rewards.Where(r => r.rewardType == SportQuestRewardType.Resource).ToDictionary(r => r.itemId, r => r.count);
            if (resRewards != null && resRewards.Count > 0 && !gameCore.playerProfile.storage.CanAcceptResourcesCompletely(resRewards))
            {
                StorageIsFull.Send(false);

                throw new NeedMoreStorageException();
            }

            // do
            foreach (var objective in quest.objectives)
            {
                sportsmanManager.RemoveSportsmen(objective.sportsmanType, objective.iterationsGoal, new Analytics.MoneyInfo("TimeQuest", objective.sportsmanType.ToString()));
            }

            if (!playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, bucksNeeded, 0),
                new Analytics.MoneyInfo("TimeQuest", quest.questTypeId)))
                throw new NoBucksException();

            foreach (var reward in quest.rewards)
            {
                switch (reward.rewardType)
                {
                    case SportQuestRewardType.Coin:
                        playerProfile.AddCoins(reward.count, new Analytics.MoneyInfo("TimeQuest", quest.questTypeId));
                        break;
                    case SportQuestRewardType.Resource:
                        playerProfile.storage.AddResource(reward.itemId, reward.count, true, new Analytics.MoneyInfo("TimeQuest", reward.itemId.ToString()));
                        break;
                    case SportQuestRewardType.Experience:
                        playerProfile.AddExperience(reward.count);
                        break;
                    case SportQuestRewardType.FitPoints:
                        playerProfile.AddFitPoints(reward.count);
                        break;
                }
            }
            quest.ForceComplete();
            quest.Claim();
        }

        public void Claim()
        {
            // check
            if (quest == null)
                throw new NullReferenceException();

            if (!quest.completed)
                throw new QuestNotCompletedException();

            var neededSportsmensCount = GetNeededSportsmensCount (quest);

            if (neededSportsmensCount > 0)
                throw new NeedMoreSportsmensException();

            var resRewards = quest.rewards.Where(r => r.rewardType == SportQuestRewardType.Resource).ToDictionary(r => r.itemId, r => r.count);
            if (resRewards != null && resRewards.Count > 0 && !gameCore.playerProfile.storage.CanAcceptResourcesCompletely(resRewards))
            {
                StorageIsFull.Send(false);
                throw new NeedMoreStorageException();
            }

            // do
            foreach (var objective in quest.objectives)
            {
                sportsmanManager.RemoveSportsmen(objective.sportsmanType, objective.iterationsGoal, new Analytics.MoneyInfo("TimeQuest", objective.sportsmanType.ToString()));
            }

            foreach (var reward in quest.rewards)
            {
                switch (reward.rewardType)
                {
                    case SportQuestRewardType.Coin:
                        playerProfile.AddCoins(reward.count, new Analytics.MoneyInfo("TimeQuest", quest.questTypeId));
                        break;
                    case SportQuestRewardType.Resource:
                        playerProfile.storage.AddResource(reward.itemId, reward.count, true, new Analytics.MoneyInfo("TimeQuest", reward.itemId.ToString()) );
                        break;
                    case SportQuestRewardType.Experience:
                        playerProfile.AddExperience(reward.count);
                        break;
                    case SportQuestRewardType.FitPoints:
                        playerProfile.AddFitPoints(reward.count);
                        break;
                }
            }
            quest.ForceComplete();
            quest.Claim();
        }

        public void SkipTimer()
        {
            if (quest.newQuestTimer != null && !(quest.newQuestTimer.secondsToFinish >= 0)) return;
            if (!playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, 10, 0),
                new Analytics.MoneyInfo("Skip", "TimeQuest")))
            {
                throw new NoBucksException();
            }
            quest.newQuestTimer.Complete();
        }

        string IPersistent.propertyName { get { return "TimeQuestPersistentData"; } }
        Type IPersistent.propertyType { get { return typeof (TimeQuestPersistentData); } }

        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            if (!(obj is TimeQuestPersistentData))
                return false;
            persistent = (TimeQuestPersistentData) obj;
            quest = InitQuestFromPersistent();
            return true;
        }

        object IPersistent.Serialize()
        {
            return persistent;
        }

        void IPersistent.OnPreLoad()
        {
        }

        void IPersistent.OnLoaded(bool success)
        {
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (quest != null && quest.newQuestTimer!=null)
                quest.newQuestTimer.FastForward((float) timePassedSinceSave.TotalSeconds);
        }

        void IPersistent.OnLoadSkipped()
        {
        }

        void OnRoomSizeUpdated (string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupDataInfo)
        {
            onRoomSizeUpdated?.Invoke(roomName, roomData, roomSetupDataInfo);
        }
    }

    public class NoBucksException : Exception
    {
    }
}