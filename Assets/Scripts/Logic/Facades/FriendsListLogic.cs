using System;
using System.Collections;
using BridgeBinder;
using Core;
using Core.Network;
using Core.Network.Friends;
using Core.SocialAPI;
using Data;
using FileSystem;
using Logic.FacadesBase;
using Presentation.Facades;
using UnityEngine;
using Core.Analytics;
using Core.SocialAPI.Networks;

namespace Logic.Facades
{
    [AddComponentMenu("Kingdom/Logic/Facades/Friends List")]
    [BindBridgeInterface(typeof (FriendsListPresenter))]
    public class FriendsListLogic
        : BaseLogicFacade
    {
        bool subscribed = false;

        #region Unity API.
        void Awake()
        {
        }

        void OnDestroy()
        {
            if (subscribed && PersistentCore.gameMode == GameMode.MainGame)
            {
                PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).onLoginStateChanged -= OnSocialNetworkLoginStateChanged;
                subscribed = false;
            }
        }
        #endregion

        public bool IsSocialNetworkLoggedIn(NetworkType networkType)
        {
            var network = PersistentCore.instance.socialManager.GetNetwork(networkType);
            return (network != null ? network.GetNetworkState() == NetworkState.LoggedIn : false);
        }

/*
        public bool IsUsingSocialNetworkOnServer(SocialNetwork network)
        {
            return PersistentCore.instance.networkController.IsSocialNetworkLinkedOnServer(SocialNetwork.Facebook);
        }
*/

        public bool GotFacebookConnectBonus()
        {
            return playerProfile.wasFBConnected;
        }

        public void GetFriendsList(OnGetFriendsFinished onComplete)
        {
            TryToSubscribeToSocialLoginEvents();

            PersistentCore.instance.friendsManager.GetFriendsList(onComplete);
        }

        public void Invite(NetworkType network)
        {
            //PersistentCore.instance.networkController.friendsManager.ShowInviteDialog();
            PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).ShowInviteDialog();
        }

        public void Connect(NetworkType network, OnLoginComplete onComplete)
        {
            TryToSubscribeToSocialLoginEvents();

            PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).TryLogin
                ( onComplete
                , ConnectionSource.FriendList);
        }

        public void ShowFriend(Friend friend, Action<bool> onComplete = null)
        {
            // Debug.LogError("Go to friends not implemented yet...");

            StartCoroutine(ShowFriendJob(friend, onComplete));
        }

        public void TryToGiveFacebookConnectBonus ()
        {
            if (!playerProfile.wasFBConnected && PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).GetNetworkState() == NetworkState.LoggedIn)
            {
                var reward = DataTables.instance.balanceData.Social.facebookConnectReward;

                playerProfile.AddFitBucks(reward, new Analytics.MoneyInfo("Facebook", "Connection"));
                playerProfile.wasFBConnected = true;
                gameCore.ScheduleSave(Serialization.ScheduledSaveType.SaveOnThisFrame);

                PersistentCore.instance.analytics.GameEvent("Connect to Facebook",
                    new GameEventParam("Level", gameCore.playerProfile.level),
                    new GameEventParam("From", PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).connectionSource.ToString()));
            }
        }

        private IEnumerator ShowFriendJob (Friend friend, Action<bool> onComplete)
        {
            yield return null;

            var splashShowComplete = false;
            PersistentCore.instance.splashScreenView.Show(() => splashShowComplete = true);

            yield return new WaitUntil(() => splashShowComplete);

            if (friend.isDummy)
            {
                onComplete?.Invoke(true);

                PersistentCore.ScheduleGameModeSwitching(GameMode.DemoLevel, DemoLevelSource.FromResource);
            }
            else
            {
                PersistentCore.instance.networkController.DownloadFriendSaveFromServer
                    ( friend.profileId
                    , ResourcePaths.Persistent.saveGameFriendFullPath
                    , false
                    , (result) =>
                    {
                        if (result == DownloadFriendSaveStatus.OK)
                        {
                            onComplete?.Invoke(true);

                            PersistentCore.instance.splashScreenView.progressText = Loc.Get("SplashScreenMessageSwitchingToFriendSaveGame");

                            PersistentCore.ScheduleGameModeSwitching(GameMode.DemoLevel, DemoLevelSource.FromFile);
                        }
                        else
                        {
                            PersistentCore.instance.splashScreenView.Hide(() => onComplete?.Invoke(false));
                        }
                    });
            }
        }

        private void TryToSubscribeToSocialLoginEvents ()
        {
            if (!subscribed && PersistentCore.gameMode == GameMode.MainGame)
            {
                PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).onLoginStateChanged += OnSocialNetworkLoginStateChanged;
                subscribed = true;
            }
        }

        private void OnSocialNetworkLoginStateChanged (NetworkType network, LoginResult result)
        {
            if (network == NetworkType.Facebook && result == LoginResult.OK)
            {
                TryToGiveFacebookConnectBonus();
            }
        }
    }
}