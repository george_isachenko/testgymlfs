using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Data;
using Data.Estate;
using Data.Estate.Static;
using Data.Person;
using Logic.Persons;
using Logic.Estate;
using Logic.FacadesBase;
using Logic.PlayerProfile;
using Presentation.Facades;
using UnityEngine;
using Logic.Persons.Events;
using Core.Analytics;
using Data.Sport;
using Logic.SportAgency.Facade;

namespace Logic.Facades
{
    [AddComponentMenu("Kingdom/Logic/Facades/Sportsman Training")]
    [BindBridgeInterface(typeof (SportsmanTrainingPresenter))]
    public class SportsmanTrainingLogic
        : BaseLogicFacade
    {
        public event Action OnExerciseFinish;

        public LogicSportsmen sportsmanManager
        {
            get { return gameCore.personManager.sportsmen; }
        }

        public LogicStorage storage
        {
            get { return playerProfile.storage; }
        }

        public LogicSportAgency sportAgencyLogic
        {
            get { return gameCore.sportAgency; }
        }

        private void Awake()
        {
            CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent;
        }

        private void OnDestroy()
        {
            CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
        }

        public List<SportExercise> GetExercisesForEquip(int estateEquipId)
        {
            return DataTables.instance.sportExercises.Where(e => e.equipmentId == estateEquipId).ToList();
        }

        public bool HasResourcesForExercise(SportExercise exercise)
        {
            var existingResources = storage.GetResourcesCount(exercise.needResource);
            var existingSportsmans = sportsmanManager.GetSportsmanCount(exercise.trainFrom, null, PersonState.Entering, PersonState.Idle, PersonState.ExerciseComplete);
            return existingSportsmans > 0 && existingResources > 0;
        }

        public bool Train(SportExercise sportExercise, int equipmentId, int preferedCharacterId, bool noResourceSpending = false)
        {
            PersonSportsman sportsman = null;
            if (preferedCharacterId != PersonData.InvalidId)
            {
                sportsman = sportsmanManager.GetMember(preferedCharacterId);
                if (sportsman != null)
                {
                    if ((sportsman.idleType != Person.IdleType.Idle) ||
                        (sportsman.sportsmanType != sportExercise.trainFrom))
                        sportsman = null;
                }
            }

            if (sportsman == null)
            {
                sportsman = sportsmanManager.GetSportsman(sportExercise.trainFrom, gameCore.roomManager.activeRoomName,
                    PersonState.Entering, PersonState.Idle, PersonState.ExerciseComplete);
            }

            if (sportsman != null)
            {
                EstateType estateType;
                if (!DataTables.instance.estateTypes.TryGetValue (sportExercise.equipmentId, out estateType))
                {
                    Debug.LogErrorFormat("No suitable Estate Type found! Sport Exercise id: {0}, Estate Type ID: {1}."
                        , sportExercise.id, sportExercise.equipmentId);
                    return false;
                }

                if (sportExercise.exerciseTier < 0 || sportExercise.exerciseTier >= estateType.exercises.Count())
                {
                    Debug.LogErrorFormat("Wrong exercise tier! Sport Exercise id: {0}, Exercise Tier: {1}."
                        , sportExercise.id, sportExercise.exerciseTier);
                    return false;
                }
                Exercise exercise = estateType.exercises[sportExercise.exerciseTier];

                Equipment equipment;
                gameCore.equipmentManager.equipment.TryGetValue(equipmentId, out equipment);
                
                if (equipment == null || equipment.state != EquipmentState.Idle || equipment.estateType.id != estateType.id)
                {
                    Debug.LogErrorFormat("Cannot find suitable equipment! Estate Type ID: {0}, Room: {1}."
                        , sportExercise.equipmentId, gameCore.roomManager.activeRoomName);
                    return false;
                }

                if (!noResourceSpending)
                {
                    if (!HasResourcesForExercise(sportExercise))
                    {
                        Debug.LogErrorFormat("Haven't resources but try to train exercise {0}.", sportExercise.id);
                        return false;
                    }

                    if(storage.SpendResource(sportExercise.needResource, new Analytics.MoneyInfo(string.Empty, string.Empty), 1))
                        PersistentCore.instance.analytics.SpendMoneyEvent("SportResource", 1, "Training", sportExercise.sportLocNameId);
                    
                }

                if (sportsman.TryToUseEquipment(equipment, exercise, sportExercise))
                {
                    return true;
                }
                else
                {
                    Debug.LogError("Sportsman refused to use equipment!");
                    return false;
                }
            }
            else
            {
                Debug.LogErrorFormat("No suitable sportman found! SportType: {0}, Room: {1}, State(s): {2}."
                    , sportExercise.trainFrom, gameCore.roomManager.activeRoomName, new[] { PersonState.Entering, PersonState.Idle, PersonState.ExerciseComplete });
            }
            return false;
        }

        public bool QuickPurchaseSportsmen(Cost sportsmenCost, SportExercise exercise)
        {
            if (playerProfile.SpendMoney(sportsmenCost, new Analytics.MoneyInfo("Sportsmen", "exercise purchase")))
            {
                var sportsmenId = sportsmanManager.CreateSportsman
                    ( exercise.trainFrom
                    , PersonAppearance.Gender.Female
                    , new Analytics.MoneyInfo("BuyWithBucks" , exercise.trainFrom.ToString())
                    , null
                    , "sportsman_spawn_quick");

                return (sportsmenId != PersonData.InvalidId);
            }

            return false;
        }

        private void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            var sportsman = person as PersonSportsman;
            if (sportsman != null)
            {
                OnExerciseFinish?.Invoke();
            }
        }
        /*
        public void DelayedInvoke(Action action)
        {
            gameCore.StartCoroutine(DelayedInvokeCoroutine(action));
        }

        private IEnumerator DelayedInvokeCoroutine(Action action)
        {
            yield return new WaitForEndOfFrame();
            action();
        }*/

        public void SelectEquip(int equipId)
        {
            gameCore.equipmentManager.SelectEquipmentRequest(equipId);
        }
    }
}
