using System;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Timer;
using Data;
using Data.Estate;
using Data.Room;
using Logic.FacadesBase;
using Logic.Rooms;
using Logic.Rooms.Events;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using Core.Analytics;
using Data.Sport;

namespace Logic.Facades
{
    [AddComponentMenu("Kingdom/Logic/Facades/Logic Sport Shop")]
    [BindBridgeInterface(typeof (PresentationSportShop))]
    public class LogicSportShop
        : BaseLogicFacade, IPersistent
    {
        private class SportShopSaveData
        {
            public Dictionary<int, TimerFloat> cooldownTimers = new Dictionary<int, TimerFloat>();
            public Dictionary<int, CooldownStackInfo> stackInfosData = new Dictionary<int, CooldownStackInfo>();
        }

        public event Action<string, RoomData, IRoomSetupDataInfo> onRoomSizeUpdated;

        //private PresentationSportShop presenter;
        private SportShopData data;
        private SportShopSaveData persistentData = new SportShopSaveData();

        string IPersistent.propertyName
        {
            get { return "SportShopSaveData"; }
        }

        Type IPersistent.propertyType
        {
            get { return typeof (SportShopSaveData); }
        }

        public bool canUseMyFeature
        {
            get { return gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) > 0; }
        }

        void Start()
        {
            RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;
        }

        void OnDestroy()
        {
            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;
        }

        public SportShopEquipLogic[] shopTrainers
        {
            get
            {
                var datas = data.trainersItems;
                return datas.Select(d =>
                {
                    var info = new SportShopEquipLogic(d,gameCore.equipmentManager);
                    info.currentCooldownStack = GetCurrentCooldownStack(info);
                    return info;
                }).ToArray();
            }
        }

        public SportShopResourceLogic[] shopResources
        {
            get
            {
                var datas = data.consumablesItems;
                return datas.Select(d =>
                {
                    var info = new SportShopResourceLogic(d, playerProfile.storage);
                    info.currentCooldownStack = GetCurrentCooldownStack(info);
                    return info;
                }).ToArray();
            }
        }

        private CooldownStackInfo GetCurrentCooldownStack(SportShopItemInfo info)
        {
            if (persistentData.stackInfosData.ContainsKey(info.id))
            {
                var stack = persistentData.stackInfosData[info.id];
                if (stack.count >= stack.stackSize && GetCooldownTime(info.id) <= 0)
                {
                    RemoveCooldown(info.id);
                    return info.baseCooldownStack;
                }
                return stack;
            }
            return info.baseCooldownStack;
        }

        [BindBridgeSubscribe]
        private void Subscribe(PresentationSportShop presentationShop)
        {
            //presenter = presentationShop;

            data = new SportShopData(gameCore);
        }

        public SportItemLockInfo GetLockState(SportShopItemInfo shopItem)
        {
            if (gameCore.playerProfile.level < shopItem.buyItemCost.level)
                return new SportItemLockInfo(true,SportItemLocktype.userHaveSmallLevel, shopItem.buyItemCost.level);
            if (GetCooldownTime(shopItem.id) > 0)
                return new SportItemLockInfo(true, SportItemLocktype.waitCooldown, GetCooldownTimer(shopItem.id));
            if (shopItem is SportShopEquipLogic)
            {
                var equip = (SportShopEquipLogic) shopItem;
                if (equip.equipData.requiredUpgradeLevel>gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)))
                {
                    return new SportItemLockInfo(true, SportItemLocktype.sportRoomLocked);
                }

                if (gameCore.equipmentManager.FindAvailableLockedStaticEquipment(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub),
                    ((SportShopEquipLogic) shopItem).equipData.id) == null)
                {
                    return new SportItemLockInfo(true, SportItemLocktype.fullStorage);
                }
            }
            if (shopItem is SportShopResourceLogic)
            {
                var res = (SportShopResourceLogic) shopItem;
                var equips =
                    DataTables.instance.sportExercises.Where(e => e.needResource == res.resourceData.id)
                        .Select(e => e.equipmentId).Distinct();
                var level =
                    DataTables.instance.sportEquipment.Where(e => equips.Contains(e.id))
                        .Distinct()
                        .Min(e => e.requiredUpgradeLevel);
                if (level > gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)))
                    return new SportItemLockInfo(true, SportItemLocktype.sportRoomLocked);

            }
            if (shopItem is SportShopResourceLogic && playerProfile.storage.isFull)
                return new SportItemLockInfo(true, SportItemLocktype.fullStorage);
            return new SportItemLockInfo(false);
        }

        private TimerFloat GetCooldownTimer(int id)
        {
            return persistentData.cooldownTimers.ContainsKey(id) ? persistentData.cooldownTimers[id] : null;
        }

        private float GetCooldownTime(int id)
        {
            if (!persistentData.cooldownTimers.ContainsKey(id)) return 0;
            if (persistentData.cooldownTimers[id].secondsToFinish > 0)
                return persistentData.cooldownTimers[id].secondsToFinish;
            persistentData.cooldownTimers.Remove(id);
            if (persistentData.stackInfosData.ContainsKey(id))
            {
                var stack = persistentData.stackInfosData[id];
                if (stack.count >= stack.stackSize) return 0;
                stack.count = 0;
                persistentData.stackInfosData[id] = stack;
            }
            return 0;
        }

        private void RemoveCooldown(int id)
        {
            if (persistentData.cooldownTimers.ContainsKey(id))
                persistentData.cooldownTimers.Remove(id);
            if (persistentData.stackInfosData.ContainsKey(id))
                persistentData.stackInfosData.Remove(id);
        }

        public void TryBuy(SportShopResourceLogic itemLogic, Action success, Action<string> fail)
        {
            if (GetCooldownTime(itemLogic.id) > 0)
            {
                fail("Need to wait cooldown finish...");
                return;
            }
            var money = new Analytics.MoneyInfo("SportResource", itemLogic.id.ToString());

            if (gameCore.playerProfile.SpendMoney(itemLogic.buyItemCost, money))
            {
                if (gameCore.playerProfile.storage.AddResource(itemLogic.resourceData.id, 1, false, new Analytics.MoneyInfo("Shop",itemLogic.resourceData.id.ToString())))
                {
                    IncStackinfo(itemLogic);
                    success();
                }
                else
                {
                    fail("Can't add buyed resource " + itemLogic.id + " to storage");
                }
            }
            else
            {
                fail("You got no money!");
            }
        }

        public void TryBuy(SportShopEquipLogic itemLogic, Action success, Action<string> fail)
        {
            var locker = GetLockState(itemLogic);
            if (locker.isLocked)
            {
                fail("Locked. Lock type: " + locker.lockType);
                return;
            }
            if (GetCooldownTime(itemLogic.id) > 0)
            {
                fail("Need to wait cooldown finish...");
                return;
            }
            var money = new Analytics.MoneyInfo("SportEquipment", itemLogic.id.ToString());
            int equipmentIdToUnlock;
            var staticEquipmentId = gameCore.equipmentManager.FindAvailableLockedStaticEquipment
                (gameCore.roomManager.activeRoomName, Convert.ToInt32(itemLogic.id));

            if (staticEquipmentId != null)
            {
                equipmentIdToUnlock = gameCore.equipmentManager.FindStaticEquipmentInRoom(staticEquipmentId.Value);
                if (equipmentIdToUnlock == EquipmentData.InvalidId)
                {
                    fail("Cannot find static equipment.");
                    return;
                }
            }
            else
            {
                fail("Cannot find available locked static equipment.");
                return;
            }

            if (gameCore.playerProfile.SpendMoney(itemLogic.buyItemCost, money))
            {
                gameCore.equipmentManager.UnlockEquipmentRequest(equipmentIdToUnlock);
                IncStackinfo(itemLogic);
                success();
            }
            else
            {
                fail("You got no money!");
            }
        }

        private void IncStackinfo(SportShopItemInfo logic)
        {
            CooldownStackInfo stackInfo;
            if (persistentData.stackInfosData.ContainsKey(logic.id))
            {
                stackInfo = persistentData.stackInfosData[logic.id];
                stackInfo.count++;
                persistentData.stackInfosData[logic.id] = stackInfo;
            }
            else
            {
                stackInfo = logic.baseCooldownStack;
                stackInfo.count++;
                persistentData.stackInfosData.Add(logic.id, stackInfo);
            }

            if (stackInfo.count >= stackInfo.stackSize)
            {
                var timer = new TimerFloat(logic.cooldownTimeSeconds);
                if (persistentData.cooldownTimers.ContainsKey(logic.id))
                    persistentData.cooldownTimers[logic.id] = timer;
                else
                    persistentData.cooldownTimers.Add(logic.id, timer);
            }
        }

        public void TryBreakLocker(SportShopItemInfo itemLogic, SportItemLockInfo locker, Action success, Action<string> fail)
        {
            if (GetCooldownTime(itemLogic.id)<=0)
            {
                fail("Timer not set...");
                return;
            }
            var money = new Analytics.MoneyInfo("SportShopCooldownSkip", itemLogic.id.ToString());
            if (gameCore.playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, DynamicPrice.GetSkipCost((int)locker.timer.secondsToFinish), 0), money))
            {
                RemoveCooldown(itemLogic.id);
                success();
            }
            else
            {
                fail("You got no money!");
            }
        }

        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            if (!(obj is SportShopSaveData)) return false;
            persistentData = (SportShopSaveData) obj;
            return true;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad()
        {

        }

        void IPersistent.OnLoaded(bool success)
        {

        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (var timer in persistentData.cooldownTimers)
            {
                timer.Value.FastForward((float) timePassedSinceSave.TotalSeconds);
            }
        }

        void IPersistent.OnLoadSkipped()
        {
        }

        void OnRoomSizeUpdated (string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupDataInfo)
        {
            onRoomSizeUpdated?.Invoke(roomName, roomData, roomSetupDataInfo);
        }
    }
}