using System.Linq;
using BridgeBinder;
using Data.Estate;
using Data.Room;
using Logic.FacadesBase;
using Logic.Rooms;
using Logic.Rooms.Facade;
using Presentation.Facades;
using UnityEngine;

namespace Logic.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Facades/Style Window")]
    [BindBridgeInterface(typeof (StyleWindowPresenter))]
    public class StyleWindowLogic : BaseLogicFacade
    {
        public string GetClubRoomName()
        {
            return gameCore.roomManager.GetRoomNameOfType(RoomType.Club);
        }

        public RoomCapacity GetRoomCapacity()
        {
            return gameCore.roomManager.roomCapacity;
        }

        public int GetClubSize()
        {
            var club = GetComponent<ILogicRoomManager>();
            return club.GetRoomSizeInCells(GetClubRoomName());
        }

        public int GetGymSizeStylePoints()
        {
            return GetRoomCapacity().GetTotalExpandStylePoints(GetClubRoomName());
        }

        public int GetDecorsCount()
        {
            return
                gameCore.equipmentManager.equipment
                    .Count(i => i.Value.estateType.itemType == EstateType.Subtype.Decor && i.Value.state == EquipmentState.Idle);
        }

        public int GetDecorsStyle()
        {
            return
                gameCore.equipmentManager.equipment.Where(
                    i => i.Value.estateType.itemType == EstateType.Subtype.Decor && i.Value.state == EquipmentState.Idle)
                    .Sum(i => i.Value.estateType.style);
        }
    }
}