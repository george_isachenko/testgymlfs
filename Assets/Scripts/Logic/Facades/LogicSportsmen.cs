﻿using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Persons;
using Logic.FacadesBase;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.Persons.Events;
using Core.Analytics;
using Data.Sport;
using Logic.SportAgency.Facade;
using Core.Timer;

namespace Logic.Facades
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Facades/Logic Sportsmen")]
    [BindBridgeInterface(typeof (PresentationSportsmen))]
    public class LogicSportsmen : BaseLogicFacade
    {
        public struct SportsmenSelection
        {
            public bool isTraining;
            public SportsmanType sportsmanType;
            public int count;
        }

        PresentationSportsmen       presenter;
        IPersonManager              manager;

        bool                        _isDirty = true;
        bool                        isDirty             { get { return _isDirty; }  set { _isDirty = value; if (value) presenter.UpdateHUD(); } }

        public IEnumerable<PersonSportsman> members     { get { return manager.GetSportsmen(); } }
        public string               countRatio          { get { return ActualSportsmenCount().ToString() + "/" + GetSportsmenCapacity().ToString(); } }
        public Cost                 expandCost          { get { return GetExpandCost(); } }
        public int                  nextCapacity        { get { return GetNextCapacity(); } }


        SportsmenStorageCapacity[]  tableCapacity       { get { return DataTables.instance.sportsmenStorageCapacity; } }
        int                         level               { get { return manager.sportsmenCapacityLevel; } set { manager.sportsmenCapacityLevel = value; } }

        public int SelectedCharacter                    {get { return manager.selectedCharacterId;}}
        public bool IsSportAgencyEnabled                {get { return gameCore.sportAgency.canUseMyFeature.canUse;}}
        public CanUseFeatureResult  agencyAvailable     { get { return gameCore.sportAgency.canUseMyFeature; } } 


        [BindBridgeSubscribe]
        private void Subscribe(PresentationSportsmen presenter)
        {
            this.presenter = presenter;
            manager = GetComponent<IPersonManager>();

            gameCore.sportsmanTraining.OnExerciseFinish += OnExerciseFinish;
        }

        void OnDestroy()
        {
            gameCore.sportsmanTraining.OnExerciseFinish -= OnExerciseFinish;
        }

        /////////////////////////////////////////
        /// DIRTY METHODS
        ////////////////////////////////////////
           
        public int CreateSportsman(SportsmanType sportsmanType, PersonAppearance.Gender gender, Analytics.MoneyInfo info, string name = null, string spawnZoneName = null)
        {
            Assert.IsNotNull(info);

            if (info.itemType != "")
                PersistentCore.instance.analytics.AddMoneyEvent("Sportsmen", 1, info.itemType, info.itemId);
            
            var person = manager.CreateSportsman(manager.gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub), Person.EntranceMode.ImmediateEntranceFast, sportsmanType, gender, name, spawnZoneName);
            presenter.UpdateHUD();
            return person.id;
        }

        public int CreateSportsman(SportsmanType sportsmanType, PersonAppearance appearance, string name = null, string spawnZoneName = null)
        {
            Assert.IsNotNull(appearance);

            var person = manager.CreateSportsman(manager.gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub), Person.EntranceMode.ImmediateEntranceFast, sportsmanType, appearance, name, spawnZoneName);
            presenter.UpdateHUD();
            return person.id;
        }

        public PersonSportsman BecameSportsman(PersonVisitor visitor, SportsmanType sportsmanType)
        {
            Assert.IsNotNull(visitor);

            var result = manager.BecameSportsman(visitor, sportsmanType, manager.gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub), Person.EntranceMode.ImmediateEntranceFast);
            presenter.UpdateHUD();
            return result;
        }

        public void Debug_CreateSportsman()
        {
            CreateSportsman(SportsmanType.Sportsman_0, PersonAppearance.Gender.Male, new Analytics.MoneyInfo("", ""), "Rabinovich");   
        }

        public int RemoveSportsmen (SportsmanType sportsmanType, int count, Analytics.MoneyInfo info)
        {
            int tCount = manager.RemoveSportsmen(sportsmanType, count);
            PersistentCore.instance.analytics.SpendMoneyEvent("Sportsmen", tCount, info.itemType, info.itemId);
            presenter.UpdateHUD();
            return tCount;
        }

        public void KillCharacterRequest(int id, Analytics.MoneyInfo info)
        {
            PersistentCore.instance.analytics.SpendMoneyEvent("Sportsmen", 1, info.itemType, info.itemId);
            manager.KillCharacterRequest(id);
            presenter.UpdateHUD();
        }

        public PersonSportsman GetMember(int id)
        {
            return manager.GetPerson(id) as PersonSportsman;
        }

        public int GetSportsmanCount(SportsmanType? sportsmanType, string roomName = null, params PersonState[] states)
        {
            return manager.GetSportsmanCount(sportsmanType, roomName, states);
        }

        public List<SportsmenSelection> GetSportsmenTypes(bool includeTraining = true)
        {
            return members
                .Where(m => m.state != PersonState.Sentenced &&
                            m.state != PersonState.Leaving &&
                            m.state != PersonState.InQueue && 
                            (includeTraining || m.isTraining == false))
                .Select(m => new
                {
                    isTraining = m.isTraining,
                    type = m.currentOrNextSportsmanType
                })
                .GroupBy(m => m.isTraining)
                .SelectMany(grouping => grouping
                    .GroupBy(m => m.type)
                    .Select(p => new SportsmenSelection()
                            {
                                isTraining = grouping.Key,
                                sportsmanType = p.Key,
                                count = p.Count()
                            })
                        )
                .ToList();
        }

        public PersonSportsman GetSportsman(SportsmanType? sportsmanType, string roomName = null, params PersonState[] states)
        {
            return manager.GetSportsman(sportsmanType, roomName, states);
        }

/*
        #if DEBUG
        public void Test_AddSpotsman()
        {
            manager.Test_AddSpotsman();
        }

        public void Test_AddSpotsman(SportsmanType type, int count)
        {
            manager.Test_AddSpotsman(type, count);
        }
        #endif
*/

        public int GetSportsmenCapacity()
        {
            if (level >= tableCapacity.Length)
                level = tableCapacity.Length - 1;

            return tableCapacity[level].capacity;
        }

        public bool CanExpand()
        {
            var sportClubAvailable = (gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) > 0);
            
            return sportClubAvailable  
                && ( (level + 1) < tableCapacity.Length);
        }

        public bool CanAdd()
        {
            var nonValidCount = GetSportsmanCount(null, null, PersonState.Leaving,  PersonState.Sentenced);
            
            return GetSportsmenCapacity() > (members.Count() - nonValidCount);
        }

        public void WarnSportRoomFull()
        {
            presenter.WarnSportRoomFull();
        }

        private void OnExerciseFinish()
        {
            presenter.UpdateView();
        }

        Cost GetExpandCost()
        {
            Assert.IsTrue(CanExpand());

            var row = tableCapacity[level + 1];

            var cost = new Cost();
            cost.type = Cost.CostType.ResourcesOnly;

			cost.AddResource(ResourceType.Storage_01, row.resource_1);
			cost.AddResource(ResourceType.Storage_02, row.resource_2);
			cost.AddResource(ResourceType.Storage_03, row.resource_3);
			cost.AddResource(ResourceType.Storage_04, row.resource_4);

            return cost;
        }

        int GetNextCapacity()
        {
            if ((level + 1) < tableCapacity.Length)
            {
                return tableCapacity[level + 1].capacity;
            }
            else
                return -1;
        }

        public void ShowExpandMemebersLimit()
        {
            presenter.ExpandMembersLimit();
        }

        public void BuyNextExpand()
        {
            if (CanExpand())
            {
                if (playerProfile.SpendMoney( expandCost, new Analytics.MoneyInfo("Expand Bench", "expand_" + (level + 1).ToString()), true ))
                {
                    level += 1;
                    //PersistentCore.instance.analytics.GameEvent("UpgradeSportBench");
                    PersistentCore.instance.analytics.GameEvent("SportBench Expand",
                        new GameEventParam("Level", gameCore.playerProfile.level),
                        new GameEventParam("Expand level", level.ToString()));
                }
            }
            else
            {
                Debug.LogError("LogicSportsmen::BuyNextExpand : can't expand");
            }
                
            presenter.HideExpandWindow();
            presenter.UpdateHUD();
        }

        int ActualSportsmenCount()
        {
            return members.Count(item => item.state != PersonState.Sentenced && item.state != PersonState.Leaving);
        }

        public int SelectNextSportsman(SportsmanType type, int index, bool isTraining)
        {
            int tcount = 0;
            PersonSportsman firstSportsman = null;
            foreach (var item in members)
            {
                var ttype = item.currentOrNextSportsmanType; // item.isTraining ? DataTables.instance.sportExercises.First(e => e.id == item.currentSportExerciseId).trainTo : item.sportsmanData.sportsmanType;
                
                if ((ttype == type) && (item.isTraining == isTraining))
                {
                    if (tcount == 0)
                        firstSportsman = item;

                    if (tcount >= index)
                    {
                        manager.SelectCharacterRequest(item.id);
                        return tcount + 1;
                    }

                    ++tcount; 
                }
            }

            if (firstSportsman != null)
            {
                manager.SelectCharacterRequest(firstSportsman.id);
                return 1;
            }

            return 0;                        
        }

        public bool IsSportsmensId(int id)
        {
            return members.Any(s => s.id == id);
        }

        public static Cost GetSportsmanCost(SportsmanType type)
        {
            var cost = new Cost();
            cost.type = Cost.CostType.BucksOnly;

            if (DataTables.instance.sportsmen.ContainsKey(type))
            {
                var coins = DataTables.instance.sportsmen[type].coins;
                cost.value = Cost.CoinsToFitBucks(coins);
            }
            else
            {
                Debug.LogError("IMPOSSIBRU: wrong sportsman type");
            }

            return cost;
        }

        public static Cost GetSportsmanCost(SportsmanCostUnit[] members)
        {
            var cost = new Cost();
            cost.type = Cost.CostType.BucksOnly;

            foreach (var member in members)
            {
                if (DataTables.instance.sportsmen.ContainsKey(member.sportsmanType))
                {
                    var bucks = DataTables.instance.sportsmen[member.sportsmanType].bucks;
                    cost.value += bucks;
                }
                else
                {
                    Debug.LogError("IMPOSSIBRU: wrong sportsman type");
                }
            }

            return cost;
        }

        public bool IsSportClubUnlockAvailable()
        {
            int expandStage = gameCore.roomManager.GetRoomExpandStage("SportClub");
            int unlockLvl = gameCore.roomManager.GetNextExpandUnlockLevel("SportClub");
            return playerProfile.level >= unlockLvl && expandStage == 0;
        }

        public int GetSportClubUnlockLevel()
        {
            return gameCore.roomManager.GetNextExpandUnlockLevel("SportClub");
        }

        public int GetExpandStage()
        {
            return gameCore.roomManager.GetRoomExpandStage("SportClub");
        }

        public TimerFloat GetUnlockTime()
        {
            return gameCore.roomManager.GetUnlockTimer("SportClub");
        }
    }
}
