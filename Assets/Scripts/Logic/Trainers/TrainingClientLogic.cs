﻿using System;
using Data.Person;
using Logic.Persons;

namespace Logic.Trainers
{
    public enum TrainerClientType
    {
        Locked,
        Empty,
        InProgress,
        Done,
    }

    public class TrainingClientLogic
    {
        #region Private data.
        PersonTrainerData personData;
        PersonTrainerClient client_;
        int idx;
        Func<int, bool> isLocked;
        #endregion

        #region Public properties.
        public int                  lvlUnlock   { private set; get; }
        public PersonTrainerClient  client      { set { SetClient(value); } get { return client_; } }

        public TrainerClientType    state       { get { return GetState(); } }

        public bool assigned            { get { return client != null; } }
        public bool completed           { get { return client != null ? (client.visitorQuest != null ? client.visitorQuest.completed : true) : false; } }
        public int  iterationsGoal      { get { return (client != null && client.visitorQuest != null) ? client.visitorQuest.iterationsGoal : 0; } }
        public int  iterationsDone      { get { return (client != null && client.visitorQuest != null) ? client.visitorQuest.iterationsDone : 0; } }
        #endregion

        #region Constructors.
        public TrainingClientLogic(PersonTrainerData personData, int idx, PersonTrainerClient client, Func<int, bool> isLocked, int lvlUnlock)
        {
            this.personData = personData;
            client_ = client;
            this.idx = idx;
            this.isLocked = isLocked;
            this.lvlUnlock = lvlUnlock;
        }
        #endregion

        #region Private methods.
        TrainerClientType GetState()
        {
            if (isLocked(idx))
                return TrainerClientType.Locked;

            if (client == null)
                return TrainerClientType.Empty;

            if (completed)
                return TrainerClientType.Done;
            else
                return TrainerClientType.InProgress;
        }

        private void SetClient (PersonTrainerClient client)
        {
            client_ = client;
            personData.clientIds[idx] = (client != null) ? client.id : PersonData.InvalidId;
        }
        #endregion
    }
}
