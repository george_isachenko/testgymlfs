﻿using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core.Analytics;
using Data;
using Data.Person;
using Data.Person.Info;
using Logic.FacadesBase;
using Logic.Persons;
using Logic.Quests.Tutorial;
using Presentation.Facades;
using UnityEngine;

namespace Logic.Trainers.Facade
{
    [AddComponentMenu("Kingdom/Logic/Achievements/Facade/Logic Trainers")]
    [DisallowMultipleComponent]
    [BindBridgeInterface(typeof(PresentationTrainers))]
    public class LogicTrainers : BaseLogicFacade 
    {
        #region Public properties.
        public PersonVisitor            visitorToAssign     { get; private set; }

        public IEnumerable<PersonTrainer> trainers          { get { return GetTrainers(); } }
        public bool                     goingToAssign       { get { return visitorToAssign != null; } }
        public bool                     availableSomeone    { get { return GetIsAvailableSomeone(); } }
        public int                      maxTrainersCount    { get { return DataTables.instance.trainerUnlocks.Length; } }
        public int                      nextLevelHire       { get { return NextLevelHire(); } }
        public Cost                     hireCost            { get { return HireCost(); } }
        #endregion

        #region Private properties.
        IPersonManager                  personManager       { get { return gameCore.personManager; } }
        #endregion

        #region Private data.
        //PresentationTrainers            presenter;
        #endregion

        #region Bridge API.
        [BindBridgeSubscribe]
        void Subscribe(PresentationTrainers presenter)
        {
            //this.presenter = presenter;
        }
        #endregion

        #region Unity API.
        void Start()
        {

        }
        #endregion

        #region Public API.
        public CanUseFeatureResult canUseMyFeature
        {
            get
            {
                if (playerProfile.level < DataTables.instance.trainerUnlocks[0].level)
                    return new CanUseFeatureResult(CanUseFeatureResult.ResultType.LockedNeededLevel);
                
                return new CanUseFeatureResult(CanUseFeatureResult.ResultType.CanUse);
            }
        }

        public bool HireTrainer()
        {
            if (!DebugHacks.disableTrainers && !gameCore.quests.tutorial.isRunning && !gameCore.quests.tutorial.forceBlockTutorialsStart)
            {
                var trainers = this.trainers.ToArray();

                string tName = "Trainer " + trainers.Count().ToString();
                TrainerName tn;
                if (DataTables.instance.trainerNames.TryGetValue(trainers.Count(), out tn))
                    tName = tn.name;

                var tutorialSequence = new TutorSequenceNewTrainer
                        ( gameCore
                        , gameCore.quests.tutorial.Presenter
                        , hireCost
                        , new Analytics.MoneyInfo("HireTrainer", "idx " + (trainers.Length + 1).ToString())
                        , tName);
                tutorialSequence.Init();
                gameCore.quests.tutorial.RunSequence(tutorialSequence);

                // presenter.UpdateTrainersView();
                return true;
            }

            return false;
        }

        public void RememberSelectedVisitor()
        {
            if (!DebugHacks.disableTrainers)
            {
                if (personManager.selectedCharacterId >= 0)
                    visitorToAssign = personManager.persons[personManager.selectedCharacterId] as PersonVisitor;
                else
                    visitorToAssign = null;
            }
        }

        public PersonTrainerClient AssignVisitorToTrainer(int trainerIdx)
        {
            if (!DebugHacks.disableTrainers)
            {
                if (visitorToAssign == null)
                {
                    Debug.LogError("AssignVisitorToTrainer : visitorToAssign == null");
                    return null;
                }

                var trns = GetTrainers().ToArray();

                if (trainerIdx < trns.Length)
                {
                    var trainer = trns[trainerIdx];
                    var trainerClient = trainer.AssignClient(visitorToAssign);
                    if (trainerClient != null)
                    {
                        visitorToAssign = null;
                        return trainerClient;
                    }
                    else
                        Debug.LogWarning("AssignVisitorToTrainer : can't assign visitor ");
                }
                else
                {
                    Debug.LogError("AssignVisitorToTrainer wring idx " + trainerIdx.ToString());
                }
            }

            return null;
        }

        public string BestWorkstationForTrainer(IPersonTrainerInfo trainer)
        {
            var locName = gameCore.shopUltimate.BestEquipmentForTrainerLevel(trainer.level).locNameId;

            return Loc.Get(locName);
        }

        public PersonTrainer SelectedTrainer()
        {
            var tPerson = personManager.persons[personManager.selectedCharacterId];
            if (tPerson is PersonTrainer)
                return tPerson as PersonTrainer;

            return null;
        }

        public bool IsTrainerClient(int id)
        {
            foreach (var trainer in trainers)
            {
                if (trainer.IsTrainingClient(id))
                    return true;
            }
            return false;
        }

        public PersonAppearance GetTrainerAppearance(int trainerIdx)
        {
            return gameCore.personManager.GetTrainerAppearance(trainerIdx);
        }
        #endregion

        #region Private methods.
        IEnumerable<PersonTrainer> GetTrainers()
        {
            return (from person in personManager.persons.Values
                where person is PersonTrainer
                select person as PersonTrainer);
        }

        Cost HireCost()
        {
            var unlock = CurrentTraunersUnlock();

            if (unlock != null)
            {
                return new Cost(unlock.price_type, unlock.hirePrice, unlock.level);
            }
            else
                Debug.LogError("LogicTrainers::HirePrice unlock == NULL");

            return new Cost(Cost.CostType.CoinsOnly, 100, 0);
        }

        int NextLevelHire()
        {
            var table =  DataTables.instance.trainerUnlocks;
            for (int i = 0; i < table.Length; i++)
            {
                var item = table[i];

                if (item.level > playerProfile.level)
                    return item.level;
            }

            return -1;
        }

        private TrainerUnlock CurrentTraunersUnlock()
        {
            var count = trainers.Count();
            var table =  DataTables.instance.trainerUnlocks;

            if (count < table.Length)
            {
                return table[count];
            }

            return null;
        }

        public bool IsAvailableWithSkill(int level)
        {
            var tr = GetTrainers();

            foreach (var trainer in tr)
            {
                if (trainer.level >= level && trainer.canAssign)
                    return true;
            }

            return false;
        }
            
        bool GetIsAvailableSomeone()
        {
            foreach(var trainer in trainers)
            {
                if (trainer.canAssign)
                    return true;
            }

            return false;
        }

        #endregion
    }
}
