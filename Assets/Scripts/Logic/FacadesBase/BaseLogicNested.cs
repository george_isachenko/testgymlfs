﻿using Logic.Core;
using Logic.Facades;
using Logic.PlayerProfile;

namespace Logic.FacadesBase
{
    public class BaseLogicNested
    {
        public BaseLogicNested(IGameCore gameCore)
        {
            _gameCore = gameCore;
        }

        IGameCore _gameCore;

        protected IGameCore gameCore { get { return _gameCore; } }

        public IPlayerProfileManager playerProfile   { get { return gameCore.playerProfile; } }
        public LogicSportsmen       sportsmen       { get { return gameCore.sportsmen; } }

        public bool isAnyTutorialRunning { get { return gameCore.quests.tutorial.isRunning; } }
    }
}
