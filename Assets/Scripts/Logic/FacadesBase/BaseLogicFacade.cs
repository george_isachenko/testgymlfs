﻿using UnityEngine;
using System.Collections;
using Logic.Core;
using UnityEngine.Assertions;
using System;
using Logic.PlayerProfile;
using Logic.Facades;
using Logic.Quests.Tutorial;

namespace Logic.FacadesBase
{
    public abstract class BaseLogicFacade : MonoBehaviour
    {
        #region Private fields.
        IGameCore _gameCore;
        #endregion

        #region Public properties.
        protected IGameCore             gameCore                { get { return GetGameCore(); } }

        public IPlayerProfileManager    playerProfile           { get { return gameCore.playerProfile; } }
        public LogicSportsmen           sportsmen               { get { return gameCore.sportsmen; } }
        public bool                     isAnyTutorialRunning    { get { return gameCore.quests.tutorial.isRunning; } }
        #endregion

        #region Public API.
        public void DelayedInvoke(Action action)
        {
            StartCoroutine(DelayedInvokeCoroutine(action));
        }

        public bool IsTutorialRunning<T>() where T : TutorSequenceBase
        {
            return gameCore.quests.tutorial.IsTutorialRunning<T>();
        }
        #endregion

        #region Private methods.
        IGameCore GetGameCore()
        {
            if (_gameCore != null)
                return _gameCore;
            
            _gameCore = GetComponent<IGameCore>();
            Assert.IsNotNull(_gameCore);

            return _gameCore;
        }

        private IEnumerator DelayedInvokeCoroutine(Action action)
        {
            yield return null;

            action?.Invoke();
        }
        #endregion
    }
}