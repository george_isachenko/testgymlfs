﻿using System;
using Data;
using Data.DailyBonus;
using Data.PlayerProfile;
using UnityEngine;

namespace Logic.Quests.DailyBonus
{
    public class DailyBonusItemLogicNew
    {
        public DailyBonusItemDataNew data;

        /*
        public Sprite   sprite      { get { return data.reward.sprite; } }
        //public bool     closed      { get { return data.closed; } set { data.closed = value; } }
        //public Cost     unlockPrice { get { return data.unlockPrice; } }
        public int      count       { get { return data.reward.value; } }
        //public bool     paid        { get { return data.paid; } set { data.paid = value; } }
        public Cost     reward      { get { return data.reward; } }
        public bool     claimed     { get { return data.claimed; } }
        */
        //public bool     isInited    { get { return count > 0; } }
        //public bool     isTop       { get { return data.isTop; } }

        public DailyBonusItemLogicNew()
        {
            data = new DailyBonusItemDataNew();
        }

        public void Generate(DailyBonusDay day)
        {
            var box = day.randomBox;
                
            var reward = box.randomReward;

            //data.isTop = box == day.mandatoryBox;
            data.reward = reward.reward;
            data.claimed = false;

            //data.unlockPrice.type    = Cost.CostType.BucksOnly;
            //data.unlockPrice.value   = 0;
        }

        public void Claim()
        {
            data.claimed = true;
        }

        Cost.CostType RandomCostType()
        {
            var cases = new Cost.CostType[] {Cost.CostType.BucksOnly, Cost.CostType.CoinsOnly, Cost.CostType.ResourcesOnly, Cost.CostType.ResourcesOnly, Cost.CostType.ResourcesOnly}; 

            var idx = UnityEngine.Random.Range(0, cases.Length);

            return cases[idx];
        }

        ResourceType RandomResourceType()
        {
            var sz = Enum.GetNames(typeof(ResourceType)).Length;
            var result = (ResourceType)UnityEngine.Random.Range(1, sz);

            if (result == ResourceType.None)
            {
                Debug.LogError("ACHTUNG");
            }

            return result;
        }

    }
}