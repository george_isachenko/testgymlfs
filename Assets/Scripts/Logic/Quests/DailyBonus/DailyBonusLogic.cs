﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Data;
using Logic.Core;
using Logic.FacadesBase;
using Logic.Serialization;
using Presentation.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using Logic.PlayerProfile.Events;
using Core.Analytics;
using Data.DailyBonus;

namespace Logic.Quests.DailyBonus
{
    public class DailyBonusLogic : BaseLogicNested, IPersistent
    {
        public PresentationDailyBonus presenter;
        private bool isFirstTime = false;
        private bool wasAvailable = false;
        NextDayDetector nextDayDetector = null;

        static readonly int itemsSize = 28;
        static readonly int daysCount = 28;

        public const int cSuperBonusPeriod = 7;

        struct PersistentData
        {
            public DailyBonusDataNew data;
            public DailyBonusItemLogicNew[] items;
        }

        #region Persistent data.
        private PersistentData          persistentData;
        #endregion

        #region Properties
        public DailyBonusItemLogicNew[] items               { get { return persistentData.items; } }
        public int                      currentDay          { get { return persistentData.data.dayNumber; } }
        public bool                     accepedByUser       { get { return persistentData.data.acceptedByUser; } }
        public TimeSpan timeToNextDay                       { get { return nextDayDetector.TimeToNextDay(); } }
        //Cost                            _mandatoryReward;
        //public Cost                    mandatoryReward     { get { return GetMandatoryReward(); } }
        #endregion

        #region IPersistent interface properties.
        string IPersistent.propertyName { get { return "DailyBonus"; } }

        Type IPersistent.propertyType { get { return typeof(PersistentData); } }
        #endregion

        // Cache
        int[] levelsList;

        public DailyBonusLogic(IGameCore gameCore, NextDayDetector nDayDetector)
            : base(gameCore)
        {
            nextDayDetector = nDayDetector;
            NewDayEvent.Event += OnNewDay;
            LevelUpEvent.Event += OnLevelUp;
            //items = new DailyBonusItemLogicNew[itemsSize];
        }

        public void Destroy()
        {
            NewDayEvent.Event -= OnNewDay;
            LevelUpEvent.Event -= OnLevelUp;
        }

        private void UpdateData()//(bool forceNew = false)
        {
            if (!IsAvailableByLevel()) 
                return;

            //if (items[0] != null && items[0].isInited && !forceNew)
            //    return;

            //var day = GetCurrentDay();

            for (int i = 0; i < items.Length; i++)
            {
                var day = GetDay(i);
                items[i] = new DailyBonusItemLogicNew();
                items[i].Generate(day);
            }
        }

        private void Generate()
        {
            persistentData.data = new DailyBonusDataNew();
            persistentData.items = new DailyBonusItemLogicNew[itemsSize];
            UpdateData();
        }

        private DailyBonusDay GetDay(int index_)
        {
            var level = GetNearestLevel();

            if (level == -1)
                return null;

            var days = from day
                in DataTables.instance.dailyBonusDays
                       where day.level == level
                       orderby day.dayIdx
                       select day;

            var dayArray = days.ToArray();

            return dayArray[index_];
        }

        private int GetNearestLevel()
        {
            if (levelsList == null)
            {
                Dictionary<int, int> lvlsDic = new Dictionary<int, int>();

                foreach (var day in DataTables.instance.dailyBonusDays)
                {
                    if (!lvlsDic.ContainsKey(day.level))
                    {
                        lvlsDic.Add(day.level, 0);
                    }
                }

                levelsList = lvlsDic.Keys.ToArray();
                Array.Sort(levelsList);
            }

            int prevLvl = -1;
            foreach (int lvl in levelsList)
            {
                if (lvl > gameCore.playerProfile.level)
                    return prevLvl;
                prevLvl = lvl;
            }

            return prevLvl;
        }

        bool InterruptBecauseOfFullStorage(int idx)
        {
            if (idx < 0 || idx >= items.Length)
            {
                Debug.LogError("DailyBonusLogic::Unlock : wrong idx");
                return true;
            }

            
            //Cost fullCost = null;
            //if (items[idx].reward.type == Cost.CostType.BucksOnly) // <-- Cost with bucks can't be merged other cost
            //    fullCost = mandatoryReward;
            //else
            //    fullCost = new Cost(items[idx].reward, mandatoryReward); 
                

            if (gameCore.playerProfile.storage.CanAcceptResourcesCompletely(items[idx].data.reward.resources))//(fullCost.resources))
            {
                return false;
            }
            else
            {
                ResolveEvents.StorageIsFull.Send(true);
                return true;
            }
        }

        public void Unlock(int idx)
        {
            if (InterruptBecauseOfFullStorage(idx))
                return;

            if (idx != currentDay)
                return;

            var item = items[idx];

            if (item.data.claimed)
                return;

            gameCore.playerProfile.AddReward(item.data.reward, new Analytics.MoneyInfo("DailyBonus", "Reward"), false);
            //gameCore.playerProfile.AddReward(mandatoryReward, new Analytics.MoneyInfo("DailyBonus", "MandatoryReward"), false);

            PersistentCore.instance.analytics.GameEvent("DailyBonusComplete",
                new GameEventParam("Level", gameCore.playerProfile.level),
                new GameEventParam("Reward", item.data.reward.type == Cost.CostType.ResourcesOnly ? item.data.reward.resourceType.ToString() : item.data.reward.type.ToString()));

            item.Claim();
            DailyBonusClaimed.Send();
            //IncrementPrice();
            persistentData.data.acceptedByUser = true;     
        }

        /*
        public void Buy(int idx)
        {
            if (InterruptBecauseOfFullStorage(idx))
                return;
            
            if (idx < items.Length)
            {
                var item = items[idx];

                if (gameCore.playerProfile.SpendMoney(item.unlockPrice, new Analytics.MoneyInfo("DailyBonus", "DailyBonus")))
                {
                    item.paid = false;
                    item.closed = false;
                    gameCore.playerProfile.AddReward(item.reward, new Analytics.MoneyInfo("DailyBonus", "DailyBonus"), false);
                    IncrementPrice();
                }
            }
            else
            {
                Debug.Log("DailyBonusLogic::Buy wrong idx");
            }
        }
        */

        /*
        public void IncrementPrice()
        {
            for (int i = 0; i < items.Length; i++)
            {
                items[i].unlockPrice.value += 4;
            }
        }
        */

        private void OnNewDay(int days)
        {

            if (isFirstTime) //--- skip update days if this happens first time 
            {
                isFirstTime = false;
                return;
            }

            persistentData.data.acceptedByUser = false;

            presenter.UpdateHUD();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        private void SetNextDay(int days_ = 1)
        {
            int tDay = persistentData.data.dayNumber;

            tDay += days_;

            if (tDay >= daysCount)
            {
                tDay = 0;
                UpdateData();
            }

            persistentData.data.dayNumber = tDay;
        }

        private void OnLevelUp(int level)
        {
            if (!wasAvailable && IsAvailableByLevel())
                Generate();
        }

        /*
        Cost GetMandatoryReward()
        {
            if (_mandatoryReward != null)
                return _mandatoryReward;

            _mandatoryReward = GetDay(persistentData.data.dayNumber).mandatoryBox.randomReward.reward;

            return _mandatoryReward;
        }
        */

        public void OnCloseByUserClick()
        {
            /*
            foreach (var item in items)
            {
                if (item != null && item.closed == false)
                {
                    persistentData.data.acceptedByUser = true;
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    Achievements.GetDailyBonus.Send();
                    return;
                };
            }
            */

            if (persistentData.data.acceptedByUser)
            {
                gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                Achievements.GetDailyBonus.Send();

                SetNextDay();
            }
        }

        public bool IsAvailableByLevel()
        {
            return GetNearestLevel() != -1;
        }

        public bool IsAvailable()
        {
            if (!IsAvailableByLevel())
                return false;

            if (persistentData.data.acceptedByUser)
                return false;

            return true;
        }
            
        #region IPersistent interface.
        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersistentData);
            if (obj is PersistentData)
            {
                persistentData = (PersistentData)obj;
                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad()
        {
            persistentData.data = new DailyBonusDataNew();
            persistentData.items = new DailyBonusItemLogicNew[itemsSize];
            //Generate();
        }

        void IPersistent.OnLoaded(bool success)
        {
            if (success)
            {
                
            }
            else
            {
                Generate();
            }
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            wasAvailable = IsAvailableByLevel();

            if (!IsAvailableByLevel())
                return;

            if (persistentData.items != null)
            {
                //--- check items
                for (int i = 0; i < itemsSize; ++i)
                {
                    if (items[i].data == null)
                    {
                        isFirstTime = true;
                        Generate();
                        return;
                    }
                }
            }
            else
            {
                isFirstTime = true;
                Generate();
            }

            //--- seek for last day
            int tDay = -1;
            for (int i = 0; i < itemsSize; ++i)
            {
                if (!items[i].data.claimed)
                {
                    tDay = i;
                    break;
                }
            }

            if (tDay < 0)
                Generate();
            else
                persistentData.data.dayNumber = tDay;
        }

        void IPersistent.OnLoadSkipped()
        {
            Generate();
        }
        #endregion
    }
}
