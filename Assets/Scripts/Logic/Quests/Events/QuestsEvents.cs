﻿using Core.EventSystem;
using Logic.Facades;
using Logic.Quests.Base;
using Logic.Quests.DailyQuest;
using Logic.Quests.SportQuest.SportLaneQuest;

namespace Logic.Quests.Events
{
    public class SubscribeTimeUpdate : StaticEvent<SubscribeTimeUpdate.Delegate, IBaseQuestLogicTime>
    {
        public delegate void Delegate (IBaseQuestLogicTime quest);
    }

    public class UnSubscribeTimeUpdate : StaticEvent<UnSubscribeTimeUpdate.Delegate, IBaseQuestLogicTime>
    {
        public delegate void Delegate (IBaseQuestLogicTime quest);
    }

    public class DailyQuestStartEvent : StaticEvent<DailyQuestStartEvent.Delegate, DailyQuestLogic>
    {
        public delegate void Delegate(DailyQuestLogic quest);
    }

    public class LaneQuestClaimedEvent : StaticEvent<LaneQuestClaimedEvent.Delegate, LaneQuestLogic, int>
    {
        public delegate void Delegate(LaneQuestLogic quest, int questIndex);
    }
}
