using System;
using Data.Person;
using Logic.Quests;
using Data;
using Logic.Quests.Base;
using Data.Quests.SportQuest;

namespace Logic.Quests.SportQuest
{
    public class SportObjectiveLogic<TObjectiveLogic> : BaseQuestObjectiveLogic<TObjectiveLogic>,
        IComparable<SportObjectiveLogic<TObjectiveLogic>>
        where TObjectiveLogic : SportObjectiveData
    {
        public delegate int IterationsDoneGetter (SportObjectiveLogic<TObjectiveLogic> objective);

        IterationsDoneGetter iterationsDoneGetter;

        public SportsmanType sportsmanType
        {
            get { return data.sportsmanType; }
        }

        public override int iterationsGoal
        {
            get { return data.iterationsGoal; }
        }

        public override int iterationsDone
        {
            get
            {
                return (iterationsDoneGetter != null)
                    ? Math.Max(0, Math.Min(iterationsGoal, iterationsDoneGetter (this)))
                    : base.iterationsDone;
            }
        }

        public SportObjectiveLogic(TObjectiveLogic data, IterationsDoneGetter iterationsDoneGetter)
            : base(data)
        {
            this.iterationsDoneGetter = iterationsDoneGetter;
        }

        public int CompareTo(SportObjectiveLogic<TObjectiveLogic> other)
        {
            return other.iterationsGoal - iterationsGoal;
        }
    }
}