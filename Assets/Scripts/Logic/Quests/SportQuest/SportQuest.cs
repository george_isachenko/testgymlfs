using System;
using System.Linq;
using Logic.Quests;
using Data;
using Logic.Quests.Base;
using Data.Quests.SportQuest;

namespace Logic.Quests.SportQuest
{
    public class SportQuest<TQuestData, TObjectiveLogic, TObjectiveData> 
        : BaseQuestLogicWithObjectives<TQuestData, TObjectiveLogic, TObjectiveData>
        , IComparable<SportQuest<TQuestData, TObjectiveLogic, TObjectiveData>>
        where TQuestData : SportQuestData
        where TObjectiveLogic : SportObjectiveLogic<TObjectiveData>
        where TObjectiveData : SportObjectiveData
    {
        public SportQuest(TQuestData data) : base(data)
        {
        }

        public string questTypeId
        {
            get { return data.questTypeId; }
            set { data.questTypeId = value; }
        }

        public SportQuestReward[] rewards
        {
            get { return data.rewards; }
        }

        public void ForceComplete()
        {
            while (!completed)
            {
                currentObjective.CompleteSteps(10);
            }
        }

        public new void AddObjective(TObjectiveLogic objectiveLogic)
        {
            base.AddObjective(objectiveLogic);
        }

        public int CompareTo(SportQuest<TQuestData, TObjectiveLogic, TObjectiveData> other)
        {
            return objectives.Sum(o => o.iterationsGoal) - other.objectives.Sum(o => o.iterationsGoal);
        }
    }
}