using Core.Timer;
using Data;
using Data.Quests.SportQuest;

namespace Logic.Quests.SportQuest
{
    public class SingleSportTimeQuest :
        SportQuest<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
    {
        public TimerFloat newQuestTimer
        {
            get { return data.newQuestTimer; }
            set { data.newQuestTimer = value; }
        }

        public SingleSportTimeQuest(SportTimeQuestData data) : base(data)
        {
        }

        public override bool Claim()
        {
            if (completed)
                data.newQuestTimer = new TimerFloat(60);
            return base.Claim();
        }
    }
}
