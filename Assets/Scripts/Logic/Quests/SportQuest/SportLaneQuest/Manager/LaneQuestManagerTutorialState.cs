using System.Collections.Generic;
using System.Linq;
using Core.Timer;
using Data;
using Data.Person;
using Data.Quests.SportQuest;
using Logic.Core;
using Logic.Quests;
using Logic.Quests.Base;
using UnityEngine;

namespace Logic.Quests.SportQuest.SportLaneQuest.Manager
{
    public class LaneQuestManagerTutorialState : LaneQuestManagerState
    {
        public LaneQuestManagerTutorialState(IGameCore gameCore) : base(gameCore)
        {
        }

        public override string debugStateName 
        {
            get
            {
                return "Tutorial";
            }
        }

        public override void Start(LaneQuestManager.LaneQuestPersistentData oldData)
        {
            persistentData = new LaneQuestManager.LaneQuestPersistentData();

            Debug.Log("Set tutorial data for lane quest");
            persistentData.questStateTimer = new TimerFloat(999999);

            var objectives = new List<SportObjectiveData>()
            {
                new SportObjectiveData()
                {
                    sportsmanType = SportsmanType.Sportsman_1,
                    iterationsGoal = 1
                }
            };

            persistentData.questDatas = new[] { GenerateQuestDataFromObjectives(objectives) };
            // persistentData.questDatas[0].questName = Loc.Get("laneQuestTutorialEvent");
            persistentData.questDatas[0].questTypeId = "LaneQuestTutorialEvent";
            for (var index = 0; index < persistentData.questDatas[0].rewards.Length; index++)
            {
                if (persistentData.questDatas[0].rewards[index].rewardType == SportQuestRewardType.Experience ||
                    persistentData.questDatas[0].rewards[index].rewardType == SportQuestRewardType.FitPoints)
                {
                    persistentData.questDatas[0].rewards[index].count *= objectives.Sum(o => o.iterationsGoal);
                }
            }

            persistentData.objectiveDatas = new[] { objectives.ToArray() };
            persistentData.questState = LaneQuestState.Tutorial;

            LoadQuestFromPersistent();

            laneQuestLogic.onQuestClaimed += OnQuestClaim;

            base.Start(oldData);
        }

        protected override void LoadQuestFromPersistent()
        {
            base.LoadQuestFromPersistent();

            if (persistentData != null && persistentData.questDatas.Length > 0)
            {
                // -- BEGIN: Old data compatibility fix.
                for (int i = 0; i < persistentData.questDatas.Length; i++)
                {
                    var data = persistentData.questDatas[i];
                    if (data.questTypeId == null || data.questTypeId == string.Empty)
                    {
                        data.questTypeId = "LaneQuestTutorialEvent";
                    }
                }
                // -- END.
            }
        }

        protected override SportLaneQuestData GenerateQuestDataFromObjectives(List<SportObjectiveData> objectives)
        {
            var rewards = new List<SportQuestReward>();
            /*var availableRewardTypes = new List<SportQuestRewardType>()
            {
                SportQuestRewardType.Experience,
                SportQuestRewardType.Resource,
            };*/

            var availableResources = DataTables.instance.balanceData.Storage.Resources
                .Where(x => !x.Value.disabled && x.Value.availableAsSportLaneQuestReward).Select(x => x.Key).ToArray();

            var coinsCount =
                DataTables.instance.sportsmen
                    .Where(p => objectives.Any(o => o.sportsmanType == p.Key))
                    .Select(
                        p =>
                            new
                            {
                                type = p.Key,
                                cost = p.Value.coins,
                                goalCount = objectives.Sum(o => o.sportsmanType == p.Key ? o.iterationsGoal : 0)
                            })
                    .Sum(p => p.goalCount * p.cost);
            var expCount = (int)(coinsCount / 8);
            var randomRes = availableResources[Random.Range(0, availableResources.Length)];

            rewards.Add(new SportQuestReward()
            {
                count = coinsCount,
                rewardType = SportQuestRewardType.Coin
            });

            rewards.Add(new SportQuestReward()
            {
                count = expCount,
                rewardType = SportQuestRewardType.Experience
            });

            rewards.Add(new SportQuestReward()
            {
                count = 1,
                rewardType = SportQuestRewardType.Resource,
                itemId = randomRes
            });

            rewards.Add(new SportQuestReward()
            {
                count = 5,
                rewardType = SportQuestRewardType.Fitbucks
            });

            rewards.Sort();
            return new SportLaneQuestData
            {
                questTypeId = string.Empty,
                rewards = rewards.ToArray()
            };
        }

        private void OnQuestClaim(BaseQuestLogic<SportLaneQuestData> quest)
        {
            onStateCompleted.Invoke();
        }

        public override void Release()
        {
            if (laneQuestLogic != null)
                laneQuestLogic.onQuestClaimed -= OnQuestClaim;
        }
    }
}