using System;
using System.Collections.Generic;
using Core.Timer;
using Logic.Core;
using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;
using Data;
using Newtonsoft.Json;
using Logic.Quests.Tutorial;
using Data.Quests.SportQuest;

namespace Logic.Quests.SportQuest.SportLaneQuest.Manager
{
    public class LaneQuestManager
    {
        public class LaneQuestPersistentData
        {
            [JsonProperty(PropertyName = "fisrtEnter")] // There was typo in property name. Keep it. :-/
            public bool firstEnter = true;
            public SportLaneQuestData[] questDatas;
            public SportObjectiveData[][] objectiveDatas;
            public LaneQuestState questState;
            public TimerFloat questStateTimer;
            public bool isLastProgressAccumulationFailed;
        }

        public readonly UnityEvent onQuestsUpdated = new UnityEvent();
        public Func<bool> canAutoFinishCooldown = () => false;

        public bool isCurentQuestTutorial
        {
            get { return currentState == LaneQuestState.Tutorial; }
        }

        private bool readyToFininshCooldownState = false;

        private LaneQuestPersistentData persistentData
        {
            get { return states[currentState].persistentData; }
        }

        private readonly Dictionary<LaneQuestState, LaneQuestManagerState> states =
            new Dictionary<LaneQuestState, LaneQuestManagerState>();

        private LaneQuestState currentState = LaneQuestState.Tutorial;

        public LaneQuestManager(IGameCore gameCore)
        {
            states.Add(LaneQuestState.Tutorial, new LaneQuestManagerTutorialState(gameCore));
            states.Add(LaneQuestState.WaitForNewQuests, new LaneQuestManagerCooldownState(gameCore));
            states.Add(LaneQuestState.ProgressAccumulation, new LaneQuestManagerCollectProgressState(gameCore));

            foreach (var state in states)
            {
                if (state.Value is LaneQuestManagerCooldownState)
                {
                    state.Value.onStateCompleted.AddListener(() =>
                    {
                        if (canAutoFinishCooldown != null && canAutoFinishCooldown())
                            SwitchQuestState();
                        else
                            readyToFininshCooldownState = true;
                        onQuestsUpdated.Invoke();
                    });
                }
                else
                {
                    state.Value.onStateCompleted.AddListener(SwitchQuestState);
                }

                state.Value.onQuestLogicUpdated.AddListener(onQuestsUpdated.Invoke);
            }

            SwitchQuestState(LaneQuestState.Tutorial, null);
        }

        public LaneQuestLogic GetCurrentLaneQuest()
        {
            return states[currentState].laneQuestLogic;
        }

        public bool FinishCooldownStateIfCan()
        {
            lock (this)
            {
                if (currentState != LaneQuestState.WaitForNewQuests || !readyToFininshCooldownState)
                    return false;

                readyToFininshCooldownState = false;
                SwitchQuestState();
                return true;
            }
        }

        private void SwitchQuestState()
        {
            var next = currentState == LaneQuestState.Tutorial || currentState == LaneQuestState.ProgressAccumulation
                ? LaneQuestState.WaitForNewQuests
                : LaneQuestState.ProgressAccumulation;
            var prev = states[currentState];
            SwitchQuestState(next, prev.persistentData);
        }

        private void SwitchQuestState(LaneQuestState next, LaneQuestPersistentData data)
        {
            Debug.LogFormat("LaneQuests state machine: release {0} state and start {1} state.", currentState, next);
            states[currentState].Release();
            currentState = next;
            states[currentState].Start(data);
        }

        public void RestoreData(LaneQuestPersistentData laneQuestPersistentData)
        {
            Debug.Log("Lane quest data restored");
            SwitchQuestState(laneQuestPersistentData.questState, laneQuestPersistentData);
        }

        public LaneQuestPersistentData GetPersistentData()
        {
            return persistentData;
        }

        public void PushTimer(TimeSpan timeSpan)
        {
            var cur = states[currentState];
            cur.FastForwardTimer((float) timeSpan.TotalSeconds);
        }

        public void StartTutorialQuest()
        {
            SwitchQuestState(LaneQuestState.Tutorial, null);
        }
    }
}