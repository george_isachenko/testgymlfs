
//#define DEBUG_TIMERS

using System;
using System.Collections;
using System.Linq;
using Core.Timer;
using Data.Quests.SportQuest;
using Logic.Core;
using Logic.Quests;
using Logic.Quests.Base;
using UnityEngine;
using Core;
using Core.Analytics;

namespace Logic.Quests.SportQuest.SportLaneQuest.Manager
{
    public class LaneQuestManagerCollectProgressState : LaneQuestManagerState
    {
#if DEBUG_TIMERS
        private static readonly Func<int, float> CollectingTimeGetter = (questsCount) => 30;
#else
        private static readonly Func<int, float> CollectingTimeGetter = (questsCount) => questsCount * 60 * 60 * 24;
#endif

        public LaneQuestManagerCollectProgressState(IGameCore gameCore) : base(gameCore)
        {
        }

        public override string debugStateName
        {
            get { return "Collect progress"; }
        }

        private bool isStateRestored = false;

        public override void Start(LaneQuestManager.LaneQuestPersistentData oldData)
        {
            if (oldData.questState == LaneQuestState.ProgressAccumulation)
            {
                isStateRestored = true;
                persistentData = oldData;
                LoadQuestFromPersistent();
                laneQuestLogic.onQuestClaimed += OnQuestClaim;
                StartCoroutine(StateTimer());

                return;
            }

            isStateRestored = false;
            persistentData = oldData;
            var collectionTime = CollectingTimeGetter(persistentData.objectiveDatas.Length);
            persistentData.questStateTimer = new TimerFloat(collectionTime);
            persistentData.questState = LaneQuestState.ProgressAccumulation;
            LoadQuestFromPersistent();

            laneQuestLogic.onQuestClaimed += OnQuestClaim;
            StartCoroutine(StateTimer());

            gameCore.sportLaneQuest.ResetSequenceReward();

            base.Start(oldData);
        }

        private void OnQuestClaim(BaseQuestLogic<SportLaneQuestData> quest)
        {
            if (laneQuestLogic.GetQuestList().All(q => q.claimed))
            {
                gameCore.sportLaneQuest.GenerateSequenceReward();
                persistentData.isLastProgressAccumulationFailed = false;
                onStateCompleted.Invoke();
            }
        }

        private IEnumerator StateTimer()
        {
            while (persistentData.questStateTimer != null && 
                persistentData.questStateTimer.secondsToFinish > 0)
            {
                yield return null;
            }
//            yield return new WaitForSeconds(persistentData.questStateTimer.secondsToFinish);
            Debug.Log("LaneQuests: state progress accumulation failed by timer...");
            persistentData.isLastProgressAccumulationFailed = true;
            onStateCompleted.Invoke();
        }

        public override void Release()
        {
            StopAllCoroutines();
            laneQuestLogic.onQuestClaimed -= OnQuestClaim;
        }

        public override void FastForwardTimer(float totalSeconds)
        {
            if (persistentData == null)
                return;

            if (persistentData.questStateTimer == null)
                return;

            persistentData.questStateTimer.FastForward(totalSeconds);
            if (persistentData.questStateTimer.secondsToFinish <= 1)
            {
                if (isStateRestored)
                {
                    Debug.Log("LaneQuests: state progress accumulation failed by timer...");
                    persistentData.isLastProgressAccumulationFailed = true;
                    onStateCompleted.Invoke();

                    PersistentCore.instance.analytics.GameEvent("Sport Lane Quest Failed", new GameEventParam("Level", gameCore.playerProfile.level)); 
                }
                else
                {
                    onStateCompleted.Invoke();
                }
            }
        }

        protected override void LoadQuestFromPersistent()
        {
            base.LoadQuestFromPersistent();

            if (persistentData != null && persistentData.questDatas.Length > 0)
            {
                // -- BEGIN: Old data compatibility fix.
                for (int i = 0; i < persistentData.questDatas.Length; i++)
                {
                    var data = persistentData.questDatas[i];
                    if (data.questTypeId == null || data.questTypeId == string.Empty)
                    {
                        data.questTypeId = QuestTypeIds[i];
                    }
                }
                // -- END.
            }
        }
    }
}