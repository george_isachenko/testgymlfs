using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data;
using Data.PlayerProfile;
using Data.Quests.SportQuest;
using Logic.Core;
using Logic.Quests;
using Logic.Quests.Tutorial;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Logic.Quests.SportQuest.SportLaneQuest.Manager
{
    public abstract class LaneQuestManagerState
    {
        public virtual string debugStateName { get; }
        public UnityEvent onStateCompleted { get; private set; }
        public UnityEvent onQuestLogicUpdated { get; private set; }

        public LaneQuestLogic laneQuestLogic
        {
            get { return laneQuestLogicBuffer; }
            protected set
            {
                laneQuestLogicBuffer = value; 
                onQuestLogicUpdated.Invoke();
            }
        }

        private class CoroutinerDummyBehaviour : MonoBehaviour
        {
        }

        public LaneQuestManager.LaneQuestPersistentData persistentData { get; protected set; }

        protected readonly IGameCore gameCore;
        private LaneQuestLogic laneQuestLogicBuffer;
        private MonoBehaviour coroutinerInstance;

        protected static readonly string[] QuestTypeIds =
        {
            "LaneQuestLocalEvent",
            "LaneQuestCityEvent",
            "LaneQuestCountryEvent",
            "LaneQuestContinentalEvent",
            "LaneQuestWorldEvent"
        };

        private MonoBehaviour coroutiner
        {
            get
            {
                lock (this)
                {
                    if (coroutinerInstance == null)
                    {
                        var go = new GameObject("~LaneQuestsCoroutiner");
                        coroutinerInstance = go.AddComponent<CoroutinerDummyBehaviour>();
                    }
                    return coroutinerInstance;
                }
            }
        }

        protected LaneQuestManagerState(IGameCore gameCore)
        {
            this.gameCore = gameCore;
            this.debugStateName = "AHTUNG: wrong lane quest manager state!";
            this.onStateCompleted = new UnityEvent();
            this.onQuestLogicUpdated = new UnityEvent();
        }

        public virtual void Start(LaneQuestManager.LaneQuestPersistentData oldData)
        {
            // gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        public virtual void Release() { }

        protected virtual SportLaneQuestData GenerateQuestDataFromObjectives(List<SportObjectiveData> objectives)
        {
            var rewards = new List<SportQuestReward>();
            var availableResources = DataTables.instance.balanceData.Storage.Resources
                .Where(x => !x.Value.disabled && x.Value.availableAsSportLaneQuestReward).Select(x => x.Key).ToArray();
            
            var coinsCount =
                DataTables.instance.sportsmen
                    .Where(p => objectives.Any(o => o.sportsmanType == p.Key))
                    .Select(
                        p =>
                            new
                            {
                                type = p.Key,
                                cost = p.Value.coins,
                                goalCount = objectives.Sum(o => o.sportsmanType == p.Key ? o.iterationsGoal : 0)
                            })
                    .Sum(p => p.goalCount * p.cost);
            var fitPoints = Mathf.Max((int)((coinsCount+1)*.01f),1);
            var expCount = (int)(coinsCount / 3.2);
            var randomRes = availableResources[Random.Range(0, availableResources.Length)];

            var sportsmenTotalInObjectives = objectives.Sum( o => o.iterationsGoal);
            var platinumCount = Mathf.CeilToInt(sportsmenTotalInObjectives / 4f);
            
            rewards.Add(new SportQuestReward()
            {
                count = coinsCount,
                rewardType = SportQuestRewardType.Coin
            });

            rewards.Add(new SportQuestReward()
            {
                count = fitPoints,
                rewardType = SportQuestRewardType.FitPoints
            });

            rewards.Add(new SportQuestReward()
            {
                count = expCount,
                rewardType = SportQuestRewardType.Experience
            });

            rewards.Add(new SportQuestReward()
            {
                count = 1,
                rewardType = SportQuestRewardType.Resource,
                itemId = randomRes
            });

            rewards.Add(new SportQuestReward()
                {
                    count = platinumCount,
                    rewardType = SportQuestRewardType.Resource,
                    itemId = ResourceType.Platinum
                });

            rewards.Sort();
            return new SportLaneQuestData
            {
                questTypeId = string.Empty,
                rewards = rewards.ToArray()
            };
        }

        protected virtual void LoadQuestFromPersistent()
        {
            if (persistentData == null)
            {
                Debug.LogError("Persistent data not ready for load, we can't load lane quest!");
                return;
            }

            Debug.Log("Loading lane quest from persistent data...");

            for (var i = 0; i < persistentData.questDatas.Length; i++)
            {
                if (persistentData.questDatas[i].questTypeId == null)
                    persistentData.questDatas[i].questTypeId = string.Empty;
            }

            var lane = new LaneQuestLogic
            {
                currentStateTimer = persistentData.questStateTimer.secondsToFinish > 0 ? persistentData.questStateTimer : null,
                state = persistentData.questState,
                isLastProgressAccumulationFailed = persistentData.isLastProgressAccumulationFailed
            };

            for (var i = 0; i < persistentData.questDatas.Length; i++)
            {
                var quest = new SingleSportLaneQuest(persistentData.questDatas[i]);
                for (var j = 0; j < persistentData.objectiveDatas[i].Length; j++)
                {
                    quest.AddObjective(new SportObjectiveLogic<SportObjectiveData>(persistentData.objectiveDatas[i][j], gameCore.sportLaneQuest.GetIterationsDone));
                }
                lane.AddQuest(quest);
            }

            lane.onQuestClaimed += (a) => onQuestLogicUpdated.Invoke();
            laneQuestLogic = lane;
        }

        public virtual void FastForwardTimer(float totalSeconds)
        {
            if (persistentData == null)
                return;
            
            if (persistentData.questStateTimer == null)
                return;

            persistentData.questStateTimer.FastForward(totalSeconds);
            if (persistentData.questStateTimer.secondsToFinish<=1)
                onStateCompleted.Invoke();
        }

        protected void StartCoroutine(IEnumerator coroutine)
        {
            coroutiner.StartCoroutine(coroutine);
        }
        protected void StopAllCoroutines()
        {
            coroutiner.StopAllCoroutines();
        }
    }
}