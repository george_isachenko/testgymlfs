
// #define DEBUG_TIMERS

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Timer;
using Data;
using Data.Person;
using Logic.Core;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;
using Core;
using Core.Analytics;
using Data.Quests.SportQuest;

namespace Logic.Quests.SportQuest.SportLaneQuest.Manager
{
    public class LaneQuestManagerCooldownState : LaneQuestManagerState
    {
#if DEBUG_TIMERS
        private static readonly Func<int, float> CooldownTimeGetter = (questsCount) => 30;
        private static readonly float afterTutorialCooldownTime = 10;
#else
        private static readonly Func<int, float> CooldownTimeGetter = (questsCount) => questsCount * 60 * 60;
        private static readonly float afterTutorialCooldownTime = 60;
#endif

        public LaneQuestManagerCooldownState(IGameCore gameCore) : base(gameCore)
        {
            
        }

        public override string debugStateName
        {
            get { return "Cooldown"; }
        }

        public override void Start(LaneQuestManager.LaneQuestPersistentData oldData)
        {
            if (oldData.questState == LaneQuestState.WaitForNewQuests)
            {
                persistentData = oldData;
                LoadQuestFromPersistent();
                StartCoroutine(StateTimer());
                return;
            }

            GenerateCooldownPersistentData(oldData);

            LoadQuestFromPersistent();
            StartCoroutine(StateTimer());

            base.Start(oldData);

            var reward = gameCore.sportLaneQuest.sequenceReward;
            if (reward != null)
            {
                gameCore.playerProfile.AddReward(reward, new Analytics.MoneyInfo("LaneQuestReward", ""), true, false);
            }
        }

        private void GenerateCooldownPersistentData(LaneQuestManager.LaneQuestPersistentData oldData)
        {
            Debug.Log("Generate data for lane quest with state 'WaitForNewQuests'");

            persistentData = new LaneQuestManager.LaneQuestPersistentData
            {
                firstEnter = oldData.firstEnter,
                isLastProgressAccumulationFailed = oldData.isLastProgressAccumulationFailed
            };

            var questCount = CalculateQuestCountByLevel(gameCore.playerProfile.level);
            var cooldownTime = persistentData.firstEnter ? afterTutorialCooldownTime : CooldownTimeGetter(questCount);
            persistentData.questStateTimer = new TimerFloat(cooldownTime);

            var availableSportmanTypes = DataTables.instance.sportExercises
                .Where(e => e.leverRequired <= gameCore.playerProfile.level)
                .Select(e => e.trainTo)
                .Distinct().ToList();
            availableSportmanTypes.Sort((a, b) => Random.Range(-1, 2));
            availableSportmanTypes = availableSportmanTypes.Take(4).ToList();

            var questDatas = new List<SportLaneQuestData>();
            var objectiveDatas = new List<SportObjectiveData[]>();

            for (var i = 0; i < questCount; i++)
            {
                var availableSportmans = new List<SportsmanType>(availableSportmanTypes);
                var objecttivesCount = Mathf.Min(Random.Range(1, 5), availableSportmans.Count);
                var objectives = Enumerable.Repeat(0, objecttivesCount).Select(x =>
                {
                    var type = availableSportmans[Random.Range(0, availableSportmans.Count)];
                    var minCount = 1;
                    var exercise = DataTables.instance.sportExercises.FirstOrDefault(e => e.trainTo == type);
                    var maxCount = 1;
                    if (exercise == null)
                    {
                        Debug.LogErrorFormat(
                            "Can't find exercise for training sportsman {0} and calculate count for lane quest objective. Set max objectives count as 1 immediately.");
                    }
                    else
                    {
                        maxCount =
                            ClampMaxObjectivesCountByLevel(
                                (int) Mathf.Min(6, 720f/(float) exercise.duration.TotalMinutes - 1));
                    }
                    var count = Random.Range(minCount, maxCount + 1);

                    availableSportmans.Remove(type);
                    var objectiveData = new SportObjectiveData()
                    {
                        sportsmanType = type,
                        iterationsGoal = count
                    };
                    return objectiveData;
                }).ToList();
                questDatas.Add(GenerateQuestDataFromObjectives(objectives));
                objectives.Sort();
                objectiveDatas.Add(objectives.ToArray());
            }
            // just sorting and post updating there
            var quests =
                questDatas.Select(
                    (d, index) =>
                        new KeyValuePair<SportLaneQuestData, SportObjectiveData[]>(d, objectiveDatas[index])).ToList();
            quests.Sort((a, b) => a.Key.rewards.Sum(r => r.count) - b.Key.rewards.Sum(r => r.count));

            for (var i = 0; i < quests.Count; i++)
            {
                quests[i].Key.questTypeId = QuestTypeIds[i];
            }
            persistentData.questDatas = quests.Select(e => e.Key).ToArray();
            persistentData.objectiveDatas = quests.Select(e => e.Value).ToArray();
            persistentData.firstEnter = false;
            persistentData.questState = LaneQuestState.WaitForNewQuests;
        }

        private IEnumerator StateTimer()
        {
            while (persistentData.questStateTimer != null &&
                persistentData.questStateTimer.secondsToFinish > 0)
            {
                yield return null;
            }
//            yield return new WaitForSeconds(persistentData.questStateTimer.secondsToFinish);
            Debug.Log("LaneQuests: cooldown state finished, let's go new state");
            onStateCompleted.Invoke();
        }

        public override void Release()
        {
            StopAllCoroutines();
        }

        private int ClampMaxObjectivesCountByLevel(int level)
        {
            var byLevelMax = 1;
            if (gameCore.playerProfile.level > 12) byLevelMax = 2;
            if (gameCore.playerProfile.level > 14) byLevelMax = 3;
            if (gameCore.playerProfile.level > 17) byLevelMax = 4;
            if (gameCore.playerProfile.level > 20) byLevelMax = 6;
            if (gameCore.playerProfile.level > 25) byLevelMax = 8;
            return Mathf.Min(level, byLevelMax);
        }

        private int CalculateQuestCountByLevel(int level)
        {
            Assert.IsNotNull(DataTables.instance.laneQuestsCounts);
            if (level < DataTables.instance.laneQuestsCounts[0].startLevel) return 0;
            for (var i = DataTables.instance.laneQuestsCounts.Length - 1; i >= 0; i--)
            {
                if (level >= DataTables.instance.laneQuestsCounts[i].startLevel)
                    return DataTables.instance.laneQuestsCounts[i].questsCount;
            }
            Debug.LogErrorFormat("Can't find lane quests count for level {0}, will be 0!", level);
            return 0;
        }

        protected override void LoadQuestFromPersistent()
        {
            if (persistentData == null)
            {
                Debug.LogError("Persistent data not ready for load, we can't load lane quest!");
                return;
            }

            Debug.Log("Loading lane quest from persistent data...");
            var lane = new LaneQuestLogic
            {
                currentStateTimer = persistentData.questStateTimer.secondsToFinish > 0 ? persistentData.questStateTimer : null,
                state = persistentData.questState,
                isLastProgressAccumulationFailed = persistentData.isLastProgressAccumulationFailed
            };

//            Assert.IsTrue(persistentData.questDatas.Length > 0);
            if (persistentData.questDatas.Length > 0)
            {
                // -- BEGIN: Old data compatibility fix.
                for (int i = 0; i < persistentData.questDatas.Length; i++)
                {
                    var data = persistentData.questDatas[i];
                    if (data.questTypeId == null || data.questTypeId == string.Empty)
                    {
                        data.questTypeId = QuestTypeIds[i];
                    }
                }
                // -- END.

                var quest = new SingleSportLaneQuest(persistentData.questDatas[0]);
                var objectives = persistentData.objectiveDatas.SelectMany(o => o)
                        .GroupBy(o => o.sportsmanType)
                        .Select(
                            p =>
                                new SportObjectiveData()
                                {
                                    sportsmanType = p.Key,
                                    iterationsGoal = 10
                                });
                foreach (var objective in objectives)
                {
                    quest.AddObjective(new SportObjectiveLogic<SportObjectiveData>(objective, gameCore.sportLaneQuest.GetIterationsDone));
                }
                lane.AddQuest(quest);
            }

            laneQuestLogic = lane;
        }
    }
}