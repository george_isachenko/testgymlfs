using Core.Timer;
using Data.Quests.SportQuest;
using Logic.Facades;

namespace Logic.Quests.SportQuest.SportLaneQuest
{
    public class LaneQuestLogic : QuestScope<SingleSportLaneQuest, SportLaneQuestData>
    {
        public LaneQuestState state;
        public TimerFloat currentStateTimer;
        internal bool isLastProgressAccumulationFailed;

        public float questsCount
        {
            get { return quests.Count; }
        }
    }

    public enum LaneQuestState
    {
        Tutorial,
        ProgressAccumulation,
        WaitForNewQuests
    }
}