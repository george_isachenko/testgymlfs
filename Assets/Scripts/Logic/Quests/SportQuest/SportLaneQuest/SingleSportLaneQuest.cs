﻿using System.Linq;
using Data;
using Data.Person;
using Data.Quests.SportQuest;
using Data.Sport;
using Logic.Facades;

namespace Logic.Quests.SportQuest.SportLaneQuest
{
    public class SingleSportLaneQuest :
        SportQuest<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
    {
        #region Public API.
        public SingleSportLaneQuest(SportLaneQuestData data) : base(data)
        {
        }

        public SportsmanCostUnit[] GetSporsmenShortage(LogicSportsmen sportsmanManager)
        {
            var objectivesSelected = objectives.Where( o => o.iterationsGoal > sportsmanManager.GetSportsmanCount(o.sportsmanType, null,
                PersonState.Idle,
                //PersonState.InQueue,
                PersonState.Entering,
                PersonState.ExerciseComplete)).ToArray();

            var list = new SportsmanCostUnit[objectivesSelected.Length];

            for (int i = 0; i < objectivesSelected.Length; i++)
            {
                var sportsmenCount = sportsmanManager.GetSportsmanCount(objectivesSelected[i].sportsmanType, null, PersonState.Idle, PersonState.Entering, PersonState.ExerciseComplete);
                    
                list[i].sportsmanType = objectivesSelected[i].sportsmanType;
                list[i].count = objectivesSelected[i].iterationsGoal - sportsmenCount;
            }

            return list;
        }
        #endregion
    }
}
