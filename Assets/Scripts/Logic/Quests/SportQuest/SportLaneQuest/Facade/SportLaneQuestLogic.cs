using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BridgeBinder;
using Core;
using Core.Analytics;
using Data;
using Data.Person;
using Data.Quests.SportQuest;
using Data.Room;
using Data.Sport;
using Logic.Facades;
using Logic.FacadesBase;
using Logic.Quests.Events;
using Logic.Quests.SportQuest.SportLaneQuest.Manager;
using Logic.Quests.Tutorial;
using Logic.Rooms.Events;
using Logic.Serialization;
using Logic.SportAgency.Facade;
using Presentation.Facades;
using UnityEngine;

namespace Logic.Quests.SportQuest.SportLaneQuest.Facade
{
    [AddComponentMenu("Kingdom/Logic/Facades/Sport Lane Quest")]
    [BindBridgeInterface(typeof(SportLaneQuestPresenter))]
    public class SportLaneQuestLogic
        : BaseLogicFacade, IPersistent
    {
        SportLaneQuestPresenter presenter;

        LaneQuestManager    questManagerInstance;
        LaneQuestManager    questManager        { get { return questManagerInstance ?? (questManagerInstance = new LaneQuestManager(gameCore)); } } 

        public event Action onQuestsUpdated;
        public event Action<string, RoomData, IRoomSetupDataInfo> onRoomSizeUpdated;
        public event Action OnExerciseFinish
        {
            add { gameCore.sportsmanTraining.OnExerciseFinish += value; }
            remove { gameCore.sportsmanTraining.OnExerciseFinish -= value; }
        }

        public bool                 isClubInEditMode            { get { return gameCore.equipmentManager.isInEditMode; } }
        public bool                 isTutorialQuest             { get { return questManager.isCurentQuestTutorial; } }
        public LogicSportsmen       sportsmanManager            { get { return gameCore.personManager.sportsmen; } }
        public LaneQuestLogic       currentLaneQuest            { get { return questManager.GetCurrentLaneQuest(); } }
        public bool                 canUseMyFeature             { get { return gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) > 0; } }
        public LogicSportAgency     sportAgencyLogic            { get { return gameCore.sportAgency; } }
        public string               propertyName                { get { return propertyType.Name; } }
        public Type                 propertyType                { get { return typeof(LaneQuestManager.LaneQuestPersistentData); } }
        public bool                 isLaneQuestTutorialRunning  { get { return gameCore.quests.tutorial.IsTutorialRunning<TutorSequenceLaneQuests>(); } }

        public float                progress                    { get { return GetProgress(); } }

        List<SingleSportLaneQuest>  quests                      { get { return currentLaneQuest.GetQuestList(); } }

        Cost                        _sequenceReward;              
        public Cost                 sequenceReward              { get { return _sequenceReward; } }

        #region Private fields.
        IEnumerator                 destroyRewardJob;
        #endregion
        
        #region Unity API.
        void Start()
        {
            RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;

            questManager.onQuestsUpdated.AddListener(() =>
            {
                onQuestsUpdated?.Invoke();
            });
        }

        void OnDestroy()
        {
            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;

            foreach (var quest in quests)
                quest.UnSubscribe();

            presenter = null;
        }
        #endregion

        #region Bridge interface.
        [BindBridgeSubscribe]
        private void Subscribe(SportLaneQuestPresenter presenter)
        {
            this.presenter = presenter;
        }
        #endregion

        #region Public API.
        public void DestroyRewardInSeconds(float seconds)
        {
            if (destroyRewardJob == null && _sequenceReward != null)
            {
                destroyRewardJob = DestroyRewardJob(seconds);
                StartCoroutine(destroyRewardJob);
            }
        }

        public void ResetSequenceReward()
        {
            _sequenceReward = null;
        }

        public void GenerateSequenceReward()
        {
            _sequenceReward = GetRandomSequenceReward();
        }

        public int GetSportsmenShortage (SingleSportLaneQuest quest)
        {
            return quest.objectives.Sum ( o => (o.iterationsGoal - o.iterationsDone));
        }

        public int GetIterationsDone (SportObjectiveLogic<SportObjectiveData> questObjective)
        {
            return sportsmanManager.GetSportsmanCount
                ( questObjective.sportsmanType
                , null
                , PersonState.Idle
                // , PersonState.InQueue
                , PersonState.Entering
                , PersonState.ExerciseComplete);
        }

        public void Claim(int questIndex, SportsmanCostUnit[] ignoredShortage)
        {
            // check
            var quest = currentLaneQuest.GetQuest(questIndex);
            if (quest == null)
                throw new NullReferenceException();
            //            if (!quest.completed)
            //                throw new QuestNotCompletedException();

            bool isTutor = gameCore.quests.tutorial.isRunning;

            if (!isTutor)
            {
                foreach (var objective in quest.objectives)
                {
                    var currentShortage = (objective.iterationsGoal - objective.iterationsDone);

                    // Because time can pass between ignoredShortage is calculated - check how many shortage of sportsmen is now and if any objective fails check - stop.
                    if (currentShortage > 0 && (ignoredShortage == null || ignoredShortage.Any(x => x.sportsmanType == objective.sportsmanType) &&
                        currentShortage > ignoredShortage.First(x => x.sportsmanType == objective.sportsmanType).count))
                    {
                        throw new NeedMoreSportsmensException();
                    }
                }

                var resRewards = quest.rewards.Where(r => r.rewardType == SportQuestRewardType.Resource).ToDictionary(r => r.itemId, r => r.count);
                if (resRewards != null && resRewards.Count > 0 && !gameCore.playerProfile.storage.CanAcceptResourcesCompletely(resRewards))
                {
                    throw new NeedMoreStorageException();
                }
            }

            ClaimReward(quest, ignoredShortage);
        }

/*
        public void BuyCompleteAndClaim(int questIndex)
        {
            // check
            var quest = currentLaneQuest.GetQuest(questIndex);
            if (quest == null)
                throw new NullReferenceException();

            var neededSportsmensCount = GetNeededSportsmensCount (quest);

            var bucksNeeded = neededSportsmensCount * 10;
            if (bucksNeeded > playerProfile.fitBucks)
                throw new NeedMoreBucksException();

            var resRewards = quest.rewards.Where(r => r.rewardType == SportQuestRewardType.Resource).ToDictionary(r => r.itemId, r => r.count);
            if (resRewards != null && resRewards.Count > 0 && !gameCore.playerProfile.storage.CanAcceptResourcesCompletely(resRewards))
            {
                StorageIsFull.Send(false);
                throw new NeedMoreStorageException();
            }

            var analyticsInfo = new Analytics.MoneyInfo("LaneQuest", quest.questTypeId);

            playerProfile.SpendMoney(new Cost(Cost.CostType.BucksOnly, bucksNeeded, 0), analyticsInfo);
            
            ClaimReward(quest);
        }
*/

/*
        public void AddRandomNeededSportsman(SingleSportLaneQuest quest, LogicSportsmen manager)
        {
            SportObjectiveLogic<SportObjectiveData> objective = null;
            if (quest != null && quest.objectives != null)
            {
                foreach (var o in (quest.objectives))
                {
                    var count = manager.GetSportsmanCount(o.sportsmanType, gameCore.roomManager.GetRoomNameOfType
                        (RoomType.SportClub), PersonState.Entering, PersonState.Idle, PersonState.ExerciseComplete);

                    if (count - o.iterationsGoal <= 0)
                    {
                        objective = o;
                        break;
                    }
                }
            }

            if (objective == null)
                return;

            gameCore.personManager.sportsmen.CreateSportsman(objective.sportsmanType,
                PersonAppearance.Gender.Female, new Analytics.MoneyInfo("", ""));
        }
*/

        public bool SkipCooldown()
        {
            var skipCost = GetSkipCooldownCost();
            if (playerProfile.SpendMoney(skipCost,new Analytics.MoneyInfo("Skip", "LaneQuest")))
            {
                questManager.PushTimer(questManager.GetCurrentLaneQuest().currentStateTimer.timeSpan);
                return true;
            }
            return false;
        }

        public Cost GetSkipCooldownCost()
        {
            var bucks = DynamicPrice.GetSkipCost((int)questManager.GetCurrentLaneQuest().currentStateTimer.secondsToFinish);
            var skipCost = new Cost(Cost.CostType.BucksOnly, bucks, 0);
            return skipCost;
        }

        public bool FinishCooldownStateIfCan()
        {
            return questManager.FinishCooldownStateIfCan();
        }

        public void SetAutoFinishCooldownCondition(Func<bool> condition)
        {
            questManager.canAutoFinishCooldown = condition;
        }

        public void StartTutorQuest()
        {
            questManager.StartTutorialQuest();
        }
        #endregion

        #region Debug API.
#if DEBUG
        public void DebugSkip()
        {
            questManager.PushTimer(questManager.GetPersistentData().questStateTimer.timeSpan);
        }
#endif
        #endregion

        #region IPersistent interface.
        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            if (obj == null)
                return true;

            if (!(obj is LaneQuestManager.LaneQuestPersistentData))
                return false;

            questManager.RestoreData((LaneQuestManager.LaneQuestPersistentData) obj);
            return true;
        }

        object IPersistent.Serialize()
        {
            return questManager.GetPersistentData();
        }

        void IPersistent.OnPreLoad()
        {
        }

        void IPersistent.OnLoaded(bool success)
        {
        }

        void IPersistent.OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            questManager.PushTimer(timePassedSinceSave);
        }

        void IPersistent.OnLoadSkipped()
        {
        }
        #endregion

        #region Private methods
        void ClaimReward(SingleSportLaneQuest quest, SportsmanCostUnit[] ignoredShortage = null)
        {
            if (ignoredShortage == null ||
                gameCore.playerProfile.SpendMoney(LogicSportsmen.GetSportsmanCost(ignoredShortage), new Analytics.MoneyInfo("Sportsmen", "claim lane quest")))
            {
                foreach (var objective in quest.objectives)
                {
                    var removeCount = (ignoredShortage == null)
                        ? objective.iterationsDone
                        : ignoredShortage.Any(x => x.sportsmanType == objective.sportsmanType)
                            ? objective.iterationsGoal - ignoredShortage.First(x => x.sportsmanType == objective.sportsmanType).count
                            : objective.iterationsDone;

                    if (removeCount > 0)
                    {
                        sportsmanManager.RemoveSportsmen(objective.sportsmanType, removeCount, new Analytics.MoneyInfo("LaneQuest", objective.sportsmanType.ToString()));
                    }
                }

                var analyticsInfo = new Analytics.MoneyInfo("LaneQuest", quest.questTypeId);

                int platinaCount = 0;

                foreach (var reward in quest.rewards)
                {
                    switch (reward.rewardType)
                    {
                        case SportQuestRewardType.Coin:
                            playerProfile.AddCoins(reward.count, analyticsInfo);
                            break;

                        case SportQuestRewardType.Resource:
                            playerProfile.AddResource(reward.itemId, reward.count, analyticsInfo, true);
                            if (reward.itemId == Data.PlayerProfile.ResourceType.Platinum)
                                platinaCount += reward.count;
                            break;

                        case SportQuestRewardType.Experience:
                            playerProfile.AddExperience(reward.count);
                            break;

                        case SportQuestRewardType.FitPoints:
                            playerProfile.AddFitPoints(reward.count);
                            break;

                        case SportQuestRewardType.Fitbucks:
                            playerProfile.AddFitBucks(reward.count, new Analytics.MoneyInfo("LaneQuest","Reward"));
                            break;

                        default:
                            Debug.LogError("Error RewardType " + reward.rewardType.ToString());
                            break;
                    }
                }

                var laneQuest = currentLaneQuest;
                var qIndex = currentLaneQuest.GetQuestList().IndexOf(quest);
                quest.ForceComplete();
                quest.Claim();
                LaneQuestClaimedEvent.Send(laneQuest, qIndex);

                PersistentCore.instance.analytics.GameEvent("SportLaneQuestComplete", 
                    new GameEventParam("Level", gameCore.playerProfile.level),
                    new GameEventParam("Reward Platinum amount", platinaCount),
                    new GameEventParam("Reward XP amount", quest.reward.experience),
                    new GameEventParam("Reward Coins amount", quest.reward.coins),
                    new GameEventParam("Reward Resource", quest.rewardResource.ToString()));

                Achievements.CompleteLaneQuest.Send();
            }
        }

        Cost GetRandomSequenceReward()
        {
            var rewardValue = quests.Count;

            if (rewardValue > 0)
            {
                Cost reward = new Cost();

                if (UnityEngine.Random.value >= 0.5)
                {
                    reward.type = Cost.CostType.BucksOnly;
                    reward.value = rewardValue;
                }
                else
                {
                    reward.type = Cost.CostType.ResourcesOnly;
                    reward.AddResource(Data.PlayerProfile.ResourceType.Platinum, rewardValue);
                }

                return reward;
            }
            
            return null;
        }

        float GetProgress()
        {
            return (quests.Count > 0) ? (quests.Count( q => q.claimed ) / quests.Count) : 0.0f;
        }

        IEnumerator DestroyRewardJob(float seconds)
        {
            yield return null;

            presenter.TakeRewardFX(seconds);
            yield return new WaitForSeconds(seconds);

            destroyRewardJob = null;
            _sequenceReward = null;

            presenter?.UpdateReward();
        }

        void OnRoomSizeUpdated (string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupDataInfo)
        {
            onRoomSizeUpdated?.Invoke(roomName, roomData, roomSetupDataInfo);
        }
        #endregion
    }

    public class NeedMoreBucksException : Exception
    {
    }

    public class NeedMoreStorageException : Exception
    {
    }

    public class QuestNotCompletedException : Exception
    {
    }

    public class NeedMoreSportsmensException : Exception
    {
    }
        
}