using System;
using System.Linq;
using Core;
using Logic.Core;
using Logic.Estate;
using Presentation.Facades;
using UnityEngine;
using Data;
using Logic.Estate.Events;
using Core.Analytics;
using Data.Estate;

namespace Logic.Quests.Tutorial
{
    public class TutorSkipLegExtension : TutorSequenceBase
    {
        public TutorSkipLegExtension(IGameCore gameCore, PresentationTutorial presenter)
            : base(gameCore, presenter)
        {
        }

        public override bool NeedBlockGameSave => true;
        public override bool NeedBlockHUD => false;
        public override bool IsParallel => false;
//        public override bool IsRepeat => true; // DEBUG
        public override bool IsNeeded => needStart;

        private bool needStart = false;
        private Equipment equip = null;

        public override void Init()
        {
            base.Init();

            if (gameCore.equipmentManager.equipment.All(e => e.Value.estateType.id != 3))
                EquipmentBirthEvent.Event += OnEquipmentBirth;
        }

        public override void Start()
        {
            EquipmentAssemblyCompleteEvent.Event += OnEquipmentAssemblyComplete;

            AddStep()
                .DebugName("Select equip")
                .MoveCamera(() => presenter.GetWorldPositionProvider(equip), true)
                .Do(() =>
                {
                    gameCore.equipmentManager.SelectEquipmentRequest(equip.id);
                });

            AddStep()
                .DebugName("Skip assembling")
				.ShowTutorialMessage(Loc.Get("skipLegExtension"))
                .ShowRect(()=>(RectTransform) presenter.gui.hud.skipPanel.transform)
                .ShowOverheadArrowAt(()=> presenter.gui.hud.skipPanel.transform, new Vector3(-200,75,0), new Vector3(0,0,90))
                .Do(() =>
                {
                    if (gameCore.playerProfile.fitBucks < 1)
                        gameCore.playerProfile.AddFitBucks(1, new Analytics.MoneyInfo("Tutorial", "Leg extension skip"));
                })
                .Until(presenter.gui.hud.skipPanel.onHide)
                .DoAtFinish(() =>
                {
                    gameCore.playerProfile.AddFitBucks(5, new Analytics.MoneyInfo("Tutorial", "Leg extension skip"), true);
                });

            base.Start();
        }

        private void OnEquipmentAssemblyComplete(Equipment equipment, bool b)
        {
            if (equip != null && equipment.id == equip.id && IsRunning)
            {
                CompleteSequence();
                EquipmentAssemblyCompleteEvent.Event -= OnEquipmentAssemblyComplete;
            }
        }

        public override void OnDestroy()
        {
            EquipmentAssemblyCompleteEvent.Event -= OnEquipmentAssemblyComplete;
            EquipmentBirthEvent.Event -= OnEquipmentBirth;
            base.OnDestroy();
        }

        protected override void CompleteSequence()
        {
            EquipmentBirthEvent.Event -= OnEquipmentBirth;
            needStart = false;
            base.CompleteSequence();
        }

        private void OnEquipmentBirth(Equipment equipment, bool restored)
        {
            if (restored || equipment.estateType.id != 3)
                return;

            needStart = true;
            equip = equipment;
        }
    }
}