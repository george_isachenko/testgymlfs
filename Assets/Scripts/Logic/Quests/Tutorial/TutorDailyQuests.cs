﻿using System;
using System.Collections;
using Data;
using Logic.Core;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Events;
using Data.Room;
using Core;
using Core.Analytics;

namespace Logic.Quests.Tutorial
{
    public class TutorDailyQuests : TutorSequenceBase
    {
        public TutorDailyQuests(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override bool IsNeeded
        {
            get
            {
                return (gameCore.playerProfile.level >= DataTables.instance.balanceData.Quests.DailyQuests.startLevel &&
                        !presenter.gui.levelUp.isActiveAndEnabled) &&
                        !IsRunning &&
                        gameCore.roomManager.IsRoomAtInitialSize(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
            }
        }

        public override bool NeedBlockGameSave { get { return true; } }

        public override bool IsRepeat { get { return false; } }

        public override bool NeedBlockHUD { get { return false; } }
        
        private readonly UnityEvent onClubActivate = new UnityEvent();

        public override void Init()
        {

            if (Steps.Count > 0)
            {
                Debug.LogError("Trying to initialize TutorSequenceVisitorQuest for second time");
                return;
            }
            base.Init();
        }

        public override void Start()
        {
            var botLeft = presenter.gui.hud.transform.Find("BotLeft");
            var btnDailyQuests = botLeft.Find("QuestHolderClub").GetComponentInChild<RectTransform>("BtnDailyQuests");

            AddStep()
                .DebugName("move camera to club if needed")
                .Do(() =>
                {
                    gameCore.StartCoroutine(ExecuteActionInRoom("Club", () => onClubActivate.Invoke()));
                })
                .Until(onClubActivate)
                .BreakAtStartIf(() => gameCore.roomManager.activeRoomName == "Club");

            AddStep()
                .DebugName("show daily bonus icon")
				.ShowTutorialMessage(Loc.Get("dailyActivitiesShowMessage"),false)
                .ShowRect(() => btnDailyQuests)
                .ShowOverheadArrowAt(() => btnDailyQuests,new Vector3(75,120))
                .Until(presenter.gui.dailyQuests.OnShowHandler)
                .DoAtFinish(() => 
                    { 
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.OpenDailyQuest); // Open Daily quest window
                    });

            AddStep()
                .DebugName("wait for daily quest closed")
                .Until(presenter.gui.dailyQuests.OnHideHandler);

            base.Start();
        }

        private IEnumerator ExecuteActionInRoom(string room, Action action)
        {
            if (gameCore.roomManager.activeRoomName != room)
            {
                gameCore.roomManager.TransitionToRoomRequest(room);
                yield return new WaitUntil(() => gameCore.roomManager.activeRoomName == room);
            }
            action.Invoke();
        }
    }
}