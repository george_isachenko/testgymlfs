﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data.Room;
using Logic.Persons;
using Logic.Core;
using Logic.Estate;
using Logic.FacadesBase;
using Logic.PlayerProfile;
using Logic.Rooms;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using View.UI;
using Logic.Estate.Events;
using Logic.Persons.Events;
using Logic.PlayerProfile.Events;
using Logic.Rooms.Events;
using Core;

namespace Logic.Quests.Tutorial
{
    public class TutorManager : BaseLogicNested, IPersistent
    {
        private class TutorialSave
        {
            // ReSharper disable once FieldCanBeMadeReadOnly.Local
            public List<string> finishedTutorials = new List<string>();
            public bool wasReturnFromLinaNoticed = false;
        }
        
        /// TODO: убрать этот костыль во время переписывания биндеров
        public PresentationTutorial Presenter
        {
            set
            {
                presenter = value;
                if (sequences.Count == 0) Init();
            }
            get { return presenter; }
        }
        
        public bool isRunning
        {
            get { return sequences.Any(sequence => sequence.IsRunning && !sequence.IsParallel); }
        }

        private readonly List<TutorSequenceBase> sequences = new List<TutorSequenceBase>();

        private TutorialSave savedData = new TutorialSave();
        private PresentationTutorial presenter;
        private int saveBlockersCount = 0;
        private int hudBlockersCount = 0;
        private bool saveLoaded = false;
        public bool forceBlockTutorialsStart = false;

        public TutorManager(IGameCore gameCore)
            : base(gameCore)
        {
            LevelUpEvent.Event += OnLevelUp;
        }

        public bool IsTutorialRunning<T>() where T : TutorSequenceBase
        {
            return sequences.Any(sequence => sequence.IsRunning && !sequence.IsParallel && sequence is T);
        }

        public bool RunSequence(TutorSequenceBase sequence)
        {
            if (forceBlockTutorialsStart)
            {
                Debug.Log("Force skip tutor start");
                return false;
            }

            if (sequence.NeedBlockGameSave) saveBlockersCount++;
            if (sequence.NeedBlockHUD) hudBlockersCount++;
            UpdateGameState();
            sequence.Start();
            return true;
        }

        private void SetIgnoreButtonHandlingMode(bool ignore)
        {
            presenter.gui.windows.SetIgnoreButtonHandlingMode(ignore);
            presenter.gui.hud.skipPanel.SetIgnoreButtonHandlingMode(ignore);
        }

        public void Init()
        {
            if (!DebugHacks.disableTutorialInit)
            {
                sequences.Add(new TutorSequenceVisitorQuest(gameCore, Presenter));
                sequences.Add(new TutorSquenceFriends(gameCore, Presenter));
                sequences.Add(new TutorSquenceExpand(gameCore, Presenter));
                sequences.Add(new TutorBuyEquipQuest(gameCore, Presenter));
                sequences.Add(new TutorBuyDecorQuest(gameCore, Presenter));
                sequences.Add(new TutorDailyQuests(gameCore, Presenter));
                sequences.Add(new TutorSequenceSportClubUnlock(gameCore, Presenter));
                sequences.Add(new TutorSequenceSportClubOpen(gameCore, Presenter));
                sequences.Add(new TutorSequenceLaneQuests(gameCore, Presenter));
                sequences.Add(new TutorSportAgencyQuest(gameCore, presenter));
                //sequences.Add(new TutorSquenceVisitorMultiObjectives(gameCore, Presenter));
                sequences.Add(new TutorSkipExercise(gameCore, presenter));
                sequences.Add(new TutorSkipLegExtension(gameCore, presenter));
                sequences.Add(new TutorSequenceJuiceBarOpen(gameCore, presenter));
                sequences.Add(new TutorSequenceJuiceBar(gameCore, presenter));
                sequences.Add(new TutorSequenceTrainersUnlock(gameCore, presenter));

                ClearFinishedTutorials();
                sequences.ForEach(s =>
                {
                    s.Init();
                    s.OnSequenceFinished = OnSequenceEnded;
                });

                CharacterBirthEvent.Event += OnPersonBirth;
                StorageUpdatedEvent.Event += OnStorageUpdated;
                RoomActivatedEvent.Event += OnRoomActivated;
                RoomSizeUpdatedEvent.Event += OnRoomSizeUpdated;
                EquipmentBirthEvent.Event += OnEquipmentBirth;

                gameCore.onGameStarted += OnGameStarted;

                Debug.Log("Tutorial Initialized.");
            }
            else
            {
                gameCore.ChangeSaveGameBehaviour(SaveGameBehaviour.SaveEnabled);
                Debug.Log("Tutorial Initialization disbled, enabling save.");
            }
        }

        public void Destroy()
        {
            LevelUpEvent.Event -= OnLevelUp;
            CharacterBirthEvent.Event -= OnPersonBirth;
            StorageUpdatedEvent.Event -= OnStorageUpdated;
            RoomActivatedEvent.Event -= OnRoomActivated;
            RoomSizeUpdatedEvent.Event -= OnRoomSizeUpdated;
            EquipmentBirthEvent.Event -= OnEquipmentBirth;

            gameCore.onGameStarted -= OnGameStarted;

            sequences.ForEach(i => i.OnDestroy());
            sequences.Clear();

            Debug.Log("TUTOR MANAGER DESTROYED");
        }

        private void OnStorageUpdated(int i, int i1, float arg3)
        {
            StartNeededSequences();
        }

        private void OnRoomActivated(string name)
        {
            StartNeededSequences();
        }

        private void OnRoomSizeUpdated(string roomName, RoomData roomData, IRoomSetupDataInfo roomSetupData)
        {
            StartNeededSequences();
        }

        private void OnEquipmentBirth(Equipment equipment, bool b)
        {
            if (b) return;

            StartNeededSequences();
        }

        private void OnSequenceEnded(TutorSequenceBase sequence)
        {
            if (!sequence.IsRepeat)
            {
                savedData.finishedTutorials.Add(sequence.GetType().Name);
                sequences.Remove(sequence);
            }
            if (sequence.NeedBlockGameSave) saveBlockersCount--;
            if (sequence.NeedBlockHUD) hudBlockersCount--;
            UpdateGameState();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            StartNeededSequences();
        }

        private void UpdateGameState()
        {
            if (saveBlockersCount == 0)
            {
                gameCore.ChangeSaveGameBehaviour(SaveGameBehaviour.SaveEnabled);
                Debug.LogFormat("TutorManager: saving turned on, current save blockers count: {0}",
                    saveBlockersCount);
            }
            else
            {
                gameCore.ChangeSaveGameBehaviour(SaveGameBehaviour.SaveDisabled);
                Debug.LogFormat("TutorManager: saving turned off, current save blockers count: {0}",
                    saveBlockersCount);
            }
            if (hudBlockersCount == 0)
            {
                SetIgnoreButtonHandlingMode(false);
                Presenter.EnableHud();
                Debug.LogFormat("TutorManager: hud turned on, current hud blockers count: {0}",
                    hudBlockersCount);
            }
            else
            {
                SetIgnoreButtonHandlingMode(true);
                Presenter.DisableHud();
                Debug.LogFormat("TutorManager: hud turned off, current hud blockers count: {0}", hudBlockersCount);
            }

            foreach (var component in UnityEngine.Object.FindObjectsOfType<UIObjLocalizedCustomMessage>())
            {
                component.canShowMessage = hudBlockersCount == 0;
            }
        }

        public void OnMessageHide()
        {
            sequences.Where(i => i.IsRunning).ToList().ForEach(s => s.OnMessageHide());
            StartNeededSequences();
        }

        public void OnLevelUpWindowHide()
        {
            sequences.Where(i => i.IsRunning).ToList().ForEach(s => s.OnLevelUpWindowHide());
            StartNeededSequences();
        }

        private void OnPersonBirth(Person person, bool restored, CharacterDestination destenation)
        {
            sequences.Where(i => i.IsRunning || i.ForceHandleBirthEvent).ToList().ForEach(s => s.OnPersonBirth(person, restored, destenation));
            StartNeededSequences();
        }

        public void OnLevelUp(int level)
        {
            //sequences.Where(i=>i.IsRunning).ToList().ForEach(s => s.OnLevelUp(level));
            //StartNeededSequences();
        }

        private void OnGameStarted(bool b)
        {
            StartNeededSequences();
        }

        private void StartNeededSequences()
        {
            if (!saveLoaded)
            {
                Debug.Log("Tutorial sequences loop skipped - game not loaded");
                return;
            }
            if (isRunning) return;
            var needed = sequences.FirstOrDefault(s => s.IsNeeded && !s.IsRunning);
            if (needed != null)
            {
                RunSequence(needed);
            }
        }

        private void ClearFinishedTutorials()
        {
            var toRemove = sequences.Where(sequence => savedData.finishedTutorials.Contains(sequence.GetType().Name)).ToList();
            foreach (var sequenceBase in toRemove)
            {
                Debug.LogFormat("Tutorial - Remove finished ({0}) sequense from queue.", sequenceBase.GetType().Name);
                sequences.Remove(sequenceBase);
            }

            RemoveUnnecessaryTutorials();
        }

        private void RemoveUnnecessaryTutorials()
        {
            //---TutorSequenceJuiceBar
            int juicers = gameCore.personManager.GetCharactersCount(typeof(PersonJuicer));
            int orders = gameCore.juiceBar.orders.Count;
            if (juicers > 0 || orders > 0)
                sequences.RemoveAll(a => a is TutorSequenceJuiceBar);
        }

        public bool IsTutorialFinished<T>()
        {
            return !sequences.Any(t => t is T);
        }

        #region IPersistent
        public string propertyName
        {
            get { return typeof (TutorialSave).Name; }
        }

        public Type propertyType
        {
            get { return typeof (TutorialSave); }
        }

        void IPersistent.InitDefaultData()
        {

        }

        public bool Deserialize(object obj)
        {
            Assert.IsTrue(obj is TutorialSave);
            savedData = obj as TutorialSave;
            return true;
        }

        public object Serialize()
        {
            return savedData;
        }

        public void OnPreLoad()
        {

        }

        public void OnLoaded(bool success)
        {
            Debug.Log("Tutorial save loaded? "+success);
            saveLoaded = true;
        }

        public void OnPostLoad(TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            ClearFinishedTutorials();
        }

        public void OnLoadSkipped()
        {
            Debug.Log("Tutorial save not loaded - loading skipped");
            saveLoaded = true;
            ClearFinishedTutorials();
        }
#endregion
    }
}
