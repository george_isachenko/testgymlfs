using Core;
using Data;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Events;
using View.PrefabResources;
using View.UI;
using View.UI.Base;
using View.UI.OverheadUI;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceTrainersUnlock : TutorSequenceBase
    {
        public TutorSequenceTrainersUnlock(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override bool IsRepeat { get { return false; } }
        public override bool NeedBlockGameSave { get { return false; } }
        public override bool NeedBlockHUD { get { return true; } }

        private readonly UnityEvent hqOpen = new UnityEvent();
        private readonly UnityEvent trainersOpen = new UnityEvent();

        public override bool IsNeeded
        {
            get { return gameCore.trainers.canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse && 
                !IsRunning &&
                !presenter.gui.levelUp.isActiveAndEnabled; }
        }

        public override void Start()
        {

            GameObject hqGO = GameObject.Find("HeadQuarters");

            AddStep()
                .DebugName("camera go")
                .MoveCamera(new Vector3(-1.6f, 0.0f, 0.2f))
                .DoAtFinish(() =>
                    {
                    });

            AddStep()
                .DebugName("openHQ")
                .Delay(1)
                .Do(() => {
                    presenter.gui.hq.trainersNew = true;
                    presenter.LockCamera(true);
                    presenter.gui.windows.OnWindowOpenedEvent += OnWindowsOpening;
                })
                .ShowOverheadArrowAt(() => { return new FixedWorldPositionProvider(hqGO.transform.position); })
                //.ShowRect(() => { return new FixedWorldPositionProvider(hqGO.transform.position); }, new Vector2(250, 250), Vector2.zero)
                .ShowTutorialMessage(Loc.Get("trainersUnlockMessage"), false)
                .Until(hqOpen);

            AddStep()
                .DebugName("ctrl Trainers")
                .ShowOverheadArrowAt(() => { return presenter.gui.hq.trainersButton.transform; })
                .ShowRect(() => { return presenter.gui.hq.trainersButton.GetComponent<RectTransform>(); })
                .ShowTutorialMessage(Loc.Get("trainersCtrlHere"), false)
                .Until(trainersOpen)
                .DoAtFinish(() =>
                {
                    presenter.gui.hq.trainersNew = false;
                    presenter.LockCamera(false);
                    presenter.gui.windows.OnWindowOpenedEvent -= OnWindowsOpening;
                });

            base.Start();
        }

        void OnHQOpen()
        {
            hqOpen.Invoke();
        }

        void OnWindowsOpening(WindowTypes type, UIBaseView wnd)
        {
            if(type == WindowTypes.HQ)
                hqOpen.Invoke();

            if (type == WindowTypes.Trainers)
                trainersOpen.Invoke();
        }
    }
}