using Core;
using Data;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using UnityEngine;
using View.PrefabResources;
using View.UI.OverheadUI;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceJuiceBarOpen : TutorSequenceBase
    {
        public TutorSequenceJuiceBarOpen(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override bool IsRepeat { get { return false; } }
        public override bool NeedBlockGameSave { get { return false; } }
        public override bool NeedBlockHUD { get { return true; } }

        public override bool IsNeeded
        {
            get { return gameCore.juiceBar.canUseMyFeature.type == CanUseFeatureResult.ResultType.Locked && 
                !IsRunning &&
                !presenter.gui.levelUp.isActiveAndEnabled; }
        }

        public override void Start()
        {
            AddStep()
                .DebugName("camera go")
                .Do(() =>
                    { /*PersistentCore.instance.analytics.GameEvent("SportTutor:Club Unlock Started:Level_", "Level", gameCore.playerProfile.level);*/})
                .MoveCamera(
                    () =>
                        GameObject.Find("_Rooms").GetComponent<GameObjectsArray>().array[2].GetComponent<IWorldPositionProvider>())
                .DoAtFinish(() =>
                    {
                        
                    });

            AddStep()
			    .DebugName("Pow! & message step")
                .Delay(1)
            /*.Do(() =>
                {
                    var particler = Object.FindObjectOfType<TutorialParticlesStarter>();
                    particler.PlayEffect();
                })*/
                //.CheckCameraCollision()
                .ShowTutorialMessage(Loc.Get("juiceBarOpenedHeader"), Loc.Get("juiceBarOpenedMessage"))
                .DoAtFinish(() =>
                    {
                        //PersistentCore.instance.analytics.GameEvent("SportTutor:Club Unlock Finished:Level_", "Level", gameCore.playerProfile.level); 
                    });

            base.Start();
        }
    }
}