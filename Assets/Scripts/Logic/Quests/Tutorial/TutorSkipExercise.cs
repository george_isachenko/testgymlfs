using System;
using System.Linq;
using Data;
using Data.Person.Info;
using Logic.Persons;
using Logic.Core;
using Logic.Estate;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Events;
using Logic.Persons.Events;
using Data.Person;

namespace Logic.Quests.Tutorial
{
    public class TutorSkipExercise : TutorSequenceBase
    {
        private IPersonInfo targetPerson;
        private readonly UnityEvent onOverheadShowed = new UnityEvent();
        private readonly UnityEvent onExersiceFinished = new UnityEvent();

        public override bool IsNeeded
        {
            get
            {
                return (gameCore.playerProfile.level == 2 || gameCore.playerProfile.level == 3) &&
                       !IsRunning;
            }
        }

        public override bool NeedBlockGameSave
        {
            get { return false; }
        }

        public override bool NeedBlockHUD
        {
            get { return false; }
        }

        public override bool IsRepeat
        {
            get { return true; }
        }

        public override bool IsParallel
        {
            get { return true; }
        }

        public TutorSkipExercise(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override void Start()
        {
            Steps.Clear();

            AddStep()
                .DebugName("wait for overhead spawn")
                .Do(()=> { CharacterShowReadyToSkipOverhead.Event += OnOverheadSpawned; })
                .Until(onOverheadShowed)
                .DoAtFinish(()=> { CharacterShowReadyToSkipOverhead.Event -= OnOverheadSpawned; });

            AddStep()
                .DebugName("show skip overhead timer")
                .Do(() => { CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent; })
                .ShowOverheadArrowAt(() =>
                {
                    var overhead = presenter.GetOverheadIcon(targetPerson);
                    return overhead?.transform;
                }, new Vector3(0,140,0), new Vector3(0,0,0))
                .Until(onExersiceFinished)
                .DoAtFinish(() => { CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent; });
            
            base.Start();
        }

        public override void OnLevelUp(int level)
        {
            base.OnLevelUp(level);

            if (level > 3)
                CompleteSequence();
        }

        public override void OnDestroy()
        {
            CharacterShowReadyToSkipOverhead.Event -= OnOverheadSpawned;
            CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
            base.OnDestroy();
        }

        private void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            if (person.id == targetPerson.id)
                onExersiceFinished.Invoke();
        }

        private void OnOverheadSpawned(IPersonInfo person)
        {
            this.targetPerson = person;
            onOverheadShowed.Invoke();
        }
    }
}