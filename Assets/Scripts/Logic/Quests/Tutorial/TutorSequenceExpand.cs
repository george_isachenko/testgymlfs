﻿using Core;
using Data;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Events;
using View.UI;
using View.UI.Base;
using View.UI.OverheadUI;
using Core.Analytics;

namespace Logic.Quests.Tutorial
{
    public class TutorSquenceExpand : TutorSequenceBase
    {

        public TutorSquenceExpand (IGameCore gameCore, PresentationTutorial presenter)
            :base(gameCore, presenter)
        {

        }

        public override bool NeedBlockGameSave  { get { return true; }   }
        public override bool NeedBlockHUD       { get { return true; } }
        public override bool IsNeeded           { get { return gameCore.playerProfile.level >= 5 && !presenter.gui.levelUp.isActiveAndEnabled && !IsRunning; } }
        
        private readonly UnityEvent roomExpandWindowShow = new UnityEvent();
        private readonly UnityEvent onRoomExpandCancel = new UnityEvent();
        private bool roomExpandCanceled = false;

        public override void Init()
        {

            if (Steps.Count > 0)
            {
                Debug.LogError("Trying to initialize TutorSequenceVisitorQuest for second time");
                return;
            }
            base.Init();
        }

        public override void Start()
        {
            AddStep()
                .DebugName("show message")
                .Do(() => { gameCore.personManager.SetCharactersSelectionMode(false); })
                .Do(() => { gameCore.equipmentManager.SetEquipmentSelectionMode(false); })
                .Do(() =>
                {
                    var provider = presenter.gameObject.GetComponent<IClubInteractionProvider>();
                    if (provider == null)
                    {
                        Debug.LogError("Can't find club interaction provider at tutor!");
                    }
                    else
                    {
                        var room = provider.GetRoom(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
                        room.prepareToExpandRequest += SubscribeToPrepareExpandRequest;
                    }
                })
                .ShowTutorialMessage(Loc.Get("gymExpancionShowHeader"), Loc.Get("gymExpancionShowMessage"))
                .DoAtFinish(() => { });

            AddStep()
                .DebugName("move/lock camera, show arrow, wait for expand")
                .MoveCamera(
                    () => GameObject.Find("ExpandSign").transform,
                    false)
                .ShowOverheadArrowAt(
                    () => GameObject.Find("ExpandSign").transform
                        .GetComponent<IWorldPositionProvider>(),
                    new Vector3(10, 90, 0),
                    new Vector3(0, 0, 0))
                .Do(() =>
                {
                    presenter.gui.windows.OnWindowOpenedEvent += OnWindowOpened;
                    gameCore.equipmentManager.canEditEquip = false;
                })
                .Until(roomExpandWindowShow)
                .Until(onRoomExpandCancel)
                .DoAtFinish(() =>
                {
                    presenter.gui.windows.OnWindowOpenedEvent -= OnWindowOpened;
                    gameCore.personManager.SetCharactersSelectionMode(true);
                    gameCore.equipmentManager.SetEquipmentSelectionMode(true);
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.OpenExpand);//Open Expand Window
                    gameCore.equipmentManager.canEditEquip = true;
                });

            AddStep()
                .DebugName("wait for expand ended")
                .Until(presenter.gui.buyWithResources.OnHideHandler)
                .DoAtFinish(() =>
                {
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Expand);//Expand
                })
                .BreakAtStartIf(() => roomExpandCanceled);

            base.Start();
        }

        private bool SubscribeToPrepareExpandRequest(string roomname)
        {
            var provider = presenter.gameObject.GetComponent<IClubInteractionProvider>();
            if (provider == null)
            {
                Debug.LogError("Can't find club interaction provider at tutor!");
            }
            else
            {
                var room = provider.GetRoom(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
                room.prepareToExpandRequest -= SubscribeToPrepareExpandRequest;
            }

            presenter.gui.hud.btnBack.onClick.AddListener(() =>
            {
                onRoomExpandCancel.Invoke();
                roomExpandCanceled = true;
            });

            return true;
        }

        private void OnWindowOpened(WindowTypes type, UIBaseView wnd)
        {
            roomExpandWindowShow.Invoke();
        }
    }
}
