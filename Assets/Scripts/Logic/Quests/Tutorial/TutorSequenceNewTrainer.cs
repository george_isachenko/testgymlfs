﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic.Core;
using Presentation.Facades;
using View.CameraHelpers;
using Data.Room;
using Logic.Persons;
using System.Linq;
using UnityEngine.Assertions;
using Data.Person;
using Logic.Persons.Tasks;
using Logic.Persons.Events;
using Core.Analytics;
using Data;
using View.UI;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceNewTrainer : TutorSequenceBase 
    {
        #region Public virtual properties.
        public override bool IsRepeat { get { return true; } }
        public override bool NeedBlockGameSave { get { return false; } } // It's 'special' tutorial, so we disabling/enabling save manually.
        public override bool NeedBlockHUD { get { return true; } }

        GUIRoot gui;

        public override bool IsNeeded
        {
            get { return false; }
        }
        #endregion

        #region Constructor.
        public TutorSequenceNewTrainer(IGameCore gameCore, PresentationTutorial presenter, Cost hireCost, Analytics.MoneyInfo moneyInfo, string trainerName) 
            : base(gameCore, presenter)
        {
            this.hireCost = hireCost;
            this.moneyInfo = moneyInfo;
            this.trainerName = trainerName;

            gui = GUIRoot.instance;
        }
        #endregion

        #region Private data.
        string roomName;
        Cost hireCost;
        Analytics.MoneyInfo moneyInfo;
        string trainerName;
        PersonTrainer trainer;
        Person[] inQueuePersons;
        bool moneySpent;
        #endregion

        #region Public virtual API.
        public override void Init()
        {
            roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.Club);
            trainer = null; // Reset it, in case previous step forgot to cleanup this.

            presenter.LockCamera(true);
        }

        public override void Start()
        {
            AddStep()
                .DebugName("Buy trainer")
                .BreakAtStartIf(() => roomName == null, true)
                .Do ( () =>
                {
                    gameCore.ChangeSaveGameBehaviour(Serialization.SaveGameBehaviour.SaveDisabled);

                    presenter.ShowBlockRect(false);

                    if (gameCore.playerProfile.SpendMoney(hireCost, moneyInfo))
                    {
                        moneySpent = true;

                        if (gameCore.roomManager.activeRoomName != roomName)
                            gameCore.roomManager.TransitionToRoomRequest(roomName);
                    }
                })
                .Until(() => gameCore.roomManager.activeRoomName == roomName);

            AddStep()
                .DebugName("Trainer's car arrives to gym")
                .BreakAtStartIf(() => !moneySpent, true)
                .Do ( () =>
                {
                    gameCore.roomManager.AllowRoomsActivation(false);

                    presenter.LockCamera(true);
                    CutScenes.instance.carCutScene.Play();
                });

            AddStep()
                .DebugName("Trainer gets out of car")
                .Delay(7.0f)
                .Do ( () =>
                    {
                        trainer = gameCore.personManager.CreateTrainer
                            ( roomName
                            , Person.EntranceMode.NoMove
                            , trainerName);
                        Assert.IsNotNull(trainer);

                        if (trainer != null)
                        {
                            var trainerDestination = new CharacterDestination(trainer);

                            // Select persons who are:
                            // 1. In same room with trainer.
                            // 2. Are in queue.
                            // 3. Not running or walking to queue or in queue.

                            inQueuePersons = SelectInQueuePersons().ToArray(); // Must cache them and perform later operations on a same set of persons!

                            foreach (var person in inQueuePersons)
                            {
                                //person.ResetTasks();
                                person.SetTask(new TaskDelegate             // Performs look-at and then waits for some time in this state.
                                                ( new TaskDelegate.Settings
                                                    {
                                                        isInterruptable = (task) => true,
                                                        onActivated = (task) =>
                                                        {
                                                            CharacterLookAtEvent.Send(person, trainerDestination, null);
                                                        },
                                                        onUpdate = (task, dt) =>
                                                        {
                                                            return Task.UpdateResult.Running;
                                                        }
                                                    }
                                                ));
                            }

                            trainer.AddTask(new TaskDelegate             // Keeps him steady until task is reset.
                                                ( new TaskDelegate.Settings
                                                    {
                                                        isInterruptable = (task) => true,
                                                        onUpdate = (task, dt) =>
                                                        {
                                                            return Task.UpdateResult.Running;
                                                        }
                                                    }
                                                ));
                        }
                    });

            AddStep()
                .DebugName("Trainers greeting")
                .BreakAtStartIf(() => trainer == null, true)
                .Delay(1.0f)
                .Do(() =>
                {
                    Assert.IsNotNull(trainer);
                    trainer.ResetTasks();

                    PlayEmoteAnimationEvent.Send(trainer, AnimationSocial.Greeting, 1, false, null);
                });

            AddStep()
                .DebugName("Crowd applauds")
                .Delay(1.0f)
                .Do(() =>
                {
                    foreach (var person in inQueuePersons)
                    {
                        var updateDelay = Random.Range(0.0f, 0.33f); // NOTE! Max delay should not exceed delay of the next tutorial step!

                        person.SetTask(new TaskDelegate  // Performs emote after some small delay and then waits until task is reset.
                                        ( new TaskDelegate.Settings
                                            {
                                                isInterruptable = (task) => true,
                                                
                                                onUpdate = (task, dt) =>
                                                {
                                                    if (updateDelay > 0)
                                                        updateDelay -= dt;

                                                    if (updateDelay <= 0 && updateDelay > float.MinValue)
                                                    {
                                                        PlayEmoteAnimationEvent.Send(person, AnimationSocial.Applaud_1, -1, false, null);
                                                        updateDelay = float.MinValue;
                                                    }

                                                    return Task.UpdateResult.Running;
                                                }
                                            }
                                        ));
                    }
                });

            AddStep()
                .DebugName("Trainer gets into gym")
                .Delay(1.75f)
                .Do(() =>
                {
                    // Release crowd back to their duties.
                    foreach (var person in inQueuePersons)
                    {
                        StopCharacterEvent.Send(person);
                        person.ResetTasks();
                    }

                    Assert.IsNotNull(trainer);
                    trainer.EnterRoom(Person.EntranceMode.ImmediateEntranceFast);
                })
                .Until(() => trainer.state == PersonState.Idle);

            AddStep()
                .DebugName("Set name for trainer")
                .Delay(2.0f)
                .ShowTutorialMessage("Set name for trainer")
                .Do(() =>
                {
                        gui.confirmWnd.OnCustom(Loc.Get("idNewTrainer"), Loc.Get("idTrainerRenameRequest"), null, true);
                        gui.confirmWnd.OnConfirm = () => { gui.trainerInfo.ShowTrainer(trainer); gui.trainerInfo.SetInputMode(true); };
                    CommonFinalizer();
                });

            base.Start();
        }

        protected override void CompleteSequence()
        {
            CommonFinalizer();

            base.CompleteSequence();
        }
        #endregion

        #region Private methods.
        private IEnumerable<Person> SelectInQueuePersons ()
        {
            return gameCore.personManager.persons.Values
                        .Where(p => p.roomName == roomName &&
                                    p.state == PersonState.InQueue &&
                                    p.FindTask<TaskGoto>() == null);
        }

        private void CommonFinalizer()
        {
            // Note: can be invoked two times, so no side-effects expected!
            presenter.HideRect();
            presenter.EnableHud();
            presenter.LockCamera(false);
            gameCore.roomManager.AllowRoomsActivation(true);
            gameCore.ChangeSaveGameBehaviour(Serialization.SaveGameBehaviour.SaveEnabled);
        }
        #endregion
    }
}