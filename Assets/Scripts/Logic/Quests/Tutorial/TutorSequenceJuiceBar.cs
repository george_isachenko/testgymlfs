using System;
using Core.Analytics;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Core;
using Logic.Persons;
using Logic.Persons.Tasks;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using View.Actors.OverheadUI;
using View.CameraHelpers;
using View.UI.OverheadUI;
using View.Actors;
using Logic.Estate;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceJuiceBar : TutorSequenceBase
    {
        public TutorSequenceJuiceBar(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
            
        }

        public override bool IsRepeat { get { return false; } }
        public override bool NeedBlockGameSave { get { return true; } }
        public override bool NeedBlockHUD { get { return true; } }

        public override bool IsNeeded
        {
            get { return gameCore.juiceBar.canUseMyFeature.type == CanUseFeatureResult.ResultType.CanUse && 
                !IsRunning &&
                !presenter.gui.levelUp.isActiveAndEnabled; }
        }

        private readonly UnityEvent juiceMakeStart = new UnityEvent();
        private readonly UnityEvent juiceMakeSkip = new UnityEvent();
        private readonly UnityEvent characterSeats = new UnityEvent();
        private readonly UnityEvent characterSelected = new UnityEvent();
        private readonly UnityEvent juiceConsumeStarted = new UnityEvent();
        private readonly UnityEvent juiceConsumeEnded = new UnityEvent();
        private readonly UnityEvent appleCollected = new UnityEvent();
        private readonly UnityEvent rewardCollected = new UnityEvent();
        private readonly UnityEvent juiceBarOpened = new UnityEvent();
        private readonly UnityEvent cameraMovementComplete = new UnityEvent();

        TutorStepLogic makeJuiceStep;

        private PersonJuicer person;
        private IFruitTreeActor fruitTreeActor;
        string juiceBarRoomName;

        public override void Init()
        {
            juiceBarRoomName = gameCore.roomManager.GetRoomNameOfType(RoomType.JuiceBar);
            Assert.IsNotNull(juiceBarRoomName);
        }

        public override void Start()
        {
            var cam = Camera.main.gameObject.GetComponent<CameraController>();

            AddStep()
                .DebugName("Open Juice Bar")
                .ShowBlockRect(false)
                .Do(() =>
                {
                    cam.Stop();
                    gameCore.playerProfile.AddResource(ResourceType.Fruit_01, 4, new Analytics.MoneyInfo("Fruit_01", "JuiceBarTutorial"), true);

                    Assert.IsNull(fruitTreeActor);
                    fruitTreeActor = presenter.GetFruitTreeActor(0);
                    Assert.IsNotNull(fruitTreeActor);

                    presenter.LockCamera(true);
                })
                .MoveCamera(presenter.GetRoomView(RoomType.JuiceBar).homeFocusPosition, false, (bool val)=>
                {
                    gameCore.juiceBar.ShowMakeJuiceUI();
                })
                .Do(() =>
                {
                    cam.onMoveTransformComplete += OnJuiceBarOpeningComplete;
                })
                .ZoomCamera(() => 0, false)
                .RotateCamera(() => 0, false)
                .Until(juiceBarOpened)
                .DoAtFinish(() =>
                {
                    cam.onMoveTransformComplete -= OnJuiceBarOpeningComplete;

                    gameCore.roomManager.AllowRoomsActivation(false);
                });

            makeJuiceStep = AddStep()
			    .DebugName("make juice message")
                .ShowTutorialMessage(Loc.Get("makeAppleJuiceMessage"), false)
                .Do(() =>
                {
                    presenter.gui.windows.PreventWindowClose(true);
                    ShowMakeJuiceArrow();
                    presenter.gui.makeJuice.GetDragHandler(0).OnDragBeginCallback += OnJuiceDragBegin;
                    presenter.gui.makeJuice.GetDragHandler(0).OnDragEndCallback += OnJuiceDragEnd;
                    gameCore.juiceBar.onJuiceAddInOrder += OnJuiceInOrder;
                    presenter.gui.makeJuice.genericView.SetActive("btnClose", false);
                })
                .Until(juiceMakeStart)
                .DoAtFinish(() =>
                {
                    presenter.gui.makeJuice.GetDragHandler(0).OnDragBeginCallback -= OnJuiceDragBegin;
                    presenter.gui.makeJuice.GetDragHandler(0).OnDragEndCallback -= OnJuiceDragEnd;
                    gameCore.juiceBar.onJuiceAddInOrder -= OnJuiceInOrder;
                    presenter.HideArrow(makeJuiceStep);
                });

            AddStep()
                .DebugName("skip juice")
                .ShowTutorialMessage(Loc.Get("let'sSkipMessage"), false)
                .ShowOverheadArrowAt(() => presenter.gui.makeJuice.orderViews[0].genericView.GetTransform("btnSkip"))
                .ShowRect(()=> presenter.gui.makeJuice.orderViews[0].genericView.GetComponent<RectTransform>("btnSkip"), false)
                .Do(() =>
                {
                    gameCore.juiceBar.onJuiceOrderClaim += OnJuiceOrderskip;
                })
                .Until(juiceMakeSkip)
                .DoAtFinish(() =>
                {
                    gameCore.juiceBar.onJuiceOrderClaim -= OnJuiceOrderskip;
                    presenter.gui.makeJuice.genericView.SetActive("btnClose", true);
                    presenter.gui.windows.PreventWindowClose(false);
                });

            AddStep()
                .DebugName("let's Feast Someone")
                .ShowTutorialMessage(Loc.Get("let'sFeastMessage"))
                .ShowOverheadArrowAt(() => presenter.gui.makeJuice.genericView.GetTransform("btnClose"), new Vector3(-100, -45, 0), new Vector3(0, 0, 90))
                .ShowRect(() => presenter.gui.makeJuice.genericView.GetComponent<RectTransform>("btnClose"), false)
                .Until(presenter.gui.makeJuice.OnHideHandler);

            AddStep()
                .DebugName("Spawn & wait for client")
                .MoveCamera(() => presenter.GetRoomView(RoomType.JuiceBar).roomConductor.roomObject.transform)
                .ZoomCamera(() => 0, false)
                .RotateCamera(() => 0, false)
                .ShowBlockRect(false)
                .Do(() =>
                {
                    Assert.IsNull(person);
                    person = gameCore.personManager.CreateJuicer(juiceBarRoomName, Person.EntranceMode.ImmediateEntranceFast, "juicer_spawn_tutorial");

                    Persons.Events.CharacterStartExerciseEvent.Event += OnCharacterStartExerciseEvent;
                })
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(person), new Vector3(0, 50, 0))
                .Until(characterSeats)
                .DoAtFinish(() =>
                {
                    Persons.Events.CharacterStartExerciseEvent.Event -= OnCharacterStartExerciseEvent;
                });

            AddStep()
                .DebugName("Move camera to client")
                .ShowBlockRect(false)
                .MoveCamera(() => presenter.GetWorldPositionProvider(person), false, (result) => cameraMovementComplete.Invoke())
                .Until(cameraMovementComplete);

            AddStep()
                .DebugName("Click on client")
                .ShowTutorialMessage(Loc.Get("dragJuiceMessage"), true)
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(person), new Vector3(0, 50, 0))
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(80, 150), new Vector2(0, 25), true)
                .Do(() =>
                {
                    Persons.Events.SetSelectionEvent.Event += OnCharacterSelected;
                })
                .Until(characterSelected)
                .DoAtFinish(() =>
                {
                    Persons.Events.SetSelectionEvent.Event -= OnCharacterSelected;
                });

            AddStep()
                .DebugName("Drag juice")
                .ShowTutorialMessage(Loc.Get("dragJuiceMessage"), true)
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(260, 220), new Vector2(-195, 60), true)
                .Do(() =>
                {
                    gameCore.juiceBar.onJuiceConsumeStarted += OnJuiceConsumeStarted;
                })
                .Until(juiceConsumeStarted)
                .DoAtFinish(() =>
                {
                    gameCore.juiceBar.onJuiceConsumeStarted -= OnJuiceConsumeStarted;

                    gameCore.personManager.DeselectCharacterIfSelectedRequest(person.id);

                    var task = person.FindTask<TaskExercise>();
                    if (task != null)
                    {
                        task.UpdateDuration(TimeSpan.Zero); // Infinite time.
                    }
                });

            AddStep()
                .DebugName("Move to Tree")
                .Do(() => {
                    while(fruitTreeActor.numFruits < fruitTreeActor.maxFruits)
                        gameCore.juiceBar.SkipFruitTreeRegen(fruitTreeActor.id);
                })
                .BreakAtStartIf(() => (fruitTreeActor == null || fruitTreeActor.overheadUIIcon == null || fruitTreeActor.numFruits < fruitTreeActor.maxFruits))
                .ShowBlockRect(false)
                .MoveCamera(() => GetApplePositionProvider(), false, (result) => cameraMovementComplete.Invoke())
                .Until(cameraMovementComplete);

            AddStep()
                .DebugName("Collect from tree")
                .BreakAtStartIf(() => (fruitTreeActor == null || fruitTreeActor.overheadUIIcon == null || fruitTreeActor.numFruits < fruitTreeActor.maxFruits))
                .ShowRect(() => fruitTreeActor.overheadUIIcon.worldPositionProvider, new Vector2(180, 275), new Vector2(0, -80), true)
                .ShowTutorialMessage(Loc.Get("let'sCollectApplesMessage"))
                .ShowOverheadArrowAt(() => fruitTreeActor.overheadUIIcon.worldPositionProvider, new Vector3(0, 50, 0), new Vector3(0, 0, 0))
                .Do(() =>
                {
                    gameCore.juiceBar.SelectFruitTreeByIndex(0);

                    gameCore.juiceBar.onFruitCollected += OnFruitCollected;
                })
                .Until(appleCollected)
                .DoAtFinish(() =>
                {
                    gameCore.juiceBar.onFruitCollected -= OnFruitCollected;

                    gameCore.juiceBar.SelectFruitTreeByIndex(-1);
                });

            AddStep()
                .DebugName("Move back to person")
                .ShowBlockRect(false)
                .MoveCamera(() => presenter.GetWorldPositionProvider(person), false, (result) => cameraMovementComplete.Invoke())
                .Until(cameraMovementComplete);

            AddStep()
                .DebugName("Wait for consume end")
                .ShowRect(() => (RectTransform) presenter.gui.hud.skipPanel.transform, true)
                .Do(() =>
                {
                    var newDuration = TimeSpan.FromSeconds(2.5f);

                    var task = person.FindTask<TaskExercise>();
                    if (task != null)
                    {
                        task.UpdateDuration(newDuration);
                    }

                    gameCore.personManager.SelectCharacterRequest(person.id);
                })
                .ShowTutorialMessage(Loc.Get("collectJuicerRewardMessage"))
                .Do(() =>
                {
                    gameCore.juiceBar.onJuiceConsumeEnded += OnJuiceConsumeEnded;
                })
                .Until(juiceConsumeEnded)
                .DoAtFinish(() =>
                {
                    gameCore.juiceBar.onJuiceConsumeEnded -= OnJuiceConsumeEnded;
                });

            AddStep()
                .DebugName("Collect Reward")
                .BreakAtStartIf(() => (person.state != PersonState.Workout))
//              .MoveCamera(() => presenter.GetWorldPositionProvider(person))
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(80, 150), new Vector2(0, 25), true)
                .ShowTutorialMessage(Loc.Get("collectJuicerRewardMessage"))
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(person), new Vector3(0, 70, 0), new Vector3(0, 0, 0))
                .Do(() =>
                {
                    gameCore.quests.juiceQuests.onQuestClaimed += OnRewardCollect;
                })
                .Until(rewardCollected)
                .DoAtFinish(() =>
                {
                    gameCore.quests.juiceQuests.onQuestClaimed -= OnRewardCollect;
                });

/*
            AddStep()
                .DebugName("Finish")
                .Do(() =>
                {
                });
*/

            base.Start();
        }

        protected override void CompleteSequence()
        {
            gameCore.roomManager.AllowRoomsActivation(true);

            presenter.LockCamera(false);

            base.CompleteSequence();
        }

        void OnJuiceDragBegin()
        {
            presenter.HideArrow(makeJuiceStep);
            presenter.ShowTextHintMessage(Loc.Get("dradToEmptySlotMessage"), false);
        }

        void OnJuiceDragEnd()
        {
            ShowMakeJuiceArrow();
            presenter.ShowTextHintMessage(Loc.Get("makeAppleJuiceMessage"), false);
        }

        void OnJuiceInOrder()
        {
            juiceMakeStart.Invoke();
        }

        void ShowMakeJuiceArrow()
        {
            presenter.ShowArrow(makeJuiceStep, () => presenter.gui.makeJuice.recipes[0].transform,
                    new Vector3(50, 0, 0),
                    new Vector3(0, 0, -90));
        }

        void OnJuiceOrderskip()
        {
            juiceMakeSkip.Invoke();
        }

        void OnJuiceConsumeStarted(int personId, int equipmentId, ResourceType juiceType)
        {
            if (person != null && person.id == personId)
                juiceConsumeStarted.Invoke();
        }

        void OnJuiceConsumeEnded (int personId, int equipmentId)
        {
            if (person != null && person.id == personId)
                juiceConsumeEnded.Invoke();
        }

        void OnCharacterStartExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed)
        {
            if (person != null && ReferenceEquals(person, this.person) && equipment != null)
                characterSeats.Invoke();
        }

        void OnCharacterSelected(Person person, bool selected, bool focus)
        {
            if (selected && person == this.person)
                characterSelected.Invoke();
        }

        void OnFruitCollected(string fruitTreeName, int collected)
        {
            appleCollected.Invoke();
        }

        void OnRewardCollect()
        {
            rewardCollected.Invoke();
        }

        IWorldPositionProvider GetApplePositionProvider()
        {
            if (fruitTreeActor != null)
            {
                var customWorldPositionProvider = new ActorPositionWorldPositionProvider<string>(fruitTreeActor, true, true);
                return customWorldPositionProvider;
            }
            return null;
        }

        void OnJuiceBarOpeningComplete()
        {
            juiceBarOpened.Invoke();
        }

/*
        public override void OnPersonBirth(Person person, bool restored, CharacterDestination destination)
        {
            base.OnPersonBirth(person, restored, destination);

            if (this.person == null && person is PersonJuicer && person.roomName == juiceBarRoomName)
            {
                this.person = person as PersonJuicer;
                characterSpawned.Invoke();
            }
        }
*/
    }
}