﻿using System;
using System.Linq;
using Core;
using Data;
using Data.Person;
using Data.PlayerProfile;
using Data.Room;
using Logic.Persons;
using Logic.Persons.Tasks;
using Logic.Core;
using Logic.Estate;
using Logic.Serialization;
using Presentation.Facades;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI;
using View.UI.Base;
using View.UI.OverheadUI;
using Logic.Estate.Events;
using Logic.Persons.Events;
using Core.Analytics;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceLaneQuests : TutorSequenceBase
    {
        public TutorSequenceLaneQuests (IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override bool IsRepeat { get { return false; } }
        public override bool NeedBlockGameSave { get { return true; } }
        public override bool NeedBlockHUD { get { return true; } }

        public override bool IsNeeded
        {
            get
            {
                return gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) == 1 && !IsRunning &&
                       gameCore.quests.tutorial.IsTutorialFinished<TutorSequenceSportClubOpen>();
            }
        }

        private readonly UnityEvent onBuyTarget = new Button.ButtonClickedEvent();
        private readonly UnityEvent onTrainPressed = new Button.ButtonClickedEvent();
        private readonly UnityEvent onTrainFinished = new Button.ButtonClickedEvent();
        private readonly UnityEvent charEntered = new UnityEvent();
        private readonly UnityEvent charSelectedEvent = new UnityEvent();
        private readonly UnityEvent exerciseStarted = new UnityEvent();
        private readonly UnityEvent equipmentSelected = new UnityEvent();

        private PersonSportsman simpleSportsman;

        private PersonSportsman targetSportsman
        {
            get
            {
                return simpleSportsman ??
                       (simpleSportsman = gameCore.personManager.sportsmen.GetSportsman(SportsmanType.Sportsman_0,
                           gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)));
            }
        }

        public override void Start ()
        {
            AddStep()
                .DebugName("show board")
                .MoveCamera(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(), true)
                .RotateCamera(() => 16)
                .ZoomCamera(()=>0, false)
                .ShowRect(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(),
                    new Vector2(280, 310),
                    new Vector2(0, -140))
                .ShowTutorialMessage(Loc.Get("sportClubTutorialShowBoard"),false)
                .ShowOverheadArrowAt(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(),
                    new Vector3(-50, 50, 0),
                    new Vector3(0, 0, -90))
                .Do(() =>
                {
                    gameCore.sportLaneQuest.StartTutorQuest();
                    presenter.gameObject.GetComponent<SportLaneQuestPresenter>().UpdateBoardPlanks();
                    gameCore.personManager.SetCharactersSelectionMode(false);
                })
                .Until(presenter.gui.sportLaneQuest.onOpen)
                .DoAtFinish(() =>
                    {
                        //PersistentCore.instance.analytics.GameEvent("SportTutor:Lane Quest Window Open"); 
                    });

            AddStep()
                .DebugName("wait for close")
                .ShowRect(() => presenter.gui.sportLaneQuest.genericView.GetComponent<RectTransform>("btnClose"), false)
                .Do(() =>
                {
                    presenter.gui.sportLaneQuest.genericView.GetComponent<Button>("claimButton").interactable = false;
                })
                .ShowTutorialMessage(Loc.Get("sportClubTutorialWaitForClose"))
                .ShowOverheadArrowAt(() => presenter.gui.sportLaneQuest.genericView.GetTransform("btnClose"),
                    new Vector3(-100, -35, 0),
                    new Vector3(0, 0, 90))
                .Until(presenter.gui.sportLaneQuest.OnHideHandler)
                .DoAtFinish(() =>
                {
                    presenter.gui.sportLaneQuest.genericView.GetComponent<Button>("claimButton").interactable = true;
                });

            AddStep()
                .DebugName("Add bottle")
                .Do(() =>
                {
                    gameCore.playerProfile.storage.AddResource(ResourceType.Sport_01, 1, true, new Analytics.MoneyInfo("Tutorial", ResourceType.Sport_01.ToString()));
                });

            AddStep()
                .DebugName("Wait for char")
                .LockCameraForStep()
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(targetSportsman))
                .ShowRect(() => presenter.GetWorldPositionProvider(targetSportsman), new Vector2(0, 0), new Vector2(0, 0), false)
                .Until(charEntered)
                .BreakAtStartIf(() => targetSportsman.state == PersonState.Idle)
                .Do(() =>
                {
                    CharacterEnteredRoomEvent.Event += OnCharacterEnteredRoomEvent;
                })
                .DoAtFinish(() =>
                {
                    CharacterEnteredRoomEvent.Event -= OnCharacterEnteredRoomEvent;
                });

            AddStep()
                .DebugName("select equip")
                .ShowRect(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)),
                    new Vector2(100, 130),
                    new Vector2(-50, -40), false)
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)),
                    new Vector3(0, 10, 0))
                .ShowTutorialMessage(Loc.Get("sportClubTutorialSelectEquip"))
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)), true)
                .Do(() =>
                {
                    EquipmentSelectionEvent.Event += EquipmentSelectTrigger;
                })
                .Until(equipmentSelected)
                .DoAtFinish(() =>
                    {
                        EquipmentSelectionEvent.Event -= EquipmentSelectTrigger;
                        //PersistentCore.instance.analytics.GameEvent("SportTutor:Equipment Selected");
                    });

            /*     AddStep()
                    .DebugName("select exercise (go to shop)")
                    .ShowRect(() => (RectTransform) presenter.gui.sportsmanTrainingView.exerciceViews[0].transform)
                    .ShowTutorialMessage("You need to have a Sport Item to train Professional. Let's visit Shop")
                    .ShowOverheadArrowAt(() => presenter.gui.sportsmanTrainingView.exerciceViews[0].trainButton,
                        new Vector3(0, 50, 0),
                        new Vector3(0, 0, 0))
                    .Do(() => { presenter.gui.sportsmanTrainingView.onExerciseSelected += OnExerciseSelected; })
                    .Until(onTrainPressed)
                    .DoAtFinish(() => 
                        { 
                            presenter.gui.sportsmanTrainingView.onExerciseSelected -= OnExerciseSelected;
                            PersistentCore.instance.analytics.GameEvent("SportTutor:Shop Opened");
                        })
                        .DoAtFinish(() =>
                        {
                            if (gameCore.playerProfile.coins < 50)
                                gameCore.playerProfile.AddCoins(50 - gameCore.playerProfile.coins,
                                    new Analytics.MoneyInfo("Tutorial coins help", "add_coins_in_lane_tutor"));
                        });

               AddStep()
                    .DebugName("buy necessary resource")
                    .ShowRect(() => (RectTransform) presenter.gui.shopUltimate.list[0].transform)
                    .ShowOverheadArrowAt(() => presenter.gui.shopUltimate.list[0].genericView.GetTransform("btnBuy"),
                        new Vector3(0, 50, 0))
                    .Do(() => { presenter.gui.shopUltimate.list[0].OnClickCallback+=OnShopItemBuy; })
                    .Until(onBuyTarget)
                    .DoAtFinish(() => 
                        { 
                            presenter.gui.shopUltimate.list[0].OnClickCallback -= OnShopItemBuy;
                            PersistentCore.instance.analytics.GameEvent("SportTutor:Sport Resource Purchased");
                        });*/

            AddStep()
                .DebugName("open exercises view")
                .Do(presenter.gui.sportsmanTrainingView.Show);

            AddStep()
                .DebugName("select exercise (start exercise)")
                .ShowRect(() => (RectTransform)presenter.gui.sportsmanTrainingView.exerciceViews[0].transform)
                .ShowTutorialMessage(Loc.Get("sportClubTutorialSelectExerciseOpenShop"),false)
                .ShowOverheadArrowAt(() => presenter.gui.sportsmanTrainingView.exerciceViews[0].trainButton,
                    new Vector3(0, 100, 0))
                .Do(() => { presenter.gui.sportsmanTrainingView.onExerciseSelected += OnExerciseSelected; })
                .Until(onTrainPressed)
                .DoAtFinish(() =>
                    {
                        presenter.gui.sportsmanTrainingView.onExerciseSelected -= OnExerciseSelected;
                        //PersistentCore.instance.analytics.GameEvent("SportTutor:Training Started");
                    });

            AddStep()
                .DebugName("select equip until exercise not started")
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)), true)
                .ShowRect(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)),
                    new Vector2(100, 130),
                    new Vector2(0, -40), false)
                //.LockCameraForStep()
                .ShowTutorialMessage(Loc.Get("sportClubTutorialMarathonCyclerTraining"))
                // .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)))
                .Do(() =>
                {
                    EquipmentStartExerciseEvent.Event += EquipmentStartExerciseTrigger;
                    gameCore.personManager.SetCharactersSelectionMode(true);
                })
                .Until(exerciseStarted)
                .DoAtFinish(() => { EquipmentStartExerciseEvent.Event -= EquipmentStartExerciseTrigger; });

            AddStep()
                .DebugName("select training person")
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)), true)
                .ShowRect(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)),
                    new Vector2(100, 130),
                    new Vector2(-50, -40), false)
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)))
                .ShowTutorialMessage(Loc.Get("sportClubTutorialSkipMassage"))
                .Do(() => { CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent; })
                .Do(() => { EquipmentSelectionEvent.Event += EquipmentSelectTrigger; })
                .Until(onTrainFinished)
                .Until(equipmentSelected)
                .DoAtFinish(() => { CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent; })
                .DoAtFinish(() => { EquipmentSelectionEvent.Event -= EquipmentSelectTrigger; });

            AddStep()
                .DebugName("select skip")
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetFreeEquipByEstateId(185)), true)
                .ShowRect(() => (RectTransform)presenter.gui.hud.skipPanel.transform, false)
                .ShowTutorialMessage(Loc.Get("sportClubTutorialBoostForFree"))
                .ShowOverheadArrowAt(() => presenter.gui.hud.skipPanel.transform,
                    new Vector3(110, 80, 0),
                    new Vector3(0, 0, -90))
                .Do(() => { CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent; })
                .Until(onTrainFinished)
                .DoAtFinish(() =>
                {
                    CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
                    //PersistentCore.instance.analytics.GameEvent("SportTutor:Training Finished");
                })
                .BreakAtStartIf(() => targetSportsman.state != PersonState.Workout);

            AddStep()
                .DebugName("show board")
                .ShowTutorialMessage(Loc.Get("sportClubTutorialEarnReward"),false)
                .RotateCamera(() => 20)
                .ShowRect(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(),
                    new Vector2(325, 350),
                    new Vector2(0, -180))
                .MoveCamera(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(), true)
                .ShowOverheadArrowAt(() => GameObject.Find("LaneQuestBoard").GetComponent<IWorldPositionProvider>(),
                    new Vector3(-50, 50, 0),
                    new Vector3(0, 0, -90))
                .Do(() =>
                {
                    gameCore.personManager.SetCharactersSelectionMode(false);
                    presenter.gameObject.GetComponent<SportLaneQuestPresenter>().UpdateBoardPlanks();
                })
                .Until(presenter.gui.sportLaneQuest.onOpen)
                .DoAtFinish(() =>
                {
                    //PersistentCore.instance.analytics.GameEvent("SportTutor:LaneQuestWindowOpenSecondTime");
                });

            AddStep()
                .DebugName("wait for windows will be closed")
                .Until(presenter.gui.sportLaneQuest.OnHideHandler)
                .DoAtFinish(() =>
                {
                    //PersistentCore.instance.analytics.GameEvent("SportTutor:Lane Quest Completed");
                });

            //AddStep()
            //	.DebugName("post tutor message")
            //    .ShowTutorialMessage(Loc.Get("sportClubTutorialFinishHeader"), Loc.Get("sportClubTutorialFinishText"));

            AddStep()
                .DebugName("some cookies on finish (+4 sportsman)")
                .Do(() =>
                {
                    for (int i = 0; i < 4; i++)
                    {
                        gameCore.sportsmen.CreateSportsman
                            (SportsmanType.Sportsman_0
                                ,
                                UnityEngine.Random.value < 0.5f
                                    ? PersonAppearance.Gender.Male
                                    : PersonAppearance.Gender.Female
                                , new Analytics.MoneyInfo("", ""));
                    }
                    gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
                    presenter.EnableHud();
                    gameCore.personManager.SetCharactersSelectionMode(true);
                });

            base.Start();
        }

        private void EquipmentSelectTrigger (Equipment equipment, bool selected)
        {
            if (equipment.estateType.id == 185 && selected)
                equipmentSelected.Invoke();
        }

        private void OnCharacterEnteredRoomEvent (Person person)
        {
            if (targetSportsman != null && person.id == targetSportsman.id)
            {
                charEntered.Invoke();
            }
        }

        private void EquipmentStartExerciseTrigger
            (Equipment equipment, int exerciseAnimation, float speed)
        {
            if (equipment.estateType.id == 185)
            {
                exerciseStarted.Invoke();

                var task = targetSportsman.FindTask<TaskExercise>();
                if (task != null)
                {
                    task.UpdateDuration(new TimeSpan(0, 0, 30));
                }
                else
                {
                    Debug.LogError("Очень грустно....");
                }
            }
        }

        private Equipment GetFreeEquipByEstateId (int estateId)
        {
            return gameCore.equipmentManager.equipment
                .Where(equip => equip.Value.estateType.id == estateId)
                .Select(equip => equip.Value)
                .FirstOrDefault();
        }

        private void CharacterSelectTrigger (Person person, bool selected)
        {
            if (person == targetSportsman)
                charSelectedEvent.Invoke();
        }

        private void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            //            if (person != targetSportsman) return;
            if (person is PersonSportsman)
                onTrainFinished.Invoke();
        }

        private void OnExerciseSelected (int exerciseIndex)
        {
            if (exerciseIndex == 0)
            {
                onTrainPressed.Invoke();
            }
        }

        private void OnShopItemBuy (UIBaseViewListItem item)
        {
            if (item.idx == 0)
            {
                onBuyTarget.Invoke();
            }
        }
    }
}