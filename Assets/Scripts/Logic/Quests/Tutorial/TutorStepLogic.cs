﻿using UnityEngine;
using System;
using System.Collections;
using Data.Room;
using Logic.Core;
using UnityEngine.Events;
using View.CameraHelpers;
using View.UI.OverheadUI;
using Presentation.Facades;

namespace Logic.Quests.Tutorial
{
    public class OnCompleteCallback : UnityEvent<bool>
    {
    }

    public class TutorStepLogic
    {
        public readonly OnCompleteCallback onCompletedCallback = new OnCompleteCallback();
        public string debugName { get; private set; }

        protected IGameCore gameCore;
        protected PresentationTutorial presenter;

        private Action startActions;
        private Func<bool> untilFuncs;
        private Action completeActions;
        private float startDelay;
        private bool hasUntilEvents = false;
        private bool complete = false;

        public TutorStepLogic (IGameCore gameCore, PresentationTutorial presenter)
        {
            this.gameCore = gameCore;
            this.presenter = presenter;
        }

        public virtual void Start ()
        {
            complete = false;
            gameCore.StartCoroutine(DelayedStart());
        }

        private IEnumerator DelayedStart ()
        {
            yield return null;

            if (startDelay > 0)
            {
                yield return new WaitForSeconds(startDelay);
            }

            if (startActions != null)
            {
                foreach (Action action in startActions.GetInvocationList())
                {
                    action();
                    if (complete)
                        break;
                }
            }

            if (!complete)
            {
                if (!hasUntilEvents && untilFuncs == null)
                    Complete(false);

                if (untilFuncs != null && !complete)
                {
                    yield return new WaitUntil(() => untilFuncs.Invoke());
                    Complete(false);
                }
            }
        }

        public virtual void Complete (bool cancelAllSteps)
        {
            if (!complete)
            {
                complete = true;

                completeActions?.Invoke();

                onCompletedCallback.Invoke(cancelAllSteps);
            }
        }

        public TutorStepLogic ShowOverheadArrowAt (Func<IWorldPositionProvider> worldPositionProviderGetter,
            Vector3 screenPositionShift = default(Vector3), Vector3 rotationEuler = default(Vector3))
        {
            Do(() => { presenter.ShowArrow(this, worldPositionProviderGetter, screenPositionShift, rotationEuler); });
            DoAtFinish(() => { presenter.HideArrow(this); });
            return this;
        }

        public TutorStepLogic ShowOverheadArrowAt (Func<Transform> transformGetter,
            Vector3 screenPositionShift = default(Vector3), Vector3 rotationEuler = default(Vector3))
        {
            Do(() => { presenter.ShowArrow(this, transformGetter, screenPositionShift, rotationEuler); });
            DoAtFinish(() => { presenter.HideArrow(this); });
            return this;
        }

        public TutorStepLogic Until (UnityEvent finishOnEvent)
        {
            if (untilFuncs != null)
            {
                Debug.LogWarningFormat("Tutorial step '{0}' tries to mix UnityEvent and callback Until clauses, this can cause missing Until checks!", debugName);
            }

            hasUntilEvents = true;
            UnityAction action = () =>
            {
                Complete(false);
            };
            Do(() => { finishOnEvent.AddListener(action); });
            DoAtFinish(() => { finishOnEvent.RemoveListener(action); });
            return this;
        }

        public TutorStepLogic Until<T> (UnityEvent<T> finishOnEvent)
        {
            if (untilFuncs != null)
            {
                Debug.LogWarningFormat("Tutorial step '{0}' tries to mix UnityEvent and callback Until clauses, this can cause missing Until checks!", debugName);
            }

            hasUntilEvents = true;
            UnityAction<T> action = (a) =>
            {
                Complete(false);
            };
            Do(() => { finishOnEvent.AddListener(action); });
            DoAtFinish(() => { finishOnEvent.RemoveListener(action); });
            return this;
        }

        public TutorStepLogic Until (Func<bool> condition)
        {
            if (hasUntilEvents)
            {
                Debug.LogWarningFormat("Tutorial step '{0}' tries to mix UnityEvent and callback Until clauses, this can cause missing Until checks!", debugName);
            }

            if (untilFuncs == null)
            {
                untilFuncs = new Func<bool>(condition); // Do not assign but make our own func!
            }
            else
            {
                untilFuncs += condition;
            }
            return this;
        }

        public TutorStepLogic Do (Action action)
        {
            startActions += action;
            return this;
        }

        public TutorStepLogic DoAtFinish (Action action)
        {
            completeActions += action;
            return this;
        }

        public TutorStepLogic Delay (float time)
        {
            startDelay = time;
            return this;
        }

        public TutorStepLogic ShowTutorialMessage (string title, string message)
        {
            Do(() => { presenter.ShowModalMessage(title, message); });
            Until(presenter.gui.tutorialMsg.onHideHandler);
            DoAtFinish(() => presenter.gui.tutorialMsg.HideTextHint());
            return this;
        }

        public TutorStepLogic ShowTutorialMessage (string message, bool anchoredAtLeftSide = true)
        {
            Do(() => { presenter.ShowTextHintMessage(message, anchoredAtLeftSide); });
            DoAtFinish(() => presenter.gui.tutorialMsg.HideTextHint());
            return this;
        }

        public TutorStepLogic MoveCamera (Func<Transform> targetGetter, bool lockCameraForStep = false, Action<bool> onComplete_ = null)
        {
            Do(() =>
            {
                var t = targetGetter();
                if (t == null)
                    return;

                var cam = Camera.main.gameObject.GetComponent<CameraController>();
                var pos = t.position;
                cam.AutoMoveTo(pos.x, pos.z, onComplete_);
            });
            if (lockCameraForStep)
                LockCameraForStep();
            return this;
        }

        public TutorStepLogic MoveCamera (Vector3 target, bool lockCameraForStep = false, Action<bool> onComplete_ = null)
        {
            Do(() =>
            {
                var cam = Camera.main.gameObject.GetComponent<CameraController>();
                cam.AutoMoveTo(target.x, target.z, onComplete_);
            });
            if (lockCameraForStep)
                LockCameraForStep();
            return this;
        }

        public TutorStepLogic MoveCamera (Func<IWorldPositionProvider> targetGetter, bool lockCameraForStep = false, Action<bool> onComplete_ = null)
        {
            Do(() =>
            {
                var cam = Camera.main.gameObject.GetComponent<CameraController>();
                var pos = targetGetter().GetWorldPosition();
                cam.AutoMoveTo(pos.x, pos.z, onComplete_);
            });
            if (lockCameraForStep)
                LockCameraForStep();
            return this;
        }

        public TutorStepLogic RotateCamera (Func<float> angleGetter, bool rotateBackWhenFinish = true)
        {
            var oldAngle = 0f;
            var cam = Camera.main.gameObject.GetComponent<CameraController>();
            Do(() =>
            {
                oldAngle = cam.cameraRotation.rotationAngle;
                cam.AutoRotateTo(angleGetter());
            });
            if (rotateBackWhenFinish)
            {
                DoAtFinish(() =>
                {
                    cam.AutoRotateTo(oldAngle);
                });
            }
            return this;
        }

        public TutorStepLogic ZoomCamera (Func<float> zoomGetter, bool zoomBackWhenFinish = true)
        {
            var oldZoom = 0f;
            var cam = Camera.main.gameObject.GetComponent<CameraController>();
            Do(() =>
            {
                oldZoom = cam.cameraZoom.zoomInterpolator;
                cam.AutoZoomTo(zoomGetter());
            });
            if (zoomBackWhenFinish)
            {
                DoAtFinish(() =>
                {
                    cam.AutoZoomTo(oldZoom);
                });
            }
            return this;
        }

        /*
                public TutorStepLogic CheckCameraCollision()
                {
                    Do(() =>
                        {
                            var cam = Camera.main.gameObject.GetComponent<CameraController>();
                            cam.CheckCollision();
                        });
                    return this;
                }
        */

        public TutorStepLogic LockCameraForStep ()
        {
            Do(() =>
            {
                var cam = Camera.main.gameObject.GetComponent<CameraController>();
                cam.states.stickToPos = true;
            });
            DoAtFinish(() =>
            {
                var cam = Camera.main.gameObject.GetComponent<CameraController>();
                cam.states.stickToPos = false;
            });
            return this;
        }

        public TutorStepLogic DebugName (string name)
        {
            debugName = name;
            return this;
        }

        public TutorStepLogic BreakAtStartIf (Func<bool> condition, bool cancelAllSteps = false)
        {
            Do(() => { if (condition()) Complete(cancelAllSteps); });
            return this;
        }

        public TutorStepLogic ShowRect (Func<IWorldPositionProvider> targetGetter, Vector2 rectSize, Vector2 rectOffset, bool isRectVisible = true)
        {
            Do(() => { presenter.ShowRectAt(targetGetter(), rectSize, rectOffset, isRectVisible); });
            DoAtFinish(() => { presenter.HideRect(); });
            return this;
        }

        public TutorStepLogic ShowRect (Func<RectTransform> targetGetter, bool isShadowVisible = true)
        {
            Do(() => { presenter.ShowRectAt(targetGetter(), isShadowVisible); });
            DoAtFinish(() => { presenter.HideRect(); });
            return this;
        }

        public TutorStepLogic ShowBlockRect (bool isRectVisible = true)
        {
            Do(() => { presenter.ShowBlockRect(isRectVisible); });
            DoAtFinish(() => { presenter.HideRect(); });
            return this;
        }
    }
}