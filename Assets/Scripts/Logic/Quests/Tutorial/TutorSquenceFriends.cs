﻿using System.Collections;
using Core;
using Data;
using Data.Room;
using Logic.Core;
using Logic.Serialization;
using Presentation.Facades;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Events;
using View.UI;
using View.UI.Base;
using Core.Analytics;

namespace Logic.Quests.Tutorial
{
    public class TutorSquenceFriends : TutorSequenceBase
    {
        private readonly UnityEvent onGoToFriendClicked = new UnityEvent();
        private UIBaseViewListItem.OnUIBaseViewListItemClickDelegate oldButtonCallbacks;

        public TutorSquenceFriends(IGameCore gameCore, PresentationTutorial presenter)
            :base(gameCore, presenter)
        {

        }

        public override bool NeedBlockGameSave
        {
            get { return true; }
        }

        public override bool IsNeeded
        {
            get
            {
                return gameCore.playerProfile.level >= 5 && !presenter.gui.levelUp.isActiveAndEnabled &&
                       !IsRunning;
            }
        }

        public override void Init(){

            if (Steps.Count > 0)
            {
                Debug.LogError("Trying to initialize TutorSequenceVisitorQuest for second time");
                return;
            }
            base.Init();
        }

        public override void Start()
        {
            AddStep()
                .DebugName("show message")
                .MoveCamera(() =>
                {
                    var provider = presenter.gameObject.GetComponent<IClubInteractionProvider>();
                    var room = provider.GetRoom(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
                    return provider.activeRoom != room
                        ? room.roomObject.transform
                        : null;
                },
                    true)
                .ShowTutorialMessage(Loc.Get("friendVisitTutorialHeader"),
                    Loc.Get("friendVisitTutorialDetails"));

            AddStep()
                .DebugName("open friends window")
                .ShowOverheadArrowAt(() => presenter.gui.hud.btnFriendsTransform, new Vector3(175, 77, 0), new Vector3(0,0,270))
                .ShowRect(() => (RectTransform)presenter.gui.hud.btnFriendsTransform)
                .LockCameraForStep()
                .Until(presenter.gui.friendsList.OnShowHandler)
                .Do(()=> { presenter.SetFriendsButtonActive(true); })
                .DoAtFinish(() =>
                {
                    presenter.SetFriendsButtonActive(false);
                    // mb some analytics?
                });

            AddStep()
                .DebugName("go to Lina")
                .ShowRect(() => presenter.gui.friendsList.items[0].transform as RectTransform)
                .Do(() =>
                {
                    // dark tutorial hack
                    oldButtonCallbacks = presenter.gui.friendsList.items[0].OnClickCallback;
                    presenter.gui.friendsList.items[0].OnClickCallback = OnGoToFiendClickEvent;
                    presenter.gui.friendsList.SetScrollEnabled(false);
                })
                .Until(onGoToFriendClicked)
                .DoAtFinish(() =>
                {
                    presenter.gui.friendsList.items[0].OnClickCallback = oldButtonCallbacks;
                    presenter.gui.friendsList.SetScrollEnabled(true);
                });

            base.Start();
        }

        private void OnGoToFiendClickEvent(UIBaseViewListItem item)
        {
            onGoToFriendClicked.Invoke();
            PersistentCore.instance.analytics.TutorialStep(TutorialStepId.VisitLina); // visit Lina
        }

        protected override void CompleteSequence()
        {
            gameCore.quests.tutorial.forceBlockTutorialsStart = true;
            base.CompleteSequence();
            gameCore.StartCoroutine(DelayedLinaClubLoding());
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        private IEnumerator DelayedLinaClubLoding()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            PersistentCore.ScheduleGameModeSwitching(GameMode.DemoLevel, DemoLevelSource.FromResource);
        }
    }
}