using System;
using System.Collections;
using Core;
using Data;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Events;
using View.UI;
using View.UI.Base;
using Core.Analytics;

namespace Logic.Quests.Tutorial
{
    public class TutorBuyEquipQuest : TutorSequenceBase
    {
        public override bool IsNeeded
        {
            get
            {
                return (gameCore.playerProfile.level >= 3 && !presenter.gui.levelUp.isActiveAndEnabled) &&
                       !IsRunning &&
                       gameCore.roomManager.IsRoomAtInitialSize(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
            }
        }

        public override bool NeedBlockGameSave
        {
            get { return true; }
        }

        public override bool NeedBlockHUD
        {
            get { return false; }
        }

        public override bool IsRepeat
        {
            get { return false; }
        }
        
        private readonly UnityEvent itemBuyed = new UnityEvent();
        private readonly UnityEvent isEditingFinished = new UnityEvent();

        public TutorBuyEquipQuest(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override void Start()
        {
            AddStep()
                .DebugName("show message")
                .MoveCamera(() =>
                {
                    var provider = presenter.gameObject.GetComponent<IClubInteractionProvider>();
                    var room = provider.GetRoom(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
                    return provider.activeRoom != room ? room.roomObject.transform : null;
                }, true)
                .ShowTutorialMessage(Loc.Get("newEquipmentShowHeader"),Loc.Get("newEquipmentShowMessage"));

            AddStep()
                .DebugName("open shop")
                .ShowOverheadArrowAt(() => presenter.gui.hud.btnShop.transform, new Vector3(-100, 200, 0))
                .ShowRect(() => (RectTransform) presenter.gui.hud.btnShop.transform)
                .LockCameraForStep()
                .Do(() => presenter.gui.hud.SetShopNewItems(0))
                .Until(presenter.gui.shopUltimate.OnShowHandler)
                .DoAtFinish(() => {
                    presenter.ShowItemShop();
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.OpenShop); //Open Shop
                });

            AddStep()
                .DebugName("buy")
                .ShowRect(() => (RectTransform) presenter.gui.shopUltimate.list[1].transform)
                .ShowOverheadArrowAt(() => presenter.gui.shopUltimate.list[1].transform,
                    new Vector3(50, -310, 0))
                .Do(() => { presenter.gui.shopUltimate.list[1].OnClickCallback += OnBuyItem; })
                .Do(() => { presenter.gui.shopUltimate.LockItemsScrolling(isLocked: true); })
                .Until(itemBuyed)
                .DoAtFinish(() =>
                {
                    presenter.gui.shopUltimate.list[2].OnClickCallback -= OnBuyItem;
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.SelectStepper); //Select stepper
                })
                .DoAtFinish(() => { presenter.gui.shopUltimate.LockItemsScrolling(isLocked: false); });

            AddStep()
                .DebugName("wait for decor installation finished")
                .Do(() => gameCore.StartCoroutine(WaitForClubEditingFinished(isEditingFinished.Invoke)))
                .Until(isEditingFinished)
                .DoAtFinish(() =>
                { 
                        PersistentCore.instance.analytics.TutorialStep(TutorialStepId.BuyStepper); //Buy stepper 
                })
                .BreakAtStartIf(() => !presenter.IsClubInEditingMode());

            base.Start();
        }

        private IEnumerator WaitForClubEditingFinished(Action action)
        {
            yield return new WaitUntil(() => !presenter.IsClubInEditingMode());
            action.Invoke();
        }
        private void OnBuyItem(UIBaseViewListItem item)
        {
            itemBuyed.Invoke();
        }
    }
}