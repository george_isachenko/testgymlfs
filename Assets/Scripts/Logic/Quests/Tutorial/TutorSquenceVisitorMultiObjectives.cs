﻿using System.Linq;
using Core;
using Data;
using Logic.Persons;
using Logic.Core;
using Presentation.Facades;
using UnityEngine;

namespace Logic.Quests.Tutorial
{
    public class TutorSquenceVisitorMultiObjectives : TutorSequenceBase
    {

        public TutorSquenceVisitorMultiObjectives(IGameCore gameCore, PresentationTutorial presenter)
            : base(gameCore, presenter)
        {

        }

        public override bool NeedBlockGameSave
        {
            get { return true; }
        }

        public override bool IsNeeded
        {
            get { return GetMultiObjectivesVisitor() != null && !presenter.gui.levelUp.isActiveAndEnabled; }
        }

        public override void Init()
        {

            if (Steps.Count > 0)
            {
                Debug.LogError("Trying to initialize TutorSequenceVisitorQuest for second time");
                return;
            }
            base.Init();
        }

        public override void Start()
        {
            Person visitor = null;

            AddStep()
                .DebugName("look at visitor")
                .ShowTutorialMessage(Loc.Get("moreExercisesShowMessageHeader"), Loc.Get("moreExercisesShowMessageText"))
                .Do(() =>
                {
                    visitor = GetMultiObjectivesVisitor();
                    gameCore.personManager.SelectCharacterRequest(visitor.id);
                })
                .ShowOverheadArrowAt(() => presenter.gui.hud.personPanel.visitor.transform,
                    new Vector3(-225, -200, 0),
                    new Vector3(0, 0, 180))
                .MoveCamera(() => presenter.GetWorldPositionProvider(visitor), true)
                .DoAtFinish(() => { /*PersistentCore.instance.analytics.GameEvent("Tutor9¾:Founded hidden wall");*/ });

            base.Start();
        }

        private Person GetMultiObjectivesVisitor()
        {
            var quest = gameCore.quests.visitorQuests.quests.FirstOrDefault(q => q.objectivesCount > 1);
            return quest == null ? null : gameCore.personManager.GetPerson(quest.visitorId);
        }
    }
}