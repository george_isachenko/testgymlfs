﻿using UnityEngine;
using Logic.Core;
using System.Collections.Generic;
using System;
using Data.Room;
using Logic.Persons;
using Presentation.Facades;

namespace Logic.Quests.Tutorial
{

    public class TutorSequenceBase
    {
        public TutorSequenceBase(IGameCore gameCore, PresentationTutorial presenter)
        {
            this.gameCore = gameCore;
            this.presenter = presenter;
        }

        public bool         IsRunning               { get { return CurrentStep != null; } }
        public virtual bool IsNeeded                { get { return false; } }
        public virtual bool NeedBlockGameSave       { get { return false; } }
        public virtual bool IsRepeat                { get { return false; } }
        public virtual bool NeedBlockHUD            { get { return true; } }
        public virtual bool IsParallel              { get { return false; } }
        public virtual bool ForceHandleBirthEvent   { get { return false; } }

        public Action<TutorSequenceBase> OnSequenceFinished;

        protected IGameCore gameCore;
        protected PresentationTutorial presenter;
        protected List<TutorStepLogic> Steps = new List<TutorStepLogic>();

        protected TutorStepLogic CurrentStep
        {
            get { return _currentStepInd > -1 && _currentStepInd < Steps.Count ? Steps[_currentStepInd] : null; }
        }

        private int _currentStepInd = -1;

        public virtual void Init() {}

        public virtual void OnMessageHide()
        {
//            if (IsRunning) CurrentStep.OnMessageHide();
        }

        public virtual void OnLevelUpWindowHide() { }

        public virtual void OnPersonBirth(Person person, bool restored, CharacterDestination destination) { }
        
        public virtual void OnLevelUp(int level) { }

        public virtual void Start()
        {
            if (IsRunning)
            {
                Debug.LogError("Trying to run sequence second time");
                return;
            }
            if (Steps.Count < 1)
            {
                Debug.LogErrorFormat("There are no steps in sequence {0}, starting aborted. Try to add steps before TutorSequenceBase.Start() method call.",GetType().Name);
                if (!IsRepeat) CompleteSequence();
                return;
            }
            _currentStepInd = 0;
            CurrentStep.Start();
            Debug.LogFormat("Tutorial: sequence {0} started!", GetType().Name);
            Debug.LogFormat("Step {0} ({1}) from tutor sequence {2} started.", _currentStepInd,
                CurrentStep.debugName, GetType().Name);
            Steps.ForEach(s => s.onCompletedCallback.AddListener(RunNextStep));
        }
        
        protected void RunNextStep(bool cancelAllSteps)
        {
            if (cancelAllSteps)
            {
                CompleteSequence();
            }
            else
            {
                _currentStepInd++;
                if (IsRunning)
                {
                    CurrentStep.Start();
                    Debug.LogFormat("Step {0} ({1}) from tutor sequence {2} started.", _currentStepInd,
                        CurrentStep.debugName, GetType().Name);
                }
                else
                    CompleteSequence();
            }
        }

        protected virtual void CompleteSequence()
        {
            _currentStepInd = -1;
            Debug.LogFormat("Tutorial: sequence {0} finished!", GetType().Name);
            Steps.ForEach(s => s.onCompletedCallback.RemoveAllListeners());

            OnSequenceFinished?.Invoke(this);
        }

        protected TutorStepLogic AddStep()
        {
            var step = new TutorStepLogic(gameCore, presenter);
            Steps.Add(step);
            return step;
        }

        public virtual void OnDestroy()
        {
        }
    }
}