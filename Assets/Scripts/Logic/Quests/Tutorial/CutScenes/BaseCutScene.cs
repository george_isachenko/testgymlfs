﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.CameraHelpers;
using View.UI;

public class BaseCutScene : MonoBehaviour 
{
    protected virtual string    cameraTransitionName    { get { return "Camera"; } }

    Animator            animator;
    Transform           cameraTransitionTransform;

    bool                guiCameraOn             { set { GUIRoot.instance.UICamera.enabled = value; } }
    CameraController    camController;
    // Camera              localMainCamera;

    void Awake()
    {
        animator = GetComponent<Animator>();
        cameraTransitionTransform = transform.Find(cameraTransitionName);
        // localMainCamera = cameraTransitionTransform.gameObject.GetComponent<Camera>();
    }

    // Use this for initialization
    void Start () {
        camController = CameraController.instance;
        gameObject.SetActive(false);
    }

    public void Play()
    {
        gameObject.SetActive(true);
        animator.enabled = true;
        animator.Rebind();

        camController.ClearCullingMask();
        guiCameraOn = false;
    }

    public void SwitchToGameCamera()
    {
        camController.ResotreCullingMask();
        camController.SetCameraTransform(cameraTransitionTransform);
        guiCameraOn = true;
    }

    public void SceneEnd()
    {
        animator.enabled = false;
        gameObject.SetActive(false);
    }
}
