﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.CameraHelpers;

public class CutSceneMainCamera : MonoBehaviour 
{
    void OnEnable()
    {
        tag = "MainCamera";
        CameraController.instance.mainCameraOn = false;
    }

    void OnDisable()
    {
        tag = "Untagged";
        CameraController.instance.mainCameraOn = true;
    }

}
