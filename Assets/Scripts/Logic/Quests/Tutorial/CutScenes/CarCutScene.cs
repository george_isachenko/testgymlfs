﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.CameraHelpers;
using View.UI;
using System;

public class CarCutScene : BaseCutScene {
    protected override string    cameraTransitionName    { get { return "Cam3"; } }
}
