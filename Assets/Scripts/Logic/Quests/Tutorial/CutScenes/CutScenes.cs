﻿using UnityEngine;

public class CutScenes : MonoBehaviour 
{
    public static CutScenes instance = null;

    [HideInInspector] public IntroCutScene      introCutScene;
    [HideInInspector] public CarCutScene        carCutScene;

    void Awake()
    {
        instance = this;

        introCutScene = GetComponentInChildren<IntroCutScene>();
        carCutScene = GetComponentInChildren<CarCutScene>();
    }
}
