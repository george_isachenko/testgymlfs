﻿using System;
using System.Collections;
using System.Linq;
using Data;
using Data.Estate;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Events;

namespace Logic.Quests.Tutorial
{
    public class TutorBuyDecorQuest : TutorSequenceBase
    {
        public override bool IsNeeded
        {
            get
            {
                return gameCore.playerProfile.level >= decorStartLevel && !presenter.gui.levelUp.isActiveAndEnabled &&
                       !IsRunning;
            }
        }

        public override bool NeedBlockGameSave
        {
            get { return true; }
        }

        public override bool NeedBlockHUD
        {
            get { return false; }
        }

        public override bool IsRepeat
        {
            get { return false; }
        }
        
        private readonly UnityEvent isEditingFinished = new UnityEvent();

        private int decorStartLevel
        {
            get
            {
                return DataTables.instance.estateTypes.Min(d => d.Value.itemType == EstateType.Subtype.Decor ? d.Value.levelRequirement : int.MaxValue);
            }
        }

        public TutorBuyDecorQuest(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override void Start()
        {
            AddStep()
                .DebugName("Move camera")
                .MoveCamera(() =>
                {
                    var provider = presenter.gameObject.GetComponent<IClubInteractionProvider>();
                    var room = provider.GetRoom(gameCore.roomManager.GetRoomNameOfType(RoomType.Club));
                    return provider.activeRoom != room ? room.roomObject.transform : null;
                },true);

            AddStep()
                .DebugName("open shop")
				.ShowTutorialMessage(Loc.Get("decorationsShowMessage"))
                .ShowOverheadArrowAt(() => presenter.gui.hud.btnShop.transform, new Vector3(-100, 200, 0))
                .ShowRect(() => (RectTransform) presenter.gui.hud.btnShop.transform)
                .Do(() => presenter.gui.hud.SetShopNewItems(0))
                .Until(presenter.gui.shopUltimate.OnShowHandler)
                .DoAtFinish(() => presenter.ShowDecorShop());

            AddStep()
                .DebugName("wait for shop closed")
                .Until(presenter.gui.shopUltimate.OnHideHandler);

            AddStep()
                .DebugName("wait for decor installation finished")
                .Do(() => gameCore.StartCoroutine(WaitForClubEditingFinished(isEditingFinished.Invoke)))
                .Until(isEditingFinished)
                .BreakAtStartIf(() => !presenter.IsClubInEditingMode());

            base.Start();
        }

        private IEnumerator WaitForClubEditingFinished(Action action)
        {
            yield return new WaitUntil(() => !presenter.IsClubInEditingMode());
            action.Invoke();
        }
    }
}