﻿using Core;
using Data;
using Data.Room;
using Logic.Core;
using Presentation.Facades;
using Presentation.Facades.Interfaces.Club;
using UnityEngine;
using View.PrefabResources;
using View.UI.OverheadUI;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceSportClubUnlock : TutorSequenceBase
    {
        public TutorSequenceSportClubUnlock(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override bool IsRepeat { get { return false; } }
        public override bool NeedBlockGameSave { get { return false; } }
        public override bool NeedBlockHUD { get { return true; } }
        public override bool IsNeeded
        {
            get
            {
                return
                    gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) ==
                    0 &&
                    !IsRunning &&
                    !presenter.gui.levelUp.isActiveAndEnabled &&
                    gameCore.playerProfile.level >=
                    gameCore.roomManager.GetNextExpandUnlockLevel(
                        gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub));
            }
        }

        public override void Start()
        {   
            AddStep()
                .DebugName("camera go")
                .Do(()=> {
                    gameCore.personManager.SetCharactersSelectionMode(false);
                    gameCore.equipmentManager.SetEquipmentSelectionMode(false);
                })
                .MoveCamera(
                    () =>
                        GameObject.Find("_Rooms").GetComponent<GameObjectsArray>().array[1]
                            .GetComponent<IWorldPositionProvider>());
            
            AddStep()
                .DebugName("show message")
                .Delay(1)
                .ShowTutorialMessage(Loc.Get("sportClubAvailableHeader"), Loc.Get("sportClubAvailableText"))
                .DoAtFinish(() =>
                {
                        //PersistentCore.instance.analytics.GameEvent("SportTutor:Welcome Popup Show"); 
                });
            
            AddStep()
                .DebugName("show window")
                .Do(() =>
                {
                    var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub);

                    // HACKERY!!!
                    //PersistentCore.instance.analytics.GameEvent("SportTutor:UlockClub Window Show");
                    UnityEngine.Object.FindObjectOfType<PresentationClubGame>().ShowGymUnlockMessage
                        ( Loc.Get("unlockSportClubHeader")
							, Loc.Get(string.Format("unlock{0}AvailableMessage", roomName))
                        , gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)
                        , gameCore.roomManager.GetExpandCost(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)));
                })
                .Until(presenter.gui.gymUnlock.OnHideHandler)
                .DoAtFinish(() =>
                {
                    gameCore.personManager.SetCharactersSelectionMode(true);
                    gameCore.equipmentManager.SetEquipmentSelectionMode(true);
                });

            /*
            AddStep()
                .DebugName("show rateUs")
                .Do(() =>
                {

                })                
                .DoAtFinish(() =>
                {
                    if(gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) <= 0)
                        presenter.ShowRateUs();
                });
             */
            

            base.Start();
        }
    }
}