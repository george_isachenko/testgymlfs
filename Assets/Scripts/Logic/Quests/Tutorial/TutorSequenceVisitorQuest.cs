﻿using Core;
using Core.Analytics;
using Data;
using Data.Estate;
using Data.Person;
using Data.Room;
using Logic.Core;
using Logic.Estate;
using Logic.Estate.Events;
using Logic.Persons;
using Logic.Persons.Events;
using Logic.Persons.Tasks;
using Presentation.Facades;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace Logic.Quests.Tutorial
{
    public class TutorSequenceVisitorQuest : TutorSequenceBase
    {
        public TutorSequenceVisitorQuest (IGameCore gameCore, PresentationTutorial presenter)
            :base(gameCore, presenter)
        {
        }

        public override bool IsNeeded               { get { return gameCore.playerProfile.level == 1 && !IsRunning && !DebugHacks.gameLoaded; } }
        public override bool NeedBlockGameSave      { get { return true; } }
        public override bool IsRepeat               { get { return false; } }
        public override bool NeedBlockHUD           { get { return true; } }
        public override bool ForceHandleBirthEvent  { get { return true; } }

        private string clubRoomName;
        private Person person;
        private int cachedEquipmentId = EquipmentData.InvalidId;

        private readonly UnityEvent characterSelected = new UnityEvent();
        private readonly UnityEvent equipmentSelected = new UnityEvent();
        private readonly UnityEvent exerciseCompleted = new UnityEvent();
        private readonly UnityEvent visitorQuestClaimed = new UnityEvent();
        private readonly UnityEvent exerciseStarted = new UnityEvent();
        private bool skipForceComplete;

        public override void Init()
        {
            clubRoomName = gameCore.roomManager.GetRoomNameOfType(RoomType.Club);
            Assert.IsNotNull(clubRoomName);

            if (Steps.Count > 0)
            {
                Debug.LogError("Trying to initialize TutorSequenceVisitorQuest for second time");
                return;
            }
            base.Init();
        }

        public override void Start()
        {
            AddStep()
                .DebugName("Dramatic Camera Movement")
                .ShowBlockRect(false)
                .Do ( () =>
                    {
                        gameCore.roomManager.AllowRoomsActivation(false);
                        presenter.LockCamera(true);
                        CutScenes.instance.introCutScene.Play();
                    });
            
            AddStep()
                .DebugName("spawn visitor")
                .ShowBlockRect(false)
                .Do(() =>
                {
                    person = gameCore.personManager.CreateVisitor(clubRoomName, Person.EntranceMode.ImmediateEntranceFast, "visitor_spawn_tutorial");
                    gameCore.equipmentManager.canEditEquip = false;

                    var destination = new CharacterDestination("visitor_lobby_tutorial");
                    var existingGotoTask = person.FindTask<TaskGoto>();
                    if (existingGotoTask != null)
                    {
                        existingGotoTask.UpdateDestination(destination);
                    }
                });

            AddStep()
                .DebugName("initial move visitor")
                .ShowBlockRect(false)
                .Until(() => person.state == PersonState.Idle);

            AddStep()
                .DebugName("freeze visitor and wait for cut scene")
                .ShowBlockRect(false)
                .Do(() =>
                {
                    var actions = new TaskDelegate.Settings()
                    {
                        isInterruptable = task => true,
                        onUpdate = (task, dt) => Task.UpdateResult.Running
                    };
                    person.AddTask(new TaskDelegate(actions));
                })
                .Until(() => CutScenes.instance.introCutScene.gameObject.activeSelf == false);

            AddStep()
                .DebugName("look to camera")
                .ShowBlockRect(false)
                .Do(() =>
                {
                    gameCore.roomManager.AllowRoomsActivation(true);

                    CharacterLookAtEvent.Send(person, new CharacterDestination(GetTargetEquip()), null);
                });

            AddStep()   // Do NOT put delay for this step - breaks step if selected before delay is finished!
                .DebugName("show welcome message")
                .ShowTutorialMessage(Loc.Get("visitorTutorialShowWelcomeMessage"))
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(100, 250), new Vector2(0, 0), false)
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider (person),
                    new Vector3(0, 70, 0))
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetTargetEquip()))
                .Do(() =>
                {
                    SetSelectionEvent.Event += OnCharacterSelected;
                })
                .Until(characterSelected)
                .LockCameraForStep()
                .DoAtFinish(() =>
                {
                    SetSelectionEvent.Event -= OnCharacterSelected;
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.Client1Selected); //GameEvent("Tutor1:Client Selected");
                });

            AddStep()
                .DebugName("select equip")
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(GetTargetEquip()))
                .ShowTutorialMessage(Loc.Get("visitorTutorialSelectEquip"))
                .ShowRect(() => presenter.GetWorldPositionProvider(GetTargetEquip()),
                    new Vector2(100, 100),
                    Vector2.zero,
                    false)
                .MoveCamera(() => presenter.GetWorldPositionProvider(GetTargetEquip()))
                .LockCameraForStep()
                .Do(() =>
                {
                    EquipmentSelectionEvent.Event += OnEquipmentSelected;
                })
                .Until(equipmentSelected)
                .DoAtFinish(() => 
                { 
                    EquipmentSelectionEvent.Event -= OnEquipmentSelected; 
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.EquipmentSelected); //GameEvent("Tutor1:Equipment Selected");
                });

            AddStep()
                .DebugName("select exercise")
                .ShowOverheadArrowAt(() => presenter.gui.hud.exerciseSelectorPanel.exercise1.itemButton.transform,
                    new Vector3(0, 100, 0))
                .ShowRect(() => (RectTransform) presenter.gui.hud.exerciseSelectorPanel.exercise1.itemButton.transform)
                .ShowTutorialMessage(Loc.Get("visitorTutorialSelectExercise"))
                .Do(() =>
                {
                    CharacterStartExerciseEvent.Event += OnCharacterStartExerciseEvent;

                    presenter.gui.hud.exerciseSelectorPanel.exercise1.txtDuration = string.Empty;
                    presenter.gui.hud.exerciseSelectorPanel.exercise2.txtDuration = string.Empty;
                    presenter.gui.hud.exerciseSelectorPanel.exercise3.txtDuration = string.Empty;
                })
                .Until(exerciseStarted) 
                .DoAtFinish(() => 
                { 
                    CharacterStartExerciseEvent.Event -= OnCharacterStartExerciseEvent; 
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.StartExercise); //GameEvent("Tutor1:Start Exercise");
                });
            
            AddStep()
                .DebugName("select character")
                .ShowTutorialMessage(Loc.Get("visitorTutorialForceComplete"))
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(person), new Vector3(0, 0, 0))
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(100,100), new Vector2(0,-120), false)
                .LockCameraForStep()
                .Do(() =>
                {
                    SetSelectionEvent.Event += OnCharacterSelected;
                    CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent;
                })
                .Until(characterSelected)
//              .Until(exerciseCompleted)
                .DoAtFinish(() =>
                {
                    SetSelectionEvent.Event -= OnCharacterSelected;
                    CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent;
                });

            AddStep()
                .DebugName("force complete")
                .BreakAtStartIf(() => skipForceComplete)
                .ShowOverheadArrowAt(() => presenter.gui.hud.skipPanel.transform,
                    new Vector3(110, 80, 0), new Vector3(0, 0, -90))
                .ShowTutorialMessage(Loc.Get("visitorTutorialForceComplete"))
                .ShowRect(() => (RectTransform) presenter.gui.hud.skipPanel.transform, false)
                .LockCameraForStep()
                .Do(() =>
                {
                    CharacterEndedExerciseEvent.Event += OnCharacterEndedExerciseEvent;
                })
                .Until(exerciseCompleted)
                .DoAtFinish(() =>
                {
                    CharacterEndedExerciseEvent.Event -= OnCharacterEndedExerciseEvent; 
                    PersistentCore.instance.analytics.TutorialStep(TutorialStepId.FinishExercise);
                    //GameEvent("Tutor1:Finish Exercise");
                });

            AddStep()
                .DebugName("claim quest")
                .ShowTutorialMessage(Loc.Get("visitorTutorialClaimQuest"))
                .ShowRect(() => presenter.GetWorldPositionProvider(person), new Vector2(100, 250), new Vector2(0, 0), false)
                .MoveCamera(() => presenter.GetWorldPositionProvider(person))
                .LockCameraForStep()
                .ShowOverheadArrowAt(() => presenter.GetWorldPositionProvider(person),
                    new Vector3(0, 70, 0), new Vector3(0, 0, 0))
                .Do(() =>
                {
                    VisitorQuestClaimedRequest.Event += OnVisitorQuestClaimed;
                })
                .Until(visitorQuestClaimed)
                .DoAtFinish(() => 
                { 
                    VisitorQuestClaimedRequest.Event -= OnVisitorQuestClaimed;
                    gameCore.equipmentManager.canEditEquip = true;
                });

         //   AddStep()
         //       .ShowTutorialMessage("Great!", "Now try to do it by Yourself! Good Luck!");

            base.Start();
        }

        private void OnCharacterStartExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed)
        {
            if (person == this.person)
                exerciseStarted.Invoke();
        }

        private void OnVisitorQuestClaimed(Person person)
        {
            visitorQuestClaimed.Invoke();
        }

        private void OnCharacterEndedExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            skipForceComplete = true;
            exerciseCompleted.Invoke();
        }

        private Equipment GetTargetEquip()
        {
            Equipment equipment = null;
            if (cachedEquipmentId != EquipmentData.InvalidId)
            {
                gameCore.equipmentManager.equipment.TryGetValue(cachedEquipmentId, out equipment);
            }
            else
            {
                var visitor = person as PersonVisitor;
                equipment = (visitor != null)
                    ? (from equip in gameCore.equipmentManager.equipment
                        where
                            visitor.visitorQuest != null &&
                            visitor.visitorQuest.CanUseEquipment(equip.Value.estateType, 1)
                        select equip.Value).FirstOrDefault()
                    : null;
                if (equipment != null)
                    cachedEquipmentId = equipment.id;
            }
            return equipment;
        }

        private void OnCharacterSelected(Person person, bool selected, bool focus)
        {
            if (person == this.person)
                characterSelected.Invoke();
        }

        void OnEquipmentSelected(Equipment equipment, bool selected)
        {
            if (equipment == GetTargetEquip())
                equipmentSelected.Invoke();
        }

/*
        public override void OnPersonBirth(Person person, bool restored, CharacterDestination destination)
        {
            base.OnPersonBirth(person, restored, destination);

            if (this.person == null)
            {
                this.person = person;
            }
        }
*/
    }
}