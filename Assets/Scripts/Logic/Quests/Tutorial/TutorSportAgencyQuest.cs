using Data;
using Logic.Core;
using Presentation.Facades;
using UnityEngine;
using View.UI.OverheadUI;

namespace Logic.Quests.Tutorial
{
    public class TutorSportAgencyQuest : TutorSequenceBase
    {
        public override bool IsNeeded
        {
            get
            {
                return gameCore.playerProfile.level >= sportAgencyStartLevel &&
                       !presenter.gui.levelUp.isActiveAndEnabled && 
                       !IsRunning &&
                       gameCore.roomManager.GetRoomExpandStage(
                           gameCore.roomManager.GetRoomNameOfType(Data.Room.RoomType.SportClub)) > 0;
            }
        }

        public override bool NeedBlockGameSave
        {
            get { return true; }
        }

        public override bool NeedBlockHUD
        {
            get { return true; }
        }

        public override bool IsRepeat
        {
            get { return false; }
        }

        private int sportAgencyStartLevel
        {
            get { return DataTables.instance.balanceData.Agency.startLevel; }
        }

        public TutorSportAgencyQuest(IGameCore gameCore, PresentationTutorial presenter) : base(gameCore, presenter)
        {
        }

        public override void Start()
        {
            AddStep()
                .DebugName("show agency")
                .MoveCamera(() => GameObject.Find("SportAgency").GetComponent<IWorldPositionProvider>(), true)
                .ShowRect(() => GameObject.Find("SportAgency").GetComponent<IWorldPositionProvider>(),
                    new Vector2(380, 390),
                    new Vector2(-200, 50),
                    false)
                .ShowTutorialMessage(Loc.Get("sportAgenctTutorialShowMessage"))
                .ShowOverheadArrowAt(() => GameObject.Find("SportAgency").GetComponent<IWorldPositionProvider>(),
                    Vector3.zero,// new Vector3(-50, 50, 0),
                    new Vector3(0, 0, -90))
                .Until(presenter.gui.sportsmenAgency.OnShowHandler);

            AddStep()
                .DebugName("wait for close")
                .Until(presenter.gui.sportsmenAgency.OnHideHandler);

            base.Start();
        }
    }
}