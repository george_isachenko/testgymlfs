﻿using System.Collections.Generic;
using Data;
using Data.Estate;
using Logic.Core;
using Logic.PlayerProfile;
using UnityEngine;

namespace Logic.Quests
{
    public static class ExerciseEquipSelector
    {
        class RandomEquipItem {

            public RandomEquipItem(EstateType equip)
            {
                this.equip = equip;
            }

            public bool         isRelevant;
            public EstateType   equip       { get; private set; }
        }

        #region Readonly/static data.
        static private readonly float TEIR_1_PROBABILITY_FATIGUE_0 = 90;
        static private readonly float TEIR_2_PROBABILITY_FATIGUE_0 = 5;
        static private readonly float TEIR_1_PROBABILITY_FATIGUE_50 = 35;
        static private readonly float TEIR_2_PROBABILITY_FATIGUE_50 = 35;
        static private readonly float TEIR_1_PROBABILITY_FATIGUE_100 = 5;
        static private readonly float TEIR_2_PROBABILITY_FATIGUE_100 = 15;
        #endregion

        #region Private data.
        private static List<RandomEquipItem>        equipmentRelevancy      = new List<RandomEquipItem> ();
        private static List<EstateType>             equipmentAvailable      = new List<EstateType> ();
        private static List<ExersiseType>           availableExerciseTypes  = new List<ExersiseType> ();

        private static int prevEquipId1 = EquipmentData.InvalidId;
        private static int prevEquipId2 = EquipmentData.InvalidId;

        private static int prevExerciseType1 = -1;
        private static int prevExerciseType2 = -1;
        #endregion

        #region Public API.
        public delegate bool Relevancy(EstateType equip);
        public static List<EstateType> RandomEquip (IGameCore gameCore, bool isSimple, Relevancy func, int count)
        {
            UpdateRelevancy (func);

            var filteredEquip = new List<EstateType> ();
            var result = new List<EstateType> ();

            //////////////////////////
            /// FILTER
            //////////////////////////

            foreach (var item in equipmentRelevancy)
            {
                if (item.isRelevant)
                {
                    if (isSimple)
                    {
                        if (item.equip.IsSimple())
                        {
                            filteredEquip.Add(item.equip);
                        }
                    }
                    else
                        filteredEquip.Add(item.equip);
                }
            }

            ////////////////////////////
            /// Select random
            ////////////////////////////

            if (filteredEquip.Count >= count) // we have enough options in filtered results
            {
                while (result.Count < count)
                {
                    var rndIdx = Random.Range(0, filteredEquip.Count);
                    result.Add( filteredEquip[rndIdx] );
                    filteredEquip.Remove(filteredEquip[rndIdx]);
                }
            }
            else // fuck this shit and select necessary amount from entire range
            {
                result.AddRange(filteredEquip);

                if (gameCore.playerProfile.level > 1)
                    Debug.LogWarningFormat("RandomEquip : filteredEquip.Count ({0}) < count ({1}), simple = {2}, level = {3}", filteredEquip.Count, count, isSimple, gameCore.playerProfile.level);

                for(var i = filteredEquip.Count; i < count; i++)
                {
                    result.Add(RandomEquipment(gameCore));
                }
            }

            return result;
        }

        public static Exercise RandomExerciseFromEquip(IGameCore gameCore, EstateType equip, bool needSimple)
        {
            var tier = needSimple ? 1 : RandomTier(gameCore);

            foreach(var exe in equip.exercises)
            {
                if (exe.tier == tier)
                    return exe;
            }

            Debug.LogError("IMPOSIBRU RandomExerciseFromEquip");

            return equip.exercises[0];
        }

        /*public static Exercise RandomExercise (IGameCore gameCore, int _tier = -1 , int _maxLevel = -1)
        {
            var rndEquip = RandomEquipment(gameCore);
            int tier = _tier;

            if (tier == -1)
                tier = RandomTier(gameCore);

            var exercises = DataTables.instance.exercises;
            foreach(var exeId in rndEquip.exercisesIds)
            {
                var exe = exercises[exeId];

                if (_maxLevel == -1)
                {
                    if (exe.tier == tier)
                        return exe;
                }
                else
                {
                    if (exe.tier == tier && exe.workstationLevel <= _maxLevel)
                        return exe;
                }
            }

            Debug.LogError("RandomExercise : can't find exercise with desired tear");

            return exercises[rndEquip.exercisesIds[0]];
        }*/

        static void UpdateRelevancy(Relevancy func)
        {
            foreach ( var item in equipmentRelevancy)
            {
                item.isRelevant = func(item.equip);
            }
        }

        public static Exercise RandomExercise (IGameCore gameCore)
        {
            var rndEquip = RandomEquipment(gameCore);
            var tier = RandomTier(gameCore);

            var exercises = DataTables.instance.exercises;
            foreach(var exeId in rndEquip.exercisesIds)
            {
                var exe = exercises[exeId];

                if (exe.tier == tier)
                    return exe;
            }

            Debug.LogError("RandomExercise : can't find exercise with desired tear");

            return exercises[rndEquip.exercisesIds[0]];
        }

        public static void UpdateAvailability(IGameCore gameCore)
        {
            UpdateAvailableExercises(gameCore);
            UpdateAvailableEquipment(gameCore);

            if (!DebugHacks.gamePassedLoading)
            {
                Debug.LogError("calling UpdateAvailability inside loading");
            }
        }

        private static void UpdateAvailableEquipment(IGameCore gameCore)
        {
            if (equipmentAvailable.Count > 0)
            {
                if (equipmentAvailable [0].levelRequirement == gameCore.playerProfile.level)
                    return;

                equipmentAvailable.Clear ();
                equipmentRelevancy.Clear();
            }

            foreach (var item in DataTables.instance.estateTypes)
            {
                if (item.Value.levelRequirement <= gameCore.playerProfile.level)
                {
                    if (item.Value.itemType == EstateType.Subtype.Training && item.Value.exercisesIds.Length > 0)
                    {
                        equipmentAvailable.Add (item.Value);
                        equipmentRelevancy.Add( new RandomEquipItem(item.Value) );
                    }
                }
            }
        }

        private static void UpdateAvailableExercises(IGameCore gameCore)
        {
            if (DataTables.instance == null || DataTables.instance.exercises.Count == 0)
                Debug.LogError("ExerciseEquipSelector used before initialization.");

            Dictionary<Data.ExersiseType, bool> tmpAvailableExeTypeDic = new Dictionary<ExersiseType, bool>();

            availableExerciseTypes.Clear();


            tmpAvailableExeTypeDic.Add(ExersiseType.Torso, false);
            tmpAvailableExeTypeDic.Add(ExersiseType.Cardio, false);
            tmpAvailableExeTypeDic.Add(ExersiseType.Arms, false);
            tmpAvailableExeTypeDic.Add(ExersiseType.Legs, false);


            foreach (var item in DataTables.instance.exercises)
            {
                var exe = item.Value;
                if (exe.equipment != null && exe.equipment.itemType == EstateType.Subtype.Training && exe.equipment.levelRequirement <= gameCore.playerProfile.level)
                {
                    if (exe.equipment.stats.arms > 0)
                        tmpAvailableExeTypeDic[ExersiseType.Arms] = true;

                    if (exe.equipment.stats.cardio > 0)
                        tmpAvailableExeTypeDic[ExersiseType.Cardio] = true;

                    if (exe.equipment.stats.legs > 0)
                        tmpAvailableExeTypeDic[ExersiseType.Legs] = true;

                    if (exe.equipment.stats.torso > 0)
                        tmpAvailableExeTypeDic[ExersiseType.Torso] = true;
                }
            }

            foreach( var item in tmpAvailableExeTypeDic)
            {
                if (item.Value == true)
                    availableExerciseTypes.Add(item.Key);
            }
        }

        public static EstateType RandomEquipment(IGameCore gameCore)
        {
            if (equipmentAvailable.Count == 0)
                UpdateAvailableEquipment(gameCore);

            if (gameCore.playerProfile.level > 2 && equipmentAvailable.Count == 1)
            {
                Debug.LogError("IMPOSIBRU gameCore.playerProfile.level > 2 && equipmentAvailable.Count == 1");
                UpdateAvailableEquipment(gameCore);
            }

            if (equipmentAvailable.Count == 0)
            {
                Debug.LogError("FATAL ERROR: no available equipment to select");
                return null;
            }

            if (equipmentAvailable.Count == 1)
            {
                return equipmentAvailable [0];
            }

            var rndIdx = prevEquipId1;

            if (equipmentAvailable.Count == 2)
            {
                if (prevEquipId1 == 0)
                    rndIdx = 1;
                else
                    rndIdx = 0;
            }

            if (equipmentAvailable.Count >= 3)
            {
                while(rndIdx == prevEquipId1 || rndIdx == prevEquipId2)
                {
                    rndIdx = Random.Range(0, equipmentAvailable.Count);
                }
            }
                
            prevEquipId2 = prevEquipId1;
            prevEquipId1 = rndIdx;

            return equipmentAvailable [rndIdx];
        }

        public static ExersiseType RandomExerciseType (IGameCore gameCore)
        {
            if (availableExerciseTypes.Count == 0)
                UpdateAvailableExercises(gameCore);

            if (availableExerciseTypes.Count == 0)
            {
                Debug.LogError("FATAL ERROR: no available exercise type to select");
                return ExersiseType.Unknown;
            }

            if (availableExerciseTypes.Count == 1)
            {
                return availableExerciseTypes [0];
            }

            var rndIdx = prevExerciseType1;

            if (availableExerciseTypes.Count == 2)
            {
                if (prevExerciseType1 == 0)
                    rndIdx = 1;
                else
                    rndIdx = 0;
            }

            if (availableExerciseTypes.Count >= 3)
            {
                while(rndIdx == prevExerciseType1 || rndIdx == prevExerciseType2)
                {
                    rndIdx = Random.Range(0, availableExerciseTypes.Count);
                }
            }

            prevExerciseType2 = prevExerciseType1;
            prevExerciseType1 = rndIdx;

            return availableExerciseTypes [rndIdx];
        }
        #endregion

        #region Private API.
        private static int RandomTier(IGameCore gameCore)
        {
            var levelAccess = gameCore.playerProfile.level;
            //var fatigue = playerProfile.fatigue.ratio;
            var fatigue = 0.5f;

            if (levelAccess <= 5)
                return 1;

            float tier1_probability = 0;
            float tier2_probability = 0;
//            float tier3_probability = 0;

            if (fatigue <= 0.5)
            {
                tier1_probability = Mathf.Lerp(TEIR_1_PROBABILITY_FATIGUE_0, TEIR_1_PROBABILITY_FATIGUE_50, fatigue * 2f);
                tier2_probability = Mathf.Lerp(TEIR_2_PROBABILITY_FATIGUE_0, TEIR_2_PROBABILITY_FATIGUE_50, fatigue * 2f);
            }
            else
            {
                tier1_probability = Mathf.Lerp(TEIR_1_PROBABILITY_FATIGUE_50, TEIR_1_PROBABILITY_FATIGUE_100, (fatigue - 0.5f) * 2f);
                tier2_probability = Mathf.Lerp(TEIR_2_PROBABILITY_FATIGUE_50, TEIR_2_PROBABILITY_FATIGUE_100, (fatigue - 0.5f) * 2f);
            }

//            tier3_probability = 100f - tier1_probability - tier2_probability;

            int percent = Random.Range (1, 100);

            var result = 1;

            if (percent <= tier1_probability)
                result = 1;
            else if (percent <= tier1_probability + tier2_probability)
                result = 2;
            else
                result = 3;

            if (levelAccess <= 10)
                result = Mathf.Min(result, 2);

            return result;
        }




        #endregion
    }
}