﻿using System;
using System.Collections;
using System.Collections.Generic;
using BridgeBinder;
using Data.Quests.DailyQuest;
using Data.Quests.Visitor;
using Logic.FacadesBase;
using Logic.Quests.Base;
using Logic.Quests.DailyBonus;
using Logic.Quests.DailyQuest;
using Logic.Quests.Events;
using Logic.Quests.Juicer;
using Logic.Quests.Tutorial;
using Logic.Quests.Visitor;
using Logic.Serialization;
using Logic.Statistics;
using Presentation.Facades;
using UnityEngine;

namespace Logic.Quests.Facade
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Logic/Quests/Facade/Logic Quests")]
    [BindBridgeInterface(typeof(PresentationQuests))]
    public class LogicQuests : BaseLogicFacade, IPersistentMulti
    {
        #region Public properties.
        public VisitorQuestsManager     visitorQuests   { get; private set; }
        public DailyQuestManager        dailyQuests     { get; private set; }
        public JuiceQuestManager        juiceQuests     { get; private set; }
        public TutorManager             tutorial        { get; private set; }
        //public DailyBonusLogic        dailyBonus      { get; private set; }
        public DailyBonusLogic       dailyBonusNew   { get; private set; }
        #endregion

        #region Private data.
        // private PresentationQuests      presenter;
        private StatisticsLogic         statistics;

        private int persistentEnumeratorIdx = 0;
        private IPersistent[]           persistentInterfaces;
        private NextDayDetector         nextDayDetector;
        List<IBaseQuestLogicTime>       questTimers = new List<IBaseQuestLogicTime>();
        #endregion

        #region IPersistent properties.
        string IPersistent.propertyName
        {
            get
            {
                return persistentInterfaces[persistentEnumeratorIdx].propertyName;
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return persistentInterfaces[persistentEnumeratorIdx].propertyType;
            }
        }
        #endregion

        #region Bridge.
        [BindBridgeSubscribe]
        private void Subscribe(PresentationQuests presentationQuests)
        {
            // presenter = presentationQuests;
        }
        #endregion

        #region Unity API.
        void Awake()
        {
            nextDayDetector = new NextDayDetector();

            tutorial        = new TutorManager(gameCore);
            visitorQuests   = new VisitorQuestsManager(gameCore);
            juiceQuests     = new JuiceQuestManager(gameCore);
            dailyQuests     = new DailyQuestManager(gameCore);
            dailyBonusNew   = new DailyBonusLogic(gameCore, nextDayDetector);

            persistentInterfaces = new IPersistent[]
            {
                visitorQuests,
                dailyQuests,
                dailyBonusNew,
                tutorial,
                juiceQuests
            };

            dailyQuests.OnQuestCompleted_Callback += OnDailyQuestCompleted;

            visitorQuests.OnQuestCompleted_Callback += OnVisitorQuestCompleted;

            statistics = gameCore.statistics;

            // SUBSCRIBE
            statistics.OnStatisticsUpdated_EndExercise += dailyQuests.OnStatisticsUpdated_EndExercise;
            statistics.OnStatisticsUpdated_StatsUp += dailyQuests.OnStatisticsUpdated_StatsUp;
            statistics.OnStatisticsUpdated_QuestCompleted += dailyQuests.OnStatisticsUpdated_QuestCompleted;

            SubscribeTimeUpdate.Event += OnSubscribeTimeUpdate;
            UnSubscribeTimeUpdate.Event += OnUnSubscribeTimeUpdate;

            gameCore.onGameStarted += OnGameStarted;
        }

        void Start()
        {
            dailyQuests.Start();
            visitorQuests.Start();
        }
            
        void OnDestroy()
        {
            gameCore.onGameStarted -= OnGameStarted;

            dailyQuests.OnQuestCompleted_Callback -= OnDailyQuestCompleted;
            
            visitorQuests.OnQuestCompleted_Callback -= OnVisitorQuestCompleted;

            // UNSUBSCRIBE
            statistics.OnStatisticsUpdated_EndExercise -= dailyQuests.OnStatisticsUpdated_EndExercise;
            statistics.OnStatisticsUpdated_StatsUp -= dailyQuests.OnStatisticsUpdated_StatsUp;
            statistics.OnStatisticsUpdated_QuestCompleted -= dailyQuests.OnStatisticsUpdated_QuestCompleted;

            SubscribeTimeUpdate.Event -= OnSubscribeTimeUpdate;
            UnSubscribeTimeUpdate.Event -= OnUnSubscribeTimeUpdate;

            persistentInterfaces = null;

            juiceQuests.OnDestroy();
            juiceQuests = null;

            visitorQuests.OnDestroy();
            visitorQuests = null;

            statistics.Destroy();
            statistics = null;

            dailyQuests.OnDestroy();
            dailyQuests = null;

            dailyBonusNew.Destroy();
            dailyBonusNew = null;

            tutorial.Destroy();
            tutorial = null;
        }

        void Update()
        {
            for (int i = 0; i < questTimers.Count; i++)
                questTimers[i].UpdateTime();
        }
        #endregion

        #region Event handlers.
        void OnDailyQuestCompleted(BaseQuestLogic<DailyQuestData> quest)
        {
            // Debug.Log(">>> LogicQuests.OnDailyQuestCompleted");
            statistics.OnQuestCompleted(quest.type);
        }

        void OnVisitorQuestCompleted(BaseQuestLogic<VisitorQuestData> quest)
        {
            // Debug.Log(">>> LogicQuests.OnVisitorQuestCompleted");
            statistics.OnQuestCompleted(quest.type);
        }
        #endregion

        #region Private methods.
        void OnSubscribeTimeUpdate(IBaseQuestLogicTime quest)
        {
            foreach(var item in questTimers)
            {
                if (item == quest)
                {
                    Debug.LogError("trying to add quest second time");
                    return;
                }
            }

            questTimers.Add(quest);
        }

        void OnUnSubscribeTimeUpdate(IBaseQuestLogicTime quest)
        {
            foreach(var item in questTimers)
            {
                if (item == quest)
                {
                    questTimers.Remove(item);
                    return;
                }
            }

            Debug.LogError("Can't unsubscribe");
        }

        void OnGameStarted (bool isFreshInstall)
        {
            StartCoroutine(NextDayDetectorUpdateJob());
        }

        private IEnumerator NextDayDetectorUpdateJob ()
        {
            yield return null;

            while (true)
            {
                nextDayDetector.Update();

                yield return new WaitForSeconds(1.0f);
            }
        }
        #endregion

        #region IPersistentMulti interface.
        void IPersistent.InitDefaultData()
        {
        }

        bool IPersistent.Deserialize(object obj)
        {
            return persistentInterfaces[persistentEnumeratorIdx].Deserialize(obj);
        }

        object IPersistent.Serialize()
        {
            return persistentInterfaces[persistentEnumeratorIdx].Serialize();
        }

        void IPersistent.OnPreLoad ()
        {
            persistentInterfaces[persistentEnumeratorIdx].OnPreLoad();
        }

        void IPersistent.OnLoaded (bool success)
        {
            persistentInterfaces[persistentEnumeratorIdx].OnLoaded(success);
        }

        void IPersistent.OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            persistentInterfaces[persistentEnumeratorIdx].OnPostLoad(timePassedSinceSave, loadTimeUTC);

            if (persistentEnumeratorIdx == persistentInterfaces.Length - 1)
            {
                nextDayDetector.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }
        }

        void IPersistent.OnLoadSkipped ()
        {
            persistentInterfaces[persistentEnumeratorIdx].OnLoadSkipped();
        }

        bool IPersistentMulti.MoveNext()
        {
            if (persistentEnumeratorIdx < persistentInterfaces.Length - 1)
            {
                persistentEnumeratorIdx++;
                return true;
            }
            return false;
        }

        void IPersistentMulti.Reset()
        {
            persistentEnumeratorIdx = 0;
        }
        #endregion
    }
}