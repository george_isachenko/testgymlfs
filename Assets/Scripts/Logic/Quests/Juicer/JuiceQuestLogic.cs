﻿using Data.Quests.Juicer;
using Logic.Quests.Base;
using UnityEngine;
using Data.PlayerProfile;
using Data;
using System.Linq;
using System;
using Logic.Core;

namespace Logic.Quests.Juicer
{
    public class JuiceQuestLogic : BaseQuestLogicWithObjectives<JuiceQuestData, JuiceQuestObjectiveLogic, JuiceQuestObjectiveData>
    {
        public int      juicerId        { get { return data.juicerId; } }
        public Cost     order           { get { return GetOrder(); } }

        public float    drinkSeconds    { get { return GetDrinkSeconds(); } }
        public int      exerciseId      { get { return GetExerciseId(); } }
        public int      exerciseCompleteId { get { return GetExerciseCompleteId(); } }

        public bool     readyToComplete     { get { return IsReadyToComplete(); } }

        IGameCore       gameCore;

        public JuiceQuestLogic(JuiceQuestData juiceQuestData, IGameCore gameCore)
            : base (juiceQuestData)
        {
            this.gameCore = gameCore;
        }

        Cost GetOrder()
        {
            var cost = new Cost();
            cost.type = Cost.CostType.ResourcesOnly;

            foreach (var obj in objectives)
            {
                cost.AddResource(obj.juice, obj.amount);
            }

            return cost;
        }

        public JuiceQuestLogic(JuiceQuestData juiceQuestData, JuiceQuestObjectiveData[] objectivesData, IGameCore gameCore)
            : base (juiceQuestData)
        {
            this.gameCore = gameCore;
            if (objectivesData == null)
                return;

            if (_objectives != null)
            {
                for (int i = 0; i < objectivesData.Length; i++)
                {
                    if ( i < 4)
                    {
                        var objectiveLogic = new JuiceQuestObjectiveLogic(objectivesData[i]);
                        AddObjective(objectiveLogic);
                    }
                }

                if (objectivesData.Length > 4)
                {
                    Debug.LogErrorFormat("JuiceQuestLogic : trying to construct with {0} objectives", objectivesData.Length);
                }
            }
        }

        public virtual void AddObjective(JuiceQuestObjectiveData data)
        {
            if (objectives.Count < 4)
            {
                var objective = new JuiceQuestObjectiveLogic(data);
                AddObjective(objective);
            }
            else
            {
                Debug.LogWarningFormat("JuiceQuestLogic : trying to add more than 4 objectives");
            }
        }

        public int CountJuiceInObjectives(ResourceType juiceId)
        {
            if (isNew)
                return 0;
            
            int count = 0;
            foreach (var obj in objectives)
            {
                if (obj.juice == juiceId)
                    count += obj.iterationsGoal;
            }
            return count;
        }

        float GetDrinkSeconds()
        {
            return objectives.Sum(x => x.drinkSeconds);
        }

        int GetExerciseId ()
        {
            if (objectives.Count > 0)
            {
                return objectives.First().exerciseId; // Currently we getting Exercise Id from first objective.
            }
            else
            {
                Debug.LogWarningFormat("JuiceQuestLogic : no objectives, cannot get exercise id.");
                return -1;
            }
        }

        int GetExerciseCompleteId ()
        {
            if (objectives.Count > 0)
            {
                return objectives.First().exerciseCompleteId; // Currently we getting Exercise Complete Id from first objective.
            }
            else
            {
                Debug.LogWarningFormat("JuiceQuestLogic : no objectives, cannot get exercise complete id.");
                return -1;
            }
        }

        bool IsReadyToComplete()
        {
            return gameCore.playerProfile.CanSpend(order);
        }
    }
}
