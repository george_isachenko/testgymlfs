﻿using System.Linq;
using Data;
using Data.JuiceBar;
using Data.PlayerProfile;
using Data.Quests.Juicer;
using Logic.Quests.Base;
using Newtonsoft.Json;
using UnityEngine;

namespace Logic.Quests.Juicer
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class JuiceQuestObjectiveLogic : BaseQuestObjectiveLogic<JuiceQuestObjectiveData> 
    {
        QuestReward _reward  = new QuestReward(); 
        JuiceRecipesTableData recipe;
        
        public ResourceType     juice           { get { return data.juiceId; } }
        public int              amount          { get { return data.amount; } }
        public float            drinkSeconds    { get { return GetDrinkSeconds(); } }
        public int              exerciseId      { get { return GetExerciseId(); } }
        public int              exerciseCompleteId { get { return GetExerciseCompleteId(); } }

        public override QuestReward reward      { get { return _reward; } }
            
        public JuiceQuestObjectiveLogic( JuiceQuestObjectiveData data)
            : base (data)
        {
            recipe = DataTables.instance.juiceRecipes.First(obj => obj.id == data.juiceId);
            
            _reward.rewardType = QuestRewardType.CoinsAndExperience;
            _reward.experience = recipe.reward_exp * data.amount;
            _reward.coins = recipe.reward_coins * data.amount;

            data.iterationsGoal = 1;
        }

        float GetDrinkSeconds()
        {
            if (recipe != null)
            {
                return (float)recipe.drink_time.TotalSeconds * amount;
            }
            else
                Debug.LogError("JuiceQuestObjectiveLogic recipe is null");

            return 1;
        }

        int GetExerciseId()
        {
            if (recipe != null)
            {
                return recipe.exercise_id;
            }
            else
            {
                Debug.LogError("JuiceQuestObjectiveLogic recipe is null");
            }

            return 0;
        }

        int GetExerciseCompleteId()
        {
            if (recipe != null)
            {
                return recipe.exercise_complete_id;
            }
            else
            {
                Debug.LogError("JuiceQuestObjectiveLogic recipe is null");
            }

            return 0;
        }
    }
}
