﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic.Quests.Base;
using Data.Quests.Juicer;
using Logic.Serialization;
using Logic.Core;
using Presentation.Quests;
using System;
using UnityEngine.Assertions;
using Data.JuiceBar;
using Data;
using Logic.JuiceBar.Facade;
using System.Linq;
using Logic.Persons;
using Core.Analytics;
using Core;
using Data.PlayerProfile;

namespace Logic.Quests.Juicer
{
    public class JuiceQuestManager : BaseQuestManager<JuiceQuestLogic, JuiceQuestData>, IPersistent 
    {
        protected new static readonly int initialQuestsCapacity = 8;
        private static readonly int initialQuestsObjectivesCapacity = initialQuestsCapacity * 3;

        public PresentationQuestsJuicer     presenter       { private get; set; }
        LogicJuiceBar                       juiceBar;

        #region IPersistent API
        string  IPersistent.propertyName { get { return "JuiceQuests"; } }
        Type    IPersistent.propertyType { get { return typeof(PersistentData); } }
        #endregion

        JuiceQuestLogic                     lastSelectedQuest;

        public event Action onQuestClaimed;

        private struct PersistentData
        {
            public struct JuiceQuestObjectiveDataWrapper
            {
                public int juicerId;
                public JuiceQuestObjectiveData data;

                public JuiceQuestObjectiveDataWrapper (int juicerId, JuiceQuestObjectiveData data)
                {
                    this.juicerId = juicerId;
                    this.data = data;
                }
            }

            public List<JuiceQuestData> questsData;
            public List<JuiceQuestObjectiveDataWrapper> questsObjectiveData;
        }

        PersistentData persistentData;

        public JuiceQuestManager(IGameCore gameCore)
            : base(gameCore)
        {
            juiceBar = gameCore.juiceBar;
            Assert.IsNotNull(juiceBar);

            persistentData.questsData = new List<JuiceQuestData>(initialQuestsCapacity);
            persistentData.questsObjectiveData = new List<PersistentData.JuiceQuestObjectiveDataWrapper>(initialQuestsObjectivesCapacity);

            Persons.Events.SetSelectionEvent.Event += OnSetSelectionEvent;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            Persons.Events.SetSelectionEvent.Event -= OnSetSelectionEvent;
        }

        private void OnSetSelectionEvent(Person person, bool selected, bool focus)
        {
            if (person is PersonJuicer)
            {
                lastSelectedQuest = GetQuest(person.id);
                if (lastSelectedQuest != null)
                    presenter.UpdateOrder(lastSelectedQuest);
            }
        }

        public int CountJuiceInQuests(ResourceType juiceId)
        {
            int count = 0;

            foreach(var quest in quests)
            {
                count += quest.CountJuiceInObjectives(juiceId);
            }
            return count;
        }

        public bool TryCompleteSelectedQuest()
        {
            if (lastSelectedQuest != null)
            {
                if (lastSelectedQuest.completed)
                {
                    Debug.LogError("TryCompleteSelectedQuest : quest already completed");
                    return false;
                }

                bool isTutorial = gameCore.quests.tutorial.isRunning;

                var order = lastSelectedQuest.order;

                if (playerProfile.CanSpend(order) || isTutorial)
                {
                    var juicer = gameCore.personManager.GetPerson(lastSelectedQuest.juicerId) as PersonJuicer;
                    if (juicer != null)
                    {
                        var exerciseId = lastSelectedQuest.exerciseId;
                        Exercise exercise;
                        if (DataTables.instance.exercises.TryGetValue(exerciseId, out exercise))
                        {
                            if (juicer.Drink(exercise, lastSelectedQuest.drinkSeconds ))
                            {
                                if(!isTutorial)
                                    playerProfile.SpendMoney(order, new Analytics.MoneyInfo("",""));
                                lastSelectedQuest.CompleteAllObjectives();

                                return true;
                            }
                            else
                            {
                                Debug.LogWarning("JuiceQuestManager : juicer can't drink");
                            }
                        }
                        else
                        {
                            Debug.LogErrorFormat("JuiceQuestManager : Cannot find drinking exercise with id: {0}", exerciseId);
                        }
                    }
                    else
                        Debug.LogError("TryCompleteSelectedQuest wrong juicer ID");

                }
                else
                {
                    presenter.BuyShortageOfJuices(lastSelectedQuest);
                }
            }
            else
            {
                Debug.LogError("TryCompleteSelectedQuest lastSelectedQuest == null");
            }

            return false;
        }

        protected override void OnQuestClaimed(BaseQuestLogic<JuiceQuestData> quest)
        {
            var reward = quest.reward;
            var juiceQuest = quest as JuiceQuestLogic;

            playerProfile.AddExperience(reward.experience, true);
            playerProfile.AddCoins(reward.coins, new Analytics.MoneyInfo("JuicerQuest","Complete"), true);

            Achievements.CompleteJuiceQuest.Send();

            PersistentCore.instance.analytics.GameEvent("Juicer Quest", 
                new GameEventParam("Level", gameCore.playerProfile.level),
                new GameEventParam("Reward Coins amount", reward.coins),
                new GameEventParam("Reward XP amount", reward.experience)
            );

            RemoveQuest(juiceQuest);

            var juicer = gameCore.personManager.GetPerson(juiceQuest.juicerId) as PersonJuicer;
            juicer.RemoveQuest();

            onQuestClaimed?.Invoke();
        }

        public JuiceQuestLogic NewQuest(JuiceQuestData juiceQuestData)
        {
            var juiceMaxCount = 0;
            if (playerProfile.level <= 10)
                juiceMaxCount = 1;
            else if (playerProfile.level <= 15)
                juiceMaxCount = 2;
            else if (playerProfile.level <= 20)
                juiceMaxCount = 3;
            else
                juiceMaxCount = 4;
                
            var juices = juiceBar.GetRandomJuices(UnityEngine.Random.Range(1, juiceMaxCount + 1));

            var quest = new JuiceQuestLogic(juiceQuestData, gameCore);

            foreach(var juice in juices)
            {
                var newObjectiveData = new JuiceQuestObjectiveData();
                newObjectiveData.amount = UnityEngine.Random.Range(1,2);
                newObjectiveData.juiceId = juice;

                quest.AddObjective(newObjectiveData);
                persistentData.questsObjectiveData.Add(new PersistentData.JuiceQuestObjectiveDataWrapper(juiceQuestData.juicerId, newObjectiveData));
            }

            AddQuest(quest, persistentData.questsData);
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return quest;
        }

        public void RemoveQuest(JuiceQuestLogic quest)
        {
            var juicerId = quest.juicerId;
            RemoveQuest(quest, persistentData.questsData);
            persistentData.questsObjectiveData.RemoveAll(qo => qo.juicerId == juicerId);
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

//             if (presenter != null)
//                 presenter.ScheduleUpdateView();
        }

        public JuiceQuestLogic  GetQuest(int juicerId)
        {
            return _quests.Find (quest => quest is JuiceQuestLogic && quest.juicerId == juicerId) as JuiceQuestLogic;
        }

        public bool BuyShortageOfJuices(JuiceQuestLogic quest)
        {
            var cost = quest.order;
            var costShortage = playerProfile.GetShortageCost(cost);
            var costFitbucks = playerProfile.storage.GetExtraBucksPrice(costShortage);

            if (playerProfile.SpendMoney(costFitbucks, new Analytics.MoneyInfo("","")))
            {
                playerProfile.AddReward(costShortage, new Analytics.MoneyInfo("",""), true);
                //var result = playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("",""));
                //Assert.IsTrue(result);

                return true;
            }

            return false;
        }

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {
        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersistentData);

            if (obj is PersistentData)
            {
                persistentData = (PersistentData)obj;

                if (persistentData.questsData != null)
                {
                    foreach (var questData in persistentData.questsData)
                    {
                        if (questData.juicerId == -1 && questData.timer == null)
                        {
                            Debug.LogError("JuiceQuestManager.IPersistent.Deserialize juicerId == -1 && timer == null");
                        }

                        var objectivesDataQuery = from objective in persistentData.questsObjectiveData
                                where  objective.juicerId == questData.juicerId
                            select objective.data;

                        var objectives = objectivesDataQuery.ToArray();

                        AddQuest( new JuiceQuestLogic(questData, objectives, gameCore), null);
                    }
                }
                else
                {
                    persistentData.questsData = new List<JuiceQuestData>(initialQuestsCapacity);
                }

                return true;
            }

            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad ()
        {
        }

        void IPersistent.OnLoaded (bool success)
        {
            if (!success)
            {
                persistentData.questsData.Clear();
                persistentData.questsObjectiveData.Clear();
                _quests.Clear();
            }
            else
            {
                //foreach (var quest in quests)
                //    quest.OnTrySkipVisitorQuest_Callback = TryToSkipQuest;

                persistentData.questsObjectiveData.RemoveAll(qo => !persistentData.questsData.Any(q => q.juicerId == qo.juicerId));
            }
        }

        void IPersistent.OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (JuiceQuestLogic quest in _quests)
            {
                quest.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }
            //presenter.UpdateView();
        }

        void IPersistent.OnLoadSkipped ()
        {

            //presenter.UpdateView();

        }

        #endregion
    }
}
