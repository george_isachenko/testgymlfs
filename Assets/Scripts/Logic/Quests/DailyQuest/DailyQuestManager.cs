﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Advertisement;
using Core.Analytics;
using Data;
using Data.Estate;
using Data.PlayerProfile;
using Data.Quests;
using Data.Quests.DailyQuest;
using DataProcessing.CSV;
using Logic.Core;
using Logic.PlayerProfile.Events;
using Logic.Quests.Base;
using Logic.Quests.Events;
using Logic.Serialization;
using Presentation.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using View;

namespace Logic.Quests.DailyQuest
{
    public class DailyQuestManager : BaseQuestManager<DailyQuestLogic, DailyQuestData>, IPersistent
    {
        private static int QUEST_COUNT = 6;

        public PresentationQuestsDaily  presenter       { private get; set; }

        private struct PersistentData
        {
            public List<DailyQuestData> questsData;
            public int                  questCounter;
        }

        private PersistentData persistentData;
        private List<DailyQuestType> rndQuestTypes = new List<DailyQuestType>(16);

        #region IPersistent API
        string  IPersistent.propertyName { get { return "DailyQuests"; } }
        Type    IPersistent.propertyType { get { return typeof(PersistentData); } }
        #endregion

        public DailyQuestManager(IGameCore gameCore)
            : base(gameCore)
        {
            persistentData.questsData = new List<DailyQuestData>(initialQuestsCapacity);

            DailyBonusClaimed.Event += OnDailyBonusClaimedEvent;
        }

        public void Start ()
        {
            presenter.UpdateContent();

            Advertisements.instance.onRewardedVideoWatched += OnRewardedVideoWatched;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            Advertisements.instance.onRewardedVideoWatched -= OnRewardedVideoWatched;

            DailyBonusClaimed.Event -= OnDailyBonusClaimedEvent;
        }

        private int GetQuestsCount()
        {
            if (playerProfile.level < DataTables.instance.balanceData.Quests.DailyQuests.startLevel)
                return 0;

            //var count = playerProfile.level - 2;
            var count = playerProfile.level;

            return Math.Min(count, QUEST_COUNT);
        }

        public void UpdateItemsCount()
        {
            var count = GetQuestsCount();
            if (_quests.Count == count)
                return;

            if (_quests.Count < count)
            {
                int diff = count - _quests.Count;
                for (int i = 0; i < diff; i++)
                    AddNewQuest();
            }

            presenter.UpdateContent();
        }

        public void CheckItemsOrder()
        {
            if (_quests[0].dailyQuestType == DailyQuestType.DailyQuest)
                return;

            var quest_0 = _quests[0];

            for (int i = 0; i < _quests.Count; i++)
            {
                if (_quests[i].dailyQuestType == DailyQuestType.DailyQuest)
                {
                    _quests[0] = _quests[i];
                    _quests[i] = quest_0;
                    return;
                }
            }

            //Debug.LogWarning("DailyQuestsManager::CheckItemsOrder no DailyQuestType.DailyQuest found");
        }

        public bool CanCliamQuest(DailyQuestLogic quest)
        {
            if (quest.dailyQuestType == DailyQuestType.DailyQuest)
            {
                // IMPORTANT!!! daily quest type store resources count in coins and exp variables
                // Check LogicQuests
                var count = quest.reward.coins + quest.reward.experience;

                if (gameCore.playerProfile.storage.capacityAvailable >= count)
                    return true;
                else 
                    return false;
            }
            else
                return true;
        }

        public void AddNewQuest()
        {
            var complexity =  (1f / QUEST_COUNT) * (_quests.Count + 1);
            var questData = GenerateQuestData(RndQuestType(), complexity);
            var quest = new DailyQuestLogic(questData);
            AddQuest(quest, questData);
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
            quest.getCurrentLevel = () =>{ return gameCore.playerProfile.level; };
            DailyQuestStartEvent.Send(quest);
        }

        private void AddQuest(DailyQuestLogic quest, DailyQuestData questData)
        {
            base.AddQuest (quest, persistentData.questsData);
        }

        int GetMultiplier()
        {
            var lvl = playerProfile.level;

            if (lvl < 10)
                return 2;
            else if (lvl < 16)
                return 3;
            else if (lvl < 21)
                return 4;
            else if (lvl < 26)
                return 5;
            else 
                return 6;
        }

        int GetDailyQuestMinLevel(DailyQuestType quest_)
        {
            switch(quest_)
            {
                case DailyQuestType.Stats: return DataTables.instance.balanceData.Quests.VisitorQuests.statsObjectiveFromLevel;
                case DailyQuestType.Exercise: return 1;
                case DailyQuestType.ExerciseTier: return 8;
                case DailyQuestType.ExerciseTotal: return 1;
                case DailyQuestType.Equipment: return 1;
                case DailyQuestType.VisitorQuest: return 1;
                case DailyQuestType.DailyQuest: return 1;
                case DailyQuestType.ClaimDailyReward: return 1;
                case DailyQuestType.VideoAd: return DataTables.instance.balanceData.Advertisement.startLevel;
            }

            return 1;
        }
            
        private DailyQuestData GenerateQuestData(DailyQuestType type, float complexity)
        {
            var k = GetMultiplier();
            
            var questData = new DailyQuestData();
            persistentData.questCounter++;

            questData.dailyQuestType = type;
            questData.type = QuestType.Daily;
            questData.complexity = complexity;
            switch (type)
            {
            case DailyQuestType.Stats:
                questData.iterationsGoal = (int)Mathf.Lerp(k*3, k*5, complexity); 
                questData.relatedId = (int)ExerciseEquipSelector.RandomExerciseType (gameCore);
                break;

            case DailyQuestType.Exercise:
                questData.iterationsGoal = (int)Mathf.Lerp(k*2, k*3, complexity);
                questData.exercise = ExerciseEquipSelector.RandomExercise(gameCore);
                questData.relatedId = questData.exercise.id;
                break;

            case DailyQuestType.ExerciseTier:
                questData.relatedId = UnityEngine.Random.Range(1,3);

                switch (questData.relatedId)
                {
                case 1:
                    questData.iterationsGoal = (int)Mathf.Lerp(k*2, k*3, complexity); 
                    break;
                case 2:
                    questData.iterationsGoal = (int)Mathf.Lerp(k*1, k*3, complexity); 
                    break;
                case 3:
                    questData.iterationsGoal = (int)Mathf.Lerp(k, k*2, complexity); 
                    break;
                default:
                    Debug.LogError("IMPOSIBRU : GenerateQuestData(DailyQuestType ");
                    questData.iterationsGoal = (int)Mathf.Lerp(k, k*2, complexity); 
                    break;
                }

                break;

            case DailyQuestType.ExerciseTotal:
                questData.iterationsGoal = (int)Mathf.Lerp(k*3, k*5, complexity); 
                break;

            case DailyQuestType.Equipment:
                questData.equipment = ExerciseEquipSelector.RandomEquipment(gameCore);
                questData.relatedId = questData.equipment.id;
                questData.iterationsGoal = (int)Mathf.Lerp(k*1, k*3, complexity); break;

            case DailyQuestType.VisitorQuest:
                questData.iterationsGoal = (int)Mathf.Lerp(k*2, k*3, complexity); 
                break;

            case DailyQuestType.DailyQuest:
                questData.iterationsGoal = Math.Min(5, GetQuestsCount() - 1); //from 3 to 5
                questData.rewardCoins = UnityEngine.Random.Range(1,2);
                questData.rewardExp = UnityEngine.Random.Range(1,1);
                questData.rewardResource = RndResourceReward();
                break;

            case DailyQuestType.ClaimDailyReward:
                questData.iterationsGoal = 1; 
                break;

            case DailyQuestType.VideoAd:
                questData.iterationsGoal = 1; 
                break;

            default:
                Debug.LogError("GENERATING UNKNOWN DAILY QUEST TYPE, sorry for caps"); break;
            }

            if (type != DailyQuestType.DailyQuest)
            {
                DataRow row;
                var rows = DataTables.instance.playerLevels;
                if (rows.Length > playerProfile.level)
                    row = rows[playerProfile.level - 1];
                else
                    row = rows[rows.Length - 1];

                questData.rewardCoins = row.GetInt("dailyquest_coins");
                questData.rewardExp = row.GetInt("dailyquest_exp");
            }

            return questData;
        }

        private ResourceType RndResourceReward()
        {
            var availableResources = DataTables.instance.balanceData.Storage.Resources
                .Where(x => !x.Value.disabled && x.Value.availableAsDailyQuestReward).Select(x => x.Key).ToArray();

            return availableResources[UnityEngine.Random.Range(0, availableResources.Length)];
        }

        public void SaveLazily()
        {
            gameCore.ScheduleSave(ScheduledSaveType.SaveSoon);
        }

            
        bool isQuestTypeExist(DailyQuestType type_)
        {
            foreach (var quest in _quests)
            {
                if (quest.dailyQuestType == type_)
                    return true;
            }

            return false;
        }
            
        private DailyQuestType RndQuestType()
        {
            if (!isQuestTypeExist(DailyQuestType.DailyQuest))
            {
                return DailyQuestType.DailyQuest;
            }

            if (!isQuestTypeExist(DailyQuestType.ClaimDailyReward) && gameCore.quests.dailyBonusNew.IsAvailable())
            {
                return DailyQuestType.ClaimDailyReward;
            }

            if (!isQuestTypeExist(DailyQuestType.VideoAd) && (GetDailyQuestMinLevel(DailyQuestType.VideoAd) <= gameCore.playerProfile.level))
            {
                return DailyQuestType.VideoAd;
            }

            if (rndQuestTypes.Count == 0)
            {
                for (int i = 0; i < Enum.GetNames(typeof(DailyQuestType)).Length - 2; i++)
                {
                    if(GetDailyQuestMinLevel((DailyQuestType)i) <= gameCore.playerProfile.level)
                        rndQuestTypes.Add((DailyQuestType)i);
                }

                rndQuestTypes = rndQuestTypes.OrderBy(a => Guid.NewGuid()).ToList();
            }

           /*
            var item = rndQuestTypes[rndQuestTypes.Count - 1];
            rndQuestTypes.Remove(item);

            while (isQuestTypeExist(item))
            {
                if (rndQuestTypes.Count == 0)
                    return RndQuestType();

                item = rndQuestTypes[rndQuestTypes.Count - 1];
                rndQuestTypes.Remove(item);
            }
            */

            //--- remove exist quests
            foreach (var quest in _quests)
                rndQuestTypes.Remove(quest.dailyQuestType);

            if (rndQuestTypes.Count > 0)
                return rndQuestTypes[UnityEngine.Random.Range(0, rndQuestTypes.Count)];
          
            return DailyQuestType.Exercise;
        }

        public void OnStatisticsUpdated_QuestCompleted(QuestType questType)
        {
            if (questType != QuestType.Visitor)
                return;
            
            foreach (DailyQuestLogic quest in _quests)
            {
                if (quest.dailyQuestType == DailyQuestType.VisitorQuest)
                {
                    quest.CompleteStep();
                }
            }
        }

        public void OnStatisticsUpdated_StatsUp(Data.ExersiseType type, int value)
        {
            foreach (DailyQuestLogic quest in _quests)
            {
                if (quest.dailyQuestType == DailyQuestType.Stats && quest.relatedId == (int)type)
                {
                    quest.CompleteSteps(value);
                }
            }
        }

        public void OnStatisticsUpdated_EndExercise(Logic.Estate.Equipment equipment, Data.Exercise exercise)
        {
            foreach (DailyQuestLogic quest in _quests)
            {
                // EXERCISE TOTAL
                if (quest.dailyQuestType == DailyQuestType.ExerciseTotal)
                {
                    quest.CompleteStep();
                }

                // EXACT EXERCISE
                if (quest.dailyQuestType == DailyQuestType.Exercise && quest.relatedId == exercise.id)
                {
                    quest.CompleteStep();
                }

                // EXERCISE TIER
                if (quest.dailyQuestType == DailyQuestType.ExerciseTier && quest.relatedId == exercise.tier)
                {
                    quest.CompleteStep();
                }

                // EQUIPMENT
                if (quest.dailyQuestType == DailyQuestType.Equipment && equipment.estateType.id == quest.relatedId)
                {
                    quest.CompleteStep();
                }
            }        
        }

        public void ReCreateQuest(DailyQuestLogic quest)
        {
            DailyQuestData questData;
            if (quest.dailyQuestType == DailyQuestType.DailyQuest)
                questData = GenerateQuestData(DailyQuestType.DailyQuest, quest.complexity);
            else
                questData = GenerateQuestData(RndQuestType(), quest.complexity);
            quest.SetNewData(questData, persistentData.questsData);
            quest.OnQuestClaimed_Callback = OnQuestClaimed;

            presenter.UpdateContent();
        
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        protected override void OnQuestCompleted (BaseQuestLogic<DailyQuestData> quest)
        {
            Debug.Log(">>> DailyQuestsManager.OnQuestCompleted");

            Assert.IsTrue (quest.type == QuestType.Daily);
            Assert.IsTrue (quest is DailyQuestLogic);

            presenter.UpdateContent();

//          var dailyQuest = quest as DailyQuestLogic;
//          if (dailyQuest == null)
//              return;

            base.OnQuestCompleted(quest);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        protected override void OnTimeRunOut(BaseQuestLogic<DailyQuestData> quest)
        {
            var dailyQuest = quest as DailyQuestLogic;
            if (dailyQuest != null)
            {
                ReCreateQuest(dailyQuest);
            }
            else
            {
                Debug.LogError("DailyQuestManager OnTimeRunOut : wrong quest type");
            }

            presenter.UpdateContent();

            base.OnTimeRunOut(quest);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        protected override void OnQuestClaimed(BaseQuestLogic<DailyQuestData> quest)
        {
            var dailyQuest = quest as DailyQuestLogic;
            Assert.IsNotNull(dailyQuest);

            if (dailyQuest != null)
            {
                if (quest.reward.coins > 0)
                {
                    playerProfile.AddCoins(quest.reward.coins, new Analytics.MoneyInfo("DailyQuest", "DailyQuest") , true);
                }

                if (quest.reward.experience > 0)
                {
                    playerProfile.AddExperience(quest.reward.experience, true);
                }

                if (dailyQuest.dailyQuestType == DailyQuestType.DailyQuest)
                {
                    playerProfile.AddResource(dailyQuest.rewardResource, dailyQuest.reward.coins, new Analytics.MoneyInfo("DailyQuest", dailyQuest.rewardResource.ToString()), true);
                    playerProfile.AddResource(dailyQuest.GetRepairResource(), dailyQuest.reward.experience, new Analytics.MoneyInfo("DailyQuest", dailyQuest.GetRepairResource().ToString()), true);
                }

                if (dailyQuest.dailyQuestType == DailyQuestType.DailyQuest)
                {
                    return;
                }

                foreach (var item in quests)
                {
                    if (item.dailyQuestType == DailyQuestType.DailyQuest)
                    {
                        item.CompleteStep();
                    }
                }

                presenter.UpdateContent();

                PersistentCore.instance.analytics.GameEvent("DailyQuestComplete",
                new GameEventParam("Level", gameCore.playerProfile.level),
                new GameEventParam("Quest Type", dailyQuest.dailyQuestType.ToString()),
                new GameEventParam("Reward XP amount", quest.reward.experience),
                new GameEventParam("Reward Coins amount", quest.reward.coins));

                Sound.instance.Claim();
            }
            else
            {
                Debug.LogError("DailyQuestManager::onQuestCompleted ACHTUNG");
            }

            base.OnQuestClaimed(quest);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        private void OnDailyBonusClaimedEvent()
        {
            foreach (DailyQuestLogic quest in _quests)
            {
                if (quest.dailyQuestType == DailyQuestType.ClaimDailyReward)
                {
                    quest.CompleteStep();
                }
            }
        }

        private void OnRewardedVideoWatched()
        {
            foreach (DailyQuestLogic quest in _quests)
            {
                if (quest.dailyQuestType == DailyQuestType.VideoAd)
                {
                    quest.CompleteStep();
                }
            }
        }

        public bool IsAvailableDailyBonus()
        {
            return gameCore.quests.dailyBonusNew.IsAvailable();
        }

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersistentData);
            if (obj is PersistentData)
            {
                persistentData = (PersistentData)obj;

                if (persistentData.questsData != null)
                {
                    foreach (var questData in persistentData.questsData)
                    {
                        var quest = new DailyQuestLogic(questData);
                        quest.getCurrentLevel = () =>{ return gameCore.playerProfile.level; };
                        base.AddQuest (quest, null); // Do not pass persistent data storage - it's already here.
                    }
                }
                else
                {
                    persistentData.questsData = new List<DailyQuestData>(initialQuestsCapacity);
                }

                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad ()
        {
        }

        void IPersistent.OnLoaded (bool success)
        {
            if (success)
            {
            }
            else
            {
                persistentData.questsData = new List<DailyQuestData>(initialQuestsCapacity);
                _quests.Clear();

                UpdateItemsCount();
            }
        }

        void IPersistent.OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (DailyQuestLogic quest in _quests)
            {
                quest.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }
            presenter.UpdateContent();
        }

        void IPersistent.OnLoadSkipped ()
        {
            UpdateItemsCount();
        }

        #endregion

        public bool IsAvailableByLevel()
        {
            return playerProfile.level >= DataTables.instance.balanceData.Quests.DailyQuests.startLevel;
        }

        public bool SkipQuest(DailyQuestLogic quest)
        {
            var cost = new Cost(Cost.CostType.BucksOnly, DynamicPrice.GetSkipCost((int) quest.timer.secondsToFinish), 0);
            return playerProfile.SpendMoney(cost, new Analytics.MoneyInfo("Skip", "DailyQuest"));
        }

        public Exercise GetFirstExercise(EstateType equipment)
        {
            return DataTables.instance.exercises.FirstOrDefault(p => p.Value.equipment == equipment).Value;
        }

        public override int NewQuests()
        {
            return _quests.Count(quest => quest.isNew && quest.dailyQuestType != DailyQuestType.DailyQuest) +
                   (((_quests.FirstOrDefault(q => q.dailyQuestType == DailyQuestType.DailyQuest)?.completed) ?? false)
                       ? 1
                       : 0);
        }

        public string GetStorageCapacityRatioStr()
        {
            return playerProfile.storage.capacityRatioStr;
        }

        public float GetStorageCapacityRatio()
        {
            return playerProfile.storage.capacityRatio;
        }
    }
}