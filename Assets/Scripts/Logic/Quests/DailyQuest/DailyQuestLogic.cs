﻿using System;
using System.Collections.Generic;
using Core.Timer;
using Data;
using Data.Estate;
using Data.PlayerProfile;
using Data.Quests;
using Data.Quests.DailyQuest;
using Logic.Quests.Base;
using Logic.Quests.Events;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic.Quests.DailyQuest
{
    public delegate int GetCurrentLevel_delegate();

    [JsonObject(MemberSerialization.OptIn)]
    public class DailyQuestLogic : BaseQuestLogic<DailyQuestData>
    {
        private static float TIME_TO_REFRESH_QUEST_MIN = 0f * 60f; // Seconds.
        private static float TIME_TO_REFRESH_QUEST_MAX = 60f * 60f; // Seconds.
        private static float TIME_TO_REFRESH_QUEST_STEP = 5f * 60f; // Seconds.

        public GetCurrentLevel_delegate getCurrentLevel = null;

        // overriden
        public override QuestReward reward
        {
            get
            {
                return new QuestReward()
                {
                    rewardType = QuestRewardType.CoinsAndExperience,
                    experience = data.rewardExp,
                    coins = data.rewardCoins
                };
            }
        }

        public override ResourceType rewardResource      { get { return data.rewardResource; } }
        public override int          iterationsGoal      { get { return data.iterationsGoal; } }
        public override bool         isNew               { get { return IsNew();             } }

        public DailyQuestType  dailyQuestType           { get { return data.dailyQuestType; } }
        public int             relatedId                { get { return data.relatedId;      } }
        public Exercise        exercise                 { get { return data.exercise;       } }
        public EstateType      equipment                { get { return data.equipment;      } }
        public string          title                    { get { return data.title;          } }
        public string          subTitle                 { get { return data.subTitle;       } }
        public float           complexity               { get { return data.complexity;     } }


        public DailyQuestLogic(DailyQuestData data)
            : base (data)
        {
        }

        public void SetNewData(DailyQuestData data, ICollection<DailyQuestData> dataStorage)
        {
            RemoveSelfFromDataStorage (dataStorage);
            this.data = data;
            AddSelfToDataStorage (dataStorage);
        }

        protected override void OnClaim()
        {
            float refreshTime = getCurrentLevel == null ? TIME_TO_REFRESH_QUEST_MAX : refreshTime = TIME_TO_REFRESH_QUEST_MIN + TIME_TO_REFRESH_QUEST_STEP * (getCurrentLevel() - 3);
            refreshTime = Mathf.Clamp(refreshTime, TIME_TO_REFRESH_QUEST_MIN, TIME_TO_REFRESH_QUEST_MAX);

            data.timer = new TimerFloat(refreshTime);

            if (dailyQuestType == DailyQuestType.DailyQuest)
                data.timer.Complete();

            Achievements.CompleteDailyActivity.Send();

            SubscribeTimeUpdate.Send(this);
        }

        protected override void OnTimeRunOut()
        {
            base.OnTimeRunOut();

            UnSubscribeTimeUpdate.Send(this);
        }

        protected bool IsNew()
        {
            if (completed)
                return !claimed;
            else
                return data.isNew;
        }

        public override void OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            base.OnPostLoad(timePassedSinceSave, loadTimeUTC);

            if (timer != null)
                SubscribeTimeUpdate.Send(this);

            if (data.timer == null && claimed)
            {
                Debug.LogWarning("DailyQestLogic - OnPostLoad: no timer in claimed test. Trying to recreate");
                OnTimeRunOut();
            }

            if (dailyQuestType == DailyQuestType.DailyQuest)
            {
				if (data.rewardResource == ResourceType.None) // this quest must be fixed
                {
                    data.rewardCoins = UnityEngine.Random.Range(1,3);
                    data.rewardExp = UnityEngine.Random.Range(1,1);
                    data.rewardResource = ResourceType.Expand_01;
                }
            }
        }

        public override void CompleteStep()
        {
            if (DebugHacks.questsBooster && dailyQuestType != DailyQuestType.DailyQuest)
                base.CompleteSteps(100);
            else
                base.CompleteStep();
        }

        public override void CompleteSteps(int steps)
        {
            if (DebugHacks.questsBooster && dailyQuestType != DailyQuestType.DailyQuest)
                base.CompleteSteps(100);
            else
                base.CompleteSteps(steps);
        }

        public ResourceType GetRepairResource()
        {
            Assert.IsTrue(dailyQuestType == DailyQuestType.DailyQuest);
            return ResourceType.Equip_01;
        }
    }
}