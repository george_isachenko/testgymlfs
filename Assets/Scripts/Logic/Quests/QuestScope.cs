using System;
using System.Collections.Generic;
using Data.Quests;
using Logic.Quests;
using Logic.Quests.Base;

namespace Logic.Facades
{
    public class QuestScope<TQuestLogic, TQuestData> 
        where TQuestLogic : BaseQuestLogic<TQuestData>
        where TQuestData : BaseQuestData
    {
        public BaseQuestLogic<TQuestData>.OnQuestCompleted_Delegate onQuestCompleted = a => { };
        public BaseQuestLogic<TQuestData>.OnQuestClaimed_Delegate onQuestClaimed = a => { };

        public QuestScope()
        {
            quests = new List<TQuestLogic>();
        }

        protected List<TQuestLogic> quests { get; }

        public void AddQuest(TQuestLogic quest)
        {
            quests.Add(quest);
            quest.OnQuestCompleted_Callback = d => onQuestCompleted(d);
            quest.OnQuestClaimed_Callback = d => onQuestClaimed(d);
        }

        public void RemoveQuest(TQuestLogic quest)
        {
            quests.Remove(quest);
        }

        public void Clear()
        {
            quests.Clear();
        }

        public TQuestLogic GetQuest(int index)
        {
            if (index < 0 || index >= quests.Count) return null;
            return quests[index];
        }

        public List<TQuestLogic> GetQuestList()
        {
            return quests;
        }
    }
}