﻿using System;
using Core;
using Core.Timer;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Person;
using Data.Quests;
using Data.Quests.Visitor;
using Logic.Facades;
using Logic.PlayerProfile;
using Logic.Quests.Base;
using Logic.Quests.Events;
using UnityEngine;

namespace Logic.Quests.Visitor
{
    public delegate void OnStatsUp_Delegate(ExersiseType type, int value);
    public delegate void OnTrySkipVisitorQuest_Delegate(VisitorQuestLogic quest);
    public delegate void RemoveObjectives_Delegate(VisitorQuestLogic quest);

    public class VisitorQuestLogic : BaseQuestLogicWithObjectives<VisitorQuestData, VisitorQuestObjectiveLogic, VisitorQuestObjectiveData>
    {
        public string   visitorName;
        public int      visitorId               { get { return data.visitorId; } }
        public bool     isSimpleExcercises      { get { return GetIsSimpleExercises(); } }
        public bool     isSimpleEquipment       { get { return GetIsSimpleEquip(); } }
        public int      trainerExp              { get { return GetTrainerExp(); } }
        public int      trainerLevel            { get { return GetTrainerLevel(); } }

        Cost                    skipPrice = new Cost();
        IPlayerProfileManager   playerProfile;

        public OnStatsUp_Delegate               OnStatsUp_Callback;
        public OnTrySkipVisitorQuest_Delegate   OnTrySkipVisitorQuest_Callback;
        public RemoveObjectives_Delegate        RemoveObjectives_Callback;

        public VisitorQuestLogic(VisitorQuestData visitorQuestData, IPlayerProfileManager playerProfile)
            : base (visitorQuestData)
        {
            this.playerProfile = playerProfile;
        }

        public VisitorQuestLogic(VisitorQuestData visitorQuestData, VisitorQuestObjectiveData[] objectivesData, IPlayerProfileManager playerProfile)
            : base (visitorQuestData)
        {
            this.playerProfile = playerProfile;

            if (objectivesData == null)
                return;

            if (_objectives != null)
            {
                for (int i = 0; i < objectivesData.Length; i++)
                {
                    if ( i < 3)
                    {
                        var objectiveLogic = new VisitorQuestObjectiveLogic(objectivesData[i]);
                        AddObjective(objectiveLogic);
                    }
                }

                if (objectivesData.Length > 3)
                {
                    Debug.LogErrorFormat("VisitorQuestLogic : trying to construct with {0} objectives. {1}", objectivesData.Length, StackTraceUtility.ExtractStackTrace());
                }
            }
        }

        public virtual void AddObjective(VisitorQuestObjectiveData data)
        {
            if (objectives.Count < 3)
            {
                var objective = new VisitorQuestObjectiveLogic(data);
                AddObjective(objective);
            }
            else
            {
                Debug.LogWarningFormat("VisitorQuestLogic : trying to add more than 3 objectives {0}", StackTraceUtility.ExtractStackTrace());
            }
        }

        public void CompleteStep (Exercise exercise, int equipmentUpgradeLevel)
        {
            bool completeFlag = false;

            foreach (var obj in _objectives)
            {
                if (obj.CanUseEquipment(exercise.equipment, exercise.tier))
                {
                    obj.CompleteSteps(exercise, equipmentUpgradeLevel);
                    completeFlag = true;
                }
            }

            if (!completeFlag)
                Debug.LogError("ACHTUNG: VisitorQuestLogic can't complete step");
        }

        public bool CanUseEquipment(EstateType estateType, int tier)
        {
            foreach (var obj in _objectives)
            {
                if (obj.CanUseEquipment(estateType, tier))
                    return true;
            }

            return false;
        }

        public bool CanUseEquipment(EstateType estateType)
        {
            foreach (var obj in _objectives)
            {
                if (obj.CanUseEquipment(estateType))
                    return true;
            }

            return false;
        }

        public void OnVisitorKick()
        {
            var time0 = 1 * 60;
            var time1 = 10 * 60;
            var time2 = 20 * 60;
            var time3 = 30 * 60;


            if (playerProfile.level <= 8)
                timer = new TimerFloat(time0);
            else if (playerProfile.level <= 11)
                timer = new TimerFloat(time1);
            else if (playerProfile.level <= 15)
                timer = new TimerFloat(time2);
            else
                timer = new TimerFloat(time3);

            if (RemoveObjectives_Callback != null)
                RemoveObjectives_Callback(this);
            else
                Debug.LogError("RemoveObjectives_Callback == null");

            data.visitorId = PersonData.InvalidId;

            SubscribeTimeUpdate.Send(this);
        }

        protected override void OnTimeRunOut()
        {
            base.OnTimeRunOut();

            UnSubscribeTimeUpdate.Send(this);
        }

        public void OnTrySkipQuest()
        {
            if (OnTrySkipVisitorQuest_Callback != null)
                OnTrySkipVisitorQuest_Callback(this);
            else
                Debug.LogError("OnTrySkipVisitorQuest_Callback == null");
        }

        public Cost PriceToSkip()
        {
            skipPrice.type = Cost.CostType.BucksOnly;
            skipPrice.value = DynamicPrice.GetSkipCost((int)timer.secondsToFinish);

            return skipPrice;
        }

        public QuestEstimatedRewards CalculateEstimatedRewards ( IEquipmentInfo equipmentInfo )
        {
            var result = new QuestEstimatedRewards ( new QuestReward (GetRewardType(), 0, 0));

            foreach (var obj in _objectives)
            {
                result = result + obj.CalculateEstimatedRewards(equipmentInfo);
            }

            return result;
        }

/*
        public void SetSportsmen()
        {
            foreach (var objective in objectives)
            {
                objective.rewardType = QuestRewardType.BecameSportsmen;
            }
        }
*/

        public override void OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            base.OnPostLoad(timePassedSinceSave, loadTimeUTC);

            if (timer != null)
                SubscribeTimeUpdate.Send(this);

            /*
            for (int i = 1; i < objectives.Count; i++)
            {
                var obj = objectives[i];
                
                for (int j = 0; j < i; j++)
                {
                    var prev = objectives[j];

                    if (obj.type == prev.type)
                    {
                        if (obj.exerciseType == prev.exerciseType)
                        {
                            Debug.LogError("VisitorQuestLogic : duplicate exerciseType in single quest");
                        }
                        else if (obj.exercise.id == prev.exercise.id)
                        {
                            Debug.LogError("VisitorQuestLogic : duplicate exercise id in single quest");
                        }
                    }
                }
            }*/
        }

        public bool CointainUncompletedExerciseOnEquipment(int equipId)
        {
            if (visitorId == -1)
                return false;

            foreach( var obj in objectives)
                if (obj.type == VisitorQuestType.Exercise && !obj.completed)
                {
                    if (obj.exercise.equipment.id == equipId)
                        return true;
                }

            return false;
        }

        bool GetIsSimpleExercises()
        {
            foreach (var objective in objectives)
            {
                if (objective.type == VisitorQuestType.Exercise)
                {
                    if (!objective.exercise.IsSimple())
                        return false;
                }
            }

            return true;
        }

        bool GetIsSimpleEquip()
        {
            foreach (var objective in objectives)
            {
                if (objective.type == VisitorQuestType.Exercise)
                {
                    if (!objective.exercise.IsEquipSimple())
                        return false;
                }
            }

            return true;
        }

        int GetTrainerLevel()
        {
            var maxLevel = 0;

            foreach (var objective in objectives)
            {
                if (objective.type == VisitorQuestType.Exercise)
                {
                    var lvl = objective.exercise.equipment.trainerLevelRequirement;

                    if (lvl > maxLevel)
                        maxLevel = lvl;
                }
            }

            return maxLevel;
        }

        int GetTrainerExp()
        {
            double totalExerciseDuration = 1;
            int statsPoints = 0;

            foreach (var objective in objectives)
            {
                if (objective.type == VisitorQuestType.Exercise)
                {
                    totalExerciseDuration += objective.exercise.duration.TotalMinutes;
                }
                else if (objective.type == VisitorQuestType.Stats)
                {
                    var iterations = objective.iterationsGoal;

                    switch (objective.exerciseType)
                    {
                    case ExersiseType.Arms:
                        statsPoints += iterations;
                        break;
                    case ExersiseType.Cardio:
                        statsPoints += iterations;
                        break;
                    case ExersiseType.Legs:
                        statsPoints += iterations;
                        break;
                    case ExersiseType.Torso:
                        statsPoints += iterations;
                        break;
                    default:
                        Debug.LogError("GetTrainerExp IMPOSIBRU !!!");
                        break;
                    }
                }
            }

            var totalPoints = (int)totalExerciseDuration + statsPoints;

            return totalPoints;
        }
    }
}
