﻿using System.Linq;
using Data;
using Data.Estate;
using Data.Estate.Info;
using Data.Quests;
using Data.Quests.Visitor;
using Logic.Quests.Base;
using Newtonsoft.Json;
using UnityEngine;

namespace Logic.Quests.Visitor
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class VisitorQuestObjectiveLogic : BaseQuestObjectiveLogic<VisitorQuestObjectiveData> 
    {
        public VisitorQuestType type            { get { return data.type; } }
        public ExersiseType     exerciseType    { get { return data.exerciseType; } }
        public Exercise         exercise        { get { return data.exercise;  } }
        public QuestRewardType  rewardType      { get; set; }

        public override QuestReward reward
        {
            get
            {
                return new QuestReward()
                {
                    rewardType = rewardType,
                    experience = GetRewardExp(),
                    coins = GetRewardCoins()
                };
            }
        }

        public VisitorQuestObjectiveLogic( VisitorQuestObjectiveData data)
            : base (data)
        {
            rewardType = data.rewardType;

            if (data.iterationsDone > 0)
            {
                if (data.accumulatedCoinsReward == 0 || data.accumulatedExperienceReward == 0)
                {
                    // Fix accumulated values for done iterations for old saves without accumulators.

                    var coinsForDoneIterations = CompatGetRewardCoinsForDoneIterations();
                    var expForDoneIterations = CompatGetRewardCoinsForDoneIterations();

                    if (coinsForDoneIterations > data.accumulatedCoinsReward || expForDoneIterations > data.accumulatedExperienceReward)
                    {
                        Debug.LogFormat("Fixing Visitor Quest objective accumulated reward. Steps done: {0}, Coins: {1} (was {2}), Experience: {3} (was {4})."
                            , iterationsDone, coinsForDoneIterations, data.accumulatedCoinsReward, expForDoneIterations, data.accumulatedExperienceReward);

                        data.accumulatedCoinsReward = coinsForDoneIterations;
                        data.accumulatedExperienceReward = expForDoneIterations;
                    }
                }
            }
        }

        public void CompleteSteps(Exercise exercise, int equipmentUpgradeLevel)
        {
            if (completed)
            {
                Debug.LogError("Trying to call CompleteSteps for completed objective ");
                return;
            }
            
            if (type == VisitorQuestType.Exercise)
            {
                data.iterationsDone += 1;

                data.accumulatedCoinsReward += Mathf.FloorToInt(data.exercise.profits.coins * DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[equipmentUpgradeLevel]);
                data.accumulatedExperienceReward += Mathf.FloorToInt(data.exercise.profits.exp * DataTables.instance.balanceData.Equipment.Upgrades.experienceRewardMultipliers[equipmentUpgradeLevel]);

                if (completed)
                    CompleteStep();
            }
            else if (type == VisitorQuestType.Stats)
            {
                var statSteps = Mathf.Min(iterationsLeft, exercise.stats.Get(exerciseType));
                if (statSteps > 0)
                {
                    BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                    if (DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData))
                    {
                        data.accumulatedCoinsReward += Mathf.FloorToInt(statSteps * balanceData.coinsRewardMultiplier * DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[equipmentUpgradeLevel]);
                        data.accumulatedExperienceReward += Mathf.FloorToInt(statSteps * balanceData.experienceRewardMultiplier * DataTables.instance.balanceData.Equipment.Upgrades.experienceRewardMultipliers[equipmentUpgradeLevel]);
                    }
                    else
                    {
                        data.accumulatedCoinsReward += Mathf.FloorToInt(statSteps * DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[equipmentUpgradeLevel]);
                        data.accumulatedExperienceReward += Mathf.FloorToInt(statSteps * DataTables.instance.balanceData.Equipment.Upgrades.experienceRewardMultipliers[equipmentUpgradeLevel]);
                    }

                    CompleteSteps(statSteps);
                }
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }
        }

        private int GetRewardCoins()
        {
            if (type == VisitorQuestType.Exercise)
            {
                return data.accumulatedCoinsReward + data.exercise.profits.coins * iterationsLeft;
            }
            else if (type == VisitorQuestType.Stats)
            {
                BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                if (DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData))
                {
                    return data.accumulatedCoinsReward + Mathf.FloorToInt(iterationsLeft * balanceData.coinsRewardMultiplier);
                }

                return data.accumulatedCoinsReward + iterationsLeft; // No stats in balance data table - no multiplier.
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }

        private int GetRewardExp()
        {
            if (exercise != null)
            {
                return data.accumulatedExperienceReward + data.exercise.profits.exp * iterationsLeft;
            }
            else if (type == VisitorQuestType.Stats)
            {
                BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                if (DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData))
                {
                    return data.accumulatedExperienceReward + Mathf.FloorToInt(iterationsLeft * balanceData.experienceRewardMultiplier);
                }

                return data.accumulatedExperienceReward + iterationsLeft; // No stats in balance data table - no multiplier.
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }

        private int GetRewardCoinsBonus
            ( IEquipmentInfo equipmentInfo, bool useMinimumBonus)
        {
            if (type == VisitorQuestType.Exercise)
            {
                if (iterationsLeft > 0 && CanUseEquipment(equipmentInfo.estateType, data.exercise.tier))
                {
                    return Mathf.FloorToInt(data.exercise.profits.coins * Mathf.Max(0.0f, (DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[equipmentInfo.upgradeLevel] - 1.0f)));
                }
            }
            else if (type == VisitorQuestType.Stats)
            {
                if (iterationsLeft > 0 && CanUseEquipment(equipmentInfo.estateType, 0))
                {
                    BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                    var hasBalanceData = DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData);

                    var statsUsageCandidate = Mathf.Min(iterationsLeft, useMinimumBonus
                        ? equipmentInfo.estateType.exercises.Min (x => x.stats.Get(exerciseType))
                        : equipmentInfo.estateType.exercises.Max (x => x.stats.Get(exerciseType)) );

                    if (statsUsageCandidate > 0)
                    {
                        return Mathf.FloorToInt(statsUsageCandidate * Mathf.Max(0.0f, DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[equipmentInfo.upgradeLevel] - 1.0f) * (hasBalanceData ? balanceData.coinsRewardMultiplier : 1.0f));
                    }
                }
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }

        private int GetRewardExpBonus
            ( IEquipmentInfo equipmentInfo, bool useMinimumBonus)
        {
            if (type == VisitorQuestType.Exercise)
            {
                if (iterationsLeft > 0 && CanUseEquipment(equipmentInfo.estateType, data.exercise.tier))
                {
                    return Mathf.FloorToInt(data.exercise.profits.exp * Mathf.Max(0.0f, DataTables.instance.balanceData.Equipment.Upgrades.experienceRewardMultipliers[equipmentInfo.upgradeLevel] - 1.0f));
                }
            }
            else if (type == VisitorQuestType.Stats)
            {
                if (iterationsLeft > 0 && CanUseEquipment(equipmentInfo.estateType, 0))
                {
                    BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                    var hasBalanceData = DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData);

                    var statsUsageCandidate = Mathf.Min(iterationsLeft, useMinimumBonus
                        ? equipmentInfo.estateType.exercises.Min (x => x.stats.Get(exerciseType))
                        : equipmentInfo.estateType.exercises.Max (x => x.stats.Get(exerciseType)) );

                    if (statsUsageCandidate > 0)
                    {
                        return Mathf.FloorToInt(statsUsageCandidate * Mathf.Max(0.0f, DataTables.instance.balanceData.Equipment.Upgrades.experienceRewardMultipliers[equipmentInfo.upgradeLevel] - 1.0f) * (hasBalanceData ? balanceData.experienceRewardMultiplier : 1.0f));
                    }
                }
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }

        public QuestEstimatedRewards CalculateEstimatedRewards ( IEquipmentInfo equipmentInfo )
        {
            if (equipmentInfo != null && equipmentInfo.upgradeLevel > 0)
            {
                return new QuestEstimatedRewards
                    ( new QuestReward ( rewardType, GetRewardCoins(), GetRewardExp())
                    , new QuestReward ( rewardType, GetRewardCoinsBonus(equipmentInfo, true), GetRewardExpBonus(equipmentInfo, true))
                    , new QuestReward ( rewardType, GetRewardCoinsBonus(equipmentInfo, false), GetRewardExpBonus(equipmentInfo, false)));
            }
            else
            {
                return new QuestEstimatedRewards ( new QuestReward ( rewardType, GetRewardCoins(), GetRewardExp()) );
            }
        }

        public bool CanUseEquipment(EstateType estateType, int tier)
        {
            if (!completed)
            {
                if (type == VisitorQuestType.Exercise)
                {
                    foreach (var exe in estateType.exercisesIds)
                    {
                        if (exe == exercise.id && exercise.tier == tier)
                            return true;
                    }
                }
                else if (type == VisitorQuestType.Stats)
                {
                    return (estateType.stats.Get(exerciseType) > 0);
                }
            }

            return false;
        }

        public bool CanUseEquipment(EstateType estateType)
        {
            if (!completed)
            {
                if (type == VisitorQuestType.Exercise)
                {
                    foreach (var exe in estateType.exercisesIds)
                    {
                        if (exe == exercise.id)
                            return true;
                    }
                }
                else if (type == VisitorQuestType.Stats)
                {
                    return (estateType.stats.Get(exerciseType) > 0);
                }
            }

            return false;
        }

        private int CompatGetRewardCoinsForDoneIterations()
        {
            if (type == VisitorQuestType.Exercise)
            {
                return data.exercise.profits.coins * iterationsDone;
            }
            else if (type == VisitorQuestType.Stats)
            {
                BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                if (DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData))
                {
                    return Mathf.FloorToInt(iterationsDone * balanceData.coinsRewardMultiplier);
                }

                return iterationsDone; // No stats in balance data table - no multiplier.
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }

        private int CompatGetRewardExpForDoneIterations()
        {
            if (exercise != null)
            {
                return data.exercise.profits.exp * iterationsDone;
            }
            else if (type == VisitorQuestType.Stats)
            {
                BalanceData.QuestsBalance.VisitorQuestBalance.StatsObjectiveBalance balanceData;
                if (DataTables.instance.balanceData.Quests.VisitorQuests.StatsObjectives.TryGetValue(exerciseType, out balanceData))
                {
                    return Mathf.FloorToInt(iterationsDone * balanceData.experienceRewardMultiplier);
                }

                return iterationsDone; // No stats in balance data table - no multiplier.
            }
            else
            {
                Debug.LogError("VisitorQuestObjectiveLogic : wrong quest type");
            }

            return 0;
        }
    }
}
