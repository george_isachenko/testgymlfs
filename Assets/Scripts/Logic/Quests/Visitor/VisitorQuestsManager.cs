﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Analytics;
using Data;
using Data.Estate;
using Data.Quests.Visitor;
using Data.Room;
using Logic.Core;
using Logic.Estate;
using Logic.Persons;
using Logic.Persons.Events;
using Logic.Quests.Base;
using Logic.Serialization;
using Presentation.Quests;
using UnityEngine;
using UnityEngine.Assertions;
using View;
using Logic.Trainers.Facade;
using Data.Person;

namespace Logic.Quests.Visitor
{
    public class VisitorQuestsManager : BaseQuestManager<VisitorQuestLogic, VisitorQuestData>, IPersistent
    {
        public PresentationQuestsVisitor    presenter       { private get; set; }
        public LogicTrainers                trainers        { get { return gameCore.trainers; } }

        private static readonly int initialQuestsObjectivesCapacity = initialQuestsCapacity * 3;

        private struct PersistentData
        {
            public struct VisitorQuestObjectiveDataWrapper
            {
                public int visitorId;
                public VisitorQuestObjectiveData data;

                public VisitorQuestObjectiveDataWrapper (int visitorId, VisitorQuestObjectiveData data)
                {
                    this.visitorId = visitorId;
                    this.data = data;
                }
            }

            public List<VisitorQuestData> questsData;
            public List<VisitorQuestObjectiveDataWrapper> questsObjectiveData;
        }
        
        private PersistentData persistentData;

        ////////////////////////////////////////
        // PUBLIC METHODS
        ////////////////////////////////////////

        #region IPersistent API
        string IPersistent.propertyName
        {
            get
            {
                return "VisitorQuests";
            }
        }

        Type IPersistent.propertyType
        {
            get
            {
                return typeof(PersistentData);
            }
        }
        #endregion

        Data.BalanceData.QuestsBalance.VisitorQuestBalance balance;

        public VisitorQuestsManager(IGameCore gameCore)
            : base(gameCore)
        {
            balance = DataTables.instance.balanceData.Quests.VisitorQuests;

            persistentData.questsData = new List<VisitorQuestData>(initialQuestsCapacity);
            persistentData.questsObjectiveData = new List<PersistentData.VisitorQuestObjectiveDataWrapper>(initialQuestsObjectivesCapacity);

            CharacterBirthEvent.Event += OnPersonBirthEvent;
            CharacterDeathEvent.Event += OnPersonDeathEvent;
            CharacterEnteredRoomEvent.Event += OnCharacterEnteredRoomEvent;
            CharacterFinishingExerciseEvent.Event += OnCharacterFinishingExerciseEvent;
        }

        public void Start ()
        {
            presenter.ScheduleUpdateView();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            CharacterBirthEvent.Event -= OnPersonBirthEvent;
            CharacterDeathEvent.Event -= OnPersonDeathEvent;
            CharacterEnteredRoomEvent.Event -= OnCharacterEnteredRoomEvent;
            CharacterFinishingExerciseEvent.Event -= OnCharacterFinishingExerciseEvent;
        }

        public VisitorQuestLogic NewVisitorQuest(VisitorQuestData visitorQuestData)
        {
            // TODO : UGLY HACK HOT FIX

            var quest = GenerateRandomQuest(visitorQuestData, 0.5f /*playerProfile.fatigue.ratio*/, playerProfile.level);

            if (presenter != null)
                presenter.ScheduleUpdateView();

            return quest;
        }

        public void RemoveQuest(VisitorQuestLogic quest)
        {
            var visitorId = quest.visitorId;
            RemoveQuest(quest, persistentData.questsData);
            persistentData.questsObjectiveData.RemoveAll(qo => qo.visitorId == visitorId);
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            if (presenter != null)
                presenter.ScheduleUpdateView();
        }

        private void OnRemoveObjectives(VisitorQuestLogic quest)
        {
            var visitorId = quest.visitorId;
            persistentData.questsObjectiveData.RemoveAll(qo => qo.visitorId == visitorId);
            quest.objectives.Clear();
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            if (presenter != null)
                presenter.ScheduleUpdateView();
        }

        public VisitorQuestLogic GetVisitorQuest(int characterId)
        {
            return _quests.Find (quest => quest is VisitorQuestLogic && quest.visitorId == characterId) as VisitorQuestLogic;
        }

        public int CountQuestWithSimpleExercises()
        {
            int count = 0;
            foreach (var quest in quests)
            {
                if (quest.isSimpleExcercises)
                    count++;
            }
            return count;
        }

        public bool AvailableQuestWith_SimpleExeOnSimpleEquip()
        {
            foreach (var quest in quests)
            {
                if (quest.isSimpleExcercises && quest.isSimpleEquipment)
                    return true;
            }
            return false;
        }

        int GetEquipNotAssemblingCount(int equipId)
        {
            var validStates = new [] {EquipmentState.Broken, EquipmentState.Idle, EquipmentState.Locked, EquipmentState.Repairing, EquipmentState.ShopMode, EquipmentState.Waiting, EquipmentState.Working, EquipmentState.WorkPending};
            var roomName = gameCore.roomManager.GetRoomNameOfType(RoomType.Club);
            Assert.IsNotNull(roomName);

            return gameCore.equipmentManager.GetEquipmentsCount(equipId, EstateType.Subtype.Training, roomName, null, validStates);
        }

        int GetVisitorsCountWantingThisEquip(int equipId)
        {
            var query = quests.Count( q => q.CointainUncompletedExerciseOnEquipment(equipId));
            return query;
        }

        bool IsEquipmentRelevant(EstateType equip)
        {
            var equipsCount = GetEquipNotAssemblingCount(equip.id);
            var visitorsWantingThisEquip = GetVisitorsCountWantingThisEquip(equip.id);

            var reject = visitorsWantingThisEquip > equipsCount +1;

            //Debug.LogFormat("EQUIP {0} REJECT {1} == visitorsWantingThisEquip({2}) > equipsCount({3}) + 1", equip.locNameId, reject, visitorsWantingThisEquip, equipsCount);

            return !reject;
        }
        
        public VisitorQuestLogic GenerateRandomQuest(VisitorQuestData visitorQuestData, float fatigue, int levelAccess)
        {
            bool needSimple = CountQuestWithSimpleExercises() <= 2;
            bool needSimpleExeOnSimpleEquip = AvailableQuestWith_SimpleExeOnSimpleEquip() == false;

            var newQuest = new VisitorQuestLogic (visitorQuestData, gameCore.playerProfile);
            // decide type : EXERCISE or STATS

            int objectivesCount = RandomObjectivesCount (fatigue, levelAccess);
            bool visitorBecameSportman = UnityEngine.Random.value <= balance.sportsmanCreationChance 
                && playerProfile.level >= balance.sportsmanCreationStartLevel;

            var rndEquip = ExerciseEquipSelector.RandomEquip(gameCore, needSimpleExeOnSimpleEquip, IsEquipmentRelevant, objectivesCount);

            for (int i = 0; i < objectivesCount; i++)
            {
                var newObjectiveData = new VisitorQuestObjectiveData();
                var rndPercent = UnityEngine.Random.Range (1, 100);

                // only exercise below 6 level
                if (levelAccess < balance.statsObjectiveFromLevel)
                    rndPercent = 1;

                if (rndPercent <= balance.exerciseProbability)
                {
                    newObjectiveData.type = VisitorQuestType.Exercise;
                    newObjectiveData.exercise = ExerciseEquipSelector.RandomExerciseFromEquip(gameCore, rndEquip[i], needSimple);
                    newObjectiveData.iterationsGoal = RandomExerciseIterations(fatigue, levelAccess);
                }
                else
                {
                    newObjectiveData.type = VisitorQuestType.Stats;
                    newObjectiveData.exerciseType = ExerciseEquipSelector.RandomExerciseType(gameCore);

                    var rnd = UnityEngine.Random.Range(1, (int)Math.Ceiling(levelAccess * 0.2));

                    newObjectiveData.iterationsGoal = rnd * RandomExerciseTypeIterations(fatigue, levelAccess);
                }
                
                newObjectiveData.rewardType = (visitorBecameSportman && gameCore.roomManager.GetRoomExpandStage(gameCore.roomManager.GetRoomNameOfType(RoomType.SportClub)) > 0)
                    ? QuestRewardType.BecameSportsmen
                    : QuestRewardType.CoinsAndExperience;
                newQuest.AddObjective(newObjectiveData);
                persistentData.questsObjectiveData.Add(new PersistentData.VisitorQuestObjectiveDataWrapper(visitorQuestData.visitorId, newObjectiveData));
            }

            if (visitorBecameSportman)
            {
                Debug.LogFormat("Visitor {0} will became a sportsmen!",visitorQuestData.visitorId);
            }

            newQuest.OnTrySkipVisitorQuest_Callback = TryToSkipQuest;

            AddQuest(newQuest, persistentData.questsData);
            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);

            return newQuest;
        }

        public Rooms.IRoomManager roomManager { get { return gameCore.roomManager; } }

        private void OnCharacterFinishingExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            var visitor = person as PersonVisitor;
            if (visitor != null)
            {
                var visitorQuest = visitor.visitorQuest;

                Assert.IsNotNull(visitorQuest);
                if (visitorQuest != null)
                    visitorQuest.CompleteStep(exercise, equipment.upgradeLevel);
            }
        }

        private int RandomObjectivesCount(float fatigue, int levelAccess)
        {
            if (levelAccess <= balance.max_1_objectiveToLevel)
                return 1;
            
            int percent = UnityEngine.Random.Range (1, 100);
            int value = 1;

            if (percent <= balance.objectiveCountPercentThreshold_1)
                value = 1;
            else if (percent <= balance.objectiveCountPercentThreshold_1 + balance.objectiveCountPercentThreshold_2)
                value = 2;
            else
                value = 3;

            if (levelAccess <= balance.max_2_objectivesToLevel)
                value = Mathf.Min(value, 2);

            return value;
        }

        private int RandomExerciseTypeIterations(float fatigue, int levelAccess)
        {
            if (levelAccess == 1)
                return 1;
               
            return RandomExerciseIterations(fatigue, levelAccess);
        }

        private int RandomExerciseIterations(float fatigue, int levelAccess)
        {
            int max = 1;

            if (levelAccess >= 8 && levelAccess <= 11)
                max = 2;
            else if (levelAccess >= 12 && levelAccess <= 18)
                max = 3;
            else if (levelAccess >= 19 && levelAccess <= 24)
                max = 4;
            else if (levelAccess >= 25 && levelAccess <= 30)
                max = 5;
            else if (levelAccess >= 31)
                max = 6;

            var rnd = UnityEngine.Random.Range(1, max + 1);

            return rnd;
        }

        private void OnPersonBirthEvent(Person person, bool restored, CharacterDestination spawnDestination)
        {
            if (!restored)
            {
                presenter.ScheduleUpdateView();
            }
        }

        private void OnCharacterEnteredRoomEvent(Person person)
        {
            presenter.ScheduleUpdateView();
        }

        private void OnPersonDeathEvent(int id)
        {
            presenter.ScheduleUpdateView();
        }

        protected override void OnTimeRunOut(BaseQuestLogic<VisitorQuestData> quest)
        {
            var visitorQuest = quest as VisitorQuestLogic;
            if (visitorQuest != null)
                RemoveQuest(visitorQuest);
        }

        protected override void OnQuestCompleted (BaseQuestLogic<VisitorQuestData> quest)
        {
            base.OnQuestCompleted(quest);

            Debug.Log(">>> VisitorQuestsManager.OnQuestCompleted");

            presenter.ScheduleUpdateView();

            PersistentCore.instance.analytics.GameEvent("VisitorQuestComplete",
                new GameEventParam("Level", gameCore.playerProfile.level),
                new GameEventParam("Reward Coins amount", quest.reward.coins),
                new GameEventParam("Reward XP amount", quest.reward.experience),
                new GameEventParam("Reward Sportsmen", quest.reward.rewardType == QuestRewardType.BecameSportsmen ? "yes" : "no"));

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        protected override void OnQuestClaimed(BaseQuestLogic<VisitorQuestData> quest)
        {
            Assert.IsTrue(quest is VisitorQuestLogic);
            if (quest is VisitorQuestLogic)
            {
                var person = gameCore.personManager.GetPerson(((VisitorQuestLogic)quest).visitorId);
                Assert.IsNotNull(person);

                if (quest.reward.coins > 0 && quest.reward.rewardType == QuestRewardType.CoinsAndExperience)
                {
                    playerProfile.AddCoins(quest.reward.coins, new Analytics.MoneyInfo("ClientQuest", "ClientQuest") ,true);
                }

                if (quest.reward.experience > 0)
                {
                    playerProfile.AddExperience(quest.reward.experience, true);
                }

                VisitorQuestClaimedRequest.Send(person);

                person.Quit();

                Achievements.TrainVisitor.Send();

                Sound.instance.Claim();
            }

            base.OnQuestClaimed(quest);

            gameCore.ScheduleSave(ScheduledSaveType.SaveOnThisFrame);
        }

        protected void TryToSkipQuest(VisitorQuestLogic quest)
        {
            Cost price = new Cost();

            price.type = Cost.CostType.BucksOnly;
            price.value = 10;

            if (playerProfile.SpendMoney(quest.PriceToSkip(), new Analytics.MoneyInfo("Skip", "Visitor")))
            {
                RemoveQuest(quest);
            }
        }

        protected override void AddQuest(VisitorQuestLogic quest, ICollection<VisitorQuestData> dataStorage)
        {
            base.AddQuest(quest,dataStorage);

            quest.RemoveObjectives_Callback = OnRemoveObjectives;
        }

        #region IPersistent API
        void IPersistent.InitDefaultData()
        {

        }

        bool IPersistent.Deserialize(object obj)
        {
            Assert.IsTrue(obj is PersistentData);
            if (obj is PersistentData)
            {
                persistentData = (PersistentData)obj;

                if (persistentData.questsData != null)
                {
                    foreach (var questData in persistentData.questsData)
                    {
                        if (questData.visitorId == -1 && questData.timer == null)
                        {
                            Debug.LogError("VisitorQuestManager.IPersistent.Deserialize visitorId == -1 && timer == null");
                        }

                        var objectivesDataQuery =   from objective in persistentData.questsObjectiveData
                                                        where  objective.visitorId == questData.visitorId
                                                        select objective.data;

                        var objectives = objectivesDataQuery.ToArray();

                        var quest = new VisitorQuestLogic(questData, objectives, gameCore.playerProfile);
                        AddQuest(quest, null); // Do not pass persistent data storage - it's already here.
                    }
                }
                else
                {
                    persistentData.questsData = new List<VisitorQuestData>(initialQuestsCapacity);
                }

                return true;
            }
            return false;
        }

        object IPersistent.Serialize()
        {
            return persistentData;
        }

        void IPersistent.OnPreLoad ()
        {
        }

        void IPersistent.OnLoaded (bool success)
        {
            if (!success)
            {
                persistentData.questsData.Clear();
                persistentData.questsObjectiveData.Clear();
                _quests.Clear();
            }
            else
            {
                foreach (var quest in quests)
                    quest.OnTrySkipVisitorQuest_Callback = TryToSkipQuest;

                persistentData.questsObjectiveData.RemoveAll(qo => !persistentData.questsData.Any(q => q.visitorId == qo.visitorId));
            }
        }

        void IPersistent.OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            foreach (VisitorQuestLogic quest in _quests)
            {
                quest.OnPostLoad(timePassedSinceSave, loadTimeUTC);
            }
            presenter.ScheduleUpdateView();
        }

        void IPersistent.OnLoadSkipped ()
        {
            presenter.ScheduleUpdateView();
        }

        #endregion
    }

}