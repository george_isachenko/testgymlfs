﻿using System.Collections.Generic;
using System.Linq;
using Data.Quests;
using Logic.Core;
using Logic.FacadesBase;
using UnityEngine;

namespace Logic.Quests.Base
{
    public abstract class BaseQuestManager<QuestLogicT, QuestDataT> : BaseLogicNested
        where QuestLogicT : BaseQuestLogic<QuestDataT>
        where QuestDataT : BaseQuestData
    {
        protected static readonly int initialQuestsCapacity = 64;

        protected List<QuestLogicT> _quests;

        public event BaseQuestLogic<QuestDataT>.OnQuestCompleted_Delegate OnQuestCompleted_Callback;
        public event BaseQuestLogic<QuestDataT>.OnQuestClaimed_Delegate   OnQuestClaimed_Callback;
        public event BaseQuestLogic<QuestDataT>.OnTimeRunOut_Delegate     OnTimeRunOut_Callback;

        public List<QuestLogicT> quests { get { return _quests; } }

        public BaseQuestManager(IGameCore gameCore)
            : base (gameCore)
        {
            _quests = new List<QuestLogicT>(initialQuestsCapacity);
        }

        public virtual void OnDestroy()
        {
            foreach(var quest in quests)
                quest.UnSubscribe();
        }

        public int UnclaimedQuestsCount()
        {
            return _quests.Count(x => !x.claimed);
        }

        public virtual int NewQuests()
        {
            return _quests.Count(x => x.isNew);
        }

        public int ActualQuestsCount()
        {
            return _quests.Count(x => x.timer == null);
        }

        protected virtual void AddQuest(QuestLogicT quest, ICollection<QuestDataT> dataStorage)
        {
            _quests.Insert(0, quest);//_quests.Add(quest); //--- new quest to front
            if (dataStorage != null)
                quest.AddSelfToDataStorage (dataStorage);
            quest.OnQuestClaimed_Callback   = OnQuestClaimed;
            quest.OnQuestCompleted_Callback = OnQuestCompleted;
            quest.OnTimeRunOut_Callback     = OnTimeRunOut;
        }

        protected void RemoveQuest(QuestLogicT quest, ICollection<QuestDataT> dataStorage)
        {
            _quests.Remove(quest);
            if (dataStorage != null)
                quest.RemoveSelfFromDataStorage(dataStorage);

            quest.UnSubscribe();
        }

        protected virtual void OnQuestCompleted(BaseQuestLogic<QuestDataT> quest)
        {
            OnQuestCompleted_Callback?.Invoke(quest);
        }

        protected virtual void OnQuestClaimed(BaseQuestLogic<QuestDataT> quest)
        {
            OnQuestClaimed_Callback?.Invoke(quest);
        }

        protected virtual void OnTimeRunOut(BaseQuestLogic<QuestDataT> quest)
        {
            OnTimeRunOut_Callback?.Invoke(quest);
        }
    }
}