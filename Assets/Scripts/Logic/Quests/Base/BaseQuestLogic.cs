﻿using UnityEngine;
using Data;
using Data.Quests;
using System.Collections.Generic;
using System;
using UnityEngine.Assertions;
using Core.Timer;
using Data.PlayerProfile;

namespace Logic.Quests.Base
{
    public interface IBaseQuestLogicTime
    {
        void UpdateTime();
    }
    
    public abstract class BaseQuestLogic<QuestDataT> : IBaseQuestLogicTime
        where QuestDataT : BaseQuestData
    {
        public delegate void OnQuestCompleted_Delegate(BaseQuestLogic<QuestDataT> quest);
        public delegate void OnQuestClaimed_Delegate(BaseQuestLogic<QuestDataT> quest);
        public delegate void OnTimeRunOut_Delegate(BaseQuestLogic<QuestDataT> quest);

        protected QuestDataT data;

        public OnQuestCompleted_Delegate OnQuestCompleted_Callback;
        public OnQuestClaimed_Delegate   OnQuestClaimed_Callback;
        public OnTimeRunOut_Delegate     OnTimeRunOut_Callback;

        public virtual bool         completed           { get { Assert.IsTrue(iterationsGoal != 0); return iterationsDone >= iterationsGoal;  } }
        public virtual bool         claimed             { get { return data.claimed; } }
        public virtual QuestType    type                { get { return data.type; } }
        public virtual QuestReward  reward              { get { return new QuestReward(); } }
        public virtual ResourceType rewardResource      { get { return ResourceType.None; } }
        public virtual int          iterationsGoal      { get { return 1; } }
        public virtual int          iterationsDone      { get { return data.iterationsDone; } }
        public virtual string       progress            { get { return iterationsDone.ToString() + "/"  + iterationsGoal.ToString(); } }
        public         TimerFloat   timer               { get { return data.timer; } protected set { data.timer = value; } }
        public virtual bool         isNew               { get { return data.isNew; } }
        public string               refreshTime         { get { return GetRefreshTime(); } }
        public QuestStage           questStage          { get { return GetQuestStage(); } }

        public BaseQuestLogic(QuestDataT data)
        {
            this.data = data;
#if DEBUG
            DbgEvents.OneMinuteLeftForQuestTimers.Event += dbgOneMinuteLeft;
#endif
        }

        public virtual void UnSubscribe()
        {
#if DEBUG            
            DbgEvents.OneMinuteLeftForQuestTimers.Event -= dbgOneMinuteLeft;
#endif
        }

        public void WasSeen()             
        {
            data.isNew = false;
        } 

        public QuestStage GetQuestStage()
        {
            return completed
                ? QuestStage.Completed
                : (isNew ? QuestStage.New : QuestStage.InProgress);
        }

        public virtual bool Claim()
        {
            if (completed)
            {
                data.claimed = true;
                OnClaim();

                if (OnQuestClaimed_Callback != null)
                {
                    var tmp = OnQuestClaimed_Callback;
                    OnQuestClaimed_Callback = null;
                    tmp(this);
                }
                else
                    Debug.LogError("OnQuestClaimedCallback is NULL");

                return true;
            }

            Debug.LogError("Trying to Claim uncompleted quest");
            return false;    
        }

        protected virtual void OnClaim()
        {

        }

        private void Complete()
        {
            if (completed)
            {
                if (OnQuestCompleted_Callback != null)
                    OnQuestCompleted_Callback(this);
                else
                    Debug.LogError("OnQuestCompletedCallback is NULL");
            }
            else
                Debug.LogError("Trying to complete uncompleted quest");
        }

        public void UpdateTime()
        {
            if (data.timer != null)
            {
                if (data.timer.secondsToFinish == 0)
                    OnTimeRunOut();
            }
        }

        protected string GetRefreshTime()
        {
            if (data.timer != null)
            {
                if (data.timer.secondsToFinish == 0)
                    OnTimeRunOut();
                else
                {
                    if (data.timer.secondsToFinish > 1)
                        return data.GetTime();
                }
            }

            return string.Empty;
        }

        protected virtual void OnTimeRunOut()
        {
            if (OnTimeRunOut_Callback != null)
                OnTimeRunOut_Callback(this);
            else
                Debug.LogError("OnTimeRunOut_Callback is NULL");
        }

        public virtual void CompleteStep()
        {
            data.iterationsDone++;
            if (data.iterationsDone > iterationsGoal)
                data.iterationsDone = iterationsGoal;

            if (completed)
                Complete();
        }

        public virtual void CompleteSteps(int steps)
        {
            data.iterationsDone += steps;

            if (data.iterationsDone > iterationsGoal)
                data.iterationsDone = iterationsGoal;

            if (completed)
                Complete();
        }
    	
        public void RemoveSelfFromDataStorage (ICollection<QuestDataT> dataStorage)
        {
            if (data != null)
            {
                dataStorage.Remove ( data );
            }
        }

        public void AddSelfToDataStorage (ICollection<QuestDataT> dataStorage)
        {
            if (data != null)
            {
                dataStorage.Add ( data );
            }
        }

        public virtual void OnPostLoad (TimeSpan timePassedSinceSave, DateTime loadTimeUTC)
        {
            if (data.timer != null)
            {
                data.timer.FastForward((float)timePassedSinceSave.TotalSeconds);
            }
        }

        public void dbgOneMinuteLeft()
        {
            if (data.timer != null)
                data.timer.dbgOneMinuteLeft();
        }
    }
}
