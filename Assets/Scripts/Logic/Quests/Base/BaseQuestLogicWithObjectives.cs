﻿using System.Collections.Generic;
using Data;
using Data.Quests;
using System;
using System.Linq;

namespace Logic.Quests.Base
{
    public abstract class BaseQuestLogicWithObjectives<QuestDataT, QuestObjectiveLogicT, QuestObjectiveDataT> : BaseQuestLogic<QuestDataT>
        where QuestObjectiveLogicT : BaseQuestObjectiveLogic<QuestObjectiveDataT>
        where QuestObjectiveDataT : BaseQuestObjectiveData
        where QuestDataT : BaseQuestData
    {
        protected List<QuestObjectiveLogicT> _objectives = new List<QuestObjectiveLogicT>();
        private QuestObjectiveLogicT _lastCompletedObjective;

        public override QuestReward reward
        {
            get
            {
                return new QuestReward()
                {
                    rewardType = GetRewardType(),
                    experience = GetRewardExp(),
                    coins = GetRewardCoins()
                };
            }
        }

        public override int iterationsGoal      { get { return objectivesCount; } }
        public override int iterationsDone      { get { return objectivesCompleted; } }

        public int objectivesCount      { get { return _objectives.Count; } }
        public int objectivesCompleted  { get { return GetObjectivesCompleted(); } }
        public List<QuestObjectiveLogicT> objectives { get { return _objectives; } }

        public float coinsBonus = 0;

        public BaseQuestLogicWithObjectives(QuestDataT data)
            : base (data)
        {
        }

        public void CompleteAllObjectives()
        {
            foreach( var obj in objectives)
                obj.CompleteAllSteps();
        }

        protected void AddObjective(QuestObjectiveLogicT objective)
        {
            objective.OnObjectiveCompletedCallback = OnObjectiveCompleted;
            _objectives.Add(objective);
        }

        private int GetRewardCoins()
        {
            int reward = 0;
            foreach (var obj in _objectives)
                reward += obj.reward.coins;
            
            return reward + (int)((float)reward * coinsBonus);
        }

        private int GetRewardExp()
        {
            int reward = 0;
            foreach (var obj in _objectives)
                reward += obj.reward.experience;

            return reward;
        }

        protected QuestRewardType GetRewardType()
        {
            return _objectives.Any(o => o.reward.rewardType == QuestRewardType.BecameSportsmen)
                ? QuestRewardType.BecameSportsmen
                : QuestRewardType.CoinsAndExperience;
        }

        public QuestObjectiveLogicT currentObjective
        { 
            get
            { 
                foreach (var obj in _objectives)
                {
                    if (!obj.completed)
                        return obj;
                }

                return null;
            } 
        }

        public QuestObjectiveLogicT lastCompletedObjective
        {
            get { return _lastCompletedObjective ?? (completed ? objectives[0] : null); }
        }

        public void OnObjectiveCompleted(BaseQuestObjectiveLogic<QuestObjectiveDataT> quest)
        {
            base.CompleteStep();
            _lastCompletedObjective = (QuestObjectiveLogicT)quest;
        }

        int GetObjectivesCompleted()
        {
            int completed = 0;
            foreach (var obj in objectives)
            {
                if (obj.completed)
                    completed++;
            }

            return completed;
        }
    }
}
