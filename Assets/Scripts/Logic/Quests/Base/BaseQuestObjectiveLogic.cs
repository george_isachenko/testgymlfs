﻿using UnityEngine;
using Data.Quests;
using Newtonsoft.Json;
using Logic.Serialization.Converters;
using Logic.Serialization;

namespace Logic.Quests.Base
{
    public abstract class BaseQuestObjectiveLogic<QuestObjectiveDataT>
        where QuestObjectiveDataT : BaseQuestObjectiveData
    {
        public delegate void OnObjectiveCompleted(BaseQuestObjectiveLogic<QuestObjectiveDataT> quest);

        protected QuestObjectiveDataT data;

        public OnObjectiveCompleted OnObjectiveCompletedCallback;

        public virtual bool     completed           { get { return data.iterationsDone >= data.iterationsGoal; } }
        public virtual QuestReward reward           { get { return new QuestReward(); } }
        public virtual int      iterationsGoal      { get { return data.iterationsGoal; } }
        public virtual int      iterationsDone      { get { return data.iterationsDone; } }
        public virtual int      iterationsLeft      { get { return data.iterationsGoal - data.iterationsDone; } }
        public virtual string   progress            { get { return iterationsDone.ToString() + "/"  + iterationsGoal.ToString(); } }

        public BaseQuestObjectiveLogic(QuestObjectiveDataT data)
        {
            this.data = data;
        }

        public void CompleteStep()
        {
            data.iterationsDone++;
            if (data.iterationsDone > iterationsGoal)
                data.iterationsDone = iterationsGoal;

            if (completed)
                Complete();
        }

        public void CompleteAllSteps()
        {
            CompleteSteps(iterationsGoal);
        }

        public void CompleteSteps(int steps)
        {
            data.iterationsDone += steps;

            if (data.iterationsDone > iterationsGoal)
                data.iterationsDone = iterationsGoal;

            if (completed)
                Complete();
        }

        public QuestObjectiveDataT GetData()
        {
            return data;
        }

        private void Complete()
        {
            if (completed)
            {
                if (OnObjectiveCompletedCallback != null)
                    OnObjectiveCompletedCallback(this);
                else
                    Debug.LogError("OnObjectiveCompletedCallback is NULL");
            }
            else
                Debug.LogError("Trying to complete uncompleted objective");
        }

    }

    public struct QuestReward
    {
        public QuestRewardType rewardType;
        public int coins;
        public int experience;

        public QuestReward (QuestRewardType rewardType, int coins, int experience)
        {
            this.rewardType = rewardType;
            this.coins = coins;
            this.experience = experience;
        }

        public static QuestReward operator + (QuestReward qr1, QuestReward qr2)
        {
            return new QuestReward
                ( (qr1.rewardType == QuestRewardType.BecameSportsmen || qr2.rewardType == QuestRewardType.BecameSportsmen) ? QuestRewardType.BecameSportsmen : QuestRewardType.CoinsAndExperience
                , qr1.coins + qr2.coins
                , qr1.experience + qr2.experience );
        }
    }

    public struct QuestEstimatedRewards
    {
        public QuestReward baseReward;
        public QuestReward bonusRewardMin;
        public QuestReward bonusRewardMax;

        public QuestEstimatedRewards (QuestReward baseReward, QuestReward bonusRewardMin, QuestReward bonusRewardMax)
        {
            this.baseReward = baseReward;
            this.bonusRewardMin = bonusRewardMin;
            this.bonusRewardMax = bonusRewardMax;
        }

        public QuestEstimatedRewards (QuestReward baseReward)
        {
            this.baseReward = baseReward;
            this.bonusRewardMin = new QuestReward(baseReward.rewardType, 0, 0);
            this.bonusRewardMax = new QuestReward(baseReward.rewardType, 0, 0);
        }

        public static QuestEstimatedRewards operator + (QuestEstimatedRewards qr1, QuestEstimatedRewards qr2)
        {
            return new QuestEstimatedRewards
                ( qr1.baseReward + qr2.baseReward
                , qr1.bonusRewardMin + qr2.bonusRewardMin
                , qr1.bonusRewardMax + qr2.bonusRewardMax );
        }
    }

    [JsonConverter(typeof(EnumAliasConverter))]
    public enum QuestRewardType
    {
        // Temporary developer version SaveGame compatibility, remove later.
        [EnumAlias("coinsAndExpirience")]
        CoinsAndExperience = 0,
        [EnumAlias("becameSportsmen")]
        BecameSportsmen = 1,
    }
}
