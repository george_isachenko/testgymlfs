﻿using Data;
using Data.Person;
using Data.Quests;
using Logic.Core;
using Logic.Estate;
using Logic.Persons;
using Logic.Persons.Events;
using UnityEngine;

namespace Logic.Statistics
{
    public delegate void OnStatisticsUpdated_QuestCompleted(QuestType questType);
    public delegate void OnStatisticsUpdated_StatsUp(ExersiseType type, int value);
    public delegate void OnStatisticsUpdated_EndExercise(Equipment equipment, Exercise exercise);

    public class StatisticsLogic
    {
        public event OnStatisticsUpdated_QuestCompleted     OnStatisticsUpdated_QuestCompleted;
        public event OnStatisticsUpdated_StatsUp            OnStatisticsUpdated_StatsUp;
        public event OnStatisticsUpdated_EndExercise        OnStatisticsUpdated_EndExercise;

        //private IGameCore gameCore;

        public StatisticsLogic(IGameCore gameCore)
        {
            //this.gameCore = gameCore;

            CharacterFinishingExerciseEvent.Event += OnCharacterFinishingExerciseEvent;
        }

        public void Destroy()
        {
            CharacterFinishingExerciseEvent.Event -= OnCharacterFinishingExerciseEvent;
        }

        StatisticsData data = new StatisticsData();

        public void OnQuestCompleted(QuestType questType)
        {
            data.quests.Add(questType);
            OnStatisticsUpdated_QuestCompleted(questType);
        }

        public void OnStatsUp(ExersiseType type, int value)
        {
            data.stats.Add(type, value);
            OnStatisticsUpdated_StatsUp(type, value);
        }

        private void OnCharacterFinishingExerciseEvent
            (Person person, Equipment equipment, Exercise exercise, ExerciseInitiator initiator, ExerciseFlags flags)
        {
            if (person is PersonVisitor && 
                    (initiator == ExerciseInitiator.Player ||
                     initiator == ExerciseInitiator.Auto ||
                     initiator == ExerciseInitiator.DebugAutoPlay ||
                     (initiator == ExerciseInitiator.Trainer && ((flags & ExerciseFlags.EndsInOffline) == 0) )))
            {
                if (equipment == null || exercise == null)
                {
                    Debug.LogErrorFormat("Statistics:OnPersonEndExercise : {0} {1}", equipment == null ? "equipment is NULL" : "", exercise == null ? "exercise is NULL" : "");
                    return;
                }
                data.exercises.Add(exercise.id, equipment.estateType.id, exercise.tier);
                OnStatisticsUpdated_EndExercise(equipment, exercise);

                if (exercise.stats.arms > 0)
                    OnStatsUp(ExersiseType.Arms, exercise.stats.arms);
                if (exercise.stats.legs > 0)
                    OnStatsUp(ExersiseType.Legs, exercise.stats.legs);
                if (exercise.stats.cardio > 0)
                    OnStatsUp(ExersiseType.Cardio, exercise.stats.cardio);
                if (exercise.stats.torso > 0)
                    OnStatsUp(ExersiseType.Torso, exercise.stats.torso);
            }
        }
    }
}