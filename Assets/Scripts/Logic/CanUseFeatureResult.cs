﻿namespace Logic
{
    public struct CanUseFeatureResult
    {
        public enum ResultType
        {
            DependencyLocked,
            LockedNeededLevel,
            Locked,
            Pending,
            CanUse
        }

        public ResultType type;
        public int level;

        public CanUseFeatureResult(ResultType type, int level)
        {
            this.type = type;
            this.level = level;
        }

        public CanUseFeatureResult(ResultType type)
        {
            this.type = type;
            this.level = 0;
        }

        public bool canUse
        {
            get { return type == ResultType.CanUse; }
        }

    }
}
