﻿using System;
using DataProcessing.CSV;

namespace Data
{
    public class PromoPurchases
    {
        [CSVName("id")]
        public int id;
         
        [CSVName("StartLvl")]
        public int startLevel;

        [CSVName("Header")]
        public string header;

        [CSVName("Description")]
        public string description;

        [CSVName("Discount")]
        public int discount;

        //[CSVName("FitBucks")]
        //public int bucks;

        //[CSVName("Coins")]
        //public int coins;

        [CSVName("Duration")]
        public TimeSpan duration;

        [CSVName("OldPriceId")]
        public string oldPriceId;

        [CSVName("NewPriceID")]
        public string newPriceId;
    }
}


