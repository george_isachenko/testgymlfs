﻿using Data.Estate;
using System;

namespace Data
{
    public enum ShopItemCategory
    {
        EQUIPMENTS,
        DECORS,
        PAINTS,
        BANK,
        ADVERTISEMENT,
        SPORT_EQUIPMENTS,

        UNKNOWN
    }

    public class ShopItemData
    {
        public int idx; //--- use for Visual
        public int id;
        public string name;
        public string mesh_prefab;
        public Cost price = new Cost();

        public EstateType estateTypeData;
		public BankItemData? bankItemData;

        public ShopItemCategory category;

        public string GetCategoryString()
        {
            switch (category) 
            {
                case ShopItemCategory.EQUIPMENTS: return "Equipment";
                case ShopItemCategory.SPORT_EQUIPMENTS: return "SportEquipment";
                case ShopItemCategory.DECORS: return "Decor";
                case ShopItemCategory.PAINTS: return "Paint";
            }

            return Enum.GetName (typeof(ShopItemCategory), category);
        }
    }
}