﻿using Data.Person;
using DataProcessing.CSV;

namespace Data
{
    public class Costume
    {
        [CSVName("Any View Type")]
        public bool anyViewType;

        [CSVName("View Type")]
        public PersonAppearance.ViewType viewType;

        [CSVName("View Type Index")]
        public int viewTypeIndex;

        [CSVName("Part Type")]
        public PersonAppearance.BodyPart.PartType partType;

        [CSVName("Gender")]
        public PersonAppearance.Gender gender;

        [CSVName("Any Race")]
        public bool anyRace;

        [CSVName("Race")]
        public PersonAppearance.Race race;

        [CSVName("Texture Id")]
        public int textureId;

        [CSVName("Mesh Id")]
        public int meshId;
    }
}
