﻿using Data.PlayerProfile;
using Newtonsoft.Json;

namespace Data.Quests.Juicer
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn, IsReference = true)]
    public class JuiceQuestData : BaseQuestData
    {
        [JsonProperty]
        public int juicerId;

        public JuiceQuestData (int juicerId)
        :    base(QuestType.Juice)
        {
            this.isNew = true;
            this.juicerId = juicerId;
        }
    }
}