﻿using Logic.Quests;
using Logic.Quests.Base;
using Data.PlayerProfile;

namespace Data.Quests.Juicer
{
    public class JuiceQuestObjectiveData : BaseQuestObjectiveData
    {
        public ResourceType juiceId;
        public int          amount;
    }
}
