﻿namespace Data.Quests.DailyQuest
{
    public enum DailyQuestType
    {
        Stats,
        Exercise,
        ExerciseTier,
        ExerciseTotal,
        Equipment,
        VisitorQuest,
        DailyQuest,
        ClaimDailyReward,
        VideoAd
    }
}
