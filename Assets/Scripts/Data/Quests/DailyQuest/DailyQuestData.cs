﻿using Data.Estate;
using Newtonsoft.Json;
using System;
using UnityEngine;
using Data.PlayerProfile;

namespace Data.Quests.DailyQuest
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn, IsReference = true)]
    public class DailyQuestData : BaseQuestData
    {
        [JsonProperty] public DailyQuestType dailyQuestType;
        [JsonProperty] public int iterationsGoal;
        [JsonProperty] public int relatedId;
        [JsonProperty] public Exercise exercise;
        [JsonProperty] public EstateType equipment;
        [JsonProperty] public float complexity;
        [JsonProperty] public int rewardCoins = -1;
        [JsonProperty] public int rewardExp = -1;
        [JsonProperty] public ResourceType rewardResource = ResourceType.None;

        public string title
        {
            get
            {
                switch (dailyQuestType)
                {
                    case DailyQuestType.Stats:
                        return Loc.Get("dailyQuestTypeStatsQuestTitle");

                    case DailyQuestType.Exercise:
                        return Loc.Get("dailyQuestTypeExerciseQuestTitle");

                    case DailyQuestType.ExerciseTier:
                        return Loc.Get("dailyQuestTypeExerciseTierQuestTitle");

                    case DailyQuestType.ExerciseTotal:
                        return Loc.Get("dailyQuestTypeExerciseTotalQuestTitle");

                    case DailyQuestType.Equipment:
                        return Loc.Get("dailyQuestTypeEquipmentQuestTitle");

                    case DailyQuestType.VisitorQuest:
                        return Loc.Get("dailyQuestTypeVisitorQuestQuestTitle");

                    case DailyQuestType.DailyQuest:
                        return "dailyQuestTypeDailyQuestQuestTitle";

                    case DailyQuestType.ClaimDailyReward:
                        return Loc.Get("dailyQuestTypeClaimDailyRewardQuestTitle");

                    case DailyQuestType.VideoAd:
                        return Loc.Get("dailyQuestTypeVideoAdQuestTitle");

                    default:
                        return Loc.Get("dailyQuestTypedefaultQuestTitle");
                }
            }
        }

        public string subTitle
        {
            get
            {
                switch (dailyQuestType)
                {
                    case DailyQuestType.Stats:
                        return Loc.Get("dailyQuestTypeStatsQuestSubTitle", (Data.ExersiseType) relatedId);

                    case DailyQuestType.Exercise:
                        return Loc.Get("dailyQuestTypeExerciseQuestSubTitle", Loc.Get(exercise.locNameId));

                    case DailyQuestType.ExerciseTier:
                        var tier = "";
                        var orderOfOftier = "";
                        switch (relatedId)
                        {
                            case 1:
                                tier = Loc.Get("dailyQuestFirstTierName");
                                orderOfOftier = Loc.Get("dailyQuestFirstOrderOfOftier");
                                break;
                            case 2:
                                tier = Loc.Get("dailyQuestSecondTierName");
                                orderOfOftier = Loc.Get("dailyQuestSecondOrderOfOftier");
                                break;
                            case 3:
                                tier = Loc.Get("dailyQuestThirdTierName");
                                orderOfOftier = Loc.Get("dailyQuestThirdOrderOfOftier");
                                break;
                            default:
                                tier = Loc.Get("dailyQuestDefaultTierName");
                                orderOfOftier = Loc.Get("dailyQuestDefaultOrderOfOftier");
                                Debug.LogError("Wrong tier type");
                                break;
                        }

                        return Loc.Get("dailyQuestTypeExerciseTierQuestSubTitle", tier, orderOfOftier);

                    case DailyQuestType.ExerciseTotal:
                        return Loc.Get("dailyQuestTypeExerciseTotalQuestSubTitle");

                    case DailyQuestType.Equipment:
                        return Loc.Get("dailyQuestTypeEquipmentQuestSubTitle", Loc.Get(equipment.locNameId));

                    case DailyQuestType.VisitorQuest:
                        return Loc.Get("dailyQuestTypeVisitorQuestQuestSubTitle");

                    case DailyQuestType.DailyQuest:
                        return Loc.Get("dailyQuestTypeDailyQuestQuestSubTitle");

                    case DailyQuestType.ClaimDailyReward:
                        return Loc.Get("dailyQuestTypeClaimDailyRewardQuestSubTitle");

                    case DailyQuestType.VideoAd:
                        return Loc.Get("dailyQuestTypeVideoAdQuestSubTitle");
                }

                return Loc.Get("dailyQuestDefaultQuestSubTitle");
            }
        }

        public DailyQuestData ()
            : base (QuestType.Daily)
        {
        }
    }
}
