﻿using Newtonsoft.Json;

namespace Data.Quests
{
    [JsonObject(MemberSerialization.OptOut)]
    public abstract class BaseQuestObjectiveData
    {
        public int          iterationsDone;
        public int          iterationsGoal;
    }
}