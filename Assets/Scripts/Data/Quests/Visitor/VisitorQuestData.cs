﻿using Newtonsoft.Json;

namespace Data.Quests.Visitor
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn, IsReference = true)]
    public class VisitorQuestData : BaseQuestData
    {
        public VisitorQuestData (int visitorId)
            : base (QuestType.Visitor)
        {
            this.visitorId = visitorId;
        }
            
        [JsonProperty]
        public int  visitorId;
    }
}