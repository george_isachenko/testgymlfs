﻿using Logic.Quests;
using Logic.Quests.Base;

namespace Data.Quests.Visitor
{
    public enum VisitorQuestType
    {
        Exercise,
        Stats,
        None
    }

    public class VisitorQuestObjectiveData : BaseQuestObjectiveData
    {
        public VisitorQuestType type            = VisitorQuestType.None;
        public ExersiseType     exerciseType;
        public Exercise         exercise;
        public QuestRewardType  rewardType;
        public int              equipmentUpgradeLevel;
        public int              accumulatedExperienceReward = 0;
        public int              accumulatedCoinsReward = 0;
    }
}
