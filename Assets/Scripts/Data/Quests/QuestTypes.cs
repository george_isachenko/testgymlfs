﻿namespace Data.Quests
{
    public enum QuestType
    {
        Visitor,
        Daily,
        Sport,
        Juice
    }
}