﻿using Core.Timer;
using Newtonsoft.Json;

namespace Data.Quests
{
    public enum QuestStage
    {
        New = 0,
        InProgress,
        Completed
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn, IsReference = true)]
    public abstract class BaseQuestData
    {
        [JsonProperty]
        public bool         claimed = false;

        [JsonProperty]
        public QuestType    type;

        [JsonProperty]
        public int          iterationsDone;

        [JsonProperty]
        public TimerFloat   timer;

        [JsonProperty]
        public bool         isNew   = true;

        public string GetTime()
        {
            if (timer != null)
            {
                return View.PrintHelper.GetTimeString(timer.timeSpan);
            }
            else
                return string.Empty;
        }

        public BaseQuestData (QuestType type)
        {
            this.type = type;
        }
    }
}
