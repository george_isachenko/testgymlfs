using System;
using Data;
using Data.PlayerProfile;

namespace Data.Quests.SportQuest
{
    public struct SportQuestReward : IComparable<SportQuestReward>
    {
        public SportQuestRewardType rewardType;
        public ResourceType itemId;
        public int count;

        public int CompareTo(SportQuestReward other)
        {
            return rewardType - other.rewardType;
        }
    }
}