using System;
using Data.Person;
using Data.Quests;
using Newtonsoft.Json;

namespace Data.Quests.SportQuest
{
    public class SportObjectiveData : BaseQuestObjectiveData, IComparable<SportObjectiveData>
    {
        [JsonProperty]
        public SportsmanType sportsmanType;

        public int CompareTo(SportObjectiveData other)
        {
            return iterationsGoal - other.iterationsGoal;
        }
    }
}