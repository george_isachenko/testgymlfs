﻿using Core.Timer;
using Newtonsoft.Json;

namespace Data.Quests.SportQuest
{
    public class SportTimeQuestData : SportQuestData
    {
        [JsonProperty]
        public TimerFloat newQuestTimer;
    }
}