﻿using Newtonsoft.Json;

namespace Data.Quests.SportQuest
{
    public class SportLaneQuestData : SportQuestData
    {
        [JsonProperty]
        public bool isQuestClaimed;
    }
}
