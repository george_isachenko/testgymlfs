using Newtonsoft.Json;

namespace Data.Quests.SportQuest
{
    public abstract class SportQuestData : BaseQuestData
    {
        // [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        [JsonProperty]
        public string questTypeId = string.Empty;

        // NOTE: Deprecated property! Do not remove comment, do not reuse its name!
//      [JsonProperty]
//      public string questName = "Unnamed";

        [JsonProperty]
        public SportQuestReward[] rewards = new SportQuestReward[0];

        public SportQuestData()
            : base (QuestType.Sport)
        {
            questTypeId = string.Empty;
        }
    }
}