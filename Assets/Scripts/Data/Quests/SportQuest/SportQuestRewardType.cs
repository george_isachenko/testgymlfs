using Logic.Serialization;
using Logic.Serialization.Converters;
using Newtonsoft.Json;

namespace Data.Quests.SportQuest
{
    [JsonConverter(typeof(EnumAliasConverter))]   
    public enum SportQuestRewardType
    {
        Coin,
        [EnumAlias("LeaderboardScore")]
        FitPoints,
        Resource,
        [EnumAlias("Expirience")]
        Experience,
        Fitbucks
    }
}