﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FileSystem;
using DataProcessing.CSV;
using UnityEngine;
using UnityEngine.Assertions;
using View;

namespace Data
{
    public class Loc
    {
        #region Types.
        public class LocalizationLanguageItem
        {
            public string localizedName;
            public string iconId;
            public string languageId;
            public string localizationId;
        }
        #endregion

        #region Public properties.
        public static Loc instance
        {
            get { return _instance ?? (_instance = new Loc()); }
        }

        public LocalizationLanguageItem currentLanguageInfo { get; private set; }
        public LocalizationLanguageItem[] allSupportedLanguages { get; private set; }
        #endregion

        #region Private static data.
        private static readonly string fallbackLanguage = "English";
        private static readonly string PlayerPrefsLocalizationLanguage = "LocalizationLanguage";
        private static readonly string NotFoundValueMask = "[{0}]";
        private static readonly int textsDictReserve = 1024;

        private static Loc _instance;
        #endregion

        #region Private data.
        private readonly Dictionary<string, string> texts = new Dictionary<string, string>(textsDictReserve);
        #endregion

        #region Public API.
        public Loc()
        {
            lock(this)
            {
                var rows = Parser.ParseResource(ResourcePaths.BalanceTables.localizationText);

                var currrentLanguage = GetCurrentLanguage();

                foreach (var row in rows)
                {
                    int idKeyIdx = -1;
                    int fallbackLanguageIdx = 1;
                    int currentLanguageIdx = 1;

                    if (idKeyIdx < 0)
                    {
                        var keys = row.Keys.ToArray();
                        idKeyIdx = Array.IndexOf(keys, "id");
                        if (idKeyIdx < 0)
                        {
                            Debug.LogError("Cannot find 'id' field in localization table. Load of localization data canceled!");
                            break;
                        }

                        if (fallbackLanguage != string.Empty)
                        {
                            fallbackLanguageIdx = Array.IndexOf(keys, fallbackLanguage);
                            if (fallbackLanguageIdx < 0)
                            {
                                Debug.LogErrorFormat("Cannot find '{0}' field for fallback language in localization table. Load of localization data canceled!", fallbackLanguage);
                                break;
                            }
                        }

                        currentLanguageIdx = Array.IndexOf(keys, currrentLanguage);
                        if (currentLanguageIdx < 0)
                        {
                            Debug.LogWarningFormat("Cannot find '{0}' field for current language in localization table. Fallback language will be used!", currentLanguageIdx);
                        }
                    }

                    var id = row[idKeyIdx];

                    if (id == null || id == string.Empty)
                    {
                        Debug.LogWarning("Row with empty id is found in localization table. Ignored.");
                    }
                    else
                    {
#if DEBUG
                        if (texts.ContainsKey(id))
                            Debug.LogWarningFormat("Localization already exists text for key {0}. Will be overriden with a new value!", id);
#endif
                        var value = (currentLanguageIdx >= 0) ? row[currentLanguageIdx] : null;

                        if (value == null || value == string.Empty)
                        {
                            value = row[fallbackLanguage];
                            if (value == null)
                                value = string.Empty;
                        }

                        texts[id] = value;
                    }
                }
            }
        }

        public string this[string id]
        {
            get
            {
                lock(this)
                {
                    string result;
                    if (texts.TryGetValue(id, out result))
                    {
                        return result;
                    }
                    else
                    {
                        Debug.LogWarningFormat("Cannot find localization for key: '{0}'", id);
                    }
                }

                return string.Format(NotFoundValueMask, id);
            }
        }

        public void SetLanguage(string localizationId)
        {
            Assert.IsNotNull(localizationId);
            Assert.IsTrue(localizationId != string.Empty);
            PlayerPrefs.SetString(PlayerPrefsLocalizationLanguage, localizationId);

            // Reload localization.
            _instance = null;
            _instance = new Loc();
        }
        #endregion

        #region Public static API.
        public static string Get(string id)
        {
            return instance[id];
        }

        public static bool Exists(string id)
        {
            lock(instance)
            {
                return instance.texts.ContainsKey(id);
            }
        }

        public static int CountWithPrefix(string idPrefix)
        {
            lock(instance)
            {
                return instance.texts.Keys.Count(id => id.StartsWith(idPrefix));
            }
        }

        public static string GetWithPrefix(string idPrefix, int idx)
        {
            lock(instance)
            {
                var result = instance.texts.Where(pair => pair.Key.StartsWith(idPrefix)).Select(pair => pair.Value).OrderBy(value => value).ElementAtOrDefault(idx);
                return (result != null && result != string.Empty) ? result : string.Format(NotFoundValueMask, result);
            }
        }

        public static string Get(string id, params object[] args)
        {
            try
            {
                return string.Format(instance[id], args);
            }
            catch (FormatException)
            {
                return string.Format("[FormatError:{0}]", id);
            }
            catch (Exception)
            {
                return string.Format(NotFoundValueMask, id);
            }
        }

        public static string Get(TimeSpan timeSpan)
        {
            return PrintHelper.GetTimeString(timeSpan);
        }
        #endregion

        #region Private methods.
        private string GetCurrentLanguage()
        {
            if (allSupportedLanguages == null)
            {
                allSupportedLanguages = Parser.ParseResourceToObjects<LocalizationLanguageItem>(ResourcePaths.BalanceTables.localizationLanguages).ToArray();
            }

            if (PlayerPrefs.HasKey(PlayerPrefsLocalizationLanguage))
            {
                var restoredLang = PlayerPrefs.GetString(PlayerPrefsLocalizationLanguage);
                if (allSupportedLanguages.Any(i => i.languageId == restoredLang))
                {
                    Debug.LogFormat("Localization language restored from PlayerPrefs, language is {0}", restoredLang);
                    currentLanguageInfo = allSupportedLanguages.FirstOrDefault(i => i.localizationId == restoredLang);
                    return restoredLang;
                }
            }

            var lang = fallbackLanguage;
            var myLangItem = allSupportedLanguages.FirstOrDefault(l => l.languageId == Application.systemLanguage.ToString());
            if (myLangItem == null)
            {
                Debug.LogWarningFormat("Can't find localization language for system language '{0}', set default '{1}'.",
                    Application.systemLanguage, fallbackLanguage);
            }
            else
            {
                lang = myLangItem.localizationId;
            }
            Debug.LogFormat("Can't find stored localization language data, selected new '{0}' language", lang);
            PlayerPrefs.SetString(PlayerPrefsLocalizationLanguage, lang);
            currentLanguageInfo = allSupportedLanguages.FirstOrDefault(i => i.localizationId == lang) ??
                                  allSupportedLanguages.FirstOrDefault(i => i.localizationId == fallbackLanguage);
            return lang;
        }
        #endregion
    }
}
