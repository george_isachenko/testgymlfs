﻿using System;
using Data.Estate;
using DataProcessing.CSV;
using Logic.Serialization.Converters;
using Newtonsoft.Json;

namespace Data
{
    public enum ExersiseType
    {
        Cardio,
        Torso,
        Arms,
        Legs,

        Unknown
    }

    [JsonConverter(typeof(ExerciseJsonConverter))]
    public class Exercise
    {
        public enum InterruptType
        {
            Uninterruptable = 0,
            ForBucks,
            ForFree,
            ForBucksNoFree
        };

        public struct Profits
        {
            public int coins;
            public int exp;
            public int fitpoints;
        }

        public EstateType equipment;

        public int id;
        public string locNameId;

        [CSVName("icon_id")]
        public int icon;

        public int tier;

        [CSVName("character anim")]
        public int characterAnimation;

        [CSVName("equipment anim")]
        public int equipmentSubAnimation;

        [CSVName("workstation lvl")]
        public int workstationLevel;

        [CSVName("duration")]
        public TimeSpan duration;

        [CSVName("energy cost")]
        public int energyCost;

        [CSVName("")]
        public Profits profits;

        [CSVName("")]
        public Stats stats;

        public InterruptType interruptType;

        public Exercise()
        {
            profits = new Profits();
            stats = new Stats();
        }

        public Exercise(Exercise source)
        {
            this.equipment = source.equipment;
            this.id = source.id;
            this.locNameId = source.locNameId;
            this.icon = source.icon;
            this.tier = source.tier;
            this.characterAnimation = source.characterAnimation;
            this.equipmentSubAnimation = source.equipmentSubAnimation;
            this.workstationLevel = source.workstationLevel;
            this.duration = source.duration;
            this.energyCost = source.energyCost;
            this.profits = source.profits;
            this.stats = source.stats;
            this.interruptType = source.interruptType;
        }

        [CSVName("profit coins")]
        private void ParseProfitsCoins(int coins)
        {
            profits.coins = coins;
        }

        [CSVName("profit exp")]
        private void ParseProfitsExp(int exp)
        {
            profits.exp = exp;
        }

        [CSVName("FitPoints")]
        private void ParseProfitsFitPoints(int fitpoints)
        {
            profits.fitpoints = fitpoints;
        }

        [CSVName("Torso exp")]
        private void ParseStatsTorso(int value)
        {
            stats.torso = value;
        }

        [CSVName("Cardio exp")]
        private void ParseStatsCardio(int value)
        {
            stats.cardio = value;
        }

        [CSVName("Arms exp")]
        private void ParseStatsArms(int value)
        {
            stats.arms = value;
        }

        [CSVName("Legs exp")]
        private void ParseStatsLegs(int value)
        {
            stats.legs = value;
        }

        [CSVName("Interrupt option")]
        private void ParseInterruptType(string value)
        {
            if (value == "uninterruptable")
                interruptType = InterruptType.Uninterruptable;
            else if (value == "for_free")
                interruptType = InterruptType.ForFree;
            else if (value == "for_bucks")
                interruptType = InterruptType.ForBucks;
            else if (value == "for_bucks_no_free")
                interruptType = InterruptType.ForBucksNoFree;
            else
                throw new ArgumentException(string.Format("Invalid value '{0}'.", value), "Interrupt option");
        }

        public bool IsSimple()
        {
            return tier == 1;
        }

        public bool IsEquipSimple()
        {
            return equipment.IsSimple();
        }
    }
}