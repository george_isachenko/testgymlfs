﻿namespace Data
{
    public struct Stats
    {
        public int cardio;
        public int torso;
        public int arms;
        public int legs;

        public int cardioNormalized { get { return NormalizedValue(cardio); } }
        public int torsoNormalized  { get { return NormalizedValue(torso);  } }
        public int armsNormalized   { get { return NormalizedValue(arms);   } }
        public int legsNormalized   { get { return NormalizedValue(legs);   } }

        public Stats(int cardio, int torso, int arms, int legs)
        {
            this.cardio = cardio;
            this.torso = torso;
            this.arms = arms;
            this.legs = legs;
        }

        public struct StatValue
        {
            public int statIndex;
            public int statValue;
        }

        public static Stats operator + (Stats s1, Stats s2)
        {
            return new Stats(s1.cardio + s2.cardio, s1.torso + s2.torso, s1.arms + s2.arms, s1.legs + s2.legs);
        }

        public int Get (ExersiseType exerciseType)
        {
            switch (exerciseType)
            {
                case ExersiseType.Arms:     return arms;
                case ExersiseType.Cardio:   return cardio;
                case ExersiseType.Legs:     return legs;
                case ExersiseType.Torso:    return torso;
            }

            return 0;
        }

        public StatValue GetMaxStat()
        {
            StatValue result;

            int[] stats = new int[4]{cardio, torso, arms, legs};
            int maxIndex = 0;
            for (int i = 1; i < stats.Length; ++i)
            {
                if (stats[i] > stats[maxIndex])
                    maxIndex = i;
            }

            result.statIndex = maxIndex;
            result.statValue = stats[maxIndex];

            return result;
        }

        public StatValue[] GetSorted()
        {
            StatValue[] result = new StatValue[4];

            int[] stats = new int[4]{cardio, torso, arms, legs};

            for (int i = 0; i < stats.Length; ++i)
            {
                result [i].statIndex = i;
                result [i].statValue = stats [i];
            }

            for (int i = 0; i < stats.Length; ++i)
                for (int j = 0; j < stats.Length; ++j) 
                {
                    if (i == j)
                        continue;

                    if (result [j].statValue < result [i].statValue) 
                    {
                        StatValue tStat = result [i];
                        result [i] = result [j];
                        result [j] = tStat;
                    }
                }

            return result;
        }

        private int NormalizedValue(int val)
        {
            if (val == 0)
                return 0;
            if (val <= 10)
                return 1;
            if (val <= 24)
                return 2;
            if (val <= 40)
                return 3;

            return 4;
        }
    }
}
