﻿using DataProcessing.CSV;

namespace Data
{
    public class TrainerName
    {
        public int id;
        
        [CSVName("Name")]
        public string name;
    }
}