﻿using Core.IAP;
using DataProcessing.CSV;

namespace Data
{
    public struct BankItemData
    {
        public int coins;
        public int bucks;
        [CSVName("iap_id")]
        public string iapProductId;
        [CSVName("show_in_shop")]
        public bool showInShop;
        public string badgeType;
        public int percent;

        public ProductInfo productInfo { get; set; }
    }
}