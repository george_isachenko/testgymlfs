﻿using Data.Room;

namespace Data.Estate.Static
{
    public struct StaticEquipmentInfo
    {
        public int estateType;
        public EquipmentPlacement placement;
        public int showAtLevel;
        public bool initiallyLocked;
        public bool isEditable;

        public StaticEquipmentInfo (int estateType, EquipmentPlacement placement, int showAtLevel, bool initiallyLocked, bool isEditable)
        {
            this.estateType = estateType;
            this.placement = placement;
            this.showAtLevel = showAtLevel;
            this.initiallyLocked = initiallyLocked;
            this.isEditable = isEditable;
        }
    }
}