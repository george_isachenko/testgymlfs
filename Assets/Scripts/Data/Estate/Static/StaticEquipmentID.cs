﻿namespace Data.Estate.Static
{
    public struct StaticEquipmentID
    {
        public string roomName;
        public string name;

        public StaticEquipmentID (string roomName, string name)
        {
            this.roomName = roomName;
            this.name = name;
        }

        public override bool Equals(object obj) 
        {
            if (!(obj is StaticEquipmentID))
                return false;

            StaticEquipmentID ep = (StaticEquipmentID) obj;
            return (ep == this);
        }

        public override int GetHashCode()
        {
            return roomName.GetHashCode() ^ name.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}: '{1}'", roomName, name);
        }
        
        public static bool operator == (StaticEquipmentID p1, StaticEquipmentID p2)
        {
            return (p1.roomName == p2.roomName && p1.name == p2.name);
        }

        public static bool operator != (StaticEquipmentID p1, StaticEquipmentID p2)
        {
            return (p1.roomName != p2.roomName || p1.name != p2.name);
        }
    }
}