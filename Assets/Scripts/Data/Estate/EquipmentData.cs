﻿using System;
using Data.Estate.Static;
using Data.Person;
using Data.Room;
using Newtonsoft.Json;

namespace Data.Estate
{
    public enum EquipmentState
    {
        Idle = 0,
        ShopMode,
        WorkPending,
        Working,
        Waiting,            // Not used now.
        Repairing,
        Assembling,
        AssemblyComplete,
        Broken,
        Locked
    }

    [JsonObject(MemberSerialization.OptIn)]
    //
    // IMPORTANT! Previously used but deprecated and removed properties names: "selectable".
    // Do not attempt to reuse such names until you know what you're doing!
    //
    public class EquipmentData
    {
        public static readonly int InvalidId = -1;
        public static readonly int equipmentDefaultInitialHP = 100;
        public static readonly int equipmentDefaultMinHP = 80;
        public static readonly int equipmentDefaultMaxHP = 120;

        [JsonProperty]
        public int id;

        [JsonProperty]
        public EstateType estateType; // Converted to id when saving via EstateTypeJsonConverter. 

        [JsonProperty]
        public EquipmentPlacement placement;

        [JsonProperty]
        public EquipmentState state;

        [JsonProperty]
        public string roomName;

        [JsonProperty]
        public string staticEquipmentName;

        [JsonProperty]
        public bool editable;

        [JsonProperty]
        public int occupantCharacterId;

        [JsonProperty]
        public float assemblyTotalTime; // Also used for repairs.

        [JsonProperty]
        public DateTime assemblyEndTime; // Also used for repairs.

        [JsonProperty]
        public int health;

        [JsonProperty]
        public int upgradeLevel;

        public bool isOccupied
        {
            get
            {
                return occupantCharacterId != PersonData.InvalidId;
            }
        }

        public bool isUsable
        {
            get
            {
                return (state == EquipmentState.Idle && estateType.isUsable);
            }
        }

        public bool isPotentiallyAvailableForUse
        {
            get
            {
                return (state == EquipmentState.Idle ||
                        state == EquipmentState.WorkPending ||
                        state == EquipmentState.Working ||
                        state == EquipmentState.Waiting) && estateType.isUsable;
            }
        }

        public bool isSelectable
        {
            get
            {
                return ( state == EquipmentState.Idle ||
                      state == EquipmentState.Working ||
                      state == EquipmentState.Assembling ||
                      state == EquipmentState.AssemblyComplete ||
                      state == EquipmentState.Broken ||
                      state == EquipmentState.Repairing ||
                      state == EquipmentState.Locked);
            }
        }

        public bool isEditable
        {
            get
            {
                return editable && (state == EquipmentState.Idle || state == EquipmentState.Working/* || state == State.Assembling || state == State.AssemblyComplete*/);
            }
        }

        public bool affectsStyle
        {
            get
            {
                return (state == EquipmentState.Idle ||
                        state == EquipmentState.WorkPending ||
                        state == EquipmentState.Working ||
                        state == EquipmentState.Broken ||
                        state == EquipmentState.Repairing);
            }
        }

        public bool isInDelivery
        {
            get
            {
                return (state == EquipmentState.Assembling || state == EquipmentState.AssemblyComplete);
            }
        }

        public bool canSellOrStore
        {
            get // Cannot sell or store non-editable equip, occupied equip or static equip.
            {
                return editable && (state == EquipmentState.Idle) && !isStatic;
            }
        }


        public bool isStatic
        {
            get
            {
                return (staticEquipmentName != null && staticEquipmentName != string.Empty);
            }
        }

        public bool isBroken
        {
            get
            {
                return (state == EquipmentState.Broken);
            }
        }

        public bool isRepairing
        {
            get
            {
                return (state == EquipmentState.Repairing);
            }
        }

        public bool isLocked
        {
            get
            {
                return (state == EquipmentState.Locked);
            }
        }

        public StaticEquipmentID staticEquipmentId
        {
            get
            {
                return new StaticEquipmentID (roomName, staticEquipmentName != null ? staticEquipmentName : string.Empty);
            }
        }

        public EquipmentData
            (int id, EstateType estateType, string roomName, int initialHP)
        {
            this.id = id;
            this.estateType = estateType;
            this.roomName = roomName;
            health = initialHP;
            state = EquipmentState.Idle;
            staticEquipmentName = null;
            editable = true;
            occupantCharacterId = PersonData.InvalidId;
            assemblyTotalTime = 0;
            assemblyEndTime = DateTime.MinValue;
        }

        public EquipmentData
            ( int id
            , EstateType estateType
            , StaticEquipmentID staticEquipmentId
            , StaticEquipmentInfo staticEquipmentInfo)
        : this (id, estateType, staticEquipmentId.roomName, equipmentDefaultInitialHP)
        {
            staticEquipmentName = staticEquipmentId.name;
            this.placement = staticEquipmentInfo.placement;
            this.editable = staticEquipmentInfo.isEditable;

            if (staticEquipmentInfo.initiallyLocked)
            {
                state = EquipmentState.Locked;
            }
        }

        [JsonConstructor]
        protected EquipmentData ()
        {
            this.id = PersonData.InvalidId;
            this.estateType = null;
            this.roomName = string.Empty;
            health = equipmentDefaultInitialHP;
            state = EquipmentState.Idle;
            staticEquipmentName = null;
            editable = true;
            occupantCharacterId = PersonData.InvalidId;
            assemblyTotalTime = 0;
            assemblyEndTime = DateTime.MinValue;
        }
    }
}