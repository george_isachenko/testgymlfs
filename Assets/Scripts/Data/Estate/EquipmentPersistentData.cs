﻿using System.Collections.Generic;

namespace Data.Estate
{
    public struct EquipmentPersistentData
    {
        public List<EquipmentData> equipmentData;
    }
}