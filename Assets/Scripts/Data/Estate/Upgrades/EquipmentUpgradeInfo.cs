﻿using Data.PlayerProfile;
using DataProcessing.CSV;

namespace Data.Estate.Upgrades
{
    public enum UpgradeAvailabilityState
    {
        FeatureUnavailable,
        FeatureAvailable,
        EquipmentLocked,
        EquipmentAvailable,
        EquipmentFull
    }

    public class EquipmentUpgradeInfo
    {
        public struct Key
        {
            [CSVName("Estate Type Id")]
            public int estateTypeId;

            [CSVName("Upgrade Level")]
            public int upgradeLevel;

            public Key(int estateTypeId, int upgradeLevel)
            {
                this.estateTypeId = estateTypeId;
                this.upgradeLevel = upgradeLevel;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Key))
                    return false;

                Key k = (Key) obj;
                return (k == this);
            }

            public override string ToString()
            {
                return string.Format("{0}:{1}", estateTypeId, upgradeLevel);
            }

            public override int GetHashCode()
            {
                return estateTypeId.GetHashCode() ^ upgradeLevel.GetHashCode();
            }

            public static bool operator == (Key p1, Key p2)
            {
                return (p1.estateTypeId == p2.estateTypeId && p1.upgradeLevel == p2.upgradeLevel);
            }

            public static bool operator != (Key p1, Key p2)
            {
                return (p1.estateTypeId != p2.estateTypeId || p1.upgradeLevel != p2.upgradeLevel);
            }
        }

        #region Public fields.
        [CSVName("Unlock Level")]
        public int unlockLevel;

        [CSVName("Cost Type")]
        public Cost.CostType costType;

        [CSVName("Cost")]
        public int costAmount;

        [CSVName("Resource Type")]
        public ResourceType resourceType;

        [CSVName("Resource Cost")]
        public int resourceCost;
        #endregion

        #region Public properties.
        public Cost    cost
        {
            get
            {
                var cost = new Cost(costType, costAmount, 1);
                if (resourceType != ResourceType.None && resourceCost > 0)
                {
                    cost.AddResource(resourceType, resourceCost);
                }

                return cost;
            }
        }
        #endregion
    }
}
