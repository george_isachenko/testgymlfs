﻿using System;
using Data.Estate.Static;
using Data.Room;

namespace Data.Estate.Info
{
    public interface IEquipmentInfo
    {
        int                 id                  { get; }
        EstateType          estateType          { get; }
        EquipmentPlacement  placement           { get; }
        EquipmentState      state               { get; }
        string              roomName            { get; }
        string              staticEquipmentName { get; }
        StaticEquipmentID   staticEquipmentId   { get; }
        int                 occupantCharacterId { get; }
        float               assemblyTotalTime   { get; } // Also used for repairs.
        DateTime            assemblyEndTime     { get; } // Also used for repairs.
        int                 health              { get; }
        int                 upgradeLevel        { get; }
        bool                isOccupied          { get; }
        bool                isUsable            { get; }
        bool                isSelectable        { get; }
        bool                isEditable          { get; }
        bool                affectsStyle        { get; }
        bool                isInDelivery        { get; }
        bool                canSellOrStore      { get; }
        bool                isStatic            { get; }
        bool                isBroken            { get; }
        bool                isRepairing         { get; }
        bool                isLocked            { get; }
        bool                meetsLevelRequirement { get; }
    }
}