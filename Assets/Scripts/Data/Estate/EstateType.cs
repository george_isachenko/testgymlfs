﻿using System;
using System.Linq;
using DataProcessing.CSV;
using Logic.Serialization.Converters;
using Newtonsoft.Json;

namespace Data.Estate
{
    [JsonConverter(typeof(EstateTypeJsonConverter))]
    public class EstateType
    {
        public enum Subtype
        {
            Training = 0,
            Decor,
            ActiveDecor,
            Chair,
            Door,
            AdvertisingTV,
            Sport
        }

        public int id;
        public string locNameId;
        public Subtype itemType;
        public int resourceId;
        [CSVName("level requirement")]
        public int levelRequirement;
        [CSVName("trainer level requirement")]
        public int trainerLevelRequirement;
        [CSVName("assemble time")]
        public TimeSpan assemblyTime;
        [CSVName("")]
        public int[] exercisesIds;

        // Runtime-populated fields.
        [CSVName("")]
        public Stats stats;
        [CSVName("")]
        public Cost price;
        public int style;
        public Exercise[] exercises;
        public int[] countForGymSize;

        public EstateType()
        {
            exercisesIds = new int[] { -1, -1, -1 };
            itemType = Subtype.Training;
            //stats = new Stats();
            price = null;
            countForGymSize = new int[] { -1, -1, -1 };
        }

        public EstateType(EstateType source)
        {
            this.id = source.id;
            locNameId = source.locNameId;
            this.itemType = source.itemType;
            this.resourceId = source.resourceId;
            this.levelRequirement = source.levelRequirement;
            this.assemblyTime = source.assemblyTime;
            this.exercisesIds = source.exercisesIds;
            this.stats = source.stats;
            this.price = source.price;
            this.style = source.style;
            this.exercises = source.exercises.Select(e => new Exercise(e)).ToArray();
            this.countForGymSize = source.countForGymSize;
        }

        [CSVName("ItemType")]
        private bool SetItemType(string value)
        {
            try
            {
                itemType = (Subtype)Enum.Parse(typeof(Subtype), value, true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [CSVName("ex1")]
        private void AddExercise1(int value)
        {
            exercisesIds[0] = value;
        }

        [CSVName("ex2")]
        private void AddExercise2(int value)
        {
            exercisesIds[1] = value;
        }

        [CSVName("ex3")]
        private void AddExercise3(int value)
        {
            exercisesIds[2] = value;
        }

        public int GetExerciseTier(Exercise exercise)
        {
            for (int i = 0; i < exercises.Length; i++)
            {
                if (exercises[i] != null && exercises[i].id == exercise.id)
                    return i;
            }

            return -1;
        }

        [CSVName("gymSize for second")]
        private void AddSizeCount1(int value)
        {
            countForGymSize[0] = value;
        }

        [CSVName("gymSize for third")]
        private void AddSizeCount2(int value)
        {
            countForGymSize[1] = value;
        }

        [CSVName("gymSize for fourth")]
        private void AddSizeCount3(int value)
        {
            countForGymSize[2] = value;
        }

        public bool isUsable
        {
            get
            {
                return (itemType == Subtype.Training ||
                        itemType == Subtype.Sport ||
                        itemType == Subtype.ActiveDecor);
            }
        }

        public bool IsSimple()
        {
            var threshold = DataTables.instance.balanceData.Quests.VisitorQuests.simpleEquipmentLevelThreshold;

            return levelRequirement <= threshold;
        }
    }
}