﻿using System;
using System.Collections.Generic;
using Data.PlayerProfile;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

namespace Data
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class Cost
    {
        public enum CostType
        {
            None,

            CoinsOnly,
            BucksOnly,
            ResourcesOnly,
            ResourcesAndCoins,
        }

        [JsonProperty]
        public CostType     type            = CostType.None;

        [JsonProperty]
        public int          value           = 0;

        [JsonProperty]
        public int          level           = 0;

        [JsonProperty]
        public Dictionary<ResourceType, int>    resources = new Dictionary<ResourceType, int>();

        public bool         isFree
        {
            get
            {
                switch (type)
                {
                    case CostType.None:
                        return true;

                    case CostType.CoinsOnly:
                    case CostType.BucksOnly:
                        return value == 0;

                    case CostType.ResourcesOnly:
                        return (resources.Count == 0);

                    case CostType.ResourcesAndCoins:
                        return (value == 0 && resources.Count == 0);
                }

                return false;
            }
        }

        public ResourceType resourceType    { get { return GetResourceType(); } }
        public string       valueString     { get { return ValueString(); } }
        public Sprite       sprite          { get { return GetSprite(); } }
        public string       name            { get { return GetName(); } }

        public Cost ()
        {
        }

        public Cost (Cost other)
        {
            type = other.type;
            value = other.value;
            level = other.level;
            resources = new Dictionary<ResourceType, int>(other.resources);
        }

        public Cost (Cost other1, Cost other2)
        {
            Assert.IsTrue(other1.type == CostType.CoinsOnly || other1.type == CostType.ResourcesAndCoins || other1.type == CostType.ResourcesOnly);
            Assert.IsTrue(other2.type == CostType.CoinsOnly || other2.type == CostType.ResourcesAndCoins || other2.type == CostType.ResourcesOnly);

            type = CostType.ResourcesAndCoins;
            value = other1.value + other2.value;
            level = Math.Min(other1.level, other2.level);

            foreach (var res in other1.resources)
                AddResource(res.Key, res.Value);
            foreach (var res in other2.resources)
                AddResource(res.Key, res.Value);
        }

        public Cost (CostType type, int value, int level)
        {
            this.type = type;
            this.value = value;
            this.level = level;
        }

        public Cost (string type, int value, int level = 0)
        {
            if (type == "Coins")
                this.type = CostType.CoinsOnly;
            else if (type == "Bucks")
                this.type = CostType.BucksOnly;
            else
            {
                this.type = CostType.None;
                Debug.LogError("Wrong cost type in constructor");
                return;
            }

            this.value = value;
            this.level = level;
        }

        public Cost (ResourceType res, int value)
        {
            this.type = CostType.ResourcesOnly;
            resources.Add(res, value);
            this.value = value;
            this.level = 0;
        }

        public void AddResource (ResourceType id, int count = 1)
        {
            if (id != ResourceType.None && count > 0)
            {
                if (resources.ContainsKey(id))
                    resources[id] += count;
                else
                    resources.Add(id, count);
            }
        }

        private string ValueString ()
        {
            if (value == 0)
                return "0";
            else
                return value.ToString("## ### ###").Trim();
        }

        public string GetString ()
        {
            switch (type)
            {
                case CostType.CoinsOnly:
                return "@coins@ " + value;

                case CostType.BucksOnly:
                return "@bucks@ " + value;

                default:
                return value.ToString();
            }
        }

        public string GetTypeString ()
        {
            switch (type)
            {
                case CostType.CoinsOnly:
                return "Gold";

                case CostType.BucksOnly:
                return "FitBucks";

                default:
                return "unknown";
            }
        }

        public void Clear ()
        {
            type = CostType.None;
        }

        public static Sprite GetSprite (ResourceType resourceType)
        {
            try
            {
                return View.UI.GUICollections.instance.storageIcons.items[(int)resourceType].sprite;
            }
            catch
            {
                Debug.LogError("Cost : no sprite for resource");
            }

            return null;
        }

        public static Sprite GetSpriteCoins ()
        {
            try
            {
                return View.UI.GUICollections.instance.commonIcons.array[0];
            }
            catch
            {
                Debug.LogError("Cost : no sprite for resource");
            }

            return null;
        }

        public static Sprite GetSpriteBucks ()
        {
            try
            {
                return View.UI.GUICollections.instance.commonIcons.array[4];
            }
            catch
            {
                Debug.LogError("Cost : no sprite for resource");
            }

            return null;
        }

        public static Sprite GetSpriteExp ()
        {
            try
            {
                return View.UI.GUICollections.instance.commonIcons.array[1];
            }
            catch
            {
                Debug.LogError("Cost : no sprite for resource");
            }

            return null;
        }

        public static int RecourcePriceInFitbucks (ResourceType resourceType)
        {
            try
            {
                return DataTables.instance.resourceShop[resourceType].bucks;
            }
            catch
            {
                Debug.LogError("Cost : no price for resource");
            }

            return 0;
        }

        public static int CoinsToFitBucks (int coins)
        {
            return Mathf.CeilToInt((float)(coins) * DataTables.instance.balanceData.Costs.cointsToBucksRate);
        }

        public Sprite GetSprite ()
        {
            try
            {
                if (type == CostType.ResourcesOnly)
                {
                    return View.UI.GUICollections.instance.storageIcons.items[(int)resourceType].sprite;
                }
                else if (type == CostType.BucksOnly)
                {
                    return GetSpriteBucks();
                }
                else if (type == CostType.CoinsOnly)
                {
                    return GetSpriteCoins();
                }
                else
                {
                    Debug.LogError("Cost : no sprite for this type");
                }
            }
            catch
            {
                Debug.LogError("Cost : can't retrieve sprite");
            }

            return null;
        }

        public string GetName ()
        {
            try
            {
                if (type == CostType.ResourcesOnly)
                {
                    return View.UI.GUICollections.instance.storageIcons.items[(int)resourceType].name;
                }
                else if (type == CostType.BucksOnly)
                {
                    return "Fit Bucks";
                }
                else if (type == CostType.CoinsOnly)
                {
                    return "Coins";
                }
                else
                {
                    Debug.LogError("Cost : no name for this type");
                }
            }
            catch
            {
                Debug.LogError("Cost : can't retrieve name");
            }

            return "error";
        }

        public int PlatinumCount()
        {
            if (resources.ContainsKey(ResourceType.Platinum))
            {
                return resources[ResourceType.Platinum];
            }

            return 0;
        }

        public int ResourceCount()
        {
            int sum = 0;

            foreach(var it in resources)
            {
                if (it.Key != ResourceType.Platinum && it.Key != ResourceType.None)
                    sum += it.Value; 
            }
            return sum;
        }

        ResourceType GetResourceType ()
        {
            Assert.IsTrue(resources.Count <= 1);
            foreach (var res in resources)
            {
                return res.Key;
            }

            return ResourceType.None;
        }
    }
}