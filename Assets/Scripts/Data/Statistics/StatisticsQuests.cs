﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Data.Quests;

namespace Data
{
    public class StatisticsQuests
    {
        int total = 0;
        Dictionary<QuestType, int> quests = new Dictionary<QuestType, int>();

        public void Add(QuestType questType)
        {
            total++;
            if (quests.ContainsKey(questType))
                quests[questType]++;
            else
                quests.Add(questType, 1);
        }

        public int visitorsTotal
        { 
            get
            {
                if (quests.ContainsKey(QuestType.Visitor))
                    return quests[QuestType.Visitor];
                else
                    return 0;
            }
        }
    }
}
