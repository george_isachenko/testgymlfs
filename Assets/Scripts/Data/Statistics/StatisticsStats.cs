﻿namespace Data
{
    public class StatisticsStats
    {

        int total = 0;
        int cardio = 0;
        int torso = 0;
        int legs = 0;
        int arms = 0;

        public void Add (ExersiseType type, int value)
        {
            total += value;

            switch (type)
            {
                case ExersiseType.Arms:
                arms += value; break;

                case ExersiseType.Cardio:
                cardio += value; break;

                case ExersiseType.Legs:
                legs += value; break;

                case ExersiseType.Torso:
                torso += value; break;
            }
        }

    }
}
