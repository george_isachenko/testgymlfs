﻿namespace Data
{
    public class StatisticsData
    {
        public StatisticsStats          stats = new StatisticsStats();
        public StatistcsExercises       exercises = new StatistcsExercises();
        public StatisticsQuests         quests = new StatisticsQuests();
        public StatisticsUserActivities userActivities = new StatisticsUserActivities();

    }
}