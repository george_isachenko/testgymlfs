﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Data
{
    public class StatistcsExercises {
        int total = 0;

        int tier1 = 0;
        int tier2 = 0;
        int tier3 = 0;

        Dictionary<int,int> exercises = new Dictionary<int, int>();
        Dictionary<int,int> equipments = new Dictionary<int, int>();

        public void Add(int exerciseId, int equipmentId, int tier)
        {
            total++;

            switch (tier) {
            case 1: 
                tier1++; break;
            case 2:
                tier2++; break;
            case 3:
                tier3++; break;
            default:
                Debug.LogError("WRONG TIER VALUE"); break;
            }
                
            if (exercises.ContainsKey(exerciseId))
                exercises[exerciseId]++;
            else
                exercises.Add(exerciseId, 1);

            if (equipments.ContainsKey(equipmentId))
                equipments[equipmentId]++;
            else
                equipments.Add(equipmentId, 1);
        }
    }
}
