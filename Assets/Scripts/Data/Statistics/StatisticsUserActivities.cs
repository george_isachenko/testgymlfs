﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Data
{
    public enum UserActivityType {
        RewardedVideo,            
    }

    public class StatisticsUserActivities {
        int total = 0;
        Dictionary<UserActivityType, int> activities = new Dictionary<UserActivityType, int>();

        public void Add(UserActivityType activity)
        {
            total++;
            if (activities.ContainsKey(activity))
                activities[activity]++;
            else
                activities.Add(activity, 1);
        }
    }
}