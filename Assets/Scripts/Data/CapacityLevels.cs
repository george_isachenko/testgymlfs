﻿using DataProcessing.CSV;

namespace Data
{
    public class CapacityLevels
    {
        [CSVName("style level")]
        public int level;

        [CSVName("Capacity")]
        public int capacity;
    }
}
