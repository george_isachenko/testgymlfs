﻿namespace Data.RankAchievement
{
    public enum RankAchievementType
    {
        BuyEquipTotal,
        BuyEquip,
        EarnCoins,
        CompleteDailyActivitiesTotal,
        TrainVisitors,
        ExpandGym,
        ReachLevel,
        ReachStyle,
        BuyDecorTotal,
        GetDailyBonus,
        GetDailyBonusDay,
        WatchAd,
        ExpandStorage,
        OpenSportClub,
        OpenSportRoom,
        CompleteTimeQuest,
        CompleteLaneQuest,
        TrainSportsmen,
        BuySportsmen,
        SellSportsmen,
        BuySportResource,
        ExpandSportbench,
        MakeJuice,
        HarvestFruits,
        BuyTree,
        CompleteJuiceQuest
    }

    public class RankAchievementTableData // this data goes to save of achievements progress
    {
        public int rank;
        public RankAchievementType type;
        public int goal;
        public int id;
        public int reward;
    }

    public class RankAchievementTableDataSocial // this data 
    {
        public string gameCenterID;
        public string googlePlayID;
    }


    public class RankReward
    {
        public int reward;
        public string name;
    }
}