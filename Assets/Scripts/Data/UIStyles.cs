﻿using UnityEngine;
using System.Collections;

public static class UIStyles {

	public static Color txtBtnNormal = Color.white;
	public static Color txtBtnLocked = Color.black;

	public static Color btnShopNormal = new Color(167f / 255f, 228f / 255f, 0, 1);
	public static Color btnShopOwned = new Color(255f / 255f, 180f / 255f, 0, 1);
}
