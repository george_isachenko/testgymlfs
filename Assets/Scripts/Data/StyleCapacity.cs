﻿using DataProcessing.CSV;

namespace Data
{
    public class StyleCapacity
    {
        [CSVName("style level")]
        public int level;

        [CSVName("StyleNeeded")]
        public int stylePoints;
    }
}
