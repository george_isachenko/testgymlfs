﻿using DataProcessing.CSV;

namespace Data
{
    public class PersonName
    {
        public int id;
        
        [CSVName("Name")]
        public string name;

        [CSVName("Male")]
        public bool male;

        [CSVName("Female")]
        public bool female;
    }
}