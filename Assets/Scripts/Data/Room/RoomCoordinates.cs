﻿using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Data.Room
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public struct RoomCoordinates
    {
        [JsonProperty]
        public int x;
        [JsonProperty]
        public int y;

        public bool isValid
        {
            get { return x >= 0 && y >= 0; }
        }

        public bool isValidSize
        {
            get { return x > 0 && y > 0; }
        }

        public static RoomCoordinates zero
        {
            get { return new RoomCoordinates(0, 0); }
        }

        public static RoomCoordinates one
        {
            get { return new RoomCoordinates(1, 1); }
        }

        public static RoomCoordinates invalidPosition
        {
            get { return new RoomCoordinates(-1, -1); }
        }

        public RoomCoordinates sizeRotated
        {
            get { return new RoomCoordinates(y, x); }
        }

        public RoomCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public RoomCoordinates GetOrientedSize(int heading)
        {
            var h = heading % 2;
            return ((h == 0) ? this : sizeRotated);
        }

        public int GetSquare()
        {
            return x * y;
        }

        public static float Distance(RoomCoordinates coords1, RoomCoordinates coords2)
        {
            var xDelta = coords2.x - coords1.x;
            var yDelta = coords2.y - coords1.y;

            return Mathf.Sqrt ((float)(xDelta * xDelta) + (yDelta * yDelta));
        }
    
        public static RoomCoordinates GetOrientedSize(RoomCoordinates coordinates, int heading)
        {
            return coordinates.GetOrientedSize(heading);
        }

        public static RoomCoordinates operator +(RoomCoordinates coords1, RoomCoordinates coords2)
        {
            return new RoomCoordinates(coords1.x + coords2.x, coords1.y + coords2.y);
        }

        public static RoomCoordinates operator -(RoomCoordinates coords1, RoomCoordinates coords2)
        {
            return new RoomCoordinates(coords1.x - coords2.x, coords1.y - coords2.y);
        }

        public RoomCoordinates ClampToContraints(RoomCoordinates orientedSize, RoomCoordinates roomSize)
        {
            return new RoomCoordinates
                (Math.Max(0, Math.Min(roomSize.x - orientedSize.x, x))
                , Math.Max(0, Math.Min(roomSize.y - orientedSize.y, y)));
        }

        public bool IsInConstraints(RoomCoordinates orientedSize, RoomCoordinates roomSize)
        {
            return (x >= 0 && y >= 0 && x + orientedSize.x <= roomSize.x && y + orientedSize.y <= roomSize.y);
        }

        public bool IsInConstraints(RoomCoordinates roomSize)
        {
            return (x >= 0 && y >= 0 && x <= roomSize.x - 1 && y <= roomSize.y - 1);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RoomCoordinates))
                return false;

            RoomCoordinates rc = (RoomCoordinates) obj;
            return (rc == this);
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", x, y);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode();
        }

        public static bool operator ==(RoomCoordinates p1, RoomCoordinates p2)
        {
            return (p1.x == p2.x && p1.y == p2.y);
        }

        public static bool operator !=(RoomCoordinates p1, RoomCoordinates p2)
        {
            return (p1.x != p2.x || p1.y != p2.y);
        }
    }
}
