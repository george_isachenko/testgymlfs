﻿namespace Data.Room
{
    public interface IRoomSetupDataInfo
    {
        string              name                                    { get; }
        RoomType            roomType                                { get; }
        RoomExpandMode      roomExpandMode                          { get; }
        int                 expandStep                              { get; }
        int                 blockSize                               { get; }
        RoomCoordinates     initialSizeInCells                      { get; }
        RoomCoordinates     initialSizeInBlocks                     { get; }
        RoomCoordinates     initialSizeNative                       { get; }
        RoomCoordinates     maxSizeInCells                          { get; }
        RoomCoordinates     maxSizeInBlocks                         { get; }
        RoomCoordinates     maxSizeNative                           { get; }
        int                 maxBlocksCount                          { get; }
        int                 autoFillUpRoomLimit                     { get; }
        int                 autoFillUpQueueLimit                    { get; }
        int                 queueRunThreshold                       { get; }
        float               queueSpawnChancePerFreeSlotPerMinute    { get; }
        int                 adaptiveSpawnRateStartLevel             { get; }
        float               delayQueueStart                         { get; }
        bool                noSpawnWhenTutorialRunning              { get; }
    }
}