﻿using Core.Timer;
using Newtonsoft.Json;

namespace Data.Room
{
    [JsonObject]
    public class RoomData
    {
        public class RoomBlock
        {
            public bool unlocked;
        }

        public RoomCoordinates currentSize;
        public RoomBlock[] blocks;
        public int wallId;
        public int floorId;
        public int expandLevel;
        public TimerFloat expandUnlockTimer;

        public RoomData (IRoomSetupDataInfo roomSetupData, int initialExpandLevel)
        {
            var initialSize = roomSetupData.initialSizeInCells;
            this.currentSize = new RoomCoordinates(initialSize.x, initialSize.y);
            wallId = 0;
            floorId = 0;
            expandLevel = initialExpandLevel;
            expandUnlockTimer = null;

            var blockSize = roomSetupData.blockSize;
            if (blockSize > 0)
            {
                blocks = new RoomBlock[blockSize];
                for(int i = 0; i < blocks.Length; i++)
                    blocks[i] = new RoomBlock();

                for (int i = 0; i < initialExpandLevel - 1; i++)
                {
                    blocks[i].unlocked = true;
                }
            }
            else
                blocks = null;
        }

        [JsonConstructor]
        protected RoomData()
        {
            wallId = 0;
            floorId = 0;
            expandLevel = 0;
            expandUnlockTimer = null;
            blocks = null;
        }
    }
}
