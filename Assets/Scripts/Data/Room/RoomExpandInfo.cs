﻿using System;
using DataProcessing.CSV;

namespace Data.Room
{
    public class RoomExpandInfo
    {
        public struct Key
        {
            [CSVName("Expand Level")]
            public int expandLevel;

            [CSVName("Room Name")]
            public string roomName;

            public Key(int expandLevel, string roomName)
            {
                this.expandLevel = expandLevel;
                this.roomName = roomName;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Key))
                    return false;

                Key k = (Key) obj;
                return (k == this);
            }

            public override string ToString()
            {
                return string.Format("{0}:{1}", expandLevel, roomName);
            }

            public override int GetHashCode()
            {
                return expandLevel.GetHashCode() ^ roomName.GetHashCode();
            }

            public static bool operator == (Key p1, Key p2)
            {
                return (p1.expandLevel == p2.expandLevel && p1.roomName == p2.roomName);
            }

            public static bool operator != (Key p1, Key p2)
            {
                return (p1.expandLevel != p2.expandLevel || p1.roomName != p2.roomName);
            }
        }

        [CSVName("Cost")]
        public int cost;

        [CSVName("Resource1")]
        public int resource1;

        [CSVName("Resource2")]
        public int resource2;

        [CSVName("Resource3")]
        public int resource3;

        [CSVName("Resource4")]
        public int resource4;

        [CSVName("Add Style")]
        public int addStyle;

        [CSVName("Duration")]
        public TimeSpan duration;

        [CSVName("Unlock Level")]
        public int unlockLevel;

        [CSVName("Expand Width")]
        public int expandWidth;

        [CSVName("Expand Height")]
        public int expandHeight;

        static public Key CreateKey (int expandLevel, string roomName)
        {
            return new Key (expandLevel, roomName);
        }
    }
}
