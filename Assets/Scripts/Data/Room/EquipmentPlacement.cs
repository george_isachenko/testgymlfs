﻿using System;
using UnityEngine;

namespace Data.Room
{
    [Serializable]
    public struct EquipmentPlacement
    {
	    public RoomCoordinates position;
	    public int heading;

        public EquipmentPlacement (RoomCoordinates position, int heading)
        {
            this.position = position;
            this.heading = heading;
        }

        public EquipmentPlacement (int x, int y, int heading)
        {
            this.position = new RoomCoordinates(x, y);
            this.heading = heading;
        }

        public EquipmentPlacement (int x, int y)
        {
            this.position = new RoomCoordinates(x, y);
            this.heading = 0;
        }

        public Vector3 GetShiftRotated (Vector3 shift)
        {
            return GetShiftRotated(shift, heading);
        }

        public static Vector3 GetShiftRotated (Vector3 shift, int heading)
        {
            if (shift == Vector3.zero)
                return Vector3.zero;

            var rotationRaw = Quaternion.Euler(0, heading * 90.0f, 0);
            return rotationRaw * shift;
        }

        public override bool Equals(object obj) 
        {
            if (!(obj is EquipmentPlacement))
                return false;

            EquipmentPlacement ep = (EquipmentPlacement) obj;
            return (ep == this);
        }

        public override int GetHashCode()
        {
            return position.GetHashCode() ^ heading.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", position, heading);
        }
        
        public static bool operator == (EquipmentPlacement p1, EquipmentPlacement p2)
        {
            return (p1.position == p2.position && p1.heading == p2.heading);
        }

        public static bool operator != (EquipmentPlacement p1, EquipmentPlacement p2)
        {
            return (p1.position != p2.position || p1.heading != p2.heading);
        }
    }
}