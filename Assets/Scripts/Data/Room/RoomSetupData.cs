﻿using System;
using UnityEngine;

namespace Data.Room
{
    public enum RoomType
    {
        Club = 0,
        SportClub = 1,
        JuiceBar = 2,

        City = 1000,

        Unused = -1,
    }

    public enum RoomExpandMode
    {
        Fixed = 0,
        Linear = 1,
        BlockGrid = 2,
        SideBySide = 3,
    }

    [Serializable]
    public class RoomSetupData : IRoomSetupDataInfo
    {
        #region Public (Inspector) fields.
        [SerializeField]
        protected string name;

        [SerializeField]
        protected RoomType roomType;

        //--------------------------------------
        [Header("Expand mode")]

        [Tooltip("Expand mode:\n.Fixed: fixed size room in cells;\nLinear: room of irregular size that is unlocked part by part;BlockGrid: room that is unlocked block by block that form a regular grid;SideBySide: room unlocked in small steps defined by expandStepOrCellSize by each side.")]
        [SerializeField]
        public RoomExpandMode roomExpandMode;

        [Range(0, 10)]
        [Tooltip("Expand step in cells in 'SideBySide' mode or block size in cells in 'BlockGrid' mode, not valid in other modes.")]
        [SerializeField]
        protected int expandStepOrBlockSize = 1;

        //--------------------------------------
        [Header("Sizes")]

        [Range(1, 100)]
        [Tooltip("Maximum size by X in blocks in 'BlockGrid' mode or in cells in other modes.")]
        [SerializeField]
        protected int maxSizeX = 60;

        [Tooltip("Maximum size by Y in blocks in 'BlockGrid' mode or in cells in other modes.")]
        [Range(1, 100)]
        [SerializeField]
        protected int maxSizeY = 60;

        [Range(1, 100)]
        [Tooltip("Initial size in cells in 'SideBySide' mode. Not used in other modes, it's always at maximum size.")]
        [SerializeField]
        protected int initialSizeX = 16;

        [Range(1, 100)]
        [Tooltip("Initial size in cells in 'SideBySide' mode. Not used in other modes, it's always at maximum size.")]
        [SerializeField]
        protected int initialSizeY = 16;

        //--------------------------------------
        [Header("Queue settings")]

        [Tooltip("Try to fill up room with auto-generated persons until it reaches limit (but no more than room support by game logic rules).")]
        [SerializeField]
        protected int autoFillUpRoomLimit = 0;

        [Tooltip("Try to fill up room's queue with auto-generated persons until it reaches limit (but no more than room's queue can accept).")]
        [SerializeField]
        protected int autoFillUpQueueLimit = 0;

        [Tooltip("Spawned in-queue persons are run instead of walk if there's less persons than this threshold await in queue.")]
        [SerializeField]
        protected int queueRunThreshold = 1;

        [Tooltip("Spawn chance (0..1 based) per free slot in queue per minute passed from previous spawn.")]
        [SerializeField]
        protected float queueSpawnChancePerFreeSlotPerMinute = 1.0f;

        [Tooltip("Adaptive chance-based queue spawn rate algorithm starting level.")]
        [SerializeField]
        protected int adaptiveSpawnRateStartLevel = 1;

        [SerializeField]
        protected float delayQueueStart = 0.0f;

        [SerializeField]
        protected bool noSpawnWhenTutorialRunning = false;
        #endregion

        #region IRoomSetupDataInfo interface.
        string IRoomSetupDataInfo.name
        {
            get
            {
                return name;
            }
        }

        RoomType IRoomSetupDataInfo.roomType
        {
            get
            {
                return roomType;
            }
        }

        RoomExpandMode IRoomSetupDataInfo.roomExpandMode
        {
            get
            {
                return roomExpandMode;
            }
        }

        int IRoomSetupDataInfo.expandStep
        {
            get
            {
                return (roomExpandMode == RoomExpandMode.SideBySide) ? expandStepOrBlockSize : 0;
            }
        }

        int IRoomSetupDataInfo.blockSize
        {
            get
            {
                return (roomExpandMode == RoomExpandMode.BlockGrid) ? expandStepOrBlockSize : 0;
            }
        }

        RoomCoordinates IRoomSetupDataInfo.initialSizeInCells
        {
            get
            {
                switch (roomExpandMode)
                {
                    case RoomExpandMode.Fixed:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.Linear:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.BlockGrid:
                        return new RoomCoordinates(initialSizeX * expandStepOrBlockSize - 1, initialSizeY * expandStepOrBlockSize - 1);

                    case RoomExpandMode.SideBySide:
                        return new RoomCoordinates(initialSizeX, initialSizeY);
                }
                return RoomCoordinates.zero;
            }
        }

        RoomCoordinates IRoomSetupDataInfo.initialSizeInBlocks
        {
            get
            {
                if (roomExpandMode == RoomExpandMode.BlockGrid)
                {
                    return new RoomCoordinates(initialSizeX, initialSizeY);
                }
                else if (roomExpandMode == RoomExpandMode.Fixed)
                {
                    return RoomCoordinates.one;
                }
                else
                {
                    return RoomCoordinates.zero;
                }
            }
        }

        RoomCoordinates IRoomSetupDataInfo.initialSizeNative
        {
            get
            {
                switch (roomExpandMode)
                {
                    case RoomExpandMode.Fixed:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.Linear:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.BlockGrid:
                        return new RoomCoordinates(initialSizeX, initialSizeY);

                    case RoomExpandMode.SideBySide:
                        return new RoomCoordinates(initialSizeX, initialSizeY);
                }
                return RoomCoordinates.zero;
            }
        }

        RoomCoordinates IRoomSetupDataInfo.maxSizeInCells
        {
            get
            {
                switch (roomExpandMode)
                {
                    case RoomExpandMode.Fixed:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.Linear:
                        return new RoomCoordinates(maxSizeX, maxSizeY);

                    case RoomExpandMode.BlockGrid:
                        return new RoomCoordinates(maxSizeX * expandStepOrBlockSize - 1, maxSizeY * expandStepOrBlockSize - 1);

                    case RoomExpandMode.SideBySide:
                        return new RoomCoordinates(maxSizeX, maxSizeY);
                }
                return RoomCoordinates.zero;
            }
        }

        RoomCoordinates IRoomSetupDataInfo.maxSizeInBlocks
        {
            get
            {
                if (roomExpandMode == RoomExpandMode.BlockGrid)
                {
                    return new RoomCoordinates(maxSizeX, maxSizeY);
                }
                else if (roomExpandMode == RoomExpandMode.Fixed)
                {
                    return RoomCoordinates.one;
                }
                else
                {
                    return RoomCoordinates.zero;
                }
            }
        }

        RoomCoordinates IRoomSetupDataInfo.maxSizeNative
        {
            get
            {
                return new RoomCoordinates(maxSizeX, maxSizeY);
            }
        }

        int IRoomSetupDataInfo.maxBlocksCount
        {
            get
            {
                switch (roomExpandMode)
                {
                    case RoomExpandMode.Fixed:
                        return 1;

                    case RoomExpandMode.Linear:
                        return 0;

                    case RoomExpandMode.BlockGrid:
                        return maxSizeX * maxSizeY;

                    case RoomExpandMode.SideBySide:
                        return 0;
                }
                return 0;
            }
        }

        int IRoomSetupDataInfo.autoFillUpRoomLimit
        {
            get
            {
                return autoFillUpRoomLimit;
            }
        }

        int IRoomSetupDataInfo.autoFillUpQueueLimit
        {
            get
            {
                return autoFillUpQueueLimit;
            }
        }

        int IRoomSetupDataInfo.queueRunThreshold
        {
            get
            {
                return queueRunThreshold;
            }
        }

        float IRoomSetupDataInfo.queueSpawnChancePerFreeSlotPerMinute
        {
            get
            {
                return queueSpawnChancePerFreeSlotPerMinute;
            }
        }

        int IRoomSetupDataInfo.adaptiveSpawnRateStartLevel
        {
            get
            {
                return adaptiveSpawnRateStartLevel;
            }
        }

        float IRoomSetupDataInfo.delayQueueStart
        {
            get
            {
                return delayQueueStart;
            }
        }

        bool IRoomSetupDataInfo.noSpawnWhenTutorialRunning
        {
            get
            {
                return noSpawnWhenTutorialRunning;
            }
        }

        #endregion

        #region Constructors.
        public RoomSetupData ()
        {
        }

        public RoomSetupData(string name)
        {
            this.name = name;
        }
        #endregion

        #region Public Editor-only API.
#if UNITY_EDITOR
        public void Validate()
        {
            switch (roomExpandMode)
            {
                case RoomExpandMode.Fixed:
                {
                    if (maxSizeX < 1)
                        maxSizeX = 1;

                    if (maxSizeY < 1)
                        maxSizeY = 1;
                }; break;

                case RoomExpandMode.Linear:
                {
                    if (maxSizeX < 1)
                        maxSizeX = 1;

                    if (maxSizeY < 1)
                        maxSizeY = 1;
                }; break;

                case RoomExpandMode.BlockGrid:
                {
                    if (expandStepOrBlockSize < 1)
                        expandStepOrBlockSize = 1;

                    if (maxSizeX < 1)
                        maxSizeX = 1;

                    if (maxSizeY < 1)
                        maxSizeY = 1;

                    initialSizeX = maxSizeX;
                    initialSizeY = maxSizeY;
                }; break;

                case RoomExpandMode.SideBySide:
                {
                    if (expandStepOrBlockSize < 1)
                        expandStepOrBlockSize = 1;

                    if (maxSizeX < 1)
                        maxSizeX = 1;

                    if (maxSizeY < 1)
                        maxSizeY = 1;

                    maxSizeX = Math.Max(expandStepOrBlockSize, (maxSizeX / expandStepOrBlockSize) * expandStepOrBlockSize);
                    maxSizeY = Math.Max(expandStepOrBlockSize, (maxSizeY / expandStepOrBlockSize) * expandStepOrBlockSize);

                    if (initialSizeX > maxSizeX)
                        initialSizeX = maxSizeX;

                    if (initialSizeY > maxSizeY)
                        initialSizeY = maxSizeY;

                    initialSizeX = Math.Max(expandStepOrBlockSize, (initialSizeX / expandStepOrBlockSize) * expandStepOrBlockSize);
                    initialSizeY = Math.Max(expandStepOrBlockSize, (initialSizeY / expandStepOrBlockSize) * expandStepOrBlockSize);
                }; break;
            }
        }

        public int EditorGetStateHashCode()
        {
            int hashCode = 0;
            hashCode ^= (name != null ? name.GetHashCode() : 0);
            hashCode ^= roomType.GetHashCode();
            hashCode ^= roomExpandMode.GetHashCode();
            hashCode ^= expandStepOrBlockSize.GetHashCode();
            hashCode ^= maxSizeX.GetHashCode();
            hashCode ^= maxSizeY.GetHashCode();
            hashCode ^= initialSizeX.GetHashCode();
            hashCode ^= initialSizeY.GetHashCode();
            return hashCode;
        }
#endif // #if UNITY_EDITOR
        #endregion
    }
}