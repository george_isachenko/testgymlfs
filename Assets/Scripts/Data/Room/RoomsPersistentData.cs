﻿using System.Collections.Generic;

namespace Data.Room
{
    public class RoomsPersistentData // Must be a class! Shared between modules.
    {
        public Dictionary<string, RoomData> roomsData;
    }
}