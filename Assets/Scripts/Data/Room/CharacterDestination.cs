﻿using System;
using Data.Estate;
using Data.Estate.Info;
using Data.Person;
using Data.Person.Info;
using UnityEngine.Assertions;

namespace Data.Room
{
    [Serializable]
    public struct CharacterDestination
    {
        #region Types.
        public enum Mode
        {
            Coordinates = 0,
            Zone,
            Equipment,
            Person
        }
        #endregion

        public static string wholeRoom
        {
            get
            {
                return "_room";
            }
        }

        #region Properties.
        public Mode mode
        {
            get
            {
                return mode_;
            }
        }

        public RoomCoordinates coordinates
        {
            get
            {
                return coordinates_;
            }
        }

        public string zoneName
        {
            get
            {
                return (mode == Mode.Zone) ? stringMultiValue_ : null;
            }
        }

        public string navPointNamePrefix
        {
            get
            {
                return (mode == Mode.Equipment) ? stringMultiValue_ : null;
            }
        }

        public int equipmentId
        {
            get
            {
                return (mode == Mode.Equipment) ? intMultiValue_ : EquipmentData.InvalidId;
            }
        }

        public int characterId
        {
            get
            {
                return (mode == Mode.Person) ? intMultiValue_ : PersonData.InvalidId;
            }
        }

        public int exerciseTier
        {
            get
            {
                return (mode == Mode.Equipment) ? exerciseTier_ : -1;
            }
        }

        public float heading
        {
            get
            {
                return intMultiValue_;
            }
        }

        public bool isDynamic
        {
            get
            {
                return (mode_ == Mode.Person || mode_ == Mode.Equipment);
            }
        }

        public float distanceMultiplier { get; private set; }
        #endregion

        #region Constructors.
        public CharacterDestination(string zoneName)
        {
            mode_ = Mode.Zone;
            stringMultiValue_ = zoneName;
            coordinates_ = default(RoomCoordinates);
            intMultiValue_ = 0;
            exerciseTier_ = -1;
            distanceMultiplier = 0;
        }

        public CharacterDestination(RoomCoordinates coordinates)
        {
            mode_ = Mode.Coordinates;
            stringMultiValue_ = null;
            coordinates_ = coordinates;
            intMultiValue_ = 0;
            exerciseTier_ = -1;
            distanceMultiplier = 0;
        }

        public CharacterDestination(RoomCoordinates coordinates, float headingDegrees)
        {
            mode_ = Mode.Coordinates;
            stringMultiValue_ = null;
            coordinates_ = coordinates;
            intMultiValue_ = (int)headingDegrees;
            exerciseTier_ = -1;
            distanceMultiplier = 0;
        }

        public CharacterDestination(IEquipmentInfo equipmentInfo, int exerciseTier = -1)
        {
            Assert.IsNotNull(equipmentInfo);

            mode_ = Mode.Equipment;
            stringMultiValue_ = null;
            coordinates_ = default(RoomCoordinates);
            intMultiValue_ = equipmentInfo.id;
            exerciseTier_ = exerciseTier;
            distanceMultiplier = 0;
        }

        public CharacterDestination(IEquipmentInfo equipmentInfo, string navPointNamePrefix, int exerciseTier = -1)
        {
            Assert.IsNotNull(equipmentInfo);

            mode_ = Mode.Equipment;
            stringMultiValue_ = navPointNamePrefix;
            coordinates_ = default(RoomCoordinates);
            intMultiValue_ = equipmentInfo.id;
            exerciseTier_ = exerciseTier;
            distanceMultiplier = 0;
        }

        public CharacterDestination(IPersonInfo personInfo, float distanceMultiplier = 1.0f) // Distance is agent's radius multiplier.
        {
            Assert.IsNotNull(personInfo);

            mode_ = Mode.Person;
            stringMultiValue_ = null;
            coordinates_ = default(RoomCoordinates);
            intMultiValue_ = personInfo.id;
            exerciseTier_ = -1;
            this.distanceMultiplier = distanceMultiplier;
        }
        #endregion

        #region System.Object interface.
        public override bool Equals(object obj)
        {
            if (!(obj is CharacterDestination))
                return false;

            CharacterDestination ep = (CharacterDestination) obj;
            return (ep == this);
        }

        public override int GetHashCode()
        {
            return  mode_.GetHashCode() ^
                    stringMultiValue_.GetHashCode() ^
                    coordinates_.GetHashCode() ^
                    intMultiValue_.GetHashCode() ^
                    exerciseTier_.GetHashCode();
        }

        public static bool operator ==(CharacterDestination p1, CharacterDestination p2)
        {
            return (p1.mode == p2.mode &&
                    p1.stringMultiValue_ == p2.stringMultiValue_ &&
                    p1.coordinates_ == p2.coordinates_ &&
                    p1.intMultiValue_ == p2.intMultiValue_ && 
                    p1.exerciseTier_ == p2.exerciseTier_);
        }

        public static bool operator !=(CharacterDestination p1, CharacterDestination p2)
        {
            return (p1.mode != p2.mode ||
                    p1.stringMultiValue_ != p2.stringMultiValue_ ||
                    p1.coordinates_ != p2.coordinates_ ||
                    p1.intMultiValue_ != p2.intMultiValue_ ||
                    p1.exerciseTier_ != p2.exerciseTier_);
        }

        public override string ToString ()
        {
            switch (mode)
            {
                case Mode.Coordinates:  return string.Format("Coords: [{0}]:{1}", coordinates, heading);
                case Mode.Zone:         return string.Format("Zone: '{0}'", zoneName);
                case Mode.Equipment:    return stringMultiValue_ != null
                                            ? string.Format("Equip: {0}:{1}-{2}", equipmentId, exerciseTier, navPointNamePrefix)
                                            : string.Format("Equip: {0}:{1}", equipmentId, exerciseTier);
                case Mode.Person:       return string.Format("Person: {0}:{1}", characterId, distanceMultiplier);
            }
            return base.ToString();
        }
        #endregion

        #region Private data.
        Mode mode_;
        RoomCoordinates coordinates_;
        string stringMultiValue_;
        int intMultiValue_;
        int exerciseTier_;
        #endregion
    }
}