﻿using UnityEngine;
using UnityEditor;
using Data.Room;

[CustomPropertyDrawer(typeof(RoomCoordinates))]
public class MultiTypePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        var newPosition = EditorGUI.PrefixLabel(position, label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Draw type enum.
        var fieldWidth = newPosition.width / 2 - 5;
        var xRect = new Rect(newPosition.x, newPosition.y, fieldWidth, newPosition.height);
        var xProperty = property.FindPropertyRelative("x");
        EditorGUI.PropertyField(xRect, xProperty, GUIContent.none/* new GUIContent(xProperty.displayName)*/);

        var yRect = new Rect(newPosition.x + fieldWidth + 10, newPosition.y, fieldWidth, newPosition.height);
        var yProperty = property.FindPropertyRelative("y");
        EditorGUI.PropertyField(yRect, yProperty, GUIContent.none/*new GUIContent(yProperty.displayName)*/);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
