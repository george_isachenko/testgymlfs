﻿namespace Data
{
    public static class Serialization
    {
        public static readonly int currentSerializationVersion = 13;
        public static readonly int minSupportedSerializationVersion = 1;
    }
}