﻿using System.Collections.Generic;
using Core.Timer;
using Data.Person;
using Logic.SportAgency;
using UnityEngine;

namespace Data.SportAgency
{
    public delegate string GenerateRandomName(PersonAppearance.Gender gender, bool unique = true);

    public class SportAgencyPersistentData 
    {
        public List<SportAgencySellSlot>    slotsSell = new List<SportAgencySellSlot>();
        public List<SportAgencyBuySlot>     slotsBuy = new List<SportAgencyBuySlot>();
        public TimerFloat                   timerBuySlots;

        public SportAgencyPersistentData()
        {

        }

        public void InitDefaultItems(GenerateRandomName generateRandomName, Dictionary<SportsmanType, SportsmanData> members)
        {
            for (int i = 0; i < 10; i++)
            {
                slotsSell.Add( new SportAgencySellSlot());
                slotsSell[i].state = i < 2 ? SportAgencySellSlot.State.Empty : SportAgencySellSlot.State.Locked;
            }

            var sz = 4; //Enum.GetNames(typeof(SportsmanType)).Length;
            for (int i = 0; i < sz; i++)
                slotsBuy.Add(new SportAgencyBuySlot());

            RefreshBuySlots(generateRandomName, members);
        }
    
        public void RefreshBuySlots(GenerateRandomName generateRandomName, Dictionary<SportsmanType, SportsmanData> members)
        {
            for (int i = 0; i < slotsBuy.Count; i++)
            {
                slotsBuy[i].isSold = false;
                RandomizeBuySlot(i, generateRandomName, members);
            }

            timerBuySlots = new TimerFloat(4 * 60 * 60);
//            timerBuySlots = new TimerFloat(60);
        }

        public void RandomizeBuySlot(int idx, GenerateRandomName generateRandomName, Dictionary<SportsmanType, SportsmanData> members)
        {
            SportAgencyRndSlots.Init(members);
            if (idx < slotsBuy.Count)
            {
                var item = slotsBuy[idx];

                item.type = SportAgencyRndSlots.GetRndType();
                item.gender = (PersonAppearance.Gender)UnityEngine.Random.Range(0,2);
                item.name = generateRandomName(item.gender);
                var coinsPrice = Mathf.CeilToInt(DataTables.instance.sportsmen[item.type].coins * UnityEngine.Random.Range(2.8f,4.2f));
                item.cost = new Cost(Cost.CostType.CoinsOnly, coinsPrice, 0);
            }
            else
                Debug.LogError("RandomizeBuySlot : wrong idx");
        }
    }
}