﻿using System.Linq;
using Core.Timer;
using Data.Person;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

namespace Data.SportAgency
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SportAgencySellSlot 
    {
        public enum State 
        {
            Empty,
            Locked,
            Sell
        }

        public bool             isFree               { get { return GetIsFree(); } }
        public int              sportsmanId          { set { SetSportsman(value); } get { return _sportsmanId; } }

        [JsonProperty] private int             _sportsmanId = -1;

        [JsonProperty] public State            state = State.Locked;
        [JsonProperty] public SportsmanType    sportsmanType;
        [JsonProperty] public string           name;
        [JsonProperty] public TimerFloat       timer;
        [JsonProperty] public bool             complete = false;

        public void Unlock()
        {
            Assert.IsTrue(state == State.Locked);
            state = State.Empty;
        }

        public void Empty()
        {
            state = State.Empty;
            timer = null;
            _sportsmanId = -1;
            name = string.Empty;
            sportsmanType = SportsmanType.Sportsman_0;
            complete = false;
            timer = null;
        }

        public void Sell()
        {
            if (state == State.Sell && _sportsmanId >= 0)
            {
                //DataTables.instance.sportsmen[_sportsmanId].
                timer = new TimerFloat(SportsmenSellTime(sportsmanType));
            }
            else
            {
                Debug.LogError("Can't sell from this slot");
            }
        }

        float SportsmenSellTime(SportsmanType type)
        {
            if (type == SportsmanType.Sportsman_0)
            {
                return 10f * 60f * UnityEngine.Random.Range(0.4f, 1f);
            }
            
            var sportsman = DataTables.instance.sportExercises.FirstOrDefault(e => e.trainTo == (SportsmanType)type);
            if (sportsman != null)
            {
                return (float)sportsman.duration.TotalSeconds * UnityEngine.Random.Range(0.4f, 1f);
            }

            Debug.LogError("Can't calculate sportsmen sell time for type " + type.ToString());
            return 30;
        }

        bool GetIsFree(){

            if (state == State.Empty)
                return true;

            if (state == State.Sell && timer == null && complete == false)
                return true;

            return false;
        }

        void SetSportsman(int idx)
        {
            if (!isFree)
            {
                Debug.LogError("Can't set sportsman to slot");
                return;
            }

            _sportsmanId = idx;
            state = State.Sell;
        }
    }
}
