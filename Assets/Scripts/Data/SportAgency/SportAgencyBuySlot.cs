﻿using Data.Person;
using Newtonsoft.Json;

namespace Data.SportAgency
{
    [JsonObject(MemberSerialization.OptOut)]
    public class SportAgencyBuySlot
    {
        public SportsmanType                    type;
        public string                           name;
        public PersonAppearance.Gender          gender;
        public Cost                             cost;
        public bool                             isSold = false;
    };
}
