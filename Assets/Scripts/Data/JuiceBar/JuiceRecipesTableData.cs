﻿using System.Collections.Generic;
using UnityEngine;
using Data.PlayerProfile;
using System;

namespace Data.JuiceBar
{
    public class JuiceRecipesTableData
    {
        public string                           nameLocalized   { get { return Loc.Get(loc_id); } }
        public Dictionary<ResourceType, int>    ingredients     { get; private set; }
        public Cost                             cost            { get { return GetCost(); } }

        public void UpdateIngredients()
        {
            ingredients = new Dictionary<ResourceType, int>(4);

            if (ingredient_1_count > 0)
                Add(ingredient_1, ingredient_1_count);
            if (ingredient_2_count > 0)
                Add(ingredient_2, ingredient_2_count);
            if (ingredient_3_count > 0)
                Add(ingredient_3, ingredient_3_count);
            if (ingredient_4_count > 0)
                Add(ingredient_4, ingredient_4_count);
        }

        void Add(ResourceType res, int count)
        {
            if (!ingredients.ContainsKey(res))
                ingredients.Add(res, count);
            else
                Debug.LogError("JuiceRecipesTableData : ingredient already added, skipping..");
        }

        public ResourceType id;
        public int level;

        public string loc_id;
        public int exercise_id;
        public int exercise_complete_id;
        public ResourceType ingredient_1;
        public int ingredient_1_count;
        public ResourceType ingredient_2;
        public int ingredient_2_count;
        public ResourceType ingredient_3;
        public int ingredient_3_count;
        public ResourceType ingredient_4;
        public int ingredient_4_count;
        public int reward_coins;
        public int reward_exp;
        public TimeSpan make_time;
        public TimeSpan drink_time;

        Cost GetCost()
        {
            var cost = new Cost();
            cost.type = Cost.CostType.ResourcesOnly;
            cost.AddResource(ingredient_1, ingredient_1_count);
            cost.AddResource(ingredient_2, ingredient_2_count);
            cost.AddResource(ingredient_3, ingredient_3_count);
            cost.AddResource(ingredient_4, ingredient_4_count);

            return cost;
        }
    }
}
