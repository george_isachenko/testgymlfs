﻿using System;
using Data.PlayerProfile;
using DataProcessing.CSV;

namespace Data.JuiceBar
{
    public class FruitTreeDefinition
    {
        #region Public fields.
        [CSVName("Name")]
        public string name;

        [CSVName("Unlock Level")]
        public int unlockLevel;

        [CSVName("Type")]
        public ResourceType type;

        [CSVName("Cost Type")]
        public Cost.CostType costType;

        [CSVName("Cost")]
        public int costAmount;

        [CSVName("Resource Type")]
        public ResourceType resourceType;

        [CSVName("Resource Cost")]
        public int resourceCost;

        [CSVName("Max Fruits")]
        public int maxFruits;

        [CSVName("Regen Time")]
        public TimeSpan regenTime;
        #endregion

        #region Public properties.
        public Cost    cost
        {
            get
            {
                var cost = new Cost(costType, costAmount, 1);
                if (resourceType != ResourceType.None && resourceCost > 0)
                {
                    cost.AddResource(resourceType, resourceCost);
                }

                return cost;
            }
        }
        #endregion
    }
}