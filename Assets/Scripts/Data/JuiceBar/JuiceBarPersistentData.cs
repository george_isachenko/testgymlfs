﻿using System;
using System.Collections.Generic;
using Core.Timer;
using Data.PlayerProfile;
using Newtonsoft.Json;

namespace Data.JuiceBar
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class JuiceOrder 
    {
        [JsonProperty]
        public ResourceType    id       { get; private set; }

        [JsonProperty]
        public TimerFloat      time     { get; private set; }

        public int sortValue
        {
            get
            {
                if (time.secondsToFinish == 0)
                    return int.MaxValue;

                return (int)time.secondsToFinish; 
            }
        }

        public JuiceOrder(ResourceType id, TimeSpan time)
        {
            this.id = id;
            this.time = new TimerFloat((float)time.TotalSeconds);
        }

        [JsonConstructor]
        protected JuiceOrder()
        {
        }

        public static int JuiceOrderCompare(JuiceOrder o1, JuiceOrder o2)
        {
            if (o1.sortValue > o2.sortValue)
                return 1;
            else if(o1.sortValue < o2.sortValue)
                return 0;

            return 0;
        }
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class FruitTree
    {
        [JsonProperty]
        public bool unlocked;

        [JsonProperty]
        public TimerFloat maxFruitsRegenTimer; // Regen to max timer.

        [JsonProperty]
        public int numFruits;
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class JuiceBarPersistentData 
    {
        public const int DefaultSlotsCount = 2;

        [JsonProperty]
        public int orderSlotsAvailable = DefaultSlotsCount;

        [JsonProperty]
        public List<JuiceOrder> orders = new List<JuiceOrder>(8);

        [JsonProperty]
        public Dictionary<string, FruitTree> fruitTrees = new Dictionary<string, FruitTree>(16);

        public JuiceBarPersistentData()
        {
        }
    }
}