﻿using System;
using System.Collections.Generic;
using Data.Person;
using Data.PlayerProfile;
using DataProcessing.JSON;
using Newtonsoft.Json;

namespace Data
{
    public class BalanceData
    {
        public class CostsBalance
        {
            public float cointsToBucksRate;
        }

        public class AdvertisementBalance
        {
            public int startLevel;
            public int enableOnTVLevel;
            public int viewsForPeriod;
            public int viewsForPeriod_WithInApps;
            public int limit2Level;
            [JsonConverter(typeof(StringTimeSpanConverter))]
            public TimeSpan limit2Period;
            public int limit2ViewsForPeriod;
            public int limit2ViewsForPeriod_WithInApps;
        }

        public class CharactersBalance
        {
            public class TrainersBalance
            {
                public List<int> supportAnimationLoops; // NOTE! List<float> used instead of float[] because of iOS idiosyncrasies with JSON.Net parser. 
                public float prepareInteractionMinTime;
                public float prepareInteractionMaxTime;
                public float supportBeginCooldownTime;
                public float supportCooldownMinTime;
                public float supportCooldownMaxTime;
                public float trainerAwaitsClientCloseEnoughDistance;
                public float interactionCloseEnoughDistance;
                public float awaitNearEquipmentFallbackTime;
                public MovementSettings TrainerMoveToClient;
                public MovementSettings TrainerMoveToEquipment;
                public MovementSettings ClientFollow;
                public MovementSettings ClientFollowOneTime;
                public MovementSettings ClientMoveToEquipmentToWork;
                public float offlineExerciseMultiplyTimeMin;
                public float offlineExerciseMultiplyTimeMax;
                [JsonConverter(typeof(StringTimeSpanConverter))]
                public TimeSpan offlineExerciseAddTimeMin;
                [JsonConverter(typeof(StringTimeSpanConverter))]
                public TimeSpan offlineExerciseAddTimeMax;

                public TrainersBalance () // NOTE! Explicit constructor is written also for JSON.Net on iOS. 
                {
                    supportAnimationLoops = new List<int>(3);
                    prepareInteractionMinTime = 2.0f;
                    prepareInteractionMaxTime = 10.0f;
                    supportBeginCooldownTime = 1.0f;
                    supportCooldownMinTime = 1.0f;
                    supportCooldownMaxTime = 5.0f;
                    trainerAwaitsClientCloseEnoughDistance = 3.5f;
                    interactionCloseEnoughDistance = 3.5f;
                    awaitNearEquipmentFallbackTime = 4.0f;
                    offlineExerciseMultiplyTimeMin = 1.0f;
                    offlineExerciseMultiplyTimeMax = 1.005f;
                    offlineExerciseAddTimeMin = TimeSpan.FromSeconds(5.0f);
                    offlineExerciseAddTimeMax = TimeSpan.FromSeconds(20.0f);
                }
            }

            public class MovementBalance
            {
                public MovementSettings Move;
                public MovementSettings MoveToEquipment;
                public MovementSettings MoveToEquipmentToWork;
                public MovementSettings MoveInQueue;
                public MovementSettings IdleDefaultFollow;
                public MovementSettings IdleMove;
                public MovementSettings IdleInteractionMove;
                public MovementSettings EnteringRoom;
                public MovementSettings LeavingRoom;
                public MovementSettings PasserbyWalking;

                public float temporarySpeedUpDuration = 1.0f;
                public float followDistanceThreshold = 3.0f;
                public float interactionCloseEnoughDistance = 3.0f;
            }

            public class IdleSettingsBalance
            {
                // Minimum Idle time without selection to start some idle-unselected actions (in seconds).
                public float timeIdleUnselectedInitial = 9.0f;

                // Minimum Idle time without selection to start some idle-unselected actions (in seconds).
                public float timeIdleUnselectedToDoSomething = 10.0f;

                // Minimum Idle time without selection to try next idle-unselected action after first (in seconds).
                public float timeIdleUnselectedToDoNextAction = 5.0f;

                // Chance to start walking after "Time Idle Unselected To Do Something" passed, per each second after that (0 .. 1).
                public float idleWalkChancePerSecond = 0.1f;

                // Chance to run greeting emote after "Time Idle Unselected To Do Something" passed, per each second after that (0 .. 1).
                public float idleGreetingEmoteChancePerSecond = 0.1f;

                // Chance to start interaction with other character after "Time Idle Unselected To Do Something" passed, per each second after that (0 .. 1).
                public float idleInteractionChancePerSecond = 0.1f;

                public float idleInteractionMinTime = 3.0f;
                public float idleInteractionMaxTime = 10.0f;

                // Minimum Idle time without selection to start some idle-unselected actions (in seconds).
                public float timeIdleSelectedInitial = 9.0f;

                // Minimum Idle time when selected to start some idle-selected actions (in seconds).
                public float timeIdleSelectedToDoSomething = 10.0f;

                // Chance to start interaction with other character after "Time Idle Unselected To Do Something" passed when In-Queue, per each second after that (0 .. 1).
                public float idleInteractionChanceInQueuePerSecond = 0.1f;
            }

            public MovementBalance Movement;
            public TrainersBalance Trainers;
            public Dictionary<string, IdleSettingsBalance> IdleSettings; // Person class type name => IdleSettings.
            public float exerciseSpeedMultiplierMin = 1.0f;
            public float exerciseSpeedMultiplierMax = 1.025f;
        }

        public class QuestsBalance
        {
            public class DailyQuestsBalance
            {
                public int startLevel;
            }

            public class TimeQuestsBalance
            {
                public int startLevel;
            }

            public class VisitorQuestBalance
            {
                public float sportsmanCreationChance;
                public int sportsmanCreationStartLevel;
                public int statsObjectiveFromLevel;
                public int max_1_objectiveToLevel;
                public int max_2_objectivesToLevel;

                public int exerciseProbability;
                public int objectiveCountPercentThreshold_1;
                public int objectiveCountPercentThreshold_2;

                public int simpleEquipmentLevelThreshold;

                public class StatsObjectiveBalance
                {
                    public float coinsRewardMultiplier;
                    public float experienceRewardMultiplier;
                }

                public Dictionary<ExersiseType, StatsObjectiveBalance> StatsObjectives;
            }

            public DailyQuestsBalance DailyQuests;
            public TimeQuestsBalance TimeQuests;
            public VisitorQuestBalance VisitorQuests;
        }

        public class AgencyBalance
        {
            public int startLevel;
        }

        public class ConstantsBalance
        {
            public int showRateUsLevel;
        }

        public class SocialBalance
        {
            public int facebookConnectReward;
        }

        public class EquipmentBalance
        {
            public class UpgradesBalance
            {
                public List<float> experienceRewardMultipliers; // NOTE! List<float> used instead of float[] because of iOS idiosyncrasies with JSON.Net parser. 
                public List<float> coinsRewardMultipliers;

                public UpgradesBalance () // NOTE! Explicit constructor is written also for JSON.Net on iOS. 
                {
                    experienceRewardMultipliers = new List<float>(3);
                    coinsRewardMultipliers = new List<float>(3);
                }
            }

            public UpgradesBalance Upgrades;
            public float sellMultiplier;
            public float sellUpgradeMultiplier;
            public float exerciseFreeSkipTime;
        }

        public class StorageBalance
        {
            public class ResourceBalance
            {
                public string category = string.Empty;
                public int capacityCost;
                public bool hidden;
                public bool availableAsDailyQuestReward;
                public bool availableAsSportLaneQuestReward;
                public bool availableAsSportTimeQuestReward;
                public bool disabled;
            }

            public Dictionary<ResourceType, ResourceBalance> Resources;
        }

        public class TutorialBalance
        {
            public int levelForDetailAnalytics;    
        }

        public class JuiceBarBalance
        {
            public int unlockOrderSlotStartPrice;
        }

        public CostsBalance Costs;
        public AdvertisementBalance Advertisement;
        public CharactersBalance Characters;
        public QuestsBalance Quests;
        public AgencyBalance Agency;
        public ConstantsBalance Constants;
        public SocialBalance Social;
        public EquipmentBalance Equipment;
        public StorageBalance Storage;
        public TutorialBalance Tutorial;
        public JuiceBarBalance JuiceBar;
    }
}
