﻿namespace Data.PlayerProfile
{
    public struct PlayerProfilePersistentData
    {
        public PlayerProfileData data;
        public StorageData storageData;
        public Core.Advertisement.AdvertisementsData adverticementsData;
    }
}
