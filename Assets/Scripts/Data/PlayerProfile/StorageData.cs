﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;
using UnityEngine.Assertions;

namespace Data.PlayerProfile
{
    public enum ResourceType
    {
        None,

        Equip_01,
        Expand_01,
        Expand_02,
        Expand_03,
        Expand_04,
        Storage_01,
        Storage_02,
        Storage_03,
        Storage_04,

        Sport_01,
        Sport_02,
        Sport_03,
        Sport_04,
        Sport_05,
        Sport_06,
        Sport_07,
        Sport_08,
        Sport_09,

        Platinum,

        Fruit_01,
        Fruit_02,
        Fruit_03,
        Fruit_04,
        Fruit_05,
        Fruit_06,

        Juice_01,
        Juice_02,
        Juice_03,
        Juice_04,
        Juice_05,
        Juice_06,
        Juice_07,
        Juice_08
    }

    public struct ResourceShopInfo
    {
        public int bucks;
    }

    public class StorageItemInfo : IComparable<StorageItemInfo>
    {
        public ResourceType         id;
        public string               name;
        public Sprite               sprite;
        public int                  count;
        public int                  idx;
        public string               hintTitle   { get { return GetTitle(); } }
        public string               hintDesc    { get { return GetHint(); } }

        public int CompareTo(StorageItemInfo other)
        {
            return other.count - this.count;
        }

        string GetHint()
        {
            var info = DataTables.instance.resourcesInfo.FirstOrDefault(i => i.resourceId == id);
            if (info == null)
            {
                return String.Empty;
            }
            return Loc.Get(info.resourceDescriptiontLocId);
        }

        string GetTitle()
        {
            var info = DataTables.instance.resourcesInfo.FirstOrDefault(i => i.resourceId == id);
            if (info == null)
            {
                Debug.LogError("No entry in ResourceInfo for " + id.ToString() + " (Assign this bug to Max)");
                return String.Empty;
            }
            return Loc.Get(info.resourceNameLocId);
        }
    }

    public struct StorageCapacity
    {
        public int capacity;
        public int resource_1;
        public int resource_2;
        public int resource_3;
        public int resource_4;
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class StorageData
    {
        // DATA
        [JsonProperty]
        public Dictionary<ResourceType, int> resources = new Dictionary<ResourceType, int>(Enum.GetNames(typeof(ResourceType)).Length);
        [JsonProperty]
        public int capacityIdx = 0;
        [JsonProperty]
        public bool haveNewItem = false;
        [JsonProperty]
        public bool wasPlatina = false;

        // PROPERTIES
        public List<StorageItemInfo>    items                   { get { return GetItems(); } }
        public List<StorageItemInfo>    itemsJuice              { get { return GetItemsJuice(); } }
        public int                      countTotal              { get { return GetCountTotal(); } }
        public int                      countJuices             { get { return GetCountJuices(); } }
        public int                      capacity                { get { return GetCapacity(); } }
        public int                      capacityOld             { get { return GetCapacityOld(); } }
        public int                      capacityNext            { get { return GetCapacityNext(); } }
        public string                   capacityNextRatioStr    { get { return countTotal.ToString() + "/" + capacityNext.ToString(); } }
        public string                   capacityRatioStr        { get { return countTotal.ToString() + "/" + capacity.ToString(); } }
        public float                    capacityRatio           { get { return GetCapacityRatio(); } }
        public Cost                     capacityUpgradeCost     { get { return GetCapacityUpgradeCost(); } }
        public bool                     canUpdgrade             { get { return GetCanUpdgrade(); } }

        // METHODS
        public void Add (ResourceType id, int value)
        {
            BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
            if (!DataTables.instance.balanceData.Storage.Resources.TryGetValue(id, out resBalanceMeta) || resBalanceMeta.disabled)
            {
                Debug.LogError("trying to add wrong resource to storage " + id.ToString());
                return;
            }

            if (value > 0)
            {
                if (resources.ContainsKey(id))
                    resources[id] += value;
                else
                    resources.Add(id, value);

                haveNewItem = true;
            }
            else if (value == 0)
            {
                Debug.LogWarning("StorageData : adding zero value");
            }
            else if (value < 0)
            {
                Debug.LogError("StorageData : can't add negative value");
            }
        }

        public void Remove(ResourceType id, int value)
        {
            BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
            if (!DataTables.instance.balanceData.Storage.Resources.TryGetValue(id, out resBalanceMeta) || resBalanceMeta.disabled)
            {
                Debug.LogError("trying to remove wrong resource to storage " + id.ToString());
                return;
            }

            if (value > 0)
            {
                if (resources.ContainsKey(id))
                {
                    resources[id] = (value <= resources[id]) ? (resources[id] - value) : 0;
                }
                else
                {
                }
            }
            else if (value == 0)
            {
                Debug.LogWarning("StorageData : removing zero value");
            }
            else if (value < 0)
            {
                Debug.LogError("StorageData : can't remove negative value");
            }
        }

        public int GetResourcesCount(ResourceType type)
        {
            int resourcesLeft = 0;
            resources.TryGetValue(type, out resourcesLeft);
            return resourcesLeft;
        }

        List<StorageItemInfo> GetItems()
        {
            var items = new List<StorageItemInfo>(resources.Count);
            var keys = resources.Keys.ToArray();

            for (int i = 0; i < keys.Length; i++)
            {
                BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
                if (DataTables.instance.balanceData.Storage.Resources.TryGetValue(keys[i], out resBalanceMeta) &&
                    !resBalanceMeta.disabled &&
                    !resBalanceMeta.hidden)
                {
                    var item = new StorageItemInfo();
                    var id = keys[i];
    
                    item.id = id;
                    item.count = resources[id];
                    item.sprite = View.UI.GUICollections.instance.storageIcons.items[(int)id].sprite;
                    item.name = View.UI.GUICollections.instance.storageIcons.items[(int)id].name;

                    items.Add(item);
                }
            }

            return items.OrderByDescending(x => x.count).ToList();
        }

        List<StorageItemInfo> GetItemsJuice()
        {
            var items = new List<StorageItemInfo>(resources.Count);
            var keys = resources.Keys.ToArray();

            for (int i = 0; i < keys.Length; i++)
            {
                var id = keys[i];
                // BalanceData.StorageBalance.ResourceBalance resBalanceMeta;

                if (id >= ResourceType.Juice_01)
                {
                    var item = new StorageItemInfo();

                    item.id = id;
                    item.count = resources[id];
                    item.sprite = View.UI.GUICollections.instance.storageIcons.items[(int)id].sprite;
                    item.name = View.UI.GUICollections.instance.storageIcons.items[(int)id].name;

                    items.Add(item);
                }
            }

            return items;
        }

        int GetCountTotal()
        {
            int counter = 0;
            foreach (var res in resources)
            {
                BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
                if (DataTables.instance.balanceData.Storage.Resources.TryGetValue(res.Key, out resBalanceMeta))
                {
                    counter += (res.Value * resBalanceMeta.capacityCost);
                }
            }

            return counter;
        }

        int GetCountJuices()
        {
            var count = 0;
            foreach(var res in resources)
            {
                if (res.Key >= ResourceType.Juice_01)
                    count += res.Value;
            }

            return count;
        }

        int GetCapacity()
        {
            var storageCapacity = DataTables.instance.storageCapacity;

            if (capacityIdx < 0)
            {
                Debug.LogError("storage capacityIdx < 0");
                capacityIdx = 0;
            }

            if (capacityIdx >= storageCapacity.Length)
            {
                return storageCapacity[storageCapacity.Length - 1].capacity;
            }

            return storageCapacity[capacityIdx].capacity;
        }

        int GetCapacityOld()
        {
            var storageCapacity = DataTables.instance.storageCapacity;

            if (capacityIdx == 0)
                return 0;
            else
                return storageCapacity[capacityIdx - 1].capacity; 
        }

        int GetCapacityNext()
        {
            var storageCapacity = DataTables.instance.storageCapacity;

            if (capacityIdx + 1 < storageCapacity.Length)
            {
                return storageCapacity[capacityIdx + 1].capacity; 
            }

            return 0;
        }

        Cost GetCapacityUpgradeCost()
        {
            var storageCapacity = DataTables.instance.storageCapacity;

            if (capacityIdx < 0)
            {
                Debug.LogError("storage capacityIdx < 0");
                capacityIdx = 0;
            }

            var nextIdx = capacityIdx + 1;

            if (nextIdx >= storageCapacity.Length)
            {
                return null;
            }

            return CreateCost(storageCapacity[nextIdx]);
        }

        bool GetCanUpdgrade()
        {
            if (capacityIdx + 1 >= DataTables.instance.storageCapacity.Length)
                return false;
            else
                return true;
        }

        Cost CreateCost(StorageCapacity item)
        {
            var cost = new Cost();
            cost.type = Cost.CostType.ResourcesOnly;

            if (item.resource_1 > 0)
                cost.resources.Add(ResourceType.Storage_01, item.resource_1);
            if (item.resource_2 > 0)
                cost.resources.Add(ResourceType.Storage_02, item.resource_2);
            if (item.resource_3 > 0)
                cost.resources.Add(ResourceType.Storage_03, item.resource_3);
            if (item.resource_4 > 0)
                cost.resources.Add(ResourceType.Storage_04, item.resource_4);

            Assert.IsTrue(cost.resources.Count > 1);

            return cost;
        }

        float GetCapacityRatio()
        {
            if (capacity == 0)
            {
                Debug.LogError("Storage capacity can't be zero");
                return 1;
            }

            var ratio = countTotal / (float)capacity;

            if (ratio < 0)
            {
                Debug.LogError("Storage capacity ratio can't be negative");
                return 0.0f;
            }

            if (ratio > 1.0f)
            {
                Debug.LogWarning("Storage capacity ratio > 1");
                return 1.0f;
            }

            return ratio;
        }

        public void CheckDataIntegrity()
        {
            foreach (var res in resources)
            {
                BalanceData.StorageBalance.ResourceBalance resBalanceMeta;
                if (DataTables.instance.balanceData.Storage.Resources.TryGetValue(res.Key, out resBalanceMeta) && resBalanceMeta.disabled)
                {
                    Debug.LogWarningFormat("CheckDataIntegrity: Removing disabled resource '{0}' from resources list.", res.Key);
                    resources.Remove(res.Key);
                }
            }
        }
    }
}