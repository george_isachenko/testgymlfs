﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Data.PlayerProfile
{
    public class ExperienceLevels
    {
        public int[] levelUpsData { get; private set; }

        public ExperienceLevels(int[] levelUpsData)
        {
            this.levelUpsData = levelUpsData;
        }

        public int maxLevel
        {
            get
            {
                return levelUpsData.Length;
            }
        }

        public int GetExpThreshold(int level)
        {
            if (level > 0 && level < levelUpsData.Length)
            {
                var prevThreshold = levelUpsData[level - 1];
                var curThreshold = levelUpsData[level];

                return curThreshold - prevThreshold;
            }

            return 0;
        }

        public string GetExpRatioStr(int expValue_, int curLevel)
        {
            if (curLevel > 0 && curLevel < levelUpsData.Length)
            {
                int expValue = expValue_;
                int tLevel = curLevel; 

                var prevThreshold = levelUpsData[tLevel - 1];
                var curThreshold = levelUpsData[tLevel];

                var progress = Mathf.Max(0, expValue - prevThreshold);

                var target = curThreshold - prevThreshold;

                return progress.ToString() + "/" + target.ToString();
            }

            return string.Empty;
        }

        public float GetExpRatio(float expValue_, int curLevel)
        {
            float expValue = expValue_;
            int tLevel = curLevel; 

            if (tLevel >= levelUpsData.Length)
                return 1;

            return (expValue - (float)GetPrevExpThreshold(tLevel)) / (float)(GetCurrentExpThreshold((int)expValue) - GetPrevExpThreshold(tLevel));
        }

        public int GetPrevExpThreshold(int level)
        {
            int exp = 0;
            for (int lvl = 0; lvl < levelUpsData.Length; lvl++)
            {
                if (level == lvl)
                    return exp;

                exp = levelUpsData[lvl];
            }

            return 0;
        }

        public int ExpByLvl(int lvl)
        {
            if (lvl < levelUpsData.Length)
                return levelUpsData[lvl];
            else
                return levelUpsData[levelUpsData.Length - 1];
        }

        public int GetLevelByTable(int expVal)
        {
            for (int lvl = 0; lvl < levelUpsData.Length; lvl++)
            {
                int exp = levelUpsData[lvl];

                if (expVal < exp)
                    return lvl;
            }

            return levelUpsData.Length;
        }

        public int GetCurrentExpThreshold(int expValue)
        {
            for (int lvl = 0; lvl < levelUpsData.Length; lvl++)
            {
                int exp = levelUpsData[lvl];

                if (expValue < exp)
                    return exp;
            }

            return -1;
        }
    }
}

