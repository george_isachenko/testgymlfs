﻿using Core.Timer;
using Newtonsoft.Json;

namespace Data.PlayerProfile
{
    public class PlayerProfileData
    {
        public PlayerProfileData(float fatigueTimeSeconds)
        {
            fatigue = new TimerFloat(fatigueTimeSeconds);
        }

        [JsonConstructor]
        protected PlayerProfileData()
        {
            fatigue = new TimerFloat(0);
        }

#if UNITY_EDITOR || DEBUG
        public int coins = 200;
        public int fitBucks = 10;
        public int fitPoints = 0;
#else
        public int coins = 200;
        public int fitBucks = 10;
        public int fitPoints = 0;
#endif
        public int level = 1;
        public int experience = 0;

        public TimerFloat fatigue;

        public bool alreadyRateUs = false;
        public bool wasConnectToFacebook = false;
    }
}