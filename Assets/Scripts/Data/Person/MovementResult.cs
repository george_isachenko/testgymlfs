﻿namespace Data.Person
{
    public enum MovementResult
    {
        Failed = 0,
        Complete = 1,
        Stopped = 2,
        UpdatedDestination = 3,
        Stuck = 4,
    }
}
