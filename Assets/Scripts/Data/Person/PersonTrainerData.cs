using Newtonsoft.Json;

namespace Data.Person
{
    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonTrainerData : PersonData
    {
        #region Static readonly data.
        public const int maxClients = 4;
        #endregion

        #region Serializable fields.
        [JsonProperty]
        public int experience;

        [JsonProperty]
        public int[] clientIds;
        #endregion

        #region Constructors.
        public PersonTrainerData(int id, int level, string roomName)
            : base(id, level, roomName)
        {
            clientIds = new int[maxClients];
            for (int i = 0; i < clientIds.Length; i++)
                clientIds[i] = InvalidId;
        }

        [JsonConstructor]
        protected PersonTrainerData()
            : base()
        {
        }
        #endregion
    }
}