﻿namespace Data.Person
{
    public class PersonAppearance
    {
        public enum Gender
        {
            Male = 0,
            Female
        }

        public enum Race
        {
            White = 0,
            Black,
        }

        public enum VisibleMood
        {
            Good = 0,
            Neutral,
            Sad
        }

        public enum BodyType
        {
            Fat = 0,
            Normal,
            Strong
        }

        public enum ViewType
        {
            Gym = 0,
            Premium,
            Street,
            SportClub,
            Trainer,
            TrainerClient
        }

        public class BodyPart
        {
            public enum PartType
            {
                Head = 0,
                Torso = 1,
                Legs = 2
            }

            public int meshIdx;
            public int clothesIdx;
        }

        public BodyPart[] bodyParts;
        public Gender gender;
        public Race race;
        public VisibleMood visibleMood;
        public BodyType bodyType;
        public ViewType viewType;
    }
}
