﻿using Data.Quests.Juicer;
using Newtonsoft.Json;

namespace Data.Person
{
    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonJuicerData : PersonData
    {
        #region Public fields.
        [JsonProperty]
        public JuiceQuestData juiceQuestData;
        #endregion

        #region Constructors.
        public PersonJuicerData(int id, int level, string roomName, JuiceQuestData juiceQuestData)
            : base (id, level, roomName)
        {
            this.juiceQuestData = juiceQuestData;
        }

        public PersonJuicerData(PersonData other, JuiceQuestData juiceQuestData)
            : base (other)
        {
            this.juiceQuestData = juiceQuestData;
        }

        [JsonConstructor]
        protected PersonJuicerData()
            : base ()
        {
        }
        #endregion
    }
}