﻿using System;
using Data.Person.Info;
using Newtonsoft.Json;

namespace Data.Person
{
    public enum SportsmanType
    {
        Sportsman_0,

        Sportsman_1,
        Sportsman_2,
        Sportsman_3,
        Sportsman_4,
        Sportsman_5,
		Sportsman_6,
		Sportsman_7,
		Sportsman_8,
		Sportsman_9
    }

    public class SportsmanData
    {
        public string       locNameId;
        public int          coins;
        public int          bucks;
    }
    
    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonSportsmanData : PersonData
    {
        #region Public fields.
        [JsonProperty]
        public SportsmanType sportsmanType;

        [JsonProperty]
        public int currentSportExerciseId;
        #endregion

        #region Constructors.
        public PersonSportsmanData(int id, int level, string roomName, SportsmanType sportsmenType)
            : base (id, level, roomName)
        {
            this.sportsmanType = sportsmenType;
            currentSportExerciseId = -1;
        }

        public PersonSportsmanData(PersonData other, SportsmanType sportsmenType)
            : base (other)
        {
            this.sportsmanType = sportsmenType;
            currentSportExerciseId = -1;
        }

        [JsonConstructor]
        protected PersonSportsmanData()
            : base ()
        {
        }
        #endregion
    }
}
