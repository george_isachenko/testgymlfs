﻿using Data.Quests.Visitor;
using Newtonsoft.Json;

namespace Data.Person
{
    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonVisitorData : PersonData
    {
        #region Public fields.
        [JsonProperty]
        public VisitorQuestData visitorQuestData;
        #endregion

        #region Constructors.
        public PersonVisitorData(int id, int level, string roomName, VisitorQuestData visitorQuestData)
            : base (id, level, roomName)
        {
            this.visitorQuestData = visitorQuestData;
        }

        protected PersonVisitorData(PersonVisitorData other)
            : base (other)
        {
            this.visitorQuestData = other.visitorQuestData;
        }

        [JsonConstructor]
        protected PersonVisitorData()
            : base ()
        {
        }
        #endregion
    }
}