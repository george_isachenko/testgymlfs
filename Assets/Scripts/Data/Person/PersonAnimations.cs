﻿namespace Data.Person
{
    public enum AnimationIdlePassive
    {
        Random = -1,

        Default = 0,

        Passive_0 = 1,
        Passive_1 = 2,
        Passive_2 = 3,
        Passive_3 = 4,
    }

    public enum AnimationIdleComplete
    {
        Random = -1,

        Default = 0,

        Passive_0 = 1,
        Passive_1 = 2,
        Passive_2 = 3,
        Passive_3 = 4,

        Active_0 = 5,
        Active_1 = 6,
        Active_2 = 7,
        Active_3 = 8,
        Active_4 = 9,
    }

    public enum AnimationChair
    {
        Chair_anim_in = 0,
        Chair_anim_cycle_0,
        Chair_anim_cycle_1,
        Chair_anim_cycle_2,
        Chair_anim_cycle_3,
        Chair_anim_cycle_4,
        Chair_anim_out
    }

    public enum AnimationWalking
    {
        Auto = 0,
        Walking = 1,
        WalkingFast = 2,
        Running = 3,
    }

    public enum AnimationSocial
    {
        Victory = 0,
        VictoryJump = 1,
        Greeting = 2,
        Support = 3,
        Refuse = 4,
        Expect = 5,
        Facepalm = 6,

        Dialog_Random = 100,
        Dialog_1 = 101,
        Dialog_2 = 102,
        Dialog_3 = 103,
        Dialog_4 = 104,
        Dialog_5 = 105,

        Applaud_Random = 200,
        Applaud_1 = 201,
        Applaud_2 = 202,
        Applaud_3 = 203,

        Gesture_Random = 300,
        Gesture_1 = 301,
        Gesture_2 = 302,
        Gesture_3 = 303
    }

    public enum AnimationSalesman
    {
        Greeting = 0,
        Sadness,
        Idle_0,
        Idle_1,
        Idle_2,
        Idle_3,
        Idle_4,
        Happiness
    }
}
