﻿using System;
using Data.Estate;
using Data.Person.Info;
using Data.Room;
using Newtonsoft.Json;

namespace Data.Person
{
    public enum PersonState
    {
        Entering            = 0,
        Idle                = 1,
        WorkoutPending      = 2,
        Workout             = 3,
        Leaving             = 4,
        Sentenced           = 5,
        ExerciseComplete    = 6,
        InQueue             = 7,
        Refused             = 8,
    }

    public enum ExerciseInitiator
    {
        Player              = 0,    // Default compatibility value for serialization versions < 13.
        Trainer             = 1,    // Exercise was initiated by PersonTrainer for PersonTrainerClient.
        Auto                = 2,    // Means: Person decided do do this exercise by itself (his internal logic told him).
        DebugAutoPlay       = 3,    // Debug Auto-play initiated. Can be skipped for free, for example.
    }

    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonData
    {
        #region Static/readonly data.
        public static readonly int InvalidId = -1;
        #endregion

        #region Public fields.
        [JsonProperty]
        public int id;

        [JsonProperty]
        public string name;

        [JsonProperty]
        public PersonState state;

        [JsonProperty]
        public int level;

        [JsonProperty]
        public RoomCoordinates position;

        [JsonProperty]
        public float heading;

        [JsonProperty]
        public PersonAppearance appearance;

        [JsonProperty]
        public string roomName;

        [JsonProperty]
        public int workoutEquipmentId;

        [JsonProperty]
        public Exercise workoutExercise;

        [JsonProperty]
        public ExerciseInitiator exerciseInitiator;

        [JsonProperty]
        public float exerciseTotalTime;

        [JsonProperty]
        public DateTime exerciseEndTime;

        [JsonProperty]
        public DateTime spawnTime;
        #endregion

        #region Properties.
        public bool isTraining
        {
            get
            {
                return state == PersonState.Workout || state == PersonState.WorkoutPending;
            }
        }

        public bool isSelectable
        {
            get
            {
                return (state == PersonState.Idle ||
                        state == PersonState.Workout ||
                        state == PersonState.ExerciseComplete);
            }
        }

        public bool isNotLeavingOrDead
        {
            get
            {
                return (state == PersonState.InQueue ||
                        state == PersonState.Entering ||
                        state == PersonState.Idle ||
                        state == PersonState.WorkoutPending ||
                        state == PersonState.Workout ||
                        state == PersonState.ExerciseComplete);
            }
        }

        public bool isPlayerControllable
        {
            get
            {
                return (state == PersonState.Idle ||
                        state == PersonState.ExerciseComplete);
            }
        }
        #endregion

        #region Constructors.
        public PersonData(int id, int level, string roomName)
        {
            this.id = id;
            this.state = PersonState.Entering;
            this.level = level;
            this.appearance = new PersonAppearance();
            this.roomName = roomName;
            this.workoutEquipmentId = EquipmentData.InvalidId;
            this.workoutExercise = null;
            this.exerciseInitiator = ExerciseInitiator.Player;
            this.exerciseTotalTime = 0;
            this.exerciseEndTime = DateTime.MinValue;
            this.spawnTime = DateTime.UtcNow;
        }

        public PersonData(PersonData other)
        {
            this.id = other.id;
            this.name = other.name;
            this.state = other.state;
            this.level = other.level;
            this.position = other.position;
            this.heading = other.heading;
            this.appearance = other.appearance;
            this.roomName = other.roomName;
            this.workoutEquipmentId = other.workoutEquipmentId;
            this.workoutExercise = other.workoutExercise;
            this.exerciseInitiator = ExerciseInitiator.Player;
            this.exerciseTotalTime = other.exerciseTotalTime;
            this.exerciseEndTime = other.exerciseEndTime;
            this.spawnTime = other.spawnTime;
        }

        [JsonConstructor]
        protected PersonData()
        {
            this.appearance = new PersonAppearance();

            exerciseInitiator = ExerciseInitiator.Player;
            exerciseEndTime = DateTime.MinValue;
        }
        #endregion
    }
}
