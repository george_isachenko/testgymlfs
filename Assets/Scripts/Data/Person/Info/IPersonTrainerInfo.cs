using Logic.Persons;
using Logic.Trainers;

namespace Data.Person.Info
{
    public interface IPersonTrainerInfo : IPersonInfo
    {
        string                  expTxt       { get; }
        float                   expProgress  { get; }
        int                     clientsMax   { get; }
        bool                    canAssign    { get; }
        bool                    canLevelUp   { get; }
        int                     coinsBonus   { get; }

        PersonTrainer.TrainerUnlockInfo       nextUnlock  { get; }        

        TrainingClientLogic[] clients { get; }
    }
}