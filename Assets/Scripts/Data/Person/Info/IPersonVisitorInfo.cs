﻿using Data.Quests.Visitor;

namespace Data.Person.Info
{
    public interface IPersonVisitorInfo : IPersonInfo
    {
        VisitorQuestData    visitorQuestData        { get; }
    }
}
