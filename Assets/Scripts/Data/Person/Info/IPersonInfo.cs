﻿using System;
using Data.Room;

namespace Data.Person.Info
{
    public interface IPersonInfo
    {
        int                 id                      { get; }
        string              name                    { get; }
        PersonState         state                   { get; }
        int                 level                   { get; }
        RoomCoordinates     position                { get; }
        float               heading                 { get; }
        PersonAppearance    appearance              { get; }
        string              roomName                { get; }
        int                 workoutEquipmentId      { get; }
        Exercise            workoutExercise         { get; }
        float               exerciseTotalTime       { get; }
        DateTime            exerciseEndTime         { get; }
        DateTime            spawnTime               { get; }

        bool                isTraining              { get; }
        bool                isSelectable            { get; }
        bool                isPlayerControllable    { get; }
        bool                isNotLeavingOrDead      { get; }
        bool                useExtendedIdleAnimation { get; }
        bool                canBeTrainedByPlayer    { get; }
        int                 stoppedAvoidanceDelta   { get; }

#if DEBUG
        string              GetDebugInfo            (bool verbose);
#endif
    }
}