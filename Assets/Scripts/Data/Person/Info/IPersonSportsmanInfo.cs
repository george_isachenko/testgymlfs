﻿namespace Data.Person.Info
{
    public interface IPersonSportsmanInfo : IPersonInfo
    {
        SportsmanType       sportsmanType           { get; }
        int                 currentSportExerciseId  { get; }

        SportsmanType       currentOrNextSportsmanType { get; }
    }
}