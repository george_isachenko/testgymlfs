﻿using Data.Quests.Juicer;

namespace Data.Person.Info
{
    public interface IPersonJuicerInfo : IPersonInfo
    {
        JuiceQuestData    juiceQuestData        { get; }
    }
}
