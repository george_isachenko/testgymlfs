﻿namespace Data.Person
{
    public enum MovementMode
    {
        Walking = 0,
        WalkingFast = 1,
        Running = 2,
    }

    public interface IMovementSettingsInfo
    {
        MovementMode mode               { get; }
        bool blendAnimations            { get; }
        int priorityDelta               { get; }
        int numRetries                  { get; }
        float retryDelay                { get; }
    }

    public class MovementSettings : IMovementSettingsInfo
    {
        #region Public fields.
        public MovementMode mode = MovementMode.Walking;
        public bool blendAnimations = false;
        public int priorityDelta = 0;
        public int numRetries = -1;
        public float retryDelay = 1.0f;
        #endregion

        #region IMovementSettingsInfo interface.
        MovementMode IMovementSettingsInfo.mode
        {
            get
            {
                return mode;
            }
        }

        bool IMovementSettingsInfo.blendAnimations
        {
            get
            {
                return blendAnimations;
            }
        }

        int IMovementSettingsInfo.priorityDelta
        {
            get
            {
                return priorityDelta;
            }
        }

        int IMovementSettingsInfo.numRetries
        {
            get
            {
                return numRetries;
            }
        }

        float IMovementSettingsInfo.retryDelay
        {
            get
            {
                return retryDelay;
            }
        }
        #endregion

        #region Constructors
        public MovementSettings ()
        {
        }

        public MovementSettings
            ( MovementMode mode
            , bool blendAnimations = false
            , int priorityDelta = 0
            , int numRetries = -1
            , float retryDelay = 1.0f )
        {
            this.mode = mode;
            this.blendAnimations = blendAnimations;
            this.priorityDelta = priorityDelta;
            this.numRetries = numRetries;
            this.retryDelay = retryDelay;
        }

        public MovementSettings (MovementSettings other)
        {
            this.mode = other.mode;
            this.blendAnimations = other.blendAnimations;
            this.priorityDelta = other.priorityDelta;
            this.numRetries = other.numRetries;
            this.retryDelay = other.retryDelay;
        }

        public MovementSettings (IMovementSettingsInfo info)
        {
            this.mode = info.mode;
            this.blendAnimations = info.blendAnimations;
            this.priorityDelta = info.priorityDelta;
            this.numRetries = info.numRetries;
            this.retryDelay = info.retryDelay;
        }
        #endregion

        public override string ToString()
        {
            return string.Format("Mode: {0}, blend animations: {1}, priorityDelta: {2}, numRetries: {3}, retryDelay: {4}."
                , mode, blendAnimations, priorityDelta, numRetries, retryDelay);
        }
    }
}
