using Data.Quests.Visitor;
using Newtonsoft.Json;

namespace Data.Person
{
    // ===============================================================================================================
    // FULL CLASS NAME IS SERIALIZED IN A SAVEGAME. DO NOT RENAME OR CHANGE NAMESPACE WITHOUT HANDLING THIS SITUATION!
    // ===============================================================================================================
    [JsonObject(MemberSerialization.OptIn)]
    public class PersonTrainerClientData : PersonVisitorData
    {
/*
        public PersonTrainerClientData(int id, int level, string roomName, VisitorQuestData visitorQuestData)
            : base(id, level, roomName, visitorQuestData)
        {
        }
*/

        public PersonTrainerClientData(PersonVisitorData other)
            : base (other)
        {
        }

        [JsonConstructor]
        protected PersonTrainerClientData()
            : base()
        {
        }
    }
}