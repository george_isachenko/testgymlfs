﻿namespace Data.Person
{
    public class TrainerLevels
    {
        public int exp;
        public int clients;
        public int coinsBonus;
    }

    public class TrainerUnlock
    {
        public int level;
        public int count;
        public int hirePrice;
        public string price_type;
    }
}
