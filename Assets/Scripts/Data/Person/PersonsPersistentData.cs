﻿using System.Collections.Generic;

namespace Data.Person
{
    public struct PersonsPersistentData
    {
        public List<PersonData> personsData;
        public int sportsmenStorageLevel;
    }
}