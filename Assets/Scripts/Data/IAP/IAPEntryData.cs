﻿using UnityEngine.Purchasing;
using DataProcessing.CSV;

namespace Data.IAP
{
    public class IAPEntryData
    {
        [CSVName("market_id")]
        public string marketId;
        [CSVName("product_type")]
        public ProductType productType;
        public bool active;
        [CSVName("default_cost")]
        public decimal defaultCost;
    }
}