﻿using UnityEngine;
using System.Collections;
using System;

namespace Data
{
    public enum UltimateShopItemType
    {
        None,
        Equip,
        Interior,
        Decor,
        SportEquip,
        SportResources,
        Bank
    }

    public enum BankBadgeType
    {
        None,
        Best,
        Popular
    }

    public class UltimateShopItemData 
    {
        public int      id;
        public string   name;
        public bool     locked;
        public Cost     cost;
        public string   meshPrefab;
        public Sprite   icon;
        public UltimateShopItemType type;
        public BankBadgeType badgeType = BankBadgeType.None;
        public int      percent;
        public int      trainerLevel = -1;

        public string GetCategoryString()
        {
            switch (type) 
            {
                case UltimateShopItemType.Equip: return "Equipment";
                case UltimateShopItemType.SportEquip: return "SportEquipment";
                case UltimateShopItemType.Decor: return "Decor";
                case UltimateShopItemType.Interior: return "Paint";
            }

            return type.ToString();
        }
    }

    public struct UltimateShopNewInfo
    {
        public UltimateShopItemType type;
        public int id;
    }

    public static class OldShopToNew 
    {
        public static UltimateShopItemType Category(ShopItemCategory category)
        {
            if (category == ShopItemCategory.EQUIPMENTS)
                return UltimateShopItemType.Equip;
            if (category == ShopItemCategory.DECORS)
                return UltimateShopItemType.Decor;
            if (category == ShopItemCategory.PAINTS)
                return UltimateShopItemType.Interior;
            if (category == ShopItemCategory.SPORT_EQUIPMENTS)
                return UltimateShopItemType.SportEquip;
            if (category == ShopItemCategory.BANK)
                return UltimateShopItemType.Bank;
            

            return UltimateShopItemType.None;
        }
    }

    public static class NewShopToOld 
    {
        public static ShopItemCategory Category(UltimateShopItemType category)
        {
            if (category == UltimateShopItemType.Equip)
                return ShopItemCategory.EQUIPMENTS;
            if (category == UltimateShopItemType.Decor)
                return ShopItemCategory.DECORS;
            if (category == UltimateShopItemType.Interior)
                return ShopItemCategory.PAINTS;
            if (category == UltimateShopItemType.SportEquip)
                return ShopItemCategory.SPORT_EQUIPMENTS;
            if (category == UltimateShopItemType.Bank)
                return ShopItemCategory.BANK;

            return ShopItemCategory.UNKNOWN;
        }
    }
    /*
    UltimateShopItemType OldShopCategoryToNew(ShopItemCategory category)
    {

    }*/
}