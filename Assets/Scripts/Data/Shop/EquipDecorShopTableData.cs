﻿using UnityEngine;
using System.Collections;

namespace Data {
    public class EquipDecorShopTableData {
        public int      id;
        public string   name;
        public int      price;
        public string   price_type;
        public string   mesh_prefab;
    }
}