﻿using UnityEngine;
using System.Collections;

namespace Data {
    public class InteriorShopTableData {
        public int      id;
        public string   name;
        public int      price;
        public string   price_type;
        public int      level;
    }
}
