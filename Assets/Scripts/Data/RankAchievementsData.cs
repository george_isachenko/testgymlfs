﻿using System.Collections.Generic;
using Data.RankAchievement;
using Newtonsoft.Json;
using UnityEngine;
using Data.PlayerProfile;
using Logic.Core;

namespace Data
{
    public class RankAchievementsData 
    {
        public int currentRank = 1;

        public List<RankAchievementsTaskData> tasksData = new List<RankAchievementsTaskData>();
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class RankAchievementsTaskData {
        public enum TaskType {
            Level,              // this type calculates on base of current gameplay state
            Iteration           // this type accumulates events
        }

        [JsonProperty]
        public RankAchievementTableData task;

        [JsonProperty]
        public int stepsCompleted;

        [JsonProperty]
        public bool claimed;

        public bool     completed   { get { return GetCompleted(); } }
        public string   steps       { get { return GetSteps(); } }
        public float    progress    { get { return GetProgress(); } }
        public TaskType type        { get { return levelTasks.ContainsKey(task.type) ? TaskType.Level : TaskType.Iteration; } }

        private static Dictionary<RankAchievementType, bool> levelTasks = new Dictionary<RankAchievementType, bool>();

        public RankAchievementsTaskData()
        {
            InitLevelTasks();
        }

        void InitLevelTasks()
        {
            if (levelTasks.Count > 0)
                return;
            
            levelTasks.Add(RankAchievementType.ExpandGym, true);
            levelTasks.Add(RankAchievementType.ReachLevel, true);
            levelTasks.Add(RankAchievementType.ReachStyle, true);
            levelTasks.Add(RankAchievementType.GetDailyBonusDay, true);
            levelTasks.Add(RankAchievementType.ExpandStorage, true);
            levelTasks.Add(RankAchievementType.OpenSportClub, true);
            levelTasks.Add(RankAchievementType.OpenSportRoom, true);
            levelTasks.Add(RankAchievementType.ExpandSportbench, true);
            levelTasks.Add(RankAchievementType.BuyEquip, true);
            levelTasks.Add(RankAchievementType.BuyEquipTotal, true);
            levelTasks.Add(RankAchievementType.BuyDecorTotal, true);
            levelTasks.Add(RankAchievementType.BuyTree, true);
        }

        public void CompleteSteps(int steps)
        {
            stepsCompleted += steps;
            if (stepsCompleted > task.goal)
                stepsCompleted = task.goal;
        }

        string GetDescription()
        {
            if (task.type == RankAchievementType.BuyEquip)
            {
                var table = DataTables.instance.estateTypes;

                if (table.ContainsKey(task.id) && table[task.id].itemType == Data.Estate.EstateType.Subtype.Training)
                    return (table[task.id].locNameId);
                else
                    Debug.LogError("RankAchievementsTaskData : wrong equip id");
            }

            return "implement me";
        }

        string GetSteps()
        {

            if (task.type == RankAchievementType.OpenSportRoom)
            {
                return "";
            }

            int from = 0;
            int to = task.goal;

            if (to == 0)
                return string.Empty;

            if (completed)
                return to.ToString() + "/" + to.ToString();

            if (type == TaskType.Iteration)
                from = stepsCompleted;
            else
                from = GetCurrentLevel();

            return from.ToString() + "/" + to.ToString();

        }

        float GetProgress()
        {
            if (completed)
                return 1;

            if (task.type == RankAchievementType.OpenSportRoom)
            {
                return 0;
            }
            
            if (type == TaskType.Iteration)
                return (float)stepsCompleted / (float)task.goal;
            else
                return (float)GetCurrentLevel() / (float)task.goal; 
        }

        bool GetCompleted()
        {
            #if DEBUG
            if (stepsCompleted == int.MaxValue)
                return true;
            #endif

            var goal = task.goal;

            /////////////////////////////////////////
            // special case for this type
            /////////////////////////////////////////
            if (task.type == RankAchievementType.BuyEquip) // 
            {
                if (goal <=  GetCurrentLevel())
                {
                    stepsCompleted = GetCurrentLevel(); // if we reached desired result – never go back
                    return true;
                }
                return goal <= stepsCompleted;
            } //////////////////////////////////////

            if (goal > 0)
            {
                if (type == RankAchievementsTaskData.TaskType.Iteration)
                {
                    if (goal <= stepsCompleted)
                        return true;
                }
                else
                {
                    if (goal <= GetCurrentLevel())
                        return true;
                }
            }

            return false;
        }


        public int GetCurrentLevel()
        {
            if (task.type == RankAchievementType.ExpandGym)
            {
                return GameCoreBase.GetRoomExpandStage(Data.Room.RoomType.Club) - 5;
            }
            else if (task.type == RankAchievementType.ReachLevel)
            {
                return GameCoreBase.GetPlayerLevel();
            }
            else if (task.type == RankAchievementType.ReachStyle)
            {
                return GameCoreBase.GetRoomStyle();
            }
            else if (task.type == RankAchievementType.GetDailyBonusDay)
            {
                return GameCoreBase.GetDailyBonusDay();
            }
            else if (task.type == RankAchievementType.ExpandStorage)
            {
                return GameCoreBase.GetStorageCapacity();
            }
            else if (task.type == RankAchievementType.OpenSportClub)
            {
                return GameCoreBase.GetRoomExpandStage(Data.Room.RoomType.SportClub) > 0 ? 1 : 0;
            }
            else if (task.type == RankAchievementType.OpenSportRoom)
            {
                return GameCoreBase.GetRoomExpandStage(Data.Room.RoomType.SportClub);
            }
            else if (task.type == RankAchievementType.ExpandSportbench)
            {
                return GameCoreBase.GetSportsmenCapacity();
            }
            else if (task.type == RankAchievementType.BuyEquip)
            {
                var count = GameCoreBase.GetEquipCount(task.id);

                if (count == 0)
                    count = GameCoreBase.GetEquipCount(task.id,  Data.Estate.EstateType.Subtype.Sport);
                
                return count;
            }
            else if (task.type == RankAchievementType.BuyEquipTotal)
            {
                return GameCoreBase.GetEquipCount(-1);
            }
            else if (task.type == RankAchievementType.BuyDecorTotal)
            {
                var dec = GameCoreBase.GetEquipCount(-1, Data.Estate.EstateType.Subtype.Decor);
                var decActive = GameCoreBase.GetEquipCount(-1, Data.Estate.EstateType.Subtype.ActiveDecor);

                return dec + decActive;
            }
            else if (task.type == RankAchievementType.BuyTree)
            {
                return GameCoreBase.GetTreeAvailability(task.id) ? 1 : 0;
            }

            return 0;
        }

        string LocalizedRoomName()
        {
            if (task.goal == 2)
				return Loc.Get("unlockSportClubRoom2Header");
            else if (task.goal == 3)
				return Loc.Get("unlockSportClubRoom3Header");
            else 
            {
                Debug.LogError("No localization name for sport room " + task.goal.ToString());
                return "Room #" + task.goal.ToString();
            }
        }

        public string GetTitle()
        {
            return Loc.Get( task.type.ToString() + "Header");
        }

        public string GetSubTitle()
        {
            var msg = Loc.Get(task.type.ToString() + "Message");

            if (task.type == RankAchievementType.OpenSportRoom)
                return string.Format(msg, LocalizedRoomName());
            
            if (task.type == RankAchievementType.BuyEquip)
                return string.Format(msg, task.goal, LocalizedEquipName(task.id));

            if (task.type == RankAchievementType.MakeJuice)
                return string.Format(msg, task.goal, LocalizedJuiceName(task.id));

            if (task.type == RankAchievementType.HarvestFruits)
                return string.Format(msg, task.goal, Loc.Get(((ResourceType)(task.id + (int)ResourceType.Fruit_01)).ToString()));

            if (task.type == RankAchievementType.BuyTree)
                return string.Format(msg, Loc.Get("JuiceBarFruitTree_" + DataTables.instance.fruitTrees[task.id].name));
                    
            if (msg.Contains("{"))
                return string.Format(msg, task.goal);
            else
                return msg;
        }

        public string LocalizedEquipName(int id)
        {
            var table = DataTables.instance.estateTypes;

            if (table.ContainsKey(id))
            {
                return Loc.Get(table[id].locNameId);
            }

            Debug.LogError("LocalizedEquipName wrong id " + id.ToString());

            return id.ToString();
        }

        string LocalizedJuiceName(int idx)
        {
            if (DataTables.instance.juiceRecipes.Length > idx)
            {
                var currentRecipe = DataTables.instance.juiceRecipes[idx];
                return Loc.Get(currentRecipe.loc_id);
            }

            return "WRONG IDX";
        }
    }
}