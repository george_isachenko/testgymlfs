﻿using DataProcessing.CSV;

namespace Data
{
    public class MemberBody
    {
        public int id;

        [CSVName("player level min")]
        public int playerLevelMin;

        [CSVName("player level max")]
        public int playerLevelMax;

        [CSVName("Name")]
        public string name;

        [CSVName("Member sex")]
        public int sex;

        [CSVName("Member race")]
        public int race;

        [CSVName("body head mesh")]
        public int headMesh;

        [CSVName("body top mesh")]
        public int topMesh;

        [CSVName("body bot mesh")]
        public int bottomMesh;

        [CSVName("reward cash per star")]
        public int rewardCashPerStar;

        [CSVName("overall visits")]
        public int overallVisits;

        [CSVName("return delay")]
        public string returnDelay;

        [CSVName("Quest index")]
        public int questIndex;

        [CSVName("npc lvl")]
        public int npcLevel;

        [CSVName("body type")]
        public int bodyType;

        [CSVName("Default Torso")]
        public int defaultTorso;

        [CSVName("Default Cardio")]
        public int defaultCardio;

        [CSVName("Default Arms")]
        public int defaultArms;

        [CSVName("Default Legs")]
        public int defaultLegs;

        [CSVName("Default Energy")]
        public int defaultEnergy;
    }
}