using Data.PlayerProfile;
using DataProcessing.CSV;

namespace Data.Sport
{
    public class SportResourceData
    {
        public ResourceType id;

        public string name;
        public Cost cost = new Cost();
        public float unlockTimeSeconds;
        public string targetSportsmenId;
        public int bucks;

        [CSVName("unlockLevel")]
        public void SetUnlockEvent(int level)
        {
            cost.level = level;
        }

        [CSVName("coinsCost")]
        public void SetCoinsCost(int coinsCount)
        {
            cost.type = Cost.CostType.CoinsOnly;
            cost.value = coinsCount;
        }
    }
}