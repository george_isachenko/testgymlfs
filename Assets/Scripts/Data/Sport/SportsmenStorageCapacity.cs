﻿using UnityEngine;
using System.Collections;

namespace Data.Sport
{
    public struct SportsmenStorageCapacity
    {
        public int capacity;
        public int resource_1;
        public int resource_2;
        public int resource_3;
        public int resource_4;
    }
}