using DataProcessing.CSV;
using UnityEngine;

namespace Data.Sport
{
    public class SportEquipData
    {
        public int id;
        public string name;
        public Cost cost = new Cost();
        [CSVName("mesh_prefab")] public string meshPrefab;
        public float buyTime;
        public string targetSportsmenId;
        public int requiredUpgradeLevel = 0;
        public string sportEquipRoomLocId;

        [CSVName("price")]
        public void SetPrice(int price)
        {
            cost.value = price;
        }

        [CSVName("price_type")]
        public void SetPriceType(string type)
        {
            switch (type)
            {
                case "Coins":
                    cost.type = Cost.CostType.CoinsOnly;
                    break;

                case "Bucks":
                    cost.type = Cost.CostType.BucksOnly;
                    break;

                default:
                    Debug.LogErrorFormat("Cant parse type [{0}] of cost",type);
                    break;
            }
        }

        [CSVName("level")]
        public void SetLevel(int level)
        {
            cost.level = level;
        }
    }
}