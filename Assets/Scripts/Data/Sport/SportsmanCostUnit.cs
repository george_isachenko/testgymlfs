﻿using Data.Person;

namespace Data.Sport
{
    public struct SportsmanCostUnit 
    {
        public SportsmanType sportsmanType;
        public int           count;
    }
}