using System;
using System.Linq;
using Data;
using Data.Sport;
using Logic.Estate;
using Logic.PlayerProfile;
using UnityEngine;
using View.UI;

namespace Data.Sport
{
    public class SportShopResourceLogic : SportShopItemInfo
    {
        public SportResourceData resourceData { get; private set; }
        public Sprite itemIcon { get; protected set; }

        public SportShopResourceLogic(SportResourceData data, LogicStorage storage)
        {
            resourceData = data;

            id = (int)data.id;
            var exercise = DataTables.instance.sportExercises.FirstOrDefault(e=>e.needResource == data.id);
            if (exercise == null)
            {
                Debug.LogError("Can't find exercise for resource "+data.id);
            }
            else
            {
                forTrainingSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int) exercise.trainFrom].sprite;
                trainingResultSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int)exercise.trainTo].sprite;
            }

            itemIcon = GUICollections.instance.storageIcons.items[(int)data.id].sprite;
            buyItemCost = data.cost;
            baseCooldownStack = new CooldownStackInfo()
            {
                count = 0,
                stackSize = 3
            };
            cooldownTimeSeconds = 60;
            countAtStorage = storage.GetResourcesCount(data.id);
        }
    }

    public class SportShopEquipLogic : SportShopItemInfo
    {
        public SportEquipData equipData { get; private set; }

        public SportShopEquipLogic(SportEquipData equipData, IEquipmentManager manager)
        {
            this.equipData = equipData;

            id = equipData.id;
            var exercise = DataTables.instance.sportExercises.FirstOrDefault(e => e.equipmentId == equipData.id);
            if (exercise == null)
            {
                Debug.LogError("Can't find exercise for equip " + equipData.id);
            }
            else
            {
                forTrainingSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int)exercise.trainFrom].sprite;
                trainingResultSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int)exercise.trainTo].sprite;
            }
            
            buyItemCost = equipData.cost;
            baseCooldownStack = new CooldownStackInfo()
            {
                count = 0,
                stackSize = 50
            };
            cooldownTimeSeconds = 60;
            countAtStorage = manager.equipment.Count(p => p.Value.estateType.id == equipData.id &&
                                                          !p.Value.isLocked);
        }
    }

    public abstract class SportShopItemInfo : IComparable<SportShopItemInfo>
    {
        public int id { get; protected set; }
        public Sprite forTrainingSportsmanIcon { get; protected set; }
        public Sprite trainingResultSportsmanIcon { get; protected set; }
        public Cost buyItemCost { get; protected set; }
        public CooldownStackInfo baseCooldownStack { get; protected set; }
        public CooldownStackInfo currentCooldownStack { get; set; }
        public float cooldownTimeSeconds { get; protected set; }
        public int countAtStorage { get; protected set; }
        public int CompareTo(SportShopItemInfo other)
        {
            return other.countAtStorage - countAtStorage;
        }
    }

    public struct CooldownStackInfo
    {
        public int count;
        public int stackSize;
    }
}