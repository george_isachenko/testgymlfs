using System;
using Data.Person;
using Data.PlayerProfile;
using DataProcessing.CSV;

namespace Data.Sport
{
    public class SportExercise
    {
        #region Static/readonly data.
        public static readonly int InvalidId = -1;
        #endregion

        public int id;

        [CSVName("equipment_id")]
        public int equipmentId;

        [CSVName("exercise_tier")]
        public int exerciseTier;

        [CSVName("sportsmen_needed")]
        public SportsmanType trainFrom;
        
        [CSVName("sportsmen_result")]
        public SportsmanType trainTo;

        [CSVName("resource_needed")]
        public ResourceType needResource;

        [CSVName("duration")]
        public TimeSpan duration;

        public int leverRequired;

        public string sportLocNameId;
    }
}