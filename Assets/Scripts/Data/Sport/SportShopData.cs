using System.Linq;
using Data;
using Data.Sport;
using Logic.Core;
using Newtonsoft.Json;

namespace Data.Sport
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SportShopData
    {
        [JsonProperty]
        public SportShopResourceLogic[] shopResources = new SportShopResourceLogic[0];

        public SportEquipData[] trainersItems
        {
            get { return DataTables.instance.sportEquipment; }
        }

        public SportResourceData[] consumablesItems
        {
            get { return DataTables.instance.sportResources; }
        }

        public SportShopData(IGameCore gameCore)
        {

        }
    }
}