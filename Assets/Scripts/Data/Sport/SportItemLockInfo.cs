using Core.Timer;

namespace Data.Sport
{
    public struct SportItemLockInfo
    {
        public bool isLocked;
        public SportItemLocktype lockType;
        public TimerFloat timer;
        public int unlockLevel;

        public SportItemLockInfo(bool isLocked, SportItemLocktype lockType, int unlockLevel) : this()
        {
            this.isLocked = isLocked;
            this.lockType = lockType;
            this.unlockLevel = unlockLevel;
        }

        public SportItemLockInfo(bool isLocked, SportItemLocktype lockType, TimerFloat timer) : this()
        {
            this.isLocked = isLocked;
            this.lockType = lockType;
            this.timer = timer;
        }

        public SportItemLockInfo(bool isLocked, SportItemLocktype lockType) : this()
        {
            this.isLocked = isLocked;
            this.lockType = lockType;
        }

        public SportItemLockInfo(bool isLocked) : this()
        {
            this.isLocked = isLocked;
        }
    }
}