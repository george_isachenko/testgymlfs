namespace Data.Sport
{
    public enum SportItemLocktype
    {
        none,
        userHaveSmallLevel,
        waitCooldown,
        fullStorage,
        sportRoomLocked
    }
}