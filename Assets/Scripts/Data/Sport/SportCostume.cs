using Data.Person;
using Data.PlayerProfile;

namespace Data.Sport
{
    public class SportCostume
    {
        public SportsmanType sportsmanType;
        public PersonAppearance.Gender gender;
        public PersonAppearance.BodyPart.PartType bodyPart;
        public int costumeId;
    }

    public class ResourceInfo
    {
        public ResourceType resourceId;
        public string resourceDescriptiontLocId;
        public string resourceNameLocId;
    }
}