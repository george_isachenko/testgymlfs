﻿using Data.Estate;
using FileSystem;
using DataProcessing.CSV;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Data.IAP;
using UnityEngine.Assertions;
using Newtonsoft.Json;
using Data.Room;
using Data.Person;
using Data.PlayerProfile;
using Data.Estate.Upgrades;
using Data.RankAchievement;
using Data.DailyBonus;
using Data.Quests.SportQuest;
using Data.Sport;
using Data.JuiceBar;

namespace Data
{
    public class DataTables
    {
        #region Static instance.
        private static DataTables _instance;
        public static DataTables instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DataTables();
                return _instance;
            }
        }
        #endregion
        
        #region Public properties.
        public DataRow[]                                        equipmentShop               { get; private set; }
        public EquipDecorShopTableData[]                        shopEquip                   { get; private set; }
        public EquipDecorShopTableData[]                        shopDecor                   { get; private set; }
        public InteriorShopTableData[]                          shopInterior                { get; private set; }
        public DataRow[]                                        decorShop                   { get; private set; }
        public DataRow[]                                        bankShop                    { get; private set; }
        public Dictionary<ResourceType, ResourceShopInfo>       resourceShop                { get; private set; }
        public Dictionary<RoomExpandInfo.Key, RoomExpandInfo>   expand                      { get; private set; }
        public DataRow[]                                        playerLevels                { get; private set; }
        public StorageCapacity[]                                storageCapacity             { get; private set; }
        public Costume[]                                        costumes                    { get; private set; }
        public DailyBonusReward[]                               dailyBonusRewards           { get; private set; }
        public Dictionary<int, DailyBonusBox>                   dailyBonusBoxes             { get; private set; }
        public DailyBonusDay[]                                  dailyBonusDays              { get; private set; }
        public StyleCapacity[]                                  styleCapacity               { get; private set; }
        public Dictionary<int, WallFloorShopItem>               wallFloorShopItems          { get; private set; }
        public Dictionary<int, EstateType>                      estateTypes                 { get; private set; }
        public Dictionary<EquipmentUpgradeInfo.Key, EquipmentUpgradeInfo> equipmentUpgrades { get; private set; }
        //public Dictionary<int, MemberBody>                    memberBodies                { get; private set; }
        public Dictionary<int, Exercise>                        exercises                   { get; private set; }
        //public Dictionary<int, PersonName>                    personFirstNames            { get; private set; }
        public Dictionary<int, PersonName>                      personFirstNamesMale        { get; private set; }
        public Dictionary<int, PersonName>                      personFirstNamesFemale      { get; private set; }
        public Dictionary<int, TrainerName>                     trainerNames                { get; private set; }
        //public Dictionary<int, PersonName>                    personLastNames             { get; private set; }
        public Dictionary<int, PersonName>                      personLastNamesMale         { get; private set; }
        public Dictionary<int, PersonName>                      personLastNamesFemale       { get; private set; }
        public Dictionary<string, IAPEntryData>                 iapEntries                  { get; private set; }
        public SportResourceData[]                              sportResources              { get; private set; }
        public SportEquipData[]                                 sportEquipment              { get; private set; }
        public SportExercise[]                                  sportExercises              { get; private set; }
        public Dictionary<SportsmanType, SportsmanData>         sportsmen                   { get; private set; }
        public SportsmenStorageCapacity[]                       sportsmenStorageCapacity    { get; private set; }
        public PersonPhrase[]                                   personsPhrases              { get; private set; }
        public CapacityLevels[]                                 capacityLevels              { get; private set; }
        public BalanceData                                      balanceData                 { get; private set; }
        public LaneQuestsCount[]                                laneQuestsCounts            { get; private set; }
        public SportCostume[]                                   sportCostumes               { get; private set; }
        public ResourceInfo[]                                   resourcesInfo               { get; private set; }
        public PromoPurchases[]                                 promoPurchases              { get; private set; }
        public RankAchievementTableData[]                       rankAchievements            { get; private set; }
        public RankAchievementTableDataSocial[]                 rankAchievementsSocial      { get; private set; }
        public Dictionary<int, RankReward>                      rankReward                  { get; private set; }
        public JuiceRecipesTableData[]                          juiceRecipes                { get; private set; }
        public FruitTreeDefinition[]                            fruitTrees                  { get; private set; }
        public TrainerLevels[]                                  trainerLevels               { get; private set; }
        public TrainerUnlock[]                                  trainerUnlocks              { get; private set; }
        public int[]                                            experienceLevels            { get; private set; }
        #endregion

        #region Private data.
        private DataRow[]           _dailyBonusDays;
        #endregion

        #region Constructor.
        private DataTables()
        {
            equipmentShop           = Parser.ParseResource(ResourcePaths.BalanceTables.equipmentShop).ToArray();
            shopEquip               = Parser.ParseResourceToObjects<EquipDecorShopTableData>(ResourcePaths.BalanceTables.equipmentShop).ToArray();
            decorShop               = Parser.ParseResource(ResourcePaths.BalanceTables.decorShop).ToArray();
            shopDecor               = Parser.ParseResourceToObjects<EquipDecorShopTableData>(ResourcePaths.BalanceTables.decorShop).ToArray();
            bankShop                = Parser.ParseResource(ResourcePaths.BalanceTables.bankShop).ToArray();
            resourceShop            = Parser.ParseResourceToDictionary<ResourceType, ResourceShopInfo>(ResourcePaths.BalanceTables.resourceShop);
            rankAchievements        = Parser.ParseResourceToObjects<RankAchievementTableData>(ResourcePaths.BalanceTables.rankAchievements).ToArray();
            rankAchievementsSocial  = Parser.ParseResourceToObjects<RankAchievementTableDataSocial>(ResourcePaths.BalanceTables.rankAchievements).ToArray();
            rankReward              = Parser.ParseResourceToDictionary<int, RankReward>(ResourcePaths.BalanceTables.rankReward);
            juiceRecipes            = Parser.ParseResourceToObjects<JuiceRecipesTableData>(ResourcePaths.BalanceTables.juiceRecipes).ToArray();
            fruitTrees              = Parser.ParseResourceToObjects<FruitTreeDefinition>(ResourcePaths.BalanceTables.fruitTrees).ToArray();
            playerLevels            = Parser.ParseResource(ResourcePaths.BalanceTables.playerLevels).ToArray();
            dailyBonusRewards       = Parser.ParseResourceToObjects<DailyBonusReward>(ResourcePaths.BalanceTables.dailyBonusRewards).ToArray();
            storageCapacity         = Parser.ParseResourceToObjects<StorageCapacity>(ResourcePaths.BalanceTables.storageCapacity).ToArray();
            _dailyBonusDays         = Parser.ParseResource(ResourcePaths.BalanceTables.dailyBonusDays).ToArray();
            wallFloorShopItems      = Parser.ParseResourceToDictionary<int, WallFloorShopItem>(ResourcePaths.BalanceTables.wallFloorShop);
            shopInterior            = Parser.ParseResourceToObjects<InteriorShopTableData>(ResourcePaths.BalanceTables.wallFloorShop).ToArray();
            expand                  = Parser.ParseResourceToDictionaryWithCompositeKey<RoomExpandInfo.Key, RoomExpandInfo>(ResourcePaths.BalanceTables.expand);
            laneQuestsCounts        = Parser.ParseResourceToObjects<LaneQuestsCount>(ResourcePaths.BalanceTables.laneQuestsCount).ToArray();
            estateTypes             = Parser.ParseResourceToDictionary<int, EstateType>(ResourcePaths.BalanceTables.estateTypes);
            equipmentUpgrades       = Parser.ParseResourceToDictionaryWithCompositeKey<EquipmentUpgradeInfo.Key, EquipmentUpgradeInfo>(ResourcePaths.BalanceTables.equipmentUpgrades);
            costumes                = Parser.ParseResourceToObjects<Costume>(ResourcePaths.BalanceTables.costumes).ToArray();
            styleCapacity           = Parser.ParseResourceToObjects<StyleCapacity>(ResourcePaths.BalanceTables.clubStyle).ToArray();
            capacityLevels          = Parser.ParseResourceToObjects<CapacityLevels>(ResourcePaths.BalanceTables.capacityLevels).ToArray();
            sportCostumes           = Parser.ParseResourceToObjects<SportCostume>(ResourcePaths.BalanceTables.sportCostumes).ToArray();
            resourcesInfo           = Parser.ParseResourceToObjects<ResourceInfo>(ResourcePaths.BalanceTables.resourcesInfo).ToArray();
            exercises               = Parser.ParseResourceToDictionary<int, Exercise>(ResourcePaths.BalanceTables.exercises);
            sportResources          = Parser.ParseResourceToObjects<SportResourceData>(ResourcePaths.BalanceTables.sportResourceData).ToArray();
            sportEquipment          = Parser.ParseResourceToObjects<SportEquipData>(ResourcePaths.BalanceTables.sportEquipData).ToArray();
            sportExercises          = Parser.ParseResourceToObjects<SportExercise>(ResourcePaths.BalanceTables.sportExercises).ToArray();
            sportsmen               = Parser.ParseResourceToDictionary<SportsmanType, SportsmanData>(ResourcePaths.BalanceTables.sportsmen);
            sportsmenStorageCapacity = Parser.ParseResourceToObjects<SportsmenStorageCapacity>(ResourcePaths.BalanceTables.sportsmenStorageCapacity).ToArray();
            personsPhrases          = Parser.ParseResourceToObjects<PersonPhrase>(ResourcePaths.BalanceTables.personsPhrases).ToArray();
            promoPurchases          = Parser.ParseResourceToObjects<PromoPurchases>(ResourcePaths.BalanceTables.promoPurchases).ToArray();
            trainerLevels           = Parser.ParseResourceToObjects<TrainerLevels>(ResourcePaths.BalanceTables.trainerLevels).ToArray();
            trainerUnlocks          = Parser.ParseResourceToObjects<TrainerUnlock>(ResourcePaths.BalanceTables.trainersUnlocks).ToArray();
            trainerNames            = Parser.ParseResourceToDictionary<int, TrainerName>(ResourcePaths.BalanceTables.trainerNames);

            if (ResourcePaths.BalanceTables.iap != null)
            {
                iapEntries          = Parser.ParseResourceToDictionary<string, IAPEntryData>
                    (ResourcePaths.BalanceTables.iap);
            }

            /*
                        memberBodies = Parser.ParseToDictionary<int, MemberBody>(ResourcePaths.BalanceTables.memberBodies);
                        if (memberBodies == null)
                        {
                            Debug.LogErrorFormat("Failed to parse member bodies data file \"{0}\".", ResourcePaths.BalanceTables.memberBodies);
                        }
            */

            balanceData = JsonConvert.DeserializeObject<BalanceData>(Resources.Load<TextAsset>(ResourcePaths.BalanceTables.balance).text);

            foreach (var juice in juiceRecipes)
                juice.UpdateIngredients();

            var personFirstNames = Parser.ParseResourceToDictionary<int, PersonName>(ResourcePaths.BalanceTables.personFirstNames);
            {
                if (personFirstNames != null)
                {
                    personFirstNamesMale =  (from name in personFirstNames
                                              where   name.Value.male == true
                                              select  name).ToDictionary(pair => pair.Key, pair => pair.Value);

                    personFirstNamesFemale =  (from name in personFirstNames
                                              where   name.Value.female == true
                                              select  name).ToDictionary(pair => pair.Key, pair => pair.Value);
                }
                else
                {
                    Debug.LogErrorFormat("Failed to parse person's first names  data file \"{0}\".", ResourcePaths.BalanceTables.personFirstNames);
                }
            }

            var personLastNames = Parser.ParseResourceToDictionary<int, PersonName>(ResourcePaths.BalanceTables.personLastNames);
            {
                if (personLastNames != null)
                {
                    personLastNamesMale =  (from name in personLastNames
                                              where   name.Value.male == true
                                              select  name).ToDictionary(pair => pair.Key, pair => pair.Value);

                    personLastNamesFemale =  (from name in personLastNames
                                              where   name.Value.female == true
                                              select  name).ToDictionary(pair => pair.Key, pair => pair.Value);
                }
                else
                {
                    Debug.LogErrorFormat("Failed to parse person's first names  data file \"{0}\".", ResourcePaths.BalanceTables.personLastNames);
                }
            }



            LinkDailyBonusTables();
            BindEstateTypesToExercises();
            BindExercisesToEstateTypes();
            BindShopDatatoEstates ();
            BindSportExerciseNames();

            experienceLevels = playerLevels
                .Where(x => x.ContainsKey("exp"))
                .Select(x => x.GetInt("exp"))
                .ToArray();
        }
        #endregion

        #region Private methods.
        void BindEstateTypesToExercises()
        {
            foreach (var item in estateTypes)
            {
                if (item.Value.itemType == EstateType.Subtype.Training || item.Value.itemType == EstateType.Subtype.ActiveDecor)
                {
                    var equipment = item.Value;

                    foreach (var exe in item.Value.exercisesIds)
                    {
                        if (exe >= 0)
                        {
                            Exercise exercise;
                            if (exercises.TryGetValue(exe, out exercise))
                            {
                                exercise.equipment = equipment;

                                if (exercise.tier == 3)
                                    equipment.stats = exercise.stats;
                            }
                            else
                            {
                                Debug.LogErrorFormat("Invalid exercise ID {0} int Equipment {1} ({2})", exe, item.Key, item.Value.locNameId);
                            }
                        }
                    }
                }
            }
        }

        void BindExercisesToEstateTypes()
        {
            foreach (var item in estateTypes)
            {
                Exercise[] itemExercises = new Exercise[item.Value.exercisesIds.Length];
                for (int i = 0; i < item.Value.exercisesIds.Length; i++)
                {
                    var id = item.Value.exercisesIds[i];
                    if (id >= 0)
                    {
                        exercises.TryGetValue(item.Value.exercisesIds[i], out itemExercises[i]);
                    }
                }

                item.Value.exercises = itemExercises;
            }
        }

        void BindShopDatatoEstates()
        {
            foreach (var item in estateTypes) 
            {
                if (item.Value.itemType == EstateType.Subtype.Training) 
                {
                    foreach (var shopItem in equipmentShop) 
                    {
                        var id = shopItem.GetInt("id");
                        if (id == item.Value.id) 
                        {
                            item.Value.price = new Cost
                                ((shopItem.GetString ("price_type") == "Coins")
                                    ? Cost.CostType.CoinsOnly
                                    : Cost.CostType.BucksOnly
                                , shopItem.GetInt ("price")
                                , estateTypes [id].levelRequirement);
                        }
                    }
                } 
                else if (item.Value.itemType == EstateType.Subtype.Decor) 
                {
                    foreach (var shopItem in decorShop) 
                    {
                        var id = shopItem.GetInt("id");
                        if (id == item.Value.id) 
                        {
                            item.Value.price = new Cost
                                ((shopItem.GetString ("price_type") == "Coins")
                                    ? Cost.CostType.CoinsOnly
                                    : Cost.CostType.BucksOnly
                                , shopItem.GetInt ("price")
                                , estateTypes [id].levelRequirement);
                        }
                    }
                }
            }
        }

        void LinkDailyBonusTables()
        {
            dailyBonusBoxes = new Dictionary<int, DailyBonusBox>();
            dailyBonusDays = new DailyBonusDay[_dailyBonusDays.Length];
            var boxCount = _dailyBonusDays[0].Keys.Count - 2; // minus id and level column

            for (int i = 0; i < dailyBonusDays.Length; i++)
            {
                var day = dailyBonusDays[i] = new DailyBonusDay();

                for (int j = 0; j < boxCount; j++)
                {
                    var boxName = "box" + (j + 1).ToString();
                    var row = _dailyBonusDays[i];

                    day.dayIdx = row.GetInt("day");
                    day.level = row.GetInt("level");

                    Assert.AreNotEqual(day.dayIdx, -1);
                    Assert.AreNotEqual(day.level, -1);

                    var boxId = row.GetInt(boxName);

                    if (boxId != -1)
                    {
                        if (!dailyBonusBoxes.ContainsKey(boxId))
                        {
                            dailyBonusBoxes.Add(boxId, new DailyBonusBox());
                        }

                        var box = dailyBonusBoxes[boxId];
                        BindRewardsToBox(box, boxId);


                        if (boxName == "box10")
                            day.mandatoryBox = box;
                        else
                            day.boxes.Add(box);
                    }
                    else
                    {
                        Debug.LogError("LinkDailyBonusTables no box id for " + boxName);
                    }
                }
            }
        }

        void BindRewardsToBox(DailyBonusBox box, int id)
        {
            if (box == null)
            {
                Debug.LogError("BindRewardsToBox : null box");
                return;
            }

            if (box.rewards.Count > 0)
                return;

            foreach (var reward in dailyBonusRewards)
            {
                if (reward.boxId == id)
                {
                    box.rewards.Add(reward);
                }
            }
        }

        void BindSportExerciseNames()
        {
            foreach (var exerc in sportExercises)
            {
                if (sportsmen.ContainsKey(exerc.trainTo))
                {
                    exerc.sportLocNameId = sportsmen[exerc.trainTo].locNameId;
                }
                else
                {
                    exerc.sportLocNameId = "";
                }
            }
        }
        #endregion
    }
}