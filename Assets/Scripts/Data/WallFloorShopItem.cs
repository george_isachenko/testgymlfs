﻿using UnityEngine;
using System.Collections;
using DataProcessing.CSV;

namespace Data
{
    public class WallFloorShopItem
    {
        [CSVName("id")]
        public int id;
        [CSVName("name")]
        public string name;
        [CSVName("")]
        public Cost price = new Cost();
        [CSVName("level")]
        public void Level(int value)
        {
            price.level = value;
        }

        [CSVName("price")]
        public void PriceValue(int value)
        {
            price.value = value;
        }

        [CSVName("price_type")]
        public void PriceType(string value)
        {
            switch (value)
            {
                case "Coins":
                price.type = Cost.CostType.CoinsOnly;
                break;
                case "Bucks":
                price.type = Cost.CostType.BucksOnly;
                break;
                default:
                Debug.LogError("wrong price type for WallFloorShopItem");
                break;
            }
        }
    }
}
