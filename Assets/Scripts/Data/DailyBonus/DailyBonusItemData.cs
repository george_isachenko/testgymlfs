﻿using Newtonsoft.Json;

namespace Data.DailyBonus
{
    [JsonObject]
    public class DailyBonusItemData
    {
        // data to save
        public Cost reward = new Cost();
        public Cost unlockPrice = new Cost();
        public bool closed = true;
        public bool paid = false;
        public bool isTop = false;
    }
}
