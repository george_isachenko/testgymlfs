﻿using Newtonsoft.Json;

namespace Data.DailyBonus
{
    [JsonObject]
    public class DailyBonusItemDataNew
    {
        // data to save
        public Cost reward = new Cost();
        public bool claimed = false;
        //public Cost unlockPrice = new Cost();
        //public bool closed = true;
        //public bool paid = false;
        //public bool isTop = false;
    }
}
