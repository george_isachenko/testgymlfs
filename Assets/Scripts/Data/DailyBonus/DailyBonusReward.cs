﻿using System;
using Data.PlayerProfile;
using DataProcessing.CSV;
using UnityEngine;

namespace Data.DailyBonus
{
    public class DailyBonusReward
    {
        // TABLE DATA
        public int          boxId;
        public int          dropRate;
        public int          countMin;
        public int          countMax;

        [CSVName("reward")]
        protected string    _reward;

        public Cost         reward      { get { return GetReward(); } }

        [CSVName("")]
        Cost GetReward()
        {
            if (_reward == "Coins")
            {
                return new Cost(Cost.CostType.CoinsOnly, randomCount, 0);
            }
            else if (_reward == "Bucks")
            {
                return new Cost(Cost.CostType.BucksOnly, randomCount, 0);
            }


            var types = Enum.GetValues(typeof(ResourceType)) as ResourceType[];

            foreach (var type in types)
            {
                if (type.ToString() == _reward)
                {
                    return new Cost(type, randomCount);
                }
            }

            Debug.LogError("DailyBonusReward wrong reward value");
            return new Cost();
        }


        // PROCEDURE DATA and VALIDATION
        private int randomCount { get { return UnityEngine.Random.Range(countMin, countMax + 1); } }
    }
}
