﻿using Newtonsoft.Json;

namespace Data.DailyBonus
{
    [JsonObject]
    public class DailyBonusData
    {
        public int dayIdx = 0;
        public bool acceptedByUser = false;
    }
}
