﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.DailyBonus
{
    public class DailyBonusDay
    {
        public int dayIdx = 1;
        public int level = 1;
        public DailyBonusBox mandatoryBox;


        public List<DailyBonusBox> boxes = new List<DailyBonusBox>();

        public DailyBonusBox randomBox { get { return GetRandomBox(); } }

        int boxIdx = 0;
        DailyBonusBox GetRandomBox()
        {
            if (boxIdx == 0)
            {
                boxes = boxes.OrderBy(a => Guid.NewGuid()).ToList();
            }

            var value = boxes[boxIdx];
            boxIdx += 1;
            boxIdx %= boxes.Count;

            return value;
        }

    }
}