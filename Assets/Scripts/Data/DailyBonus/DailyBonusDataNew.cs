﻿using Newtonsoft.Json;

namespace Data.DailyBonus
{
    [JsonObject]
    public class DailyBonusDataNew
    {
        public int dayNumber = 0;
        public bool acceptedByUser = false;
    }
}
