﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Data.DailyBonus
{
    public class DailyBonusBox
    {
        public List<DailyBonusReward> rewards = new List<DailyBonusReward>();

        public DailyBonusReward randomReward { get { return GetRandomReward(); } }

        private DailyBonusReward GetRandomReward()
        {
            Assert.IsTrue(rewards.Count > 0);
            int totalDropRate = 0;

            foreach (var reward in rewards)
                totalDropRate += reward.dropRate;

            int rnd = Random.Range(0, totalDropRate);

            int sum = 0;

            foreach (var reward in rewards)
            {
                sum += reward.dropRate;

                if (sum > rnd)
                    return reward;
            }

            Debug.LogError("DailyBonusReward ACHTUNG!!!");
            return null;
        }
    }
}