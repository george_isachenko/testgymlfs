﻿using System;
using System.Collections.Generic;

namespace MathHelpers
{
    // Iterates abstract non-square 2D array/matrix specified by X,Y dimensions from center by spiral until it reaches bounds.
    // Center has [0,0] coordinates and bounds have range [-X/2 .. X/2) and [-Y/2 .. Y/2] .
    // So, if you need always positive coordinates, you can add X/2 and Y/2 to results.
    // Should cover all cells in array/matrix. See http://stackoverflow.com/questions/398299/looping-in-a-spiral for details.
    //
    // Code is an adapted C algorithm from http://stackoverflow.com/a/1555236 .

    public static class Spiral2DIterator
    {
        #region Types.
        public struct Result
        {
            public int x;
            public int y;

            public Result (int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
        #endregion

        public static IEnumerable<Result> Iterate (int X, int Y)
        {
            if (X > 0 && Y > 0)
            {
                int x = 0;
                int y = 0;
                int i = 0;
                int dx = 0;
                int dy = -1;
                int t = Math.Max(X, Y);
                int maxI = t * t;
                int maxX = X / 2 - (X + 1) % 2;
                int maxY = Y / 2 - (Y + 1) % 2;
                int minX = - X / 2;
                int minY = - Y / 2;

                while (i < maxI)
                {
                    if ((minX <= x) && (x <= maxX) && (minY <= y) && (y <= maxY))
                    {
                        yield return new Result(x, y);
                    }

                    if ((x == y) || ((x < 0) && (x == -y)) || ((x > 0) && (x == 1 - y)))
                    {
                        t = dx;
                        dx = -dy;
                        dy = t;
                    }
                    x += dx;
                    y += dy;
                    i++;
                }
            }

            yield break;
        }
    }
}