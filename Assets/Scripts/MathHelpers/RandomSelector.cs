﻿using UnityEngine;

namespace MathHelpers
{
    public static class RandomSelector
    {
        public static int SelectFrom (params float[] probabilities)
        {
            if (probabilities != null)
            {
                float sumTotal = 0.0f;
                foreach (var prob in probabilities)
                {
                    sumTotal += prob;
                }

                var randomValue = sumTotal <= 1.0f ? Random.value : Random.Range(0.0f, sumTotal);
                var accum = 0.0f;

                for (int i = 0; i < probabilities.Length; i++)
                {
                    if (randomValue >= accum && randomValue < accum + probabilities[i])
                        return i;
                    accum += probabilities[i];
                }
            }
            return -1;
        }
    }
}