﻿using System;
using UnityEngine.Assertions;
using System.Collections;

namespace MathHelpers
{
    public struct OccupationMap
    {
        private int[,] map;

        public bool valid
        {
            get { return map != null; }
        }

        public OccupationMap(int sizeX, int sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            if (sizeX > 0 && sizeY > 0)
                map = new int[sizeX, sizeY];
            else
                map = null;
        }

        public void Reset()
        {
            if (map != null)
            {
                var sizeX = map.GetLength(0);
                var sizeY = map.GetLength(1);
                for (var i = 0; i < sizeX; i++)
                {
                    for (var j = 0; j < sizeY; j++)
                    {
                        map[i, j] = 0;
                    }
                }
            }
        }

        public void Resize(int sizeX, int sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            if (sizeX > 0 && sizeY > 0)
            {
                if (map != null)
                {
                    var newArray = new int[sizeX, sizeY];
                    var originalSizeX = map.GetLength(0);
                    var newSizeX = newArray.GetLength(0);
                    int minX = Math.Min(originalSizeX, newSizeX);
                    int minY = Math.Min(map.GetLength(1), newArray.GetLength(1));

                    for (int i = 0; i < minY; ++i)
                        Array.Copy(map, i * originalSizeX, newArray, i * newSizeX, minX);

                    map = newArray;
                }
                else 
                {
                    map = new int[sizeX, sizeY];
                }
            }
            else
            {
                map = null;
            }
        }

        public void Add(int x, int y, int sizeX, int sizeY)
        {
            if (map != null && sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        map[i, j] = map[i, j] + 1;
                    }
                }
            }
        }

        public void Remove(int x, int y, int sizeX, int sizeY)
        {
            if (map != null && sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        map[i, j] = map[i, j] - 1;
                        //Assert.IsTrue(map[i, j] >= 0);
                    }
                }
            }
        }

        public bool IsOccupied (int x, int y, int sizeX, int sizeY, int limit = 0)
        {
            if (map != null && sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        if (map[i, j] > limit)
                            return true;
                    }
                }
                return false;
            }
            else
                return false;
        }

        public int MaxOccupied (int x, int y, int sizeX, int sizeY)
        {
            int result = 0;

            if (map != null && sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        if (map[i, j].CompareTo(result) > 0)
                        {
                            result = map[i, j];
                        }
                    }
                }
            }
            return result;
        }
    }

    public struct OccupationBitMap
    {
        private BitArray map;
        private int sizeX_;
        private int sizeY_;

        public bool valid
        {
            get { return map != null; }
        }

        public int sizeX
        {
            get { return sizeX_; }
        }

        public int sizeY
        {
            get { return sizeY_; }
        }

        public OccupationBitMap(int sizeX, int sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            this.sizeX_ = sizeX;
            this.sizeY_ = sizeY;
            if (sizeX > 0 && sizeY > 0)
                map = new BitArray(sizeX * sizeY, false);
            else
                map = null;
        }

        public void Reset(bool value = false)
        {
            if (map != null)
            {
                map.SetAll(value);
            }
        }

        public bool Resize(int sizeX, int sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            if (sizeX > 0 && sizeY > 0)
            {
                if (sizeX != this.sizeX_ || sizeY != this.sizeY_)
                {
                    if (map != null)
                    {
                        var newMap = new BitArray(sizeX * sizeY, false);
                        int minX = Math.Min(this.sizeX_, sizeX);
                        int minY = Math.Min(this.sizeY_, sizeY);

                        for (int y = 0; y < minY; ++y)
                        {
                            for (int x = 0; x < minX; ++x)
                            {
                                newMap.Set(y * sizeX + x, map.Get(y * this.sizeX_ + x));
                            }
                        }

                        map = newMap;
                    }
                    else 
                    {
                        map = new BitArray(sizeX * sizeY, false);
                    }
                    this.sizeX_ = sizeX;
                    this.sizeY_ = sizeY;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                map = null;
                this.sizeX_ = 0;
                this.sizeY_ = 0;
            }
            return true;
        }

        public void Set(int x, int y, int areaSizeX, int areaSizeY, bool value)
        {
            if (map != null && areaSizeX > 0 && areaSizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + areaSizeX, sizeX_);
                var endY = Math.Min(y + areaSizeY, sizeY_);

                for (var yy = startY; yy < endY; yy++)
                {
                    for (var xx = startX; xx < endX; xx++)
                    {
                        map.Set(yy * sizeX_ + xx, value);
                    }
                }
            }
        }

        public bool IsOccupied (int x, int y, int areaSizeX, int areaSizeY)
        {
            if (map != null && areaSizeX > 0 && areaSizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + areaSizeX, sizeX_);
                var endY = Math.Min(y + areaSizeY, sizeY_);

                for (var yy = startY; yy < endY; yy++)
                {
                    for (var xx = startX; xx < endX; xx++)
                    {
                        if (map.Get(yy * sizeX_ + xx))
                            return true;
                    }
                }
                return false;
            }
            else
                return false;
        }
    }

/*
    public interface INumericPolicy<T>
    {
        T Zero();
        T One();
        T Add(T a, T b);
        T Subtract(T a, T b);
    }

    public class All:
        INumericPolicy<sbyte>,
        INumericPolicy<short>,
        INumericPolicy<int>,
        INumericPolicy<long>
        // add more INumericPolicy<> for different numeric types.
    {
        sbyte INumericPolicy<sbyte>.Zero() { return 0; }
        short INumericPolicy<short>.Zero() { return 0; }
        int INumericPolicy<int>.Zero() { return 0; }
        long INumericPolicy<long>.Zero() { return 0; }

        sbyte INumericPolicy<sbyte>.One() { return 1; }
        short INumericPolicy<short>.One() { return 1; }
        int INumericPolicy<int>.One() { return 1; }
        long INumericPolicy<long>.One() { return 1; }

        sbyte INumericPolicy<sbyte>.Add(sbyte a, sbyte b) { return (sbyte)(a + b); }
        short INumericPolicy<short>.Add(short a, short b) { return (short)(a + b); }
        int INumericPolicy<int>.Add(int a, int b) { return a + b; }
        long INumericPolicy<long>.Add(long a, long b) { return a + b; }

        sbyte INumericPolicy<sbyte>.Subtract(sbyte a, sbyte b) { return (sbyte)(a - b); }
        short INumericPolicy<short>.Subtract(short a, short b) { return (short)(a - b); }
        int INumericPolicy<int>.Subtract(int a, int b) { return a - b; }
        long INumericPolicy<long>.Subtract(long a, long b) { return a - b; }

        public static All P = new All();
    }

    static class Algorithms
    {
         public static P Zero<P>(this P p) where P: INumericPolicy<P>
         {
            //P p = default(P);
            return p.Zero();
         }

         public static P One<P>(this P p) where P: INumericPolicy<P>
         {
            //P p = default(P);
            return p.One();
         }

         public static T Add<P, T>(this P p, T t) where P: INumericPolicy<T>
         {
            return p.Add(t);
         }

         public static T Subtract<P, T>(this P p, T t) where P: INumericPolicy<T>
         {
            return p.Subtract(t);
         }
    }
*/

    // Currently works with following cell types: sbyte, short, int, long.
/*
    public struct OccupationMap<T> where T :
          struct, 
          IComparable, 
          IComparable<T>, 
          IConvertible, 
          IEquatable<T>, 
          IFormattable/ *,
          INumericPolicy<T>* /
    {
        // private static readonly T one = (T)Convert.ChangeType(1, typeof(T));
        private static readonly T dummy = default(T);

        private T[,] map;

        public OccupationMap(uint sizeX, uint sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            if (sizeX > 0 && sizeY > 0)
                map = new T[sizeX, sizeY];
            else
                map = null;
        }

        public void Reset()
        {
            if (map != null)
            {
                var sizeX = map.GetLength(0);
                var sizeY = map.GetLength(1);
                for (var i = 0; i < sizeX; i++)
                {
                    for (var j = 0; j < sizeY; j++)
                    {
                        map[i, j] = default(T);
                    }
                }
            }
        }

        public void Resize(uint sizeX, uint sizeY)
        {
            Assert.IsTrue(sizeX > 0);
            Assert.IsTrue(sizeY > 0);
            if (sizeX > 0 && sizeY > 0)
            {
                if (map != null)
                {
                    var newArray = new T[sizeX, sizeY];
                    var originalSizeX = map.GetLength(0);
                    var newSizeX = newArray.GetLength(0);
                    int minX = Math.Min(originalSizeX, newSizeX);
                    int minY = Math.Min(map.GetLength(1), newArray.GetLength(1));

                    for (int i = 0; i < minY; ++i)
                        Array.Copy(map, i * originalSizeX, newArray, i * newSizeX, minX);

                    map = newArray;
                }
                else 
                {
                    map = new T[sizeX, sizeY];
                }
            }
            else
            {
                map = null;
            }
        }

        public void Add(int x, int y, int sizeX, int sizeY)
        {
            if (sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        map[i, j] = dummy.Add(map[i, j], Algorithms.One());
                    }
                }
            }
        }

        public void Remove(int x, int y, int sizeX, int sizeY)
        {
            if (sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        map[i, j] = dummy.Subtract(map[i, j], default(T).One());
                        Assert.IsTrue(map[i, j].CompareTo(dummy.Zero()) >= 0);
                    }
                }
            }
        }

        public bool Check(int x, int y, int sizeX, int sizeY)
        {
            if (sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        if (map[i, j].CompareTo(dummy.Zero()) > 0)
                            return true;
                    }
                }
                return false;
            }
            else
                return false;
        }

        public T Max(int x, int y, int sizeX, int sizeY)
        {
            T result = default(T);

            if (sizeX > 0 && sizeY > 0)
            {
                var startX = Math.Max(x, 0);
                var startY = Math.Max(y, 0);

                var endX = Math.Min(x + sizeX, map.GetLength(0));
                var endY = Math.Min(y + sizeY, map.GetLength(0));

                for (var i = startX; i < endX; i++)
                {
                    for (var j = startY; j < endY; j++)
                    {
                        if (map[i, j].CompareTo(result) > 0)
                        {
                            result = map[i, j];
                        }
                    }
                }
            }
            return result;
        }
    }
*/
}