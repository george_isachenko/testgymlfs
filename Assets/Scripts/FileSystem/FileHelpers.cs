﻿using System;
using System.IO;

namespace FileSystem
{
    public static class FileHelpers
    {
        public static void SafeCommitFile(string sourceFileName, string destinationFileName, string backupFileName = null)
        {
            if (sourceFileName == null || sourceFileName == string.Empty)
                throw new ArgumentException("sourceFileName is null");

            if (destinationFileName == null || destinationFileName == string.Empty)
                throw new ArgumentException("destinationFileName is null");

            if (backupFileName == null)
                backupFileName = destinationFileName + ".bak";

            File.Delete(backupFileName);
            if (File.Exists(destinationFileName))
                File.Move(destinationFileName, backupFileName);

            try
            {
                File.Move(sourceFileName, destinationFileName);
            }
            catch (Exception)
            {
                if (File.Exists(backupFileName))
                    File.Move(backupFileName, destinationFileName); // Try to move old file back.
                throw;
            }
        }
    }
}