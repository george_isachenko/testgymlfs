﻿using System.IO;
using UnityEngine;

namespace FileSystem
{
    public static class ResourcePaths
    {
        public static class BalanceTables
        {
            public static readonly string rootFolder        = "BalanceTables/";

            public static readonly string equipmentShop             = rootFolder + "EquipmentShop";
            public static readonly string decor                     = rootFolder + "Decor";
            public static readonly string decorShop                 = rootFolder + "DecorShop";
            public static readonly string wallFloorShop             = rootFolder + "WallFloorShop";
            public static readonly string bankShop                  = rootFolder + "BankShop";
            public static readonly string resourceShop              = rootFolder + "ResourceShop";
            public static readonly string expand                    = rootFolder + "Expand";
            public static readonly string playerLevels              = rootFolder + "PlayerLevels";
            public static readonly string dailyBonusRewards         = rootFolder + "DailyBonusRewards";
            public static readonly string dailyBonusBoxes           = rootFolder + "DailyBonusBoxes";
            public static readonly string dailyBonusDays            = rootFolder + "DailyBonusDays";
            public static readonly string storageCapacity           = rootFolder + "StorageCapacity";

            public static readonly string estateTypes               = rootFolder + "Equipment";
            public static readonly string equipmentUpgrades         = rootFolder + "EquipmentUpgrades";
            public static readonly string costumes                  = rootFolder + "Costumes";
//          public static readonly string memberBodies              = rootFolder + "_memberBodies";
            public static readonly string exercises                 = rootFolder + "_actions";
            public static readonly string personFirstNames          = rootFolder + "FirstName";
            public static readonly string personLastNames           = rootFolder + "LastName";
            public static readonly string trainerNames              = rootFolder + "TrainerNames";
            public static readonly string clubStyle                 = rootFolder + "style";

            public static readonly string balance                   = rootFolder + "_balance";

#if UNITY_IOS
            public static readonly string iap                       = rootFolder + "IAP_iOS";
#elif UNITY_ANDROID
            public static readonly string iap                       = rootFolder + "IAP_Android";
#else
            public static readonly string iap                       = rootFolder + "IAP_Android";//null;
#endif

            public static readonly string sportResourceData         = rootFolder + "SportResourceShop";
            public static readonly string sportEquipData            = rootFolder + "SportEquipShop";
            public static readonly string sportExercises            = rootFolder + "SportExercises";
            public static readonly string sportsmen                 = rootFolder + "Sportsmen";
            public static readonly string sportsmenStorageCapacity  = rootFolder + "SportListCapacity";
            public static readonly string personsPhrases            = rootFolder + "PersonsPhrases";
            public static readonly string laneQuestsCount           = rootFolder + "LaneQuestsCount";
            public static readonly string capacityLevels            = rootFolder + "Capacity";
            public static readonly string localizationText          = rootFolder + "Localization";
            public static readonly string localizationLanguages     = rootFolder + "LocalizationLanguages";
            public static readonly string sportCostumes             = rootFolder + "SportCostumes";
            public static readonly string resourcesInfo             = rootFolder + "ResourcesInfo";
            public static readonly string promoPurchases            = rootFolder + "PromoPurchases";
            public static readonly string rankAchievements          = rootFolder + "RankAchievement";
            public static readonly string rankReward                = rootFolder + "RankReward";
            public static readonly string juiceRecipes              = rootFolder + "JuiceRecipes";
            public static readonly string fruitTrees                = rootFolder + "FruitTrees";
            public static readonly string trainerLevels             = rootFolder + "TrainerLevels";
            public static readonly string trainersUnlocks           = rootFolder + "TrainersUnlocks";
        }

        public static class Persistent
        {
            public static readonly string keysResFolder             = "Keys/";
            public static readonly string saveGameKeyResource       = "save";

            public static readonly string settings                  = "settings.json";

            public static readonly string saveGamesResFolder        = "Saves/";
            public static readonly string saveGameDemoResource      = "savegame_demo";

            private static readonly string saveGameBaseName         = "savegame.dat";
            private static readonly string saveGameFriendBaseName   = "savegame_friend.dat";

            public static string saveGameFullPath
            {
                get
                {
                    return string.Format("{0}{1}{2}", Application.persistentDataPath, Path.DirectorySeparatorChar, saveGameBaseName);
                }
            }

            public static string saveGameFriendFullPath
            {
                get
                {
                    return string.Format("{0}{1}{2}", Application.temporaryCachePath, Path.DirectorySeparatorChar, saveGameFriendBaseName);
                }
            }
        }

        public static class Network
        {
            public static readonly string networkDataKeyResource       = "net_data";
            public static readonly string networkDataFileName          = "net_data.dat";
#if DEBUG
            public static readonly string debugForceClientIDFileName  = "forced_client_id.txt";
#endif

            public static string networkDataFullPath
            {
                get
                {
                    return string.Format("{0}{1}{2}", Application.temporaryCachePath, Path.DirectorySeparatorChar, networkDataFileName);
                }
            }

#if DEBUG
            public static string debugForceClientIDFullPath
            {
                get
                {
                    return string.Format("{0}{1}{2}", Application.persistentDataPath, Path.DirectorySeparatorChar, debugForceClientIDFileName);
                }
            }
#endif
        }
    }
}