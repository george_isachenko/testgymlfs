﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

public class EditorLeanTweenStats : EditorWindow
{
    #region Static readonly data.
    private static readonly float[] widths = new float[] { 50, 80, 120, 100, 60, 60 };
    private static readonly string tweensFieldName = "tweens";
    #endregion

    #region Private static data.
    private static bool showStats;
    private static int tweensHashCode;
    private static EditorLeanTweenStats window;
    #endregion

    #region Unity Editor handlers.
    [MenuItem("Window/LeanTween/Toggle Stats")]
    public static void ToggleStats()
    {
        showStats = !showStats;

        if (showStats)
            SceneView.onSceneGUIDelegate += ShowTweenStats;
        else
            SceneView.onSceneGUIDelegate -= ShowTweenStats;

        Debug.LogFormat("LeanTween stats {0}.", showStats ? "enabled" : "disabled");
    }

    [MenuItem("Window/LeanTween/Show Tweens")]
    public static void ShowWindow()
    {
        if (EditorApplication.isPlaying && window == null)
        {
            window = GetWindow<EditorLeanTweenStats>();
            SceneView.onSceneGUIDelegate += CheckWindowUpdate;
            EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;

            window.titleContent = new GUIContent("Tweens");
            window.ShowUtility();
        }
    }
    #endregion

    #region Unity Editor API.
    void OnDestroy ()
    {
        Assert.IsNotNull(window);

        if (window != null)
        {
            SceneView.onSceneGUIDelegate -= CheckWindowUpdate;
            EditorApplication.playmodeStateChanged -= OnPlaymodeStateChanged;
            window = null;
        }
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();

        if (EditorApplication.isPlaying)
        {
            var tweens = GetTweensArray();
            if (tweens == null)
                return;

            GUILayout.Label(string.Format("Running: {0}, Maximum: {1}, Max search idx: {2}."
                , LeanTween.tweensRunning, LeanTween.maxSimulataneousTweens, LeanTween.maxSearch));

            EditorGUILayout.BeginHorizontal();
                
                EditorGUILayout.LabelField("#",         GUILayout.Width(widths[0]));
                EditorGUILayout.LabelField("ID",        GUILayout.Width(widths[1]));
                EditorGUILayout.LabelField("Type",      GUILayout.Width(widths[2]));
                EditorGUILayout.LabelField("Time",      GUILayout.Width(widths[3]));
                EditorGUILayout.LabelField("Speed",     GUILayout.Width(widths[4]));
                EditorGUILayout.LabelField("Direction", GUILayout.Width(widths[5]));

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i <= LeanTween.maxSearch; i++)
            {
                var descr = tweens[i];
                if (descr.toggle)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(i.ToString(),                                                    GUILayout.Width(widths[0]));
                    EditorGUILayout.LabelField(descr.id.ToString(),                                             GUILayout.Width(widths[1]));
                    EditorGUILayout.LabelField(descr.type.ToString(),                                           GUILayout.Width(widths[2]));
                    EditorGUILayout.LabelField(string.Format("{0:N2}/{1:N2}", descr.passed, descr.time),        GUILayout.Width(widths[3]));
                    EditorGUILayout.LabelField(descr.speed.ToString("N2"),                                      GUILayout.Width(widths[4]));
                    EditorGUILayout.LabelField(descr.direction.ToString("N2"),                                  GUILayout.Width(widths[5]));

                    EditorGUILayout.EndHorizontal();
                }
            }

            tweensHashCode = GetTweensStateHashCode();
        }

        EditorGUILayout.EndVertical();

    }
    #endregion

    #region Event handlers.
    private static void ShowTweenStats(SceneView sceneview)
    {
        if (Application.isPlaying)
        {
            var tweens = GetTweensArray();
            if (tweens == null)
                return;

            Handles.BeginGUI();

            using (var arasScope = new GUILayout.AreaScope(new Rect(5, 5, 200, 20)))
            {
                GUILayout.Label(string.Format("LeanTween: {0}/{1}", LeanTween.tweensRunning, LeanTween.maxSimulataneousTweens));
            }

            Handles.EndGUI();

            tweensHashCode = GetTweensStateHashCode();
        }
    }

    private static void CheckWindowUpdate(SceneView sceneview)
    {
        Assert.IsNotNull(window);

        if (window != null)
        {
            var hashCode = GetTweensStateHashCode();
            if (hashCode != tweensHashCode)
            {
                window.Repaint();
            }
        }
    }

    private static void OnPlaymodeStateChanged ()
    {
        if (!EditorApplication.isPlaying && window != null)
        {
            window.Close();
        }
    }
    #endregion

    #region Private functions.
    static int GetTweensStateHashCode()
    {
        var tweens = GetTweensArray();
        if (tweens == null)
            return 0;

        int hashCode = LeanTween.maxSearch.GetHashCode() ^ LeanTween.maxSimulataneousTweens.GetHashCode();

        for (int i = 0; i <= LeanTween.maxSearch; i++)
        {
            var descr = tweens[i];

            hashCode ^= descr.toggle.GetHashCode();
            hashCode ^= descr.id.GetHashCode();
            hashCode ^= descr.type.GetHashCode();
            hashCode ^= descr.passed.GetHashCode();
            hashCode ^= descr.time.GetHashCode();
            hashCode ^= descr.speed.GetHashCode();
            hashCode ^= descr.direction.GetHashCode();
        }

        return hashCode;
    }

    static LTDescr[] GetTweensArray()
    {
        var type = typeof(LeanTween);
        var fieldInfo = type.GetField(tweensFieldName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField);
        if (fieldInfo != null)
        {
            return (LTDescr[])fieldInfo.GetValue(null);
        }
        else
        {
            Debug.LogWarningFormat("Cannot find field '{0}' in a type {1}.{2}.", tweensFieldName, type.Name, type.Namespace);
        }
        return null;
    }
    #endregion
}
