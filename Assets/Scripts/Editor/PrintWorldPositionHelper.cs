﻿ using UnityEngine;
 using UnityEditor;
 
 public static class PrintWorldPositionHelper
 {
     [MenuItem("Window/Print World Position")]
     public static void PrintWorldPosition()
     {
         if (Selection.activeGameObject != null)
         {
             Debug.Log(string.Format("'{0}': World position: {1}.", Selection.activeGameObject.name, Selection.activeGameObject.transform.position.ToString("G7")));
         }
     }
 }