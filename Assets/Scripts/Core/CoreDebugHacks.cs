﻿namespace Core
{
    public static class CoreDebugHacks
    {
        public static bool assertRaisesException        = true;
        public static bool iapDumpProductReceipt        = false;
        public static bool iapConfirmReceiptValidation  = false;
    }
}
