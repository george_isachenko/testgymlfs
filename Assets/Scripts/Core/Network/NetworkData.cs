﻿using Core.SocialAPI;
using System;
using Newtonsoft.Json;
using UnityEngine;
using System.IO;
using Newtonsoft.Json.Bson;
using FileSystem;
using Core.Encryption;

namespace Core.Network
{
    // Note: Do not rename enum or its members without changing localization keys.
    public enum GenericNetworkStatus
    {
        ServerMaintenance       = -7,
        OutdatedVersion         = -6,
        TooHighVersion          = -5,

        NetworkUnavailable      = -4,
        ConnectionFailed        = -3,
        InvalidResponse         = -2,
        InvalidRequest          = -1,
        NotInitialized          = 0,

        OK                      = 1,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum LoginStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        Failed                  = 2,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum PurchaseVerificationStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        VerificationFailed      = 2,
        CannotPerformVerification = 3,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum GetProfileDataStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        FileIOError             = 2,
        NotFound                = 3,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum SendProfileDataStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        FileIOError             = 2,
        Conflict                = 3,
        NotFound                = 4,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum GetFriendsStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        NotExists               = 2,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum SendFriendsStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        Conflict                = 2,
        AnotherIDUsed           = 3,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum LinkSocialNetworkStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
        NotFound                = 2,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum SendScoresStatus
    {
        ServerMaintenance       = GenericNetworkStatus.ServerMaintenance,
        OutdatedVersion         = GenericNetworkStatus.OutdatedVersion,
        TooHighVersion          = GenericNetworkStatus.TooHighVersion,

        NetworkUnavailable      = GenericNetworkStatus.NetworkUnavailable,
        ConnectionFailed        = GenericNetworkStatus.ConnectionFailed,
        InvalidResponse         = GenericNetworkStatus.InvalidResponse,
        InvalidRequest          = GenericNetworkStatus.InvalidRequest,
        NotInitialized          = GenericNetworkStatus.NotInitialized,

        OK                      = GenericNetworkStatus.OK,
    }

    // Note: Do not rename enum or its members without changing localization keys.
    public enum LinkSocialNetworkAction
    {   
        Restore,
        SaveCurrent
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class NetworkBootData
    {
        [JsonProperty("client_version")]
        public int clientVersion = -1;

        [JsonProperty("server_version")]
        public int serverVersion = -1;

        [JsonProperty("data_name")]
        public string dataName = string.Empty;

        [JsonProperty("game_server")]
        public string gameServer = string.Empty;

        [JsonProperty("message")]
        public string maintenanceMessage = string.Empty;

        [JsonProperty("maintenance")]
        public bool maintenance = true;
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class LoginData
    {
        [JsonProperty("profile_id")]
        public string profileId = string.Empty;

        [JsonProperty("last_cid")]
        public string lastClientId = string.Empty;

        [JsonProperty("active_cid")]
        public string activeClientId = string.Empty;

        [JsonProperty("profile_saved")]
        public DateTime profileSaved = DateTime.MinValue; // Server time!

        [JsonProperty("link_saved")]
        public DateTime linkSaved = DateTime.MinValue; // Server time!

        [JsonProperty("level")]
        public int level = 0;

        [JsonProperty("experience")]
        public int experience = 0;
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class FriendInfo
    {
        [JsonProperty("profile_id")]
        public string profileId;

        [JsonProperty("title_network")]
        public NetworkType network;

        [JsonProperty("id_network")]
        public string networkId;

        [JsonProperty("name")]
        public string name;

        [JsonProperty("level")]
        public int level;
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class NetworkInfo
    {
        [JsonProperty("title_network")]
        public NetworkType network;

        [JsonProperty("id_network")]
        public string networkId;

        [JsonProperty("name")]
        public string name;

        [JsonProperty("email")]
        public string email;
    }

    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class ScoreboardPlayerInfo
    {
        [JsonProperty("pid")]
        public string profileId;

        [JsonProperty("name")]
        public string name;

        [JsonProperty("clevel")]
        public int characterLevel;  // Unused.

        [JsonProperty("plevel")]
        public int profileLevel;

        [JsonProperty("pop")]
        public int popularity;

        [JsonProperty("rank")]
        public int rank;
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class NetworkFileTimeStamps
    {
        [JsonProperty]
        public string profileId; // Profile id to which timestamp belongs.

        [JsonProperty]
        public DateTime fileTimeStamp;

        [JsonProperty]
        public DateTime uploadServerTimeStamp;

        [JsonProperty]
        public DateTime uploadClientTimeStamp;

        [JsonProperty]
        public DateTime downloadServerTimeStamp;

        [JsonProperty]
        public DateTime downloadClientTimeStamp;

        public NetworkFileTimeStamps ()
        {
            profileId = string.Empty; // Profile id to which 
            fileTimeStamp = DateTime.MinValue;
            uploadServerTimeStamp = DateTime.MinValue;
            uploadClientTimeStamp = DateTime.MinValue;
            downloadServerTimeStamp = DateTime.MinValue;
            downloadClientTimeStamp = DateTime.MinValue;
        }

        public NetworkFileTimeStamps
            ( string profileId
            , DateTime fileTimeStamp
            , DateTime uploadServerTimeStamp
            , DateTime uploadClientTimeStamp)
        {
            this.profileId = profileId;
            this.fileTimeStamp = fileTimeStamp.ToUniversalTime();
            this.uploadServerTimeStamp = uploadServerTimeStamp.ToUniversalTime();
            this.uploadClientTimeStamp = uploadClientTimeStamp.ToUniversalTime();
            this.downloadServerTimeStamp = DateTime.MinValue;
            this.downloadClientTimeStamp = DateTime.MinValue;
        }

        public NetworkFileTimeStamps
            ( string profileId
            , DateTime fileTimeStamp
            , DateTime uploadServerTimeStamp
            , DateTime uploadClientTimeStamp
            , DateTime downloadServerTimeStamp
            , DateTime downloadClientTimeStamp)
        {
            this.profileId = profileId;
            this.fileTimeStamp = fileTimeStamp.ToUniversalTime();
            this.uploadServerTimeStamp = uploadServerTimeStamp.ToUniversalTime();
            this.uploadClientTimeStamp = uploadClientTimeStamp.ToUniversalTime();
            this.downloadServerTimeStamp = downloadServerTimeStamp.ToUniversalTime();
            this.downloadClientTimeStamp = downloadClientTimeStamp.ToUniversalTime();
        }

        public override string ToString ()
        {
            return string.Format ("Profile ID: {0}, File TS: {1}, UP Server TS: {2}, UP Client TS: {3}, DOWN Server TS: {4}, DOWN Server TS: {5}"
                , profileId != null ? profileId : "null"
                , fileTimeStamp.ToString("o")
                , uploadServerTimeStamp.ToString("o")
                , uploadClientTimeStamp.ToString("o")
                , downloadServerTimeStamp.ToString("o")
                , downloadClientTimeStamp.ToString("o") );
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    class NetworkData
    {
        #region Private static/readonly data.
        public static readonly int             encKeySize              = 128; // In bits.
        #endregion

        #region Serializable properties.
        [JsonProperty]
        public string clientID                                      { get; private set; }

        [JsonProperty]
        public string profileId                                     { get; private set; }

        [JsonProperty]
        public int experienceMeta                                   { get; private set; }

        [JsonProperty]
        public int levelMeta                                        { get; private set; }

        [JsonProperty]
        public NetworkFileTimeStamps uploadedFileTimeStamps         { get; private set; }

        [JsonProperty]
        public NetworkFileTimeStamps downloadedFileTimeStamps       { get; private set; }
        #endregion

        #region Runtime properties.
        public bool dirty { get;  private set; }
        #endregion

        #region Constuctors.
        public NetworkData (string clientID)
        {
            this.clientID = clientID;
            levelMeta = 1;
        }

        [JsonConstructor]
        protected NetworkData ()
        {
            levelMeta = 1;
        }
        #endregion

        #region Public API.
        public void SetClientID (string clientID)
        {
            if (this.clientID != clientID)
            {
                this.clientID = clientID;
                dirty = true;
            }
        }

        public void SetProfileId (string profileId)
        {
            if (this.profileId != profileId)
            {
                this.profileId = profileId;
                dirty = true;
            }
        }

        public void SetExperienceMeta (int experienceMeta)
        {
            if (this.experienceMeta != experienceMeta)
            {
                this.experienceMeta = experienceMeta;
                dirty = true;
            }
        }

        public void SetLevelMeta (int levelMeta)
        {
            if (this.levelMeta != levelMeta)
            {
                this.levelMeta = levelMeta;
                dirty = true;
            }
        }

        public void SetUploadedFileTimeStamps (NetworkFileTimeStamps uploadedFileTimeStamps)
        {
            this.uploadedFileTimeStamps = uploadedFileTimeStamps;
            dirty = true;
        }

        public void SetDownloadedFileTimeStamps (NetworkFileTimeStamps downloadedFileTimeStamps)
        {
            this.downloadedFileTimeStamps = downloadedFileTimeStamps;
            dirty = true;
        }

        public void Save (string filePath, byte[] ekBytes, JsonSerializerSettings jsonSerializerSettings)
        {
            if (dirty)
            {
                try
                {
                    var tmpPath = filePath + ".tmp";

                    using (var fileStream = File.Create(tmpPath))
                    {
                        using (var cStream = EH.CE(ekBytes, fileStream, encKeySize))
                        {
                            using (var jsonWriter = new BsonWriter(cStream))
                            {
                                jsonWriter.DateTimeKindHandling = DateTimeKind.Utc;

                                var jsonSerializer = JsonSerializer.Create(jsonSerializerSettings);
                                jsonSerializer.Serialize(jsonWriter, this);
                            }
                        }
                    }

                    FileHelpers.SafeCommitFile (tmpPath, filePath);
                    dirty = false;
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);

                    throw;
                }
            }
        }

        public static NetworkData Load (string filePath, byte[] ekBytes, JsonSerializerSettings jsonSerializerSettings)
        {
            NetworkData result = null;

            try
            {
                Debug.LogFormat("Network data file path: '{0}'", filePath);

                var fi = new FileInfo(filePath);
                if (fi.Exists)
                {
                    if (fi.Length > encKeySize / 8)
                    {
                        using (var fileStream = File.OpenRead(filePath))
                        {
                            using (var cStream = EH.CD(ekBytes, fileStream, encKeySize))
                            {
                                using (var memoryStream = new MemoryStream((int)fileStream.Length))
                                {
                                    // We're using reading all encrypted data from CryptoStream byte by byte
                                    // because there's a strange bug with a CryptoStream and BSonReader that causes 
                                    // "ArgumentException: Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."
                                    // on reading. When using MemoryStream this doesn't happens.

                                    int b = -1;
                                    while (true)
                                    {
                                        b = cStream.ReadByte();
                                        if (b < 0)
                                            break;

                                        memoryStream.WriteByte((byte)b);
                                    }

                                    memoryStream.Seek(0, SeekOrigin.Begin);

                                    using (var jsonReader = new BsonReader(memoryStream, false, DateTimeKind.Utc))
                                    {
                                        var jsonSerializer = JsonSerializer.Create(jsonSerializerSettings);
                                        result = jsonSerializer.Deserialize<NetworkData>(jsonReader);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat("Network data file '{0}' is too short, ignoring it: {1}.", filePath, fi.Length);
                    }
                }
            }
            catch (Exception ex)
            {
            	Debug.LogException(ex);
                throw;
            }
            
            return result;
        }
        #endregion
    }
}