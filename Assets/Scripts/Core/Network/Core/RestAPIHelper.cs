﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

#if BESTHTTP_ENABLE
    using BestHTTP;
#else
    #warning BestHTTP is not enabled, UnityWebRequest will be used! Please define BESTHTTP_ENABLE symbol in "Scripting Define Symbols" in Player Settings.

    using UnityEngine.Networking;
#endif

namespace Core.Network.Core
{
    public static class RestAPIHelper
    {
        #region Public static API.
#if BESTHTTP_ENABLE
        public static HTTPRequest CreateRequest
            ( string server
            , RestRequestInfo requestInfo
            , object requestData
            , JsonSerializerSettings jsonSerializerSettings
            , params object[] apiParameters)
#else
        public static HTTPRequest CreateRequest
            ( string server
            , RestRequestInfo requestInfo
            , object requestData
            , JsonSerializerSettings jsonSerializerSettings
            , params object[] apiParameters)
#endif
        {
            string urlString = (apiParameters != null && apiParameters.Length > 0)
                ? BuildURLString(server, requestInfo.apiURL, apiParameters)
                : string.Format("{0}{1}", server, requestInfo.apiURL);

            byte[] contentBytes = EncodeContentData(ref requestData, requestInfo, jsonSerializerSettings);

#if BESTHTTP_ENABLE
            var request = SetMandatoryRequestHeaders
                (new HTTPRequest
                    ( new Uri(urlString)
                    , requestInfo.method));

            if (contentBytes != null)
            {
                request.SetHeader("Content-Type", requestInfo.requestContentType);
                request.RawData = contentBytes;
            }
#else
            var expectResponseContent = (requestInfo.responseHandling != null && requestInfo.responseHandling.Any((x) => x.responseDataType != null));

            var request = SetMandatoryRequestHeaders
                (new UnityWebRequest
                    ( urlString
                    , requestInfo.method
                    , expectResponseContent ? new DownloadHandlerBuffer() : null
                    , (contentBytes != null) ? new UploadHandlerRaw(contentBytes) : null ));

            if (contentBytes != null)
            {
                request.SetRequestHeader("Content-Type", requestInfo.requestContentType);
            }
#endif

            return request;
        }

#if BESTHTTP_ENABLE
        public static StatusT ProcessRequestResponse<StatusT>
            ( HTTPRequest request
            , RestRequestInfo requestInfo
            , JsonSerializerSettings jsonSerializerSettings
            , out object responseData ) where StatusT : struct
#else
        public static StatusT ProcessRequestResponse<StatusT>
            ( UnityWebRequest request
            , RestRequestInfo requestInfo
            , JsonSerializerSettings jsonSerializerSettings
            , out object responseData ) where StatusT : struct
#endif
        {
            responseData = null;

            int resultStatus = (int)GenericNetworkStatus.NotInitialized;

#if BESTHTTP_ENABLE
            if (request.State == HTTPRequestStates.Finished ||
                request.State == HTTPRequestStates.Error ||
                request.State == HTTPRequestStates.Aborted ||
                request.State == HTTPRequestStates.ConnectionTimedOut ||
                request.State == HTTPRequestStates.TimedOut)
            {
                Debug.LogFormat("Processing response for REST request {0}. State: '{1}', Response code: {2}."
                    , requestInfo
                    , request.State
                    , request.Response != null ? request.Response.StatusCode : -1 );

                if (request.State == HTTPRequestStates.Error ||
                    request.State == HTTPRequestStates.Aborted ||
                    request.State == HTTPRequestStates.ConnectionTimedOut ||
                    request.State == HTTPRequestStates.TimedOut)
                {
                    Debug.LogWarningFormat("Failed to perform REST request {0}. State: '{1}', Response code: {2}, Error message: '{3}', Exception:\n {4}."
                        , requestInfo
                        , request.State
                        , request.Response != null ? request.Response.StatusCode : -1
                        , request.Response != null ? request.Response.Message : "<none>"
                        , request.Exception != null ? request.Exception.ToString() : "<null>" );

                    resultStatus = (int)GenericNetworkStatus.ConnectionFailed;
                }
                else
                {
                    Assert.IsNotNull(request.Response);

                    if (request.Response.StatusCode == (int)HttpStatusCode.BadRequest)
                    {
                        resultStatus = (int)GenericNetworkStatus.InvalidRequest;

                        Debug.LogErrorFormat("REST request {0}. Server refused our request. Response code: {1}, Error message: '{2}'."
                            , requestInfo, request.Response.StatusCode, request.Response.Message);
                    }
                    else
                    {
                        if (requestInfo.responseHandling == null || requestInfo.responseHandling.Length == 0)
                        {   // Do not care about result (except OK) at all.
                            if (request.Response.StatusCode == (int)HttpStatusCode.OK)
                            {
                                resultStatus = (int)GenericNetworkStatus.OK; 
                            }
                            else
                            {
                                Debug.LogErrorFormat("REST request {0}. Unexpected response from server. Response code: {1}, Error message: '{2}'."
                                    , requestInfo, request.Response.StatusCode, request.Response.Message);

                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                            }
                        }
                        else
                        {
                            var handlingIdx = Array.FindIndex(requestInfo.responseHandling, x => x.responseCode == request.Response.StatusCode);
                            if (handlingIdx >= 0)
                            {
                                var rh = requestInfo.responseHandling[handlingIdx];

                                if (rh.responseDataType != null)
                                {
                                    if (request.Response.Data != null &&
                                        request.Response.Data.Length > 0)
                                    {
                                        if (rh.contentType == null || request.Response.HasHeaderWithValue("Content-Type", rh.contentType))
                                        {
                                            if (rh.responseDataType == typeof(byte[]))
                                            {
                                                try
                                                {
                                                    responseData = Convert.FromBase64String(request.Response.DataAsText);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to decode Base64 server's response data. Response code: {1}, Error message: '{2}', Exception:\n{3}."
                                                        , requestInfo, request.Response.StatusCode, request.Response.Message, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else if (rh.responseDataType.IsPrimitive)
                                            {
                                                try
                                                {
                                                    responseData = Convert.ChangeType(request.Response.DataAsText, rh.responseDataType);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to convert response data to type {1}. Response code: {2}, Error message: '{3}', Exception:\n{4}."
                                                        , requestInfo, rh.responseDataType, request.Response.StatusCode, request.Response.Message, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else if (jsonSerializerSettings != null)
                                            {
                                                try
                                                {
                                                    responseData = JsonConvert.DeserializeObject (request.Response.DataAsText, rh.responseDataType, jsonSerializerSettings);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to parse JSON response data to type {1}. Response code: {2}, Error message: '{3}', Exception:\n{4}."
                                                        , requestInfo, rh.responseDataType, request.Response.StatusCode, request.Response.Message, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else
                                            {
                                                Debug.LogErrorFormat("REST request {0}. Unsupported response data type {1}. Response code: {2}."
                                                    , requestInfo, rh.responseDataType, request.Response.StatusCode);
                                                	
                                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                            }
                                        }
                                        else
                                        {
                                            Debug.LogErrorFormat("REST request {0}. Wrong response Content-Type header. Response code: {1}. Expected Content-Type: '{2}', server returned: '{3}'."
                                                , requestInfo, request.Response.StatusCode, rh.contentType, request.Response.GetFirstHeaderValue("Content-Type"));

                                            resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogErrorFormat("REST request {0}. Invalid response from server: empty content when it expected. Response code: {1}, Error message: '{2}'."
                                            , requestInfo, request.Response.StatusCode, request.Response.Message);

                                        resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                    }
                                }
                                else
                                {
                                    resultStatus = rh.statusCode;
                                }
                            }
                            else
                            {
                                Debug.LogErrorFormat("REST request {0}. Unexpected response from server. Response code: {1}, Error message: '{2}'."
                                    , requestInfo, request.Response.StatusCode, request.Response.Message);
                                
                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat("REST request: {0}. Trying to process request that is not complete yet! Current state: {1}."
                    , requestInfo
                    , request.State );
            }
#else
            if (request.isDone)
            {
                Debug.LogFormat("Processing REST request {0}. Error status: {1}, Response code: {2}."
                    , requestInfo, request.isError, request.responseCode );

                // NOTE: On iOS it can set 'isError' flag to true even for perfectly valid requests with 'responseCode' set to 4xx codes
                // though isError is reserved for 'system errors' only when request cannot be even performed
                // (no network, no connection, DNS errors etc). So, we're also checking responseCode to be valid and if it's 
                // higher than lower valid HTTP code (100) then isError is ignored.

                if (request.isError && request.responseCode < (int)HttpStatusCode.Continue)
                {
                    Debug.LogWarningFormat("REST request {0}. Failed to perform request. Response code: {1}, Error message: '{2}'."
                        , requestInfo, request.responseCode, request.error);

                    resultStatus = (int)GenericNetworkStatus.ConnectionFailed;
                }
                else
                {
                    if (request.responseCode == (int)HttpStatusCode.BadRequest)
                    {
                        resultStatus = (int)GenericNetworkStatus.InvalidRequest;

                        Debug.LogErrorFormat("REST request {0}. Server refused our request. Response code: {1}, Error message: '{2}'."
                            , requestInfo, request.responseCode, request.error);
                    }
                    else
                    {
                        if (requestInfo.responseHandling == null || requestInfo.responseHandling.Length == 0)
                        {   // Do not care about result (except OK) at all.
                            if (request.responseCode == (int)HttpStatusCode.OK)
                            {
                                resultStatus = (int)GenericNetworkStatus.OK; 
                            }
                            else
                            {
                                Debug.LogErrorFormat("REST request {0}. Unexpected response from server. Response code: {1}, Error message: '{2}'."
                                    , requestInfo, request.responseCode, request.error);

                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                            }
                        }
                        else
                        {
                            var handlingIdx = Array.FindIndex(requestInfo.responseHandling, x => x.responseCode == request.responseCode);
                            if (handlingIdx >= 0)
                            {
                                var rh = requestInfo.responseHandling[handlingIdx];

                                if (rh.responseDataType != null)
                                {
                                    if (request.downloadHandler != null &&
                                        request.downloadHandler.text != null &&
                                        request.downloadHandler.text != string.Empty)
                                    {
                                        if (rh.contentType == null || rh.contentType == request.GetResponseHeader("Content-Type"))
                                        {
                                            if (rh.responseDataType == typeof(byte[]))
                                            {
                                                try
                                                {
                                                    responseData = Convert.FromBase64String(request.downloadHandler.text);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to decode Base64 server's response data. Response code: {1}, Error message: '{2}', Exception:\n{3}."
                                                        , requestInfo, request.responseCode, request.error, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else if (rh.responseDataType.IsPrimitive)
                                            {
                                                try
                                                {
                                                    responseData = Convert.ChangeType(request.downloadHandler.text, rh.responseDataType);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to convert response data to type {1}. Response code: {2}, Error message: '{3}', Exception:\n{4}."
                                                        , requestInfo, rh.responseDataType, request.responseCode, request.error, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else if (jsonSerializerSettings != null)
                                            {
                                                try
                                                {
                                                    responseData = JsonConvert.DeserializeObject (request.downloadHandler.text, rh.responseDataType, jsonSerializerSettings);
                                                    resultStatus = rh.statusCode;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Debug.LogErrorFormat("REST request {0}. Failed to parse JSON response data to type {1}. Response code: {2}, Error message: '{3}', Exception:\n{4}."
                                                        , requestInfo, rh.responseDataType, request.responseCode, request.error, ex.Message);
                                                	
                                                    resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                                }
                                            }
                                            else
                                            {
                                                Debug.LogErrorFormat("REST request {0}. Unsupported response data type {1}. Response code: {2}."
                                                    , requestInfo, rh.responseDataType, request.responseCode);
                                                	
                                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                            }
                                        }
                                        else
                                        {
                                            Debug.LogErrorFormat("REST request {0}. Wrong response Content-Type header. Response code: {1}. Expected Content-Type: '{2}', server returned: '{3}'."
                                                , requestInfo, request.responseCode, rh.contentType, request.GetResponseHeader("Content-Type"));

                                            resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogErrorFormat("REST request {0}. Invalid response from server: empty content when it expected. Response code: {1}, Error message: '{2}'."
                                            , requestInfo, request.responseCode, request.error);

                                        resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                                    }
                                }
                                else
                                {
                                    resultStatus = rh.statusCode;
                                }
                            }
                            else
                            {
                                Debug.LogErrorFormat("REST request {0}. Unexpected response from server. Response code: {1}, Error message: '{2}'."
                                    , requestInfo, request.responseCode, request.error);
                                
                                resultStatus = (int)GenericNetworkStatus.InvalidResponse;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat("REST request: {0}. Trying to process request that is not complete yet! ", requestInfo);
            }
#endif

            return (StatusT)(object)resultStatus;
        }

#if BESTHTTP_ENABLE
        public static StatusT ProcessRequestResponse<StatusT>
            ( HTTPRequest request
            , RestRequestInfo requestInfo) where StatusT : struct
#else
        public static StatusT ProcessRequestResponse<StatusT>
            ( UnityWebRequest request
            , RestRequestInfo requestInfo) where StatusT : struct
#endif
        {
            object dummy;
            return ProcessRequestResponse<StatusT>(request, requestInfo, null, out dummy);
        }
        #endregion

        #region Private static methods.
        static string BuildURLString (string server, string api, params object[] parameters)
        {
            var stringBuilder = (new StringBuilder(server, 128)).Append(api);
            foreach (var param in parameters)
            {
                stringBuilder.Append('/');
                stringBuilder.Append(param);
            }

            return stringBuilder.ToString();
        }

#if BESTHTTP_ENABLE
        static HTTPRequest SetMandatoryRequestHeaders(HTTPRequest request)
#else
        static UnityWebRequest SetMandatoryRequestHeaders(UnityWebRequest request)
#endif
        {
#if BESTHTTP_ENABLE
            #if ! BESTHTTP_DISABLE_CACHING
                request.DisableCache = true;
            #endif
#else
            request.SetRequestHeader("Cache-Control", "no-cache");
#endif

            return request;
        }

        static byte[] EncodeContentData (ref object requestData, RestRequestInfo requestInfo, JsonSerializerSettings jsonSerializerSettings)
        {
            if (requestData != null && requestInfo.requestContentType != null)
            {
                var requestDataType = requestData.GetType();

                if (requestDataType == typeof(string))
                {
                    return Encoding.UTF8.GetBytes(requestData.ToString());
                }
                else if (requestDataType == typeof(byte[]))
                {
                    return (byte[])requestData;
                }
                else if (requestDataType.IsPrimitive)
                {
                    return Encoding.UTF8.GetBytes(requestData.ToString());
                }
                else if (jsonSerializerSettings != null)
                {   // Encode as JSON.
                    var jsonString = JsonConvert.SerializeObject(requestData, jsonSerializerSettings);
                    return Encoding.UTF8.GetBytes(jsonString);
                }
            }

            return null;
        }
        #endregion
    }
}