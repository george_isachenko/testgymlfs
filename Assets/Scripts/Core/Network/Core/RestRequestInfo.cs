﻿using System;

#if BESTHTTP_ENABLE
    using BestHTTP;
#else
    #warning BestHTTP is not enabled, UnityWebRequest will be used! Please define BESTHTTP_ENABLE symbol in "Scripting Define Symbols" in Player Settings.
#endif

namespace Core.Network.Core
{
    public class RestRequestInfo
    {
        #region Constants.
        public const string contentTypeApplicationJson          = "application/json";
        public const string contentTypeApplicationOctetStream   = "application/octet-stream";
        public const string contentTypeTextPlain                = "text/plain";
        #endregion

        #region Types.
        public struct ResponseHandling
        {
            public int responseCode                 { get; private set; }
            public int statusCode                   { get; private set; }
            public string contentType               { get; private set; }
            public Type responseDataType            { get; private set; }

            public ResponseHandling                 ( int responseCode, int statusCode )
            {
                this.responseCode = responseCode;
                this.statusCode = statusCode;
                this.contentType = null;
                this.responseDataType = null;
            }

            public ResponseHandling                 ( int responseCode, int statusCode, Type responseDataType, string contentType = contentTypeApplicationJson )
            {
                this.responseCode = responseCode;
                this.statusCode = statusCode;
                this.contentType = contentType;
                this.responseDataType = responseDataType;
            }
        }
        #endregion

        #region Public properties.
#if BESTHTTP_ENABLE
        public HTTPMethods method                   { get; private set; }
#else
        public string method                        { get; private set; }
#endif
        public string apiURL                        { get; private set; }
        public string requestContentType            { get; private set; }
        public ResponseHandling[] responseHandling  { get; private set; }
        #endregion

        #region Constructors.
#if BESTHTTP_ENABLE
        public RestRequestInfo ( HTTPMethods method, string apiURL )
#else
        public RestRequestInfo ( string method, string apiURL )
#endif
        {
            this.method = method;
            this.apiURL = apiURL;
            requestContentType = null;
            responseHandling = null;
        }

#if BESTHTTP_ENABLE
        public RestRequestInfo ( HTTPMethods method, string apiURL, string requestContentType )
#else
        public RestRequestInfo ( string method, string apiURL, string requestContentType )
#endif
        {
            this.method = method;
            this.apiURL = apiURL;
            this.requestContentType = requestContentType;
            responseHandling = null;
        }

#if BESTHTTP_ENABLE
        public RestRequestInfo ( HTTPMethods method, string apiURL, params ResponseHandling[] responseHandling )
#else
        public RestRequestInfo ( string method, string apiURL, params ResponseHandling[] responseHandling )
#endif
        {
            this.method = method;
            this.apiURL = apiURL;
            requestContentType = null;
            this.responseHandling = responseHandling;
        }

#if BESTHTTP_ENABLE
        public RestRequestInfo ( HTTPMethods method, string apiURL, string requestContentType, params ResponseHandling[] responseHandling )
#else
        public RestRequestInfo ( string method, string apiURL, string requestContentType, params ResponseHandling[] responseHandling )
#endif
        {
            this.method = method;
            this.apiURL = apiURL;
            this.requestContentType = requestContentType;
            this.responseHandling = responseHandling;
        }
        #endregion

        #region System.Object virtual interface.
        public override string ToString ()
        {
            return string.Format ("{0} '{1}'", method, apiURL);
        }
        #endregion

    }
}
