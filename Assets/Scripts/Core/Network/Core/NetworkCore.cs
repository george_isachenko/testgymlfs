﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using Core.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;
using UnityEngine.Assertions;

#if BESTHTTP_ENABLE
    using BestHTTP;
#else
    #warning BestHTTP is not enabled, UnityWebRequest will be used! Please define BESTHTTP_ENABLE symbol in "Scripting Define Symbols" in Player Settings.

    using UnityEngine.Networking;
#endif

namespace Core.Network.Core
{
    public delegate void OnNetworkStatusUpdated         (GenericNetworkStatus status);

    public delegate void OnBootComplete                 (GenericNetworkStatus status);
    public delegate void OnLoginComplete                (LoginStatus status, LoginData loginData, bool isNewClientID);
    public delegate void OnPurchaseVerificationComplete (PurchaseVerificationStatus status);
    public delegate void OnGetProfileDataComplete       (GetProfileDataStatus status, NetworkFileTimeStamps downloadTimeStamps);
    public delegate void OnGetFriendProfileDataComplete (GetProfileDataStatus status);
    public delegate void OnSendProfileDataComplete      (SendProfileDataStatus status, NetworkFileTimeStamps uploadTimeStamps);
    public delegate void OnGetFriendsComplete           (GetFriendsStatus status, NetworkInfo[] networks, FriendInfo[] friends);
    public delegate void OnSendFriendsComplete          (SendFriendsStatus status);
    public delegate void OnLinkSocialNetworkComplete    (LinkSocialNetworkStatus status);
    public delegate void OnSendScoresComplete           (SendScoresStatus status, ScoreboardPlayerInfo[] top, ScoreboardPlayerInfo[] local);

    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Core/Network/Network Core")]
    public class NetworkCore : MonoBehaviour
    {
#if BESTHTTP_ENABLE
        public static readonly HTTPMethods MethodGET        = HTTPMethods.Get;
        public static readonly HTTPMethods MethodPOST       = HTTPMethods.Post;
        public static readonly HTTPMethods MethodHEAD       = HTTPMethods.Head;
        public static readonly HTTPMethods MethodPUT        = HTTPMethods.Put;
        public static readonly HTTPMethods MethodDELETE     = HTTPMethods.Delete;
#else
        public static readonly string MethodGET             = UnityWebRequest.kHttpVerbGET;
        public static readonly string MethodPOST            = UnityWebRequest.kHttpVerbPOST;
        public static readonly string MethodHEAD            = UnityWebRequest.kHttpVerbHEAD;
        public static readonly string MethodPUT             = UnityWebRequest.kHttpVerbPUT;
        public static readonly string MethodDELETE          = UnityWebRequest.kHttpVerbDELETE;
#endif

        #region Types.
        struct JobData
        {
            public IEnumerator job;
            public Action cancelAction;

            public JobData (IEnumerator job, Action cancelAction)
            {
                this.job = job;
                this.cancelAction = cancelAction;
            }
        }

        struct LoginApiPostRequestData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            [JsonProperty("switch")]
            public bool switchToSelf;

            public LoginApiPostRequestData (bool switchToSelf)
            {
                this.switchToSelf = switchToSelf;
            }
        }

        class ProfileApiGetResponseData
        {
            public string save;
            public DateTime client_time;
            public DateTime profile_saved;

            public ProfileApiGetResponseData(string save, DateTime client_time, DateTime profile_saved)
            {
                this.save = save;
                this.client_time = client_time;
                this.profile_saved = profile_saved;
            }

            public ProfileApiGetResponseData()
            {
                save = string.Empty;
                client_time = DateTime.MinValue;
                profile_saved = DateTime.MinValue;
            }
        }

        class ProfileApiPostRequestData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            public int level;
            public int experience;
            public string save;
            public DateTime client_time;

            public ProfileApiPostRequestData()
            {
                level = 0;
                experience = 0;
                save = string.Empty;
                client_time = DateTime.MinValue;
            }

            public ProfileApiPostRequestData(int level, int experience)
            {
                this.level = level;
                this.experience = experience;
                this.save = string.Empty;
                this.client_time = DateTime.UtcNow;
            }
        }

        class ProfileApiPostResponseData
        {
            public DateTime profile_saved;

            public ProfileApiPostResponseData(DateTime profile_saved)
            {
                this.profile_saved = profile_saved;
            }

            public ProfileApiPostResponseData()
            {
                profile_saved = DateTime.MinValue;
            }
        }

        class FriendsApiGetResponseData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            public FriendInfo[] friends;
            public NetworkInfo[] networks;

            public FriendsApiGetResponseData(FriendInfo[] friends, NetworkInfo[] networks)
            {
                this.friends = friends;
                this.networks = networks;
            }

            public FriendsApiGetResponseData()
            {
                friends = new FriendInfo[0];
                networks = new NetworkInfo[0];
            }
        }

        class FriendsApiPostRequestData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            public string id;
            public string name;
            public string email;
            public string[] friends;

            public FriendsApiPostRequestData(string id, string name, string email, string[] friends)
            {
                this.id = id;
                this.name = name;
                this.friends = friends;
                this.email = email;
            }

            public FriendsApiPostRequestData()
            {
                id = string.Empty;
                name = string.Empty;
                email = string.Empty;
                friends = new string[0];
            }
        }

        class ScoreApiPostRequestData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            public string user_id;
            public string name;
            public int character_level;  // Unused.
            public int profile_level;
            public int popularity;

            public ScoreApiPostRequestData(string user_id, string name, int character_level, int profile_level, int popularity)
            {
                this.user_id = user_id;
                this.name = name;
                this.character_level = character_level;
                this.profile_level = profile_level;
                this.popularity = popularity;
            }

            public ScoreApiPostRequestData()
            {
                this.user_id = string.Empty;
                this.name = string.Empty;
                this.character_level = 0;
                this.profile_level = 0;
                this.popularity = 0;
            }
        }

        class ScoreApiPostResponseData
        {   // NOTE! Do not rename fields: they're mapped to JSON.
            public ScoreboardPlayerInfo[] top;
            public ScoreboardPlayerInfo[] local;

            public ScoreApiPostResponseData(ScoreboardPlayerInfo[] top, ScoreboardPlayerInfo[] local)
            {
                this.top = top;
                this.local = local;
            }

            public ScoreApiPostResponseData()
            {
                this.top = null;
                this.local = null;
            }
        }
        #endregion

        #region Public static/readonly data.
        #endregion

        #region Private static/readonly data.
        private static readonly RestRequestInfo apiLoginPOST = new RestRequestInfo
            ( MethodPOST
            , "/api/login"
            , RestRequestInfo.contentTypeApplicationJson
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)LoginStatus.OK, typeof(LoginData))
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.Created,           (int)LoginStatus.OK, typeof(LoginData)) );

        private static readonly RestRequestInfo apiProfilePOST = new RestRequestInfo
            ( MethodPOST
            , "/api/profile"
            , RestRequestInfo.contentTypeApplicationJson
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)SendProfileDataStatus.OK, typeof(ProfileApiPostResponseData))
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)SendProfileDataStatus.NotFound) 
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.Conflict,          (int)SendProfileDataStatus.Conflict) );

        private static readonly RestRequestInfo apiProfileGET = new RestRequestInfo
            ( MethodGET
            , "/api/profile"
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)GetProfileDataStatus.OK, typeof(ProfileApiGetResponseData))
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)GetProfileDataStatus.NotFound) );

        private static readonly RestRequestInfo apiProfileFriendGET = new RestRequestInfo
            ( MethodGET
            , "/api/profile/friend"
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)GetProfileDataStatus.OK, typeof(byte[]), null)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)GetProfileDataStatus.NotFound) );

        private static readonly RestRequestInfo apiFriendsGET = new RestRequestInfo
            ( MethodGET
            , "/api/friends"
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)GetFriendsStatus.OK, typeof(FriendsApiGetResponseData))
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)GetFriendsStatus.NotExists) );

        private static readonly RestRequestInfo apiFriendsPOST = new RestRequestInfo
            ( MethodPOST
            , "/api/friends"
            , RestRequestInfo.contentTypeApplicationJson
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)SendFriendsStatus.OK)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.Created,           (int)SendFriendsStatus.Conflict)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.Forbidden,         (int)SendFriendsStatus.AnotherIDUsed)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)SendFriendsStatus.AnotherIDUsed) );

        private static readonly RestRequestInfo apiLinkPOST = new RestRequestInfo
            ( MethodPOST
            , "/api/link"
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)LinkSocialNetworkStatus.OK)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)LinkSocialNetworkStatus.NotFound) );

        private static readonly RestRequestInfo apiVerifyPOST = new RestRequestInfo
            ( MethodPOST
            , "/api/verify"
            , RestRequestInfo.contentTypeApplicationJson
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)PurchaseVerificationStatus.OK)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NoContent,         (int)PurchaseVerificationStatus.VerificationFailed)
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)PurchaseVerificationStatus.CannotPerformVerification) );

        private static readonly RestRequestInfo apiScorePOST = new RestRequestInfo
            ( MethodPOST
            , "/api/score"
            , RestRequestInfo.contentTypeApplicationJson
            , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)PurchaseVerificationStatus.OK, typeof(ScoreApiPostResponseData)));

        #endregion

        #region Public properties.
        public TimeSpan clientFromServerTimeDifference      { get; private set; } // Positive if client's time is bigger.
        public NetworkBootData netBootData                  { get; private set; }
        public GenericNetworkStatus bootStatus              { get; private set; }

        public bool                 isRecoverableBootStatus
        {
            get
            {
                return (bootStatus != GenericNetworkStatus.OutdatedVersion);
            }
        }
        #endregion

        #region Public events.
        public event OnNetworkStatusUpdated onNetworkStatusUpdated;
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected NetworkCoreSettings settings;

        [SerializeField]
        [Tooltip("Time to live for cached 'network boot' state (in seconds).")]
        protected float networkStatusCacheTimeout = 900.0f;

        [SerializeField]
        [Tooltip("Maximum time we want to wait to the request to finish after the connection is established (in seconds).\n\nUsed with BestHTTP backend, for UnityWebRequest this setting is ignored.")]
        protected float timeout = 60.0f;

        [SerializeField]
        [Tooltip("Maximum time we wait to establish the connection to the target server. If set to 0 or lower, no connect timeout logic is executed (in seconds).\n\nUsed with BestHTTP backend, for UnityWebRequest this setting is ignored.")]
        protected float connectionTimeout = 20.0f;

        [SerializeField]
        [Tooltip("If set then if reading back the server's response fails, the request will fail immediately without retrying it again.\n\nUsed with BestHTTP backend, for UnityWebRequest this setting is ignored.")]
        protected bool disableRetry = true;
        #endregion

        #region Private data.
        private JsonSerializerSettings jsonSerializerSettings;
        private IEnumerator bootJob;
        private IEnumerator loginJob;
        private OnBootComplete bootCompleteCallback;
        private OnLoginComplete loginCompleteCallback;
        private AutoKeyIntDictionary<JobData> jobs;
        private string clientID;
        private float networkCacheExpirationTime;
        #endregion

        #region Unity API.
        void Awake()
        {
            var isoDateTimeConverter = new IsoDateTimeConverter();
            isoDateTimeConverter.DateTimeStyles = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;
            isoDateTimeConverter.DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters.Add(isoDateTimeConverter);
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            jsonSerializerSettings.DefaultValueHandling = DefaultValueHandling.Include | DefaultValueHandling.Populate;
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            // jsonSerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            jsonSerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
            jsonSerializerSettings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            jsonSerializerSettings.Culture = CultureInfo.InvariantCulture;
            jsonSerializerSettings.FloatParseHandling = FloatParseHandling.Double;
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;

            jobs = new AutoKeyIntDictionary<JobData>(16);
        }

        void Start()
        {
            networkCacheExpirationTime = Time.realtimeSinceStartup;
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {   // Expire network boot timeout when returning from pause, causing refetching of network boot data on 
                networkCacheExpirationTime = Time.realtimeSinceStartup;
            }
        }
        #endregion

        #region Public API.
        public void CancelAllRequests()
        {
            while (jobs.Count > 0) // Do in a loop, because cancel actions potentially can queue another jobs.
            {
                var jobsCopy = jobs.Values.ToArray();
                jobs.Clear();

                foreach (var jobData in jobsCopy)
                {
                    StopCoroutine(jobData.job);
                    jobData.cancelAction?.Invoke();
                }
            }
        }

        public void Login
            ( string clientID
            , bool switchToSelf
            , OnLoginComplete onComplete
            , float customConnectionTimeout = -1.0f
            , float customTimeout = -1.0f)
        {
            // This construction allows queuing multiple Login() requests even if previous are not finished yet.
            // Login data is taken of course only from first request, but callbacks invoked for all.

            if (loginJob == null)
            {
                loginJob = LoginJob(switchToSelf, clientID, customConnectionTimeout, customTimeout);
                if (onComplete != null)
                {
                    Assert.IsNull(loginCompleteCallback);
                    loginCompleteCallback = onComplete;
                }

                StartJob(loginJob, () =>
                {   // Cancel func.
                    UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                    LoginComplete(LoginStatus.ConnectionFailed, null, false);
                });
            }
            else
            {
                Assert.IsNotNull(loginCompleteCallback);
                if (onComplete != null)
                {
                    loginCompleteCallback += onComplete;
                }
            }
        }

        public void PurchaseVerification
            ( string receipt
            , OnPurchaseVerificationComplete onComplete)
        {
            StartJob(PurchaseVerificationJob(receipt, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(PurchaseVerificationStatus.ConnectionFailed);
            });
        }

        public void GetProfileData
            ( string profileID
            , FileInfo fileInfo
            , OnGetProfileDataComplete onComplete )
        {
            StartJob(GetProfileDataJob(profileID, fileInfo, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(GetProfileDataStatus.ConnectionFailed, null);
            });
        }

        public void GetFriendProfileData
            ( string friendUserID
            , FileInfo fileInfo
            , OnGetFriendProfileDataComplete onComplete )
        {
            StartJob(GetFriendProfileDataJob(friendUserID, fileInfo, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(GetProfileDataStatus.ConnectionFailed);
            });
        }

        public void SendProfileData
            ( string profileID
            , FileInfo fileInfo
            , int metaLevel
            , int metaExperience
            , OnSendProfileDataComplete onComplete )
        {
            StartJob(SendProfileDataJob(profileID, fileInfo, metaLevel, metaExperience, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(SendProfileDataStatus.ConnectionFailed, null);
            });
        }

        public void GetFriends
            ( OnGetFriendsComplete onComplete )
        {
            StartJob(GetFriendsJob(onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(GetFriendsStatus.ConnectionFailed, null, null);
            });
        }

        public void SendFriends
            ( string socialNetworkName
            , string networkUserId
            , string name
            , string email
            , string[] friendsIDs
            , OnSendFriendsComplete onComplete )
        {
            StartJob(SendFriendsJob(socialNetworkName, networkUserId, name, email, friendsIDs, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(SendFriendsStatus.ConnectionFailed);
            });
        }

        public void LinkSocialNetwork
            ( string socialNetworkName
            , LinkSocialNetworkAction action
            , OnLinkSocialNetworkComplete onComplete )
        {
            StartJob(LinkSocialNetworkJob(socialNetworkName, action, onComplete), () =>
            {
                UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                onComplete(LinkSocialNetworkStatus.ConnectionFailed);
            });
        }

        public void SendScores
            ( string profileID
            , string name
            , int profileLevel
            , int popularity
            , OnSendScoresComplete onComplete )
        {
            StartJob(SendScoresJob(profileID, name, profileLevel, popularity, onComplete), () =>
            {
                onComplete(SendScoresStatus.ConnectionFailed, null, null);
            });
        }
        #endregion

        #region Private methods.
        public void BootRequest
            ( OnBootComplete onComplete
            , float customConnectionTimeout = -1.0f
            , float customTimeout = -1.0f)
        {
            Assert.IsNotNull(settings);
            if (settings != null)
            {
                // This construction allows queuing multiple Initialize() requests even if previous are not finished yet.
                // Callbacks are invoked for all.

                if (netBootData != null &&
                    bootStatus == GenericNetworkStatus.OK &&
                    Time.realtimeSinceStartup < networkCacheExpirationTime)
                {
                    onComplete(bootStatus);
                }
                else if (bootStatus == GenericNetworkStatus.OK || isRecoverableBootStatus)
                {
                    if (bootJob == null)
                    {
                        bootJob = BootJob(customConnectionTimeout, customTimeout);
                        if (onComplete != null)
                        {
                            Assert.IsNull(bootCompleteCallback);
                            bootCompleteCallback = onComplete;
                        }

                        StartJob(bootJob, () =>
                        {   // Cancel func.
                            UpdateNetworkStatus(GenericNetworkStatus.ConnectionFailed);
                            BootComplete(GenericNetworkStatus.ConnectionFailed);
                        });
                    }
                    else
                    {
                        Assert.IsNotNull(bootCompleteCallback);
                        if (onComplete != null)
                        {
                            bootCompleteCallback += onComplete;
                        }
                    }
                }
                else
                {
                    onComplete(bootStatus);
                }
            }
            else
            {
                onComplete(GenericNetworkStatus.NotInitialized);
            }
        }

        IEnumerator BootJob
            ( float customConnectionTimeout
            , float customTimeout)
        {
            Assert.IsNotNull(settings);

            yield return null;

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log("Boot request. Network unavailable.");
                bootStatus = GenericNetworkStatus.NetworkUnavailable;
            }
            else
            {
//              yield return new WaitForSeconds(3.0f);

                var requestInfo = new RestRequestInfo
                    ( MethodGET
                    , string.Format ( "{0}/bootrecord-{1}.json", settings.bootHostURLPath, settings.clientVersion)
                    , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.OK,                (int)GenericNetworkStatus.OK, typeof(NetworkBootData), null)
                    , new RestRequestInfo.ResponseHandling ( (int)HttpStatusCode.NotFound,          (int)GenericNetworkStatus.TooHighVersion) );

                var request = SetMandatoryRequestHeaders
                    ( RestAPIHelper.CreateRequest
                        ( settings.bootHost
                        , requestInfo
                        , null
                        , null));

#if BESTHTTP_ENABLE
                if (customConnectionTimeout >= 0)
                    request.ConnectTimeout = TimeSpan.FromSeconds(customConnectionTimeout);

                if (customTimeout >= 0)
                    request.Timeout = TimeSpan.FromSeconds(customTimeout);
#endif

                yield return request.Send();

                object netBootDataObj;
                bootStatus = RestAPIHelper.ProcessRequestResponse<GenericNetworkStatus> 
                    ( request
                    , requestInfo
                    , jsonSerializerSettings
                    , out netBootDataObj );

                if (bootStatus == GenericNetworkStatus.OK)
                {
                    if (netBootDataObj != null)
                    {
                        netBootData = (NetworkBootData)netBootDataObj;

                        if (netBootData.clientVersion > settings.clientVersion)
                        {
                            bootStatus = GenericNetworkStatus.OutdatedVersion;
                        }
                        else if (netBootData.clientVersion < settings.clientVersion)
                        {
                            bootStatus = GenericNetworkStatus.TooHighVersion;
                        }
                        else if (netBootData.maintenance)
                        {
                            bootStatus = GenericNetworkStatus.ServerMaintenance;
                        }
                        else
                        {
                            bootStatus = GenericNetworkStatus.OK;
                        }

                        Debug.LogFormat("Network init result: {0}, expected client version: {1}, our client version: {2}, server version: {3}, server base URL: {4}"
                            , bootStatus, netBootData.clientVersion, settings.clientVersion, netBootData.serverVersion, netBootData.gameServer);
                    }
                    else
                    {
                        bootStatus = GenericNetworkStatus.InvalidResponse;
                    }
                }
            }

            UpdateNetworkStatus(bootStatus);
            BootComplete(bootStatus);
        }

        private void BootComplete(GenericNetworkStatus initializationStatus)
        {
            Assert.IsNotNull(bootJob);
            bootJob = null;

            networkCacheExpirationTime = Time.realtimeSinceStartup + ((networkStatusCacheTimeout > 1.0f) ? networkStatusCacheTimeout : 1.0f);

            // This construct allows to invoke callbacks safely even if they cause invokation of another Initialize() request.
            if (bootCompleteCallback != null)
            {
                var tmpCallback = bootCompleteCallback;
                bootCompleteCallback = null;
                tmpCallback(initializationStatus);
            }
        }

        IEnumerator LoginJob
            ( bool switchToSelf
            , string clientID
            , float customConnectionTimeout
            , float customTimeout)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(clientID);

            this.clientID = clientID;

            yield return null;

            var bootPending = true;
            BootRequest
                ((bootStatus) =>
                {
                    bootPending = false;
                }
                , customConnectionTimeout
                , customTimeout);
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                LoginStatus loginStatus = LoginStatus.NotInitialized;
                LoginData loginData = null;
                bool isNewClientID = true;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
    //              yield return new WaitForSeconds(3.0f);

                    LoginApiPostRequestData postData = new LoginApiPostRequestData(switchToSelf);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiLoginPOST
                        , postData
                        , jsonSerializerSettings
                        , clientID ));

#if BESTHTTP_ENABLE
                    if (customConnectionTimeout >= 0)
                        request.ConnectTimeout = TimeSpan.FromSeconds(customConnectionTimeout);

                    if (customTimeout >= 0)
                        request.Timeout = TimeSpan.FromSeconds(customTimeout);
#endif

                    yield return request.Send();

                    UpdateTimeDifference(request, switchToSelf);

                    object loginDataObj;
                    loginStatus = RestAPIHelper.ProcessRequestResponse<LoginStatus>
                        ( request
                        , apiLoginPOST
                        , jsonSerializerSettings
                        , out loginDataObj);

                    if (loginStatus == LoginStatus.OK)
                    {
                        if (loginDataObj != null)
                        {
                            loginData = (LoginData)loginDataObj;

#if BESTHTTP_ENABLE
                            isNewClientID = (request.Response.StatusCode == (int)HttpStatusCode.Created);
#else
                            isNewClientID = (request.responseCode == (int)HttpStatusCode.Created);
#endif

                            if (isNewClientID)
                            {
                                // Fill out some important fields that are missing in response (it contains only profile_id).
                                loginData.lastClientId = clientID;
                                loginData.activeClientId = clientID;
                            }
                        }
                        else
                        {
                            loginStatus = LoginStatus.InvalidResponse;
                        }
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiLoginPOST);
                    loginStatus = LoginStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(loginStatus);
                LoginComplete(loginStatus, loginData, isNewClientID);
            }
            else
            {
                LoginComplete((LoginStatus)(int)bootStatus, null, false);
            }
        }

        private void LoginComplete(LoginStatus loginStatus, LoginData loginData, bool isNewClientID)
        {
            // This construct allows to invoke callbacks safely even if they cause invokation of another Login() request.
            Assert.IsNotNull(loginJob);
            loginJob = null;
            if (loginCompleteCallback != null)
            {
                var tmpCallback = loginCompleteCallback;
                loginCompleteCallback = null;
                tmpCallback(loginStatus, loginData, isNewClientID);
            }
        }

        IEnumerator PurchaseVerificationJob
            ( string receipt
            , OnPurchaseVerificationComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(clientID);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                PurchaseVerificationStatus status = PurchaseVerificationStatus.NotInitialized;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
    //              yield return new WaitForSeconds(3.0f);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiVerifyPOST
                        , receipt
                        , null
                        , clientID ));

                    yield return request.Send();

                    UpdateTimeDifference(request);

                    status = RestAPIHelper.ProcessRequestResponse<PurchaseVerificationStatus> ( request, apiVerifyPOST );
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiVerifyPOST);
                    status = PurchaseVerificationStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status);
            }
            else
            {
                onComplete((PurchaseVerificationStatus)(int)bootStatus);
            }
        }

        private IEnumerator GetProfileDataJob
            ( string profileID
            , FileInfo fileInfo
            , OnGetProfileDataComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(fileInfo);
            Assert.IsNotNull(clientID);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                NetworkFileTimeStamps data = null;
                GetProfileDataStatus status = GetProfileDataStatus.NotInitialized;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
    //              yield return new WaitForSeconds(3.0f);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiProfileGET
                        , null
                        , null
                        , clientID ));

                    yield return request.Send();

                    var serverCurrentTime = UpdateTimeDifference(request);

                    object responseDataObj;

                    status = RestAPIHelper.ProcessRequestResponse<GetProfileDataStatus>
                        ( request
                        , apiProfileGET
                        , jsonSerializerSettings
                        , out responseDataObj);

                    if (status == GetProfileDataStatus.OK && responseDataObj != null)
                    {
                        ProfileApiGetResponseData responseData = (ProfileApiGetResponseData)responseDataObj;

                        if (responseData.save != null && responseData.save != string.Empty)
                        {
                            try
                            {
                                byte[] saveGameBytes = Convert.FromBase64String(responseData.save);

                                try
                                {
                                    using (var fileStream = fileInfo.Create())
                                    {
                                        fileStream.Write(saveGameBytes, 0, saveGameBytes.Length);
                                    }

                                    fileInfo.Refresh();

                                    data = new NetworkFileTimeStamps
                                        ( profileID
                                        , fileInfo.LastWriteTimeUtc.ToUniversalTime()
                                        , responseData.profile_saved
                                        , responseData.client_time
                                        , serverCurrentTime != DateTime.MinValue ? serverCurrentTime : fileInfo.LastWriteTimeUtc.ToUniversalTime()
                                        , fileInfo.LastWriteTimeUtc.ToUniversalTime());
                                }
                                catch (Exception ex)
                                {
                                    Debug.LogErrorFormat("REST request {0}. Failed to write file '{1}'. Exception:\n{2}"
                                        , apiProfileGET, fileInfo.FullName, ex);
                                    status = GetProfileDataStatus.FileIOError;
                                }
                            }
                            catch (Exception e)
                            {
                                Debug.LogErrorFormat("REST request {0}. Failed to convert 'save' field in response from Base-64. Exception:\n{1}"
                                    , apiProfileGET, e);
                                status = GetProfileDataStatus.InvalidResponse;
                            }
                        }
                        else
                        {
                            Debug.LogErrorFormat("REST request {0}. Empty 'save' field in response.", apiProfileGET);
                            status = GetProfileDataStatus.InvalidResponse;
                        }
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiProfilePOST);
                    status = GetProfileDataStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status, data);
            }
            else
            {
                onComplete((GetProfileDataStatus)(int)bootStatus, null);
            }
        }

        private IEnumerator GetFriendProfileDataJob
            ( string friendUserID
            , FileInfo fileInfo
            , OnGetFriendProfileDataComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(fileInfo);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                GetProfileDataStatus status = GetProfileDataStatus.NotInitialized;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
    //              yield return new WaitForSeconds(3.0f);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiProfileFriendGET
                        , null
                        , null
                        , friendUserID ));

                    yield return request.Send();

                    UpdateTimeDifference(request);

                    object saveGameBytesObj;

                    status = RestAPIHelper.ProcessRequestResponse<GetProfileDataStatus>
                        ( request
                        , apiProfileFriendGET
                        , null
                        , out saveGameBytesObj );

                    if (status == GetProfileDataStatus.OK && saveGameBytesObj != null)
                    {
                        byte[] saveGameBytes = (byte[])saveGameBytesObj;
                        
                        try
                        {
                            using (var fileStream = fileInfo.Create())
                            {
                                fileStream.Write(saveGameBytes, 0, saveGameBytes.Length);
                            }

                            status = GetProfileDataStatus.OK;
                        }
                        catch (Exception ex)
                        {
                            Debug.LogErrorFormat("REST request {0}. Failed to write file '{1}'. Exception:\n{2}"
                                , apiProfileFriendGET, fileInfo.FullName, ex);
                            status = GetProfileDataStatus.FileIOError;
                        }
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiProfileFriendGET);
                    status = GetProfileDataStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status);
            }
            else
            {
                onComplete((GetProfileDataStatus)(int)bootStatus);
            }
        }

        IEnumerator SendProfileDataJob
            ( string profileID
            , FileInfo fileInfo
            , int metaLevel
            , int metaExperience
            , OnSendProfileDataComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(fileInfo);
            Assert.IsNotNull(clientID);

//            yield return new WaitForSeconds(3.0f);

            yield return null; // Always continue as a Coroutine.

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                SendProfileDataStatus status = SendProfileDataStatus.NotInitialized;
                NetworkFileTimeStamps data = null;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    ProfileApiPostRequestData postData = new ProfileApiPostRequestData(metaLevel, metaExperience);

                    try
                    {
                        using (var fileStream = fileInfo.OpenRead())
                        {
                            var saveGameBytes = new byte[fileInfo.Length];
                            var count = fileStream.Read(saveGameBytes, 0, (int)fileInfo.Length);
                            if (count != fileInfo.Length)
                            {
                                onComplete(SendProfileDataStatus.FileIOError, null);
                                yield break;
                            }

                            postData.save = Convert.ToBase64String(saveGameBytes);
                        }
                    }
                    catch (Exception ex)
                    {

                        Debug.LogErrorFormat("REST request {0}. Failed to read file '{1}'. Exception:\n{2}"
                            , apiProfilePOST, fileInfo.FullName, ex);
                        onComplete(SendProfileDataStatus.FileIOError, null);
                        yield break;
                    }

                    postData.client_time = DateTime.UtcNow;

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiProfilePOST
                        , postData
                        , jsonSerializerSettings
                        , clientID ));
            
                    yield return request.Send();

                    UpdateTimeDifference(request);

                    object responseDataObj;

                    status = RestAPIHelper.ProcessRequestResponse<SendProfileDataStatus>
                        ( request
                        , apiProfilePOST
                        , jsonSerializerSettings
                        , out responseDataObj );

                    if (status == SendProfileDataStatus.OK && responseDataObj != null)
                    {
                        ProfileApiPostResponseData responseData = (ProfileApiPostResponseData)responseDataObj;

                        data = new NetworkFileTimeStamps
                            ( profileID
                            , fileInfo.LastWriteTimeUtc.ToUniversalTime()
                            , responseData.profile_saved
                            , postData.client_time);
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiProfilePOST);
                    status = SendProfileDataStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status, data);
            }
            else
            {
                onComplete((SendProfileDataStatus)(int)bootStatus, null);
            }
        }

        IEnumerator GetFriendsJob
            ( OnGetFriendsComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(clientID);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                GetFriendsStatus status = GetFriendsStatus.NotInitialized;
                NetworkInfo[] networkInfo = null;
                FriendInfo[] friendsInfo = null;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
    //              yield return new WaitForSeconds(3.0f);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiFriendsGET
                        , null
                        , null
                        , clientID ));

                    yield return request.Send();

                    UpdateTimeDifference(request);

                    object responseDataObj;

                    status = RestAPIHelper.ProcessRequestResponse<GetFriendsStatus>
                        ( request
                        , apiFriendsGET
                        , jsonSerializerSettings
                        , out responseDataObj );

                    if (status == GetFriendsStatus.OK && responseDataObj != null)
                    {
                        var friendsResponseData = (FriendsApiGetResponseData)responseDataObj;

                        networkInfo = friendsResponseData.networks;
                        friendsInfo = friendsResponseData.friends;
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiFriendsGET);
                    status = GetFriendsStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status, networkInfo, friendsInfo);
            }
            else
            {
                onComplete((GetFriendsStatus)(int)bootStatus, null, null);
            }
        }

        IEnumerator SendFriendsJob
            ( string socialNetworkName
            , string socialNetworkID
            , string name
            , string email
            , string[] friendsIDs
            , OnSendFriendsComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(clientID);
            Assert.IsNotNull(name);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                SendFriendsStatus status = SendFriendsStatus.NotInitialized;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    var postData = new FriendsApiPostRequestData
                        ( socialNetworkID
                        , name
                        , email != null ? email : string.Empty
                        , friendsIDs != null ? friendsIDs : new string[] { });

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiFriendsPOST
                        , postData
                        , jsonSerializerSettings
                        , clientID
                        , socialNetworkName ));
            
                    yield return request.Send();

                    UpdateTimeDifference(request);

                    status = RestAPIHelper.ProcessRequestResponse<SendFriendsStatus> ( request, apiFriendsPOST );
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiFriendsPOST);
                    status = SendFriendsStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status);
            }
            else
            {
                onComplete((SendFriendsStatus)(int)bootStatus);
            }
        }

        private IEnumerator LinkSocialNetworkJob
            ( string socialNetworkName
            , LinkSocialNetworkAction action
            , OnLinkSocialNetworkComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(clientID);

            yield return null;

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                LinkSocialNetworkStatus status = LinkSocialNetworkStatus.NotInitialized;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    string actionString = string.Empty;
                    if (action == LinkSocialNetworkAction.Restore)
                    {
                        actionString = "restore";
                    }
                    else if (action == LinkSocialNetworkAction.SaveCurrent)
                    {
                        actionString = "save_current";
                    }
                    else
                    {
                        onComplete(LinkSocialNetworkStatus.InvalidRequest);
                        yield break;
                    }

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiLinkPOST
                        , null
                        , null
                        , clientID
                        , actionString
                        , socialNetworkName ));
            
                    yield return request.Send();

                    UpdateTimeDifference(request);

                    status = RestAPIHelper.ProcessRequestResponse<LinkSocialNetworkStatus>( request, apiLinkPOST);
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiLinkPOST);
                    status = LinkSocialNetworkStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status);
            }
            else
            {
                onComplete((LinkSocialNetworkStatus)(int)bootStatus);
            }
        }

        private IEnumerator SendScoresJob (string profileID, string name, int profileLevel, int popularity, OnSendScoresComplete onComplete)
        {
            Assert.IsNotNull(settings);
            Assert.IsNotNull(profileID);
            Assert.IsNotNull(name);

//            yield return new WaitForSeconds(3.0f);

            yield return null; // Always continue as a Coroutine.

            var bootPending = true;
            BootRequest ((bootStatus) =>
            {
                bootPending = false;
            });
            yield return new WaitWhile(() => bootPending);

            if (bootStatus == GenericNetworkStatus.OK)
            {
                Assert.IsNotNull(netBootData);

                SendScoresStatus status = SendScoresStatus.NotInitialized;

                ScoreboardPlayerInfo[] top = null;
                ScoreboardPlayerInfo[] local = null;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    ScoreApiPostRequestData postData = new ScoreApiPostRequestData(profileID, name, 0, profileLevel, popularity);

                    var request = SetMandatoryRequestHeaders(RestAPIHelper.CreateRequest
                        ( netBootData.gameServer
                        , apiScorePOST
                        , postData
                        , jsonSerializerSettings
                        , profileID ));
            
                    yield return request.Send();

                    UpdateTimeDifference(request);

                    object responseDataObj;

                    status = RestAPIHelper.ProcessRequestResponse<SendScoresStatus>
                        ( request
                        , apiScorePOST
                        , jsonSerializerSettings
                        , out responseDataObj );

                    if (status == SendScoresStatus.OK && responseDataObj != null)
                    {
                        ScoreApiPostResponseData responseData = (ScoreApiPostResponseData)responseDataObj;

                        top = responseData.top;
                        local = responseData.local;
                    }
                }
                else
                {
                    Debug.LogFormat("REST request {0}. Network unavailable.", apiScorePOST);
                    status = SendScoresStatus.NetworkUnavailable;
                }

                UpdateNetworkStatus(status);
                onComplete(status, top, local);
            }
            else
            {
                onComplete((SendScoresStatus)(int)bootStatus, null, null);
            }
        }

#if BESTHTTP_ENABLE
        HTTPRequest SetMandatoryRequestHeaders(HTTPRequest request)
#else
        UnityWebRequest SetMandatoryRequestHeaders(UnityWebRequest request)
#endif
        {
#if BESTHTTP_ENABLE
            request.DisableRetry = disableRetry;
            request.Timeout = TimeSpan.FromSeconds(timeout);
            request.ConnectTimeout = TimeSpan.FromSeconds(timeout);
#else
            request.useHttpContinue = false;    // Also sets "Expect" header.
#endif
            return request;
        }

#if BESTHTTP_ENABLE
        DateTime UpdateTimeDifference(HTTPRequest request, bool force = false)
#else
        DateTime UpdateTimeDifference(UnityWebRequest request, bool force = false)
#endif
        {
            var timeNow = DateTime.UtcNow;

            if (force)
            {
                clientFromServerTimeDifference = TimeSpan.Zero;
            }

#if BESTHTTP_ENABLE
            if (request.State == HTTPRequestStates.Finished && request.Response != null)
#else
            if (!request.isError && request.isDone)
#endif
            {
                var dateResponseHeader = request.Response.GetFirstHeaderValue("Date");
                if (dateResponseHeader != null && dateResponseHeader != string.Empty)
                {
                    DateTime responseDate;
                    if (DateTime.TryParseExact
                        ( dateResponseHeader
                        , "r"
                        , CultureInfo.InvariantCulture.DateTimeFormat
                        , DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out responseDate ))
                    {
                        TimeSpan newDifference = timeNow - responseDate;
                        if (force || newDifference.Duration() < clientFromServerTimeDifference.Duration())
                        {
                            Debug.LogFormat("Client time differs from server's time by: {0} (previous difference is: {1})"
                                , newDifference, clientFromServerTimeDifference);

                            newDifference = clientFromServerTimeDifference;
                        }
                        return responseDate;
                    }
                    else
                    {
                        Debug.LogWarningFormat("Wrong \"Date\" header format in request's response, must be in RFC-1123 format: '{0}'."
                            , dateResponseHeader);
                    }
                }
                else
                {
                    Debug.LogWarning("Missing Wrong \"Date\" header format in request's response.");
                }
            }
            return DateTime.MinValue;
        }

        private void UpdateNetworkStatus<StatusT> (StatusT status) where StatusT : struct, IConvertible // Partial enum constraint
        {
            var intValue = Convert.ToInt32(status);
            if (intValue > (int)GenericNetworkStatus.OK)
            {
                intValue = (int)GenericNetworkStatus.OK; // Even if it's an error for request, it's not considered an error for "generic network state".
            }

            onNetworkStatusUpdated?.Invoke((GenericNetworkStatus)intValue);
        }

        private void StartJob (IEnumerator job, Action cancelAction)
        {
            var jobId = jobs.ProposeKey();
            var innerJob = JobWrapper(job, jobId);
            jobs.Add(new JobData(innerJob, cancelAction));
            StartCoroutine(innerJob);
        }

        IEnumerator JobWrapper(IEnumerator job, int id)
        {
            while (job.MoveNext())
            {
                yield return job.Current;
            }

            Assert.IsTrue(jobs.ContainsKey(id));
            jobs.Remove(id);
            yield break;
        }
        #endregion
    }
}
