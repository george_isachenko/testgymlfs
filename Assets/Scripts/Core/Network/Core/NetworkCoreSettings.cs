﻿using UnityEngine;

namespace Core.Network.Core
{
    [CreateAssetMenu(menuName = "Kingdom/Core/Network/Network Core Settings", fileName = "NetworkCoreSettings")]
    public class NetworkCoreSettings : ScriptableObject
    {
        public static readonly string defaultPath = "NetworkCoreSettings";

        public string bootHost;
        public string bootHostURLPath;
        public int clientVersion;
    }
}
