using System;
using Core.SocialAPI;
using UnityEngine;

namespace Core.Network.Friends
{
    public class Friend
    {
        #region Types.
        public enum Gender
        {
            Undefined = 0,
            Male,
            Female
        }

        [Serializable]
        public struct Desc
        {
            public int level;
            public string name;
            public Gender gender;
            public Sprite avatar;

            public Desc (int level, string name, Gender gender, Sprite avatar)
            {
                this.level = level;
                this.name = name;
                this.gender = gender;
                this.avatar = avatar;
            }
        }
        #endregion

        #region Delegates.
        public Action<Sprite> onAvatarUpdated;
        #endregion

        #region Public properties.
        public NetworkType network { get; private set; }
        public string networkId { get; private set; }
        public string profileId { get; private set; }
        public bool isDummy { get; private set; }
        public string name { get; private set; }
        public Gender gender { get; private set; }
        public int level { get; private set; }
        public Sprite avatar
        {
            get { return avatarBuffer; }
            set
            {
                avatarBuffer = value;
                if (onAvatarUpdated != null)
                    onAvatarUpdated(value);
            }
        }
        #endregion

        #region Private data.
        private Sprite avatarBuffer;
        #endregion

        #region Constructor.
        public Friend (NetworkType network, string networkId, string profileId, int level, string name, Gender gender, Sprite avatar)
        {
            this.network = network;
            this.isDummy = false;
            this.networkId = networkId;
            this.profileId = profileId;
            this.avatarBuffer = avatar;
            this.level = level;
            this.name = name;
            this.gender = gender;
        }

        public Friend (int level, string name, Gender gender, Sprite avatar)
        {
            this.network = NetworkType.Facebook;
            this.isDummy = true;
            this.networkId = string.Empty;
            this.profileId = string.Empty;
            this.avatarBuffer = avatar;
            this.level = level;
            this.name = name;
            this.gender = gender;
        }

        public Friend (Desc desc)
        {
            this.network = NetworkType.Facebook;
            this.isDummy = true;
            this.networkId = string.Empty;
            this.profileId = string.Empty;
            this.avatarBuffer = desc.avatar;
            this.level = desc.level;
            this.name = desc.name;
            this.gender = desc.gender;
        }
        #endregion
    }
}