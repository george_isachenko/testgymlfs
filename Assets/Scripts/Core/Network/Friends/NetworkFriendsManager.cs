﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.SocialAPI;
using UnityEngine;
using UnityEngine.Assertions;

namespace Core.Network.Friends
{
    public class NetworkFriendsManager : MonoBehaviour, IFriendsManager
    {
        #region Public fields.
        public float inGameSyncFriendsJobStartupDelay = 0.0f;
        #endregion

        #region Private data.
        IEnumerator syncFriendsJob = null;
        IEnumerator getFriendsJob = null;
        IEnumerator loadAvatarsJob = null;
        List<Friend> cachedFriends = new List<Friend>(128);
        List<NetworkType> networksToUpdateQueue;
        OnGetFriendsFinished onGetFriendsFinishedCallback;
        GetFriendsStatus lastGetFriendsStatus = GetFriendsStatus.NotInitialized;
        bool gameMode = false;
        #endregion

        #region Unity API.
        void Start()
        {
            foreach (var network in PersistentCore.instance.socialManager.GetAvailableNetworks())
            {
                network.onLoginStateChanged += OnSocialManagerLoginStateChanged;
            }

            // At first start try to update all registered networks (including not available on platform).
            networksToUpdateQueue = PersistentCore.instance.socialManager.allRegisteredNetworkTypes.ToList();
        }

        void OnDestroy()
        {
            if (PersistentCore.instance != null && PersistentCore.instance.socialManager != null)
            {
                foreach (var network in PersistentCore.instance.socialManager.GetAvailableNetworks())
                {
                    network.onLoginStateChanged -= OnSocialManagerLoginStateChanged;
                }
            }
        }
        #endregion

        #region IFriendsManager interface.
        void IFriendsManager.StartInGameJobs ()
        {
            StartSyncFriendsJob();
            gameMode = true;
        }

        void IFriendsManager.CancelAllInGameOperations ()
        {
            StopLoadAvatarsJob();

            if (getFriendsJob != null)
            {
                Debug.Log("Canceling friends getting routine...");
                StopCoroutine(getFriendsJob);
                getFriendsJob = null;
            }

            if (syncFriendsJob != null)
            {
                Debug.Log("Canceling friends sync background routine...");
                StopCoroutine(syncFriendsJob);
                syncFriendsJob = null;
            }
            gameMode = false;
        }

        void IFriendsManager.GetFriendsList (OnGetFriendsFinished onComplete)
        {
            if (getFriendsJob != null)
            {
                if (onComplete != null)
                    onGetFriendsFinishedCallback += onComplete;
            }
            else
            {
                Debug.Log("Queuing friends download routine...");
                if (onComplete != null)
                    onGetFriendsFinishedCallback = onComplete;
                else
                    onGetFriendsFinishedCallback = delegate { };

                getFriendsJob = GetFriendsListJob();
                StartCoroutine(getFriendsJob);
            }
        }

/*
        public bool IsSocialNetworkLinkedOnServer (SocialNetwork network)
        {
            if (socialNetworks != null)
            {
                return socialNetworks.Any(x => x.network == network);
            }
            return false;
        }
*/
        #endregion

        #region Private methods.
        void OnSocialManagerLoginStateChanged(NetworkType network, LoginResult result)
        {
            if (result == LoginResult.OK)
            {
                networksToUpdateQueue = networksToUpdateQueue.Union( new NetworkType[] { network }).ToList();

                if (gameMode)
                    StartSyncFriendsJob();
            }
        }

        private IEnumerator InGameSyncFriendsJob ()
        {
            Assert.IsNotNull(networksToUpdateQueue);

            var socialManager = PersistentCore.instance.socialManager;

            yield return null; // Skip first invocation.

            if (inGameSyncFriendsJobStartupDelay > 0)
            {
                yield return new WaitForSeconds(inGameSyncFriendsJobStartupDelay);
            }

            Debug.Log("Starting in-game friends synchronization background routine...");

            bool busy = false;
            bool stopJob = false;

            NetworkInfo[] socialNetworksFromServer = null;
            FriendInfo[] socialNetworkFriendsFromServer = null;

            while (!stopJob)
            {
                if (lastGetFriendsStatus != GetFriendsStatus.OK)
                {
                    StopLoadAvatarsJob();

                    Debug.Log ("Friends Sync: Trying to get friends from server...");

                    busy = true;
                    PersistentCore.instance.networkController.GetFriends
                        ( true
                        , (GetFriendsStatus status, NetworkInfo[] networks, FriendInfo[] friends) =>
                        {
                            busy = false;

                            Debug.LogFormat ("Friends Sync: Got friends from server. Result: {0}.", status);

                            lastGetFriendsStatus = status;
                            if (status == GetFriendsStatus.OK)
                            {
                                socialNetworksFromServer = networks;
                                socialNetworkFriendsFromServer = friends;

                                Assert.IsNotNull(socialNetworkFriendsFromServer);

                                cachedFriends = socialNetworkFriendsFromServer.Select
                                    (n => new Friend(n.network, n.networkId, n.profileId, n.level, n.name, Friend.Gender.Undefined, null)).ToList();

                                Assert.IsNotNull(cachedFriends);

                                Debug.LogFormat ("Friends Sync: Total number of friends on server: {0}.", cachedFriends.Count);

                                if (cachedFriends.Count > 0)
                                {
                                    Assert.IsNull(loadAvatarsJob);
                                    loadAvatarsJob = GetAvatarsJob();
                                    StartCoroutine(loadAvatarsJob);
                                }
                            }
                        });

                    yield return new WaitWhile(() => busy);
                }

                if (networksToUpdateQueue.Count == 0)
                    break;

                bool someSocialNetworkUpdated = false;

                while (!stopJob && networksToUpdateQueue.Count > 0) 
                {
                    var networkType = networksToUpdateQueue[0];
                    networksToUpdateQueue.RemoveAt(0);

                    var network = socialManager.GetNetwork(networkType);

                    if (network != null && network.GetNetworkState() == NetworkState.LoggedIn)
                    {
                        Debug.LogFormat ("Friends Sync: Trying to get friends from social network {0}...", networkType);

                        busy = true;
                        network.GetFriends
                            ( (netType, getSuccess, socialNetworkUsers) =>
                            {
                                busy = false;

                                Debug.LogFormat ("Friends Sync: Got friends from social network {0}. Result: {1}, Users: {2}"
                                    , networkType, getSuccess, socialNetworkUsers != null ? socialNetworkUsers.Length.ToString() : "null" );

                                if (getSuccess && socialNetworkUsers != null)
                                {
                                    var userIds = (socialNetworkUsers.Length > 0)
                                        ? socialNetworkUsers.Select(n => n.userID).OrderBy(x => x).ToArray()
                                        : new string[0];

                                    var thisNetworkDataFromServer = (socialNetworksFromServer != null )
                                        ? socialNetworksFromServer.FirstOrDefault(x => x.network == networkType)
                                        : default(NetworkInfo);

                                    var thisNetworkFriendsFromServer = (socialNetworkFriendsFromServer != null )
                                        ? socialNetworkFriendsFromServer.Where(x => x.network == networkType).Select(x => x.networkId).OrderBy(x => x).ToArray()
                                        : new string[0];

                                    // Ask for e-mail when getting info about self in these cases:
                                    // 1. No social networks on server at all (no previous sync, e.g. new user).
                                    // 2. No info for this social network on server (new user for this social network).
                                    // 3. E-mail for this social network is empty.

                                    var askForEmail = ( lastGetFriendsStatus == GetFriendsStatus.NotExists ||
                                                        thisNetworkDataFromServer == default(NetworkInfo) ||
                                                        thisNetworkDataFromServer.email == string.Empty);
                                    var self = network.GetSelfUserData(!askForEmail);
                                    Assert.IsNotNull(self);

                                    if (self != null)
                                    {
                                        // Send update to server on any of these conditions:
                                        // 1. No data on server for this network at all (e.g. new user).
                                        // 

                                        if (thisNetworkDataFromServer == default(NetworkInfo) ||
                                            thisNetworkFriendsFromServer == default(string[]) ||
                                            self.name != thisNetworkDataFromServer.name ||
                                            (self.email != string.Empty && self.email != thisNetworkDataFromServer.email) ||
                                            self.userID != thisNetworkDataFromServer.networkId ||
                                            thisNetworkFriendsFromServer != userIds )
                                        {
                                            Debug.LogFormat ("Friends Sync: Trying to send friends for network {0} to our server...", networkType);

                                            busy = true;
                                            PersistentCore.instance.networkController.SendFriends
                                                ( networkType.ToString()
                                                , self.userID
                                                , self.name
                                                , (self.email != string.Empty)
                                                    ? self.email
                                                    : (thisNetworkDataFromServer != default(NetworkInfo) ? thisNetworkDataFromServer.email : string.Empty)
                                                , (userIds != default(string[])) ? userIds : new string[0]
                                                , true
                                                , (sendStatus, linkStatus, linkAction) =>
                                                {
                                                    busy = false;

                                                    Debug.LogFormat ("Friends Sync: Finished sending friends for network {0} to our server. Send Result: {1}, Link Result: {2}."
                                                        , networkType, sendStatus, linkStatus);

                                                    if (sendStatus == SendFriendsStatus.OK)
                                                    {
                                                        someSocialNetworkUpdated = true;
                                                    }
                                                    else if (sendStatus == SendFriendsStatus.Conflict)
                                                    {
                                                        if (linkStatus == LinkSocialNetworkStatus.OK && linkAction == LinkSocialNetworkAction.Restore)
                                                        {
                                                            stopJob = true; // Because restart required.
                                                        }
                                                    }
                                                    else if (sendStatus == SendFriendsStatus.AnotherIDUsed)
                                                    {
                                                    
                                                    }
                                                    else
                                                    {
                                                        // TODO: How to handle other failures?
                                                    }
                                                });
                                        }
                                        else
                                        {
                                            Debug.LogFormat ("Friends Sync: No need to send data for social network {0}, it's same as on our server.", networkType);
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogErrorFormat ("Friends Sync: Failed to sync friends for social network {0}, cannot get info about self!", networkType);
                                    }
                                }
                                else
                                {
                                    Debug.LogWarningFormat ("Friends Sync: Failed to get friends from social network {0}.", networkType);
                                }
                            });

                        yield return new WaitWhile(() => busy);
                    }
                    else
                    {
                        Debug.LogFormat ("Friends Sync: Will not send friends for network {0}, not logged in yet.", networkType);
                    }
                }

                if (someSocialNetworkUpdated)
                {
                    lastGetFriendsStatus = GetFriendsStatus.NotInitialized; // This will cause a re-fetch of friends on next loop.
                }
            }

            Debug.Log("In-game friends synchronization background routine finished.");

            Assert.IsNotNull(syncFriendsJob);
            syncFriendsJob = null;
            yield break;
        }

        private IEnumerator GetFriendsListJob ()
        {
            yield return null; // Skip first invocation.

            Debug.Log("Starting get friends routine.");

            var socialManager = PersistentCore.instance.socialManager;

            if (syncFriendsJob != null)
            {
                Debug.LogFormat ("Get friends: Waiting for friends synchronization background routine to complete...");
                yield return new WaitWhile(() => syncFriendsJob != null);
            }

            Assert.IsNotNull(cachedFriends);

            Assert.IsNotNull(getFriendsJob);
            getFriendsJob = null;

            Debug.Log("Get friends background routine finished.");

            if (onGetFriendsFinishedCallback != null)
            {
                var tmpCallback = onGetFriendsFinishedCallback;
                onGetFriendsFinishedCallback = null;
                tmpCallback(cachedFriends);
            }

            yield break;
        }

        private IEnumerator GetAvatarsJob ()
        {
            Assert.IsNotNull(cachedFriends);

            var socialManager = PersistentCore.instance.socialManager;

            yield return null; // Skip first invocation.

            Debug.Log("Starting background avatars download & update routine...");

            bool busy = false;

            var friendsListLocal = cachedFriends; // It can be reset in another job.

            foreach (var friend in friendsListLocal)
            {
                busy = true;
                socialManager.GetNetwork(friend.network).GetAvatarPicture(friend.networkId, (networkType, avatarTexture) =>
                {
                    busy = false;

                    if (avatarTexture != null)
                    {
                        var sprite = Sprite.Create ( avatarTexture, new Rect(0, 0, avatarTexture.width, avatarTexture.height), new Vector2(0.5f,0.5f), 100);
                        friend.avatar = sprite;
                    }
                });

                yield return new WaitUntil(() => !busy);
            }

            Debug.Log("Background avatars download & update routine finished.");

            Assert.IsNotNull(loadAvatarsJob);
            loadAvatarsJob = null;
            yield break;
        }

        private void StopLoadAvatarsJob ()
        {
            if (loadAvatarsJob != null)
            {
                Debug.Log("Canceling avatars download & update routine...");
                StopCoroutine(loadAvatarsJob);
                loadAvatarsJob = null;
            }
        }

        private void StartSyncFriendsJob ()
        {
            if (syncFriendsJob == null)
            {
                Debug.Log("Queuing to start friends sync background routine...");
                syncFriendsJob = InGameSyncFriendsJob();
                StartCoroutine(syncFriendsJob);
            }
        }
        #endregion
    }
}