using System;
using System.Collections.Generic;
using Core.SocialAPI;

namespace Core.Network.Friends
{
    public delegate void OnGetFriendsFinished (List<Friend> friends);

    public interface IFriendsManager
    {
//         NetworkInfo[]        socialNetworks          { get; }
//         FriendInfo[]         socialNetworkFriends    { get; }

        void StartInGameJobs ();
        void CancelAllInGameOperations ();

        void GetFriendsList (OnGetFriendsFinished onComplete);
    }
}