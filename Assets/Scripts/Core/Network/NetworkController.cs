﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using Core.Encryption;
using Core.Network.Core;
using Data;
using FileSystem;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI;
using Core.Analytics;
using Fabric.Crashlytics;

namespace Core.Network
{
    public delegate void OnNetworkStatusChanged (GenericNetworkStatus status);

    [RequireComponent(typeof(NetworkCore))]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Core/Network/Network Controller")]
    public class NetworkController : MonoBehaviour
    {
        #region Private delegates.
        delegate void OnGetProfileDataComplete (GetProfileDataStatus status);
        delegate void OnSendProfileDataComplete (SendProfileDataStatus status);
        delegate void OnLoginComplete (LoginStatus status);
        #endregion

        #region Types.
        #endregion

        #region Private static/readonly data.
        private static readonly int             clientIdBytesLength     = 18; // After applying Base64 encoding it becomes 24 chars string.
        #endregion

        #region Public events.
        public event OnNetworkStatusChanged onNetworkStatusChanged;
        #endregion

        #region Public properties.
        public GenericNetworkStatus lastNetworkStatus       { get; private set; }
        public LoginStatus          loginStatus             { get; private set; }
        public LoginData            loginData               { get; private set; }
        public bool                 isNewClientID           { get; private set; }
        public string               clientID
        {
            get
            {
                Assert.IsNotNull(networkData);
                return networkData.clientID;
            }
        }
        public bool                 forceStartupSync        { get; private set; }

        public TimeSpan             clientFromServerTimeDifference // Positive if client's time is bigger.
        {
            get
            {
                Assert.IsNotNull(networkCore);
                return (networkCore != null) ? networkCore.clientFromServerTimeDifference : TimeSpan.Zero;
            }
        }

        public NetworkFileTimeStamps downloadedFileTimeStamps
        {
            get
            {
                return networkData != null ? networkData.downloadedFileTimeStamps : null;
            }
        }

        public string               profileId
        {
            get
            {
                return networkData != null ? networkData.profileId : string.Empty;
            }
        }
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected float saveUploadJobStartupDelay = 0.0f;

        [SerializeField]
        protected float saveGameUploadRate = 30.0f;
        
        [SerializeField]
        protected float saveGameUploadRateAfterFailure = 10.0f;

        [SerializeField]
        [Tooltip("Custom timeout for startup boot & login requests (in seconds). For other requests, default setting in NetworkCore is used.\n\nUsed with BestHTTP backend, for UnityWebRequest this setting is ignored.")]
        protected float startupLoginTimeout = 60.0f;

        [SerializeField]
        [Tooltip("Custom connection timeout for startup boot & login requests (in seconds). For other requests, default setting in NetworkCore is used.\n\nUsed with BestHTTP backend, for UnityWebRequest this setting is ignored.")]
        protected float startupLoginConnectionTimeout = 20.0f;
        #endregion

        #region Private data.
        NetworkCore networkCore;
        JsonSerializerSettings jsonSerializerSettings;
        NetworkData networkData = null;
        IEnumerator inGameSaveUploadJob = null;
        IEnumerator lastNetworkStatusChangedNotifyJob = null;
        FileInfo mainSaveGameFileInfo;
        byte[] ekBytes;
        bool forceImmediateSaveGameUpdate = false;
        bool saveGameUpdateNotified = false;
        #endregion

        #region Unity API.
        void Awake ()
        {
            var ekFileName = ResourcePaths.Persistent.keysResFolder + ResourcePaths.Network.networkDataKeyResource;
            ekBytes = EKBytes(ekFileName);
            if (ekBytes != null)
            {
                ekBytes = EH.GK(ekBytes, K.cids, NetworkData.encKeySize / 8);
            }
            else
            {
#if DEBUG
                Debug.LogErrorFormat("Cannot read Network Controller data encryption key file resource '{1}'. Network sync and login will fail.", ekFileName);
#endif
            }

            networkCore = GetComponent<NetworkCore>();
            Assert.IsNotNull(networkCore);

            networkCore.onNetworkStatusUpdated += OnNetworkStatusUpdated;

            SetDefaults();
        }

        void Start()
        {
            var isoDateTimeConverter = new IsoDateTimeConverter();
            isoDateTimeConverter.DateTimeStyles = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;
            isoDateTimeConverter.DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";

            jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters.Add(isoDateTimeConverter);
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            jsonSerializerSettings.DefaultValueHandling = DefaultValueHandling.Include | DefaultValueHandling.Populate;
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            // jsonSerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            jsonSerializerSettings.TypeNameHandling = TypeNameHandling.Auto;
            jsonSerializerSettings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            jsonSerializerSettings.Culture = CultureInfo.InvariantCulture;
            jsonSerializerSettings.FloatParseHandling = FloatParseHandling.Double;
            jsonSerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            jsonSerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
        }

        void OnDestroy ()
        {
            networkCore.onNetworkStatusUpdated -= OnNetworkStatusUpdated;
        }

        void OnApplicationPause (bool pauseStatus)
        {
            if (pauseStatus)
            {
                if (mainSaveGameFileInfo != null)
                {
                    mainSaveGameFileInfo.Refresh();
                    forceImmediateSaveGameUpdate = true;
                }
            }
        }
        #endregion

        #region Public API.
#if DEBUG
        public void DebugResetNetworkData ()
        {
            if (File.Exists(ResourcePaths.Network.networkDataFullPath))
            {
                File.Delete(ResourcePaths.Network.networkDataFullPath);
            }
        }
#endif

        public bool QuickInit ()
        {
            if (networkData == null)
            {
                if (!LoadNetworkData())
                {
                    return false;
                }
            }

            loginStatus = LoginStatus.NotInitialized;
            loginData = null;
            
            return true;
        }

        public void StartupLoginAndSync
            ( string saveGameFileName
            , bool silentErrors
            , OnStartupSyncComplete onComplete)
        {
            Assert.IsNotNull(onComplete);

            if (networkData == null)
            {
                if (!LoadNetworkData())
                {
                    PersistentCore.Quit(true);
                    onComplete(StartupSynchronizationStatus.InitializationFailedWaitForever);
                    return;
                }
            }

            loginStatus = LoginStatus.NotInitialized;
            loginData = null;

            PersistentCore.instance.splashScreenView.progressText = Loc.Get("SplashScreenMessageConnecting");

            InitiateLoginRequest
                ( true
                , (loginStatus) =>
                {
                    // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                    if (loginStatus == LoginStatus.OK)
                    {
                        Assert.IsNotNull(loginData);
                        PerformStartupSync(saveGameFileName, onComplete);
                    }
                    else
                    {
                        var genericStatus = (GenericNetworkStatus)loginStatus;

                        if (!silentErrors ||
                            genericStatus == GenericNetworkStatus.OutdatedVersion ||
                            genericStatus == GenericNetworkStatus.TooHighVersion ||
                            genericStatus == GenericNetworkStatus.ServerMaintenance)
                        {
                            ReportError
                                ( loginStatus
                                , Loc.Get("StartupSyncTryAgain")
                                , () =>
                                {
                                    onComplete(StartupSynchronizationStatus.InitializationFailedContinue);
                                }
                                , () =>
                                {
                                    onComplete(StartupSynchronizationStatus.InitializationFailedRetry);
                                });
                        }
                        else
                        {
                            onComplete(StartupSynchronizationStatus.InitializationFailedContinue);
                        }
                    }
                }
                , true
                , true);
        }

        public void PurchaseVerification
            ( string receipt
            , OnPurchaseVerificationComplete onComplete)
        {
            Assert.IsNotNull(networkCore);
            Assert.IsNotNull(onComplete);

            InitiateLoginRequest
                ( false
                , (loginStatus) =>
                {
                    if (loginStatus == LoginStatus.OK)
                    {
                        networkCore.PurchaseVerification(receipt, (status) =>
                        {
                            if (status < PurchaseVerificationStatus.OK)
                            {
                                ReportError
                                    ( status
                                    , Loc.Get("PurchaseVerificationCannotPerformVerification")
                                    , () =>
                                    {
                                        onComplete(PurchaseVerificationStatus.CannotPerformVerification);
                                    });
                            }
                            else
                            {
                                onComplete(status);
                            }
                        });
                    }
                    else
                    {
                        ReportError
                            ( loginStatus
                            , Loc.Get("PurchaseVerificationCannotPerformVerification")
                            , () =>
                            {
                                onComplete(PurchaseVerificationStatus.CannotPerformVerification);
                            });
                    }
                });
        }

        public void GetFriends
            ( bool silentErrors
            , OnGetFriendsComplete onComplete)
        {
            Assert.IsNotNull(networkCore);
            Assert.IsNotNull(onComplete);

            InitiateLoginRequest
                ( false
                , (loginStatus) =>
                {
                    if (loginStatus == LoginStatus.OK)
                    {
                        networkCore.GetFriends(onComplete);
                    }
                    else
                    {
                        if (silentErrors)
                        {
                            onComplete((GetFriendsStatus)(int)loginStatus, null, null);
                        }
                        else
                        {
                            ReportError
                                ( loginStatus
                                , Loc.Get("CannotGetFriends")
                                , () =>
                                {
                                    onComplete((GetFriendsStatus)(int)loginStatus, null, null);
                                });
                        }
                    }
                });
        }

        public void DownloadFriendSaveFromServer
            ( string profileId
            , string friendSaveGameFileName
            , bool silentErrors
            , OnDownloadFriendSaveComplete onComplete)
        {
            Assert.IsNotNull(networkCore);
            Assert.IsNotNull(profileId);
            Assert.IsNotNull(friendSaveGameFileName);
            Assert.IsNotNull(onComplete);

            var friendSaveGameFileInfo = new FileInfo(friendSaveGameFileName);

            PersistentCore.instance.splashScreenView.progressText = Loc.Get("SplashScreenMessageDownloadFriendSaveGame");

            InitiateLoginRequest
                ( false
                , (loginStatus) =>
                {
                    if (loginStatus == LoginStatus.OK)
                    {
                        try
                        {
                            if (friendSaveGameFileInfo.Exists)
                            {
                                friendSaveGameFileInfo.Delete();
                            }

                            networkCore.GetFriendProfileData
                                ( profileId
                                , friendSaveGameFileInfo
                                , (status) =>
                                {
                                    // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                                    if (status == GetProfileDataStatus.OK)
                                    {
                                        friendSaveGameFileInfo.Refresh();

                                        onComplete(DownloadFriendSaveStatus.OK);
                                    }
                                    else
                                    {
                                        PersistentCore.instance.analytics.GameEvent("Friend SaveGame Download Failed", "Status", status.ToString());

                                        ReportError
                                            ( status
                                            , Loc.Get("CannotDownloadFriendsSaveGame")
                                            , () =>
                                            {
                                                onComplete(DownloadFriendSaveStatus.Failed);
                                            });
                                    }
                                });
                        }
                        catch (Exception)
                        {
                            // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                            onComplete(DownloadFriendSaveStatus.Failed);
                            return;
                        }
                    }
                    else
                    {
                        // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                        ReportError
                            ( loginStatus
                            , Loc.Get("CannotDownloadFriendsSaveGame")
                            , () =>
                            {
                                onComplete(DownloadFriendSaveStatus.Failed);
                            });
                    }
                });
        }

        public void SendFriends
            ( string socialNetworkName
            , string networkUserId
            , string name
            , string email
            , string[] friendIds
            , bool silentErrors
            , OnSendFriendsComplete onComplete)
        {
            Assert.IsNotNull(networkCore);
            Assert.IsNotNull(socialNetworkName);
            Assert.IsNotNull(networkUserId);
            Assert.IsNotNull(friendIds);
            Assert.IsNotNull(onComplete);

            Action<LinkSocialNetworkAction> linkAction = (LinkSocialNetworkAction action) =>
            {
                InitiateLoginRequest
                    ( false
                    , (loginStatus) =>
                    {
                        if (loginStatus == LoginStatus.OK)
                        {
                            networkCore.LinkSocialNetwork
                                ( socialNetworkName
                                , action
                                , (linkStatus) =>
                                {
                                    if (linkStatus == LinkSocialNetworkStatus.OK)
                                    {
                                        if (action == LinkSocialNetworkAction.Restore)
                                        {
                                            PersistentCore.instance.analytics.GameEvent
                                                ( "Link Social Network Profile: Restart Ask"
                                                , "SocialNetwork"
                                                , socialNetworkName);

                                            PersistentCore.instance.QueueModalConfirmMessage
                                                ( Loc.Get("SocialLinkConflictRestartHeader")
                                                , Loc.Get("SocialLinkConflictRestartMessage")
                                                , () =>
                                                {
                                                    onComplete(SendFriendsStatus.OK, linkStatus, action);

                                                    PersistentCore.instance.analytics.GameEvent
                                                        ( "Link Social Network Profile: Restart Confirm"
                                                        , "SocialNetwork"
                                                        , socialNetworkName);

                                                    PersistentCore.ScheduleFullReload();
                                                }
                                                , Loc.Get("idRestart"));
                                        }
                                        else
                                        {
                                            // No user action needed, silently handle.
                                            onComplete(SendFriendsStatus.OK, linkStatus, action);
                                        }
                                    }
                                    else
                                    {
                                        ReportError
                                            ( linkStatus
                                            , Loc.Get("CannotLinkSocialNetwork" + action.ToString())
                                            , () =>
                                            {
                                                onComplete(SendFriendsStatus.OK, linkStatus, action);
                                            });
                                    }
                                });
                        }
                        else
                        {
                            ReportError
                                ( loginStatus
                                , Loc.Get("CannotLinkSocialNetwork" + action.ToString())
                                , () =>
                                {
                                    onComplete((SendFriendsStatus)(int)loginStatus, LinkSocialNetworkStatus.NotInitialized);
                                });
                        }
                    });
            };
        
            InitiateLoginRequest
                ( false
                , (loginStatus) =>
                {
                    if (loginStatus == LoginStatus.OK)
                    {
                        networkCore.SendFriends
                            ( socialNetworkName
                            , networkUserId
                            , name
                            , email
                            , friendIds
                            , (status) =>
                            {
                                if (status == SendFriendsStatus.OK)
                                {
                                    onComplete(status, LinkSocialNetworkStatus.NotInitialized);
                                }
                                else if (status == SendFriendsStatus.Conflict)
                                {
                                    PersistentCore.instance.analytics.GameEvent
                                        ( "Social Link Conflict: Ask"
                                        , new GameEventParam("SocialNetwork", socialNetworkName)
                                        , new GameEventParam("NetworkUserId", networkUserId));

                                    PersistentCore.instance.QueueModalConfirmMessage
                                        ( Loc.Get("SocialLinkConflictHeader")
                                        , Loc.Get("SocialLinkConflictMessage")
                                        , () =>
                                        {
                                            PersistentCore.instance.analytics.GameEvent
                                                ( "Social Link Conflict: Restore"
                                                , new GameEventParam("SocialNetwork", socialNetworkName)
                                                , new GameEventParam("NetworkUserId", networkUserId));

                                            linkAction(LinkSocialNetworkAction.Restore);
                                        }
                                        , Loc.Get("SocialLinkConflictLinkButton")
                                        , () =>
                                        {
                                            PersistentCore.instance.analytics.GameEvent
                                                ( "Social Link Conflict: Keep Current"
                                                , new GameEventParam("SocialNetwork", socialNetworkName)
                                                , new GameEventParam("NetworkUserId", networkUserId));

                                            linkAction(LinkSocialNetworkAction.SaveCurrent);
                                        }
                                        , Loc.Get("SocialLinkConflictKeepSeparateButton"));
                                }
                                else if (status == SendFriendsStatus.AnotherIDUsed)
                                {
                                    PersistentCore.instance.analytics.GameEvent
                                        ( "Social Link Conflict: Another ID Used"
                                        , new GameEventParam("SocialNetwork", socialNetworkName)
                                        , new GameEventParam("NetworkUserId", networkUserId));

                                    PersistentCore.instance.QueueModalConfirmMessage
                                        ( Loc.Get("CannotSendFriendsAnotherIDUsedHeader", socialNetworkName)
                                        , Loc.Get("CannotSendFriendsAnotherIDUsedMessage", socialNetworkName)
                                        , () =>
                                        {
                                            onComplete(status, LinkSocialNetworkStatus.NotInitialized);
                                        }
                                        , Loc.Get("idContinue"));
                                }
                                else
                                {
                                    if (silentErrors)
                                    {
                                        onComplete(status, LinkSocialNetworkStatus.NotInitialized);
                                    }
                                    else
                                    {
                                        ReportError
                                            ( status
                                            , Loc.Get("CannotSendFriends")
                                            , () =>
                                            {
                                                onComplete(status, LinkSocialNetworkStatus.NotInitialized);
                                            });
                                    }
                                }
                            });
                    }
                    else
                    {
                        if (silentErrors)
                        {
                            onComplete((SendFriendsStatus)(int)loginStatus, LinkSocialNetworkStatus.NotInitialized);
                        }
                        else
                        {
                            ReportError
                                ( loginStatus
                                , Loc.Get("CannotSendFriends")
                                , () =>
                                {
                                    onComplete((SendFriendsStatus)(int)loginStatus, LinkSocialNetworkStatus.NotInitialized);
                                });
                        }
                    }
                });
        }

        public void SendScores
            ( string name
            , int profileLevel
            , int popularity
            , bool silentErrors
            , OnSendScoresComplete onComplete )
        {
            InitiateLoginRequest
                ( false
                , (loginStatus) =>
                {
                    if (loginStatus == LoginStatus.OK)
                    {
                        networkCore.SendScores
                            ( loginData.profileId
                            , name
                            , profileLevel
                            , popularity
                            , (status, top, local) =>
                            {
                                if (status == SendScoresStatus.OK)
                                {
                                    onComplete(status, top, local);
                                }
                                else
                                {
                                    if (silentErrors)
                                    {
                                        onComplete(status, top, local);
                                    }
                                    else
                                    {
                                        ReportError
                                            ( status
                                            , Loc.Get("CannotSendScores")
                                            , () =>
                                            {
                                                onComplete(status, top, local);
                                            });
                                    }
                                }
                            });
                    }
                    else
                    {
                        if (silentErrors)
                        {
                            onComplete((SendScoresStatus)(int)loginStatus, null, null);
                        }
                        else
                        {
                            ReportError
                                ( loginStatus
                                , Loc.Get("CannotSendScores")
                                , () =>
                                {
                                    onComplete((SendScoresStatus)(int)loginStatus, null, null);
                                });
                        }
                    }
                });
        }

        public void StartInGameJobs (string saveGameFileName)
        {
            if (inGameSaveUploadJob == null)
            {
                if (networkCore.bootStatus == GenericNetworkStatus.OK || networkCore.isRecoverableBootStatus)
                {
                    Debug.Log("Queuing to start In-game save upload background routine...");
                    Assert.IsNull(mainSaveGameFileInfo);
                    mainSaveGameFileInfo = new FileInfo(saveGameFileName);
                    inGameSaveUploadJob = InGameSaveUploadJob();
                    StartCoroutine(inGameSaveUploadJob);
                }
                else
                {
                    Debug.LogFormat("In-game save upload routine will not be started, unrecoverable network initialization error: {0}.", networkCore.bootStatus);
                }
            }
        }

        public void CancelAllInGameOperations ()
        {
            networkCore.CancelAllRequests();

            if (inGameSaveUploadJob != null)
            {
                Debug.Log("Canceling In-game save upload background routine...");
                StopCoroutine(inGameSaveUploadJob);
                inGameSaveUploadJob = null;
                saveGameUpdateNotified = false;
            }

            mainSaveGameFileInfo = null;

            // Re-login required, so reset login data.
            loginData = null;
            loginStatus = LoginStatus.NotInitialized;
        }

        public void NotifySaveGameUpdated (bool forceImmediateUpdate)
        {
            if (mainSaveGameFileInfo != null)
            {
                mainSaveGameFileInfo.Refresh();
            }

            if (forceImmediateUpdate)
                forceImmediateSaveGameUpdate = true;

            saveGameUpdateNotified = true;

            // Reset downloaded file time stamps immediately after file is changed, they're not needed anymore.
            if (networkData.downloadedFileTimeStamps != null)
            {
                networkData.SetDownloadedFileTimeStamps(null);

                SaveNetworkData();
            }
        }

        public void SetSaveMetaData (int level, int experience)
        {
            networkData.SetLevelMeta(level);
            networkData.SetExperienceMeta(experience);

            SaveNetworkData();
        }
        #endregion

        #region Private methods.
        void SetDefaults ()
        {
            loginData = null;
            loginStatus = LoginStatus.NotInitialized;
            isNewClientID = true;
        }

        IEnumerator DelayedLoginComplete
            ( OnLoginComplete onComplete
            , LoginStatus status)
        {
            yield return null;

            onComplete(status);
        }

        void InitiateLoginRequest
            ( bool switchToSelf
            , OnLoginComplete onComplete
            , bool forceRelogin = false
            , bool startupLogin = false)
        {
            Assert.IsNotNull(networkCore);

            if (forceRelogin || loginStatus != LoginStatus.OK || loginData == null)
            {
                networkCore.Login
                    ( clientID
                    , switchToSelf
                    , (status, loginData, isNewClientID) =>
                    {
                        Assert.IsTrue(status != LoginStatus.OK || loginData != null);
                        this.loginStatus = status;
                        this.loginData = loginData;
                        this.isNewClientID = isNewClientID;

                        if (loginData != null && (networkData.profileId == null || networkData.profileId != loginData.profileId))
                        {
                            if (networkData.profileId == null || networkData.profileId == string.Empty)
                            {
                                Debug.LogFormat ("Profile (User) ID assigned: {0}.", loginData.profileId);
                            }
                            else
                            {
                                Debug.LogWarningFormat ("Profile (User) ID changed, resetting upload/download time stamps! Was: {0}, now: {1}. This is normal when user re-linked to another profile."
                                    , networkData.profileId, loginData.profileId);
                            }

                            networkData.SetProfileId(loginData.profileId);

                            Crashlytics.SetUserIdentifier(networkData.profileId);

                            // networkData.downloadedFileTimeStamps = null;
                            networkData.SetUploadedFileTimeStamps(null);

                            if (!SaveNetworkData())
                            {
                                onComplete(LoginStatus.NotInitialized);
                                return;
                            }
                        }

                        onComplete(status);
                    }
                    , startupLogin ? startupLoginConnectionTimeout : -1.0f
                    , startupLogin ? startupLoginTimeout : -1.0f);
            }
            else
            {
                StartCoroutine(DelayedLoginComplete(onComplete, loginStatus));
            }
        }

        void PerformStartupSync
            ( string saveGameFileName
            , OnStartupSyncComplete onComplete)
        {
            // Expect startup and sync complete.
            Assert.IsTrue(loginStatus == LoginStatus.OK);
            Assert.IsNotNull(loginData);

            Action<FileInfo> uploadSaveToServer = (FileInfo fileInfo) =>
            {
                PersistentCore.instance.splashScreenView.progressText = Loc.Get("SplashScreenMessageSyncUpload");

                TryUploadSaveToServer
                    ( fileInfo
                    , (uploadStatus) =>
                    {
                        // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                        switch (uploadStatus)
                        {
                            case SendProfileDataStatus.OK:
                            {
                                onComplete(StartupSynchronizationStatus.OK);
                            }; break;

                            case SendProfileDataStatus.Conflict:
                            {
                                loginData = null;
                                loginStatus = LoginStatus.NotInitialized;

                                PersistentCore.instance.analytics.GameEvent("Save Upload Conflict: Startup", "ProfileID", profileId);

                                PersistentCore.instance.QueueModalConfirmMessage
                                    ( Loc.Get("StartupSyncSaveConflictHeader")
                                    , Loc.Get("StartupSyncSaveConflictMessage")
                                    , () =>
                                    {
                                        onComplete(StartupSynchronizationStatus.InitializationFailedRetry);
                                    }
                                    , Loc.Get("idRetry"));
                            }; break;

                            default:
                            {
                                ReportError
                                    ( uploadStatus
                                    , Loc.Get("StartupSyncTryAgain")
                                    , () =>
                                    {
                                        onComplete(StartupSynchronizationStatus.InitializationFailedContinue);
                                    }
                                    , () =>
                                    {
                                        onComplete(StartupSynchronizationStatus.InitializationFailedRetry);
                                    });
                            }; break;
                        }
                    });
            };

            Action<FileInfo, bool> downloadSaveFromServer = (FileInfo fileInfo, bool expected) =>
            {
                PersistentCore.instance.splashScreenView.progressText = Loc.Get("SplashScreenMessageSyncDownload");

/*
                if (networkData.downloadedFileTimeStamps != null)
                {
                    networkData.downloadedFileTimeStamps = null;
                    if (!SaveNetworkData())
                    {
                        onComplete(StartupSynchornizationStatus.InitializationFailedWaitForever);
                        return;
                    }
                }
*/

                TryDownloadSaveFromServer
                    ( fileInfo
                    , (downloadStatus) =>
                    {
                        // PersistentCore.instance.splashScreenView.progressText = string.Empty;

                        switch (downloadStatus)
                        {
                            case GetProfileDataStatus.OK:
                            {
                                onComplete(StartupSynchronizationStatus.OK);
                            }; break;

                            case GetProfileDataStatus.NotFound:
                            {
                                if (expected)   // Are we really expecting existing save or it's optional?
                                {
                                    ReportError
                                        ( downloadStatus
                                        , Loc.Get("StartupSyncTryAgain")
                                        , () =>
                                        {
                                            onComplete(StartupSynchronizationStatus.InitializationFailedContinue);
                                        }
                                        , () =>
                                        {
                                            onComplete(StartupSynchronizationStatus.InitializationFailedRetry);
                                        });
                                }
                                else
                                {
                                    onComplete(StartupSynchronizationStatus.OK);
                                }
                            }; break;

                            default:
                            {
                                ReportError
                                    ( downloadStatus
                                    , Loc.Get("StartupSyncTryAgain")
                                    , () =>
                                    {
                                        onComplete(StartupSynchronizationStatus.InitializationFailedContinue);
                                    }
                                    , () =>
                                    {
                                        onComplete(StartupSynchronizationStatus.InitializationFailedRetry);
                                    });
                            }; break;
                        }
                    });
            };

            var sgFileInfo = new FileInfo(saveGameFileName);

            // TODO: Temporary logging, disable after testing!
#if DEBUG
            Debug.LogFormat ("SYNC DECISION! Our profile ID: {0}, Our client ID: {1}, last_cid: {2}, active_cid: {3}, profile_saved: {4}, Profile Save TS: {5}."
                , networkData.profileId
                , clientID
                , loginData.lastClientId
                , loginData.activeClientId
                , loginData.profileSaved.ToString("o")
                , sgFileInfo.Exists ? sgFileInfo.LastWriteTimeUtc.ToString("o") : "no save");
            Debug.LogFormat ("SYNC DECISION! Upload TS: {0}."
                , networkData.uploadedFileTimeStamps != null ? networkData.uploadedFileTimeStamps.ToString() : "none");
            Debug.LogFormat ("SYNC DECISION! Download TS: {0}."
                , networkData.downloadedFileTimeStamps != null ? networkData.downloadedFileTimeStamps.ToString() : "none");
#endif

            if (isNewClientID)
            {
                if (sgFileInfo.Exists)
                {   // Only local save game exists: dump it to server.
                    Debug.Log ("SYNC DECISION IS MADE: New Client id, local save file exists, trying to upload on server.");

                    PersistentCore.instance.analytics.GameEvent
                        ( "Startup Sync"
                        , new GameEventParam("Decision", "New Client ID, Upload")
                        , new GameEventParam("ProfileID", profileId));

                    uploadSaveToServer(sgFileInfo);
                }
                else
                {
                    Debug.Log ("SYNC DECISION IS MADE: New Client id, no local save file, no action is made.");

                    PersistentCore.instance.analytics.GameEvent
                        ( "Startup Sync"
                        , new GameEventParam("Decision", "New Client ID, No Save")
                        , new GameEventParam("ProfileID", profileId));

                    onComplete(StartupSynchronizationStatus.OK);
                }
            }
            else
            {
                if (sgFileInfo.Exists)
                {   // Local SaveGame exists and potentially server too. Complex sync.

                    if (loginData.lastClientId == clientID)
                    {   // We saved last file? Try to upload it again (if it's newer), no need to trying to download.
                        Debug.Log ("SYNC DECISION IS MADE: Existing Client id, last save is made by it, trying to upload file (if needed).");

                        PersistentCore.instance.analytics.GameEvent
                            ( "Startup Sync"
                            , new GameEventParam("Decision", "Existing Client ID, Upload")
                            , new GameEventParam("ProfileID", profileId));

                        uploadSaveToServer(sgFileInfo);
                    }
                    else if (networkData.uploadedFileTimeStamps != null &&
                            networkData.downloadedFileTimeStamps == null &&
                            networkData.uploadedFileTimeStamps.profileId == networkData.profileId &&
                            networkData.uploadedFileTimeStamps.fileTimeStamp != DateTime.MinValue &&
                            networkData.uploadedFileTimeStamps.fileTimeStamp == sgFileInfo.LastWriteTimeUtc.ToUniversalTime())
                    {
                        Debug.Log ("SYNC DECISION IS MADE: Existing Client id, no recently downloaded file, we uploaded file previously and it didn't changed locally from last upload - can safely download updated file from server.");

                        PersistentCore.instance.analytics.GameEvent
                            ( "Startup Sync"
                            , new GameEventParam("Decision", "Existing Client ID, Download")
                            , new GameEventParam("ProfileID", profileId));

                        downloadSaveFromServer(sgFileInfo, true);
                    }
                    else if (networkData.downloadedFileTimeStamps != null &&
                            networkData.downloadedFileTimeStamps.fileTimeStamp == sgFileInfo.LastWriteTimeUtc.ToUniversalTime() &&
                            networkData.downloadedFileTimeStamps.profileId == networkData.profileId &&
                            networkData.downloadedFileTimeStamps.uploadServerTimeStamp == loginData.profileSaved)
                    {
                        Debug.Log ("SYNC DECISION IS MADE: Existing Client id, recently downloaded save file and it didn't changed locally or on server, no action is needed.");

                        PersistentCore.instance.analytics.GameEvent
                            ( "Startup Sync"
                            , new GameEventParam("Decision", "Existing Client ID, No Action")
                            , new GameEventParam("ProfileID", profileId));

                        // Case, when we downloaded profile previously (saved by any Client ID) but it wasn't changed on server from this time and
                        // also our local save game is not changed yet - silently accept this situation.
                        onComplete(StartupSynchronizationStatus.OK);
                    }
                    else
                    {   // Ok, there's modifications both to server and client versions of save game.
                        // Let's user decide.

                        Debug.Log ("SYNC DECISION BY USER: There's modifications both to server and client versions of save game, user needs to take action.");

                        PersistentCore.instance.analytics.GameEvent
                            ( "Startup Sync"
                            , new GameEventParam("Decision", "Existing Client ID, Conflict")
                            , new GameEventParam("ProfileID", profileId));

                        var timeNow = DateTime.UtcNow;

                        var cloudModificationDateAgo = (timeNow - loginData.profileSaved - clientFromServerTimeDifference);
                        if (cloudModificationDateAgo < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat
                                ( "Negative Cloud modification time difference! Time now {0}, server's profile saved: {1}, client time difference from server: {2}"
                                , timeNow
                                , loginData.profileSaved
                                , clientFromServerTimeDifference);
                            cloudModificationDateAgo = TimeSpan.Zero;
                        }

                        var localModificationDateAgo = (timeNow - sgFileInfo.LastWriteTimeUtc.ToUniversalTime());
                        if (localModificationDateAgo < TimeSpan.Zero)
                        {
                            Debug.LogWarningFormat
                                ( "Negative Local modification time difference! Time now {0}, server last write time: {2}"
                                , timeNow
                                , sgFileInfo.LastWriteTimeUtc);
                            localModificationDateAgo = TimeSpan.Zero;
                        }

                        var expLevels = new Data.PlayerProfile.ExperienceLevels(DataTables.instance.experienceLevels);

                        PersistentCore.instance.saveConflictView.Show
                            ( loginData.level
                            , expLevels.GetExpRatioStr(loginData.experience, loginData.level)  //loginData.experience
                            , expLevels.GetExpRatio(loginData.experience, loginData.level) 
                            , cloudModificationDateAgo
                            , networkData.levelMeta
                            , expLevels.GetExpRatioStr(networkData.experienceMeta, networkData.levelMeta) //networkData.experienceMeta
                            , expLevels.GetExpRatio(networkData.experienceMeta, networkData.levelMeta)
                            , localModificationDateAgo
                            , () =>
                            {
                                Debug.Log("SYNC DECISION IS MADE: User selected cloud save game version, trying to download it (and expecting it on server)!");

                                PersistentCore.instance.analytics.GameEvent
                                    ( "Startup Sync: User Decision"
                                    , new GameEventParam("Decision", "Use Server")
                                    , new GameEventParam("ProfileID", profileId));

                                downloadSaveFromServer(sgFileInfo, true);
                            }
                            , () =>
                            {
                                Debug.Log("SYNC DECISION IS MADE: User selected local save game version, trying to upload it on server (still can have conflict)!");

                                PersistentCore.instance.analytics.GameEvent
                                    ( "Startup Sync: User Decision"
                                    , new GameEventParam("Decision", "Use Local")
                                    , new GameEventParam("ProfileID", profileId));

                                uploadSaveToServer(sgFileInfo);
                            });
                    }
                }
                else
                {   // No local save game exists, try to check for network save and download it.
                    Debug.Log ("SYNC DECISION IS MADE: Existing Client id, no save file present, trying to download from server (OK, if no save on server).");

                    PersistentCore.instance.analytics.GameEvent
                        ( "Startup Sync"
                        , new GameEventParam("Decision", "Existing Client ID, No Save, Download")
                        , new GameEventParam("ProfileID", profileId));

                    if (networkData.uploadedFileTimeStamps != null || networkData.downloadedFileTimeStamps != null)
                    {
                        networkData.SetUploadedFileTimeStamps(null);
                        networkData.SetDownloadedFileTimeStamps(null);

                        if (!SaveNetworkData())
                        {
                            onComplete(StartupSynchronizationStatus.InitializationFailedWaitForever);
                            return;
                        }
                    }

                    downloadSaveFromServer(sgFileInfo, false);
                }
            }
        }

        void TryUploadSaveToServer
            ( FileInfo saveGameFileInfo
            , OnSendProfileDataComplete onComplete)
        {
            Assert.IsNotNull(saveGameFileInfo);
            if (saveGameFileInfo != null) // Paranoid check because of reported crash via Crashlytics.
            {
                saveGameFileInfo.Refresh();
                if (saveGameFileInfo.Exists && saveGameFileInfo.Length > 0)
                {
                    // Correct "Last write time" of save file to current time if it's in future.
                    DateTime saveGameLastWriteTime = DateTime.MinValue;
                    try
                    {
                        saveGameLastWriteTime = saveGameFileInfo.LastWriteTimeUtc.ToUniversalTime();
                        var currentTime = DateTime.UtcNow;
                        if (saveGameLastWriteTime > currentTime)
                        {
                            Debug.LogWarning("Save game time stamp in future! Correcting to current time...");

                            saveGameLastWriteTime = currentTime;
                            saveGameFileInfo.LastWriteTimeUtc = currentTime;
                            saveGameFileInfo.Refresh();
                        }
                    }
                    catch (Exception)
                    {
                        onComplete(SendProfileDataStatus.FileIOError);
                        return;
                    }

                    if (loginData == null)
                    {
                        onComplete(SendProfileDataStatus.NotInitialized);
                        return;
                    }

                    if (loginData.activeClientId != clientID)
                    {
                        onComplete(SendProfileDataStatus.Conflict); // Immediate conflict because we're were not active even when logged in. No need to send.
                        return;
                    }
                    else
                    {
                        if (isNewClientID ||
                             networkData.uploadedFileTimeStamps == null ||
                             networkData.profileId != networkData.uploadedFileTimeStamps.profileId ||
                             saveGameLastWriteTime > networkData.uploadedFileTimeStamps.fileTimeStamp)
                        {
                            networkCore.SendProfileData
                                ( networkData.profileId
                                , saveGameFileInfo
                                , networkData.levelMeta
                                , networkData.experienceMeta
                                , (status, uploadTimeStamps) =>
                                {
                                    if (status == SendProfileDataStatus.OK)
                                    {
                                        Assert.IsNotNull(uploadTimeStamps);
                                        networkData.SetUploadedFileTimeStamps(uploadTimeStamps);
                                        if (!SaveNetworkData())
                                        {
                                            onComplete(SendProfileDataStatus.NotInitialized);
                                            return;
                                        }

                                        isNewClientID = false;
                                    }

                                    onComplete(status);
                                });
                            return;
                        }
                    }
                }
            }

            // No save or save game is not updated after last sync.
            onComplete(SendProfileDataStatus.OK);
        }

        void TryDownloadSaveFromServer
            (FileInfo saveGameFileInfo
            , OnGetProfileDataComplete onComplete)
        {
            Assert.IsNotNull(saveGameFileInfo);

            var tmpSaveFileInfo = new FileInfo(saveGameFileInfo.FullName + ".tmp");
            try
            {
                if (tmpSaveFileInfo.Exists)
                {
                    tmpSaveFileInfo.Delete();
                }
            }
            catch (Exception)
            {
                onComplete(GetProfileDataStatus.FileIOError);
            }

            networkCore.GetProfileData
                ( networkData.profileId
                , tmpSaveFileInfo
                , (status, downloadTimeStamps) =>
                {
                    if (status == GetProfileDataStatus.OK)
                    {
                        Assert.IsNotNull(downloadTimeStamps);

                        try
                        {
                            // Also back up last local save game file into ".last" extension.
                            if (saveGameFileInfo.Exists)
                                FileHelpers.SafeCommitFile(saveGameFileInfo.FullName, saveGameFileInfo.FullName + ".last");

                            FileHelpers.SafeCommitFile(tmpSaveFileInfo.FullName, saveGameFileInfo.FullName);
                            saveGameFileInfo.Refresh();

                            Debug.LogFormat("Downloaded file real TS: {0}, TS data: '{1}'", saveGameFileInfo.LastWriteTimeUtc.ToString("o"), downloadTimeStamps.ToString());

                            networkData.SetDownloadedFileTimeStamps(downloadTimeStamps);
                            networkData.SetUploadedFileTimeStamps(null); // Must be reset when downloaded time stamps is set.

                            if (!SaveNetworkData())
                            {
                                status = GetProfileDataStatus.NotInitialized;
                            }
                        }
                        catch (Exception)
                        {
                            networkData.SetDownloadedFileTimeStamps(null);
                            SaveNetworkData();

                            status = GetProfileDataStatus.FileIOError;
                        }
                    }

                    if (status != GetProfileDataStatus.OK)
                    {
                        PersistentCore.instance.analytics.GameEvent("SaveGame Download Failed", "Status", status.ToString());
                    }

                    onComplete(status);
                });
        }

        private IEnumerator InGameSaveUploadJob ()
        {
            Assert.IsNotNull(mainSaveGameFileInfo);
            var lastKnownModificationTime = mainSaveGameFileInfo.Exists ? mainSaveGameFileInfo.LastWriteTimeUtc : DateTime.MinValue;
            var uploadRate = saveGameUploadRate > 1.0f ? saveGameUploadRate : 1.0f;
            var uploadRateAfterFailure = saveGameUploadRateAfterFailure > 1.0f ? saveGameUploadRate : 1.0f;

            var nextUpdateTime = Time.time;
            var busy = false;

            yield return null;  // Skip first invocation.

            if (saveUploadJobStartupDelay > 0)
            {
                yield return new WaitForSeconds(saveUploadJobStartupDelay);
            }

            Action<bool> uploadSaveToServer = (bool wasForceUpdate) =>
            {
                busy = true;

                TryUploadSaveToServer
                    ( mainSaveGameFileInfo
                    , (uploadStatus) =>
                    {
                        busy = false;

                        switch (uploadStatus)
                        {
                            case SendProfileDataStatus.OK:
                            {
                                Debug.Log("Save Game successfully uploaded. Waiting for next update...");

                                nextUpdateTime = Time.time + uploadRate;
                                if (mainSaveGameFileInfo != null)
                                {
                                    mainSaveGameFileInfo.Refresh();
                                    lastKnownModificationTime = mainSaveGameFileInfo.LastWriteTimeUtc.ToUniversalTime();
                                }

                            }; break;

                            case SendProfileDataStatus.Conflict:
                            {
                                busy = true; // Stop update process!
                                if (mainSaveGameFileInfo != null)
                                    mainSaveGameFileInfo = null; // This will cause exit from Coroutine loop.

                                PersistentCore.instance.analytics.GameEvent("Save Upload Conflict: In-Game", "ProfileID", profileId);

                                PersistentCore.instance.QueueModalConfirmMessage
                                    ( Loc.Get("InGameSyncSaveConflictHeader")
                                    , Loc.Get("InGameSyncSaveConflictMessage")
                                    , () =>
                                    {
                                        PersistentCore.ScheduleFullReload();
                                    }
                                    , Loc.Get("idRestart"));
                            }; break;

                            default:
                            {
                                Debug.LogWarningFormat("Failed to upload save game. Error: {0}. Waiting for next update...", uploadStatus);

                                nextUpdateTime = Time.time + uploadRateAfterFailure;
                                if (mainSaveGameFileInfo != null)
                                {
                                    mainSaveGameFileInfo.Refresh();
                                }
                                if (wasForceUpdate)
                                    forceImmediateSaveGameUpdate = true; // Schedule it again ASAP.

                                // TODO: Notification via icon, non-modal popup and/or local notification (if app is not in the focus).
                            }; break;
                        }
                    });
            };

            Debug.Log("Starting in-game save upload routine...");

            if (!mainSaveGameFileInfo.Exists)
            {
                Debug.Log("No save game file exists yet, will wait until it appears...");
                yield return new WaitUntil (() => saveGameUpdateNotified && mainSaveGameFileInfo.Exists);
            }

            while (true)
            {
                if (mainSaveGameFileInfo == null || !mainSaveGameFileInfo.Exists)
                    break;

                yield return new WaitUntil
                    (() =>
                        mainSaveGameFileInfo == null || !mainSaveGameFileInfo.Exists ||
                        (!busy && saveGameUpdateNotified && (forceImmediateSaveGameUpdate || Time.time >= nextUpdateTime) && (mainSaveGameFileInfo.LastWriteTimeUtc.ToUniversalTime() > lastKnownModificationTime))
                    );

                if (mainSaveGameFileInfo == null || !mainSaveGameFileInfo.Exists)
                    break;

                saveGameUpdateNotified = false;

                Debug.Log("Game file modified and time until next save is passed. Trying to upload save game...");

                var wasForceUpdate = forceImmediateSaveGameUpdate;
                forceImmediateSaveGameUpdate = false;

                busy = true;
                InitiateLoginRequest
                    ( false
                    , (loginStatus) =>
                    {
                        busy = false;

                        if (loginStatus == LoginStatus.OK)
                        {
                            uploadSaveToServer(wasForceUpdate);
                        }
                        else
                        {
                            nextUpdateTime = Time.time + uploadRateAfterFailure;
                            //
                        }
                    });
            }

            Debug.LogWarning("In-game save upload routine will exit, unrecoverable error (conflict or no save game file exists)...");

            inGameSaveUploadJob = null;
            yield break;
        }


        void ReportGenericError
            ( GenericNetworkStatus status
            , string customMessage
            , Action onContinue
            , Action onRetry = null)
        {
            Assert.IsNotNull(onContinue);

            PersistentCore.instance.analytics.GameEvent("Modal Window: " + typeof(GenericNetworkStatus).Name, "Status", status.ToString());

            var header = Loc.Get(string.Format("{0}{1}Header", typeof(GenericNetworkStatus).Name, status.ToString()));

            switch (status)
            {
                case GenericNetworkStatus.OutdatedVersion:
                {
                    var message = Loc.Get(string.Format("{0}{1}Message", typeof(GenericNetworkStatus).Name, status.ToString()), customMessage);

#if DEBUG
                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , onContinue
                        , Loc.Get("idContinue"));
#else
                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , delegate
                        {
                            PersistentCore.instance.OpenApplicationMarketPage();
                            PersistentCore.Quit(false);
                        }
                        , Loc.Get("UpdateGameButton")
                        , onContinue
                        , Loc.Get("idContinue"));
#endif
                };  return;

                case GenericNetworkStatus.TooHighVersion:
                {
                    var message = Loc.Get(string.Format("{0}{1}Message", typeof(GenericNetworkStatus).Name, status.ToString()), customMessage);

                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , onContinue
                        , Loc.Get("idContinue")
                        , () =>
                        {
                            PersistentCore.Quit(false);
                        }
                        , Loc.Get("idQuitGame"));
                };  return;

                case GenericNetworkStatus.ServerMaintenance:
                {
                    var message = Loc.Get(string.Format("{0}{1}Message", typeof(GenericNetworkStatus).Name, status.ToString()), customMessage, networkCore.netBootData.maintenanceMessage);

                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , onContinue
                        , Loc.Get("idContinue")
                        , () =>
                        {
                            PersistentCore.Quit(false);
                        }
                        , Loc.Get("idQuitGame"));
                };  break; 

                default:
                {
                    var message = Loc.Get(string.Format("{0}{1}Message", typeof(GenericNetworkStatus).Name, status.ToString()), customMessage);

                    if (onRetry != null)
                    {
                        PersistentCore.instance.QueueModalConfirmMessage
                            ( header
                            , message
                            , onContinue
                            , Loc.Get("idContinue")
                            , onRetry
                            , Loc.Get("idRetry")
                            , (status == GenericNetworkStatus.NetworkUnavailable || status == GenericNetworkStatus.ConnectionFailed)
                                ? ModalConfirmWindow.IconType.NetworkConnection
                                : ModalConfirmWindow.IconType.Default );
                    }
                    else
                    {
                        PersistentCore.instance.QueueModalConfirmMessage
                            ( header
                            , message
                            , onContinue
                            , Loc.Get("idContinue")
                            , null
                            , string.Empty
                            , (status == GenericNetworkStatus.NetworkUnavailable || status == GenericNetworkStatus.ConnectionFailed)
                                ? ModalConfirmWindow.IconType.NetworkConnection
                                : ModalConfirmWindow.IconType.Default );
                    }
                };  break;
            }
        }

        void ReportError<StatusT>
            ( StatusT status
            , string customMessage
            , Action onContinue
            , Action onRetry = null) where StatusT : struct, IConvertible // Partial enum constraint
        {
            Assert.IsNotNull(onContinue);

            var intValue = Convert.ToInt32(status);
            if (intValue <= (int)GenericNetworkStatus.NotInitialized)
            {
                ReportGenericError ((GenericNetworkStatus)intValue, customMessage, onContinue, onRetry);
            }
            else
            {
                PersistentCore.instance.analytics.GameEvent("Modal Window: " + typeof(StatusT).Name, "Status", status.ToString());

                var header = Loc.Get(string.Format("{0}{1}Header", typeof(StatusT).Name, status.ToString()));
                var message = Loc.Get(string.Format("{0}{1}Message", typeof(StatusT).Name, status.ToString()), customMessage);

                if (onRetry != null)
                {
                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , onContinue
                        , Loc.Get("idContinue")
                        , onRetry
                        , Loc.Get("idRetry"));
                }
                else
                {
                    PersistentCore.instance.QueueModalConfirmMessage
                        ( header
                        , message
                        , onContinue
                        , Loc.Get("idContinue"));
                }
            }
        }

        bool LoadNetworkData ()
        {
#if DEBUG
            string forcedClientID = null;
            var forcedClientIDPath = ResourcePaths.Network.debugForceClientIDFullPath;

            if (File.Exists(forcedClientIDPath))
            {
                try
                {
                    using (var fileStream = File.OpenText(forcedClientIDPath))
                    {
                        forcedClientID = fileStream.ReadToEnd();
                        if (forcedClientID == string.Empty)
                            forcedClientID = null;
                    }
                    if (forcedClientID == string.Empty)
                        forcedClientID = null;

                    if (forcedClientID != null)
                    {
                        Debug.LogWarningFormat("Forced Debug Network Client ID will be used: {0}", forcedClientID);
                        forceStartupSync = true;
                    }
                }
                catch (Exception)
                { }
            }
#endif

            try
            {
                var filePath = ResourcePaths.Network.networkDataFullPath;
                Debug.LogFormat("Network data file path: '{0}'", filePath);

                networkData = NetworkData.Load(filePath, ekBytes, jsonSerializerSettings);
            }
            catch (Exception ex)
            {
            	Debug.LogException(ex);
                networkData = null;
            }
            
            bool result = false;
            if (networkData == null)
            {
#if DEBUG
                networkData = new NetworkData(forcedClientID != null ? forcedClientID : GenerateClientID());
#else
                networkData = new NetworkData(GenerateClientID());
#endif
                result = SaveNetworkData();
            }
            else
            {
#if DEBUG
                if (forcedClientID != null)
                {
                    networkData.SetClientID (forcedClientID);
                    result = SaveNetworkData();
                }
                else
#endif
                {
                    result = true;
                }
            }

#if DEBUG
            // Never print Client ID in release builds to log!
            Debug.LogFormat("Network Client ID: {0}", networkData.clientID);
#endif
            return result;
        }

        bool SaveNetworkData ()
        {
            Assert.IsNotNull(networkData);

            var filePath = ResourcePaths.Network.networkDataFullPath;

            try
            {
                networkData.Save(filePath, ekBytes, jsonSerializerSettings);
                return true;
            }
            catch (Exception ex)
            {
                PersistentCore.instance.analytics.GameEvent("NetData Saving Failed", "Error", ex.Message);

                SetDefaults();

                PersistentCore.instance.QueueModalConfirmMessage
                    ( Loc.Get("NetworkDataStorageSaveFailedHeader")
                    , Loc.Get("NetworkDataStorageSaveFailedMessage")
                    , delegate
                    {
                        PersistentCore.Quit(true);
                    }
                    , Loc.Get("idQuitGame"));
                return false;
            }
        }

        string GenerateClientID ()
        {
            var idBytes = new byte[clientIdBytesLength];

            {
                var rng = new RNGCryptoServiceProvider();
                rng.GetBytes(idBytes);
            }

            // ID is a Base64 string with 2 URL unsafe chars replaced with a safe analogs: '+' with '-' and '/' with '_'.
            return Convert.ToBase64String(idBytes).Replace('+', '-').Replace('/', '_');
        }

        private void OnNetworkStatusUpdated (GenericNetworkStatus status)
        {
            if (lastNetworkStatus != status)
            {
                lastNetworkStatus = status;

                if (lastNetworkStatusChangedNotifyJob == null)
                {
                    lastNetworkStatusChangedNotifyJob = LastNetworkStatusChangedNotifyJob();
                    StartCoroutine(lastNetworkStatusChangedNotifyJob);
                }
            }
        }

        private IEnumerator LastNetworkStatusChangedNotifyJob()
        {
            yield return null;

            onNetworkStatusChanged?.Invoke(lastNetworkStatus);

            Assert.IsNotNull(lastNetworkStatusChangedNotifyJob);
            lastNetworkStatusChangedNotifyJob = null;
        }
        #endregion

        #region Private static functions.
        private static byte[] EKBytes (string resourceFileName)
        {
            var keyResource = Resources.Load(resourceFileName) as TextAsset;
            return (keyResource == null || keyResource.bytes == null || keyResource.bytes.Length < 8) ? null : keyResource.bytes;
        }
        #endregion
    }

    // NOTE: Placed this after main class because Unity going crazy and cannot find MonoBehaviour if these types are placed first.

    public enum StartupSynchronizationStatus
    {
        OK = 0,
        InitializationFailedContinue,
        InitializationFailedRetry,
        InitializationFailedQuit,
        InitializationFailedWaitForever
    }

    public enum DownloadFriendSaveStatus
    {
        OK = 0,
        Failed,
    }

    public delegate void OnStartupSyncComplete (StartupSynchronizationStatus status);
    public delegate void OnSendFriendsComplete (SendFriendsStatus status, LinkSocialNetworkStatus linkStatus, LinkSocialNetworkAction actionChoosen = LinkSocialNetworkAction.SaveCurrent);
    public delegate void OnDownloadFriendSaveComplete (DownloadFriendSaveStatus status);
}