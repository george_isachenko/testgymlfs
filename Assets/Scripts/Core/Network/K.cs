﻿namespace Core.Network
{
    // Encryption system data stuff class.
    public static class K
    {
        // Client ID salt.
        public static readonly byte[] cids = new byte[] { 0xaf, 0x60, 0x9e, 0x9c, 0xd3, 0xb2, 0x7f, 0x74 };
    }
}
