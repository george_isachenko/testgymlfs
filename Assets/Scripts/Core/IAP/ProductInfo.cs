﻿using UnityEngine.Purchasing;

namespace Core.IAP
{
    public struct ProductInfo
    {
        public string id;
        public ProductType type;
        public bool availableToPurchase;
        public ProductMetadata metaData;

        public ProductInfo(string id, ProductType type, bool availableToPurchase, ProductMetadata metaData)
        {
            this.id = id;
            this.type = type;
            this.availableToPurchase = availableToPurchase;
            this.metaData = metaData;
        }
    }
}