﻿using System;
using System.Collections.Generic;
using System.IO;
using Core.Network;
using Data.IAP;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Purchasing;

#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
using UnityEngine.Purchasing.Security;
#endif

namespace Core.IAP
{
    public delegate void PurchaseFailedEvent (Product product, PurchaseFailureReason failureReason);
    public delegate bool ProcessPurchaseEvent (Product product, string storeID);

    public class IAPManager : IStoreListener
    {
        #region Types.
        public enum InitializationState
        {
            Uninitialized = 0,
            Pending,
            Initialized,
            Failed
        }

        private struct DelayedPurchaseInfo
        {
            public Product product;
            public bool success;
            public PurchaseFailureReason failureReason;

            public DelayedPurchaseInfo (Product product, bool success, PurchaseFailureReason failureReason = PurchaseFailureReason.Unknown)
            {
                this.product = product;
                this.success = success;
                this.failureReason = failureReason;
            }
        }

#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        private struct ReceiptData
        {   // DO NOT RENAME THIS FIELDS, they must correspond to receipt structure described here:
            // https://docs.unity3d.com/Manual/UnityIAPPurchaseReceipts.html
            public string Store;            // The name of the store in use, such as GooglePlay or AppleAppStore
            public string TransactionID; 	// This transaction’s unique identifier, provided by the store
            public string Payload;          // Varies by platform.

            public ReceiptData(string Store, string TransactionID, string Payload)
            {   // Not used, declared only because of warning about unused fields.
                this.Store = Store;
                this.TransactionID = TransactionID;
                this.Payload = Payload;
            }
        }
#endif

        #endregion

        #region Events
        public event PurchaseFailedEvent            onPurchaseFailed;
        public event ProcessPurchaseEvent           onProcessPurchase;
        #endregion

        #region Private Data
        Action<bool, InitializationFailureReason?>  initializationCallback;
        IDictionary<string, IAPEntryData>           iapEntries;
        List<DelayedPurchaseInfo>                   delayedPurchases = new List<DelayedPurchaseInfo>(4);
        IStoreController                            controller;
        IExtensionProvider                          extensions;
        string                                      storeID;
        #endregion

        #region Properties.
        public InitializationState                  initializationState             { get; private set; }
        public InitializationFailureReason          initializationFailureReason     { get; private set; }
        #endregion

        #region Public API
        public IAPManager (IDictionary<string, IAPEntryData> iapEntries)
        {
            Assert.IsNotNull(iapEntries);
            this.iapEntries = iapEntries;

#if UNITY_IOS || UNITY_STANDALONE_OSX
            storeID = AppleAppStore.Name;
#elif UNITY_ANDROID
            storeID = GooglePlay.Name;
#else
            storeID = "Unknown";
#endif
        }

        public void Init
            ( string googlePlayPublicKey
            , FakeStoreUIMode fakeStoreUIMode = FakeStoreUIMode.Default
            , Action<bool, InitializationFailureReason?> initializationCallback = null)
        {
            if (initializationState == InitializationState.Uninitialized)
            {
                if (storeID != null)
                {
                    this.initializationCallback = initializationCallback;

                    var purchasingModuleInstance = StandardPurchasingModule.Instance();
#if DEBUG
                    purchasingModuleInstance.useFakeStoreUIMode = fakeStoreUIMode;
#endif
                    var builder = ConfigurationBuilder.Instance(purchasingModuleInstance);

#if UNITY_ANDROID
                    var googlePlayConfiguration = builder.Configure<IGooglePlayConfiguration>();
                    if (googlePlayConfiguration != null)
                    {
                        googlePlayConfiguration.SetPublicKey(googlePlayPublicKey);
                    }
#endif

#if UNITY_IOS || UNITY_STANDALONE_OSX
                    var appleConfiguration = builder.Configure<IAppleConfiguration>();
                    if (appleConfiguration != null)
                    {
                        if (!appleConfiguration.canMakePayments)
                        {
                            Debug.LogWarning("Payments restricted in devices settings.");
                            if (initializationCallback != null)
                            {
                                initializationCallback(false, InitializationFailureReason.PurchasingUnavailable);
                            }
                            return;
                        }
                    }
#endif

                    if (iapEntries != null)
                    {
                        foreach (var iapEntry in iapEntries)
                        {
                            if (iapEntry.Value.active)
                            {
                                builder.AddProduct(iapEntry.Key, iapEntry.Value.productType, new IDs { { iapEntry.Value.marketId, storeID } } );
                                //  Debug.LogFormat("IAP add product {0}, {1}, {2}", iapEntry.Key, iapEntry.Value.marketId, iapEntry.Value.productType);
                            }
                        }
                    }

                    initializationState = InitializationState.Pending;
                    Debug.Log("IAP going to initialize...");
                    UnityPurchasing.Initialize (this, builder);
                }
                else
                {
                    Debug.LogWarningFormat("Unsupported Platform of Store: {0}", Application.platform);
                    initializationCallback?.Invoke(false, InitializationFailureReason.PurchasingUnavailable);
                }
            }
        }

        public void Update()
        {
            if (initializationState == InitializationState.Initialized &&
                delayedPurchases.Count > 0 &&
                onPurchaseFailed != null &&
                onProcessPurchase != null)
            {
                var listCopy = delayedPurchases.ToArray(); // Make a copy because it can be updated while iterating.
                delayedPurchases.Clear();

                foreach (var p in listCopy) 
                {
                    if (p.success)
                    {
                        if (onProcessPurchase(p.product, storeID))
                        {
                            controller.ConfirmPendingPurchase(p.product);
                        }
                    }
                    else
                    {
                        onPurchaseFailed(p.product, p.failureReason);
                    }
                }
            }
        }

        public bool InitializeRestoreProductsRequest(Action<bool> callback)
        {
            if (initializationState == InitializationState.Initialized)
            {
                if (extensions != null)
                {
#if UNITY_IOS || UNITY_STANDALONE_OSX
                    var appleExtension = extensions.GetExtension<IAppleExtensions>();
                    if (appleExtension != null)
                    {
                        appleExtension.RestoreTransactions(callback);
                        return true;
                    }
#endif
                }
            }
            else
            {
                Debug.LogErrorFormat("Wrong initialization state {0} ({1})."
                    , initializationState
                    , initializationFailureReason);
            }
            return false;
        }

        public bool InitiatePurchaseRequest (string iapProductId, string payload = null)
        {
            if (initializationState == InitializationState.Initialized)
            {
                if (payload != null)
                {
                    controller.InitiatePurchase(iapProductId, payload);
                }
                else
                {
                    controller.InitiatePurchase(iapProductId);
                }
                return true;
            }
            else
            {
                Debug.LogErrorFormat("Wrong initialization state {0} ({1})."
                    , initializationState
                    , initializationFailureReason);
            }
            return false;
        }

        public bool ConfirmPendingPurchase (string iapProductId)
        {
            if (initializationState == InitializationState.Initialized)
            {
                var product = controller.products.WithID(iapProductId);

                if (product != null)
                {
                    controller.ConfirmPendingPurchase(product);
                    return true;
                }
            }
            else
            {
                Debug.LogErrorFormat("Wrong initialization state {0} ({1})."
                    , initializationState
                    , initializationFailureReason);
            }
            return false;
        }

        public bool GetProductInfo (string iapProductId, out ProductInfo productInfo)
        {
            IAPEntryData iapEntry;
            if (iapEntries != null && iapEntries.TryGetValue(iapProductId, out iapEntry))
            {
                Product product;
                if (initializationState == InitializationState.Initialized &&
                    controller != null &&
                    (product = controller.products.WithID(iapProductId)) != null)
                {
                    productInfo = new ProductInfo
                        (iapProductId
                        , iapEntry.productType
                        , iapEntry.active && product.availableToPurchase
                        , product.metadata );
                    return true;
                }
                else
                {
                    var priceString = string.Format("$ {0}", iapEntry.defaultCost);
                    productInfo = new ProductInfo
                        ( iapProductId
                        , iapEntry.productType
                        , iapEntry.active
                        , new ProductMetadata (priceString, "?", "?", "USD", iapEntry.defaultCost));
                    return true;
                }
            }

            Debug.LogErrorFormat("IAP with Product ID '{0}' not found.", iapProductId);
            productInfo = default(ProductInfo);
            return false;
        }
        #endregion

        #region Private API
        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Assert.IsTrue(initializationState == InitializationState.Pending);

            Assert.IsNotNull(controller);
            this.controller = controller;
            this.extensions = extensions;

#if UNITY_IOS || UNITY_STANDALONE_OSX
            var appleExtensions = extensions.GetExtension<IAppleExtensions>();
            if (appleExtensions != null)
            {
                // TODO: Implement this feature.
                // See https://docs.unity3d.com/Manual/UnityIAPiOSMAS.html
                // and https://developer.apple.com/library/content/technotes/tn2259/_index.html#//apple_ref/doc/uid/DTS40009578-CH1-UPDATE_YOUR_APP_FOR_ASK_TO_BUY
                appleExtensions.RegisterPurchaseDeferredListener(product =>
                {
                    Debug.LogFormat("Product {0} deferred for approval.", product.definition.id);
                });
            }
#endif

            initializationState = InitializationState.Initialized;
            Debug.Log("Initialized.");

            if (initializationCallback != null)
            {
                var tmpCallback = initializationCallback;
                initializationCallback = null;
                tmpCallback.Invoke(true, null);
            }
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            Assert.IsTrue(initializationState == InitializationState.Pending);

            initializationState = InitializationState.Failed;
            initializationFailureReason = error;
            Debug.LogWarningFormat("Failed to initialize: {0}.", error);

            if (initializationCallback != null)
            {
                var tmpCallback = initializationCallback;
                initializationCallback = null;
                tmpCallback.Invoke(false, error);
            }
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (failureReason == PurchaseFailureReason.UserCancelled)
            {
                Debug.LogFormat("Purchase failed. Product id: {0}, Failure reason: {1}."
                    , product.definition.id, failureReason);
            }
            else
            {
                Debug.LogWarningFormat("Purchase failed. Product id: {0}, Failure reason: {1}."
                    , product.definition.id, failureReason);
            }

            if (onPurchaseFailed != null)
            {
                onPurchaseFailed (product, failureReason);
            }
            else
            {
                delayedPurchases.Add(new DelayedPurchaseInfo(product, false, failureReason));
            }
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs eventArgs)
        {
            Debug.LogFormat("Purchase completed. Product id: {0}."
                , eventArgs.purchasedProduct.definition.id);

            var product = eventArgs.purchasedProduct;

            if (!Application.isEditor && CoreDebugHacks.iapDumpProductReceipt)
            {
                try
                {
                    using (var writer = File.CreateText(string.Format("{0}{1}Receipt_{2}_{3}.json"
                        , Application.persistentDataPath
                        , Path.DirectorySeparatorChar
                        , storeID
                        , product.transactionID.Substring(0, Math.Min(product.transactionID.Length, 15)))))
                    {
                        writer.Write(product.receipt);
                    }
                }
                catch (Exception)
                { }
            }

            string debugReceipt = null;
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                try
                {
                    using (var reader = File.OpenText(string.Format("{0}/Debug/IAP/TestReceipt_{1}.json"
                        , Application.dataPath
                        , storeID).Replace('/', Path.DirectorySeparatorChar)))
                    {
                        debugReceipt = reader.ReadToEnd();
                    }
                }
                catch (Exception)
                { }
                
                if (debugReceipt == null)
                    return PurchaseProcessingResult.Complete;
            }
#endif

            if (!ValidatePurchaseLocal(product, debugReceipt))
            {
                Debug.LogErrorFormat("Can't perform local validation of In-App purchase for product id: '{0}'.", product.definition.storeSpecificId);

                if (onPurchaseFailed != null)
                {
                    onPurchaseFailed (product, PurchaseFailureReason.SignatureInvalid);
                }
                else
                {
                    delayedPurchases.Add(new DelayedPurchaseInfo(product, false, PurchaseFailureReason.SignatureInvalid));
                }

                return PurchaseProcessingResult.Complete;
            }

            if (CoreDebugHacks.iapConfirmReceiptValidation)
            {
                PersistentCore.instance.QueueModalConfirmMessage
                    ( "Online Receipt validation Debug"
                    , string.Format("Send receipt for product purchase '{0}' for online validation or leave product in pending state?", product.definition.storeSpecificId)
                    , delegate
                    {
                        ValidatePurchaseOnline(product, debugReceipt);
                    }
                    , "Send"
                    , delegate
                    {
                    }
                    , "Leave");
            }
            else
            {
                ValidatePurchaseOnline(product, debugReceipt);
            }
            return PurchaseProcessingResult.Pending; // Will be confirmed later via ConfirmPendingPurchase(). Or on next game session.
        }

        bool ValidatePurchaseLocal(Product product, string debugReceipt)
        {
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
            if (Application.isEditor)
                return true;

            // Unity IAP's validation logic is only included on these platforms.
            bool validPurchase = false;

            try
            {
                //var receiptData = JsonUtility.FromJson<ReceiptData>(debugReceipt != null ? debugReceipt : product.receipt);

                // Prepare the validator with the secrets we prepared in the Editor
                // obfuscation window.
#if UNITY_5_6_OR_NEWER
                var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
#else
                var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.bundleIdentifier);
#endif
    
                // On Google Play, result has a single product ID.
                // On Apple stores, receipts contain multiple products.
                var result = validator.Validate(debugReceipt != null ? debugReceipt : product.receipt);
                if (result != null && result.Length > 0)
                {
                    // For informational purposes, we list the receipt(s)
                    Debug.Log("Receipt is valid. Contents:");
                    foreach (IPurchaseReceipt productReceipt in result)
                    {
                        Debug.LogFormat("Product ID: {0}, Purchase data: '{1}', Transaction ID: '{2}'."
                            , productReceipt.productID, productReceipt.purchaseDate, productReceipt.transactionID);

                        validPurchase = true;
                    }
                }
            }
            catch (IAPSecurityException ex)
            {
                Debug.LogErrorFormat("Cannot validate receipt for Product ID: {0}. Exception: {1}."
                    , product.definition.storeSpecificId, ex);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Got unexpected exception while trying to parse receipt for Product ID: {0}. Exception: {1}."
                    , product.definition.storeSpecificId, ex);
            }
            return validPurchase;
#else
            return true; // Presume valid for platforms with no R.V.
#endif
        }

        void ValidatePurchaseOnline(Product product, string debugReceipt)
        {
            PersistentCore.instance.networkController.PurchaseVerification
                ( debugReceipt != null ? debugReceipt : product.receipt
                , (status) =>
                {
                    switch (status)
                    {
                        case PurchaseVerificationStatus.OK:
                        {
                            if (onProcessPurchase != null)
                            {
                                if (onProcessPurchase(product, storeID))
                                {
                                    controller.ConfirmPendingPurchase(product);
                                }
                            }
                            else
                            {
                                delayedPurchases.Add(new DelayedPurchaseInfo(product, true));
                            }
                        }; break;

                        case PurchaseVerificationStatus.VerificationFailed:
                        {
                            Debug.LogWarningFormat("Online purchase verification failed, purchase will be left in 'pending' state. Product id: {0}."
                                , product.definition.id);

                            if (onPurchaseFailed != null)
                            {
                                onPurchaseFailed.Invoke (product, PurchaseFailureReason.SignatureInvalid);
                            }
                            else
                            {
                                delayedPurchases.Add(new DelayedPurchaseInfo(product, false, PurchaseFailureReason.SignatureInvalid));
                            }
                        }; break;

                        default:
                        {
                            Debug.LogWarningFormat("Cannot perform online purchase verification, purchase will be left in 'pending' state. Product id: {0}, Result: {1}."
                                , product.definition.id, status);
                        }; break;
                    }
                });
        }
        #endregion
    }
}