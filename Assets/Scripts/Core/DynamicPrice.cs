﻿using System;

namespace Core
{
    public static class DynamicPrice
    {
        public delegate int GetSkipCost_Delegate(int seconds);

        public static int GetSkipCost(int totalSeconds)
        {
            if (totalSeconds <= 30)
            {
                return 0;
            }

            return GetSkipCostNoFree(totalSeconds);
        }

        public static int GetSkipCostNoFree(int totalSeconds)
        {
            if (totalSeconds < 1)
                return 0;

            return (int)Math.Max(1.0f, UnityEngine.Mathf.Sqrt(totalSeconds / CostPerSecond()));
        }

        public static float CostPerSecond()
        {
            return (2.0f * 60.0f);
        }
    }

}