﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using FileSystem;

namespace Core
{
    public static class Settings
    {
        static readonly int initialCapacity = 64;

        private static bool changed;
        private static Dictionary<string, object> settingsData;
        private static string settingsFileName_;

        private static void Init()
        {
            settingsData = new Dictionary<string, object>(initialCapacity);
        }

        private static string settingsPath
        {
            get
            {
                if (settingsFileName_ == null || settingsFileName_ == string.Empty)
                {
                    settingsFileName_ = string.Format("{0}{1}{2}", Application.persistentDataPath, Path.DirectorySeparatorChar, ResourcePaths.Persistent.settings);
                }
                return settingsFileName_;
            }
        }

        public static int GetInt (string name, int defaultValue = 0)
        {
            try
            {
                Load();
                object result;
                if ( settingsData.TryGetValue(name, out result))
                {
                    return Convert.ToInt32 (result);
                }
            }
            catch (Exception)
            { }

            return defaultValue;
        }

        public static float GetSingle (string name, float defaultValue = 0)
        {
            try
            {
                Load();
                object result;
                if ( settingsData.TryGetValue(name, out result))
                {
                    return Convert.ToSingle (result);
                }
            }
            catch (Exception)
            { }

            return defaultValue;
        }

        public static bool GetBool (string name, bool defaultValue = false)
        {
            try
            {
                Load();
                object result;
                if ( settingsData.TryGetValue(name, out result))
                {
                    return Convert.ToBoolean (result);
                }
            }
            catch (Exception)
            { }

            return defaultValue;
        }

        public static string GetString (string name, string defaultValue = default(string))
        {
            try
            {
                Load();
                object result;
                if ( settingsData.TryGetValue(name, out result))
                {
                    return Convert.ToString (result);
                }
            }
            catch (Exception)
            { }

            return defaultValue;
        }

        public static void Set (string name, object value)
        {
            Load();
            if (settingsData.ContainsKey(name))
            {
                settingsData[name] = value;
            }
            else
            {
                settingsData.Add(name, value);
            }
            changed = true;
        }

        public static bool Load()
        {
            if (settingsData == null)
            {
                Init();

                try
                {
                    if (!File.Exists(settingsPath))
                        return false;

                    using (var reader = File.OpenText(settingsPath))
                    {
                        using (var jsonReader = new JsonTextReader(reader))
                        {
                            var serializer = new JsonSerializer();

                            SetCommonSerializerOptions(serializer);

                            if (jsonReader.Read() && jsonReader.TokenType == JsonToken.StartObject)
                            {
                                while (jsonReader.Read() && jsonReader.TokenType == JsonToken.PropertyName)
                                {
                                    string propertyName = (string)jsonReader.Value;
                                    if (jsonReader.Read())
                                    {
                                        var obj = serializer.Deserialize(jsonReader);
                                        if (propertyName != null && propertyName != string.Empty && obj != null)
                                        {
                                            if (settingsData.ContainsKey(propertyName))
                                                settingsData.Remove(propertyName); // Override already existing.

                                            settingsData.Add(propertyName, obj);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Debug.LogErrorFormat("Loading settings from file '{0}' failed. Invalid start of settings file.", settingsPath);
                                return false;
                            }
                        }
                    }
                    changed = false;
                    Debug.LogFormat("Loaded settings from file '{0}'.", settingsPath);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.LogWarningFormat("Got exception while loading settings file '{0}'.\n{1}", settingsPath, ex);
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        static void SetCommonSerializerOptions(JsonSerializer serializer)
        {
            //          serializer.ReferenceResolver = new SimpleReferenceResolver();
            //          serializer.Converters.Add(isoDateTimeConverter);
            serializer.Converters.Add(new StringEnumConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.DefaultValueHandling = DefaultValueHandling.Ignore;
            // serializer.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            serializer.TypeNameHandling = TypeNameHandling.Objects;
            serializer.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
        }

        public static bool TrySave()
        {
            if (changed && settingsData != null && settingsData.Count > 0)
            {
                changed = false;
                try
                {
                    using (var writer = File.CreateText(settingsPath))
                    {
                        using (var jsonWriter = new JsonTextWriter(writer))
                        {
                            jsonWriter.Formatting = Formatting.Indented;

                            var serializer = new JsonSerializer();

                            SetCommonSerializerOptions(serializer);

                            jsonWriter.WriteStartObject();
                            {
                                foreach (var item in settingsData)
                                {
                                    if (item.Key != null && item.Key != string.Empty && item.Value != null)
                                    {
                                        jsonWriter.WritePropertyName(item.Key);
                                        serializer.Serialize(jsonWriter, item.Value);
                                    }
                                }
                            }
                            jsonWriter.WriteEndObject();
                        }
                    }
                    Debug.LogFormat("Saved settings to file '{0}'.", settingsPath);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.LogErrorFormat("Got exception while saving settings file '{0}'.\n{1}", settingsPath, ex);
                }
            }
            else
            {
                return true;
            }
            return false;
        }
    }
}
