﻿using Newtonsoft.Json;
using UnityEngine;
using System;

namespace Core.Timer
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class TimerFloat: IComparable<TimerFloat>
    {
        [JsonProperty("secondsTotal")]
        public float secondsTotal       { get; private set; }

        [JsonProperty("secondsToFinish")]
        public float secondsToFinish    { get { return SecondsToFinish(); } set { SetSecondsToFinish(value); } }

        [JsonIgnore] public float       timeEnd            { get; private set; }

        [JsonIgnore] public float       secondsPassed      { get { return SecondsPassed(); } }
        [JsonIgnore] public float       ratio              { get { return Ratio(); } }
        [JsonIgnore] public float       timeNow            { get { return TimeHelper.realtimeSinceStartup; } }
        [JsonIgnore] public TimeSpan    timeSpan           { get { return GetTimeSpan();} }
        [JsonIgnore] public string      printHelper        { get { return GetPrintHelper(); } }

        public TimerFloat(float secondsTotal)
        {
            this.secondsTotal = secondsTotal;
            timeEnd = secondsTotal + TimeHelper.realtimeSinceStartup;
        }

        public void FastForward(float seconds)
        {
            timeEnd -= seconds;
            Normalize();
        }

        public void Complete()
        {
            FastForward(secondsTotal);
        }

        public void dbgInc()
        {
            timeEnd -= secondsTotal * 0.05f;
        }

        public void dbgDec()
        {
            timeEnd += secondsTotal * 0.05f;
        }

        public void dbgOneMinuteLeft()
        {
            secondsTotal = 60;
            secondsToFinish = 60;
        }

        public int CompareTo(TimerFloat other)
        {
            return secondsToFinish.CompareTo(other.secondsToFinish);
        }

        private void Normalize()
        {
            var secondsPassed = secondsTotal - secondsToFinish;
            if (secondsPassed < 0)
                secondsToFinish = secondsTotal;
            else if (secondsPassed > secondsTotal)
                secondsToFinish = 0;
        }

        private float SecondsToFinish()
        {
            var time = timeEnd - timeNow;
            if (time > 0)
                return time;
            else
                return 0;
        }

        private void SetSecondsToFinish(float seconds)
        {
            if (seconds > 0)
                timeEnd = seconds + timeNow;
            else if (seconds == 0)
                timeEnd = timeNow;
            else
            {
                Debug.LogError("TimerFloat:SetSecondsToFinish wrong value");
                timeEnd = timeNow;
            }
        }

        private float SecondsPassed()
        {
            Normalize();
            var result = secondsTotal - secondsToFinish;
            return result;
        }

        private float Ratio()
        {
            if (secondsTotal == 0)
            {
                Debug.Log("TimerFloat: secondsTotal = 0");
                return 0;
            }
            Normalize();

            return secondsPassed / secondsTotal;
        }

        private TimeSpan GetTimeSpan()
        {
            Normalize();
            return TimeSpan.FromSeconds(secondsToFinish);
        }

        string GetPrintHelper()
        {
            return View.PrintHelper.GetTimeString(timeSpan);
        }
    }
}