﻿using UnityEngine;
using System;

namespace Core.Timer
{
    public static class TimeHelper
    {
        private class Context
        {
            public float initialTimeAfterStartup;
            public DateTime initialDateTime;

            public Context(float initialTimeAfterStartup, DateTime initialDateTime)
            {
                this.initialTimeAfterStartup = initialTimeAfterStartup;
                this.initialDateTime = initialDateTime;
            }
        }

        private static Context context = new Context (Time.realtimeSinceStartup, DateTime.UtcNow);

        public static float realtimeSinceStartup
        {
            get 
            {
                var now = DateTime.UtcNow;
                var delta = (now - context.initialDateTime).TotalSeconds;

                if (delta < 0)
                {
                    Debug.LogWarningFormat("Time jumped backwards! Initial date: {0}, Current date: {1}, delta: {2}. Will use zero delta"
                        , context.initialDateTime, now, delta);
                }

                var result = context.initialTimeAfterStartup + Math.Max(0.0f, (float)delta);
                return result;
            }
        }
    }
}
