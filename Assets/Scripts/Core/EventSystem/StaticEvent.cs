﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

namespace Core.EventSystem
{
    public abstract class StaticEventBase
    {
        private static readonly int initialListenersSize = 16;

        private struct ListenerInfo
        {
            public WeakReference target;
            public MethodInfo method;

            public ListenerInfo(WeakReference target, MethodInfo method)
            {
                this.target = target;
                this.method = method;
            }
        }

        private List<ListenerInfo> listeners;

        protected StaticEventBase()
        {
            listeners = new List<ListenerInfo>(initialListenersSize);
        }

        protected void AddListener(object target, MethodInfo mi)
        {
            Assert.IsNotNull(listeners);
            listeners.Add(new ListenerInfo(new WeakReference(target, false), mi));
        }

        protected void RemoveListener(object target, MethodInfo mi)
        {
            Assert.IsNotNull(listeners);
            for (int i = 0; i < listeners.Count; i++)
            {
                if (listeners[i].method == mi && listeners[i].target != null && ReferenceEquals(listeners[i].target.Target, target))
                {
                    listeners[i] = default(ListenerInfo); // Reset it but do not change container here, because it can be inside send loop.
                }
            }
        }

        protected void SendEvent(params object[] args)
        {
            if (listeners != null)
            {
                var cleanupDead = false;
                for (int i = 0; i < listeners.Count; i++)
                {
                    var listener = listeners[i];

                    if (listener.target != null)
                    {
                        var target = listener.target.Target;
                        if (target != null)
                        {
                            listener.method.Invoke(target, args);
                            continue;
                        }
                    }
                    cleanupDead = true;
                }

                if (cleanupDead)
                    listeners.RemoveAll(w => w.target == null || !w.target.IsAlive);
            }
        }

#if UNITY_EDITOR
        public static readonly string instanceFieldName = "instance_";
        public static readonly BindingFlags instanceFieldFlags = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.FlattenHierarchy;

        public static void CheckStaticEventsLeaks()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

            var baseType = typeof(StaticEventBase);
            var staticEventTypes = baseType.Assembly.GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.BaseType != baseType && !t.IsGenericType && baseType.IsAssignableFrom(t));

            bool stop = false;

            foreach (var type in staticEventTypes)
            {
                // Debug.LogFormat("Checking type: '{0}'...", type.FullName);
/*
                var seType = type;

                // Go up hierarchy until BaseType is a StaticEventBase. 
                // We need this, because private members are not inherited and you cannot use 'FlattenHiearchy' binding flag.
                while (seType.BaseType != baseType) 
                {
                    seType = seType.BaseType;
                }
*/

                var instanceField = type.GetField(instanceFieldName, instanceFieldFlags);
                if (instanceField != null)
                {
                    var instanceValue = (instanceField.GetValue(null) as StaticEventBase);
                    if (instanceValue != null && instanceValue.listeners != null)
                    {
                        foreach (var listener in instanceValue.listeners)
                        {
                            if (listener.target != null)
                            {
                                var target = listener.target.Target;

                                if (target != null)
                                {
                                    Debug.LogErrorFormat("Static Event '{0}.{1}' has a leaked subscriber: '{2}' : '{3}'"
                                        , type.Namespace, type.Name, target.ToString(), listener.method.ToString());
                                    stop = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogWarningFormat("Class '{0}.{1}' is missing static field named '{2}'."
                        , type.Namespace, type.Name, instanceFieldName);
                }
            }

            if (stop)
            {
/*
                if (UnityEditor.EditorApplication.isPlaying)
                    UnityEditor.EditorApplication.isPlaying = false;
*/
            }
        }
#endif
    }

    public class StaticEvent<DelegateT> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT>();
                return instance_;
            }
        }

        public static event Action Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send()
        {
            instance.SendEvent();
        }
    }

    public class StaticEvent<DelegateT, T1> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1>();
                return instance_;
            }
        }

        public static event Action<T1> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1)
        {
            instance.SendEvent(p1);
        }
    }

    public class StaticEvent<DelegateT, T1, T2> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2>();
                return instance_;
            }
        }

        public static event Action<T1, T2> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2)
        {
            instance.SendEvent(p1, p2);
        }
    }

    public class StaticEvent<DelegateT, T1, T2, T3> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3>();
                return instance_;
            }
        }

        public static event Action<T1, T2, T3> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3)
        {
            instance.SendEvent(p1, p2, p3);
        }
    }

    public class StaticEvent<DelegateT, T1, T2, T3, T4> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3, T4> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3, T4> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3, T4>();
                return instance_;
            }
        }

        public static event Action<T1, T2, T3, T4> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3, T4 p4)
        {
            instance.SendEvent(p1, p2, p3, p4);
        }
    }

    // .Net 3.5 has only Action with up to 4 parameters. So bad!
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11, in T12>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11, in T12, in T13>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11, in T12, in T13, in T14>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11, in T12, in T13, in T14, in T15>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15);
    public delegate void ActionPlus<in T1, in T2, in T3, in T4, in T5, in T6, in T7, in T8, in T9, in T10, in T11, in T12, in T13, in T14, in T15, in T16>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15, T16 arg16);

    public class StaticEvent<DelegateT, T1, T2, T3, T4, T5> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3, T4, T5> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3, T4, T5> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3, T4, T5>();
                return instance_;
            }
        }

        public static event ActionPlus<T1, T2, T3, T4, T5> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            instance.SendEvent(p1, p2, p3, p4, p5);
        }
    }

    public class StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6>();
                return instance_;
            }
        }

        public static event ActionPlus<T1, T2, T3, T4, T5, T6> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
        {
            instance.SendEvent(p1, p2, p3, p4, p5, p6);
        }
    }

    public class StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7>();
                return instance_;
            }
        }

        public static event ActionPlus<T1, T2, T3, T4, T5, T6, T7> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
        {
            instance.SendEvent(p1, p2, p3, p4, p5, p6, p7);
        }
    }

    public class StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7, T8> : StaticEventBase
        where DelegateT : class
    {
        protected static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7, T8> instance_;

        protected StaticEvent()
            : base ()
        {
        }

        private static StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7, T8> instance
        {
            get
            {
#if DEBUG
                if (!typeof(DelegateT).IsSubclassOf(typeof(Delegate)))
                {
                    throw new InvalidOperationException(typeof(DelegateT).Name + " is not a delegate type");
                }
#endif

                if (instance_ == null)
                    instance_ = new StaticEvent<DelegateT, T1, T2, T3, T4, T5, T6, T7, T8>();
                return instance_;
            }
        }

        public static event ActionPlus<T1, T2, T3, T4, T5, T6, T7, T8> Event
        {
            add { instance.AddListener(value.Target, value.Method); }
            remove { instance.RemoveListener(value.Target, value.Method); }
        }

        public static void Send(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
        {
            instance.SendEvent(p1, p2, p3, p4, p5, p6, p7, p8);
        }
    }
}
