﻿using System;
using Core.Assets;
using UnityEngine;

namespace Core.Loader
{
    [Serializable]
    public class PreloadItem
    {
        #region Types.
        public enum ItemType
        {
            Resource = 0,
            Scene
        }
        #endregion

        #region Public fields.
        public ItemType itemType;
        [ResourceSelector(typeof(UnityEngine.Object))]
        public string resourcePath;
        public bool allowSceneActivation;
        public bool waitForPrevious;
        public bool delayStartupUntilLoaded;
        #endregion

        #region Constructors.
        public PreloadItem
            ( ItemType itemType
            , string resourcePath
            , bool allowSceneActivation = true
            , bool waitForPrevious = false
            , bool delayStartupUntilLoaded = false)
        {
            this.itemType = itemType;
            this.resourcePath = resourcePath;
            this.waitForPrevious = waitForPrevious;
            this.delayStartupUntilLoaded = delayStartupUntilLoaded;
        }
        #endregion
    }
}
