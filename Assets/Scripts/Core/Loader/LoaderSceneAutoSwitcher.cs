﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Loader
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Core/Loader Scene Auto Switcher")]
    public class LoaderSceneAutoSwitcher : MonoBehaviour
    {
        #region Unity API.
        void Start()
        {
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                var persistentCoreInstance = PersistentCore.instance;
                if (persistentCoreInstance == null)
                {
/*
                    UnityEditor.EditorUtility.DisplayDialog
                        ( "You're doing it wrong!"
                        , string.Format ("Unload scene {0} and load _Loader scene into Editor to start game!", gameObject.scene.name)
                        , "OK" );
                    PersistentCore.Quit(true);
*/

                    Debug.Log("LoaderSceneAutoSwitcher: There's no PersistentCore object in a scene, switching to the loader scene first...");
                    PersistentCore.EditorInitialSwitchToLoader();
                    return;
                }
/*
                else if (PersistentCore.globalState == GlobalState.Startup)
                {   // Two scenes loaded in editor - loader and main. Unload main scene (self) and leave only loader.
                    Debug.LogFormat("LoaderSceneAutoSwitcher: Two scene loaded in Editor on startup, unloading scene '{0}'.", gameObject.scene.name);
                    UnityEngine.SceneManagement.SceneManager.UnloadScene(gameObject.scene);
                }
*/
            }
#endif
            Destroy(gameObject);    // Destroy self, not needed anymore.
        }
        #endregion

        #region Private methods.
#if UNITY_EDITOR
        IEnumerator ReloadJob()
        {
            Scene loaderScene = new Scene();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.IsValid() && scene.name == PersistentCore.loaderSceneName && scene.isLoaded)
                {
                    loaderScene = scene;
                }
            }

            if (loaderScene.IsValid())
            {
                Debug.Log ("Loader scene is loaded in Editor, unloading it first...");
                yield return SceneManager.UnloadSceneAsync(loaderScene);
            }

            PersistentCore.EditorInitialSwitchToLoader();
        }
#endif
        #endregion
    }
}
