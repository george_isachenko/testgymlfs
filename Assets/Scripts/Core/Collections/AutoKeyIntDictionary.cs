﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Core.Collections
{
    public class AutoKeyIntDictionary<TValue> : Dictionary<int, TValue>
    {
        public readonly static int invalidId = -1;

        int maxUsedKey = invalidId;
        int minUsedKey = invalidId;

        public AutoKeyIntDictionary()
            : base()
        {
        }

        public AutoKeyIntDictionary(IEqualityComparer<int> comparer)
            : base (comparer)
        {
        }

        public AutoKeyIntDictionary(IDictionary<int, TValue> dictionary)
            : base (dictionary)
        {
            UpdateMinMaxIDs();
        }

        public AutoKeyIntDictionary(int capacity)
            : base (capacity)
        {
        }

        public AutoKeyIntDictionary(IDictionary<int, TValue> dictionary, IEqualityComparer<int> comparer)
            : base (dictionary, comparer)
        {
            UpdateMinMaxIDs();
        }

        public AutoKeyIntDictionary(int capacity, IEqualityComparer<int> comparer)
            : base (capacity, comparer)
        {
            
        }

        protected AutoKeyIntDictionary(SerializationInfo info, StreamingContext context)
            : base (info, context)
        {
            UpdateMinMaxIDs();
        }

        public new TValue this[int key]
        {
            get
            {
                return base[key];
            }

            set
            {
                try
                {
                    base[key] = value;
                }
                catch (System.Exception)
                {
                	throw;
                }

                CommitKey(key);
            }
        }

        public override void OnDeserialization(object sender)
        {
            base.OnDeserialization(sender);
            UpdateMinMaxIDs();
        }

/*
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            UpdateMinMaxIDs();
        }
*/

        public int Add(TValue value)
        {
            var key = ProposeKey();
            try
            {
                base.Add(key, value);
            }
            catch (System.Exception)
            {
            	throw;
            }
            CommitKey(key);
            return key;
        }

        public new void Add(int key, TValue value)
        {
            try
            {
                base.Add(key, value);
            }
            catch (System.Exception)
            {
            	throw;
            }
            CommitKey(key);
        }

        public new void Clear()
        {
            base.Clear();
            maxUsedKey = minUsedKey = invalidId;
        }

        public new bool Remove(int key)
        {
            if (base.Remove(key))
            {
                if (Count == 0)
                {
                    maxUsedKey = minUsedKey = invalidId;
                }
                else
                {
                    RemovedKey(key);
                }
                return true;
            }
            else
                return false;
        }

        public int ProposeKey()
        {
            if (maxUsedKey < int.MaxValue)
            {
                return maxUsedKey + 1;
            }
            else if (minUsedKey > int.MinValue)
            {
                return minUsedKey - 1;
            }
            else
            {
                for (int i = minUsedKey + 1; i < maxUsedKey; i++)
                {
                    if (i != invalidId && !ContainsKey(i))
                        return i;
                }
            }

            // TODO: Exception?
            return invalidId;
        }

        void CommitKey(int key)
        {
            if (key != invalidId)
            {
                if (key > maxUsedKey)
                    maxUsedKey = key;
                else if (key < minUsedKey)
                    minUsedKey = key;
            }
            else
            {
                // TODO: Exception?
            }
        }

        void RemovedKey(int key)
        {
            if (key == maxUsedKey || key == minUsedKey)
                UpdateMinMaxIDs();
        }

        void UpdateMinMaxIDs()
        {
            maxUsedKey = invalidId;
            minUsedKey = invalidId;

            foreach (var key in Keys)
            {
                if (key != invalidId)
                {
                    if (key > maxUsedKey)
                        maxUsedKey = key;
                    else if (key < minUsedKey)
                        minUsedKey = key;
                }
                else
                {
                    // TODO: Exception?
                }
            }
        }
    }
}