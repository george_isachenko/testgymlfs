﻿using System;
using System.Globalization;

namespace Core
{
    public static class DateTimeHelpers
    {
        public static bool TryParseExactDateTimeISO8601 (this string dateString, out DateTime result)
        {
            return DateTime.TryParseExact
                ( dateString, "o", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out result);
        }

        public static DateTime ParseExactDateTimeISO8601 (this string dateString)
        {
            return DateTime.ParseExact
                ( dateString, "o", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
        }

        public static string FormatDateTimeISO8601 (this DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("o", CultureInfo.InvariantCulture);
        }
    }
}