﻿//#define TRACK_MEMORY_EVERY_FRAME

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Analytics;
using Core.Assets;
using Core.IAP;
using Core.Loader;
using Core.Network;
using Core.Network.Friends;
using Core.SocialAPI;
using Data;
using Fabric.Crashlytics;
using FileSystem;
using InspectorHelpers;
using Logic.Serialization;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using View.UI;

#if TRACK_MEMORY_EVERY_FRAME
using System.Globalization;
#endif

#if ENABLE_PROFILER
using UnityEngine.Profiling;
#endif

namespace Core
{
    public enum GameMode
    {
        MainGame = 0,
        DemoLevel,
    }

    public enum DemoLevelSource
    {
        FromFile = 0,
        FromResource
    }

    public enum GlobalState
    {
        Startup = 0,
        Loading,
        Game
    }

    public delegate void WillRestartGameEvent (GameMode mode, DemoLevelSource demoLevelSource);

    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/Core/Persistent Core")]
    [RequireComponent(typeof(NetworkController))]
    public class PersistentCore : MonoBehaviour
    {
        #region Types.
        class ModalConfirmQueueItem
        {
            public string header;
            public string text;
            public string okButtonText;
            public string cancelButtonText;
            public Action onConfirm;
            public Action onCancel;
            public ModalConfirmWindow.IconType iconType;

            public ModalConfirmQueueItem
                ( string header
                , string text
                , Action onConfirm
                , string okButtonText
                , Action onCancel
                , string cancelButtonText
                , ModalConfirmWindow.IconType iconType )
            {
                this.header = header;
                this.text = text;
                this.onConfirm = onConfirm;
                this.okButtonText = okButtonText;
                this.onCancel = onCancel;
                this.cancelButtonText = cancelButtonText;
                this.iconType = iconType;
            }
        }

        class PreloadItemRuntimeData
        {
            public PreloadItem item;
            public AsyncOperation request;

            public PreloadItemRuntimeData (PreloadItem item)
            {
                this.item = item;
                request = null;
            }

            public PreloadItemRuntimeData (PreloadItem item, AsyncOperation request)
            {
                this.item = item;
                this.request = request;
            }
        }
        #endregion

        #region Readonly/static data.
#if UNITY_EDITOR
        public static readonly string loaderSceneName = "_Loader"; // Cannot be moved to inspector variable now.
#endif

        //private static readonly string bridgeObjectRootName = "/_BridgeBinders";
        private static readonly string guiRootObjectScenePath = "/_GUIRootLoader";
        private static readonly float[] progressSteps = { 1.0f }; // { 0.1f, 0.4f, 0.5f };

#if TRACK_MEMORY_EVERY_FRAME
        private static readonly NumberFormatInfo memoryReportFormatting = new NumberFormatInfo { NumberGroupSeparator = " " };
#endif
        #endregion

        #region Public static properties.
        public static PersistentCore    instance                            { get; private set; }
        public static GlobalState       globalState
        {
            get
            {
                return globalState_;
            }
            
            private set
            {
                globalState_ = value;
                Crashlytics.SetKeyValue("GlobalState", value.ToString());
            }
        }

        public static GameMode          gameMode
        {
            get
            {
                return gameMode_;
            }
            
            private set
            {
                gameMode_ = value;
                Crashlytics.SetKeyValue("GameMode", value.ToString());
            }
        }

        public static DemoLevelSource   demoLevelSource                     { get; private set; }
        #endregion

        #region Public properties.
        public NetworkController        networkController                   { get; private set; }
        public Analytics.Analytics      analytics                           { get; private set; }
        public IAPManager               iapManager                          { get; private set; }
        public ISocialManager           socialManager                       { get; private set; }
        public IFriendsManager          friendsManager                      { get; private set; }

        public SaveConflictView         saveConflictView
        {
            get
            {
                if (guiRoot != null)
                {
                    var view = guiRoot.GetComponentInChildren<SaveConflictView>(true);
                    Assert.IsNotNull(view);

                    return view;
                }
                return null;
            }
        }

        public SplashScreenView         splashScreenView
        {
            get
            {
                if (guiRoot != null)
                {
                    var splash = guiRoot.GetComponentInChildren<SplashScreenView>(true);
                    Assert.IsNotNull(splash);

                    return splash;
                }
                return null;
            }
        }

        public bool modalMessagesQueueEmpty
        {
            get
            {
                return (modalConfirmQueue.Count == 0);
            }
        }

        public bool modalMessagesWindowShown
        {
            get
            {
                return modalConfirmWindow.gameObject.activeSelf;
            }
        }
        #endregion

        #region Public events.
        public event WillRestartGameEvent  onWillRestartGame;
        #endregion

        #region Private properties.
        private ModalConfirmWindow      modalConfirmWindow
        {
            get
            {
                if (guiRoot != null)
                {
                    var mcw = guiRoot.GetComponentInChildren<ModalConfirmWindow>(true);
                    Assert.IsNotNull(mcw);

                    return mcw;
                }
                return null;
            }
        }

        private int numberOfLoads
        {
            get
            {
                return numberOfLoads_;
            }

            set
            {
                numberOfLoads_ = value;
                Crashlytics.SetKeyValue("NumberOfLoads", value.ToString());
            }
        }

        #endregion

        #region Inspector fields.
        //------------------------------------------------------------------------
        [Header("Scenes")]

        [SerializeField]
        protected string mainSceneName;

        [SerializeField]
        protected string auxSceneName;

        [SerializeField]
        protected float auxSceneLoadDelay = 1.0f;

        [SerializeField]
        protected float auxScenePostLoadDelay = 1.0f;

        [SerializeField]
        protected bool loadMainSceneAfterResourcesLoaded = false;

        //------------------------------------------------------------------------
        [Header("Resources")]

/*
        [ReorderableList(LabelsEnumType = typeof(GameMode))]
        [ResourceSelector(typeof(GameObject))]
        [SerializeField]
        protected string[] gameModesPrefabs;
*/

        [SerializeField]
        protected bool loadGameModePrefabAfterMainScene = false;

        [ReorderableList]
        [SerializeField]
        protected PreloadItem[] preloadedResourcesFirstTime;

        [ReorderableList]
        [SerializeField]
        protected PreloadItem[] preloadedResourcesReload;

        [SerializeField]
        protected ShaderVariantCollection preloadedShaderVariants;

        //------------------------------------------------------------------------
        [Header("Splash screen")]

        [Tooltip("Delay after which initial hint message is replaced with a random hint.")]
        [SerializeField]
        protected float splashScreenTipDelay = 1.5f;

        //------------------------------------------------------------------------
        [Header("System settings")]

        [Tooltip("Async Texture upload time when starting game, until splash screen. See QualitySettings.asyncUploadTimeSlice documentation for details.")]
        [SerializeField]
        protected int asyncUploadTimeSliceStartup = 33;

        [Tooltip("Async Texture upload time when starting game, until splash screen. See QualitySettings.asyncUploadTimeSlice documentation for details.")]
        [SerializeField]
        protected int asyncUploadTimeSliceInGame = 4;

        //------------------------------------------------------------------------
        [Header("Startup settings")]

        [Tooltip("If set - do not issue error messages about generic network errors (no connection, failed to connect etc) on startup.")]
        [SerializeField]
        protected bool silentStartupNetworkErrors = false;

        //------------------------------------------------------------------------
        [Header("Support and Market settings")]

        [SerializeField]
        protected string iosAppId;

        [SerializeField]
        protected string supportEmailAddress;

        //------------------------------------------------------------------------
        [Header("Other settings")]
        [SerializeField]
        protected int leanTweenMaxTweens = 500;
        #endregion

        #region Private static data.
        private static GlobalState globalState_ = GlobalState.Startup;
        private static GameMode gameMode_ = GameMode.MainGame;
        #endregion

        #region Private data.
        private GameObject activeMainGameObject;
        private List<ModalConfirmQueueItem> modalConfirmQueue = new List<ModalConfirmQueueItem>(8);
        private bool fatalError = false;
        private bool reloadRequested = false;
        private bool fullReload = false;
        private bool activateAuxScene = false;
        private int numberOfLoads_ = 0;
        private IEnumerator queueLoadRequestsJob = null;
        private IEnumerator loadMainSceneAndStartGameJob = null;
        private IEnumerator reloadJob = null;
        private AsyncOperation auxSceneLoadRequest = null;
        private GameObject guiRoot = null;
        private List<PreloadItemRuntimeData> preloadData;
        #endregion

        #region Unity API.
        void Awake()
        {
            try
            {
                Assert.raiseExceptions = CoreDebugHacks.assertRaisesException;

    /*
    #if ENABLE_PROFILER
                if (Profiler.supported)
                {
                    Debug.LogWarning("PROFILER ENABLED!");
                    Profiler.enableBinaryLog = true;
                    Profiler.logFile = Application.persistentDataPath + "/profilerLog.txt";
                    Profiler.enabled = true;
                }
    #endif
    */

                Debug.Log (">>> PersistentCore.Awake(): Begin.");

#if UNITY_EDITOR
                if (!EditorCheckForCleanSceneEnvironment())
                    return;
#endif

                if (instance != null)
                {
                    Debug.LogErrorFormat("Duplicate PersistentCore ('{0}'). This is not supported!", gameObject.name);
                    Destroy(gameObject);
                    return;
                }

                analytics = new Analytics.Analytics();

                if (leanTweenMaxTweens > 0)
                {
                    LeanTween.init(leanTweenMaxTweens);
                }

                if (SaveGame.Exists(ResourcePaths.Persistent.saveGameFullPath))
                {
                    Crashlytics.SetKeyValue("NewUser", "no");
                    analytics.GameEvent("Startup: Existing User");
                }
                else
                {
                    Crashlytics.SetKeyValue("NewUser", "yes");
                    analytics.GameEvent("Startup: New User");
                }

                analytics.GameEvent("Load: Persistent Awake: Begin", "Time", Time.realtimeSinceStartup);
                Crashlytics.SetKeyValue("LoadingStage", "Persistent Awake: Begin");

                DontDestroyOnLoad(gameObject);

                gameMode = GameMode.MainGame;
                //gameMode = GameMode.DemoLevel;

                DataTables dataTables = DataTables.instance; // Causes constructor call that can fail with exception.

                DecideQuality();

                QualitySettings.asyncUploadTimeSlice = asyncUploadTimeSliceStartup;

                guiRoot = GameObject.Find(guiRootObjectScenePath);
                Assert.IsNotNull(guiRoot);

                networkController = GetComponent<NetworkController>();
                Assert.IsNotNull(networkController);

                iapManager = new IAPManager(dataTables.iapEntries);

                socialManager = GetComponent<ISocialManager>();
                Assert.IsNotNull(socialManager);

                friendsManager = GetComponent<IFriendsManager>();
                Assert.IsNotNull(friendsManager);

                splashScreenView.versionText = string.Format(splashScreenView.versionText, Application.version);

                analytics.GameEvent("Load: Persistent Awake: End", "Time", Time.realtimeSinceStartup);
                Crashlytics.SetKeyValue("LoadingStage", "Persistent Awake: End");

                instance = this;

                Debug.Log (">>> PersistentCore.Awake(): End.");
            }
            catch (Exception ex)
            {
            	Debug.LogErrorFormat("PersistentCore.Awake() failed. Exception:\n\n{0}", ex.ToString());

                analytics.GameEvent("PersistentCore.Awake failed", "Error", ex.Message);

                Quit(true);
                Destroy(gameObject);
                throw;
            }
        }

        void Start()
        {
            try
            {
                if (gameObject != null)
                {
                    Debug.Log (">>> PersistentCore.Start(): Start.");

                    analytics.GameEvent("Load: Persistent Start: Begin", "Time", Time.realtimeSinceStartup);
                    Crashlytics.SetKeyValue("LoadingStage", "Persistent Start: Begin");

                    // SceneManager.sceneLoaded += OnSceneLoaded;
                    globalState = GlobalState.Startup;

                    SetRandomSplashTip();

                    splashScreenView.progressOn = true;
                    splashScreenView.progressValue = 0.0f;

#if UNITY_EDITOR
                    DumpLeanTweenStats();
#endif

                    QueueLoadRequests ();

                    if (!networkController.QuickInit())
                    {
                        // Do not quit here: inner methods are showing error message and perform Quit() when user confirms it.

                        // Quit();
                        fatalError = true;
                        return;
                    }

                    Crashlytics.SetUserIdentifier(networkController.profileId);

                if (SaveGame.Exists(ResourcePaths.Persistent.saveGameFullPath) || networkController.forceStartupSync)
                    {
#if !UNITY_EDITOR
                        // NOTE! In Editor it causes second load of main scene (when you go to one of your friend's gyms) to stuck for up to 1-2 mins.
                        // Therefore in Editor AUX scene loading is scheduled on later stage after main scene is loaded.
                    
                        if (auxSceneName != null && auxSceneName != string.Empty)
                        {
                            StartCoroutine(LoadAuxSceneJob(false));
                        }
#endif

                        StartCoroutine(InitializeNetworkOnStartupJob());
                    }
                    else
                    {
                        analytics.GameEvent("Load: Network Quick Init", "Time", Time.realtimeSinceStartup);

                        LoadMainSceneAndStartGame();
                    }

                    analytics.GameEvent("Load: Persistent Start: End", "Time", Time.realtimeSinceStartup);
                    Crashlytics.SetKeyValue("LoadingStage", "Persistent Start: End");

                    Debug.Log (">>> PersistentCore.Start(): End.");
                }
            }
            catch (Exception ex)
            {
            	Debug.LogErrorFormat("PersistentCore.Start() failed. Exception:\n\n{0}", ex.ToString());

                analytics.GameEvent("PersistentCore.Start failed", "Error", ex.Message);

                Quit(true);
                Destroy(gameObject);
                throw;
            }
        }

        void OnDestroy()
        {
            if (ReferenceEquals(instance, gameObject)) 
            {   // Do this only on 'instance' object, ignore OnDestroy() on secondary temporary GameObject with PersistentCore that can appear on scene reloads and gets destroyed in Awake().
                // SceneManager.sceneLoaded -= OnSceneLoaded;

                instance = null;
            }
        }

        void OnApplicationQuit()
        {
            analytics.GameEvent("Application Quit", "Time", Time.realtimeSinceStartup);

            DestroyMainGameObject();
        }

        void Update()
        {
#if ENABLE_PROFILER && TRACK_MEMORY_EVERY_FRAME
            Debug.Log(string.Format(memoryReportFormatting
                , "==> UPD  [{0}]: Mono (Used/Total): {1:n0} / {2:n0}, Total (Alloc/Reserved/Unused): {3:n0} / {4:n0} / {5:n0}."
                , Time.frameCount
                , Profiler.GetMonoUsedSize()
                , Profiler.GetMonoHeapSize()
                , Profiler.GetTotalAllocatedMemory()
                , Profiler.GetTotalReservedMemory()
                , Profiler.GetTotalUnusedReservedMemory()));
#endif

            if (iapManager != null)
            {
                iapManager.Update();
            }
        }

        void LateUpdate()
        {
#if ENABLE_PROFILER && TRACK_MEMORY_EVERY_FRAME
            Debug.Log(string.Format(memoryReportFormatting
                , "==> LATE [{0}]: Mono (Used/Total): {1:n0} / {2:n0}, Total (Alloc/Reserved/Unused): {3:n0} / {4:n0} / {5:n0}."
                , Time.frameCount
                , Profiler.GetMonoUsedSize()
                , Profiler.GetMonoHeapSize()
                , Profiler.GetTotalAllocatedMemory()
                , Profiler.GetTotalReservedMemory()
                , Profiler.GetTotalUnusedReservedMemory()));
#endif

            if (reloadRequested)
            {
                Debug.LogFormat(">>> Game reload requested. Game mode requested: {0}, Demo level source: {1}", gameMode, demoLevelSource);

                reloadRequested = false;

                instance.networkController.CancelAllInGameOperations();
                instance.friendsManager.CancelAllInGameOperations();

                DestroyMainGameObject();

                splashScreenView.progressOn = true;
                splashScreenView.progressValue = 0.0f;

                Assert.IsNull(reloadJob);
                if (reloadJob == null)
                {
                    reloadJob = ReloadJob();
                    StartCoroutine(reloadJob);
                }

                return;
            }

            if (fatalError)
            {
                splashScreenView.progressOn = false;
            }
            else if (preloadData != null && (globalState == GlobalState.Startup || globalState == GlobalState.Loading))
            {
                if (preloadData.Count > 0)
                {
                    var sum = preloadData.Sum(x => (x.request != null) ? x.request.progress : 0.0f);
                    var divisor = preloadData.Sum (x => (x.item.delayStartupUntilLoaded) ? 1 : 0);
                    if (divisor > 0)
                    {
                        splashScreenView.progressOn = true;
                        SetProgress(0, sum / divisor);
                    }
                }
                else
                {
                    splashScreenView.progressOn = false;
                }
            }
        }
        #endregion

        #region Public static API.
        public static void Quit(bool isCriticalError)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            instance.analytics.GameEvent
                ( "Quit"
                , "IsCriticalError"
                , isCriticalError ? 1 : 0 );

            Application.Quit();
#endif
        }

        public static void ScheduleGameModeSwitching(GameMode mode, DemoLevelSource demoLevelSource = DemoLevelSource.FromFile)
        {
            if (!instance.reloadRequested)
            {
                gameMode = mode;
                PersistentCore.demoLevelSource = demoLevelSource;

                instance.onWillRestartGame?.Invoke(gameMode, demoLevelSource);

                instance.SetRandomSplashTip ();

                instance.splashScreenView.Show (() =>
                {
                    instance.reloadRequested = true;
                });
            }
        }

        public static void ScheduleFullReload()
        {
            if (!instance.reloadRequested)
            {
                instance.onWillRestartGame?.Invoke(gameMode, demoLevelSource);

                instance.SetRandomSplashTip ();

                instance.splashScreenView.Show (() =>
                {
                    instance.reloadRequested = true;
                    instance.fullReload = true;
                });
            }
        }

#if UNITY_EDITOR
        public static void EditorInitialSwitchToLoader()
        {
            SceneManager.LoadScene(loaderSceneName, LoadSceneMode.Single);
        }
#endif
        #endregion

        #region Public API.
        public void OpenApplicationMarketPage()
        {
            var url = string.Empty;
#if UNITY_IOS
            url = string.Format("itms-apps://itunes.apple.com/app/id{0}", iosAppId);
#elif UNITY_ANDROID
    #if UNITY_5_6_OR_NEWER
            url = string.Format("http://play.google.com/store/apps/details?id={0}", WWW.EscapeURL(Application.identifier));
    #else
            url = string.Format("http://play.google.com/store/apps/details?id={0}", WWW.EscapeURL(Application.bundleIdentifier));
    #endif
#endif
            instance.analytics.GameEvent
                ( "Open Application Market Page"
                , "URL", url );

            if (url != null && url != string.Empty)
            {
                Application.OpenURL(url);
            }
        }

        public void QueueModalConfirmMessage
            ( string header
            , string text
            , Action onConfirm
            , string okButtonText = "OK"
            , Action onCancel = null
            , string cancelButtonText = "Cancel"
            , ModalConfirmWindow.IconType iconType = ModalConfirmWindow.IconType.Default )
        {
            modalConfirmQueue.Add (new ModalConfirmQueueItem
                ( header, text, onConfirm, okButtonText, onCancel, cancelButtonText, iconType ));

            if (modalConfirmQueue.Count == 1)
            {
                QueueFirstModalConfirmMessage();
            }
        }

        public void ContactSupport (string subject, string body)
        {
            Assert.IsNotNull(subject);
            Assert.IsNotNull(body);

            if (supportEmailAddress != null && supportEmailAddress != string.Empty)
            {
                Application.OpenURL(string.Format("mailto:{0}?subject={1}&body={2}", supportEmailAddress, MyEscapeURL(subject), MyEscapeURL(body)));
            }
        }

        public void NotifyGameStarted(bool isFreshInstall, Action onSplashScreenHidden = null)
        {
            Assert.IsTrue(globalState == GlobalState.Loading);

            Debug.LogFormat(">>> NotifyGameStarted: Begin. Fresh Install: {0} Frame: {1}", isFreshInstall, Time.frameCount);

            analytics.GameEvent
                ( "Load: Game Started: Begin"
                , new GameEventParam("Time", Time.realtimeSinceStartup)
                , new GameEventParam("Fresh Install", isFreshInstall ? 1 : 0));
            Crashlytics.SetKeyValue("LoadingStage", "Game Started: Begin");

            if (!isFreshInstall && gameMode == GameMode.MainGame)
            {
                networkController.StartInGameJobs(ResourcePaths.Persistent.saveGameFullPath);
                friendsManager.StartInGameJobs();
            }

            QualitySettings.asyncUploadTimeSlice = asyncUploadTimeSliceInGame;

            globalState = GlobalState.Game;

            if (numberOfLoads == 1 && gameMode == GameMode.MainGame)
            {
                socialManager.Initialize();

#if UNITY_EDITOR
                // NOTE! See corresponding note in Start().
                if (auxSceneName != null && auxSceneName != string.Empty)
                {
                    StartCoroutine(LoadAuxSceneJob(!isFreshInstall));
                }
#else
                if (auxSceneName != null && auxSceneName != string.Empty)
                {
                    if (auxSceneLoadRequest == null)
                    {
                        StartCoroutine(LoadAuxSceneJob(false));
                    }
                    else
                    {
                        activateAuxScene = true;
                    }
                }
#endif
            }

            splashScreenView.progressOn = false;

            if (numberOfLoads == 1 && gameMode == GameMode.MainGame)
            {
                splashScreenView.Hide(() =>
                {
                    Debug.Log(">>> Splash screen deactivated!");

                    analytics.GameEvent
                        ( "Load: Closed Splash Screen"
                        , "Time", Time.realtimeSinceStartup);
                    Crashlytics.SetKeyValue("LoadingStage", "Closed Splash Screen");

                    onSplashScreenHidden?.Invoke();
                });
            }
            else
            {
                splashScreenView.Hide(() =>
                {
                    Debug.Log(">>> Splash screen deactivated!");

                    onSplashScreenHidden?.Invoke();
                });
            }

            analytics.GameEvent
                ( "Load: Game Started: End"
                , new GameEventParam("Time", Time.realtimeSinceStartup)
                , new GameEventParam("Fresh Install", isFreshInstall ? 1 : 0));
            Crashlytics.SetKeyValue("LoadingStage", "Game Started: End");

            Debug.Log(">>> NotifyGameStarted: End.");
        }

        public void NotifySaveGameCreated ()
        {
            Assert.IsTrue(globalState == GlobalState.Game);

            Debug.LogFormat(">>> NotifySaveGameCreated: Start. Frame: {0}", Time.frameCount);

            analytics.GameEvent
                ( "Load: New Save Game Created: Begin"
                , "Time", Time.realtimeSinceStartup);

            if (gameMode == GameMode.MainGame)
            {
                networkController.StartInGameJobs(ResourcePaths.Persistent.saveGameFullPath);
                friendsManager.StartInGameJobs();
            }

            if (auxSceneLoadRequest != null)
            {
                activateAuxScene = true;
            }

            analytics.GameEvent
                ( "Load: New Save Game Created: End"
                , "Time", Time.realtimeSinceStartup);

            Debug.Log(">>> NotifySaveGameCreated: End.");
        }
        #endregion

        #region Private functions.
        private string MyEscapeURL (string url)
        {
            return WWW.EscapeURL(url).Replace("+","%20");
        }

        void DecideQuality()
        {
#if UNITY_IOS
            var device = UnityEngine.iOS.Device.generation;

            if (device == UnityEngine.iOS.DeviceGeneration.iPad1Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPad2Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPadMini1Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPhone3GS ||
                device == UnityEngine.iOS.DeviceGeneration.iPhone4 ||
                device == UnityEngine.iOS.DeviceGeneration.iPhone4S ||
                device == UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen ||
                device == UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen
            )
            {
                QualitySettings.SetQualityLevel(0);
            }
            else
            {
                QualitySettings.SetQualityLevel(1);
            }
#else
            QualitySettings.SetQualityLevel(1);
#endif
        }

        private void QueueFirstModalConfirmMessage()
        {
            if (modalConfirmQueue.Count > 0)
            {
                var item = modalConfirmQueue[0];

                modalConfirmWindow.Show
                    ( item.header
                    , item.text
                    , OnModalMessageConfirm
                    , item.okButtonText
                    , item.onCancel != null ? OnModalMessageCancel : (Action)null
                    , item.cancelButtonText
                    , item.iconType );
            }
        }

        private void DestroyMainGameObject()
        {
            if (activeMainGameObject != null)
            {
                var tmpObject = activeMainGameObject;
                activeMainGameObject = null;
                DestroyImmediate(tmpObject);
            }
        }

        void OnModalMessageConfirm()
        {
            Assert.IsTrue(modalConfirmQueue.Count > 0);
            
            var tmpConfirm = modalConfirmQueue[0].onConfirm;
            modalConfirmQueue.RemoveAt(0);

            QueueFirstModalConfirmMessage();

            if (tmpConfirm != null)
            {
                tmpConfirm();
            }
        }

        void OnModalMessageCancel()
        {
            Assert.IsTrue(modalConfirmQueue.Count > 0);
            
            var tmpCancel = modalConfirmQueue[0].onCancel;
            modalConfirmQueue.RemoveAt(0);

            QueueFirstModalConfirmMessage();

            if (tmpCancel != null)
            {
                tmpCancel();
            }
        }

/*
        private void OnSceneLoaded (Scene scene, LoadSceneMode mode)
        {
            Assert.IsTrue(scene != null);

            if (scene != null && scene.IsValid()/ * && scene.isLoaded* /)
            {
                if (scene.name == mainSceneName)
                {
                }
                else if (scene.name == loaderSceneName)
                {
                }
            }
        }
*/

/*
        private void OnSceneUnloaded(Scene scene)
        {
            Assert.IsTrue(scene != null);

            if (scene != null && scene.IsValid() / * && scene.isLoaded* / && (scene.name == loaderSceneName || scene.name == mainSceneName))
            {
            }
        }
*/

        private IEnumerator InitializeNetworkOnStartupJob()
        {
            yield return null; // Skip immediate invocation.

            analytics.GameEvent("Load: Network Init and Sync: Begin", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Network Init and Sync: Begin");

            while (true)
            {
                var resultSet = false;
                var result = StartupSynchronizationStatus.InitializationFailedQuit;

                networkController.StartupLoginAndSync
                    ( ResourcePaths.Persistent.saveGameFullPath
                    , silentStartupNetworkErrors
                    , (status) =>
                    {
                        result = status;
                        resultSet = true;
                    });

                yield return new WaitUntil(() => resultSet);

                switch (result)
                {
                    case StartupSynchronizationStatus.OK:
                    {
                        analytics.GameEvent("Load: Network Init and Sync: End", "Time", Time.realtimeSinceStartup);
                        Crashlytics.SetKeyValue("LoadingStage", "Network Init and Sync: End");

                        LoadMainSceneAndStartGame();
                        yield break;
                    };

                    case StartupSynchronizationStatus.InitializationFailedContinue:
                    {
                        analytics.GameEvent("Load: Network Init and Sync: End", "Time", Time.realtimeSinceStartup);
                        Crashlytics.SetKeyValue("LoadingStage", "Network Init and Sync: End");

                        LoadMainSceneAndStartGame();
                        yield break;
                    };

                    case StartupSynchronizationStatus.InitializationFailedRetry:
                    {
                        // 
                    }; break;

                    case StartupSynchronizationStatus.InitializationFailedQuit:
                    {
                        fatalError = true;
                        Quit(true);
                        yield break;
                    };

                    case StartupSynchronizationStatus.InitializationFailedWaitForever:
                    {
                        fatalError = true;
                        for (;;)    // Loop forever. This status code returned when another window with critical error opened and game will quit on Close.
                        {
                            yield return null;
                        }
                    };
                }
            }
        }

        private void QueueLoadRequests ()
        {
            // It's OK to invoke it multiple times on startup - it should handle this gracefully.

            if (queueLoadRequestsJob == null)
            {
                if (preloadData == null)
                {
                    var preloadList = (numberOfLoads == 0) ? preloadedResourcesFirstTime : preloadedResourcesReload;

                    preloadData = new List<PreloadItemRuntimeData>(preloadList.Length + 2);

                    if (preloadList.Length > 0)
                    {
                        foreach (var pli in preloadList)
                        {
                            preloadData.Add (new PreloadItemRuntimeData (pli));
                        }
                    }

                    // Add map loading request.
                    {
                        preloadData.Add (new PreloadItemRuntimeData (new PreloadItem (PreloadItem.ItemType.Scene, mainSceneName, false, loadMainSceneAfterResourcesLoaded, true)));
                    }

                    // Add Main game prefab preloading request.
/*
                    {
                        if ((int)gameMode < gameModesPrefabs.Length && gameModesPrefabs[(int)gameMode] != null)
                        {
                            preloadData.Add (new PreloadItemRuntimeData (new PreloadItem (PreloadItem.ItemType.Resource, gameModesPrefabs[(int)gameMode], true, loadGameModePrefabAfterMainScene, true)));
                        }
                    }
*/
                }

                queueLoadRequestsJob = QueueLoadRequestsJob();
                StartCoroutine(queueLoadRequestsJob);
            }
        }

        private IEnumerator QueueLoadRequestsJob ()
        {
            yield return null;

            if (numberOfLoads == 0)
            {
/*
#if UNITY_EDITOR
                {
                    var mainScene = SceneManager.GetSceneByName(mainSceneName);
                    if (mainScene != null && mainScene.IsValid() && mainScene.isLoaded)
                    {
                        Debug.LogWarningFormat("PersistentCore: Main scene ('{0}') is also loaded in Editor on startup, unloading it.", mainScene.name);
                        SceneManager.UnloadScene(mainScene);
                    }
                }
#endif
*/

            }

            CheckAndQueuePreloads();

            if (preloadedShaderVariants != null && numberOfLoads == 0 && !preloadedShaderVariants.isWarmedUp)
            {
                preloadedShaderVariants.WarmUp();
            }

            Assert.IsNotNull(queueLoadRequestsJob);
            queueLoadRequestsJob = null;

            yield break;
        }

        private void CheckAndQueuePreloads (bool forceActivation = true)
        {
            if (preloadData != null)
            {
                var prevLoading = false;
                foreach (var pd in preloadData)
                {
                    if (prevLoading && pd.item.waitForPrevious)
                        break;

                    if (pd.request == null)
                    {
                        if (pd.item.itemType == PreloadItem.ItemType.Resource)
                        {
                            Debug.LogFormat("Queuing BG load of resource: '{0}...", pd.item.resourcePath);

                            pd.request = Resources.LoadAsync(pd.item.resourcePath);
                            pd.request.allowSceneActivation = true;
                        }
                        else if (pd.item.itemType == PreloadItem.ItemType.Scene)
                        {
                            Debug.LogFormat("Queuing BG load of scene: '{0}'...", pd.item.resourcePath);

                            pd.request = SceneManager.LoadSceneAsync(pd.item.resourcePath, LoadSceneMode.Additive);
                            pd.request.allowSceneActivation = false; // Must be activated at right time!
                        }
                    }

                    if (pd.request != null)
                    {
                        if (forceActivation)
                        {
                            pd.request.allowSceneActivation = true;
                        }

                        if (!prevLoading)
                        {
                            prevLoading = !pd.request.isDone;
                        }
                    }
                }
            }
        }

        void LoadMainSceneAndStartGame ()
        {
            if (loadMainSceneAndStartGameJob == null)
            {
#if UNITY_EDITOR
                DumpLeanTweenStats();
#endif

                analytics.GameEvent("Load: Main Scene And Start Game", "Time", Time.realtimeSinceStartup);
                Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene And Start Game");

                if (globalState != GlobalState.Startup)
                {
                    Debug.LogErrorFormat("Unexpected global state ({0}) when starting loading game.", globalState);
                }

                // Assert.IsTrue(globalState == GlobalState.Startup);
                globalState = GlobalState.Loading;

                // splashScreenView.splashText = Loc.Get("SplashScreenMessageLoading");


            /*
                int tCloudExp = 10000;
                int tCloudLevel = Logic.Helpers.ExpirienceLevels.GetLevelByTable(tCloudExp);
                int tDeviceExp = 5000;
                int tDeviceLevel = Logic.Helpers.ExpirienceLevels.GetLevelByTable(tDeviceExp);
                saveConflictView.Show
                ( 
                    tCloudLevel
                    , Logic.Helpers.ExpirienceLevels.GetExpRatioStr(tCloudExp, tCloudLevel)
                    , Logic.Helpers.ExpirienceLevels.GetExpRatio(tCloudExp, tCloudLevel)
                    , new TimeSpan(3, 10, 15)
                    , tDeviceLevel
                    , Logic.Helpers.ExpirienceLevels.GetExpRatioStr(tDeviceExp, tDeviceLevel)
                    , Logic.Helpers.ExpirienceLevels.GetExpRatio(tCloudExp, tCloudLevel) 
                    , new TimeSpan(20600, 5, 59, 3)
                    , () =>
                    {
                        Debug.Log("Cloud version selected!");
                        StartCoroutine(LoadMainSceneAndStartGameJob());
                    }
                    , () =>
                    {
                        Debug.Log("Device version selected!");
                        StartCoroutine(LoadMainSceneAndStartGameJob());
                    });

                return;
                */

                QueueLoadRequests ();

                loadMainSceneAndStartGameJob = LoadMainSceneAndStartGameJob();
                StartCoroutine(loadMainSceneAndStartGameJob);
            }
            else
            {
                Debug.LogError("LoadMainSceneAndStartGameJob is already running!");
            }
        }

        private IEnumerator LoadMainSceneAndStartGameJob()
        {
            yield return null;

            if (fatalError)
            {
                Assert.IsNotNull(loadMainSceneAndStartGameJob);
                loadMainSceneAndStartGameJob = null;
                yield break;
            }

            analytics.GameEvent("Load: Main Scene Job: Begin", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: Begin");

            if (queueLoadRequestsJob != null)
            {   // Probably QueueLoadRequests() is not complete yet.
                yield return new WaitUntil(() => queueLoadRequestsJob == null);
            }

            // Wait for resources marked with a "Wait for previous" flag and still not finished (or not even queued yet).
            {
                while (preloadData.Any (x => (x.item.waitForPrevious && (x.request == null || !x.request.isDone)) ))
                {
                    CheckAndQueuePreloads(true);
                    yield return null;
                }
            }

            {
                Debug.LogFormat(">>> Allowing main scene activation after load. Frame: {0}", Time.frameCount);

                AsyncOperation mainSceneLoadRequest = null;
                while (mainSceneLoadRequest == null)
                {
                    mainSceneLoadRequest = preloadData
                        .Where(x => x.request != null && x.item.itemType == PreloadItem.ItemType.Scene && x.item.resourcePath == mainSceneName)
                        .Select(x => x.request)
                        .FirstOrDefault();

                    yield return null;
                }

                mainSceneLoadRequest.allowSceneActivation = true;   // Now we can activate Main scene and wait until its done.

                yield return null; // Wait for another frame.

                if (!mainSceneLoadRequest.isDone)
                {
                    Debug.LogFormat(">>> Waiting until main scene loading is done (can take up to ~1-2 min in Editor). Frame: {0}.", Time.frameCount);

                    yield return new WaitUntil (() => mainSceneLoadRequest.isDone);
                }
            }

            var mainScene = SceneManager.GetSceneByName(mainSceneName);
            SceneManager.SetActiveScene(mainScene);
            Debug.LogFormat(">>> Main scene activated. Frame: {0}", Time.frameCount);

            analytics.GameEvent("Load: Main Scene Job: Main Scene Loaded", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: Main Scene Loaded");

            numberOfLoads++;

            // Final wait for required resources with a "Delay Startup Until Loaded" flag.
            {
                while (preloadData.Any (x => (x.item.delayStartupUntilLoaded && (x.request == null || !x.request.isDone)) ))
                {
                    CheckAndQueuePreloads(true);
                    yield return null;
                }
            }

            analytics.GameEvent("Load: Main Scene Job: Preloads Loaded", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: Preloads Loaded");

            splashScreenView.progressText = string.Empty;


/*
            // Find and wait for main GameObject prefab resource until it's loaded.
            if ((int)gameMode < gameModesPrefabs.Length && gameModesPrefabs[(int)gameMode] != null)
            {
                Debug.LogFormat(">>> Waiting for main GameObject prefab resource. Frame: {0}", Time.frameCount);

                ResourceRequest mainGameObjectPrefabLoadRequest = null;
                while (mainGameObjectPrefabLoadRequest == null)
                {
                    mainGameObjectPrefabLoadRequest = preloadData
                        .Where(x => x.request != null &&
                                    x.item.itemType == PreloadItem.ItemType.Resource &&
                                    x.item.resourcePath == gameModesPrefabs[(int)gameMode] &&
                                    x.request.isDone)
                        .Select(x => ((ResourceRequest)x.request))
                        .FirstOrDefault();

                    yield return null;
                }

                Debug.LogFormat(">>> Main GameObject prefab resource loaded, instantiating into scene. Frame: {0}", Time.frameCount);

                analytics.GameEvent("Load: Main Scene Job: Main GameObject Loaded", "Time", Time.realtimeSinceStartup);
                Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: Main GameObject Loaded");

                Assert.IsTrue(mainGameObjectPrefabLoadRequest.isDone && mainGameObjectPrefabLoadRequest.asset != null);
                Assert.IsNull(activeMainGameObject);

                activeMainGameObject = Instantiate((GameObject)mainGameObjectPrefabLoadRequest.asset);
                activeMainGameObject.name = string.Format("_{0}", gameMode);

                Assert.IsNotNull(activeMainGameObject);

                Debug.Log(">>> Main GameObject prefab resource instantiated.");
            }
            else
            {
                Debug.LogErrorFormat("There's no main GameObject prefab resource set up for game mode {0}.", gameMode);
            }
*/
            Debug.LogFormat(">>> Activating Game Mode object in scene. Frame: {0}", Time.frameCount);

            var gameModesRootObject = mainScene.GetRootGameObjects().FirstOrDefault(x => x.name == "_GameModes");
            Assert.IsNotNull(gameModesRootObject);
            if (gameModesRootObject != null)
            {
                var gameModeObject = gameModesRootObject.transform.Find(gameMode.ToString())?.gameObject;
                Assert.IsNotNull(gameModeObject);
                if (gameModeObject != null)
                {
                    gameModeObject.SetActive(true);
                    activeMainGameObject = gameModeObject;
                }
            }

            analytics.GameEvent("Load: Main Scene Job: Main GameObject Loaded", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: Main GameObject Loaded");

            preloadData = null; // Resources not marked as "delayStartupUntilLoaded" are left to be loaded when game is running.

            analytics.GameEvent("Load: Main Scene And Start Game Job: End", "Time", Time.realtimeSinceStartup); // <-- Remove this event later, after some time.
            analytics.GameEvent("Load: Main Scene Job: End", "Time", Time.realtimeSinceStartup);
            Crashlytics.SetKeyValue("LoadingStage", "Load Main Scene Job: End");

            Assert.IsNotNull(loadMainSceneAndStartGameJob);
            loadMainSceneAndStartGameJob = null;
            yield break;
        }

        private IEnumerator LoadAuxSceneJob(bool allowSceneActivation)
        {
            Assert.IsNull(auxSceneLoadRequest);

            yield return null;

            analytics.GameEvent("Load AUX: Aux Scene Job: Begin", "Time", Time.realtimeSinceStartup);

            Debug.LogFormat ("Aux scene loading queued. Scene name: '{0}', allowSceneActivation: {1}, Delay: {2}, Frame: {3},  ..."
                , auxSceneName, allowSceneActivation, auxSceneLoadDelay, Time.frameCount);

            if (auxSceneLoadDelay > 0)
            {
                yield return new WaitForSeconds(auxSceneLoadDelay);
            }
            
            auxSceneLoadRequest = SceneManager.LoadSceneAsync (auxSceneName, LoadSceneMode.Additive);
            auxSceneLoadRequest.allowSceneActivation = allowSceneActivation;

            if (!allowSceneActivation)
            {
                yield return new WaitUntil(() => activateAuxScene);

                if (auxScenePostLoadDelay > 0)
                {
                    yield return new WaitForSeconds(auxScenePostLoadDelay);
                }

                auxSceneLoadRequest.allowSceneActivation = true;
            }

            yield return auxSceneLoadRequest;
            Assert.IsTrue(auxSceneLoadRequest.isDone);
            auxSceneLoadRequest = null;

            analytics.GameEvent("Load AUX: Aux Scene Job: End", "Time", Time.realtimeSinceStartup);
        }

        private IEnumerator ReloadJob ()
        {
            yield return null;

            // HACK: Reset LeanTween because they can be leaked between scene reloads sometimes.
			LeanTween.reset();

            // Additional paranoid checks added here because of bug https://tatemgames.atlassian.net/browse/DGUNITY-1744
            {
                var mainScene = SceneManager.GetSceneByName(mainSceneName);
                if (mainScene.IsValid() && mainScene.isLoaded)
                {
                    yield return SceneManager.UnloadSceneAsync(mainScene);
                }
            }

            if (leanTweenMaxTweens > 0)
            {
                LeanTween.init(leanTweenMaxTweens);
            }

#if UNITY_EDITOR
            yield return null;

            EventSystem.StaticEventBase.CheckStaticEventsLeaks();
#endif

            globalState = GlobalState.Startup;

            if (fullReload && SaveGame.Exists(ResourcePaths.Persistent.saveGameFullPath))
            {
                StartCoroutine(InitializeNetworkOnStartupJob());
            }
            else
            {
                LoadMainSceneAndStartGame();
            }

            Assert.IsNotNull(reloadJob);
            reloadJob = null;
        }

        void SetProgress(int stepIdx, float value)
        {
            value = Mathf.Clamp (value, 0.0f, 1.0f);

            float progressMultiplier = (stepIdx >= 0 && stepIdx < progressSteps.Length) ? progressSteps[stepIdx] : 1.0f;
            float progressStart = 0.0f;
            do 
            {
                stepIdx--;
                if (stepIdx >= 0 && stepIdx < progressSteps.Length)
                {
                    progressStart += progressSteps[stepIdx];
                }
            } while (stepIdx >= 0);

            splashScreenView.progressOn = true;
            splashScreenView.progressValue = Mathf.Clamp(progressStart + value * progressMultiplier, 0.0f, 1.0f);
        }

        private void SetRandomSplashTip ()
        {
            splashScreenView.tipText = null; // Set default, auto-localized text.
            splashScreenView.tipOn = true;
            
            StartCoroutine(SetRandomSplashTipJob());
        }

        private IEnumerator SetRandomSplashTipJob ()
        {
            yield return null;

            if (splashScreenTipDelay > 0)
            {
                yield return new WaitForSeconds(splashScreenTipDelay);
            }

            var tip = SelectRandomTip("LoadingTipFormat", "LoadingTipMessage");
            if (tip != null)
            {
                splashScreenView.tipText = tip;
            }
        }

        private string SelectRandomTip (string formatLocKey, string messageLocKeyPrefix)
        {
            var numberOfTips = Loc.CountWithPrefix(messageLocKeyPrefix);
            if (numberOfTips > 0)
            {
                return Loc.Get(formatLocKey, Loc.GetWithPrefix(messageLocKeyPrefix, UnityEngine.Random.Range(0, numberOfTips)));
            }
            else
                return string.Empty;
        }

#if UNITY_EDITOR
        private static void DumpLeanTweenStats ()
        {
/*
            Debug.LogWarningFormat("LeanTween stats: {0}/{1}.", LeanTween.tweensRunning, LeanTween.maxSimulataneousTweens);

            for (int i = 0; i <= LeanTween.maxSearch; i++)
            {
                // Requires hack where LeanTween.tweens is declared public.

/ *
                if (LeanTween.tweens[i].toggle)
                {
                    var descr = LeanTween.tweens[i];
                    var name = descr.trans != null
                        ? descr.trans.GetScenePath()
                        : (descr.rectTransform != null)
                            ? descr.rectTransform.GetScenePath()
                            : "<null>";

                    Debug.LogWarningFormat("LeanTween running: [{0}]: {1}", i, name);
                }
* /
            }
*/
        }

        bool EditorCheckForCleanSceneEnvironment()
        {
            if (SceneManager.sceneCount > 1)
            {
                UnityEditor.EditorUtility.DisplayDialog
                    ( "You're doing it wrong!"
                    , string.Format ("Cannot start game from Editor when both Loader ('{0}') and Main scenes ('{0}') are open! Open either Loader or Main scene but not both!"
                        , loaderSceneName, mainSceneName)
                    , "OK" );
                
                Quit(true);
                return false;
            }

            return true;
        }
#endif
        #endregion
    }
}