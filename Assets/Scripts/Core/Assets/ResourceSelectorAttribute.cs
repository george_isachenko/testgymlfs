﻿using System;
using UnityEngine;

namespace Core.Assets
{
    public class ResourceSelectorAttribute : PropertyAttribute
    {
        public ResourceSelectorAttribute()
            : base()
        {
            this.resourceType = null;
            this.typeDonorFieldName = null;
        }

        public ResourceSelectorAttribute(Type resourceType)
            : base()
        {
            this.resourceType = resourceType;
            this.typeDonorFieldName = null;
        }

        public ResourceSelectorAttribute(string typeDonorFieldName)
            : base()
        {
            this.resourceType = null;
            this.typeDonorFieldName = typeDonorFieldName;
        }

        public Type ResourceType
        {
            get { return resourceType; }
            set { resourceType = value; }
        }

        public string TypeDonorFieldName
        {
            get { return typeDonorFieldName; }
            set { typeDonorFieldName = value; }
        }

        Type resourceType;
        string typeDonorFieldName;
    }
}