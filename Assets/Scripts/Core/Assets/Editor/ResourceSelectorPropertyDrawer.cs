﻿using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Core.Assets.Editor
{
    [CustomPropertyDrawer(typeof(ResourceSelectorAttribute), true)]
    public class ResourceSelectorPropertyDrawer : PropertyDrawer
    {
        private static readonly float sObjectSelectorHeight = 16.0f;
        private static readonly float sObjectSelectorWidthProportion = 0.4f;
        private static readonly float sWarningBoxHeight = 40.0f;

        private static readonly string resPathPart = "/Resources/";

        // Provide easy access to the ResourceSelectorAttribute for reading information from it.
        protected new ResourceSelectorAttribute attribute
        {
            get
            {
                return (base.attribute as ResourceSelectorAttribute);
            }
        }

        public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                return sObjectSelectorHeight /* + sPathStringHeight*/;
            }
            else
            {
                return sWarningBoxHeight + EditorGUI.GetPropertyHeight(property, label, true);
            }
        }

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                System.Type desiredType = typeof(Object);

                if (attribute.ResourceType != null)
                {
                    desiredType = attribute.ResourceType;
                }
                else if (attribute.TypeDonorFieldName != null && attribute.TypeDonorFieldName != string.Empty)
                {
                    var fieldInfo = (property.serializedObject.targetObject.GetType()).GetField(attribute.TypeDonorFieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (fieldInfo != null)
                    {
                        desiredType = fieldInfo.FieldType;
                    }
                }

                Object currentResourceObj = null;
/*
                if (property.stringValue != null && property.stringValue != string.Empty)
                {
                    var allAssetPaths = AssetDatabase.GetAllAssetPaths();
                    foreach (var path in allAssetPaths)
                    {
                        if (AssetDatabase.GetMainAssetTypeAtPath(path) == desiredType)
                        {
                            var pathNoExt = Path.ChangeExtension(path, null);

                            Debug.LogWarningFormat(">>> {0}", pathNoExt);
                            if (pathNoExt.EndsWith(property.stringValue))
                            {
                                currentResourceObj = AssetDatabase.LoadAssetAtPath(path, desiredType);
                                break;
                            }
                        }
                    }
                }
*/

                var subRect = EditorGUI.PrefixLabel(position, label);

                EditorGUI.PropertyField
                    ( new Rect (subRect.x - 14.0f, subRect.y, subRect.width * (1.0f - sObjectSelectorWidthProportion) + 14.0f, subRect.height)
                    , property
                    , GUIContent.none
                    , true);

                var newObject = EditorGUI.ObjectField
                    ( new Rect (subRect.x + (subRect.width * (1.0f - sObjectSelectorWidthProportion)) - 14.0f, subRect.y, subRect.width * sObjectSelectorWidthProportion + 14.0f, subRect.height)
                    , GUIContent.none
                    , currentResourceObj
                    , desiredType
                    , false);

                if (newObject != null)
                {
                    var path = AssetDatabase.GetAssetPath(newObject);
                    if (AssetDatabase.Contains(newObject) && AssetDatabase.IsMainAsset(newObject) && path != null && path != string.Empty)
                    {
                        var assetType = AssetDatabase.GetMainAssetTypeAtPath(path);
                        if (assetType == desiredType || assetType.IsSubclassOf(desiredType))
                        {

                            if (path.Contains(resPathPart))
                            {
                                var resName = Path.ChangeExtension(path, null);
                                var idx = resName.LastIndexOf(resPathPart);
                                resName = resName.Substring(idx + resPathPart.Length);

                                property.stringValue = resName;
                                // property.serializedObject.ApplyModifiedProperties();
                            }
                            else
                            {
                                EditorUtility.DisplayDialog("Invalid resource asset selected",
                                    "Selected asset must be located in one of the 'Resources' folders.", "OK");
                            }
                        }
                        else
                        {
                            EditorUtility.DisplayDialog("Invalid resource asset selected",
                                string.Format("Asset of wrong type selected ({0}), {1} expected.", assetType.Name, desiredType.Name), "OK");
                        }
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Invalid resource asset selected", "Only main root assets can be selected.", "OK");
                    }
                }
            }
            else
            {
                EditorGUI.HelpBox(new Rect (position.x, position.y, position.width, sWarningBoxHeight), "[ResourceSelector] attribute can only be used on string fields.", MessageType.Warning);
                EditorGUI.PropertyField (new Rect (position.x, position.y + sWarningBoxHeight, position.width, position.height - sWarningBoxHeight), property, label, true);
            }
        }
    }
}
