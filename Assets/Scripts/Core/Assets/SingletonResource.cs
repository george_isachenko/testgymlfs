﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Core.Assets
{
    public abstract class SingletonResource<TypeT> : ScriptableObject
        where TypeT : ScriptableObject
    {
        #region Public static API.
        public static TypeT instance
        {
            get
            {
                return LoadFromResource();
            }
        }

        public static TypeT LoadFromResource ()
        {
            if (instance_ == null)
            {
                var type = typeof(TypeT);
                var attr = (SingletonResourceAttribute)Attribute.GetCustomAttribute(type, typeof(SingletonResourceAttribute), true);

                var resPath = (attr != null) ? attr.ResourcePath : type.Name;
                instance_ = Resources.Load<TypeT> (resPath);

                Assert.IsNotNull(instance_, string.Format("Failed to load SingletonResource of type {0} at path '{1}'.", type.Name, resPath));
            }

            return instance_;
        }
        #endregion

        #region Private static fields.
        private static TypeT instance_;
        #endregion
    }

    public class SingletonResourceAttribute : Attribute
    {
        public SingletonResourceAttribute(string resourcePath)
        {
            this.resourcePath = resourcePath;
        }

        public string ResourcePath
        {
            get { return resourcePath; }
        }

        private string resourcePath;
    }
}