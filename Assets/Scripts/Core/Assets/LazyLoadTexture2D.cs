﻿using System;
using UnityEngine;

namespace Core.Assets
{
    [Serializable]
    public class LazyLoadTexture2D : LazyLoadResource<Texture2D>
    {
    }
}
