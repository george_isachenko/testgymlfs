﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Core.Assets
{
    [Serializable]
    public class LazyLoadResource<ResourceT>
        where ResourceT : UnityEngine.Object
    {
        #region Public fields.
        [ResourceSelector/*(TypeDonorFieldName = "resource_")*/]
        public string resourcePath;
        #endregion

        #region Public properties.
        public ResourceT resource
        {
            get
            {
                return resource_;
            }
        }

        public float progress
        {
            get
            {
                return (loadRequest != null ? loadRequest.progress : (resource_ != null ? 1.0f : 0.0f));
            }
        }
        #endregion

        #region Private data.
        private ResourceT resource_ = null;
        private ResourceRequest loadRequest = null;
        #endregion

        #region Public API.
        public IEnumerator LoadAsync (Action<ResourceT> onComplete)
        {
            if (loadRequest == null && resourcePath != null && resourcePath != string.Empty)
            {
                loadRequest = Resources.LoadAsync<ResourceT>(resourcePath);
            }

            if (loadRequest != null)
            {
                loadRequest.allowSceneActivation = true;

                yield return loadRequest;

                Assert.IsTrue(loadRequest.isDone);

                resource_ = loadRequest.asset as ResourceT;
                onComplete(resource_);
            }
            else
            {
                onComplete(null);
            }
        }
        #endregion
    }

}