﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Profiling;

public class ProfilerHelper
{
    [MenuItem("Window/Load Profiler Log...")]
    public static void LoadProfileLog()
    {
        var fileName = EditorUtility.OpenFilePanel("Select profiler log", Application.dataPath, "");
        if (fileName != null && fileName != string.Empty)
        {
            Debug.LogFormat("Selected profiler log file: '{0}'.", fileName);
            Profiler.AddFramesFromFile(fileName);
        }
    }
}
