﻿using System;
using UnityEngine;
using View;
using Core.Advertisement.Networks;

namespace Core.Advertisement
{
    public delegate void OnRewardedVideoAvailable();
    public delegate void OnRewardedVideoWatched();
    public delegate bool IsRewardedVideoEnableCanShow_delegate();

    public enum ShowResult
    {
        Finished = 0,
        Skipped,
        Failed
    }

    public class AdvertisementsData
    {
        public float curPeriodTimeLeft;
        public int videoWatchedCount;

        public AdvertisementsData()
        {
            curPeriodTimeLeft = 0.0f;
            videoWatchedCount = 0;
        }

        public void SpendTime(float seconds)
        {
            curPeriodTimeLeft -= seconds;
        }
    }

    public class Advertisements: MonoBehaviour
    {
        public static Advertisements instance { get; private set;}

        public string videoAdvertisementsZoneId; //rewardedVideo

        public Action UpdateAdverticementEnabled_Callback;
        private IAdvertisementNetwork currentNetwork;

        private const float cPeriodSeconds = 60.0f * 60.0f;

        private AdvertisementsData advertisementData;
        public AdvertisementsData data { get { return advertisementData;}}

        private int inAppsCount = 0;
        private int curLevel = 0;
        private bool wasSoundOn;
        private bool wasMusicOn;

        #region Unity API.
        void Awake ()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        void Start()
        {
            if (currentNetwork == null)
            {
#if UNITY_EDITOR
    #if UNITY_ADS
                currentNetwork = new UnityAdsScript(videoAdvertisementsZoneId);
    #else
                currentNetwork = null;
    #endif
#else
    #if (UNITY_ANDROID)
                //currentNetwork = FyberScript.instance;
                currentNetwork = new AppodealScript();
        #if UNITY_ADS
                // currentNetwork = new UnityAdsScript(videoAdvertisementsZoneId);
        #endif
    #elif (UNITY_IPHONE)
                //currentNetwork = new UnityAdsScript(videoAdvertisementsZoneId);
                currentNetwork = new AppodealScript();
        #if UNITY_ADS
                // currentNetwork = new UnityAdsScript(videoAdvertisementsZoneId);
        #endif
    #else
                currentNetwork = null;
    #endif
#endif

                if (currentNetwork != null)
                {
                    currentNetwork.onRewardedVideoWatched += OnVideoWatched;
                }
            }
        }

        void Update()
        {
            if (advertisementData == null)
                return;

            advertisementData.curPeriodTimeLeft -= Time.deltaTime;
            if (advertisementData.curPeriodTimeLeft <= 0.0f)
            {
                var adBalance = Data.DataTables.instance.balanceData.Advertisement;

                advertisementData.curPeriodTimeLeft = (curLevel >= adBalance.limit2Level) ? (float)adBalance.limit2Period.TotalSeconds :cPeriodSeconds;

                advertisementData.videoWatchedCount = 0;
                UpdateAdverticementEnabled_Callback?.Invoke();
            }

            #if DEBUG
            if(View.UI.DbgView.instance != null && View.UI.DbgView.instance.gameObject.activeSelf)
            {
                View.UI.DbgView.instance.adTimerLeft.text = "AD time left " + advertisementData.curPeriodTimeLeft;
            }
            #endif
        }
        #endregion

        bool IsRewardedVideoEnableCanShow()
        {
            if (advertisementData == null)
                return false;

            var adBalance = Data.DataTables.instance.balanceData.Advertisement;

            int videoLimit_WithoutInApps = (curLevel >= adBalance.limit2Level) ? adBalance.limit2ViewsForPeriod : adBalance.viewsForPeriod;
            int videoLimit_WithInApps = (curLevel >= adBalance.limit2Level) ? adBalance.limit2ViewsForPeriod_WithInApps : adBalance.viewsForPeriod_WithInApps;

            int videoLimit = (inAppsCount > 0) ? videoLimit_WithInApps : videoLimit_WithoutInApps;

            return advertisementData.videoWatchedCount < videoLimit;
        }

        void OnVideoWatched()
        {
            advertisementData.videoWatchedCount++;
            if(!IsRewardedVideoEnableCanShow())
                UpdateAdverticementEnabled_Callback?.Invoke();

            Sound.instance.musicOn = wasMusicOn;
            Sound.instance.soundOn = wasSoundOn;
        }

        #region IAdvertisementNetwork
        public event OnRewardedVideoAvailable onRewardedVideoAvailable
        {
            add
            {
                if (currentNetwork != null)
                {
                    currentNetwork.onRewardedVideoAvailable += value;
                }
            }

            remove
            {
                if (currentNetwork != null)
                {
                    currentNetwork.onRewardedVideoAvailable -= value;
                }
            }
        }

        public event OnRewardedVideoWatched onRewardedVideoWatched
        {
            add
            {
                if (currentNetwork != null)
                {
                    currentNetwork.onRewardedVideoWatched += value;
                }
            }

            remove
            {
                if (currentNetwork != null)
                {
                    currentNetwork.onRewardedVideoWatched -= value;
                }
            }
        }

        //public event IsRewardedVideoEnableCanShow_delegate isRewardedVideoCanShow;

        public void Init(AdvertisementsData advertisementData_)
        {
            advertisementData = advertisementData_ != null ? advertisementData_: new AdvertisementsData();

            if (currentNetwork != null)
            {
                currentNetwork.Init();
            }
        }

        public void UpdateAds()
        {
            if (currentNetwork != null)
            {
                currentNetwork.UpdateAds();
            }
        }

        public void RequestRewardedVideo()
        {
            if (currentNetwork != null)
            {
                currentNetwork.RequestRewardedVideo();
            }
        }

        public bool ShowRewardedAd(Action<ShowResult, int, string> callback, int curBucks)
        {
            if (currentNetwork != null)
            {
                bool result = currentNetwork.ShowRewardedAd(callback);

                if (result)
                {
                    wasMusicOn = Sound.instance.musicOn;
                    wasSoundOn = Sound.instance.soundOn;
                    Sound.instance.musicOn = false;
                    Sound.instance.soundOn = false;

                    Core.PersistentCore.instance.analytics.GameEvent("Watch VideoAd",
                        new Core.Analytics.GameEventParam("Level", curLevel.ToString()),
                        new Core.Analytics.GameEventParam("Curent FitBucks", curBucks.ToString()));
                }

                return result;
            }
            else
            {
                return false;
            }
        }

        public bool IsRewardedVideoAvailable()
        {
            if(!IsRewardedVideoEnableCanShow())
                return false;

            return (currentNetwork != null) ? currentNetwork.IsRewardedVideoAvailable() : false;
        }

        public void SetupCurLevel(int level)
        {
            curLevel = level;
            if (currentNetwork != null)
            {
                currentNetwork.SetupCurLevel(level);
            }
        }

        public void SetupInAppsCount(int count)
        {
            inAppsCount = count;
            if (currentNetwork != null)
            {
                currentNetwork.SetupInAppsCount(count);
            }
        }
        #endregion
    }
}
