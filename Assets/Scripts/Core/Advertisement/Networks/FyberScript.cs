﻿#if (UNITY_ANDROID) //|| (UNITY_IPHONE)
#define FYBER_SCRIPT_ENABLE
#endif

/*
using UnityEngine;
using System.Collections;
using FyberPlugin;
using System;

// BEGIN: Kingdom project edit.
#pragma warning disable 67
// END: Kingdom project edit.

namespace Core.Advertisement.Networks
{

    public class FyberScript : MonoBehaviour, IAdvertisementNetwork 
    {
        Ad rewardedVideoAd;
        private Action<ShowResult, int, string> callback = null;
        bool isShowing = false;
        int currentLevel = 0;

        const float cRequestCoolDown = 2.0f;
        float requestCooldown = cRequestCoolDown;

        public event OnRewardedVideoAvailable onRewardedVideoAvailable;
        public event OnRewardedVideoWatched onRewardedVideoWatched;

        public static FyberScript instance { get; private set;} 

        void Awake()
        {
            #if FYBER_SCRIPT_ENABLE            
            if (instance == null)
            {
                instance = this;

                string appId = string.Empty;
                string securityToken = string.Empty;

                #if UNITY_ANDROID
                appId = "60470";
                securityToken = "aaba3c492ee953188b5f59b777c751ef";
                #elif UNITY_IPHONE
                appId = "60471";
                securityToken = "54ac30e36a6cb86fc41c71b132349361";
                #endif

                FyberPlugin.Settings settings = Fyber.With(appId)
                                            // optional chaining methods
                                            //.WithUserId(userId)
                                            //.WithParameters(dictionary)
                .WithSecurityToken(securityToken)
                                            //.WithManualPrecaching()
                .Start();
            }
            #endif

        }

        void Start()
        {
            #if FYBER_SCRIPT_ENABLE 
            InnerRequest();
            #endif
        }

        void OnEnable()
        {
            FyberCallback.NativeError += OnNativeExceptionReceivedFromSDK;
            FyberCallback.AdNotAvailable += OnAdNotAvailable;   
            FyberCallback.RequestFail += OnRequestFail;

            FyberCallback.AdStarted += OnAdStarted;
            FyberCallback.AdFinished += OnAdFinished;  

            FyberCallback.VirtualCurrencySuccess += OnCurrencyResponse;
            FyberCallback.VirtualCurrencyError += OnCurrencyErrorResponse;   

            // Ad availability
            FyberCallback.AdAvailable += OnAdAvailable;

        }

        void OnDisable()
        {
            FyberCallback.NativeError -= OnNativeExceptionReceivedFromSDK;
            FyberCallback.AdNotAvailable -= OnAdNotAvailable;   
            FyberCallback.RequestFail -= OnRequestFail;

            FyberCallback.AdStarted -= OnAdStarted;
            FyberCallback.AdFinished -= OnAdFinished; 

            FyberCallback.VirtualCurrencySuccess -= OnCurrencyResponse;
            FyberCallback.VirtualCurrencyError -= OnCurrencyErrorResponse;    

            // Ad availability
            FyberCallback.AdAvailable -= OnAdAvailable;
        }

        public void OnNativeExceptionReceivedFromSDK(string message)
        {
            //handle exception
            Debug.LogError("FYBER: " + message);
        }

        private void OnAdAvailable(Ad ad)
        {
            switch(ad.AdFormat)
            {
                case AdFormat.REWARDED_VIDEO:
                    rewardedVideoAd = ad;
                    Debug.Log("VIDEO onRewardedVideoAvailable " + (onRewardedVideoAvailable != null).ToString());
                    if(onRewardedVideoAvailable != null)
                        onRewardedVideoAvailable();
                    //Debug.Log("VIDEO ENABLED " + this.name);
                    break;
                    //handle other ad formats if needed
            }
        }

        private void OnAdNotAvailable(AdFormat adFormat)
        {
            switch(adFormat)
            {
                case AdFormat.REWARDED_VIDEO:
                    rewardedVideoAd = null;
                    break;
                    //handle other ad formats if needed
            }

            Debug.Log(adFormat.ToString()+ " VIDEO NOT ENABLED");
            //InnerRequest();
            Debug.Log("VIDEO onRewardedVideoAvailable " + (onRewardedVideoAvailable != null).ToString());
            if(onRewardedVideoAvailable != null)
                onRewardedVideoAvailable();

        }

        private void OnRequestFail(RequestError error)
        {
            // process error
            //UnityEngine.Debug.Log("OnRequestError: " + error.Description);
            //InnerRequest();

            Debug.Log("REQUEST FAIL");
        }

        private void OnAdStarted(Ad ad)
        {
            isShowing = true;
            switch(ad.AdFormat)
            {
                case AdFormat.REWARDED_VIDEO:
                    rewardedVideoAd = null;
                    break;
                    //handle other ad formats if needed
            }

            Debug.Log("AD STARTED " + ad.AdFormat.ToString());

            if(onRewardedVideoAvailable != null)
                onRewardedVideoAvailable();
        }

        private void OnAdFinished(AdResult result)
        {
            isShowing = false;
            switch (result.AdFormat)
            {
                case AdFormat.REWARDED_VIDEO:
                    UnityEngine.Debug.Log("rewarded video closed with result: " + result.Status + "and message: " + result.Message);
                    if (onRewardedVideoWatched != null)
                        onRewardedVideoWatched();
                    break;
                    //handle other ad formats if needed
            }

            Debug.Log("AD FINISHED " + result.AdFormat.ToString() + " " + result.Status.ToString() + " " + result.Message);
                    
            //InnerRequest();
            requestCooldown = cRequestCoolDown;
        }

        public void OnCurrencyResponse(VirtualCurrencyResponse response)
        {
            isShowing = false;
            UnityEngine.Debug.Log("Delta of coins: " + response.DeltaOfCoins.ToString() +
                ". Transaction ID: " + response.LatestTransactionId +
                ".\nCurreny ID: " + response.CurrencyId +
                ". Currency Name: " + response.CurrencyName);

            Debug.Log("CALLBACK " + (callback != null).ToString());

            if (callback != null)
                callback(ShowResult.Finished, (int)response.DeltaOfCoins, response.CurrencyName);
        }

        public void OnCurrencyErrorResponse(VirtualCurrencyErrorResponse vcsError)
        {
            isShowing = false;
            UnityEngine.Debug.Log(System.String.Format("Delta of coins request failed.\n" +
                "Error Type: {0}\nError Code: {1}\nError Message: {2}",
                vcsError.Type, vcsError.Code, vcsError.Message));
        }

        void InnerRequest()
        {
            if (rewardedVideoAd != null || isShowing || currentLevel <= 0)
                return;

            VirtualCurrencyRequester virtualCurrencyRequester = VirtualCurrencyRequester.Create()
                // optional method chaining
                //.AddParameter("Level", currentLevel.ToString())
                //.AddParameters(dictionary)
                // Overrideing currency Id
                //.ForCurrencyId(currencyId)
                // Changing the GUI notification behaviour for when the user is rewarded
                //.NotifyUserOnReward(true)
                // you don't need to add a callback if you are using delegates
                //.WithCallback(vcsCallback)
                ;

            RewardedVideoRequester.Create()
            // optional method chaining
            //.AddParameter("key", "value")
                //.AddParameter("Level", currentLevel.ToString())
                //.AddParameters(dictionary)
                //.WithPlacementId(placementId)
                // changing the GUI notification behaviour when the user finishes viewing the video
                //.NotifyUserOnCompletion(true)
                // you can add a virtual currency requester to a video requester instead of requesting it separately
                .WithVirtualCurrencyRequester(virtualCurrencyRequester)
                // you don't need to add a callback if you are using delegates
                //.WithCallback(requestCallback)
                // requesting the video
                .Request(); 

            Debug.Log("VIDEOAD REQUESTED");
        }

        #region IAdvertisementNetwork
        void IAdvertisementNetwork.UpdateAds()
        {
            if (isShowing)
                return;

            requestCooldown -= Time.deltaTime;

            if (requestCooldown <= 0.0f)
            {
                InnerRequest();
                requestCooldown = cRequestCoolDown;
            }
        }

        bool IAdvertisementNetwork.IsRewardedVideoAvailable()
        {
            Debug.Log("IS VIDEOAD AVAILABLE: " + (rewardedVideoAd != null).ToString());
            return (rewardedVideoAd != null);
        }

        void IAdvertisementNetwork.RequestRewardedVideo()
        {
            InnerRequest();
        }

        bool IAdvertisementNetwork.ShowRewardedAd(Action<ShowResult, int, string> callback)
        {
            this.callback = null;
            if (rewardedVideoAd != null)
            {
                this.callback = callback;
                rewardedVideoAd.Start();
                rewardedVideoAd = null;
                return true;
            }       

            return false;
        }

        void IAdvertisementNetwork.SetupCurLevel(int level)
        {
            currentLevel = level;
            User.PutCustomValue("Level", level.ToString());
            Debug.Log("ADV: Set Level " + level.ToString());
        }

        #endregion
    }
}
*/
