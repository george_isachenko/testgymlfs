﻿using System;

namespace Core.Advertisement
{
    public interface IAdvertisementNetwork
    {
        event OnRewardedVideoAvailable onRewardedVideoAvailable;
        event OnRewardedVideoWatched onRewardedVideoWatched;

        void Init();
        void UpdateAds();
        void RequestRewardedVideo();
        bool ShowRewardedAd(Action<ShowResult, int, string> callback);
        bool IsRewardedVideoAvailable();
        void SetupCurLevel(int level);
        void SetupInAppsCount(int count);
    }
}

