﻿using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;

namespace Core.Advertisement.Networks
{
    public class AppodealScript: IAdvertisementNetwork, IRewardedVideoAdListener
    {
        public event OnRewardedVideoAvailable onRewardedVideoAvailable;
        public event OnRewardedVideoWatched onRewardedVideoWatched;

        private Action<ShowResult, int, string> callback = null;
        bool isShowing = false;

        int rewardAmount = 0;
        string rewardCurrencyName;
        bool wasRewarded = false;
        bool wasVideoLoaded = false;

        const float cRequestCoolDown = 2.0f;
//        float requestCooldown = cRequestCoolDown;

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
        string appKey = "";
#elif UNITY_ANDROID
        string appKey = "9591eb665e2f47a2f0f7046cce38c0ce3f07a77a0d3fb8ac";
#elif UNITY_IPHONE
        string appKey = "3847ab47ab018f829dfd460cf0c524244843f6728e77038e";
#else
        string appKey = "";
#endif

        bool inited = false;

        public AppodealScript()
        {
            inited = false;
        }

        #region Rewarded Video callback handlers
        public void onRewardedVideoLoaded() 
        {
            lock(this)
            {
                Debug.Log("Rewarded Video is loaded.");
                wasVideoLoaded = true;
            }
        }

        public void onRewardedVideoFailedToLoad() { Debug.Log("Video failed to load."); }
        public void onRewardedVideoShown() { Debug.Log("Video was shown."); }
        public void onRewardedVideoClosed() 
        {
            lock(this)
            {
                isShowing = false;
                Debug.Log("Video closed"); 
            }
        }

        public void onRewardedVideoFinished(int amount, string name) 
        {
            lock(this)
            {
                rewardAmount = amount;
                rewardCurrencyName = "FitBucks";//name;
                isShowing = false;
                wasRewarded = true;
                Debug.Log("Reward: " + amount + " " + name); 
            }
        }
        #endregion

        void InnerRequest()
        {
            if (!inited || Appodeal.isLoaded(Appodeal.REWARDED_VIDEO) || isShowing)
                return;

            Appodeal.cache(Appodeal.REWARDED_VIDEO);
        }

        #region IAdvertisementNetwork
        void IAdvertisementNetwork.Init()
        {
            if (inited)
                return;
                
            //Appodeal.disableLocationPermissionCheck();
            //Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
            //Appodeal.setTesting(true);
            Appodeal.setSmartBanners(false);
            Appodeal.initialize(appKey, Appodeal.REWARDED_VIDEO);
            
            Appodeal.setRewardedVideoCallbacks(this);
            //Appodeal.cache(Appodeal.REWARDED_VIDEO);

            inited = true;

            Debug.Log("APPODEAL: Inited");
        }
            
        void IAdvertisementNetwork.UpdateAds()
        {
            if (!inited)
            {
                wasRewarded = false;
                wasVideoLoaded = false;
                return;
            }

            lock(this)
            {
                if (wasRewarded)
                {
                    wasRewarded = false;

                    onRewardedVideoWatched?.Invoke();

                    callback?.Invoke(ShowResult.Finished, rewardAmount, rewardCurrencyName);
                }

                if (wasVideoLoaded)
                {
                    wasVideoLoaded = false;
                    onRewardedVideoAvailable?.Invoke();
                }
            }

            /*
            if (isShowing)
                return;

            requestCooldown -= Time.deltaTime;

            if (requestCooldown <= 0.0f)
            {
                InnerRequest();
                requestCooldown = cRequestCoolDown;
            }
            */
        }

        void IAdvertisementNetwork.RequestRewardedVideo()
        {
            InnerRequest();
        }

        bool IAdvertisementNetwork.ShowRewardedAd(Action<ShowResult, int, string> callback)
        {
            Debug.Log("SHOW REWARDED: inited=" + inited.ToString() + " isLoaded=" + Appodeal.isLoaded(Appodeal.REWARDED_VIDEO));

            if (!inited)
                return false;

            this.callback = null;
            if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
            {
                this.callback = callback;
                isShowing = Appodeal.show(Appodeal.REWARDED_VIDEO);
                Debug.Log("SHOWING " + isShowing.ToString());
                return isShowing;
            }

            return false;
        }

        bool IAdvertisementNetwork.IsRewardedVideoAvailable()
        {
            return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
        }

        void IAdvertisementNetwork.SetupCurLevel(int level)
        {
            Appodeal.setCustomRule("Level", level);
            Debug.Log("APPODEAL: Level = " + level.ToString());
        }

        void IAdvertisementNetwork.SetupInAppsCount(int count)
        {
            bool value = count > 0 ? true : false;
            Appodeal.setCustomRule("WasInApps", value);
            Debug.Log("APPODEAL: WasInApps = " + value.ToString());
        }
        #endregion
    }
}
