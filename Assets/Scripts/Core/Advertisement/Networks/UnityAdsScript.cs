﻿using System;
using UnityEngine;

namespace Core.Advertisement.Networks
{
#if UNITY_ADS
    // See also UnityAdsBuildProcessor.

    public class UnityAdsScript: IAdvertisementNetwork
    {
        private Action<ShowResult, int, string> callback = null;
        private bool showingAd = false;
        private bool videoAvailableNotified = false;

        private string zoneId;
        private UnityEngine.Advertisements.ShowOptions showOptions;

        public event OnRewardedVideoAvailable onRewardedVideoAvailable;
        public event OnRewardedVideoWatched onRewardedVideoWatched;

        public UnityAdsScript(string zoneId)
        {
            showOptions = new UnityEngine.Advertisements.ShowOptions { resultCallback = HandleShowResult };
            this.zoneId = zoneId;
        }
            
        public bool ShowRewardedAd(Action<ShowResult, int, string> callback)
        {
            this.callback = null;
            if (IsRewardedVideoAvailable())
            {
                this.callback = callback;
                showingAd = true;
                videoAvailableNotified = false; // Force update for next video after finished displaying this.
                UnityEngine.Advertisements.Advertisement.Show(zoneId, showOptions);
                return true;
            }

            return false;
        }

        public bool IsRewardedVideoAvailable()
        {
            return (!showingAd && UnityEngine.Advertisements.Advertisement.IsReady(zoneId));
        }

        public void Init()
        {
        }

        public void UpdateAds()
        {
            if (!videoAvailableNotified)
            {
                if (IsRewardedVideoAvailable())
                {
                    if (onRewardedVideoAvailable != null)
                    {
                        onRewardedVideoAvailable();
                        videoAvailableNotified = true;
                    }
                }
            }
        }

        public void RequestRewardedVideo()
        {
            videoAvailableNotified = false;
        }

        private void HandleShowResult(UnityEngine.Advertisements.ShowResult result)
        {
            showingAd = false;
            switch (result)
            {
                case UnityEngine.Advertisements.ShowResult.Finished:
                {
                    callback?.Invoke(ShowResult.Finished, 1, "FitBucks");

                    onRewardedVideoWatched?.Invoke();

                }; break;

                case UnityEngine.Advertisements.ShowResult.Skipped:
                {
                    Debug.Log("The ad was skipped before reaching the end.");
                    callback?.Invoke(ShowResult.Skipped, 0, "");
                }; break;

                case UnityEngine.Advertisements.ShowResult.Failed:
                {
                    Debug.LogError("The ad failed to be shown.");
                    callback?.Invoke(ShowResult.Failed, 0, "");
                }; break;
            }
        }

        public void SetupCurLevel(int level)
        {

        }

        public void SetupInAppsCount(int count)
        {
            
        }
    }
#endif // UNITY_ADS
}

