﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.SocialAPI.Networks;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SocialPlatforms;

namespace Core.SocialAPI
{
    public class SocialManager : MonoBehaviour, ISocialManager
    {
        #region Private data
        ISocialNetwork[] networks = null;
        #endregion

        #region ISocialManager interface properties
        IEnumerable<NetworkType> ISocialManager.allRegisteredNetworkTypes
        {
            get
            {
                return networks.Select(x => x.networkType);
            }
        }
        #endregion

        #region Unity Api
        void Awake ()
        {
            networks = GetComponents<ISocialNetwork>();
        }
        #endregion

        #region ISocialManager interface API
        void ISocialManager.Initialize()
        {
            foreach (var network in networks)
            {
                if (network.GetNetworkState() != NetworkState.Unavailable)
                {
                    Debug.LogFormat("Initializing social network '{0}'...", network.GetType().Name);
                    network.Initialize();
                }
                else
                {
                    Debug.LogFormat("Skipping initialization of social network '{0}', not available.", network.GetType().Name);
                }
            }
        }

        ISocialNetwork ISocialManager.GetNetwork (NetworkType network)
        {
            var result = networks.FirstOrDefault (n => n.networkType == network);

            Assert.IsNotNull(result, network.ToString() + " instance doesn't exists.");
            return result;
        }

        IEnumerable<ISocialNetwork> ISocialManager.GetAvailableNetworks (bool onlyLoggedIn)
        {
            return networks.Where(n => onlyLoggedIn ? n.GetNetworkState() == NetworkState.LoggedIn : n.GetNetworkState() != NetworkState.Unavailable );
        }

        IEnumerable<ISocialNetwork> ISocialManager.GetAvailableNetworks (NetworkFeatures features, bool onlyLoggedIn)
        {
            return networks.Where(n => (n.supportedFeatures & features) == features &&
                (onlyLoggedIn ? n.GetNetworkState() == NetworkState.LoggedIn : n.GetNetworkState() != NetworkState.Unavailable) );
        }
        #endregion
    }
}

