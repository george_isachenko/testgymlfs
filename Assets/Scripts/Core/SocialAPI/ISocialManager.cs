﻿using UnityEngine.SocialPlatforms;
using UnityEngine;
using System.Collections.Generic;
using Core.SocialAPI.Networks;
using System;

namespace Core.SocialAPI
{
    public enum NetworkState
    {
        Unavailable = 0,
        LoggedOut,
        LoggedIn
    }

    public enum LoginResult
    {
        OK = 0,
        Failed = 1,
    }

    public enum NetworkType
    {
        GooglePlay = 0,
        Facebook = 1,
        GameCenter = 2,
    }

    public enum ConnectionSource
    {
        Automatic = 0,
        FriendList,
        Settings
    }

    [Flags]
    public enum NetworkFeatures
    {
        SupportsAchievements = 1,
        SupportsScores = 2,
        SupportsSingleAchievementReset = 4,
        SupportsAllAchievementsReset = 8,
        SupportsInvites = 16,
    }

    public interface ISocialManager
    {
        IEnumerable<NetworkType> allRegisteredNetworkTypes { get; }

        void Initialize();

        ISocialNetwork GetNetwork (NetworkType network);
        IEnumerable<ISocialNetwork> GetAvailableNetworks (bool onlyLoggedIn = false);
        IEnumerable<ISocialNetwork> GetAvailableNetworks (NetworkFeatures features, bool onlyLoggedIn = false);
    }
}