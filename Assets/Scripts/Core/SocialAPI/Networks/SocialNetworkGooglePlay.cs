﻿//#define FORCE_GOOGLE_PLAY

#if (FORCE_GOOGLE_PLAY || UNITY_ANDROID)
//#if (FORCE_GOOGLE_PLAY || UNITY_ANDROID || (UNITY_IPHONE && !NO_GPGS))
#define USE_GOOGLE_PLAY
#endif

using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

namespace Core.SocialAPI.Networks
{
    public class SocialNetworkGooglePlay : MonoBehaviour, ISocialNetwork
    {
        #region Private data.
#if USE_GOOGLE_PLAY
        OnLoginComplete onLoginCompleteCallback = delegate { };
        IEnumerator loginJob = null;
#endif
        #endregion

        #region Unity API.
        #endregion

        #region ISocialNetwork
#pragma warning disable 67
        public event OnLoginStateChanged onLoginStateChanged = delegate{ };
#pragma warning restore 67

        public NetworkType networkType { get { return NetworkType.GooglePlay; } }
        public NetworkFeatures supportedFeatures
        {
            get
            {
                return  NetworkFeatures.SupportsAchievements |
                        NetworkFeatures.SupportsScores;
            }
        }

        public ConnectionSource connectionSource { get; private set; }

        NetworkState ISocialNetwork.GetNetworkState()
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
                return NetworkState.LoggedIn;
            else
                return NetworkState.LoggedOut;
#else
            return NetworkState.Unavailable;
#endif
        }

        void ISocialNetwork.Initialize()
        {
#if USE_GOOGLE_PLAY
            StartCoroutine(InitializationJob());
#endif
        }

        void ISocialNetwork.TryLogin(OnLoginComplete onComplete, ConnectionSource source)
        {
#if USE_GOOGLE_PLAY
            QueueLogin(onComplete, source, false);
#else
            onComplete(networkType, LoginResult.Failed);
#endif
        }

        void ISocialNetwork.Logout()
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
                instance.SignOut();
#endif
        }

        UserData ISocialNetwork.GetSelfUserData(bool emailIsOptional)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                var email = emailIsOptional ? null : instance.GetUserEmail();

                return new UserData
                    ( instance.GetUserId()
                    , instance.GetUserDisplayName()
                    , email != null ? email : string.Empty);
            }
#endif

            return null;
        }

        public void GetFriends(OnGetFriendsComplete onComplete)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            var friends = instance.localUser.friends.Select
                (i => new UserData(i.id, i.userName, string.Empty, i.image)).ToArray();
            onComplete(networkType, true, friends);
#else
            onComplete(networkType, false, null);
#endif
        }

        public void GetAvatarPicture(string userId, OnGetFriendAvatarImageComplete onComplete)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            var friends = instance.localUser.friends;
            var friend = friends.SingleOrDefault(x => x.id == userId);
            onComplete(networkType, friend != null ? friend.image : null);
#else
            onComplete(networkType, null);
#endif

        }

        void ISocialNetwork.ShowInviteDialog()
        {
            Debug.LogWarning("GooglePlay doesn't support invites.");
        }

        void ISocialNetwork.ReportAchievement(string achievementId, float progressPercents)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                instance.ReportProgress(achievementId, progressPercents, (result) =>
                {
                    if (!result)
                    {
                        Debug.LogErrorFormat("Failed to submit progress ({0}) for achievement '{1}'.", progressPercents, achievementId);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ShowAchievementsWindow()
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                instance.ShowAchievementsUI();
            }
#endif
        }

        void ISocialNetwork.ResetAchievement(string achievementId)
        {
            Debug.LogWarning("GooglePlay doesn't support achievements reset from client API.");
        }

        void ISocialNetwork.ResetAllAchievements()
        {
            Debug.LogWarning("GooglePlay doesn't support achievements reset from client API.");
        }

        void ISocialNetwork.ReportScore(long score, string leaderboardId)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                instance.ReportScore(score, leaderboardId, (result) =>
                {
                    if (!result)
                    {
                        Debug.LogErrorFormat("Failed to submit score {0} to leaderboard '{1}'.", score, leaderboardId);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ReportScore(long score, string leaderboardId, int context)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                var metadata = context.ToString();
                instance.ReportScore(score, leaderboardId, metadata, (result) =>
                {
                    if (!result)
                    {
                        Debug.LogErrorFormat("Failed to submit score {0} to leaderboard '{1}' with metadata {2}.", score, leaderboardId, metadata);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ShowLeaderboardWindow (TimeScope timeScope)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                instance.ShowLeaderboardUI();
            }
#endif
        }

        void ISocialNetwork.ShowLeaderboardWindow (string leaderboardId, TimeScope timeScope)
        {
#if USE_GOOGLE_PLAY
            var instance = PlayGamesPlatform.Instance;

            if (instance.localUser.authenticated)
            {
                LeaderboardTimeSpan timeSpan = LeaderboardTimeSpan.AllTime;
                switch (timeScope)
                {
                    case TimeScope.AllTime: 
                    timeSpan = LeaderboardTimeSpan.AllTime;
                    break;

                    case TimeScope.Week:
                    timeSpan = LeaderboardTimeSpan.Weekly;
                    break;

                    case TimeScope.Today:
                    timeSpan = LeaderboardTimeSpan.Daily;
                    break;
                }

                instance.ShowLeaderboardUI(leaderboardId, timeSpan, (status) => { });
            }
#endif
        }

        #endregion

        #region Private methods.
#if USE_GOOGLE_PLAY
        private IEnumerator InitializationJob ()
        {
            yield return null;

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
#if DEBUG
            PlayGamesPlatform.DebugLogEnabled = true;
#endif
            PlayGamesPlatform.Activate();

            if (loginJob == null)
                QueueLogin(null, ConnectionSource.Automatic, true);
        }

        private IEnumerator LoginJob (ConnectionSource source, bool silent)
        {
            yield return null;

            var instance = PlayGamesPlatform.Instance;

            var state = LoginResult.Failed;

            var busy = false;

            Action getFriendsFunc = () =>
            {
                busy = true;

                instance.LoadFriends(instance.localUser, (bool getFriendsSuccess) =>
                {
                    if (getFriendsSuccess)
                    {
                        var friends = instance.localUser.friends;

                        Debug.LogFormat("Google Play: got {0} friends.", friends.Count());
                        foreach (var friend in friends)
                        {
                            Debug.LogFormat("Google Play: Friend id: {0}, user name: '{1}', state: '{2}'.", friend.id, friend.userName, friend.state);
                        }
                    }
                    else
                    {
                        Debug.LogWarning("Google Play: failed to get friends.");
                    }

                    busy = false;
                });
            };

            if (!instance.localUser.authenticated)
            {
                busy = true;
                instance.Authenticate((bool loginSuccess) =>
                {
                    busy = false;

                    state = loginSuccess ? LoginResult.OK : LoginResult.Failed;

                    if (loginSuccess)
                    {
                        connectionSource = source;
                        getFriendsFunc();
                    }
                }, silent);
            }
            else
            {
                state = LoginResult.OK;
                connectionSource = source;
                getFriendsFunc();
            }

            if (busy)
            {
                yield return new WaitWhile(() => busy);
            }

            loginJob = null;

            if (onLoginCompleteCallback != null)
            {
                var tmpCallback = onLoginCompleteCallback;
                onLoginCompleteCallback = null;
                tmpCallback(networkType, state);
            }
            onLoginStateChanged(networkType, state);

            yield break;
        }

        private void QueueLogin(OnLoginComplete onComplete, ConnectionSource source, bool silent)
        {
            if (loginJob != null)
            {
                if (onComplete != null)
                    onLoginCompleteCallback += onComplete;
            }
            else
            {
                if (onComplete != null)
                    onLoginCompleteCallback = onComplete;
                else
                    onLoginCompleteCallback = delegate { };
                loginJob = LoginJob(source, silent);
                StartCoroutine(loginJob);
            }
        }
#endif
        #endregion
    }
}
