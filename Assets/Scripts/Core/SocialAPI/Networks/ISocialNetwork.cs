﻿using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Core.SocialAPI.Networks
{
    public delegate void OnLoginStateChanged (NetworkType network, LoginResult result);
    public delegate void OnLoginComplete (NetworkType network, LoginResult result);
    public delegate void OnGetFriendsComplete (NetworkType network, bool success, UserData[] users);
    public delegate void OnGetFriendAvatarImageComplete (NetworkType network, Texture2D image);

    public interface ISocialNetwork
    {
        event OnLoginStateChanged onLoginStateChanged;

        NetworkType networkType { get; }
        NetworkFeatures supportedFeatures { get; }
        ConnectionSource connectionSource { get; }

        NetworkState GetNetworkState ();
        void Initialize();

        void TryLogin (OnLoginComplete onComplete, ConnectionSource source);
        void Logout ();

        UserData GetSelfUserData (bool emailIsOptional);
        void GetFriends (OnGetFriendsComplete onComplete);
        void GetAvatarPicture (string userId, OnGetFriendAvatarImageComplete onComplete);

        void ShowInviteDialog();

        void ReportScore(long score, string leaderboardId);
        void ReportScore(long score, string leaderboardId, int context);
        void ShowLeaderboardWindow(TimeScope timeScope = TimeScope.AllTime);
        void ShowLeaderboardWindow(string leaderboardId, TimeScope timeScope = TimeScope.AllTime);

        void ReportAchievement(string achievementId, float progressPercents);
        void ShowAchievementsWindow();
        void ResetAchievement(string achievementId);
        void ResetAllAchievements();
    }
}