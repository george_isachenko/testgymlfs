﻿using UnityEngine;

namespace Core.SocialAPI.Networks
{
    public class UserData
    {
        public string userID;
        public string name;
        public string email;
        public Texture2D picture;

        public UserData (string userID)
        {
            this.userID = userID;
            this.name = string.Empty;
            this.email = string.Empty;
        }

        public UserData (string userID, string name)
        {
            this.userID = userID;
            this.name = name;
            this.email = string.Empty;
        }

        public UserData (string userID, string name, string email)
        {
            this.userID = userID;
            this.name = name;
            this.email = email;
        }

        public UserData (string userID, string name, string email, Texture2D picture)
        {
            this.userID = userID;
            this.name = name;
            this.email = email;
            this.picture = picture;
        }

        public override string ToString ()
        {
            return string.Format ("UserID: {0}, Name: '{1}', E-mail: '{2}'", userID, name, email);
        }
    }
}