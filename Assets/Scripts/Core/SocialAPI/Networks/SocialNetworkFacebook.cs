﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SocialPlatforms;

namespace Core.SocialAPI.Networks
{
    public class SocialNetworkFacebook : MonoBehaviour, ISocialNetwork
    {
        public enum PictureType
        {   // Do not rename these values, they're converted 'as is' to strings and must correspond to supported picture types:
            // https://developers.facebook.com/docs/graph-api/reference/user/picture/
            small,
            normal,
            album,
            large,
            square
        }

        #region Public (Inspector) data.
        [Tooltip("Facebook permissions to request on login. See https://developers.facebook.com/docs/facebook-login/permissions for details.")]
        [ContextMenuItem("Open documentation page", "EditorOpenPermissionsDocumentation")]
        public string[] requestedLoginPermissions;
        public PictureType friendsAvatarsPictureType = PictureType.normal;
        #endregion

        #region Private data.
        bool initializeInvoked = false;
        UserData me = null;
        OnLoginComplete onLoginCompleteCallback = delegate { };
        UserData[] friendsCache;
        IEnumerator initJob = null;
        IEnumerator getMeJob = null;
        IEnumerator loginJob = null;
        Dictionary<string, Texture2D> avatarsCache = new Dictionary<string, Texture2D>(64);
        #endregion

        #region Unity API.
        void OnApplicationPause (bool pauseStatus)
        {
            // Guides tell that after pause, it's better to re-init FB.
            if (!pauseStatus && initializeInvoked)
            {
                PerformInit();
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void EditorOpenPermissionsDocumentation()
        {
            Application.OpenURL("https://developers.facebook.com/docs/facebook-login/permissions");
        }
#endif
        #endregion

        #region ISocialNetwork API.
        public event OnLoginStateChanged onLoginStateChanged = delegate{ };

        public NetworkType networkType { get { return NetworkType.Facebook; } }
        public NetworkFeatures supportedFeatures
        {
            get
            {
                return NetworkFeatures.SupportsInvites;
            }
        }

        public ConnectionSource connectionSource { get; private set; }

        NetworkState ISocialNetwork.GetNetworkState ()
        {
            return (FB.IsInitialized && FB.IsLoggedIn)
                ? NetworkState.LoggedIn
                : NetworkState.LoggedOut;
        }

        void ISocialNetwork.Initialize()
        {
            if (!initializeInvoked)
            {
                initializeInvoked = true;
                PerformInit();
            }
        }

        void ISocialNetwork.TryLogin (OnLoginComplete onComplete, ConnectionSource source)
        {
            if (FB.IsLoggedIn && me != null)
            {
                onComplete(networkType, LoginResult.OK);
            }
            else
            {
                QueueLogin(onComplete, source);
            }
        }

        void ISocialNetwork.Logout ()
        {
            if (FB.IsInitialized && FB.IsLoggedIn)
            {
                FB.LogOut();
                Cleanup();
            }
        }

        UserData ISocialNetwork.GetSelfUserData (bool emailIsOptional)
        {
            return me;
        }

        void ISocialNetwork.GetFriends (OnGetFriendsComplete onComplete)
        {
            // TODO: Protect from multiple calls.
            StartCoroutine(GetFriendsJob(onComplete));
        }

        void ISocialNetwork.GetAvatarPicture (string userId, OnGetFriendAvatarImageComplete onComplete)
        {
            if (FB.IsInitialized)
            {
                Texture2D avatar;
                if (avatarsCache.TryGetValue(userId, out avatar))
                {
                    onComplete(networkType, avatar);
                }
                else
                {
                    FB.API
                        ( string.Format("{0}/picture?type={1}", userId, friendsAvatarsPictureType.ToString())
                        , HttpMethod.GET
                        , (result) =>
                        {
                            if (result != null && !result.Cancelled && result.Texture != null)
                            {
                                avatarsCache[userId] = result.Texture;

                                onComplete(networkType, result.Texture);
                            }
                            else
                            {
                                Debug.LogErrorFormat("Facebook: 'GET /{user}/picture' request failed: {0}."
                                    , result != null ? result.Error : string.Empty);

                                onComplete(networkType, null);
                            }
                        });
                }
            }
            else
            {
                onComplete(networkType, null);
            }
        }

        void ISocialNetwork.ShowInviteDialog ()
        {
            if (FB.IsInitialized)
            {
                FB.Mobile.AppInvite(
                    new Uri("https://fb.me/1675182129465398"),
                    new Uri("https://pbs.twimg.com/profile_images/537635340077391873/Q4UZ4LHP.png")
                );
            }
        }

        void ISocialNetwork.ReportScore(long score, string leaderboardId)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support leaderboards");
        }

        void ISocialNetwork.ReportScore(long score, string leaderboardId, int context)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support leaderboards");
        }

        void ISocialNetwork.ShowLeaderboardWindow (TimeScope timeScope)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support leaderboards");
        }

        void ISocialNetwork.ShowLeaderboardWindow (string leaderboardId, TimeScope timeScope)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support leaderboards");
        }

        void ISocialNetwork.ReportAchievement(string achievementId, float progressPercents)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support achievements");
        }

        void ISocialNetwork.ShowAchievementsWindow()
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support achievements");
        }

        void ISocialNetwork.ResetAchievement(string achievementId)
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support achievements");
        }

        void ISocialNetwork.ResetAllAchievements()
        {
            // Not supported.
            Debug.LogWarning("Facebook doesn't support achievements");
        }
        #endregion

        #region Private methods.

        void PerformInit ()
        {
            if (initJob == null)
            {
                initJob = PerformInitJob();
                StartCoroutine(initJob);
            }
        }

        private IEnumerator PerformInitJob ()
        {
            Assert.IsNotNull(initJob);

            yield return null;

            var initPending = false;

            Action activateAndGetMe = () =>
            {
                FB.ActivateApp();
                FB.LogAppEvent(AppEventName.ActivatedApp);

                if (FB.IsLoggedIn && me == null)
                {
                    Debug.Log("Facebook: Facebook is logged in right after initialization, trying to get information about me...");

                    StartGetMeJob();
                }
            };

            if (FB.IsInitialized)
            {
                activateAndGetMe();
            }
            else
            {
                initPending = true;

                FB.Init(() =>
                    {
                        initPending = false;

                        if (FB.IsInitialized)
                        {
                            activateAndGetMe();
                        }
                        else
                        {
                            Debug.LogWarning("Facebook: Failed to initialize Facebook.");
                        }
                    });
            }

            if (initPending || getMeJob != null)
            {
                yield return new WaitWhile(() => initPending || getMeJob != null);
            }

            initJob = null;
        }

        private IEnumerator LoginJob (ConnectionSource source)
        {
            yield return null;

            if (initJob != null)
            {
                yield return new WaitWhile(() => initJob != null);
            }

            var busy = false;

            if (FB.IsInitialized)
            {
                if (!FB.IsLoggedIn)
                {
                    busy = true;
                    FB.LogInWithReadPermissions(requestedLoginPermissions, (ILoginResult res) =>
                        {
                            busy = false;

                            if (res.Error != null)
                            {
                                Debug.LogErrorFormat("Facebook: Error Response: {0}\n", res.Error);
                            }
                            else if (!FB.IsLoggedIn)
                            {
                                Debug.Log("Facebook: Login canceled by Player.");
                            }
                            else
                            {
                                Debug.Log("Facebook: Logged in.");
                            }
                        });
                }
            }
            else
            {
                Debug.LogError("Facebook: Not initialized when trying to login.");
            }

            yield return new WaitWhile(() => busy);

            if (FB.IsInitialized && FB.IsLoggedIn && me == null)
            {
                StartGetMeJob();

                yield return new WaitUntil(() => getMeJob == null);
            }

            loginJob = null;

            var state = (FB.IsInitialized && FB.IsLoggedIn && me != null) ? LoginResult.OK : LoginResult.Failed;

            if (state == LoginResult.OK)
                connectionSource = source;

            if (onLoginCompleteCallback != null)
            {
                var tmpCallback = onLoginCompleteCallback;
                onLoginCompleteCallback = null;
                tmpCallback(networkType, state);
            }
            onLoginStateChanged(networkType, state);

            yield break;
        }

        private void QueueLogin(OnLoginComplete onComplete, ConnectionSource source)
        {
            if (loginJob != null)
            {
                if (onComplete != null)
                    onLoginCompleteCallback += onComplete;
            }
            else
            {
                loginJob = LoginJob(source);
                if (onComplete != null)
                    onLoginCompleteCallback = onComplete;
                else
                    onLoginCompleteCallback = delegate { };

                StartCoroutine(loginJob);
            }
        }

        private void StartGetMeJob ()
        {
            Assert.IsNull(getMeJob);
            getMeJob = GetMeJob();
            StartCoroutine(getMeJob);
        }

        private IEnumerator GetMeJob ()
        {
            Assert.IsTrue(FB.IsInitialized);
            Assert.IsTrue(FB.IsLoggedIn);
            Assert.IsNotNull(getMeJob);

            yield return null;

            Debug.Log("Facebook: Getting information about me...");

            var busy = true;
            FB.API("/me?fields=id,name,email", HttpMethod.GET, (result) =>
            {
                busy = false;

                if (result != null && !result.Cancelled && result.ResultDictionary != null)
                {
                    Debug.LogFormat("Facebook: Got information about me.");

                    var dic = result.ResultDictionary;
                    if (dic.ContainsKey("id"))
                    {
                        me = new UserData(dic["id"].ToString());
                        if (dic.ContainsKey("name"))
                            me.name = dic["name"].ToString();
                        if (dic.ContainsKey("email"))
                            me.email = dic["email"].ToString();

                        Debug.LogFormat("Facebook: Got self: {0}.", me);
                    }
                    else
                    {
                        Debug.LogErrorFormat("Facebook: 'GET /me' request response doesn't contain required field: id: {0}", dic);
                        FB.LogOut();
                        Cleanup();
                    }
                }
                else
                {
                    Debug.LogErrorFormat("Facebook: 'GET /me' request failed: {0}."
                        , result != null ? result.Error : string.Empty);
                    FB.LogOut();
                    Cleanup();
                }
            });

            yield return new WaitWhile(() => busy);
            getMeJob = null;
        }

        private IEnumerator GetFriendsJob (OnGetFriendsComplete onComplete)
        {
            yield return null;

            if (initJob != null)
            {
                yield return new WaitWhile(() => initJob != null);
            }

            if (FB.IsInitialized && FB.IsLoggedIn)
            {
                if (friendsCache != null)
                {
                    onComplete(networkType, true, friendsCache);
                }
                else
                {
                    var busy = true;
                    FB.API
                        ("me/friends?fields=id,name"
                        , HttpMethod.GET
                        , (result) =>
                        {
                            busy = false;

                            if (result != null && !result.Cancelled && result.ResultDictionary != null)
                            {
                                if (result.ResultDictionary.ContainsKey("data"))
                                {
                                    var data = result.ResultDictionary["data"] as IEnumerable<object>;
                                    if (data != null && data.Any())
                                    {
                                        var fields = data as List<object>;
                                        friendsCache = fields.Cast<Dictionary<string, object>>().Select(i => new UserData(i["id"].ToString(), i["name"].ToString())).ToArray();
                                        onComplete(networkType, true, friendsCache);
                                    }
                                    else
                                    {
                                        onComplete(networkType, true, new UserData[0]);
                                    }
                                }
                                else
                                {
                                    Debug.LogErrorFormat("Facebook: 'GET /me/friends' response doesn't contained data: Data available: {0}."
                                        , result.ResultDictionary);

                                    onComplete(networkType, false, null);
                                }
                            }
                            else
                            {
                                Debug.LogErrorFormat("Facebook: 'GET /me/friends' request failed: {0}."
                                    , result != null ? result.Error : string.Empty);

                                onComplete(networkType, false, null);
                            }
                        });

                    yield return new WaitWhile(() => busy);
                }
            }
            else
            {
                onComplete(networkType, false, null);
            }
        }

        private void Cleanup ()
        {
            friendsCache = null;
            avatarsCache.Clear();
            me = null;
        }
        #endregion
    }
}