﻿//#define FORCE_GAME_CENTER

#define GAME_CENTER_ENABLED

#if (FORCE_GAME_CENTER || ((UNITY_IPHONE || UNITY_TVOS) && !UNITY_EDITOR && GAME_CENTER_ENABLED))
#define USE_GAME_CENTER
#endif

#if USE_GAME_CENTER
using System;
using System.Collections;
using System.Linq;
using UnityEngine.SocialPlatforms.GameCenter;
#endif

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SocialPlatforms;

namespace Core.SocialAPI.Networks
{
    public class SocialNetworkGameCenter : MonoBehaviour, ISocialNetwork
    {
#if USE_GAME_CENTER
        OnLoginComplete onLoginCompleteCallback;
        OnGetFriendsComplete onGetFriendsCompleteCallback;
        IEnumerator loginJob = null;
        IEnumerator getFriendsJob = null;
#endif

        #region Unity API.
        void Awake ()
        {
#if USE_GAME_CENTER
#endif
        }

        void OnDestroy ()
        {
#if USE_GAME_CENTER
#endif
        }
        #endregion

        #region ISocialNetwork API.
#pragma warning disable 67
        public event OnLoginStateChanged onLoginStateChanged = delegate { };
#pragma warning restore 67

        public NetworkType networkType { get { return NetworkType.GameCenter; } }
        public NetworkFeatures supportedFeatures
        {
            get
            {
                return  NetworkFeatures.SupportsAchievements |
                        NetworkFeatures.SupportsAllAchievementsReset |
                        NetworkFeatures.SupportsScores;
            }
        }

        public ConnectionSource connectionSource { get; private set; }

        NetworkState ISocialNetwork.GetNetworkState ()
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform)
            {
                return (socialAPI.localUser != null && socialAPI.localUser.authenticated) ? NetworkState.LoggedIn : NetworkState.LoggedOut;
            }
            else
            {
                return NetworkState.Unavailable;
            }
#else
            return NetworkState.Unavailable;
#endif
        }

        void ISocialNetwork.Initialize ()
        {
#if USE_GAME_CENTER
            QueueLogin(null, ConnectionSource.Automatic);
#endif
        }

        void ISocialNetwork.TryLogin (OnLoginComplete onComplete, ConnectionSource source)
        {
            Assert.IsNotNull(onComplete);

#if USE_GAME_CENTER
            QueueLogin(onComplete, source);
#else
            onComplete(networkType, LoginResult.Failed);
#endif
        }

        void ISocialNetwork.Logout ()
        {
            Debug.LogWarning("You can't log out of GameCenter.");
        }

        UserData ISocialNetwork.GetSelfUserData (bool emailIsOptional)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                var lu = socialAPI.localUser;
                return new UserData(lu.id, lu.userName, string.Empty);
            }
#endif

            return null;
        }

        void ISocialNetwork.GetFriends (OnGetFriendsComplete onComplete)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                QueueGetFriends(onComplete);
            }
            else
            {
                onComplete(networkType, false, null);
            }
#else
            onComplete(networkType, false, null);
#endif
        }

        void ISocialNetwork.GetAvatarPicture (string userId, OnGetFriendAvatarImageComplete onComplete)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated &&
                socialAPI.localUser.friends != null)
            {
                var image = socialAPI.localUser.friends.Where(x => x.id == userId)?.Select(x => x.image).FirstOrDefault();
                onComplete(networkType, image);
            }
            else
            {
                onComplete(networkType, null);
            }
#else
            onComplete(networkType, null);
#endif
        }

        void ISocialNetwork.ShowInviteDialog ()
        {
            Debug.LogWarning("GameCenter doesn't support invites.");
        }

        void ISocialNetwork.ReportAchievement (string achievementId, float progressPercents)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                socialAPI.ReportProgress(achievementId, progressPercents, (result) =>
                {
                    if (!result)
                    {
                       Debug.LogFormat("Failed to report progress of {0} for achievement: {1}.", progressPercents, achievementId);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ShowAchievementsWindow ()
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                socialAPI.ShowAchievementsUI();
            }
#endif
        }

        void ISocialNetwork.ResetAchievement(string achievementId)
        {
            // Not supported.
            Debug.LogWarning("GameCenter doesn't support resetting single achievement.");
        }

        void ISocialNetwork.ResetAllAchievements()
        {
#if USE_GAME_CENTER
            GameCenterPlatform.ResetAllAchievements((result) => 
            {
                Debug.LogFormat("Reset achievements: Result: {0}.", result);
            });
#endif
        }

        void ISocialNetwork.ReportScore (long score, string leaderboardId)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                socialAPI.ReportScore(score, leaderboardId, (result) =>
                {
                    if (!result)
                    {
                       Debug.LogFormat("Failed to report progress of {0} for leaderboard: {1}.", score, leaderboardId);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ReportScore (long score, string leaderboardId, int context)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                socialAPI.ReportScore(score, leaderboardId, (result) =>
                {
                    if (!result)
                    {
                       Debug.LogFormat("Failed to report progress of {0} for leaderboard: {1}.", score, leaderboardId);
                    }
                });
            }
#endif
        }

        void ISocialNetwork.ShowLeaderboardWindow (TimeScope timeScope)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                socialAPI.ShowLeaderboardUI();
            }
#endif
        }

        void ISocialNetwork.ShowLeaderboardWindow (string leaderboardId, TimeScope timeScope)
        {
#if USE_GAME_CENTER
            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                GameCenterPlatform.ShowLeaderboardUI(leaderboardId, timeScope);
            }
#endif
        }
        #endregion

        #region Private methods.
#if USE_GAME_CENTER
        private IEnumerator LoginJob (ConnectionSource source)
        {
            yield return null;

            bool busy = false;
            var state = LoginResult.Failed;

            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null)
            {
                if (socialAPI.localUser.authenticated)
                {
                    state = LoginResult.OK;
                    connectionSource = source;
                }
                else
                {
                    busy = true;
                    socialAPI.localUser.Authenticate((result) =>
                    {
                        busy = false;

                        if (result)
                        {
                            state = LoginResult.OK;
                            connectionSource = source;
                        }
                        else
                        {
                            //Debug.LogWarningFormat("GameCenter authentication failed: {0}.", errorMessage);
                        }
                    });

                    yield return new WaitWhile(() => busy);
                }
            }

            loginJob = null;

            if (onLoginCompleteCallback != null)
            {
                var tmpCallback = onLoginCompleteCallback;
                onLoginCompleteCallback = null;
                tmpCallback(networkType, state);
            }
            onLoginStateChanged(networkType, state);

            yield break;
        }

        private IEnumerator GetFriendsJob ()
        {
            yield return null;

            bool busy = false;
            var status = false;

            var socialAPI = Social.Active;
            if (socialAPI != null && socialAPI is GameCenterPlatform && socialAPI.localUser != null && socialAPI.localUser.authenticated)
            {
                if (socialAPI.localUser.friends != null)
                {
                    status = true;
                }
                else
                {
                    busy = true;
                    socialAPI.localUser.LoadFriends((result) =>
                    {
                        busy = false;
                        status = result;
                    });

                    yield return new WaitWhile(() => busy);
                }
            }

            UserData[] friends = null;
            if (status && socialAPI.localUser.friends != null)
            {
                friends = socialAPI.localUser.friends.Select(x => new UserData(x.id, x.userName, string.Empty, x.image)).ToArray();
            }

            getFriendsJob = null;

            if (onGetFriendsCompleteCallback != null)
            {
                var tmpCallback = onGetFriendsCompleteCallback;
                onGetFriendsCompleteCallback = null;
                tmpCallback(networkType, status, friends);
            }

            yield break;
        }

        private void QueueLogin (OnLoginComplete onComplete, ConnectionSource source)
        {
            if (loginJob != null)
            {
                if (onComplete != null)
                {
                    if (onLoginCompleteCallback != null)
                        onLoginCompleteCallback += onComplete;
                    else
                        onLoginCompleteCallback = onComplete;
                }
            }
            else
            {
                loginJob = LoginJob(source);
                onLoginCompleteCallback = onComplete;
                StartCoroutine(loginJob);
            }
        }

        private void QueueGetFriends (OnGetFriendsComplete onComplete)
        {
            if (getFriendsJob != null)
            {
                if (onComplete != null)
                {
                    if (onGetFriendsCompleteCallback != null)
                        onGetFriendsCompleteCallback += onComplete;
                    else
                        onGetFriendsCompleteCallback = onComplete;
                }
            }
            else
            {
                getFriendsJob = GetFriendsJob();
                onGetFriendsCompleteCallback = onComplete;
                StartCoroutine(getFriendsJob);
            }
        }
#endif
        #endregion
    }
}
