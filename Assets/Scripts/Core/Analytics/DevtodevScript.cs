﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System;

namespace Core.Analytics
{
    public enum TutorialStepId
    {
        None,
        Begin,
        Client1Selected,
        EquipmentSelected,
        StartExercise,
        FinishExercise,
        Level2,
        Level3,
        OpenShop,
        SelectStepper,
        BuyStepper,
        Level4,
        OpenDailyQuest,
        Level5,
        VisitLina,
        OpenExpand,
        Expand,
        Level6,
        JuiceBarUnlock,
        FinishJuiceBarUnlock
    }

    public class DevtodevScript : MonoBehaviour 
    {
        
       void Awake()
       {
            #if UNITY_ANDROID
            // <param name="androidAppKey"> devtodev App ID for Google Play version of application </param>
            // <param name="androidAppSecret"> devtodev Secret key for Google Play version of application </param>
            DevToDev.Analytics.Initialize("dda33cee-01ee-0aed-b4b4-5d4a46d901f8", "rQ7MjUIpqn2Z31RLTkExYo5VbvzBmFfS");
            #elif UNITY_IOS
            // <param name="iosAppKey"> devtodev App ID for App Store version of application </param>
            // <param name="iosAppSecret"> devtodev Secret key for App Store version of application </param>
            DevToDev.Analytics.Initialize("f4e54177-c335-0bea-9362-07cb9d3b62f8", "src5JGRSlE6I3WqVw7uBfzFbkDeYo8hn");
            #endif

            /*
            #elif UNITY_WEBGL
            // <param name="webglAppKey"> devtodev App ID for Web version of application </param>
            // <param name="webglAppKey"> devtodev Secret key Web version of application </param>
            DevToDev.Analytics.Initialize(string webglAppKey, string webglAppSecret);
            #elif UNITY_STANDALONE_WIN
            // <param name="winAppKey"> devtodev App ID for Windows Store version of application </param>
            // <param name="winAppSecret"> devtodev Secret key for Windows Store version of application </param>
            DevToDev.Analytics.Initialize(string winAppKey, string winAppSecret);
            #endif
            */

        }

        public void TutorialStep(int step)
        {
            DevToDev.Analytics.Tutorial(step);
        }

        public void TutorialStart()
        {
            DevToDev.Analytics.Tutorial(DevToDev.TutorialState.Start);
        }

        public void TutorialFinish()
        {
            DevToDev.Analytics.Tutorial(DevToDev.TutorialState.Finish);
        }

        public void LevelUp(int level)
        {
            DevToDev.Analytics.LevelUp(level);
        }

        public void Purchase(string purchaseId, string purchaseType, int purchaseAmount,  Dictionary<string, int> resources)
        {
            DevToDev.Analytics.InAppPurchase(purchaseId, purchaseType, purchaseAmount, resources);
        }

        public void Purchase(string purchaseId, string purchaseType, int purchaseAmount, int purchasePrice, string purchaseCurrency)
        {
            DevToDev.Analytics.InAppPurchase(purchaseType, purchaseId, purchaseAmount, purchasePrice, purchaseCurrency);
        }

        public void Payment(string paymentId, float inAppPrice, string inAppName, string inAppCurrencyISOCode)
        {
            DevToDev.Analytics.RealPayment(paymentId, inAppPrice, inAppName, inAppCurrencyISOCode);
        }

        public void CustomEvent(string eventName)
        {
            Assert.IsNotNull(eventName);

            DevToDev.Analytics.CustomEvent(eventName);
        }

        public void CustomEvent(string eventName, string paramName, int value)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(paramName);

            DevToDev.CustomEventParams parms = new DevToDev.CustomEventParams();
            parms.AddParam(paramName, value);
            DevToDev.Analytics.CustomEvent(eventName, parms);
        }

        public void CustomEvent(string eventName, string paramName, long value)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(paramName);

            DevToDev.CustomEventParams parms = new DevToDev.CustomEventParams();
            parms.AddParam(paramName, value);
            DevToDev.Analytics.CustomEvent(eventName, parms);
        }

        public void CustomEvent(string eventName, string paramName, double value)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(paramName);

            DevToDev.CustomEventParams parms = new DevToDev.CustomEventParams();
            parms.AddParam(paramName, value);
            DevToDev.Analytics.CustomEvent(eventName, parms);
        }

        public void CustomEvent(string eventName, string paramName, string value)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(paramName);

            DevToDev.CustomEventParams parms = new DevToDev.CustomEventParams();
            parms.AddParam(paramName, value);
            DevToDev.Analytics.CustomEvent(eventName, parms);
        }

        public void CustomEvent(string eventName, params GameEventParam[] parameters)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(parameters);

            DevToDev.CustomEventParams parms = new DevToDev.CustomEventParams();
            foreach (var p in parameters)
            {
                if (p.value is float || p.value is double)
                {
                    parms.AddParam(p.paramName, Convert.ToDouble(p.value));
                }
                else if (p.value is int || p.value is uint || p.value is sbyte || p.value is byte || p.value is short || p.value is ushort)
                {
                    parms.AddParam(p.paramName, Convert.ToInt32 (p.value));
                }
                else if (p.value is long || p.value is ulong)
                {
                    parms.AddParam(p.paramName, Convert.ToInt64(p.value));
                }
                else
                {
                    parms.AddParam(p.paramName, p.ToString());
                }
            }
            DevToDev.Analytics.CustomEvent(eventName, parms);
        }
    }
}