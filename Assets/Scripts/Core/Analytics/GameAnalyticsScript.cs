﻿using System;
using GameAnalyticsSDK;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Core.Analytics
{
    public class GameAnalyticsScript
    {
#if (UNITY_ANDROID)
        class PurchaseReceipt
        {
            public string Store = string.Empty;
            public string TransactionID  = string.Empty;
            public string Payload = string.Empty;
        }

        class PayloadData
        {
            public string json = string.Empty;
            public string signature = string.Empty;
        }
#endif

        public void BusinessEvent(string currency, float amount, string itemType, string itemId, string cartType, string receiptInApp)
        {
#if (UNITY_ANDROID)

            string receipt = string.Empty;
            string signature = string.Empty;

            if (receiptInApp != null)
            {
                PurchaseReceipt purchaseReceipt = JsonConvert.DeserializeObject<PurchaseReceipt>(receiptInApp); 

                if(purchaseReceipt.Payload != null)
                {
                    PayloadData payloadData = JsonConvert.DeserializeObject<PayloadData>(purchaseReceipt.Payload); 
                    if (payloadData.json != null)
                        receipt = payloadData.json;

                    if (payloadData.signature != null)
                        signature = payloadData.signature;
                }
            }

            GameAnalytics.NewBusinessEventGooglePlay(currency, (int)amount, itemType, itemId, cartType, receipt, signature); 

#elif (UNITY_IOS) 

            // iOS - with receipt
            //GameAnalytics.NewBusinessEventIOS (string currency, int amount, string itemType, string itemId, string cartType, string receipt)

            // iOS - with autoFetchReceipt
            GameAnalytics.NewBusinessEventIOSAutoFetchReceipt (currency, (int)amount, itemType, itemId, cartType);

#endif
        }

        public void ProgressionEvent(int newLevel)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level " + newLevel);

            UnityEngine.Analytics.Analytics.CustomEvent("LevelUp", new Dictionary<string, object>
                {
                    { "Level",  newLevel}
                });
        }

        /*
        public void ProgressionEvent_1(string progression01, int value)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, progression01, value);
        }
        */

        public void SpendMoneyEvent(string currency, float amount, string itemType, string itemId)
        {
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, currency, amount, itemType, itemId);
        }

        public void AddMoneyEvent(string currency, float amount, string itemType, string itemId)
        {
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, currency, amount, itemType, itemId);
        }

        public void SetCustomDimension(int dimensionId, string strDimension)
        {
            switch (dimensionId)
            {
                case 0:
                    GameAnalytics.SetCustomDimension01(strDimension);
                    break;

                case 1:
                    GameAnalytics.SetCustomDimension02(strDimension);
                    break;

                case 2:
                    GameAnalytics.SetCustomDimension03(strDimension);
                    break;

            }
        }

    }
}

