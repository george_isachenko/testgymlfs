﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Core.Analytics
{
    public struct GameEventParam
    {
        public string paramName;
        public object value;

        public GameEventParam (string paramName, object value)
        {
            Assert.IsNotNull(paramName);
            Assert.IsNotNull(value);

            this.paramName = paramName;
            this.value = value;
        }

        public override string ToString()
        {
            return string.Format("{0} = {1};", paramName, value);
        }
    }

    public class Analytics
    {
        #region Types.
        public enum CustomDimension
        {
            Level
        }
            
        public class MoneyInfo
        {
            public string itemType;
            public string itemId;

            public MoneyInfo(string itemType_, string itemId_)
            {
                itemType = itemType_;
                itemId = itemId_;
            }

            public static MoneyInfo dummy
            {
                get
                {
                    return new MoneyInfo (string.Empty, string.Empty);
                }
            }

            public bool valid
            {
                get
                {
                    return (itemType != string.Empty && itemId != string.Empty);
                }
            }
        }

        enum AnalyticsFlags : int
        {
            NONE = 0,
            GA = 1,
            DEVTODEV = 2
        }
        #endregion

        #region Private fields.

        // Dictionary<CustomDimension, object> customDimensions = new Dictionary<CustomDimension, object>();
            
        DevtodevScript devtodevScript = null;
        GameAnalyticsScript gameAnalyticsScript = null;
        FacebookAnalyticsScript facebookScript = null;

        private int curLevel = 0;

        #endregion

        #region Public API.

        public Analytics()
        {
            devtodevScript = GameObject.Find("_PersistentCore").GetComponent<DevtodevScript>();
            gameAnalyticsScript = new GameAnalyticsScript();
            facebookScript = new FacebookAnalyticsScript();
        }

        public void BusinessEvent(string currency, float amount, string itemType, string itemId, string cartType, string receiptInApp, string transactionId)//, AnalyticsFlags sdkFlags = AnalyticsFlags.GA)
        {
            gameAnalyticsScript.BusinessEvent(currency, amount, itemType, itemId, cartType, receiptInApp);
            devtodevScript.Payment(transactionId, amount / 100.0f, itemId, currency);
            facebookScript.BusinessEvent(currency, amount, itemType, itemId);

            Debug.Log("<color=fuchsia>BusinessEvent:</color>" + " type: " + itemType + " id: " + itemId + " cartType: " + cartType + " receipt: " + receiptInApp);
        }

        public void ProgressionEvent(int newLevel)
        {
            //gameAnalyticsScript.ProgressionEvent(newLevel);
            facebookScript.ProgressionEvent(newLevel);
            devtodevScript.LevelUp(newLevel);

			Debug.LogFormat("<color=cyan>ProgressionEvent:</color> Level {0}", newLevel);
        }

        public void GameEvent(string eventName)
        {
            Assert.IsNotNull(eventName);

			Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}", eventName);

            devtodevScript.CustomEvent(eventName);

            //GameAnalytics.NewDesignEvent(eventName);
        }

        public void GameEvent(string eventName, string paramName, double value)
        {
            Assert.IsNotNull(eventName);

			Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}: {1} = {2}", eventName, paramName, value);

            devtodevScript.CustomEvent(eventName, paramName, value);
            //GameAnalytics.NewDesignEvent(eventName, eventValue);
        }

        public void GameEvent(string eventName, string paramName, int value)
        {
            Assert.IsNotNull(eventName);

			Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}: {1} = {2}", eventName, paramName, value);

            devtodevScript.CustomEvent(eventName, paramName, value);
            //GameAnalytics.NewDesignEvent(eventName, eventValue);
        }

        public void GameEvent(string eventName, string paramName, long value)
        {
            Assert.IsNotNull(eventName);

			Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}: {1} = {2}", eventName, paramName, value);

            devtodevScript.CustomEvent(eventName, paramName, value);

            //GameAnalytics.NewDesignEvent(eventName, eventValue);
        }

        public void GameEvent(string eventName, string paramName, string value)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(value);

			Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}: {1} = {2}", eventName, paramName, value);

            devtodevScript.CustomEvent(eventName, paramName, value);

            //GameAnalytics.NewDesignEvent(eventName, eventValue);
        }

        public void GameEvent(string eventName, params GameEventParam[] parameters)
        {
            Assert.IsNotNull(eventName);
            Assert.IsNotNull(parameters);

            string strParams = string.Empty;
            foreach (var it in parameters)
                strParams += (" " + it.ToString());

            Debug.LogFormat("<color=cyan>DesignEvent:</color> {0}: {1}", eventName, strParams);//parameters.ToString());

            devtodevScript.CustomEvent(eventName, parameters);

            //GameAnalytics.NewDesignEvent(eventName, eventValue);
        }

        public void SpendMoneyEvent(string currency, int amount, string itemType, string itemId)
        {
            if (currency == null || currency == string.Empty || itemType == null || itemId == null || itemType == string.Empty || itemId == string.Empty)
                return;

            string tCurrency = currency + "_Sink";

            //gameAnalyticsScript.SpendMoneyEvent(currency, amount, itemType, itemId);
            devtodevScript.Purchase(itemType, itemId, 1, amount, tCurrency);
            Debug.Log("<color=cyan>Purchase: </color>" + tCurrency + "," + amount.ToString() + "," + itemType + "," + itemId);
        }

        public void AddMoneyEvent(string currency, int amount, string itemType, string itemId, bool isInApp = false)
        {
            if (currency == null || currency == string.Empty || itemType == null || itemId == null || itemType == string.Empty || itemId == string.Empty)
                return;

            string tCurrency = currency + "_Source";

            DevToDev.Analytics.CurrencyAccrual((int)amount, tCurrency, isInApp ? DevToDev.AccrualType.Purchased : DevToDev.AccrualType.Earned);
            //Debug.Log("<color=cyan>CurrencyAccrual: </color>" + tCurrency + "," + amount.ToString());

            devtodevScript.Purchase(itemType, itemId, 1, amount, tCurrency);
            //gameAnalyticsScript.AddMoneyEvent(currency, amount, itemType, itemId);
            Debug.Log("<color=cyan>Purchase: </color>" + tCurrency + "," + amount.ToString() + "," + itemType + "," + itemId);
            
        }

        public void TutorialStep(TutorialStepId step)
        {
            int stepIndex = (int)step;
            devtodevScript.TutorialStep(stepIndex); 
            Debug.Log("<color=cyan>Tutorial Step: </color>" + stepIndex.ToString());
        }

        public void TutorialStart()
        {
            devtodevScript.TutorialStart();
            Debug.Log("<color=cyan>Tutorial Start </color>");
        }

        public void TutorialFinish()
        {
            devtodevScript.TutorialFinish();
            Debug.Log("<color=cyan>Tutorial Finish </color>");
        }

        public void SetCustomDimension(int dimensionId, CustomDimension dimensionType, object dimension)
        {
/*
            if (!customDimensions.ContainsKey(dimensionType))
                customDimensions.Add(dimensionType, dimension);
            else
                customDimensions[dimensionType] = dimension;
                       
            string strDimension = string.Empty;

            switch (dimensionType)
            {
                case CustomDimension.Level:
                    strDimension = "Level " + (int)dimension;
                    curLevel = (int)dimension;
                    break;

                default:
                    Debug.LogError("SetCustomDimension: dimensionType haven't description");
                    break;
            }

            gameAnalyticsScript.SetCustomDimension(dimensionId, strDimension);
*/
        }

        public void DetailGameEvent(string eventName, string paramName = null, string value = null)
        {
            if (!IsDetailAnalyticsEnable())
                return;

            if (paramName == null)
                GameEvent(eventName);
            else if(value == null)
                GameEvent(eventName, paramName, "");
            else
                GameEvent(eventName, paramName, value);
        }

        public void DetailGameEvent(string eventName, params GameEventParam[] parameters)
        {
            if (!IsDetailAnalyticsEnable())
                return;

            GameEvent(eventName, parameters);
        }

        #endregion

        #region Private methods.
        bool IsDetailAnalyticsEnable()
        {
            var tutBalance = Data.DataTables.instance.balanceData.Tutorial;
            if (curLevel > tutBalance.levelForDetailAnalytics)
                return false;

            if (PersistentCore.gameMode != GameMode.MainGame)
                return false;

            return true;
        }
        #endregion
    }
}