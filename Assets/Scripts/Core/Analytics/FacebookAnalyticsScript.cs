﻿using System;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

namespace Core.Analytics
{
    public class FacebookAnalyticsScript
    {
        public void BusinessEvent(string currency, float amount, string itemType, string itemId)
        {
            Dictionary<string, object> param = new Dictionary<string, object>(1);
            param.Add (itemType, itemId);
            if (FB.IsInitialized)
            {
                float tAmount = amount / 100.0f;

                Debug.Log("<color=fuchsia>Facebook LogPurchase:</color>" + " currency: " + currency + " amount: " + tAmount + " itemType: " + itemType + " itemId: " + itemId);
                FB.LogPurchase(tAmount, currency, param);
            }
        }

        public void ProgressionEvent(int newLevel)
        {
            if (FB.IsInitialized)
            {
                FB.LogAppEvent(AppEventName.AchievedLevel, newLevel, null);
            }
        }
    }
}

