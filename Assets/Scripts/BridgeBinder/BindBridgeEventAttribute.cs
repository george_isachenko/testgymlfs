﻿using System;

namespace BridgeBinder
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class BindBridgeEventAttribute : Attribute
    {
        public BindBridgeEventAttribute(string eventName)
        {
            this.eventName = eventName;
        }

        public string EventName
        {
            get { return eventName; }
        }

        private string eventName;
    }
} 
