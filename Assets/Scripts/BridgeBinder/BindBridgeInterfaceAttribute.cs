﻿using System;

namespace BridgeBinder
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class BindBridgeInterfaceAttribute : Attribute
    {
        public BindBridgeInterfaceAttribute(Type interfaceType)
        {
            this.interfaceType = interfaceType;
        }

        public Type InterfaceType
        {
            get { return interfaceType; }
        }

        private Type interfaceType;
    }
} 
