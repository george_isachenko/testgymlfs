﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System.Linq;

namespace BridgeBinder
{
    public class BindHelper
    {
        private static readonly BindingFlags eventBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
        private static readonly BindingFlags methodBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;

        public static bool BindBridge(GameObject side1, GameObject side2)
        {
            Assert.IsNotNull(side1);
            Assert.IsNotNull(side2);
            return (BindBridgeSide(side1, side2) && BindBridgeSide(side2, side1));
        }

        public static void UnbindBridge(GameObject side1, GameObject side2)
        {
            Assert.IsNotNull(side1);
            Assert.IsNotNull(side2);
            UnbindBridgeSide(side1, side2);
            UnbindBridgeSide(side2, side1);
        }

        private static bool BindBridgeSide(GameObject bindSide, GameObject bindTo)
        {
            Component[] allSideComponents = bindSide.GetComponents(typeof(Component));
            if (allSideComponents != null)
            {
                foreach (var component in allSideComponents)
                {
                    var bindInterfaceAttr = (BindBridgeInterfaceAttribute)Attribute.GetCustomAttribute(component.GetType(), typeof(BindBridgeInterfaceAttribute), true);
                    if (bindInterfaceAttr != null)
                    {   // Yes, it has [BindBridgeInterface], so look other part for its target.
                        var otherSideComponent = GetComponentImplementingInterface(bindTo, bindInterfaceAttr.InterfaceType);
                        if (otherSideComponent == null)
                        {
                            Debug.LogErrorFormat("BindBridge: Cannot find corresponding component implementing interface of type '{0}' on side \"{1}\" for component \"{2}\" ({3}) on side \"{4}\"."
                                , bindInterfaceAttr.InterfaceType.Name
                                , bindTo.name
                                , component.name
                                , component.GetType().Name
                                , bindSide.name);
                            return false;
                        }

                        if (!BindEvents(component, otherSideComponent))
                            return false;

                        if (!InvokeSubscribeMethod(component, otherSideComponent))
                            return false;
                    }
                }
            }

            return true;
        }

        private static bool UnbindBridgeSide(GameObject bindSide, GameObject bindTo)
        {
            Component[] allSideComponents = bindSide.GetComponents(typeof(Component));
            if (allSideComponents != null)
            {
                foreach (var component in allSideComponents)
                {
                    var bindInterfaceAttr = (BindBridgeInterfaceAttribute)Attribute.GetCustomAttribute(component.GetType(), typeof(BindBridgeInterfaceAttribute), true);
                    if (bindInterfaceAttr != null)
                    {   // Yes, it has [BindBridgeInterface], so look other part for its target.
                        var otherSideComponent = GetComponentImplementingInterface(bindTo, bindInterfaceAttr.InterfaceType);
                        if (otherSideComponent != null)
                        {
                            InvokeUnsubscribeMethod(component, otherSideComponent);
                            UnbindEvents(component, otherSideComponent);
                        }
                    }
                }
            }

            return true;
        }

        static Component GetComponentImplementingInterface(GameObject gameObject, Type interfaceType)
        {
            Assert.IsNotNull(gameObject);

            Component[] allComponents = gameObject.GetComponents(typeof(Component));
            if (allComponents != null)
            {
                foreach (var component in allComponents)
                {
                    if (interfaceType.IsAssignableFrom(component.GetType()))
                        return component;
                }
            }

            return null;
        }

        static bool BindEvents(Component component, Component bindToComponent)
        {
            Assert.IsNotNull(component);
            Assert.IsNotNull(bindToComponent);

            Type componentType = component.GetType();
            Type bindToComponentType = bindToComponent.GetType();

            var componentTypeMethods = IterateMethodInfoToBase(componentType, methodBindingFlags, typeof(MonoBehaviour), typeof(Component));

            foreach (var componentMethod in componentTypeMethods)
            {
                var bindEventAttr = (BindBridgeEventAttribute)Attribute.GetCustomAttribute(componentMethod, typeof(BindBridgeEventAttribute), true);
                if (bindEventAttr != null && bindEventAttr.EventName != null && bindEventAttr.EventName != string.Empty)
                {
                    var bindToEventInfo = bindToComponentType.GetEvent(bindEventAttr.EventName, eventBindingFlags);
                    if (bindToEventInfo == null)
                    {
                        Debug.LogErrorFormat
                            ("BindBridge.BindEvents: Cannot find event \"{0}\" in object \"{1}\" ({2}) to bind method \"{3}\" in object \"{4}\" ({5}), probably wrong event name in attribute [BindBridgeEvent]."
                            , bindEventAttr.EventName
                            , bindToComponent.name
                            , bindToComponentType.Name
                            , componentMethod.Name
                            , component.name
                            , componentType.Name);
                        return false;
                    }

                    Delegate handler = null;

                    try
                    {
                        handler = Delegate.CreateDelegate(bindToEventInfo.EventHandlerType, component, componentMethod);
                    }
                    catch (Exception)
                    {
                    }

                    if (handler == null)
                    {
                        Debug.LogErrorFormat
                            ("BindBridge.BindEvents: Failed to create delegate to bind method \"{0}\" in object \"{1}\" ({2}) to event \"{3}\" in object \"{4}\" ({5}), probably incompatible types or wrong event name in attribute [BindBridgeEvent]."
                            , componentMethod.Name
                            , component.name
                            , componentType.Name
                            , bindEventAttr.EventName
                            , bindToComponent.name
                            , bindToComponentType.Name);
                        return false;
                    }

                    try
                    {
                        // Using GetAddMethod() instead of directly calling bindToEventInfo.AddEventHandler() because it works in AOT mode.
                        var addMethod = bindToEventInfo.GetAddMethod();
                        if (addMethod != null)
                        {
                            addMethod.Invoke(bindToComponent, new object[] { handler });
                        }
                    }
                    catch (Exception)
                    {
                        Debug.LogErrorFormat
                            ("BindBridge.BindEvents: Failed to add event handler delegate to bind method \"{0}\" in object \"{1}\" ({2}) to event \"{3}\" in object \"{4}\" ({5}), probably incompatible types or wrong event name in attribute [BindBridgeEvent]."
                            , componentMethod.Name
                            , component.name
                            , componentType.Name
                            , bindEventAttr.EventName
                            , bindToComponent.name
                            , bindToComponentType.Name);
                        return false;
                    }
                }
            }

            return true;
        }

        static void UnbindEvents(Component component, Component bindToComponent)
        {
            Assert.IsNotNull(component);
            Assert.IsNotNull(bindToComponent);

            Type componentType = component.GetType();
            Type bindToComponentType = bindToComponent.GetType();

            var componentTypeMethods = IterateMethodInfoToBase(componentType, methodBindingFlags, typeof(MonoBehaviour), typeof(Component));

            foreach (var componentMethod in componentTypeMethods)
            {
                var bindEventAttr = (BindBridgeEventAttribute)Attribute.GetCustomAttribute(componentMethod, typeof(BindBridgeEventAttribute), true);
                if (bindEventAttr != null && bindEventAttr.EventName != null && bindEventAttr.EventName != string.Empty)
                {
                    var bindToEventInfo = bindToComponentType.GetEvent(bindEventAttr.EventName, eventBindingFlags);
                    if (bindToEventInfo == null)
                        continue;

                    Delegate handler = null;

                    try
                    {
                        handler = Delegate.CreateDelegate(bindToEventInfo.EventHandlerType, component, componentMethod);
                    }
                    catch (Exception)
                    {
                    }

                    if (handler == null)
                        continue;

                    try
                    {
                        // Using GetRemoveMethod() instead of directly calling bindToEventInfo.RemoveEventHandler() because it works in AOT mode.
                        var removeMethod = bindToEventInfo.GetRemoveMethod();
                        if (removeMethod != null)
                        {
                            removeMethod.Invoke(bindToComponent, new object[] { handler });
                        }
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }

        static bool InvokeSubscribeMethod(Component component, Component otherSideComponent)
        {
            Assert.IsNotNull(component);
            Type componentType = component.GetType();

            var componentTypeMethods = IterateMethodInfoToBase(componentType, methodBindingFlags, typeof(MonoBehaviour), typeof(Component));

            foreach (var componentMethod in componentTypeMethods)
            {
                if (Attribute.IsDefined(componentMethod, typeof(BindBridgeSubscribeAttribute), true))
                {
                    try
                    {
                        int paramsLength = componentMethod.GetParameters().Length;
                        if (paramsLength == 1)
                        {
                            componentMethod.Invoke(component, new object[] { otherSideComponent });
                        }
                        else if (paramsLength == 0)
                        {
                            componentMethod.Invoke(component, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.LogErrorFormat("BindBridge: Exception while invoking subscription method \"{0}\" in object \"{1}\" ({2}).\n\n{3}"
                            , componentMethod.Name
                            , component.name
                            , componentType.Name
                            , ex);
                        return false;
                    }
                    break;
                }
            }
            return true;
        }

        static void InvokeUnsubscribeMethod(Component component, Component otherSideComponent)
        {
            Assert.IsNotNull(component);
            Type componentType = component.GetType();

            var componentTypeMethods = IterateMethodInfoToBase(componentType, methodBindingFlags, typeof(MonoBehaviour), typeof(Component));

            foreach (var componentMethod in componentTypeMethods)
            {
                if (Attribute.IsDefined(componentMethod, typeof(BindBridgeUnsubscribeAttribute), true))
                {
                    try
                    {
                        int paramsLength = componentMethod.GetParameters().Length;
                        if (paramsLength == 1)
                        {
                            componentMethod.Invoke(component, new object[] { otherSideComponent });
                        }
                        else if (paramsLength == 0)
                        {
                            componentMethod.Invoke(component, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.LogErrorFormat("BindBridge: Exception while invoking unsubscription method \"{0}\" in object \"{1}\" ({2}).\n\n{3}"
                            , componentMethod.Name
                            , component.name
                            , componentType.Name
                            , ex);
                    }
                    break;
                }
            }
        }

        static IEnumerable<MethodInfo> IterateMethodInfoToBase
            ( Type type, BindingFlags bindingFlags, params Type[] stopBaseTypes)
        {
            while (type != null && (stopBaseTypes == null || stopBaseTypes.Length == 0 || !stopBaseTypes.Contains(type)))
            {
                var componentTypeMethods = type.GetMethods(bindingFlags);
                if (componentTypeMethods != null)
                {
                    foreach (var componentMethod in componentTypeMethods)
                    {
                        yield return componentMethod;
                    }
                }

                type = type.BaseType;
            } 

            yield break;
        }
    }
}
