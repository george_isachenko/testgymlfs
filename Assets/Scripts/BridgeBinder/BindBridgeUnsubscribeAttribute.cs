﻿using System;
using System.Reflection;

namespace BridgeBinder
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class BindBridgeUnsubscribeAttribute : Attribute
    {
    }
} 
