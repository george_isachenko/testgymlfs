﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using Core;

namespace BridgeBinder
{
    [DisallowMultipleComponent]
    public class BridgeBinder : MonoBehaviour
    {
        public GameObject logicSide;
        public GameObject presentationSide;

        private GameObject logicInstance = null;
        private GameObject presentationInstance = null;

        private void Start()
        {
            Assert.raiseExceptions = CoreDebugHacks.assertRaisesException;

            Assert.IsNotNull(logicSide);
            Assert.IsNotNull(presentationSide);

            // Delayed binding used, after all Awake() and Start() calls for scene objects.
            StartCoroutine(BindJob());
            return;
        }

        private void OnDestroy()
        {
            if (logicInstance != null && presentationInstance != null)
            {
                BindHelper.UnbindBridge (logicInstance, presentationInstance);
                Cleanup();
            }
        }

        private void Cleanup()
        {
            if (logicInstance != null)
            {
                Destroy(logicInstance);
                logicInstance = null;
            }

            if (presentationInstance != null)
            {
                Destroy(presentationInstance);
                presentationInstance = null;
            }
        }

        private IEnumerator BindJob()
        {
            if (logicSide != null && presentationSide != null)
            {
                Debug.LogFormat("BridgeBinder: Bind scheduled for next frame (current frame: {0})", Time.frameCount);
                yield return 0; // Skip first immediate call.

                Debug.LogFormat("BridgeBinder: Start binding (current frame: {0})...", Time.frameCount);

                logicInstance = Instantiate(logicSide) as GameObject;
                presentationInstance = Instantiate(presentationSide) as GameObject;

                Assert.IsNotNull(logicInstance);
                Assert.IsNotNull(presentationInstance);

                if (!BindHelper.BindBridge(logicInstance, presentationInstance))
                {
                    BindHelper.UnbindBridge(logicInstance, presentationInstance);
                    Cleanup();
                    yield break;
                }

                if (logicInstance != null)
                {
                    logicInstance.transform.SetParent(transform, false);
                }

                if (presentationInstance != null)
                {
                    presentationInstance.transform.SetParent(transform, false);
                }

                Debug.Log("BridgeBinder: Binding complete.");
            }

            yield break;
        }
    }
} 