﻿using UnityEngine;
using System;
using InspectorHelpers;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CustomAssets
{
    [CreateAssetMenu(menuName = "Kingdom/Materials Replacement Policy")]
    public class MaterialsReplacementPolicy : ScriptableObject
    {
        [Serializable]
        public struct PolicyData
        {
            [Serializable]
            public struct Substitution
            {
                [Serializable]
                public struct PropertyMode
                {
                    public enum Type
                    {
                        Color,
                        ColorArray,
                        Float,
                        FloatArray,
                        Int,
                        Matrix,
                        MatrixArray,
                        Texture,
                        TextureOffset,
                        TextureScale,
                        Vector,
                        VectorArray,
                    }

                    public string name;
                    public Type type;
                }

                public Material original;
                public Material substitute;
                public Shader originalShader;
                public Shader substituteShader;
                public PropertyMode[] propertiesFromSubstitute;
                public bool useRenderQueueFromSubstitute;
            }

            public string name;
            [ReorderableList]
            public Substitution[] substitutions;
        }

        [ReorderableList]
        public PolicyData[] array;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MaterialsReplacementPolicy), true)]
    public class MaterialReplacementPolicyEditor : Editor
    {
        UnityEngine.Object selectedObject;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();

            selectedObject = EditorGUILayout.ObjectField("Source object", selectedObject, typeof(GameObject), true);

            if (GUILayout.Button("Fill originals from object"))
            {
                if (selectedObject != null && selectedObject is GameObject)
                {
                    var go = selectedObject as GameObject;
                    AddUniqueMaterials(go.transform.GetComponents<Renderer>());
                    AddUniqueMaterials(go.transform.GetComponentsInChildren<Renderer>());
                }
            }
            serializedObject.ApplyModifiedProperties();
        }

        void AddUniqueMaterials(Renderer[] renderers)
        {
            if (renderers != null && target != null && target is MaterialsReplacementPolicy)
            {
                var obj = target as MaterialsReplacementPolicy;

                foreach (var renderer in renderers)
                {
                    if (renderer.sharedMaterials != null && renderer.sharedMaterials.Length > 0)
                    {
                        foreach (var mat in renderer.sharedMaterials)
                        {
                            for (var i = 0; i < obj.array.Length; i++)
                            {
                                if (!Array.Exists(obj.array[i].substitutions, substitution => substitution.original == mat))
                                {
                                    var dataList = new List<MaterialsReplacementPolicy.PolicyData.Substitution>(obj.array[i].substitutions);
                                    dataList.Add(new MaterialsReplacementPolicy.PolicyData.Substitution { original = mat });
                                    obj.array[i].substitutions = dataList.ToArray();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
#endif
}