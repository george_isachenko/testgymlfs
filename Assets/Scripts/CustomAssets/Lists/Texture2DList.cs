﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: Texture2D")]
    public class Texture2DList : ScriptableObject
    {
        [ReorderableList]
        public Texture2D[] array;
    }
}