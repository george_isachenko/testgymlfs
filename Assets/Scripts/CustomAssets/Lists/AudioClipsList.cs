﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: AudioClip")]
    public class AudioClipsList : ScriptableObject
    {
        [ReorderableList]
        public AudioClip[] array;
    }
}