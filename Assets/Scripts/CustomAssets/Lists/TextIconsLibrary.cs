﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/Text Icons Library")]
    public class TextIconsLibrary : ScriptableObject
    {
        #region Types.
        [System.Serializable]
        public struct NamedIcon
        {
            public string name;
            public Sprite image;
        }
        #endregion

        #region Public fields.
        [ReorderableList]
        public NamedIcon[] icons;
        #endregion

        #region Public methods.
        public int GetIconIndex (string str)
        {
            for (int i = 0; i < icons.Length; ++i)
            {
                if (str.IndexOf(icons[i].name) == 0)
                    return i;
            }

            return -1;
        }

        public Sprite GetSprite (string str)
        {
            for (int i = 0; i < icons.Length; ++i)
            {
                if (str.IndexOf(icons[i].name) == 0)
                    return icons[i].image;
            }

            return icons[0].image;
        }

        public int GetIconTextLength (int index)
        {
            return icons[index].name.Length;
        }

        public Sprite GetIcon (int index)
        {
            return icons[index].image;
        }

        public string GetStatText (int statIndex) // 0-cardio 1-torso 2-arms 3-legs 
        {
            string result = "";

            switch (statIndex)
            {
                case 0: result = "@cardio@"; break;
                case 1: result = "@torso@"; break;
                case 2: result = "@arms@"; break;
                case 3: result = "@legs@"; break;
            }

            return result;
        }

        public Sprite GetStatIcon (int statIndex) // 0-cardio 1-torso 2-arms 3-legs 
        {
            switch (statIndex)
            {
                case 0: return GetSprite("@cardio@");
                case 1: return GetSprite("@torso@");
                case 2: return GetSprite("@arms@");
                case 3: return GetSprite("@legs@");
            }

            return GetSprite("@legs@");
        }
        #endregion
    }
}