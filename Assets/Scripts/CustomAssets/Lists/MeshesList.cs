﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: Mesh")]
    public class MeshesList : ScriptableObject
    {
        [ReorderableList]
        public Mesh[] array;
    }
}