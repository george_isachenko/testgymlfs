﻿using System;
using Data.PlayerProfile;
using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/Storage Icons List")]
    public class StorageIconsList : ScriptableObject
    {
        [Serializable]
        public struct StorageItemDesc
        {
            public string name;
            public Sprite sprite;
        }

        [ReorderableList(LabelsEnumType = typeof(ResourceType))]
        public StorageItemDesc[]   items;
    }
}