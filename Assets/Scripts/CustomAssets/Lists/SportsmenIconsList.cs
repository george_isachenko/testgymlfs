﻿using System;
using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/Sportsmen Icons List")]
    public class SportsmenIconsList : ScriptableObject
    {
        [Serializable]
        public struct SportsmenItemDesc
        {
            public Sprite sprite;
            public string name;
        }

        [ReorderableList(LabelsEnumType = typeof(Data.Person.SportsmanType))]
        public SportsmenItemDesc[]   items;
    }
}