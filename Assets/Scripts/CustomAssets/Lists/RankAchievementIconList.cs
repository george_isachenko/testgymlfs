﻿using System;
using Data.RankAchievement;
using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/'RankAchievementIcon's List")]
    public class RankAchievementIconList : ScriptableObject
    {
        [Serializable]
        public struct RankAchievementItemDesc
        {
            public Sprite sprite;
        }

        [ReorderableList(LabelsEnumType = typeof(RankAchievementType))]
        public RankAchievementItemDesc[]   items;
    }
}