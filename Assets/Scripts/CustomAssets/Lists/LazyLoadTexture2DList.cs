﻿using InspectorHelpers;
using UnityEngine;
using Core.Assets;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: LazyLoadTexture2D")]
    public class LazyLoadTexture2DList : ScriptableObject
    {
        [ReorderableList]
        public LazyLoadTexture2D[] array;
    }
}
