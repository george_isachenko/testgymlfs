﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: Sprite")]
    public class SpritesList : ScriptableObject
    {
        [ReorderableList]
        public Sprite[] array;
    }
}