﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: GameObject")]
    public class GameObjectsList : ScriptableObject
    {
        [ReorderableList]
        public GameObject[] array;
    }
}