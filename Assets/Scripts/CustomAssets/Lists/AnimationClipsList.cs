﻿using InspectorHelpers;
using UnityEngine;

namespace CustomAssets.Lists
{
    [CreateAssetMenu(menuName = "Kingdom/Lists/List of: AnimationClip")]
    public class AnimationClipsList : ScriptableObject
    {
        [ReorderableList]
        public AnimationClip[] array;
    }
}