﻿using UnityEngine;

namespace InspectorHelpers.Editor
{
    public interface IListLabelsProvider
    {
        GUIContent GetLabelForItem(int itemIdx);
//         float GetMaxLabelWidth();
    }
} 