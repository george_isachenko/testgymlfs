﻿using Rotorz.ReorderableList;
using UnityEditor;
using UnityEngine;

namespace InspectorHelpers.Editor
{
    public abstract class ReorderableArraysAttributePropertyDrawerBase : PropertyDrawer
    {
        private static readonly char[] arrayFileTrimChars = { '[', ']' };

        // Provide easy access to the RegexAttribute for reading information from it.
        protected ReorderableArraysAttributeBase reorderableListAttribute
        {
            get
            {
                return (attribute as ReorderableArraysAttributeBase);
            }
        }

        protected ReorderableListFlags listFlags
        {
            get
            {
                ReorderableListFlags listFlags = 0;
                if (reorderableListAttribute.ShowIndices)
                    listFlags |= ReorderableListFlags.ShowIndices;
                //listFlags |= ReorderableListFlags.DisableClipping;
                return listFlags;
            }
        }

        protected float ProcessDefaultProperty(bool draw, Rect position, SerializedProperty property, GUIContent label, float currentY, bool includeChildren)
        {
            var fieldHeight = EditorGUI.GetPropertyHeight(property, label, includeChildren);

            if (draw)
            {
                Rect fieldRect = new Rect(position.x, currentY, position.width, fieldHeight);
                EditorGUI.PropertyField(fieldRect, property, label, includeChildren);
            }
            return fieldHeight;
        }

        protected float ProcessReorderableListField
            (bool draw
            , Rect position
            , SerializedProperty arrayEnclosingProperty
            , SerializedProperty arrayProperty
            , IListLabelsProvider listLabelsProvider
            , float currentY
            , int additionalIndent)
        {
            float indent = (EditorGUI.indentLevel + additionalIndent) * 16; // TODO: Hardcoded, find how to indent correctly!
            float fieldHeight = 0;
            if (reorderableListAttribute.DrawHeader)
            {
                var headerHeight = reorderableListAttribute.HeaderHeight;
                if (draw)
                {
                    Rect labelRect = new Rect(position.x + indent, currentY, position.width - indent, headerHeight);
                    ReorderableListGUI.Title(labelRect, string.Format("{0} ({1})"
                        , arrayProperty.name
                        , (arrayEnclosingProperty.type == "vector" ? fieldInfo.FieldType.Name.TrimEnd(arrayFileTrimChars) : arrayEnclosingProperty.type)));
                }
                currentY += headerHeight;
                fieldHeight += headerHeight;
            }

            IReorderableListAdaptor adaptor =
                (listLabelsProvider != null)
                ? new AdvancedSerializedPropertyAdaptor(arrayEnclosingProperty, arrayProperty, listLabelsProvider)
                : new AdvancedSerializedPropertyAdaptor(arrayEnclosingProperty, arrayProperty);
            /*
                            if (arrayEnclosingProperty.type == "vector")
                            {
                                adaptor = new SerializedPropertyAdaptor(property);
                            }
                            else
                            {
                                adaptor = new AdvancedSerializedPropertyAdaptor(property);
                            }
            */

            var listHeight = ReorderableListGUI.CalculateListFieldHeight(adaptor, listFlags);

            if (draw)
            {
                // Rect listRect = EditorGUILayout.GetControlRect(false, listHeight);
                Rect listRect = new Rect(position.x + indent, currentY, position.width - indent, listHeight);
                ReorderableListGUI.ListFieldAbsolute(listRect, adaptor, listFlags);
            }
            currentY += listHeight;
            fieldHeight += listHeight;

            return fieldHeight;
        }
    }
} 
