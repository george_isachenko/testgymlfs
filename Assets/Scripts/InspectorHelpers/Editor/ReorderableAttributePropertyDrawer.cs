﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace InspectorHelpers.Editor
{
    [CustomPropertyDrawer(typeof(ReorderableListAttribute), true)]
    public class ReorderableListAttributePropertyDrawer : ReorderableArraysAttributePropertyDrawerBase
    {
        // Provide easy access to the ReorderableListAttribute for reading information from it.
        protected new ReorderableListAttribute reorderableListAttribute
        {
            get
            {
                return (attribute as ReorderableListAttribute);
            }
        }

        private static List<bool> recursionGuards = new List<bool>(16);    // Can be grown further.
        private static Regex arrayMatchRegex = null;

        private static readonly string arrayPathRegex = @"((.*?)\.Array)\.data\[(\d+)]$";

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return ProcessHierarchy(false, new Rect(0, 0, 0, 0), property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ProcessHierarchy(true, position, property, label);
        }

        protected float ProcessHierarchy(bool draw, Rect position, SerializedProperty property, GUIContent label)
        {
            float currentY = position.y;

            SerializedProperty parentProperty, parentEnclosingProperty;
            int thisPropertyArrayIndex = GetParentArrayProperty(property, out parentProperty, out parentEnclosingProperty);
            if (thisPropertyArrayIndex >= 0 &&
                parentEnclosingProperty != null &&
                parentProperty != null &&
                parentProperty.arraySize > 0)
            {
                if (!IsInRecursion(parentProperty))
                {
                    float height = 0;
                    if (thisPropertyArrayIndex == 0)
                    {
                        SetRecursionGuard(parentProperty, true);
                        height = ProcessReorderableListField
                            (draw
                            , position
                            , parentEnclosingProperty
                            , parentProperty
                            , (reorderableListAttribute.LabelsEnumType != null)
                                ? new EnumNamesLabelsProvider(reorderableListAttribute.LabelsEnumType)
                                : null
                            , currentY
                            , 0);
                        SetRecursionGuard(parentProperty, false);
                    }
                    else
                    {
                        height = -2.0f; // Hack to compensate extra-height added. Some f*cking magic.
                    }

                    if (thisPropertyArrayIndex >= parentProperty.arraySize - 1)
                    {
                        ResetRecursionGuard(parentProperty);
                    }

                    return height;
                }
            }

            return /*draw ? 0 :*/ ProcessDefaultProperty(draw, position, property, label, currentY, true);
        }

        protected int GetParentArrayProperty
            (SerializedProperty property, out SerializedProperty parentProperty, out SerializedProperty parentEnclosingProperty)
        {
            try
            {
                if (arrayMatchRegex == null)
                {
                    arrayMatchRegex = new Regex(arrayPathRegex);
                }

                var match = arrayMatchRegex.Match(property.propertyPath);
                if (match != null && match.Success && match.Groups.Count == 4)
                {
                    string parentPropertyName = match.Groups[1].Value;
                    string parentEnclosingPropertyName = match.Groups[2].Value;
                    string idxStr = match.Groups[3].Value;

                    var tmpParentEnclosingProperty = property.serializedObject.FindProperty(parentEnclosingPropertyName);
                    var tmpParentProperty = property.serializedObject.FindProperty(parentPropertyName);
                    if (tmpParentProperty.isArray)
                    {
                        var tmpPropertyIndex = System.Convert.ToInt32(idxStr);
                        parentProperty = tmpParentProperty;
                        parentEnclosingProperty = tmpParentEnclosingProperty;
                        return tmpPropertyIndex;
                    }
                }
            }
            catch (Exception)
            { }

            parentProperty = null;
            parentEnclosingProperty = null;
            return -1;
        }

        protected bool IsInRecursion(SerializedProperty arrayProperty)
        {
            if (arrayProperty.depth >= 0 && arrayProperty.depth < recursionGuards.Count)
            {
                return recursionGuards[arrayProperty.depth];
            }

            return false;
        }

        protected void SetRecursionGuard(SerializedProperty arrayProperty, bool value)
        {
            if (arrayProperty.depth >= 0)
            {
                if (arrayProperty.depth >= recursionGuards.Count)
                {
                    if (recursionGuards.Capacity <= arrayProperty.depth)
                        recursionGuards.Capacity = arrayProperty.depth + 1;
                    while (arrayProperty.depth >= recursionGuards.Count)
                    {
                        recursionGuards.Add(false);
                    }
                }

                recursionGuards[arrayProperty.depth] = value;
            }
        }

        protected void ResetRecursionGuard(SerializedProperty arrayProperty)
        {
            for (var i = arrayProperty.depth; i < recursionGuards.Count; i++)
            {
                recursionGuards[i] = false;
            }
        }
    }
} 