﻿using Rotorz.ReorderableList;
using UnityEditor;
using UnityEngine;

namespace InspectorHelpers.Editor
{
    public class AdvancedSerializedPropertyAdaptor : SerializedPropertyAdaptor
    {
        //private SerializedProperty arrayEnclosingProperty;
        private IListLabelsProvider listLabelsProvider;

        private bool drawItemsLabel = false;
/*
        private float savedLabelWidth = 0;

        private float labelWidth
        {
            get
            {
                return (listLabelsProvider != null) ? listLabelsProvider.GetMaxLabelWidth() : -1;
            }
        }
*/

        #region Construction

        public AdvancedSerializedPropertyAdaptor(SerializedProperty arrayEnclosingProperty, SerializedProperty arrayProperty, float fixedItemHeight)
            : base(arrayProperty, fixedItemHeight)
        {
            //this.arrayEnclosingProperty = arrayEnclosingProperty;
            this.listLabelsProvider = null;
            this.drawItemsLabel = (arrayEnclosingProperty.type != "vector");
        }

        public AdvancedSerializedPropertyAdaptor(SerializedProperty arrayEnclosingProperty, SerializedProperty arrayProperty)
            : base(arrayProperty)
        {
            //this.arrayEnclosingProperty = arrayEnclosingProperty;
            this.listLabelsProvider = null;
            this.drawItemsLabel = (arrayEnclosingProperty.type != "vector");
        }

        public AdvancedSerializedPropertyAdaptor(SerializedProperty arrayEnclosingProperty, SerializedProperty arrayProperty, IListLabelsProvider listLabelsProvider, float fixedItemHeight)
            : base(arrayProperty, fixedItemHeight)
        {
            //this.arrayEnclosingProperty = arrayEnclosingProperty;
            this.listLabelsProvider = listLabelsProvider;
            this.drawItemsLabel = (listLabelsProvider != null || (arrayEnclosingProperty.type != "vector"));
        }

        public AdvancedSerializedPropertyAdaptor(SerializedProperty arrayEnclosingProperty, SerializedProperty arrayProperty, IListLabelsProvider listLabelsProvider)
            : base(arrayProperty)
        {
            //this.arrayEnclosingProperty = arrayEnclosingProperty;
            this.listLabelsProvider = listLabelsProvider;
            this.drawItemsLabel = (listLabelsProvider != null || (arrayEnclosingProperty.type != "vector"));
        }

        #endregion Construction

        // 		    public override void DrawItemBackground(Rect position, int index)
        //             {
        //                 EditorGUI.DrawRect(position, Color.magenta);
        // 		    }

/*
        public override void BeginGUI()
        {
            float cachedLabelWidth = labelWidth;
            if (cachedLabelWidth >= 0)
            {
                savedLabelWidth = EditorGUIUtility.labelWidth;
            }
        }
*/

/*
        public override void EndGUI()
        {
            float cachedLabelWidth = labelWidth;
            if (cachedLabelWidth >= 0)
            {
                EditorGUIUtility.labelWidth = savedLabelWidth;
            }
        }
*/

        public override void DrawItem(Rect position, int index)
        {
            try
            {
                SerializedProperty property = this[index];

//                 float cachedLabelWidth = labelWidth;
//                 if (cachedLabelWidth >= 0)
//                 {
//                     EditorGUIUtility.labelWidth = cachedLabelWidth + (EditorGUI.indentLevel + 1) * 16.0f;
//                 }

                GUIContent label = null;
                if (drawItemsLabel)
                {
                    label = (listLabelsProvider != null)
                        ? listLabelsProvider.GetLabelForItem(index)
                        : null;
                }
                else
                {
                    label = GUIContent.none;
                }

                int originalIndent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = property.hasChildren ? 1 : 0;
                EditorGUI.PropertyField
                    (position
                    , property
                    , label
                    , true);

                EditorGUI.indentLevel = originalIndent;
            }
            catch (ExitGUIException)
            {
            }
        }

        public override float GetItemHeight(int index)
        {
            return (FixedItemHeight != 0f)
                ? FixedItemHeight
                : EditorGUI.GetPropertyHeight(this[index], GUIContent.none, true);
        }
    }
} 