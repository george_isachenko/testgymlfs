﻿using System;
using UnityEngine;

namespace InspectorHelpers.Editor
{
    public class EnumNamesLabelsProvider : IListLabelsProvider
    {
        private GUIContent[] labels;
        private GUIContent emptyLabel;
//         private float labelsMaxWidth = -1;

        public EnumNamesLabelsProvider(Type enumType)
        {
            var enumNames = Enum.GetNames(enumType);
            labels = new GUIContent[enumNames.Length];

            for (var i = 0; i < enumNames.Length; i++)
            {
                labels[i] = new GUIContent(enumNames[i]);
//                 var size = GUI.skin.label.CalcSize(labels[i]);
//                 if (size.x > labelsMaxWidth)
//                     labelsMaxWidth = size.x;
            }

            emptyLabel = new GUIContent(" ");
        }

        GUIContent IListLabelsProvider.GetLabelForItem(int itemIdx)
        {
            return (itemIdx >= 0 && itemIdx < labels.Length) ? labels[itemIdx] : emptyLabel;
        }

/*
        float IListLabelsProvider.GetMaxLabelWidth()
        {
            return labelsMaxWidth;
        }
*/
    }
} 