﻿using UnityEditor;
using UnityEngine;

namespace InspectorHelpers.Editor
{
    //[CustomPropertyDrawer(typeof(ReorderableArraysAttribute))]
    public class ReorderableArraysAttributePropertyDrawer : ReorderableArraysAttributePropertyDrawerBase
    {
        // Provide easy access to the RegexAttribute for reading information from it.
        new protected ReorderableArraysAttribute reorderableListAttribute
        {
            get
            {
                return (attribute as ReorderableArraysAttribute);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return ProcessHierarchy(false, new Rect(0, 0, 0, 0), property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ProcessHierarchy(true, position, property, label);
        }

        protected float ProcessHierarchy(bool draw, Rect position, SerializedProperty property, GUIContent label)
        {
            float currentY = position.y;

            if (property.isExpanded && property.hasVisibleChildren)
            {
                if (!reorderableListAttribute.AllowMultiEditing && property.serializedObject.isEditingMultipleObjects)
                {
                    return ProcessDefaultProperty(draw, position, property, label, currentY, true);
                }

                bool nextIsOurArray = false;
                int rememberedIndentLevel = EditorGUI.indentLevel;
                SerializedProperty arrayEnclosingProperty = null;
                var endProperty = property.GetEndProperty(true);
                //Debug.Log("depth: [isArray] name, type, propertyType, displayName, propertyPath");
                do
                {
                    if (draw)
                        EditorGUI.indentLevel = rememberedIndentLevel + property.depth;

                    if (property.depth == 0)
                    {
                        currentY += ProcessDefaultProperty(draw, position, property, null, currentY, false);
                    }
                    else if (property.depth == 1)
                    {
                        if (nextIsOurArray)
                        {
                            nextIsOurArray = false;
                            currentY += ProcessReorderableListField(draw, position, arrayEnclosingProperty, property, null, currentY, 1);
                            arrayEnclosingProperty = null;
                        }
                        else if (property.isExpanded && MatchArrayProperty(property))
                        {
                            nextIsOurArray = true;
                            arrayEnclosingProperty = property;
                            currentY += ProcessDefaultProperty(draw, position, property, null, currentY, false);
                        }
                        else
                        {
                            currentY += ProcessDefaultProperty(draw, position, property, null, currentY, true);
                        }
                    }

                    //Debug.Log(string.Format("{0,-3} : [{1}] {2,-10}, {3,-10}, {4,-10}, {5,-20}, {6,-20}.", property.depth, property.isArray ? "*" : " ",  property.name, property.type, property.propertyType.ToString(), property.displayName, property.propertyPath));
                } while (IsContinueLoop(ref property, endProperty, property.depth < 1, nextIsOurArray));

                if (draw)
                    EditorGUI.indentLevel = rememberedIndentLevel;

                return currentY;
            }
            else
            {
                return ProcessDefaultProperty(draw, position, property, label, currentY, false);
            }
        }

        protected bool MatchArrayProperty(SerializedProperty property)
        {
            if (property.isArray && property.depth == 1) // Some magic.
            {
                var fieldNames = reorderableListAttribute.FieldNames;
                if (fieldNames == null || fieldNames.Length == 0)
                    return true; // Match all arrays.

                for (var i = 0; i < fieldNames.Length; i++)
                {
                    if (fieldNames[i] == property.name)
                        return true;
                }
            }
            return false;
        }

        protected bool IsContinueLoop(ref SerializedProperty property, SerializedProperty endProperty, bool enterChildren, bool nextAny)
        {
            bool nextAvailable = nextAny ? property.Next(true) : property.NextVisible(enterChildren);
            return (nextAvailable && !SerializedProperty.EqualContents(property, endProperty));
        }
    }
} 
