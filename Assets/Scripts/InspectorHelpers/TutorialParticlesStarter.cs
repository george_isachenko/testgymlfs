﻿using System;
using UnityEngine;
using System.Collections;

public class TutorialParticlesStarter : MonoBehaviour
{
    public GameObject[] particleSystems;
    public float AutoStopTime = 1;
    public Action onStopEmission;
    public bool needStop = true;

    public void PlayEffect()
    {
        StartCoroutine(AutoStop());
        foreach (var ps in particleSystems)
        {
            ps.SetActive(true);
        }
    }

    private IEnumerator AutoStop()
    {
        yield return new WaitForSeconds(AutoStopTime);
        if (needStop)
        {
            foreach (var psGo in particleSystems)
            {
                var ps = psGo.GetComponent<ParticleSystem>();
                var emiter = ps.emission;
                emiter.enabled = false;
            }
        }
        if (onStopEmission != null)
            onStopEmission();
    }
}
