using UnityEngine;
using View.UI.OverheadUI;

[ExecuteInEditMode]
public class TutorialPointer : MonoBehaviour, IWorldPositionProvider
{
    public Vector3 pointOffset;

    public Vector3 GetPoint()
    {
        return transform.position + pointOffset;
    }

#if UNITY_EDITOR
    private void Update()
    {
        var p = GetPoint();
        Debug.DrawLine(p + Vector3.left, p + Vector3.right);
        Debug.DrawLine(p + Vector3.forward, p + Vector3.back);
        Debug.DrawLine(p + Vector3.down, p + Vector3.up);
    }
#endif
    public bool allowUpdate { get; set; }

    public Vector3 GetWorldPosition()
    {
        return GetPoint();
    }
}