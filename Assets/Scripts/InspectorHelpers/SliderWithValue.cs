﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SliderWithValue : MonoBehaviour 
{
    public Slider slider;
    public Text text;
    public string unit;
    public byte decimals = 2;

    void OnEnable () 
    {
        if (slider != null)
        {
            slider.onValueChanged.AddListener(ChangeValue);
            ChangeValue(slider.value);
        }
    }

    void OnDisable()
    {
        if (slider != null)
        {
            slider.onValueChanged.RemoveListener(ChangeValue);//  RemoveAllListeners();
        }
    }

    void ChangeValue(float value)
    {
        if (text != null)
        {
            if (slider.wholeNumbers)
                text.text = ((int)value).ToString("d") + " " + unit;
            else
                text.text = value.ToString("n" + decimals) + " " + unit;
        }
    }
}