﻿using System;

namespace InspectorHelpers
{
    public sealed class ReorderableListAttribute : ReorderableArraysAttributeBase
    {
        public ReorderableListAttribute()
            : base()
        {
            this.labelsEnumType = null;
        }

        public Type LabelsEnumType
        {
            get { return labelsEnumType; }
            set { labelsEnumType = value; }
        }

        Type labelsEnumType;
    }
}