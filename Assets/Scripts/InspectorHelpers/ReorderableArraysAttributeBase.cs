﻿using UnityEngine;

namespace InspectorHelpers
{
    public abstract class ReorderableArraysAttributeBase : PropertyAttribute
    {
        protected ReorderableArraysAttributeBase()
        {
            this.allowMultiEditing = true;
            this.showIndices = true;
            this.drawHeader = true;
            this.headerHeight = 20.0f;
            //this.helpBoxHeight = 25.0f;
        }

        public bool AllowMultiEditing
        {
            get { return allowMultiEditing; }
            set { allowMultiEditing = value; }
        }

        public bool ShowIndices
        {
            get { return showIndices; }
            set { showIndices = value; }
        }

        public bool DrawHeader
        {
            get { return drawHeader; }
            set { drawHeader = value; }
        }

        public float HeaderHeight
        {
            get { return headerHeight; }
            set { headerHeight = value; }
        }

        //     public float HelpBoxHeight
        //     {
        //         get { return helpBoxHeight; }
        //         set { helpBoxHeight = value; }
        //     }

        protected bool allowMultiEditing;
        protected bool showIndices;
        protected bool drawHeader;
        protected float headerHeight;
        //     protected float helpBoxHeight;
    }
}