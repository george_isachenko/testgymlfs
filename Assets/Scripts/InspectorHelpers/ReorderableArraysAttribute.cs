﻿namespace InspectorHelpers
{
    public sealed class ReorderableArraysAttribute : ReorderableArraysAttributeBase
    {
        public ReorderableArraysAttribute(params string[] fieldNames)
            : base()
        {
            this.fieldNames = fieldNames;
        }

        public ReorderableArraysAttribute()
            : base()
        {
            this.fieldNames = null;
        }

        public string[] FieldNames
        {
            get
            {
                return fieldNames;
            }
        }

        private string[] fieldNames;
    }

}