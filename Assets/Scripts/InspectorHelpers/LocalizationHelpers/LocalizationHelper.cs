﻿using Data;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[AddComponentMenu("UI/Localization Helper", 11)]
public class LocalizationHelper : MonoBehaviour
{
    public string localizationKeyString
    {
        get
        {
            return this.localizationKey;
        }

        set
        {
            localizationKey = value;
            UpdateLocalizationText();
        }
    }

    #region Public fields.
    [SerializeField]
    protected string localizationKey;
    #endregion

    #region Private data.
    Text cachedTextComponent;
    #endregion

    #region Unity API.
    void Awake()
    {
        cachedTextComponent = GetComponent<Text>();
    }

    void OnEnable ()
    {
        UpdateLocalizationText();
    }
    #endregion

    #region Unity Editor API.
#if UNITY_EDITOR
    protected void OnValidate()
    {
        if (localizationKey != null)
        {
            var text = GetComponent<Text>();
            if (text != null)
            {
                text.text = GetLocalized();
            }
        }
    }
#endif
    #endregion

    #region Private API.
    private void UpdateLocalizationText ()
    {
        if (cachedTextComponent != null && localizationKey != null && localizationKey != string.Empty)
        {
            cachedTextComponent.text = GetLocalized();
        }
    }

    private string GetLocalized()
    {
        return Loc.Get(localizationKey);
    }
    #endregion
}
