﻿using System;
using Data;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR

using UnityEditor;

#endif

//[ExecuteInEditMode]
[AddComponentMenu("UI/Localization Text", 10)]
public class LocalizationText : Text
{
    [SerializeField]
    protected string m_LocKey = string.Empty;

    public override string text
    {
        get
        { 
            return GetLocalized();
        }
        set
        {
            m_LocKey = value;
            base.text = GetLocalized();
        }
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.text = GetLocalized();
        base.OnValidate();

        MigrateToLocalizationHelper();
    }

    private void MigrateToLocalizationHelper ()
    {
        var localizationKey = m_LocKey;

        var color = this.color;
        var raycastTarget = this.raycastTarget;
        var material = this.material;

        var alignByGeometry = this.alignByGeometry;
        var alignment = this.alignment;
        var font = this.font;
        var fontSize = this.fontSize;
        var fontStyle = this.fontStyle;
        var horizontalOverflow = this.horizontalOverflow;
        var lineSpacing = this.lineSpacing;
        var resizeTextForBestFit = this.resizeTextForBestFit;
        var resizeTextMinSize = this.resizeTextMinSize;
        var resizeTextMaxSize = this.resizeTextMaxSize;
        var supportRichText = this.supportRichText;
        var verticalOverflow = this.verticalOverflow;

        var go = gameObject;

        EditorApplication.CallbackFunction performReplace = () =>
        {
            if (go.GetComponent<LocalizationHelper>() == null)
            {
                DestroyImmediate(this, true);

                var textComponent = go.AddComponent<Text>();

                textComponent.color = color;
                textComponent.raycastTarget = raycastTarget;
                textComponent.material = material;
                textComponent.alignByGeometry = alignByGeometry;
                textComponent.alignment = alignment;
                textComponent.font = font;
                textComponent.fontSize = fontSize;
                textComponent.fontStyle = fontStyle;
                textComponent.horizontalOverflow = horizontalOverflow;
                textComponent.lineSpacing = lineSpacing;
                textComponent.resizeTextForBestFit = resizeTextForBestFit;
                textComponent.resizeTextMinSize = resizeTextMinSize;
                textComponent.resizeTextMaxSize = resizeTextMaxSize;
                textComponent.supportRichText = supportRichText;
                textComponent.verticalOverflow = verticalOverflow;

                var localizationHelper = go.AddComponent<LocalizationHelper>();
                localizationHelper.localizationKeyString = localizationKey;
            }
        };

        if (go.GetComponent<LocalizationHelper>() == null)
        {
            var assetPath = AssetDatabase.GetAssetPath(go);

            if (assetPath == null || assetPath == string.Empty)
            {
                // Scene object.
            }
            else
            {
                // Asset object.
                Debug.LogFormat ("Replacing LocalizationText component with Text + LocalizationHelper combination on asset object {0} ('{1}')..."
                    , go.GetInstanceID(), AssetDatabase.GetAssetPath(go));

                EditorApplication.delayCall += performReplace;
            }
        }
    }
#endif

    protected virtual string GetLocalized()
    {
        return Loc.Get(m_LocKey);
    }

    public void CopyFrom(Text source)
    {
        m_LocKey = source.text;
        color = source.color;
        raycastTarget = source.raycastTarget;
        material = source.material;
        font = source.font;
        supportRichText = source.supportRichText;
        resizeTextForBestFit = source.resizeTextForBestFit;
        resizeTextMinSize = source.resizeTextMinSize;
        resizeTextMaxSize = source.resizeTextMaxSize;
        alignment = source.alignment;
        alignByGeometry = source.alignByGeometry;
        fontSize = source.fontSize;
        horizontalOverflow = source.horizontalOverflow;
        verticalOverflow = source.verticalOverflow;
        lineSpacing = source.lineSpacing;
        fontStyle = source.fontStyle;
    }

    public void CopyTo(Text target)
    {
        target.text = m_LocKey;
        target.color = color;
        target.raycastTarget = raycastTarget;
        target.material = material;
        target.font = font;
        target.supportRichText = supportRichText;
        target.resizeTextForBestFit = resizeTextForBestFit;
        target.resizeTextMinSize = resizeTextMinSize;
        target.resizeTextMaxSize = resizeTextMaxSize;
        target.alignment = alignment;
        target.alignByGeometry = alignByGeometry;
        target.fontSize = fontSize;
        target.horizontalOverflow = horizontalOverflow;
        target.verticalOverflow = verticalOverflow;
        target.lineSpacing = lineSpacing;
        target.fontStyle = fontStyle;
    }
}

#if UNITY_EDITOR
    [CustomEditor(typeof(LocalizationText), true)]
    public class LocalizationTextEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.HelpBox("LocalizationText component is deprecated, use combination of Text and LocalizationHelper components.", MessageType.Warning);

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
