﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.UI;

[CanEditMultipleObjects]
[CustomEditor(typeof(LocalizationText), true)]
public class LocalizationTextEditor : GraphicEditor {

    private SerializedProperty m_Text;
    private SerializedProperty m_FontData;

    protected override void OnEnable()
    {
        base.OnEnable();
        this.m_Text = this.serializedObject.FindProperty("m_LocKey");
        this.m_FontData = this.serializedObject.FindProperty("m_FontData");
    }

    public override void OnInspectorGUI()
    {
        this.serializedObject.Update();
        EditorGUILayout.PropertyField(this.m_Text);
        EditorGUILayout.PropertyField(this.m_FontData);
        this.AppearanceControlsGUI();
        this.RaycastControlsGUI();
        this.serializedObject.ApplyModifiedProperties();
    }
}
