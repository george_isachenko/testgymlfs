﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public partial class LocMigrationManager : EditorWindow
{
    private Transform _root;
    private Texts _currents = new Texts();
    private Vector2 _textsPos;
    [NonSerialized] private bool _isSubscribedToUndo = false;
    
    void OnGUI()
    {
        if (!_isSubscribedToUndo)
        {
            Undo.undoRedoPerformed += OnUndo;
            _isSubscribedToUndo = true;
        }

        if (_root == null)
        {
            DrawRootGetDialog();
            return;
        }
        DrawHeader();
        DrawTexts();
    }

    private void OnUndo()
    {
        _currents.CollectDataFrom(_root);
        Repaint();
    }

    private void DrawTexts()
    {
        _textsPos = EditorGUILayout.BeginScrollView(_textsPos);
        _currents.OnGUI();
        if (_currents.IsUpdated)
        {
            Repaint();
        }
        EditorGUILayout.EndScrollView();
    }

    private void DrawRootGetDialog()
    {
        var go = GameObject.Find("_GUIRoot");
        if (go != null)
        {
            _root = go.transform;
        }
        else
        {
            _root = (Transform) EditorGUILayout.ObjectField("Put GUI parent here: ", _root, typeof (Transform), true);
        }
    }

    private void DrawHeader()
    {
        if (_currents.IsInited)
        {
            EditorGUILayout.TextField($"There are {_currents.Count} texts at scene.",
                EditorStyles.boldLabel);
            if (GUILayout.Button("Recollect texts"))
            {
                _currents.CollectDataFrom(_root);
                Repaint();
            }
        }
        else
        {
            _currents.CollectDataFrom(_root);
            DrawHeader();
        }
    }

    [MenuItem("Window/Localization/Migration manager")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof (LocMigrationManager));
    }
}

public partial class LocMigrationManager : EditorWindow
{
    private class TextAtSceneData
    {
        public Text Text;
        public GameObject SelfGO;
        public GameObject LockerGO;

        public TextAtSceneData()
        {
            IsUpdated = false;
        }

        public bool IsLocked => LockerGO != null;
        public bool IsUpdated { get; private set; }

        public void OnGUI()
        {
            IsUpdated = false;
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(Text != null && !(Text is LocalizationText) ? Text.text : "---", GUILayout.Width(50), GUILayout.Height(EditorStyles.label.lineHeight*1.2f));
            EditorGUILayout.ObjectField(SelfGO, typeof (GameObject), false, GUILayout.Width(50));
            GUIStyle style = new GUIStyle(EditorStyles.label);
            style.normal.textColor = IsLocked ? new Color(.5f, 0, 0, 1) : new Color(0, .5f, 0, 1);
            GUILayout.Label(IsLocked.ToString(), style,GUILayout.Width(70));
            if (SelfGO != null) Text = SelfGO.GetComponent<Text>();

            var baseColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.green;
            if (!(Text is LocalizationText) && GUILayout.Button("Swap to Localized"))
            {
                UpdateToLocalized();
                IsUpdated = true;
            }
            GUI.backgroundColor = Color.yellow;
            if ((Text is LocalizationText) && GUILayout.Button("Swap to Text"))
            {
                UpdateToText();
                IsUpdated = true;
            }
            GUI.backgroundColor = baseColor;
            EditorGUILayout.EndHorizontal();
        }

        private void UpdateToText()
        {
            var needSelect = Selection.activeGameObject == SelfGO;

            var targetGO = Instantiate(SelfGO);
            Undo.RegisterCreatedObjectUndo(targetGO, SelfGO.name);
            targetGO.transform.SetParent(SelfGO.transform.parent, false);
            targetGO.transform.SetSiblingIndex(SelfGO.transform.GetSiblingIndex());
            targetGO.name = SelfGO.name;

            var targetLoc = targetGO.GetComponent<LocalizationText>();
            DestroyImmediate(targetLoc);
            var targetTxt = targetGO.AddComponent<Text>();
            SelfGO.GetComponent<LocalizationText>().CopyTo(targetTxt);

            Undo.RegisterCompleteObjectUndo(SelfGO, "");
            Undo.DestroyObjectImmediate(SelfGO);
            SelfGO = targetGO;
            Text = targetLoc;
            UpdateLocker();
            if (needSelect) Selection.activeGameObject = targetGO;
        }

        private void UpdateToLocalized()
        {
            var needSelect = Selection.activeGameObject == SelfGO;

            var targetGO = Instantiate(SelfGO);
            Undo.RegisterCreatedObjectUndo(targetGO,SelfGO.name);
            targetGO.transform.SetParent(SelfGO.transform.parent, false);
            targetGO.transform.SetSiblingIndex(SelfGO.transform.GetSiblingIndex());
            targetGO.name = SelfGO.name;

            var targetTxt = targetGO.GetComponent<Text>();
            DestroyImmediate(targetTxt);
            var targetLoc = targetGO.AddComponent<LocalizationText>();
            targetLoc.CopyFrom(SelfGO.GetComponent<Text>());

            Undo.RegisterCompleteObjectUndo(SelfGO, "");
            Undo.DestroyObjectImmediate(SelfGO);
            SelfGO = targetGO;
            Text = targetLoc;
            UpdateLocker();
            if (needSelect) Selection.activeGameObject = targetGO;
        }

        public void UpdateLocker()
        {
            if (!SelfGO.activeSelf)
                LockerGO = SelfGO;
            else if (!SelfGO.activeInHierarchy)
            {
                var locker = SelfGO.transform;
                while (locker != null && locker.gameObject.activeSelf)
                    locker = locker.parent;
                LockerGO = locker ? locker.gameObject : null;
            }
        }
    }

    private class Texts : List<TextAtSceneData>
    {
        public Texts()
        {
            IsUpdated = false;
            IsInited = false;
        }

        public bool IsInited { get; private set; }
        public bool IsUpdated { get; private set; }

        public void CollectDataFrom(Transform parent)
        {
            Clear();
            var txts = GetAllChildrensForm<Text>(parent);
            foreach (var txt in txts)
            {
                var tas = new TextAtSceneData();
                tas.Text = txt;
                tas.SelfGO = txt.gameObject;
                tas.UpdateLocker();
                Add(tas);
            }
            IsInited = true;
            
        }

        private List<T> GetAllChildrensForm<T>(Transform parent)
        {
            var list = new List<T>();
            foreach (Transform child in parent)
            {
                var target = child.GetComponent<T>();
                if (target != null)
                    list.Add(target);
                list.AddRange(GetAllChildrensForm<T>(child));
            }
            return list;
        }

        public void OnGUI()
        {
            IsUpdated = false;
            // draw header
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Text",GUILayout.Width(50));
            GUILayout.Label("GO", GUILayout.Width(50));
            GUILayout.Label("IsLocked", GUILayout.Width(70));
            GUILayout.Label("Options");
            EditorGUILayout.EndHorizontal();
            // draw elements
            foreach (var text in this)
            {
                text.OnGUI();
                if (text.IsUpdated) IsUpdated = true;
            }
        }
    }
}