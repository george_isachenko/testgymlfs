﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NiceJson;
using System.Runtime.InteropServices;

namespace Helpers
{

    public class InputHelper : MonoBehaviour {

        public delegate void CustomInputEnd (string data = null);
        private static CustomInputEnd CustomInputCallBack; 

        #if UNITY_ANDROID
        private AndroidJavaObject _input;
        #elif UNITY_IOS
        [DllImport ("__Internal")]
        private static extern void inputLaunch (string text, bool mode);
        [DllImport ("__Internal")]
        private static extern void inputClose ();
        #endif

        static bool keyboardInput = false;
        string currentInputString = string.Empty;
        int maxCharacters;
        int keyboardHeight = 0;

        public static InputHelper _instance;
 
        //static bool wasClose;

        void Awake () {
            _instance = GetComponent<InputHelper> ();
        }

        void Update()
        {
            if (keyboardInput)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (CustomInputCallBack != null)
                        CustomInputCallBack (); 
                }
                if (Input.GetKeyDown(KeyCode.Backspace))
                {
                    if (currentInputString.Length > 0)
                    {
                        currentInputString = currentInputString.Substring(0, currentInputString.Length - 1);
                        if (CustomInputCallBack != null)
                            CustomInputCallBack(currentInputString);
                    }
                }
                else if (Input.anyKey)
                {
                    var inputString = Input.inputString;
                    if (inputString != string.Empty)
                    {
                        currentInputString += inputString;
                        if(currentInputString.Length > maxCharacters)
                            currentInputString = currentInputString.Substring(0, maxCharacters);

                        //Debug.Log(inputString);
                        if (CustomInputCallBack != null)
                            CustomInputCallBack(currentInputString);
                    }
                }
            }
        }

        public void ShowInput(string inputText, CustomInputEnd callBack, int maxCharacters_ = 18)
        {
            Debug.Log("INPUT SHOW");
            CustomInputCallBack = callBack;
            //wasClose = false;
            keyboardHeight = 0;
            maxCharacters = maxCharacters_;
            currentInputString = inputText;

            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            {
                OpenOnDevices(inputText);

                /*
                #if UNITY_ANDROID
                if (CanReopenKeyboard())
                    StartCoroutine(ReopenKeyboard());
                #endif
                */

                keyboardInput = false;
            }
            else
            {
                keyboardInput = true;
            }
        }

        void OpenOnDevices(string inputText)
        {
            #if UNITY_ANDROID
            if (_instance._input != null) 
                Close ();
            _instance._input = new AndroidJavaObject("ru.mopsicus.custominput.Plugin");
            _instance._input.Call("show", inputText, false);
            #elif UNITY_IOS 
            inputLaunch(inputText, false);
            #endif  
        }

        public static void Close () 
        {
            //wasClose = true;
            keyboardInput = false;

            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
            { 
                #if UNITY_ANDROID
                if (_instance._input != null) 
                {
                    _instance._input.Call("close");
                    _instance._input.Dispose();
                    _instance._input = null;
                }
                #elif UNITY_IOS
                inputClose ();
                #endif
            }
        }   


        void OnCustomInputAction (string data) {
         
            Debug.Log("INPUT ACTION " + data);

            JsonObject info = (JsonObject)JsonNode.ParseJsonString(data);      
            int code = info["code"];
            switch (code)
            {
                case 0: // close or return for singleline
                    //
                    // info["data"] in null
                    // can send event to save text in PlayerPrefs or send to server
                    //
                    if (CustomInputCallBack != null)
                        CustomInputCallBack(); 
                    break;
                case 1: // keyboard height change
                    //
                    // in info["data"] contains keyboard height 
                    // can send event to move smth when keyboard hide or show
                    // e.g. you can use my simple event manager http://mopsicus.ru/all/unity-event-manager-with-parameters/
                    //
                    keyboardHeight = info["data"];
                    Debug.Log("keyboard height = " + keyboardHeight.ToString());
                    break;
                case 2: // text input

                    string newString = info["data"];
                    int lenDiff = newString.Length - currentInputString.Length;
                    string resultStr = newString;

                    if (newString.Length > maxCharacters)
                    {
                        resultStr = (lenDiff < 0) ? 
                            newString.Substring(0, Mathf.Clamp(maxCharacters + lenDiff, 0, maxCharacters)) :
                            newString.Substring(0, maxCharacters);
                    }

                    currentInputString = newString;

                    if (CustomInputCallBack != null)
                        CustomInputCallBack(resultStr);         
                    break;
                default:
                    break;
            }
        }

        public void TestInput()
        {
            ShowInput("", null);     
        }

        /*
        IEnumerator ReopenKeyboard()
        {
            yield return new WaitForSeconds(4);

            if (CanReopenKeyboard())
            {
                #if UNITY_ANDROID
                if(_instance != null)
                    _instance._input.Call("show", currentInputString, false);
                #endif
            }
        }

        bool CanReopenKeyboard()
        {
            return !wasClose && keyboardHeight <= 0;
        }
        */
    }
}
