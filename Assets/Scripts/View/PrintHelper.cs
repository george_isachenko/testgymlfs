﻿using UnityEngine;
using System;
using System.Text;
using Data;

namespace View
{
    public static class PrintHelper
    {
        public static string GetTimeString(int seconds)
        {
            var ts = TimeSpan.FromSeconds(seconds);
            return GetTimeString(ts);
        }

        public static string GetTimeString(TimeSpan ts)
        {
            if (ts.TotalDays >= 1)
                return Loc.Get("timerDays", ((int)ts.TotalDays).ToString("0"), ts.Hours.ToString("00"));

            if (ts.TotalHours >= 1)
                return Loc.Get("timerHours", ((int)ts.TotalHours).ToString("0"), ts.Minutes.ToString("00"));

            if (ts.TotalMinutes >= 1)
                return Loc.Get("timerMinutes", ((int)ts.TotalMinutes).ToString("0"), ts.Seconds.ToString("00"));

            return Loc.Get("timerSeconds", ts.Seconds.ToString("0"));
        }

        public static string GetShortTimeString(TimeSpan duration)
        {
            var formatter = new StringBuilder();

            if (duration.Days > 0)
                formatter.Append(Loc.Get("timeShortDays", duration.Days));
            if (duration.Hours > 0)
                formatter.Append(Loc.Get("timeShortHours", duration.Hours));
            if (duration.Minutes > 0)
                formatter.Append(Loc.Get("timeShortMinutes", duration.Minutes));
            if (duration.Seconds > 0)
                formatter.Append(Loc.Get("timeShortSeconds", duration.Seconds));

            return formatter.ToString().Trim();
        }

        public static string GetMinSecTimeString(TimeSpan ts)
        {
            return string.Format("{0:D2}:{1:D2}", (int)ts.TotalMinutes, (int)ts.Seconds);
        }

        public static string FormatCurrencyAmount (int value)
        {
            return (value != 0) ? value.ToString("# ### ### ###").Trim() : value.ToString();
        }
    }
}
