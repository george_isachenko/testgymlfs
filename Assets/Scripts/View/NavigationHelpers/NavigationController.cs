﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using View.EditorHelpers;
using View.UI.OverheadUI;

namespace View.NavigationHelpers
{
    [AddComponentMenu("Kingdom/View/Navigation Helpers/Navigation Controller")]
    public class NavigationController : MonoBehaviour
    {
        #region Types.
        public enum NavigationResult
        {
            Complete = 0,
            Failed = 1,
            Stuck = 2,
        }
        #endregion

        #region Public events.
        public event Action onNavigationUpdate;
        public Func<int> onGetStoppedAvoidanceDelta;
        public event Action<NavigationResult> onNavigationComplete;
        #endregion

        #region Inspector fields
        [Tooltip("Time to wait until real stop when already in proximity of destination.")]
        [SerializeField]
        protected float stoppingDecisionTime = 0.5f;

        [SerializeField]
        [Tooltip("Time passed in a stuck detected state after which actor tries to pass by reducing its radius.")]
        protected float stuckFixingTime = 0.5f;

        [SerializeField]
        [Tooltip("Time passed in a stuck detected state after which it's decided that actor is stuck forever and failed to navigate.")]
        protected float stuckDecisionTime = 1.5f;

        [Tooltip("Percentage (0..1) of navigation agent speed below which agent's velocity is starting to be treated as 'stuck'.")]
        [Range(0.0f, 1.0f)]
        [SerializeField]
        protected float stuckDecisionVelocityFactor = 0.25f;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        protected float stuckDecisionTimeKnobackFactor = 0.75f;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        [Tooltip("Percentage (0..1) of initial agent radius below which agent's radius is not reduced.")]
        protected float stuckFixingMinAgentRadius = 0.25f;

        [Range(0.0f, 10.0f)]
        [SerializeField]
        [Tooltip("Rate of radius reducing speed per second of time passed in a 'stuck fixing state'\n\n(e.g. 0.5 means 'reduce radius by factor of 1/2 if staying in fixing state for 1 second').")]
        protected float stuckFixingRadiusChangeRate = 0.75f;

        [Range(1.0f, 10.0f)]
        [SerializeField]
        [Tooltip("Multiplier for agent's radius to use as 'max distance' limiter to use in a call to NavMesh.SamplePosition().")]
        protected float sampleAreaMaxDistanceRadiusMultiplier = 2.5f;
        #endregion

        #region Public properties.
        public Vector3 velocity
        {
            get
            {
                return navMeshAgent != null ? navMeshAgent.velocity : Vector3.zero ;
            }
        }
        #endregion

        #region Private data
        private NavMeshAgent navMeshAgent;
        private int initialAvoidancePriority;
        private float initialAgentRadius = 0;
        private float timePassedInProximity = 0;
        private float timePassedInStuckState = 0;
        private float stuckDecisionVelocitySpeed = 0;

        //private float distanceRemainingAtStartOfDetection = 0;
        #endregion

        #region Unity API
        void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();

            initialAvoidancePriority = navMeshAgent.avoidancePriority;
            initialAgentRadius = navMeshAgent.radius;
            // Debug.LogFormat("NavigationController.Start: {0}.", initialAvoidancePriority);
        }

        void Start()
        {
            if (navMeshAgent == null)
            {
                enabled = false; // Disable self.
            }
        }

        void Update()
        {
            //Debug.LogFormat("NavigationController.Update: Name: {0}.", gameObject.name);

            if (navMeshAgent != null)
            {
/*
                if (navMeshAgent.velocity.magnitude <= stuckDecisionVelocity * 3)
                {
                    Debug.LogFormat("Name: {0}, Velocity: {1}, Squared: {2}", gameObject.name, navMeshAgent.velocity.magnitude, navMeshAgent.velocity.sqrMagnitude);
                }
*/

/*
                if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance * 1.5f)
                     Debug.LogFormat("NavigationReachedHandler.Update(): Name: {0}, Remaining: {1}, Stop distance: {2}"
                        , gameObject.name, navMeshAgent.remainingDistance, navMeshAgent.stoppingDistance);
*/
                var hasPath = navMeshAgent.hasPath;
                var pathPending = navMeshAgent.pathPending;

                if (hasPath && !pathPending && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                {
                    timePassedInProximity += Time.deltaTime;
                }
                else
                {
                    timePassedInProximity = 0;
                }

                bool stuck = DetectStuckCondition();

                if (stuck ||
                    (timePassedInProximity >= stoppingDecisionTime) ||
                    (!hasPath && !pathPending))
                {
                    var pathStatus = navMeshAgent.pathStatus;

                    StopMovement();

                    onNavigationComplete?.Invoke((!stuck && pathStatus == NavMeshPathStatus.PathComplete) 
                        ? NavigationResult.Complete
                        : (stuck ? NavigationResult.Stuck : NavigationResult.Failed));
                }
                else
                {
                    if (hasPath && !pathPending)
                        onNavigationUpdate?.Invoke();
                }
            }
            else
            {
                enabled = false; // Disable self.
            }
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (stoppingDecisionTime < 0)
            {
                stoppingDecisionTime = 0;
            }

            if (stuckDecisionTime < 0)
            {
                stuckDecisionTime = 0;
            }
        }
#endif
        #endregion

        #region Public API
        public bool NavigateTo (Vector3 destination, float speed, int priorityDelta, int areaMask)
        {
            //Debug.LogFormat("NavigationController.NavigateTo: Name: {0}.", gameObject.name);

            if (navMeshAgent != null)
            {
                NavigateCommon(speed, priorityDelta, areaMask);
                if (navMeshAgent.SetDestination(destination))
                {
                    return true;
                }
                else
                {
                    Debug.LogWarningFormat("Name: {0}. Failed to set destination destination. Destination: {1}, Priority delta: {2}."
                        , gameObject.name, destination, priorityDelta);
                    StopMovement();
                }
            }
            return false;
        }

        public bool NavigateTo (NavMeshPath path, float speed, int priorityDelta, int areaMask)
        {
            //Debug.LogFormat("NavigationController.NavigateTo: Name: {0}.", gameObject.name);

            if (navMeshAgent != null)
            {
                NavigateCommon(speed, priorityDelta, areaMask);
                if (navMeshAgent.SetPath(path))
                {
                    return true;
                }
                else
                {
                    Debug.LogWarningFormat("Name: {0}. Failed to set navigation path: Path last corner: {1}, Status: {2}, Priority delta: {3}."
                        , gameObject.name
                        , (path.corners != null && path.corners.Length > 0) ? path.corners[path.corners.Length - 1].ToString() : "<empty>"
                        , path.status
                        , priorityDelta);
                    StopMovement();
                }
            }
            return false;
        }

        public bool SamplePosition(Vector3 sourcePosition, out NavMeshHit hit)
        {
            if (navMeshAgent != null)
            {
                return NavMesh.SamplePosition(sourcePosition, out hit, initialAgentRadius * sampleAreaMaxDistanceRadiusMultiplier, navMeshAgent.areaMask);
            }

            hit = default(NavMeshHit);
            return false;
        }

        public bool ChangeDestination (Vector3 destination)
        {
            if (navMeshAgent != null && navMeshAgent.enabled)
            {
                return navMeshAgent.SetDestination(destination);
            }

            return false;
        }

        public void StopMovement ()
        {
            //Debug.LogFormat("NavigationController.StopMovement: Name: {0}.", gameObject.name);

            if (navMeshAgent != null)
            {
                if (navMeshAgent.enabled)
                    navMeshAgent.ResetPath();
                navMeshAgent.avoidancePriority = initialAvoidancePriority - (onGetStoppedAvoidanceDelta != null ? onGetStoppedAvoidanceDelta.Invoke() : 0);
                if (navMeshAgent.radius != initialAgentRadius)
                    navMeshAgent.radius = initialAgentRadius;
            }

            timePassedInProximity = timePassedInStuckState /*= distanceRemainingAtStartOfDetection*/ = 0;
            enabled = false;
        }

        public void SetSpeed (float speed)
        {
            if (speed > Mathf.Epsilon)
            {
                navMeshAgent.speed = speed;
            }
        }

        public bool CalculatePath (Vector3 destination, int areaMask, ref NavMeshPath path)
        {
            if (navMeshAgent != null)
            {
                if (path == null)
                {
                    path = new NavMeshPath();
                }

                var originalAreaMask = navMeshAgent.areaMask;
                var originalState = navMeshAgent.enabled;

                if (areaMask != 0)
                    navMeshAgent.areaMask = areaMask;

                navMeshAgent.enabled = true;
                var result = navMeshAgent.CalculatePath (destination, path);

                if (areaMask != 0)
                    navMeshAgent.areaMask = originalAreaMask;
                navMeshAgent.enabled = originalState;

                return result;
            }

            return false;
        }
        #endregion

        #region Private methods.
        private void NavigateCommon (float speed, int priorityDelta, int areaMask)
        {
            if (speed > Mathf.Epsilon)
            {
                navMeshAgent.speed = speed;
            }

            navMeshAgent.avoidancePriority = initialAvoidancePriority - priorityDelta;
            initialAgentRadius = navMeshAgent.radius;
            stuckDecisionVelocitySpeed = stuckDecisionVelocityFactor * navMeshAgent.speed;
            timePassedInProximity = timePassedInStuckState /*= distanceRemainingAtStartOfDetection*/ = 0;
            enabled = true;
            navMeshAgent.enabled = true;
            if (areaMask != 0)
            {
                navMeshAgent.areaMask = areaMask;
            }
        }

        bool DetectStuckCondition()
        {
            bool stuck = false;

/*
            if (stuckDecisionVelocitySpeed > 0 &&
                stuckDecisionTime > 0 &&
                navMeshAgent.hasPath &&
                !navMeshAgent.pathPending && 
                !float.IsInfinity(navMeshAgent.remainingDistance) &&
                navMeshAgent.remainingDistance > 0)
            {
                if (distanceRemainingAtStartOfDetection == 0)
                {
                    if (navMeshAgent.velocity.magnitude <= stuckDecisionVelocitySpeed)
                        distanceRemainingAtStartOfDetection = navMeshAgent.remainingDistance;
                }
                else
                {
                    timePassedStuckDetection += Time.deltaTime;
                    if (timePassedStuckDetection >= stuckDecisionTime)
                    {
                        var distanceDelta = Mathf.Abs (distanceRemainingAtStartOfDetection - navMeshAgent.remainingDistance);
                        var realVelocity = distanceDelta / timePassedStuckDetection;
                        var realVelocitySqr = realVelocity * realVelocity;

                        if (realVelocitySqr <= stuckDecisionVelocitySqrSpeed)
                        {
                            Debug.LogFormat("Navigation agent is stuck! Name: {0}, Velocity: {1}, Real velocity: {2}."
                                , gameObject.name, navMeshAgent.velocity.magnitude, realVelocity);
                            stuck = true;
                        }

                        timePassedStuckDetection = 0;
                        distanceRemainingAtStartOfDetection = 0;
                    }
                }
            }
*/
            if (stuckDecisionVelocitySpeed > 0 &&
                stuckDecisionTime > 0 &&
                navMeshAgent.hasPath &&
                !navMeshAgent.pathPending)
            {
                if (navMeshAgent.velocity.magnitude <= stuckDecisionVelocitySpeed)
                {
                    timePassedInStuckState += Time.deltaTime;

                    if (timePassedInStuckState >= stuckDecisionTime)
                    {
//                         Debug.LogFormat("Navigation agent is stuck! Name: {0}, Velocity: {1}."
//                             , gameObject.name, navMeshAgent.velocity.magnitude);
                        stuck = true;
                    }
                }
                else
                {
                    // Knock back time but with a reducing factor (anti-jitter case where velocity jumps between small and big value).
                    timePassedInStuckState = Mathf.Max(0, timePassedInStuckState - (Time.deltaTime * stuckDecisionTimeKnobackFactor));
                }

                if (timePassedInStuckState >= stuckFixingTime)
                {   // Start reducing agent radius until stuckDecisionTime passed or agent radius is at stuckFixingMinAgentRadius.
                    var newRadius = initialAgentRadius * Math.Max(stuckFixingMinAgentRadius, (1.0f - (timePassedInStuckState - stuckFixingTime) * stuckFixingRadiusChangeRate));
                    if (newRadius != navMeshAgent.radius)
                    {
                        navMeshAgent.radius = newRadius;
                    }
                }
                else
                {
                    if (initialAgentRadius != navMeshAgent.radius)
                    {
                        navMeshAgent.radius = initialAgentRadius;
                    }
                }
            }

            return stuck;
        }
        #endregion

        #region Unity Editor API
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying && navMeshAgent != null && navMeshAgent.enabled)
            {
                var selected = UnityEditor.Selection.Contains(gameObject);

                var stringBuilder = new StringBuilder(256);

                if (selected)
                {
                    stringBuilder.AppendFormat("\nHas path: {0}, Path pending: {1}, Priority: {2}", navMeshAgent.hasPath, navMeshAgent.pathPending, navMeshAgent.avoidancePriority);

                    if (navMeshAgent.hasPath)
                    {
                        var navAgentVelocity = navMeshAgent.velocity.magnitude;

                        if (navAgentVelocity > 0)
                        {
                            stringBuilder.AppendFormat("\nVelocity: {0:N3}", navAgentVelocity);

                            EditorGizmoHelper.DrawArrow(transform.position, transform.position + navMeshAgent.velocity, Color.red, 0.25f);
                        }

                        EditorGizmoHelper.DrawArrow(transform.position, navMeshAgent.destination, Color.yellow, 0.25f);

                        var pathCornders = navMeshAgent.path.corners;

                        if (pathCornders != null && pathCornders.Length > 0)
                        {
                            for (int i = 0; i < pathCornders.Length - 1; i++)
                            {
                                EditorGizmoHelper.DrawArrow(i > 0 ? pathCornders[i] : transform.position, pathCornders[i + 1], Color.blue, 0.25f);
                            }
                        }
                    }
                    else if (navMeshAgent.pathPending)
                    {
                        EditorGizmoHelper.DrawArrow(transform.position, navMeshAgent.destination, Color.magenta, 0.25f);
                    }
                }

                if (navMeshAgent.radius != initialAgentRadius)
                {
                    stringBuilder.AppendFormat("\nRadius fixup: {0:N3}/{1:N3}", navMeshAgent.radius, initialAgentRadius);
                }

                if (stringBuilder.Length > 0)
                {
                    var worldPositionProvider = GetComponent<IWorldPositionProvider>();
                    var position = worldPositionProvider != null ? worldPositionProvider.GetWorldPosition() : transform.position;

                    UnityEditor.Handles.Label(position, stringBuilder.ToString(), EditorGizmoHelper.instance.GetOverheadLabelStyle());
                }
            }
        }
#endif
        #endregion
    }
}