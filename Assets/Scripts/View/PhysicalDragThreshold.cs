﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace View
{
    // This is a modified version of script from:
    // https://forum.unity3d.com/threads/buttons-within-scroll-rect-are-difficult-to-press-on-mobile.265682/#post-1993502

    /// <summary>
    /// Sets the drag threshold for an EventSystem as a physical size based on DPI.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Physical Drag Threshold")]
    public class PhysicalDragThreshold : MonoBehaviour
    {
        #region Private static data.
        private static readonly float inchToCm = 2.54f;
        #endregion

        [SerializeField]
        private EventSystem eventSystem = null;

        [SerializeField]
        private float dragThresholdCM = 0.5f;

        void Start ()
        {
            if (eventSystem == null)
            {
                eventSystem = GetComponent<EventSystem>();
            }
            SetDragThreshold();
        }

        private void SetDragThreshold ()
        {
            if (eventSystem != null)
            {
                eventSystem.pixelDragThreshold = Mathf.Max (eventSystem.pixelDragThreshold, (int)(dragThresholdCM * Screen.dpi / inchToCm));
            }
        }
    }
}