﻿using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public delegate bool CanEnabled_Delegate ();

    public class Enabler
    {
        public Button.ButtonClickedEvent _onClick = new Button.ButtonClickedEvent();

        public bool turnOn
        {
            set { Enable(value); }
        }

        public Button.ButtonClickedEvent onClick
        {
            get { return _onClick; }
        }

        public CanEnabled_Delegate CanEnabled_Callback;
        public CanEnabled_Delegate CanVisible_Callback = null;
        public bool visibleWhenDisabled = false;
        public System.Action onInactiveClick = null;

        public bool forceOff
        {
            get { return forceDisable; }
            set
            {
                forceDisable = value;
                Enable(!value);
            }
        }

        private GameObject go;
        private bool forceDisable;

        public Enabler (GameObject go)
        {
            this.go = go;

            var click = GetOnClick();
            if (click != null)
            {
                click.AddListener(OnClick_Intermediate);
            }

        }

        private void Enable (bool value)
        {
            if (forceOff)
            {
                // Debug.LogFormat("ButtonEnabler: force off {0} GO", go.name);
                SetActiveMode(false);
                go.SetActive(false);
                return;
            }

            bool value2 = true;

            if (CanEnabled_Callback != null)
                value2 = CanEnabled_Callback();

            if (visibleWhenDisabled)
            {
                SetActiveMode(value && value2);
            }
            else
            {
                go.SetActive(value && value2);
            }

            if (CanVisible_Callback != null)
                go.SetActive(CanVisible_Callback());
        }

        private void OnClick_Intermediate ()
        {
            var canEnable = true;
            if (CanEnabled_Callback != null)
                canEnable = CanEnabled_Callback();

            if (canEnable)
                _onClick.Invoke();
            else
            {
                if (onInactiveClick != null)
                    onInactiveClick.Invoke();
                //Debug.Log("SHOW HINT");
            }
        }

        private Button.ButtonClickedEvent GetOnClick ()
        {
            if (go != null)
            {
                var btn = go.GetComponent<Button>();
                if (btn != null)
                {
                    return btn.onClick;
                }
            }

            return null;
        }

        private void SetActiveMode (bool value)
        {
            if (go != null)
            {
                var btn = go.GetComponent<Button>();
                if (btn != null)
                {
                    var colors = btn.colors;

                    Image[] images = btn.transform.GetComponentsInChildren<Image>();

                    colors.normalColor = (value == true) ? Color.white : btn.colors.disabledColor;

                    btn.colors = colors;

                    foreach (var im in images)
                        im.color = colors.normalColor;
                }
            }

        }
    }
}