using System;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI;
using View.UI.Base;

namespace View
{
    public class FriendsListView : UIBaseViewList<FriendsListItem>
    {
        private Button inviteBtn { get { return genericView.GetComponent<Button>("inviteBtn"); } }
        private Button connectBtn { get { return genericView.GetComponent<Button>("connectBtn"); } }

        public bool inviteBtnInteractable { set { genericView.GetComponent<Selectable>("inviteBtn").interactable = value; } }
        public bool connectBtnInteractable { set { genericView.GetComponent<Selectable>("connectBtn").interactable = value; } }

        public string rewardCounTxt { set{genericView.GetComponent<Text>("rewardCount").text = value; } }
        
        protected void Awake()
        {
            itemsRoot = genericView.GetTransform("itemsRoot");
            BindCloseButton();
        }

        public void SetInviteButtonActive(bool active)
        {
            inviteBtn.gameObject.SetActive(active);
        }

        public void SetConnectButtonActive(bool active)
        {
            connectBtn.gameObject.SetActive(active);
        }

        public void AddInviteButtonCallback(UnityAction action)
        {
            inviteBtn.onClick.AddListener(action);
        }

        public void AddConnectButtonCallback(UnityAction action)
        {
            connectBtn.onClick.AddListener(action);
        }

        public void SetScrollEnabled(bool isEnabled)
        {
            genericView.GetComponent<ScrollRect>("contentScrollView").vertical = isEnabled;
        }

        public void UseConnectWithBonusText ()
        {
            genericView.SetActive("connectWithBonusContent", true);
            genericView.SetActive("connectContent", false);
        }

        public void UseConnectText ()
        {
            genericView.SetActive("connectWithBonusContent", false);
            genericView.SetActive("connectContent", true);
        }
    }
}