using UnityEngine;
using View.UI;
using View.UI.Base;

namespace View
{
    public class FriendsListItem : UIBaseViewListItem
    {
        public string FriendName
        {
            set
            {
                genericView.SetText("nameLabel",value);
            }
        }

        public Sprite FriendAvatar
        {
            set
            {
                genericView.SetSprite("avatarImage", value); 
            }
        }

        public int FriendLevel
        {
            set
            {
                genericView.SetText("levelLabel", value.ToString());
            }
        }

        public void SetupAddFriends()
        {
            Clear();
            genericView.SetActive("addFriend", true);
        }

        public void SetupFriendView()
        {
            Clear();
            genericView.SetActive("avatarImage", true);
            genericView.SetActive("nameLabel", true);
        }

        public bool IsAddFriend()
        {
            return genericView.GetTransform("addFriend").gameObject.activeSelf;
        }

        void Clear()
        {
            genericView.SetActive("avatarImage", false);
            genericView.SetActive("nameLabel", false);
            genericView.SetActive("addFriend", false);
        }
    }
}