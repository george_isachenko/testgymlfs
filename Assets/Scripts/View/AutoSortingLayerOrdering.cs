﻿using UnityEngine;
using UnityToolbag;

namespace View
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Auto Sorting Layer Ordering")]
    public class AutoSortingLayerOrdering : MonoBehaviour
    {
        [SortingLayer]
        public int sortinglLayer;
        public Renderer[] orderedRenderers;

        #region Unity API.
        void OnEnable ()
        {
            SetOrdering();
        }

        void LateUpdate ()
        {
            SetOrdering();
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void OnValidate()
        {
            // Call this only in editor, only when not playing and only for objects instantiated in scene (not prefabs).
            if (Application.isEditor && !Application.isPlaying && gameObject.scene.IsValid() && gameObject.scene.isLoaded)
            {   
                SetOrdering();
            }
        }
#endif
        #endregion

        #region Private methods.
        private void SetOrdering ()
        {
            if (orderedRenderers != null && orderedRenderers.Length > 0)
            {
                var instanceID = Mathf.Abs(gameObject.GetInstanceID());

                for (int i = 0; i < orderedRenderers.Length; i++)
                {
                    var renderer = orderedRenderers[i];
                    renderer.sortingLayerID = sortinglLayer;
                    renderer.sortingOrder = instanceID * orderedRenderers.Length + i;
                }
            }
        }
        #endregion
    }
}