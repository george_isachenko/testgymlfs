﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using Data.PlayerProfile;
using UnityEngine.UI;
using System;
using Data;
using Core;
using Data.JuiceBar;

namespace View.UI
{
    public delegate int GetOrderSkipCost_d(int seconds);

    [RequireComponent(typeof(UIGenericView))]
    public class MakeJuiceView : UIBaseView 
    {
//      [HideInInspector] public List<ResourceType>               availableJuices { set { SetAvailableJuices(value); } }
        [HideInInspector] public List<JuiceOrder>                 orders          { set { SetOrders(value); } }
        [HideInInspector] public List<MakeJuiceOrderItemView>     orderViews      = new List<MakeJuiceOrderItemView>();
        [HideInInspector] public int                              orderSlotsAvailable = 0;
        [HideInInspector] public int                              orderSlotUnlockPrice = 0;

        [HideInInspector] public Action UpdateOrdersCallback;
        [HideInInspector] public JuiceRecipeDetailsView  recipeDetails;

        public GetOrderSkipCost_d getOrderSkipCost = null;
        public List<JuiceRecipeItemView>   recipes = new List<JuiceRecipeItemView>();
        public Func<string> storageInfoGetter;
        public Func<float> storageCapacityGetter;
        public Func<bool> storageButtonEnableGetter;

        public Action onStorageClick { set { genericView.SetButtonCallback("btnStorageHud", value); } }


        CanvasGroup     ordersCanvas;
        Image flyJuice;

        #region UnityAPI
        void Awake()
        {
            BindCloseButton();

            ordersCanvas = genericView.GetComponent<CanvasGroup>("orders");

            // Init Recipes
            var recipe1 = genericView.GetSubView("recipe1");
            recipes.Add(recipe1.GetComponent<JuiceRecipeItemView>());

            for(int i = 2; i <= 8; i++)
            {
                var newRecipe = Instantiate(recipe1.gameObject, recipe1.transform.parent);
                newRecipe.name = "Recipe" + i.ToString();
                var recipeView = newRecipe.GetComponent<JuiceRecipeItemView>();
                recipeView.genericView.alias = "recipe" + i.ToString();
                recipes.Add(recipeView);
            }

            // Init Orders
            var order1 = genericView.GetSubView("order1");
            orderViews.Add( order1.GetComponent<MakeJuiceOrderItemView>() );

            for(int i = 1; i < 6; i++)
            {
                var newOrder = Instantiate(order1.gameObject, order1.transform.parent);
                var orderView = newOrder.GetComponent<MakeJuiceOrderItemView>();
                orderView.idx = i;
                orderViews.Add(orderView);
            }
                
            recipeDetails = GetComponentInChildren<JuiceRecipeDetailsView>();
            recipeDetails.turnOn = true;

            flyJuice = genericView.GetComponent<Image>("flyJuice");
        }

        new void Start()
        {
            base.Start();
            recipeDetails.turnOn = false;
        }

        void Update()
        {
            if (orderViews[0].icon.gameObject.activeSelf)
            {
                if (UpdateOrdersCallback != null)
                {
                    UpdateOrdersCallback.Invoke();
                }
                else
                {
                    Debug.LogError("MakeJuiceView UpdateOrdersCallback == NULL");
                }
            }

        }
        #endregion

        #region PrivateAPI
        void OnDrop()
        {
            ordersCanvas.blocksRaycasts = true;
        }

        void SetOrders(List<JuiceOrder> list)
        {
            var count = Math.Min(list.Count, orderSlotsAvailable);

            for (int i = 0; i < orderViews.Count; i++)
            {
                orderViews[i].btnUnlock.gameObject.SetActive(i >= orderSlotsAvailable);
                orderViews[i].gameObject.SetActive(i <= orderSlotsAvailable);
                orderViews[i].txtUnlockPrice.text = orderSlotUnlockPrice.ToString();
                //orderViews[i].orderArrowEnable = false;
            }

            for (int i = 0; i < orderViews.Count; i++)
            {
                var orderView = orderViews[i];
                orderView.icon.gameObject.SetActive(i < count);
                orderView.btn.gameObject.SetActive(i < count);

                orderView.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

                if (i < count)
                {
                    var order = list[i];
                    var res = list[i].id;

                    orderView.icon.sprite = GUICollections.instance.storageIcons.items[(int)res].sprite;

                    orderView.btnSkip.gameObject.SetActive(order.time.secondsToFinish > 0);

                    if (order.time.secondsToFinish > 0)
                    {
                        if (i > 0)
                        {
                            orderView.txtTimer = string.Empty;
                            orderView.btnSkip.gameObject.SetActive(false);
                            orderView.btn.gameObject.SetActive(false);
                        }
                        else
                        {
                            orderView.txtTimer = order.time.printHelper;
                            orderView.txtSkipCost = getOrderSkipCost != null ? getOrderSkipCost((int)order.time.secondsToFinish) : 1;
                            orderView.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
                        }
                    }
                    else
                    {
                        orderView.txtTimer = Loc.Get("idReady");
                    }
                }
                else
                {
                    orderView.txtTimer = "";
                    orderView.btnSkip.gameObject.SetActive(false);
                }
            }
        }
        #endregion

        #region ProtectedAPI
        protected override void OnShow()
        {
            if (OnUpdateContentOnShow_Callback != null)
                OnUpdateContentOnShow_Callback.Invoke();
            if (OnShow_Callback != null)
                OnShow_Callback.Invoke();
            if (OnShowHandler != null)
                OnShowHandler.Invoke();

            genericView.SetActive("flyJuice", false);
            genericView.SetText("storageText", storageInfoGetter());
            genericView.GetComponent<Button>("btnStorageHud").enabled = storageButtonEnableGetter();
            genericView.SetSliderValue("storageBar", storageCapacityGetter());
        }
        #endregion

        #region PublicAPI
        public int GetIndexOfDraggable(GameObject obj)
        {
            for( int i = 0; i < recipes.Count; i++)
            {
                if (recipes[i].imgObj == obj)
                    return i;
            }

            return -1;
        }

        public void SubscribeOnDrop(Action val)
        {
            var drop = GetComponentInChildren<JuiceDropHandler>();
            drop.OnDropCallback = val;
        }

        public void SubscribeOnDrag(Action begin, Action end)
        {
            var items = GetComponentsInChildren<JuiceDragHandler>();

            foreach(var drag in items)
            {
                drag.OnDragBeginCallback = begin;
                drag.OnDragEndCallback = end;
            }
        }

        public void OnDragBegin(int index)
        {
            recipeDetails.turnOn = true;
            recipeDetails.transform.SetParent(JuiceDragHandler.itemBeingDragged.transform, false);
            recipeDetails.transform.localPosition = Vector3.zero;

            if (index >= 0 && !genericView.GetActive("EmptyFrame"))
            {
                genericView.SetActive("EmptyFrame", true);
                genericView.GetTransform("EmptyFrame").SetSiblingIndex(index);

                JuiceDragHandler.itemBeingDragged.transform.parent.SetParent(genericView.GetTransform("Recepies"), true);
            }

            ordersCanvas.blocksRaycasts = false;

            //UpdateArrows();
        }

        public void OnDragEnd()
        {
            recipeDetails.turnOn = false;

            genericView.SetActive("EmptyFrame", false);

            if (JuiceDragHandler.itemDraggedIndex >= 0 && JuiceDragHandler.itemBeingDragged != null)
            {
                JuiceDragHandler.itemBeingDragged.transform.parent.SetParent(genericView.GetTransform("itemsHolder"), true);
                JuiceDragHandler.itemBeingDragged.transform.parent.SetSiblingIndex(JuiceDragHandler.itemDraggedIndex);
            }

            JuiceDragHandler.itemBeingDragged = null;

            ordersCanvas.blocksRaycasts = true;

            //UpdateArrows();
        }

        public void UpdateArrows(List<JuiceOrder> list)
        {
            var count = Math.Min(list.Count, orderSlotsAvailable);
            bool arrowWasShow = false;

            for (int i = 0; i < orderViews.Count; i++)
            {
                if (i >= count && i < orderSlotsAvailable && !arrowWasShow && JuiceDragHandler.itemBeingDragged != null)
                {
                    orderViews[i].orderArrowEnable = true;
                    arrowWasShow = true;
                }
                else
                {
                    orderViews[i].orderArrowEnable = false;
                }
            }
        }

        public JuiceDragHandler GetDragHandler(int index)
        {
            return recipes[index].dragHandler;
        }

        public void OnClaimJuice(int index)
        {
            flyJuice.sprite = orderViews[index].icon.sprite;

            var go = flyJuice.gameObject;
            go.SetActive(true);
            go.transform.position = orderViews[index].transform.position;
            Transform target = genericView.GetTransform("btnStorageHud");

            LeanTween.move(go, target, 1).setEase(LeanTweenType.easeInCubic).setOnComplete(()=> { go.SetActive(false); genericView.SetText("storageText", storageInfoGetter()); });
        }

        public void UpdateStorageInfo()
        {
            genericView.SetText("storageText", storageInfoGetter());
            genericView.SetSliderValue("storageBar", storageCapacityGetter());
        }
        #endregion

    }
}