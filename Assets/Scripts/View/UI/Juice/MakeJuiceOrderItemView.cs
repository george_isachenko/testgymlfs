﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using Data.PlayerProfile;
using UnityEngine.UI;
using System;
using Data;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class MakeJuiceOrderItemView : UIGenericMonoBehaviour 
    {
        [HideInInspector] public int            idx;
        [HideInInspector] public Action<int>    OnClickCallback;
        [HideInInspector] public Action         OnClickUnlockCallback;
        [HideInInspector] public Image          icon;
        [HideInInspector] public Button         btn;
        [HideInInspector] public Text           btnText;
        [HideInInspector] public Button         btnUnlock;
        [HideInInspector] public Text           txtUnlockPrice;
        [HideInInspector] public string         txtTimer        { set { genericView.SetText("txtTimer", value); } }
        [HideInInspector] public Button         btnSkip;
        [HideInInspector] public int            txtSkipCost     { set { SetSkipCost(value); } }
        [HideInInspector] public bool           orderArrowEnable{ set { genericView.SetActive("orderArrow", value); } }

        void Awake()
        {
            icon        = genericView.GetComponent<Image>("icon");
            btn         = genericView.GetComponent<Button>("btn");
            btnText     = btn.gameObject.GetComponentInChildren<Text>();
            btnUnlock   = genericView.GetComponent<Button>("btnUnlock");
            btnSkip     = genericView.GetComponent<Button>("btnSkip");
            txtUnlockPrice = genericView.GetComponent<Text>("txtUnlockPrice");

            genericView.SetButtonCallback("btn", OnBtnClick);
            genericView.SetButtonCallback("btnUnlock", OnBtnUnlockClick);
            genericView.SetButtonCallback("btnSkip", OnBtnClick);

            orderArrowEnable = false;
        }

        void OnBtnClick()
        {
            if (OnClickCallback != null)
                OnClickCallback.Invoke(idx);
            else
                Debug.LogError("MakeJuiceOrderItemView OnClickCallback == NULL");
        }

        void OnBtnUnlockClick()
        {
            if (OnClickUnlockCallback != null)
                OnClickUnlockCallback.Invoke();
            else
                Debug.LogError("MakeJuiceOrderItemView OnClickUnlockCallback == NULL");
        }

        void SetSkipCost(int cost)
        {
            if (cost > 0)
            {
                genericView.SetActive("bucksIcon", true);
                genericView.SetText("txtSkipCost", cost.ToString()); 
            }
            else
            {
                genericView.SetActive("bucksIcon", false);
                genericView.SetText("txtSkipCost", "FREE"); 
            }
        }
    }
}