﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace View.UI
{
    public class JuiceDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler  
    {
        public static GameObject    itemBeingDragged;
        public static int           itemDraggedIndex;
        Vector3                     startPosition;
        CanvasGroup                 canvasGroup;

        Camera                      cam;

        public Action               OnDragBeginCallback;
        public Action               OnDragEndCallback;

        void Start()
        {
            canvasGroup = GetComponent<CanvasGroup>();

            cam = GUIRoot.instance.GetComponentInChildren<Camera>();
        }

        public void OnBeginDrag (PointerEventData eventData)
        {
            if (itemBeingDragged != null)
                return;

            itemBeingDragged = gameObject;
            startPosition = transform.position;

            canvasGroup.blocksRaycasts = false;

            if (OnDragBeginCallback != null)
                OnDragBeginCallback.Invoke();
        }

        public void OnDrag (PointerEventData eventData)
        {
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent as RectTransform, Input.mousePosition, cam, out localPoint);

            transform.localPosition = localPoint;
        }

        public void OnEndDrag (PointerEventData eventData)
        {
            canvasGroup.blocksRaycasts = true;
            //itemBeingDragged = null;
            transform.position = startPosition;
            transform.localPosition = Vector3.zero;

            if (OnDragEndCallback != null)
                OnDragEndCallback.Invoke();
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            OnBeginDrag(eventData);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            OnEndDrag(eventData);
        }
    }
}