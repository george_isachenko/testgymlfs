﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using UnityEngine.UI;

namespace View.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UIGenericView))]
    public class JuiceRecipeItemView : UIGenericMonoBehaviour 
    {
        public Sprite   icon          {   set { image.sprite = value; } }
        public bool     attention     {   set { genericView.SetActive("attention",value); } }

        public GameObject           imgObj        { get { return image.gameObject; } }
        public JuiceDragHandler     dragHandler;

        Image      image;

        void Awake()
        {
            image =  genericView.GetComponent<Image>("icon");
            dragHandler = genericView.GetComponent<JuiceDragHandler>("icon");
        }
    	
    }
}