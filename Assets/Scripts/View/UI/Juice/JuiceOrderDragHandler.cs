﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace View.UI
{
    public class JuiceOrderDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
    {
        Vector3                     startPosition;
        // CanvasGroup                 canvasGroup;

        public Action               OnDragBeginCallback;
        public Action               OnDragEndCallback;

        [HideInInspector] public GameObject           objectToDrag;

        void Start()
        {
            // canvasGroup = GetComponent<CanvasGroup>();
        }
        

        public void OnBeginDrag (PointerEventData eventData)
        {
            startPosition = transform.position;

            if (OnDragBeginCallback != null)
                OnDragBeginCallback.Invoke();

            if (objectToDrag != null)
                objectToDrag.SetActive(true);
            else
                Debug.LogError("objectToDrag == NULL");
                
        }

        public void OnDrag (PointerEventData eventData)
        {            
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent as RectTransform, eventData.position, OverheadUICam.cam, out localPoint);

            if (objectToDrag != null)
                objectToDrag.transform.localPosition = localPoint;
            else
                Debug.LogError("objectToDrag == NULL");
            
        }

        public void OnEndDrag (PointerEventData eventData)
        {
            transform.position = startPosition;

            if (OnDragEndCallback != null)
                OnDragEndCallback.Invoke();

            if (objectToDrag != null)
                objectToDrag.SetActive(false);
            else
                Debug.LogError("objectToDrag == NULL");
        }
    }
}