﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using Data.PlayerProfile;
using UnityEngine.UI;
using Data.JuiceBar;
using System;

namespace View.UI
{
    public class JuiceRecipeDetailsView : UIGenericMonoBehaviour 
    {
        public Func<ResourceType, int>  ResourceCount; 
        public JuiceRecipesTableData    recipe      { set { SetRecipe(value); } }
        public bool                     turnOn      { set { this.gameObject.SetActive(value); } }
        public int                      inStorage   { set { genericView.SetText("txtInStorage", value.ToString()); } }
        public string                   juiceName   { set { genericView.SetText("txtJuiceName", value); } }
        public string                   time        { set { genericView.SetText("txtTime", value); } }

        Image[] ingredients = new Image[4];
        Text[]  counters    = new Text[4];

        void Awake()
        {
            var ingredient1 = transform.Find("bg").Find("Ingredient").gameObject;

            ingredients[0] = ingredient1.transform.Find("Icon").GetComponent<Image>();
            counters[0] = ingredient1.GetComponentInChildren<Text>();

            for (int i = 1; i < 4; i++)
            {
                var newIngredient = Instantiate(ingredient1.gameObject, ingredient1.transform.parent);

                ingredients[i] = newIngredient.transform.Find("Icon").GetComponent<Image>();
                counters[i] = newIngredient.GetComponentInChildren<Text>();
            }
        }

        void SetRecipe(JuiceRecipesTableData data)
        {
            SetIngredient(0, data.ingredient_1, data.ingredient_1_count);
            SetIngredient(1, data.ingredient_2, data.ingredient_2_count);
            SetIngredient(2, data.ingredient_3, data.ingredient_3_count);
            SetIngredient(3, data.ingredient_4, data.ingredient_4_count);
        }

        void SetIngredient(int idx, ResourceType res, int count)
        {
            //Debug.LogError("idx " + idx.ToString() + " res = " + res.ToString() + " count = " + count.ToString());

            var isVisible = !(res == ResourceType.None || count <= 0);

            ingredients[idx].transform.parent.gameObject.SetActive(isVisible);

            if (isVisible)
            {
                if (ResourceCount != null)
                {
                    ingredients[idx].sprite = GUICollections.instance.storageIcons.items[(int)res].sprite;

                    var inStorage = ResourceCount(res);

                    string strColor = inStorage >= count ? "<color=#A7E500FF>" : "<color=#FF8C8DFF>";
                    counters[idx].text = strColor + count.ToString() + "/" + inStorage.ToString() + "</color>";
                    //counters[idx].transform.parent.gameObject.SetActive(count > 1);
                }
                else
                {
                    Debug.LogError("JuiceRecipeDetailsView ResourceCount == NULL");
                }
            }
               
        }
    }
}