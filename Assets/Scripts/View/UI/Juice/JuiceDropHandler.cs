﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace View.UI
{
    public class JuiceDropHandler : MonoBehaviour, IDropHandler 
    {
        public Action   OnDropCallback;

        public bool     isOn        { set { gameObject.SetActive(value); } }
       
        public void OnDrop (PointerEventData eventData)
        {
            if (OnDropCallback != null)
                OnDropCallback.Invoke();
        }
    }
}
