﻿using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class RateUsView : UIBaseView
    {
        public Button btnMailTo;
        public Button btnRateUs;

        void Awake()
        {
            BindCloseButton();

            btnMailTo = transform.Find("BtnSendEmail").GetComponent<Button>();
            btnRateUs = transform.Find("BtnRate").GetComponent<Button>();
        }

    }
}