﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace View.UI {
    public class IncDecControl : MonoBehaviour {

        public Action   OnIncAction;
        public Action   OnDecAction;

        private Text _txt;
        public string text { set { _txt.text = value; } }

        void Awake(){
            _txt = transform.Find("Txt").GetComponent<Text>();

            var btnDec = transform.Find("BtnDec").GetComponent<Button>();
            var btnInc = transform.Find("BtnInc").GetComponent<Button>();

            btnDec.onClick.AddListener(OnDec);
            btnInc.onClick.AddListener(OnInc);
        }

        void OnInc(){
            if (OnIncAction != null)
                OnIncAction.Invoke();
        }

        void OnDec(){
            if (OnDecAction != null)
                OnDecAction.Invoke();
        }
    }
}
