﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using View.UI.Base;
using UI.HUD;

namespace View.UI
{
    public class DailyBonusView :  UIBaseViewList<DailyBonusViewItem>
    {

        //List<DayilyBonusDayViewItem> days = new List<DayilyBonusDayViewItem>();
        //Transform daysParent;
        //Image   imgTopRewardIcon;
        //Text    txtTopRewardCount;

        // public properties
        //public int          currentDay  { set { SetCurrentDay(value); } }
        //public Data.Cost    mandatoryReward   { set { SetTopReward(value); } }

        public GameObject dayViewItemPrefab;
        public bool         btnCloseOn  { set { genericView.SetActive("btnClose", value); } }
        public ActiveText timer { get { return genericView.GetComponent<ActiveText>("timer"); } }

        void Awake()
        {
            itemsRoot = DefaultItemsRoot();
            BindCloseButton();

            //var topReward = transform.FindChild("TopReward");
            //imgTopRewardIcon = topReward.FindChild("ImgIcon").GetComponent<Image>();
            //txtTopRewardCount = topReward.FindChild("TxtCount").GetComponent<Text>();

            //daysParent = transform.FindChild("Days");

            //foreach (Transform child in daysParent) {
            //    GameObject.Destroy(child.gameObject);
            //}

            /*
            for (int i = 0; i < 5; i++)
            {
                var item = NewDayItem();
                item.dayIdx = i + 1;
            }
            */
        }

        /*
        DayilyBonusDayViewItem NewDayItem()
        {
            var viewItem = InstantiatePrefab<DayilyBonusDayViewItem>(dayViewItemPrefab, daysParent, "day_" + days.Count);

            days.Add(viewItem);
            return viewItem;
        }
        */

        /*
        void SetCurrentDay(int dayIdx)
        {
            for (int i = 0; i < 5; i++)
            {
                var day = days[i];

                if ( i < dayIdx)
                    day.type = DayilyBonusDayViewItem.DayType.PastDay;
                else if (i == dayIdx)
                    day.type = DayilyBonusDayViewItem.DayType.ToDay;
                else
                    day.type = DayilyBonusDayViewItem.DayType.NextDay;
            }
        }
        */

        /*
        void SetTopReward(Data.Cost reward)
        {
            if (reward != null)
            {
                imgTopRewardIcon.sprite = reward.sprite;
                txtTopRewardCount.text = reward.value.ToString();
            }
            else
            {
                imgTopRewardIcon.sprite = null;
                txtTopRewardCount.text = "";

                Debug.LogError("DailyBonusView : top reward is null");
            }
        }
        */
    }
}