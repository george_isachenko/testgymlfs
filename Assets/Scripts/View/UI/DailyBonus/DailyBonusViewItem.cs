﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Data;
using View.UI.Base;

namespace View.UI
{
    public class DailyBonusViewItem : UIBaseViewListItem
    {
        /*
        Image       imgBox;
        Image       imgItem;
        Text        txt;
        Text        txtUnlockPrice;
        GameObject  objUnlockPrice;
        */

        //public bool     closed      { set { SetClosed(value); } }
        //public bool     paid        { set { SetPaid(value); } }
        //public Cost     unlockPrice { set { SetUnlockPrice(value);  } }

        public bool     claimed     { set { genericView.SetActive("collected", value); genericView.SetActive("ok", value); genericView.SetActive("bonusIcon", !value); } }
        public Sprite   sprite      { set { genericView.GetComponent<Image>("bonusIcon").sprite = value; } }
        public int      count       { set { genericView.SetText("bonusCount" ,value.ToString()); } }
        public bool     superBonus  { set { genericView.SetActive("superBonus", value); } }
        public bool     current     { set { genericView.SetActive("current", value); } }
        public bool     gray        { set { genericView.SetActive("gray", value); } }
        public int      dayNumber   { set { SetDayNumber(value); } }

        new void Awake()
        {
            base.Awake();

            /*
            imgBox = transform.FindChild("ImgBox").GetComponent<Image>();
            imgItem = transform.FindChild("ImgItem").GetComponent<Image>();
            txt = transform.FindChild("Text").GetComponent<Text>();
            objUnlockPrice = transform.FindChild("UnlockPrice").gameObject;
            txtUnlockPrice = objUnlockPrice.transform.FindChild("Text").GetComponent<Text>();
            */
        }

        /*    
        private void SetClosed(bool value)
        {
            
            imgBox.gameObject.SetActive(value); 
            imgItem.gameObject.SetActive(!value);
            txt.gameObject.SetActive(!value);
            
        }
        */

        /*
        private void SetPaid(bool value)
        {   
            //objUnlockPrice.SetActive(value);
        }
        */

        /*
        private void SetUnlockPrice(Cost value)
        {
           
            if (value.type == Cost.CostType.BucksOnly)
            {
                txtUnlockPrice.text = value.value.ToString();
            }
            else
            {
                Debug.LogError("DailyBonusViewItem price not in bucks");
            }
            
        }
        */

        private void SetDayNumber(int day)
        {
            string strDay = Loc.Get("dailyBonusDay");
            genericView.SetText("grayDayCount", strDay + " " + day.ToString());
            genericView.SetText("superDayCount", strDay + " " + day.ToString());
            genericView.SetText("collectedDay", strDay + " " + day.ToString());
        }

    }
}