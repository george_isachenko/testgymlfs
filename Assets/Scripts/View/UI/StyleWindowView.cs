using System;
using Data;
using View.UI.Base;

namespace View.UI
{
    public class StyleWindowView : UIBaseView
    {
        public event Action onGymExpandPress;
        public event Action onBuyDecorPress;


        private void Awake()
        {
            BindCloseButton();
            genericView.SetButtonCallback("gymExpandBtn", ()=> { if (onGymExpandPress != null) onGymExpandPress(); });
            genericView.SetButtonCallback("buyDecorBtn", ()=> { if (onBuyDecorPress != null) onBuyDecorPress(); });
        }

        public void SetStyleLevelData(int level, int currentStylePoints, int maxStylePoints)
        {
            genericView.SetText("styleLevel", level.ToString());
            genericView.SetSliderValue("styleSlider", currentStylePoints*1f/maxStylePoints);
            genericView.SetText("styleValue",string.Format("{0}/{1}", currentStylePoints, maxStylePoints));
        }

        public void SetStyleExpandData(int currentGymSize, int gymSizeStylePoints)
        {
            genericView.SetText("currentGymSize", Loc.Get("styleWindowCurrentGymSize", currentGymSize));
            genericView.SetText("gymSizeStylePoints", gymSizeStylePoints.ToString());
        }

        public void SetDecorStylePoints(int currentDecorsCount, int decorStylePoints)
        {
            genericView.SetText("currentDecorsCount", Loc.Get("styleWindowCurrentDecors", currentDecorsCount));
            genericView.SetText("decorStylePoints", decorStylePoints.ToString());
        }
    }
}