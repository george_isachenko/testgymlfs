﻿using System;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class HQView : UIBaseView
    {
        public Action onAchievementsClick   { set { genericView.SetButtonCallback("btnAchievements", value); } }
        public Action onStaffClick          { set { genericView.SetButtonCallback("btnStaff", value); } }
        public Action onLeaderboardsClick   { set { genericView.SetButtonCallback("btnLeaderboards", value); } }

        public GameObject trainersButton    { get { return genericView.GetGameObject("btnStaff"); } }

        public string trainersCaption       { set { genericView.SetText("staffText", value); } }
        public string trainersLockCaption   { set { genericView.SetText("trainersLockText", value); } }
        public bool trainersLock            { set { genericView.SetActive("staffText", !value);
                                                    genericView.SetActive("trainersLocker", value);
                                                    trainersButton.GetComponent<Button>().enabled = !value; } }
        public bool trainersNew             { set { genericView.SetActive("tarinersNew", value); } }

        void Awake()
        {
            BindCloseButton();
        }
    }
}