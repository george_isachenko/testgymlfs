﻿using System;
using InspectorHelpers;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    public class ModalConfirmWindow : UIGenericMonoBehaviour
    {
        #region Types.
        public enum IconType
        {
            Default = 0,
            NetworkConnection
        }
        #endregion

        #region Public fields.
        [ReorderableList(LabelsEnumType = typeof(IconType))]
        public Sprite[] icons;
        #endregion

        #region Private properties.
        private bool   btnOkOn          { set { genericView.SetActive("btnConfirm", value); } }
        private bool   btnCancelOn      { set { genericView.SetActive("btnCancel", value); } }
        private string btnConfirmText   { set { genericView.SetText("btnConfirmText", value); } }
        private string btnCancelText    { set { genericView.SetText("btnCancelText", value); } }
        private string header           { set { genericView.SetText("header", value); } }
        private string description      { set { genericView.SetText("description", value); } }
        private Sprite spriteIcon       { set { genericView.SetSprite("icon", value); } }
        #endregion

        #region Private fields.
        private Action onCancel;  
        private Action onConfirm;
        #endregion

        #region Unity API.
        private void Awake()
        {
            genericView.SetButtonCallback("btnCancel", OnCancelClick);
            genericView.SetButtonCallback("btnConfirm", OnConfirmClick);
        }
        #endregion
            
        #region Public API.
        public void Show
            ( string header
            , string text
            , Action onConfirm
            , string okButtonText = "OK"
            , IconType iconType = IconType.Default)
        {
            DisableAll();

            //genericView.SetActive("genericIcon");

            this.header = header;
            this.description = text;

            this.onConfirm = onConfirm;
            btnOkOn = true;
            btnConfirmText = (okButtonText != null) ? okButtonText : "OK";

            btnCancelOn = false; 

            SetIcon(iconType);

            Show();
        }

        public void Show
            ( string header
            , string text
            , Action onConfirm
            , string okButtonText = "OK"
            , Action onCancel = null
            , string cancelButtonText = "Cancel"
            , IconType iconType = IconType.Default)
        {
            DisableAll();

            //genericView.SetActive("genericIcon");

            this.header = header;
            this.description = text;

            this.onConfirm = onConfirm;
            btnOkOn = true;
            btnConfirmText = (okButtonText != null) ? okButtonText : "OK";

            if (onCancel != null && cancelButtonText != null)
            {
                this.onCancel = onCancel;
                btnCancelOn = true;
                btnCancelText = cancelButtonText;
            }
            else
            {
                btnCancelOn = false; 
            }

            SetIcon(iconType);

            Show();
        }

        public void Hide()
        {
            gameObject.SetActive(false);            

            onCancel = null;
            onConfirm = null;
        }

        public void OnCancelClick() // Public, so it can be bound in Inspector, or force-invoked from code.
        {
            var tmpCancel = onCancel;
            onCancel = null;
            onConfirm = null;

            Hide();

            tmpCancel?.Invoke();
        }
            
        public void OnConfirmClick() // Public, so it can be bound in Inspector, or force-invoked from code.
        {
            var tmpConfirm = onConfirm;
            onCancel = null;
            onConfirm = null;

            Hide();

            tmpConfirm?.Invoke();
        }
        #endregion

        #region Private functions.
        void DisableAll()
        {
/*
            for (int i = 0; i < transform.childCount; ++i)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
*/

            btnConfirmText = string.Empty;
            btnCancelOn = false;

            header = string.Empty;
            description = string.Empty;

            genericView.SetActive("header");
            genericView.SetActive("description");

            onConfirm = null;
            onCancel = null;
        }

        void Show()
        {
            gameObject.SetActive(true);
            transform.parent.gameObject.SetActive(true);
        }

        void SetIcon (IconType iconType)
        {
            var idx = (int)iconType;

            if (icons != null && idx < icons.Length && icons[idx] != null)
            {
                spriteIcon = icons[idx];
            }
        }
        #endregion
   }
}