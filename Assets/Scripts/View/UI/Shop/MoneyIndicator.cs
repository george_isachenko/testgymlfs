﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Data;

public class MoneyIndicator : MonoBehaviour {

    Text txt;
    Image imgBucks;
    Image imgCoins;

    public Cost                price       { set { SetPrice(value); } }
    public Cost.CostType  priceType   { set { SetPriceType(value); } }
    public int                      priceInt    { set { SetPriceInt(value); } }

    void Awake()
    {
        txt         = transform.Find("Text").GetComponent<Text>();
        imgBucks    = transform.Find("ImgBucks").GetComponent<Image>();
        imgCoins    = transform.Find("ImgCoins").GetComponent<Image>();
    }

    private void SetPrice(Cost price)
    {
        SetPriceType(price.type);
        txt.text = price.valueString;
    }

    private void SetPriceInt(int price)
    {
        if (price == 0)
            txt.text = "0";
        else
            txt.text =  price.ToString("## ### ###").Trim();
    }

    private void SetPriceType(Cost.CostType type)
    {
        imgCoins.gameObject.SetActive(type == Cost.CostType.CoinsOnly);
        imgBucks.gameObject.SetActive(type == Cost.CostType.BucksOnly);
    }
	
	
}
