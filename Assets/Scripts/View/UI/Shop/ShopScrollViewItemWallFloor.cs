﻿using UnityEngine;
using UnityEngine.UI;

namespace View.UI.Shop
{
    [AddComponentMenu("Kingdom/View/UI/Shop/Shop Scroll View Item Wall Floor")]
    public class ShopScrollViewItemWallFloor : MonoBehaviour
    {
        public void SetId(int id)
        {
            var sprites = GUICollections.instance.wallFloorTextures.array[id];
            SetSprites(sprites.wall, sprites.floor);
        }
        public void SetSprites(Sprite wallSprite, Sprite floorSprite)
        {
            // hide all
            wall.visible = floor.visible = wallFloor.visible = false;

            if (wallSprite != null && floorSprite != null)
            {
                wallFloor.visible = true;
                wallFloor.imgWallIconSq.sprite = wallSprite;
                wallFloor.imgWallIconTall.sprite = wallSprite;

                bool isSquare = wallSprite.rect.width == wallSprite.rect.height;

                wallFloor.imgWallIconTall.gameObject.SetActive(!isSquare);
                wallFloor.imgWallIconSq.gameObject.SetActive(isSquare);

                wallFloor.imgFloorIcon.sprite = floorSprite;
            }
            else if (wallSprite != null)
            {
                wall.visible = true;
                wall.imgIcon.sprite = wallSprite;
            }
            else if (floorSprite != null)
            {
                floor.visible = true;
                floor.imgIcon.sprite = floorSprite;
            }
        }

        public struct FloorOrWallItem
        {
            private GameObject root;
            public Image imgIcon;

            public void SetRoot(Transform _root)
            {
                root = _root.gameObject;
                imgIcon = _root.Find("ImgIcon").GetComponent<Image>();
            }

            public bool visible
            {
                set
                {
                    root.gameObject.SetActive(value);
                }
            }
        }

        public struct WallFloorItem
        {
            private GameObject root;
            public Image imgWallIconSq;
            public Image imgWallIconTall;
            public Image imgFloorIcon;

            public void SetRoot(Transform _root)
            {
                root = _root.gameObject;
                imgFloorIcon = _root.Find("ImgFloorIcon").GetComponent<Image>();
                imgWallIconTall = _root.Find("WallClipper").Find("ImgWallIconTall").GetComponent<Image>();
                imgWallIconSq = _root.Find("ImgWallIconSq").GetComponent<Image>();
            }

            public bool visible
            {
                set
                {
                    root.gameObject.SetActive(value);
                }
            }
        }

        private FloorOrWallItem floor;
        private FloorOrWallItem wall;
        private WallFloorItem wallFloor;

        void Awake()
        {
            floor.SetRoot(transform.Find("Floor_Item"));
            wall.SetRoot(transform.Find("Wall_Item"));
            wallFloor.SetRoot(transform.Find("WallFloor_Item"));
        }

        public void Show(bool show = true)
        {
            gameObject.SetActive(show);
        }
    }

}