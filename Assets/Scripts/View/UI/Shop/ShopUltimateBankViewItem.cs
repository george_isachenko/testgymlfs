﻿using UnityEngine;
using System.Collections;
using Data;
using View.UI.Shop;
using System;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class ShopUltimateBankViewItem : UIBaseViewListItem 
    {

        public Cost     cost        { set { SetCost(value); } }
        public Sprite   icon        { set { genericView.SetSprite("icon", value); } }
        public string   priceReal   { set { genericView.SetText("priceReal", value); } }

        new void Awake()
        {
            buyButtonAlias = "btn";
            base.Awake();

            genericView.SetActive("extraCurencyPercent", false);
        }

        void SetCost(Cost cost)
        {
            genericView.SetText("txtReward", "+" + cost.value.ToString());
            genericView.SetSprite("iconCost", cost.sprite);
        }

        public void SetBadge(BankBadgeType type_)
        {
            genericView.SetActive("BestValueBadge", type_ == BankBadgeType.Best);
            genericView.SetActive("PopularBadge", type_ == BankBadgeType.Popular);
        }

        public void SetPercent(int val)
        {
            genericView.SetActive("extraCurencyPercent", val > 0);
            genericView.GetComponent<Text>("extraCurencyPercent").text = val.ToString() + "% " + Loc.Get("idMore");                     
        }

    }
}
