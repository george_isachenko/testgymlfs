﻿using UnityEngine;
using System.Collections;
using Data;
using View.UI.Shop;
using System;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class ShopUltimateViewItem : UIBaseViewListItem 
    {
        public enum LockType
        {
            None,
            Level,
            Time,
            GymSize,
            // hack, need rewrite normal locking result`
            GymExpandSeparator,
            GymExpand1,
            GymExpand2,
            GymExpand3,
            GymExpand4,
            GymExpand5,
            GymExpand6
        }
        
        public GameObject               interiorPrefab;
        [HideInInspector] public ShopScrollViewItemExersise       exercise;
        [HideInInspector] public Button                           btnBuyMain;

        private UltimateShopItemType            _type;
        public  UltimateShopItemType            type   { get { return _type; } set { SetType(value); } }
        public Action<ShopUltimateViewItem>     onSkip;

        public string       title           { set { genericView.SetText("txtTitle", value);  } }
        public string       meshPrefab      { set { SetMeshPrefab(value); } }
        public string       inStock         { set { SetInStock(value); } }
        public string       inStorage       { set { SetInStorage(value); } }
        public Sprite       icon            { set { SetIcon(value); } }
        public bool         btnInfoOn       { set { genericView.SetActive("btnInfo", value); } }
        public Cost         cost            { set { SetCost(value); } }
        public bool         outOfStock      { set { SetOutOfStock(value); } }
        public int          interiorId      { set { SetInteriorId(value); } }
        public bool         owned           { set { btnRestoreOn = value; if (value) genericView.SetActive("btnBuy2", false);  } }
        public string       time            { set { txtTime.text = value; } get { return txtTime.text; } }
        public int          skipPrice       { set { SetSkipPrice(value); } }
        public int          style           { set { SetStyle(value); } }
        public int          lockGymSize     { set { SetLockGymSize(value); } }
        public bool         exerciseOn      { set { genericView.SetActive("exercise", value); } }
        public bool         newOn           { set { genericView.SetActive("new", value); } }


        bool            imgOn           { set { genericView.SetActive("imgIcon", value); } }
        bool            lockerOn        { set { genericView.SetActive("lock", value); } }
        bool            interiorOn      { set { genericView.SetActive("interiorRoot", value); } }
        bool            inStorageOn     { set { genericView.SetActive("inStorage", value); } }
        bool            inStockOn       { set { genericView.SetActive("inStock", value); } }
        bool            btnBuyOn        
        { 
            set 
            { 
                genericView.SetActive("btnBuy2", value); 
                if (btnBuyMain != null) 
                    btnBuyMain.interactable = value;
            } 
        }
        bool            btnRestoreOn    { set { genericView.SetActive("btnRestore", value); } }


        Text                    txtTime;
        Transform               interiorRoot    { get { return genericView.GetTransform("interiorRoot"); } }

        bool                    item3dLocked    { set { SetItem3dLocked(value); } }
        Transform               item3dRoot      { get { return genericView.GetTransform("3dRoot"); } }
        GameObject              item3d;
        MeshRenderer            item3dRenderer;
        MaterialPropertyBlock   item3dMaterialPropBlock;
        int                     item3dShaderPropertyId;

        ShopScrollViewItemWallFloor interiorView;

        new void Awake()
        {
            buyButtonAlias = "btnBuy";
            base.Awake();

            btnBuyOn        = true;
            btnRestoreOn    = false;
            btnInfoOn       = false;
            inStorageOn     = false;
            inStockOn       = false;
            exerciseOn      = false;

            item3dMaterialPropBlock = new MaterialPropertyBlock();
            item3dShaderPropertyId = Shader.PropertyToID("_Color");

            txtTime = genericView.GetComponent<Text>("txtTime");
            exercise = genericView.GetComponent<ShopScrollViewItemExersise>("exercise");

            genericView.SetButtonCallback("btnSkip", OnSkip);
            genericView.SetButtonCallback("btnRestore", OnRestore);

            btnBuyMain = genericView.GetComponent<Button>("btnBuy");
        }

        void OnSkip()
        {
            if (onSkip != null)
                onSkip(this);
            else
                Debug.LogError("No skip callback");
        }

        void OnRestore()
        {
            if (OnClickCallback != null)
                OnClickCallback.Invoke(this);
        }

        void SetIcon(Sprite value)
        {
            if (value == null)
            {
                imgOn = false;
                return;
            }

            imgOn = true;
            genericView.SetSprite("imgIcon", value);
        }

        void SetMeshPrefab(string value)
        {
            if (value != null || value != string.Empty)
            {
                if (item3d != null)
                    item3d.SetActive(true);
            }
                
            if (item3d != null)
            {
                if(item3d.name == value)
                    return;
                else
                    Destroy(item3d);
            }
            
            if (value == null || value == string.Empty)
            {
                if (item3d != null)
                    item3d.SetActive(false);
                return;
            }

            var prefab = Resources.Load<GameObject>("UI/EquipmentUI3dPrefabs/" + value);
            if (prefab == null)
            {
                Debug.LogError("ShopUltimateViewItem: can't load prefab " + value);
                return;
            }
            item3d = Instantiate(prefab);
            item3d.SetActive(true);
            item3d.transform.SetParent(item3dRoot, false);
            item3dRenderer = item3d.GetComponentInChildren<MeshRenderer>();
            item3d.name = value;
        }

        void SetCost(Cost cost)
        {
            if (cost.type == Cost.CostType.CoinsOnly)
                genericView.SetSprite("btnBuyIcon", Cost.GetSpriteCoins());
            else if (cost.type == Cost.CostType.BucksOnly)
                genericView.SetSprite("btnBuyIcon", Cost.GetSpriteBucks());
            else
            {
                Debug.LogError("ShopUltimateViewItem : wrong Cost type " + cost.type.ToString());
                return;
            }

            genericView.SetText("btnBuyText", cost.value.ToString());
            genericView.SetText("txtUnlockLevel", cost.level.ToString());
        }

        void SetItem3dLocked(bool value)
        {
            if (item3dRenderer != null)
            {
                var multColor = value ? new Color(0.2f, 0.2f, 0.2f, 1f) : Color.white;
                item3dMaterialPropBlock.SetColor(item3dShaderPropertyId, multColor);
                item3dRenderer.SetPropertyBlock(item3dMaterialPropBlock);
            }
        }

        void SetOutOfStock(bool value)
        {
            genericView.SetActive("outOfStock", value);
            if (value)
            {
                btnBuyOn = false;
                item3dLocked = value;
            }
        }

        void InitInterior()
        {
            var obj = UIBaseView.InstantiatePrefab(interiorPrefab, interiorRoot, "WallFloor", 0.9f);
            interiorView = obj.GetComponent<ShopScrollViewItemWallFloor>();
        }

        void SetInteriorId(int id)
        {
            if (id < 0)
            {
                interiorOn = false;
                return;
            }

            interiorOn = true;

            if (interiorView == null)
                InitInterior();

            interiorView.SetId(id);
        }

        void SetInStock(string value)
        {
            if (value == string.Empty)
            {
                inStockOn = false;
                return;
            }

            inStockOn = true;

            genericView.SetText("txtInStock", value);
        }

        void SetInStorage(string value)
        {

            if (value == string.Empty)
            {
                inStorageOn = false;
                return;
            }

            inStorageOn = true;
            genericView.SetText("txtInStorage", value);

            if (type == UltimateShopItemType.SportResources)
            {
                genericView.SetActive("inStorageLock", value != string.Empty);
                genericView.SetText("txtInStorageLock", value);
            }
        }

        void SetType(UltimateShopItemType type)
        {
            _type = type;
        }

        void UpdateLocks()
        {
            if (type == UltimateShopItemType.SportResources)
            {
                genericView.SetActive("lockTime", time != string.Empty);
                genericView.SetActive("lockLevel", time == string.Empty);
                genericView.SetActive("lockRoom", false); 
            }
            else 
            {
                genericView.SetActive("lockTime", false);
                genericView.SetActive("lockLevel", type != UltimateShopItemType.SportEquip);
                genericView.SetActive("lockRoom", type == UltimateShopItemType.SportEquip); 
            }
        }

        private void SetSkipPrice(int value)
        {
            if (value == 0)
                genericView.SetText("txtSkipPrice", "FREE"); 
            else
                genericView.SetText("txtSkipPrice", value.ToString()); 
        }

        void SetStyle(int value)
        {
            genericView.SetActive("style", value > 0);

            if (value > 0)
            {
                genericView.SetText("txtStyle", "+" + value.ToString());
            }
        }

        void SetLockGymSize(int value)
        {
            if (value > 0)
            {
                genericView.SetText("txtUnlockGymSize", value.ToString());
            }
        }

        void SetLocked(bool value)
        {
            lockerOn = value;
            btnBuyOn = !value;
            item3dLocked = value;

            if (type == UltimateShopItemType.SportResources)
            {
                genericView.SetActive("inStorageLock", value);
                genericView.SetActive("inStorage", !value);
            }
        }

        public void SetLockType(LockType value, string needeRoomName = "")
        {

            var isLocked = (value != LockType.None);

            SetLocked(isLocked);

            if (isLocked)
            {
                genericView.SetActive("lockTime", value == LockType.Time);
                genericView.SetActive("lockLevel", value == LockType.Level);
                genericView.SetActive("lockRoom", value >= LockType.GymExpandSeparator); 
                genericView.SetActive("lockGymSize", value == LockType.GymSize);

                if (value >= LockType.GymExpandSeparator)
                {
                    genericView.SetText("lockRoomText", Loc.Get("shopUnlockRequired", needeRoomName));
                }
            }
        }

        public void ActivateDelayedSkipShowing()
        {
            var anim = genericView.GetComponent<Animator>("btnSkip");
            if (!gameObject.activeInHierarchy)
                return;
            StartCoroutine(WaitForAnimInited(anim, () => { anim.SetTrigger("animateShowing"); }));
        }

        IEnumerator forceShowCoroutine = null;

        public void SetForceShowSkipAnimationState(bool isShown)
        {
            var anim = genericView.GetComponent<Animator>("btnSkip");
            if (!gameObject.activeInHierarchy)
                return;
            if (forceShowCoroutine != null)
                StopCoroutine(forceShowCoroutine);
            forceShowCoroutine = WaitForAnimInited(anim,
                () =>
                {
                    anim.SetBool("forceShow", isShown);
                    forceShowCoroutine = null;
                });
            StartCoroutine(forceShowCoroutine);
        }

        private IEnumerator WaitForAnimInited(Animator anim, Action action)
        {
            while (!anim.isInitialized)
            {
                yield return null;
            }
            action();
        }
    }
}
