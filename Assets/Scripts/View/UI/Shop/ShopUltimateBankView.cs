﻿using UnityEngine;
using System.Collections;
using View.UI.Base;

namespace View.UI
{
    public class ShopUltimateBankView : UIGenericMonoBehaviour
    {
        
        [HideInInspector] public ShopUltimateBankViewList row1;
        [HideInInspector] public ShopUltimateBankViewList row2;

        void Awake()
        {
            row1 = genericView.GetComponent<ShopUltimateBankViewList>("row1");
            row2 = genericView.GetComponent<ShopUltimateBankViewList>("row2");
        }
    }
}
