﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI.Shop
{
    [AddComponentMenu("Kingdom/View/UI/Shop/Sport Shop View")]
    public class SportShopView : UITabControlledList<SportShopViewItem>
    {
        public Action onSetDirty = () => { };
        public UnityEvent<int> onBuy = new UnityEventWithIndexer();

        protected override void Awake()
        {
            BindCloseButton();

            tabsGenericAliases = new[]
            {
                "tabBtn_resources",
                "tabBtn_equipment"
            };
            
            itemsRoot = genericView.GetTransform("contentRoot");

            base.Awake();
        }

        protected override SportShopViewItem CreateNewItem()
        {
            var item = base.CreateNewItem();
            item.onSetDirty += onSetDirty;
            item.OnClickCallback += u => onBuy.Invoke(items.IndexOf((SportShopViewItem) u));
            return item;
        }

        public SportShopViewItem this[int i]
        {
            get { return items[i]; }
        }

        public void ShowResourcesTab()
        {
            Show();
            SwitchTab(0);
        }

        public void ShowTrainersTab()
        {
            Show();
            SwitchTab(1);
        }
    }

    public class UnityEventWithIndexer : UnityEvent<int>
    {
    }
}
