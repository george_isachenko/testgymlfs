using System;
using System.Linq;
using Core;
using Data;
using Data.Sport;
using Logic.Facades;
using Presentation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI.Shop
{
    public class SportShopViewItem : UIBaseViewListItem
    {
        public Transform buttonTransform
        {
            get { return genericView.GetTransform("buyButton"); }
        }

        public Action onSetDirty = () => { };
        public UnityEvent onBuy = new Button.ButtonClickedEvent();

        private SportItemLockInfo locker;
        private Animator animator;

        public SportItemLockInfo lockInfo
        {
            set
            {
                locker = value;
                genericView.SetActive("locker", value.isLocked);
                genericView.SetActive("buyButton",
                    (value.isLocked && value.lockType == SportItemLocktype.waitCooldown) || !value.isLocked ||
                    (value.isLocked && value.lockType == SportItemLocktype.fullStorage));
                switch (locker.lockType)
                {
                    case SportItemLocktype.userHaveSmallLevel:
                        genericView.SetText("lockerDescription", "Will be available at level " + value.unlockLevel);
                        break;
                    case SportItemLocktype.fullStorage:
                        genericView.SetText("lockerDescription", "Storage is full");
                        break;
                    case SportItemLocktype.sportRoomLocked:
                        genericView.SetText("lockerDescription", "Sport room locked");
                        break;
                    default:
                        genericView.SetText("lockerDescription", "");
                        break;
                }
            }
        }

        protected void Update()
        {
            if (!locker.isLocked || locker.lockType != SportItemLocktype.waitCooldown) return;
            var caption = View.PrintHelper.GetTimeString(locker.timer.timeSpan);
            genericView.SetText("lockerDescription", "Restock in:\n" + caption);
            genericView.SetText("price", DynamicPrice.GetSkipCost((int) locker.timer.secondsToFinish).ToString());
            if (locker.timer.secondsToFinish <= 0)
                onSetDirty();
        }

        protected override void Awake()
        {
            animator = GetComponent<Animator>();
            genericView.SetButtonCallback("swapInfoButton", SwitchInfoState);
            buyButtonAlias = "buyButton";
            genericView.SetButtonCallback("buyButton", () => onBuy.Invoke());
            base.Awake();
        }

        private void SwitchInfoState()
        {
            animator.SetBool("InfoShowing", !animator.GetBool("InfoShowing"));
        }

        public void SetData(SportShopEquipLogic equip)
        {
            SetBaseData(equip);

            // set icon
            var t = genericView.GetTransform("itemIcon");
            t.Cast<Transform>().ToList().ForEach(child => Destroy(child.gameObject));
            genericView.SetSprite("itemIcon", null);
            var prefab = Resources.Load<GameObject>("UI/EquipmentUI3dPrefabs/" + equip.equipData.meshPrefab);
            if (prefab == null)
                return;
            var item3D = Instantiate(prefab);
            item3D.SetActive(true);
            item3D.transform.SetParent(t, false);
            // set name
            genericView.SetText("name", equip.equipData.name);
            //stack
            genericView.SetText("cooldownCountText","");
        }

        public void SetData(SportShopResourceLogic resource)
        {
            SetBaseData(resource);

            // set icon
            var t = genericView.GetTransform("itemIcon");
            t.Cast<Transform>().ToList().ForEach(child => Destroy(child.gameObject));
            var ind = (int) resource.resourceData.id;
            var sprite = GUICollections.instance.storageIcons.items[ind].sprite;
            genericView.SetSprite("itemIcon", sprite);
            // set name
            genericView.SetText("name", resource.resourceData.name);
            //stack
            genericView.SetText("cooldownCountText",
                String.Format("{0}/{1}",
                    resource.currentCooldownStack.stackSize - resource.currentCooldownStack.count,
                    resource.currentCooldownStack.stackSize));
        }

        private void SetBaseData(SportShopItemInfo item)
        {
            genericView.SetSprite("trainingTargetVisual", item.trainingResultSportsmanIcon);
            genericView.SetSprite("forTrainingVisual", item.forTrainingSportsmanIcon);

            // cost
            genericView.SetActive("buyBucksIcon", locker.isLocked && locker.lockType == SportItemLocktype.waitCooldown);
            genericView.SetActive("buyCoinsIcon", !locker.isLocked);
            genericView.SetActive("price",
                !locker.isLocked ||
                (locker.isLocked &&
                 (locker.lockType == SportItemLocktype.waitCooldown || locker.lockType == SportItemLocktype.fullStorage)));
            if (!locker.isLocked)
                genericView.SetText("price", item.buyItemCost.valueString);
            if (locker.isLocked && locker.lockType == SportItemLocktype.waitCooldown)
                genericView.SetText("price", DynamicPrice.GetSkipCost((int)locker.timer.secondsToFinish).ToString());
            if (locker.isLocked && locker.lockType == SportItemLocktype.fullStorage)
                genericView.SetText("price", "Open storage");
            genericView.SetText("countAtStorageText", item.countAtStorage.ToString());
        }
    }
}