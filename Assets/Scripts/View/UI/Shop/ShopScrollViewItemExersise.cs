﻿using Data;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace View.UI.Shop
{
    [AddComponentMenu("Kingdom/View/UI/Shop/Shop Scroll View Item Exersise")]
    public class ShopScrollViewItemExersise : MonoBehaviour
    {
        public Sprite   typeSprite  { set { if (icon != null) icon.sprite = value; } }
        public int      power       { set { SetPower(value); } }
        public bool     isInited    { get { return full != null && full.fillAmount > 0; } }

        Image icon;
        Image full;



        void Awake ()
        {
            icon = transform.Find("Icon").GetComponent<Image>();
            full = transform.Find("Full").GetComponent<Image>();

            Assert.IsNotNull(icon);
            Assert.IsNotNull(full);

            full.fillAmount = 0;
        }

        void SetPower(int value)
        {
            if (full == null)
                return;
            
            switch (value)
            {
            case 0:
                full.fillAmount = 0f; break;

            case 1:
                full.fillAmount = 0.25f; break;

            case 2:
                full.fillAmount = 0.5f; break;

            case 3:
                full.fillAmount = 0.75f; break;

            default:
                full.fillAmount = 1f; break;
            }
        }
    }

}