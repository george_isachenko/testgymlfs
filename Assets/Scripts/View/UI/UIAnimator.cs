﻿using UnityEngine;

public class UIAnimator : MonoBehaviour
{
    #region Types.
    public enum UIAnimationType 
    {
        HideAutoDetect,
        HideLeft,
        HideRight,
        HideTop,
        HideBottom,
        HideAlpha
    }

    enum State
    {
        Visible = 0,
        Showing,
        Hidden,
        Hiding
    }
    #endregion

    #region Inspector fields.
    [SerializeField]
    protected UIAnimationType type = UIAnimationType.HideAutoDetect;

    [SerializeField]
    protected float hideTime = 0.5f;

    [SerializeField]
    protected float showTime = 0.5f;
    #endregion

    #region Private date.
    private Vector3 initPos;
    private Vector3 hiddenPos;
    int tweenId = 0;
    CanvasGroup canvasGroup = null;
    State state = State.Visible;
    #endregion

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        initPos = transform.localPosition;

        if (type == UIAnimationType.HideAutoDetect)
        {
            var rect = transform as RectTransform;
            if (rect != null)
            {
                if (rect.anchoredPosition.x == 0)
                    type = UIAnimationType.HideLeft;
                else if (rect.anchoredPosition.x == Screen.width)
                    type = UIAnimationType.HideRight;
                else if (rect.anchoredPosition.y == 0)
                    type = UIAnimationType.HideTop;
                else if (rect.anchoredPosition.y == Screen.height)
                    type = UIAnimationType.HideBottom;
                else
                    type = UIAnimationType.HideAlpha;
            }
            else
                Debug.LogError("UIAnimator : rect transform is necessary");
        }

        hiddenPos = GetHiddenPos();
        state = gameObject.activeSelf ? State.Visible : State.Hidden;
    }

    public void Show(bool show)
    {
        if (show)
        {
            if (state == State.Hidden || state == State.Hiding || !gameObject.activeSelf)
            {
                if (tweenId != 0)
                {
                    LeanTween.cancel(tweenId);
                    tweenId = 0;
                }

                gameObject.SetActive(true);

                state = State.Showing;
                tweenId = LeanTween.moveLocal(gameObject, initPos, showTime)
                    .setEase(LeanTweenType.easeOutQuart)
                    .setOnComplete(() =>
                        {
                            tweenId = 0;
                            state = State.Visible;
                            if (canvasGroup != null)
                            {
                                canvasGroup.interactable = true;
                            }
                        })
                    .id;
            }
        }
        else
        {
            if (state == State.Visible || state == State.Showing)
            {
                if (tweenId != 0)
                {
                    LeanTween.cancel(tweenId);
                    tweenId = 0;
                }
            
                if (canvasGroup != null)
                {
                    canvasGroup.interactable = false;
                }

                state = State.Hiding;
                tweenId = LeanTween.moveLocal(gameObject, hiddenPos, hideTime)
                    .setEase(LeanTweenType.easeInQuart)
                    .setOnComplete(() =>
                        {
                            tweenId = 0;
                            state = State.Hidden;
                            gameObject.SetActive(false);
                        })
                    .id;
            }
        }
    }
        
    Vector3 GetHiddenPos()
    {
        var rect = (transform as RectTransform).rect;


        if (type == UIAnimationType.HideLeft)
            return transform.localPosition - new Vector3(rect.width, 0, 0);
        else if (type == UIAnimationType.HideRight)
            return transform.localPosition + new Vector3(rect.width, 0, 0);
        else if (type == UIAnimationType.HideTop)
            return transform.localPosition + new Vector3(0, rect.height, 0);
        else if (type == UIAnimationType.HideBottom)
            return transform.localPosition - new Vector3(0, rect.height, 0);
        else
            return transform.localPosition;    
    }

}
