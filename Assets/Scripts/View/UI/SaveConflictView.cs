﻿using System;
using System.Collections;
using Data;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class SaveConflictView : UIGenericMonoBehaviour
    {
        #region Private properties.
        private bool   btnUseCloud              { set { genericView.GetComponent<Selectable>("btnUseCloud").interactable = value; } }
        private bool   btnUseDevice             { set { genericView.GetComponent<Selectable>("btnUseDevice").interactable = value; } }
        private string txtCloudLevel            { set { genericView.SetText("txtCloudLevel", value); } }
        private string txtCloudExperience       { set { genericView.SetText("txtCloudExperience", value); } }
        private string txtCloudSaveTime         { set { genericView.SetText("txtCloudSaveTime", value); } }
        private string txtDeviceLevel           { set { genericView.SetText("txtDeviceLevel", value); } }
        private string txtDeviceExperience      { set { genericView.SetText("txtDeviceExperience", value); } }
        private string txtDeviceSaveTime        { set { genericView.SetText("txtDeviceSaveTime", value); } }
        #endregion

        #region Public(Inspector) fields.
        public Color newerColor;
        public float waitUntilButtonsEnabledTime = 1.0f;
        #endregion

        #region Private fields.
        private Action onUseCloud;  
        private Action onUseDevice;
        #endregion

        #region Unity API.
        private void Awake()
        {
            genericView.SetButtonCallback("btnUseCloud", OnUseCloudClick);
            genericView.SetButtonCallback("btnUseDevice", OnUseDeviceClick);
        }
        #endregion
            
        #region Public API.
        public void Show
            ( int cloudLevel
            , string cloudExperience
            , float cloudExpRatio
            , TimeSpan cloudDateAgo
            , int deviceLevel
            , string deviceExperience
            , float deviceExpRatio
            , TimeSpan deviceDateAgo
            , Action onUseCloud
            , Action onUseDevice)
        {
            txtCloudLevel = (cloudLevel > deviceLevel)
                ? Loc.Get ("SaveConflictViewPanelLevel", string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(newerColor), cloudLevel))
                : Loc.Get ("SaveConflictViewPanelLevel", cloudLevel);

            txtCloudExperience = cloudExperience;/*(cloudExperience > deviceExperience)
                ? Loc.Get ("SaveConflictViewPanelExperience", string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(newerColor), cloudExperience))
                : Loc.Get ("SaveConflictViewPanelExperience", cloudExperience);*/

            txtCloudSaveTime = Loc.Get ("SaveConflictViewPanelSaved", Loc.Get(cloudDateAgo));

            txtDeviceLevel = (deviceLevel > cloudLevel)
                ? Loc.Get ("SaveConflictViewPanelLevel", string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(newerColor), deviceLevel))
                : Loc.Get ("SaveConflictViewPanelLevel", deviceLevel);

            txtDeviceExperience = deviceExperience;/*(deviceExperience > cloudExperience)
                ? Loc.Get ("SaveConflictViewPanelExperience", string.Format("<color=#{0}>{1}</color>", ColorUtility.ToHtmlStringRGBA(newerColor), deviceExperience))
                : Loc.Get ("SaveConflictViewPanelExperience", deviceExperience);*/

            txtDeviceSaveTime = Loc.Get ("SaveConflictViewPanelSaved", Loc.Get(deviceDateAgo));

            this.onUseCloud = onUseCloud;
            this.onUseDevice = onUseDevice;

            btnUseCloud = false;
            btnUseDevice = false;

            genericView.GetComponent<Slider>("progressCloud").value = cloudExpRatio;
            genericView.GetComponent<Slider>("progressDevice").value = deviceExpRatio;

            Show();

            StartCoroutine(DelayedButtonsEnableJob());
        }

        public void Hide()
        {
            gameObject.SetActive(false);

            onUseCloud = null;
            onUseDevice = null;
        }
        #endregion

        #region Private functions.
        private IEnumerator DelayedButtonsEnableJob()
        {
            yield return null;

            if (waitUntilButtonsEnabledTime > 0)
            {
                yield return new WaitForSeconds(waitUntilButtonsEnabledTime);
            }

            btnUseCloud = true;
            btnUseDevice = true;
        }
            
        void OnUseCloudClick()
        {
            var tmpCallback = onUseCloud;
            onUseCloud = null;
            onUseDevice = null;

            Hide();

            if (tmpCallback != null)
            {
                tmpCallback();
            }
        }
            
        void OnUseDeviceClick()
        {
            var tmpCallback = onUseDevice;
            onUseCloud = null;
            onUseDevice = null;

            Hide();

            if (tmpCallback != null)
            {
                tmpCallback();
            }
        }

        void Show()
        {
            gameObject.SetActive(true);
            transform.parent.gameObject.SetActive(true);
        }
        #endregion
   }
}