using System;
using System.Collections.Generic;
using Data;
using Data.Person;
using Data.Sport;
using Logic.Facades;
using Logic.PlayerProfile;
using Logic.SportAgency.Facade;
using UnityEngine;
using View.UI;

public class SportTrainingViewData
{
    public int exercisesCount;
    public SportTrainingExerciseViewData[] exercises;
    public string headerText;

    public SportTrainingViewData(List<SportExercise> exercises, LogicStorage storage, LogicSportsmen sportmanManager, LogicSportAgency sportAgencyLogic)
    {
        exercisesCount = exercises.Count;

        this.exercises = new SportTrainingExerciseViewData[exercisesCount];
        for (var i = 0; i < exercisesCount; i++)
        {
            var exercise = exercises[i];
            var existingResources = storage.GetResourcesCount(exercise.needResource);
            var existingSportsmans = sportmanManager.GetSportsmanCount(exercise.trainFrom, null, PersonState.Idle, PersonState.Entering, /*PersonState.InQueue,*/ PersonState.ExerciseComplete);
            var canTrain = existingSportsmans > 0;

            this.exercises[i] = new SportTrainingExerciseViewData()
            {
                durationText = View.PrintHelper.GetTimeString(exercise.duration),
                targetSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int) exercise.trainTo].sprite,
                baseSportsmanIcon = GUICollections.instance.sportsmenIcons.items[(int) exercise.trainFrom].sprite,
                needResourceIcon = GUICollections.instance.storageIcons.items[(int) exercise.needResource].sprite,
                needResourceText =
                    String.Format("<color={0}>{1}/1</color>", existingResources < 1 ? "red" : "green", existingResources),
                needSportsmanText =
                    String.Format("<color={0}>{1}/1</color>",
                        existingSportsmans < 1 ? "red" : "green",
                        existingSportsmans),
                isBuyTextEnabled = !canTrain,
                isTrainLabelEnabled = canTrain,
                buyButtonGrayMode = false, //!canTrain && existingResources > 0 && !sportAgencyLogic.canUseMyFeature.canUse,
                sportNameText = Loc.Get(exercise.sportLocNameId)
            };
        }
    }
}


public class SportTrainingExerciseViewData
{
    public Sprite targetSportsmanIcon;
    public Sprite needResourceIcon;
    public Sprite baseSportsmanIcon;
    public string durationText;
    public string needResourceText;
    public string needSportsmanText;
    public string sportNameText;
    public bool isBuyTextEnabled;
    public bool isTrainLabelEnabled;
    public bool buyButtonGrayMode;
}