﻿using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    public class BuyWithResourcesView : UIBaseView
    {
        public Transform btnBuyTransform { get { return genericView.GetTransform("btnBuy"); } }

        public string   header          { set { genericView.SetText("header", value.ToString()); } }
        public int      priceTotal      { set { genericView.SetText("btnBuyPriceText", value.ToString()); } }
        public bool     extraPriceOn    { set { SetExtraPrice(value); } }

        public string actionButtonPrefixText
        {
            set { genericView.SetText("actionButtonPrefix", value); }
        }

        public CostViewSubList costView;
        
        void Awake()
        {
            BindCloseButton();
            costView = genericView.GetComponent<CostViewSubList>("costViewSubList");
        }

        void SetExtraPrice(bool value)
        {
            genericView.SetActive("btnBuyPriceText", value);
            genericView.SetActive("btnBuyPriceIcon", value);
        }

        public UIGenericView IsolateHeader(string alias)
        {
            var headers = new[]
            {
                "expandStorage",
                "expandGym",
                "expandSportsmenLimit"
            };
            foreach (var a in headers)
            {
                genericView.SetActive(a, false);
            }
            genericView.SetActive(alias);
            return genericView.GetSubView(alias);
        }
    }
}
