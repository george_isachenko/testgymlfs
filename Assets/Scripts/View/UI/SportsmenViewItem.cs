﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Data.Person;
using View.UI.Base;

namespace View.UI
{
    public class SportsmenViewItem : UIBaseViewListItem
    {

        [HideInInspector] public override bool selected {set { genericView.SetActive("selected", value); }}    

        private SportsmanType _sportType;

        public bool isTraining{ get; private set;}

        public SportsmanType sportType
        {
            set
            {
                SetIcon(value);
                _sportType = value;
            }
            get { return _sportType; }
        }

        public string text
        {
            set { genericView.SetText("txtName", value); }
        }

        public bool showTimeIcon
        {
            set
            {
                genericView.SetActive("timeIcon", value);
                isTraining = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            sportType = SportsmanType.Sportsman_0;
        }

        private void SetIcon(SportsmanType value)
        {
            if (GUICollections.instance != null)
            {
                var icon = GUICollections.instance.sportsmenIcons.items[(int) value].sprite;
                genericView.SetSprite("icon", icon);
            }
        }
    }
}