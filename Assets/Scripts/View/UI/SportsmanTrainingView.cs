using UnityEngine;
using View.UI.Base;

public class SportsmanTrainingView : UIBaseView
{
    public delegate void SportExerciseSelectedAction(int exerciseIndex);

    public SportsmanTrainingExerciceView[] exerciceViews;
    public SportExerciseSelectedAction onExerciseSelected = a => { };

    void Awake()
    {
        BindCloseButton();

        if (exerciceViews == null)
        {
            exerciceViews = gameObject.GetComponentsInChildren<SportsmanTrainingExerciceView>();
        }

        foreach (var view in exerciceViews)
        {
            view.onButtonPressed += v =>
            {
                var index = -1;
                for (var i = 0; i < exerciceViews.Length; i++)
                    if (exerciceViews[i] == v)
                    {
                        index = i;
                        break;
                    }
                onExerciseSelected(index);
            };
        }
    }

    public void FillExercisesFields(SportTrainingViewData viewData)
    {
        if (viewData.exercisesCount != exerciceViews.Length)
        {
            Debug.LogErrorFormat("Count of exercises {0} no same than exercisec view {1}, can fill wrong valuers",
                viewData.exercisesCount,
                exerciceViews.Length);
        }

        genericView.SetText("headerText",viewData.headerText);

        for (var i = 0; i < Mathf.Min(viewData.exercisesCount, exerciceViews.Length); i++)
        {
            var exerciseViewData = viewData.exercises[i];
            var view = exerciceViews[i];
            view.FillData(exerciseViewData);
        }
    }
}