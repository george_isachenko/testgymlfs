﻿using UnityEngine;
using View.UI.Base;

namespace View.UI.HUD
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Stars Icon")]
    public class StarsIcon : UIGenericMonoBehaviour
    {
        #region Private properties.
        private bool    activeStar1  { set { genericView.SetActive("Star1", value); } }
        private bool    activestar2  { set { genericView.SetActive("Star2", value); } }
        private bool    activeStar3  { set { genericView.SetActive("Star3", value); } }
        #endregion

        #region Public properties.
        public int level
        {
            set
            {
                if (value > 0)
                {
                    gameObject.SetActive(true);
                    activeStar1 = (value > 0);
                    activestar2 = (value > 1);
                    activeStar3 = (value > 2);
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
        #endregion
    }
}