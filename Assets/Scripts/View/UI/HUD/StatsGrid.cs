﻿using UnityEngine;
using View.UI.Base;

namespace View.UI.HUD
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Stats Grid")]
    public class StatsGrid : UIGenericMonoBehaviour
    {
        #region Private properties.
        public StatIcon   statIcon1      { get { return genericView.GetComponent<StatIcon>("Stat1"); } }
        public StatIcon   statIcon2      { get { return genericView.GetComponent<StatIcon>("Stat2"); } }
        #endregion

        #region Indexers.
        public StatIcon this[int index]
        {
            get
            {
                return GetIcon(index);
            }
        }
        #endregion

        #region Public functions.
        public StatIcon   GetIcon  ( int idx )
        {
            switch (idx)
            {
                case 0: return statIcon1;
                case 1: return statIcon2;
                default: return null;
            }
        }
        #endregion
    }
}