﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.HUD
{
    public delegate string ActiveTextDelegate();

    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Active Text")]
    public class ActiveText : MonoBehaviour
    {
        public ActiveTextDelegate textGetter = null;
        protected Text text_;

        protected string timeValue
        {
            set
            {
                if (text_ == null)
                {
                    text_ = gameObject.GetComponentInSelfOrChild<Text>("ActiveText");
                    if (text_ == null)
                        return;
                }

                text_.text = value;
                text_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }

        void Update()
        {
            timeValue = textGetter != null ? textGetter() : string.Empty;
        }
    }
}
