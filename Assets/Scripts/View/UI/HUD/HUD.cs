﻿using System;
using Core;
using Core.Network;
using Data;
using InspectorHelpers;
using UI.HUD.ExerciseSelector;
using UI.HUD.Repair;
using UI.HUD.Skip;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using View.UI.Base;
using UI.HUD;

namespace View.UI.HUD
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/HUD")]
    public class HUD : UIGenericMonoBehaviour
    {
        public enum HUDMode
        {
            None,
            Club,
            SportClub,
            JuiceBar
        }

        [Serializable]
        public struct GenericNetworkStatusIcon
        {
            public GenericNetworkStatus status;
            public Sprite icon;
        }

        private Button  btnDailyQuests_;
        private Button  btnDailyBonus_;
        
        private Button  btnAchievements             { get { return genericView.GetComponent<Button>("btnAchievements"); } }
        private Button  btnFriends;
        private Button  btnLaneQuests_              { get { return genericView.GetComponent<Button>("LaneQuest"); } }
        private Button  btnSportQuests_             { get { return genericView.GetComponent<Button>("SportQuest"); } }
        private Button  btnStorage_;

        public Button   btnBack                     { get; private set; }
        public Button   btnVisitorQuests            { get; private set; }
        public Button   btnSportsmen                { get; private set; }
        public Button   btnShop                     { get; private set; }
        public Button   btnSettings                 { get; private set; }
        public Button   btnDbg                      { get; private set; }
        public Button   btnShopOld                  { get { return genericView.GetComponent<Button>("btnShopOld"); } }
        public Button   btnPromo                    { get; private set; }
        public Button   btnJuiceBar                 { get { return genericView.GetComponent<Button>("btnJuiceBar"); } }
        public Button   btnSportClub                { get { return genericView.GetComponent<Button>("btnSportClub"); } }
        public Button   btnStyle                    { get { return genericView.GetComponent<Button>("btnStyle"); } }

        public bool     btnDbgNoSave                { set { genericView.SetActive("btnDbgNoSave", value); } }
        public int      btnAchievCounter            { set { genericView.SetText("achievementCounter", value > 3 ? "3+" : value.ToString()); } }
        public bool     btnAchievCounterOn          { set { genericView.SetActive("achievementCounterOn", value); } }
        public Text     txtPromoTimer               { get; private set; }
        public bool     btnJuiceBarBadge            { set { genericView.SetActive("juiceBarBadge", value); } }
        public bool     btnSportClubBadge           { set { genericView.SetActive("sportClubBadge", value); } }
        public ActiveText btnJuiceBarTimer          { get { return genericView.GetComponent<ActiveText>("btnJuiceBar"); } }
        public ActiveText btnSportClubTimer         { get { return genericView.GetComponent<ActiveText>("btnSportClub"); } }

        public Enabler  btnDailyQuests              { get; private set; }
        public Enabler  btnDailyBonus               { get; private set; }
        public Enabler  btnStorage                  { get; private set; }
        public Enabler  btnStorageFake              { get; private set; }
        public Enabler  btnLaneQuests               { get; private set; }
        public Enabler  btnSportQuests              { get; private set; }
        public Enabler  btnStyleEnabler             { get; private set; }
        public Enabler  btnAchievementsEnabler      { get; private set; }
        public Enabler  enablerPromo                { get; private set; }
        public Enabler  enablerJuiceBar             { get; private set; }
        public Enabler  enablerSportclub            { get; private set; }
        public Enabler  enablerFriends              { get; private set; }
        public Enabler  enablerTrainers             { get; private set; } 

        public GameObject dailyQuestHint            { get; private set; }
        public GameObject btnStorageFakeGO          { get {return transform.GetComponentInChild<Button>("BtnStorageFake").gameObject; } }

        public Action           OnAddCoins          { set { genericView.SetButtonCallback("btnAddCoins", value); } }
        public Action           OnAddFitBucks       { set { genericView.SetButtonCallback("btnAddBucks", value); } }
        public Action           OnAddPlatinum       { set { genericView.SetButtonCallback("btnPlatinum", value); } }
        public Action           OnShowAchievements  { set { genericView.SetButtonCallback("btnAchievements", value); } }
        public Action           OnShowTrainers      { set { genericView.SetButtonCallback("btnTrainers", value); } }
        public event Action     OnBackgroundClick;
        public Action           CustomDefaultView;
        public Action           OnFriendsClick      { set { btnFriends.onClick.AddListener(new UnityAction(value)); } }
        public Action           OnScoresClick       { set { genericView.SetButtonCallback("fitPointsPanel", value); } }

        private Text            txtDailyQuestCount;
        private AnimatedCounter coinsCounter;
        private AnimatedCounter fitBucksCounter;
        private AnimatedCounter platinumCounter;
        private Text newItemsTxt;

        private Func<bool> canUseTimeQuestsGetter;

        HUDMode hudMode = HUDMode.Club;
        bool    _modalMode = false;

        public string   stylePoints         { set { genericView.SetText("txtStylePoints", value);} }
        public int      styleCapacity       { set { genericView.SetText("txtStyleCapacity", value.ToString());} }
        public float    styleProgress       { set { genericView.SetSliderValue("slyderStyle",value); } }
        private bool    styleVisible        { set { genericView.ShowAnimated("stylePanel", value); } }//{ set { genericView.SetActive("stylePanel", value); } }

        public string   profileLevel        { set { genericView.SetText("txtLevel", value); } }
        public string   profileExpTxt       { set { genericView.SetText("txtXp", value); } }
        public float    profileExpProgress  { set { genericView.SetSliderValue("sliderXp",value); } }
        public bool     xpVisible           { set { genericView.SetActive("xpPanel", value); } }
        public float    storageProgress     { set { genericView.SetSliderValue("storageBar", value); } }
        public string   storageItemsTxt     { set { genericView.SetText("storageText", value); } }

        private bool fitPointsVisible       { set { genericView.ShowAnimated("fitPointsPanel", value); } }//{ set { genericView.SetActive("fitPointsPanel", value); } }


        public int fitPointsCount           { set { genericView.SetText("txtFitPoints", value.ToString()); } }

        public int      coins               { set { SetCoins( value ); } }
        public int      fitBucks            { set { SetFitbucks( value ); } }
        public int      platinum            { set { SetPlatinum( value); } }

        public int      dailyQuestCount     { set { SetDailyQuestsCount(value);} }
        public string   visitorQuestsCount  { set { genericView.SetText("visitorQuestsCount", value); }}
        public string   sportsmenCount      { set { genericView.SetText("sportsmenCout", value); }}

        public bool     modalMode           { set { _modalMode = value; UpdateWindowsModalMode (); genericView.SetActive("imgModalBkg", value); } }
        public bool     modalModeTutorial   { set { genericView.SetActive("imgModalBkgTutorial", value); } }
        public bool     modalModeConfirm    { set { genericView.SetActive("imgModalBkgConfirm", value); } }

        public bool     topLeftOn           { set { genericView.ShowAnimated("topLeft", value); } }
        public bool     botLeftOn           { set { genericView.ShowAnimated("botLeft", value); } }
        public bool     botRightOn          { set { genericView.ShowAnimated("botRight", value); } }
        public bool     botCenterOn         { set { genericView.ShowAnimated("botCenter", value); } }
        public bool     rightOn             { set { genericView.ShowAnimated("right", value); } }

        public bool     wndMode             { get; set; }

        public HUDPersonPanel           personPanel { get; private set; }
        public SportsmanPanel           sportsmanPanel { get; private set; }
        public ExerciseSelectorPanel    exerciseSelectorPanel { get; private set; }
        public SkipPanel                skipPanel { get; private set; }
        public RepairPanel              repairPanel { get; private set; }

        public RectTransform focusPointLeftMiddle { get; private set; }
        public RectTransform focusPointLeft025 { get; private set; }
        public RectTransform focusPointRightMiddle { get; private set; }
        public RectTransform focusPointRight025 { get; private set; }

        public Transform btnFriendsTransform { get { return btnFriends.transform; } }

        [ReorderableList]
        public GenericNetworkStatusIcon[] networkStatusIcons;
        public float networkStatusHintDelay = 0.0f;

        void Awake()
        {
            var botRight    = transform.Find("BotRight");
            var botLeft     = transform.Find("BotLeft");

            btnDbg              = gameObject.GetComponentInChild<Button>("BtnDbg");
            btnBack             = botRight.GetComponentInChild<Button>("BtnBack");
            btnShop             = botRight.GetComponentInChild<Button>("BtnShop");
            btnStorage_         = botRight.GetComponentInChild<Button>("BtnStorage");
			btnSettings         = botRight.GetComponentInChild<Button>("BtnSettings");
            btnPromo            = botRight.GetComponentInChild<Button>("BtnPromo");
            btnFriends          = botLeft.GetComponentInChild<Button>("BtnFriends");
            btnDailyQuests_     = botLeft.Find("QuestHolderClub").GetComponentInChild<Button>("BtnDailyQuests");
            btnVisitorQuests    = genericView.GetComponent<Button>("btnVisitorQuests");
            btnSportsmen        = genericView.GetComponent<Button>("btnSportsmen");
            btnDailyBonus_      = botLeft.Find("QuestHolderClub").GetComponentInChild<Button>("BtnDailyBonus");

            btnStorage              = new Enabler(btnStorage_.gameObject);
            enablerPromo            = new Enabler(btnPromo.gameObject);
            enablerJuiceBar         = new Enabler(btnJuiceBar.gameObject);
            enablerSportclub        = new Enabler(btnSportClub.gameObject);
            btnStorageFake          = new Enabler(btnStorageFakeGO);
            btnDailyQuests          = new Enabler(btnDailyQuests_.gameObject);
            btnDailyBonus           = new Enabler(btnDailyBonus_.gameObject);
            btnLaneQuests           = new Enabler(btnLaneQuests_.gameObject);
            btnSportQuests          = new Enabler(btnSportQuests_.gameObject);
            btnStyleEnabler         = new Enabler(btnStyle.gameObject);
            btnAchievementsEnabler  = new Enabler(btnAchievements.gameObject);
            enablerFriends          = new Enabler(btnFriends.gameObject);
            enablerTrainers         = new Enabler(genericView.GetGameObject("btnTrainers"));

            btnStorageFake.turnOn = false;

            coinsCounter    = transform.GetComponentInChild<AnimatedCounter>("BtnCoins");
            fitBucksCounter = transform.GetComponentInChild<AnimatedCounter>("BtnFitBucks");
            platinumCounter = transform.GetComponentInChild<AnimatedCounter>("BtnPlatinum");

            genericView.SetActive("btnPlatinum", false);

            newItemsTxt         = btnShop.transform.Find("ShopCountBg").Find("NewItemsCount").GetComponent<Text>();
            txtPromoTimer       = btnPromo.gameObject.GetComponentInChild<Text>("Timer");
            txtDailyQuestCount  = btnDailyQuests_.gameObject.GetComponentInChildren<Text>();

            dailyQuestHint = btnDailyQuests_.transform.Find("Hint").gameObject;

            personPanel = transform.GetComponentInChild<HUDPersonPanel>("PersonPanel");
            Assert.IsNotNull(personPanel);

            sportsmanPanel = transform.GetComponentInChild<SportsmanPanel>("SportsmenPanel");
            Assert.IsNotNull(sportsmanPanel);

            exerciseSelectorPanel = transform.GetComponentInChild<ExerciseSelectorPanel>("ExerciseSelectorPanel");
            Assert.IsNotNull(exerciseSelectorPanel);

            skipPanel = transform.GetComponentInChild<SkipPanel>("SkipPanel_new");
            Assert.IsNotNull(skipPanel);

            repairPanel = transform.GetComponentInChild<RepairPanel>("RepairPanel");
            Assert.IsNotNull(repairPanel);

            focusPointLeftMiddle =  transform.Find("FocusPointLeftMiddle") as RectTransform;
            Assert.IsNotNull(focusPointLeftMiddle);
            focusPointLeft025 =  transform.Find("FocusPointLeft025") as RectTransform;
            Assert.IsNotNull(focusPointLeft025);
            focusPointRightMiddle = transform.Find("FocusPointRightMiddle") as RectTransform;
            Assert.IsNotNull(focusPointRightMiddle);
            focusPointRight025 = transform.Find("FocusPointRight025") as RectTransform;
            Assert.IsNotNull(focusPointRight025);

            EventTrigger trigger = genericView.GetComponent<EventTrigger>("imgModalBkg");
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener( (eventData) => { BackgroundClick(); } );
            trigger.triggers.Add(entry);

            EventTrigger trigger2 = genericView.GetComponent<EventTrigger>("imgModalBkgTutorial");
            EventTrigger.Entry entry2 = new EventTrigger.Entry();
            entry2.eventID = EventTriggerType.PointerClick;
            entry2.callback.AddListener( (eventData) => { BackgroundClick(); } );
            trigger2.triggers.Add(entry);

            /*
            EventTrigger trigger3 = genericView.GetComponent<EventTrigger>("imgModalBkgConfirm");
            EventTrigger.Entry entry3 = new EventTrigger.Entry();
            entry3.eventID = EventTriggerType.PointerClick;
            entry3.callback.AddListener( (eventData) => { } );
            trigger3.triggers.Add(entry);
            */

#if !DEBUG
            if (btnDbg != null)
            {
                btnDbg.gameObject.SetActive(false);
            }
#endif
        }

        void Start ()
        {
            if (PersistentCore.instance != null && PersistentCore.instance.networkController != null)
            {
                PersistentCore.instance.networkController.onNetworkStatusChanged += OnNetworkStatusChanged;
                OnNetworkStatusChanged(PersistentCore.instance.networkController.lastNetworkStatus);

                // genericView.SetButtonCallback("noInternetConnection", OnNoInternetConnection);

                var hint = genericView.GetComponent<UIHintBehaviour>("noInternetConnection");
                if (hint != null)
                {
                    hint.customTransform = genericView.GetTransform("noInternetConnectionHintRoot") as RectTransform;
                    hint.delay = networkStatusHintDelay;
                }
            }
        }

        void OnDestroy ()
        {
            if (PersistentCore.instance != null && PersistentCore.instance.networkController != null)
            {
                PersistentCore.instance.networkController.onNetworkStatusChanged -= OnNetworkStatusChanged;
                // genericView.ResetButtonCallback("noInternetConnection");
            }
        }

        private void OnNetworkStatusChanged (GenericNetworkStatus lastNetworkStatus)
        {
            var idx = Array.FindIndex(networkStatusIcons, x => x.status == lastNetworkStatus);

            if (idx >= 0)
            {
                genericView.SetSprite ("noInternetConnection", networkStatusIcons[idx].icon);
                genericView.SetActive ("noInternetConnection", true);

                var hint = genericView.GetComponent<UIHintBehaviour>("noInternetConnection");
                if (hint != null)
                {
                    hint.title = null;
                    hint.desc = Loc.Get(string.Format("{0}{1}Header", typeof(GenericNetworkStatus).Name, lastNetworkStatus.ToString()));
                }
            }
            else
            {
                genericView.SetActive ("noInternetConnection", false);
            }
        }

        private void SetDailyQuestsCount(int count)
        {
            if (count <= 3)
                txtDailyQuestCount.text = count.ToString();
            else
                txtDailyQuestCount.text = "3+";

            txtDailyQuestCount.transform.parent.gameObject.SetActive(count > 0);
        }

        public void SetCoins(int coins)
        {
            if (coinsCounter != null)
            {
                coinsCounter.value = coins;
            }
        }

        public void SetFitbucks(int bucks)
        {
            if (fitBucksCounter != null)
            {
                fitBucksCounter.value = bucks;
            }
        }

        public void SetPlatinum(int platinum)
        {
            if (platinumCounter != null)
            {
                platinumCounter.value = platinum;
            }

            genericView.SetActive("btnPlatinum", PersistentCore.gameMode == GameMode.MainGame && platinum >= 0);
        }

        public void EnableMoneyButtons(bool value)
        {
            if (coinsCounter != null)
                coinsCounter.enableButton = value;

            if (fitBucksCounter != null)
                fitBucksCounter.enableButton = value;

            btnStyle.gameObject.SetActive(value);
        }

        public void HideBackButton()
        {
            if (btnBack.gameObject.activeSelf)
            {
                btnBack.onClick.RemoveAllListeners();
                UpdateWindowsModalMode ();
                ShowAll(true);
            }
        }

        public void ShowBackButton(Action onClick)
        {
            ShowAll(false);
            btnBack.onClick.RemoveAllListeners();
            botRightOn = true;
            if (onClick != null)
            {
                btnBack.onClick.AddListener
                (
                    delegate()
                    {
                        btnBack.onClick.RemoveAllListeners();
                        onClick();
                    }
                );
                btnBack.gameObject.SetActive(true);
            }
        }

        public void ShowAll(bool show = true)
        {
            if (coinsCounter != null)
                coinsCounter.gameObject.SetActive(show);
            if (fitBucksCounter != null)
                fitBucksCounter.gameObject.SetActive(show);

            btnBack.onClick.RemoveAllListeners();
            btnBack.gameObject.SetActive(!show);

            btnStorage.turnOn = show;

            //btnShop.gameObject.SetActive(show && !btnStorage_.IsActive());
            //btnSettings.gameObject.SetActive(show);

            enablerPromo.turnOn = show && hudMode != HUDMode.None;
            enablerJuiceBar.turnOn = show;
            enablerSportclub.turnOn = show;
            enablerFriends.turnOn = show && ((hudMode != HUDMode.None) && (hudMode != HUDMode.JuiceBar)); 
            enablerTrainers.turnOn = show;

            btnAchievementsEnabler.turnOn = show; 

            genericView.ShowAnimated("btnVisitorQuests", show && hudMode == HUDMode.Club);
            genericView.ShowAnimated("btnSportsmen", show && hudMode == HUDMode.SportClub);

            genericView.ShowAnimated("questsHolderClub", show && hudMode == HUDMode.Club);
            genericView.ShowAnimated("questsHolderSport", show && hudMode == HUDMode.SportClub);

            genericView.ShowAnimated("btnShopHud", show && (hudMode == HUDMode.Club || hudMode == HUDMode.SportClub));
            genericView.ShowAnimated("btnStorageHud", show && !wndMode && hudMode == HUDMode.JuiceBar);

            genericView.ShowAnimated("btnSettingsHud", show && hudMode != HUDMode.None);
            genericView.ShowAnimated("btnPromoHud", btnPromo.IsActive() && show && hudMode != HUDMode.None);

            //genericView.ShowAnimated("btnFriends", show && ((hudMode != HUDMode.None) && (hudMode != HUDMode.JuiceBar)));

            styleVisible = show && hudMode == HUDMode.Club;
            fitPointsVisible = show && hudMode == HUDMode.SportClub;

            btnLaneQuests.turnOn = ((show && (hudMode == HUDMode.SportClub)) || (btnLaneQuests_.gameObject.activeSelf && hudMode == HUDMode.None));
            btnSportQuests.turnOn = (show && (hudMode == HUDMode.SportClub) && canUseTimeQuestsGetter != null && canUseTimeQuestsGetter());
            btnDailyQuests.turnOn = (btnVisitorQuests.gameObject.activeSelf);
            btnDailyBonus.turnOn = (btnVisitorQuests.gameObject.activeSelf);

            btnStyleEnabler.turnOn = show;
        }

        /*
        public void CloseWindow()
        {
            btnShop.gameObject.SetActive(true);
            currentWindow = WindowTypes.None;
            SetWindowChangeView ();
        }
        */

        public void ShowMoney()
        {
            if (coinsCounter != null)
                coinsCounter.gameObject.SetActive(true);
            if (fitBucksCounter != null)
                fitBucksCounter.gameObject.SetActive(true);
        }
            
        public void BackgroundClick()
        {
            if (OnBackgroundClick != null) 
            {
                OnBackgroundClick();
            }
        }

        public void SetShopNewItems(int count)
        {
            if (count <= 0)
            {
                newItemsTxt.transform.parent.gameObject.SetActive(false); 
            }
            else
            {
                newItemsTxt.transform.parent.gameObject.SetActive(true);
                newItemsTxt.text = "!";//count.ToString();
            }
        }
            
        public void SetStorageNewItems(bool value)
        {
            //newStorageItemTxt.transform.parent.gameObject.SetActive(value);
        }

        //public void ShowDailyBonusHint()
        //{
        //    StartCoroutine(ShowDailyBonusHint_Coro());
        //}

        public void SetHUDMode(HUDMode mode)
        {
            if (mode != hudMode)
            {
                hudMode = mode;
                UpdateHUD();
            }
        }

        public void UpdateHUD()
        {
            UpdateWindowsModalMode();
            if (CustomDefaultView == null)
            {
                //Debug.LogFormat("HUD: set hud state to {0}. Custom hud mode is not setted.", hudMode);
                ShowAll(true);
            }
            else
            {
                //Debug.LogFormat("HUD: set hud state to custom.");
                CustomDefaultView.Invoke();
            }
        }

        void UpdateWindowsModalMode ()
        {
            var noWindow = !wndMode; //&& hudMode != HUDMode.None);
            var noModalWindow = !wndMode || !_modalMode; //&& hudMode != HUDMode.None;
            var noPersonPanel = !personPanel.SomePanelActiveAndEnabled;

            topLeftOn =     noWindow;
            botLeftOn =     noWindow;
            botRightOn =    noWindow;
            botCenterOn =   noModalWindow;
            rightOn =       noModalWindow && noPersonPanel;
        }

        public void OnPersonSelection()
        {
            var noModalWindow = ((!wndMode || !_modalMode) && hudMode != HUDMode.None);
            var noPersonPanel = !personPanel.SomePanelActiveAndEnabled;

            rightOn = noModalWindow && noPersonPanel;

            enablerJuiceBar.turnOn = true;
            enablerSportclub.turnOn = true;
        }

        public void SetTimeQuestsActivityGetter(Func<bool> getter)
        {
            canUseTimeQuestsGetter = getter;
        }
    }
}
