﻿using System;
using Data;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using View.UI;
using CustomAssets.Lists;

namespace UI.HUD.Repair
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Repair/Repair Panel")]
    public class RepairPanel : SlidingPanel
    {
        Button button;
        Image repairIcon;
        Text repairCostTxt;
        Image fitBucksIcon;
        Text repairBucksCostTxt;

        StorageIconsList.StorageItemDesc[] storageItems
        {
            get { return GUICollections.instance.storageIcons != null ? GUICollections.instance.storageIcons.items : null; }
        }

        void Awake()
        {
            button = transform.GetComponentInChild<Button>("Button");
            Assert.IsNotNull(button);
            repairIcon = transform.GetComponentInChild<Image>("Resource/Icon");
            Assert.IsNotNull(repairIcon);
            repairCostTxt = transform.GetComponentInChild<Text>("Resource/Txt");
            Assert.IsNotNull(repairCostTxt);
            fitBucksIcon = transform.GetComponentInChild<Image>("Button/TextLayout/FitBucksIcon");
            Assert.IsNotNull(fitBucksIcon);
            repairBucksCostTxt = transform.GetComponentInChild<Text>("Button/TextLayout/RepairBucksCostTxt");
            Assert.IsNotNull(repairBucksCostTxt);
        }

        public void SetData (Cost repairCost, int repairResourcesAvailable, Action onClick)
        {
            Assert.IsNotNull(repairCost);

            var useFitBucksCost = (repairResourcesAvailable < repairCost.value);

            if (repairCostTxt != null)
            {
                // TODO: Common formatting helper.
                repairCostTxt.text = string.Format("<color={0}>{1}</color>/{2}", repairResourcesAvailable < repairCost.value ? "#CA0000" : "#333333FF", repairResourcesAvailable, repairCost.value);
            }

            if (repairIcon != null)
            {
                var resTypeIdx = (int)repairCost.resourceType;
                if (resTypeIdx >= 0 && resTypeIdx < storageItems.Length)
                {
                    repairIcon.sprite = storageItems[resTypeIdx].sprite;
                }
            }

            if (button != null)
            {
                button.onClick.RemoveAllListeners();
                if (onClick != null)
                {
                    button.onClick.AddListener(delegate()
                        {
                            Hide();
                            if (onClick != null)
                                onClick();
                        }
                    );
                }
            }

            if (fitBucksIcon != null)
            {
                fitBucksIcon.gameObject.SetActive(useFitBucksCost);
            }

            if (repairBucksCostTxt != null)
            {
                repairBucksCostTxt.gameObject.SetActive(useFitBucksCost);
                if (useFitBucksCost)
                    repairBucksCostTxt.text = Cost.RecourcePriceInFitbucks(repairCost.resourceType).ToString();
            }
        }

        public override void Hide()
        {
            if (button != null)
            {
                button.onClick.RemoveAllListeners();
            }
            base.Hide();
        }
    }
}