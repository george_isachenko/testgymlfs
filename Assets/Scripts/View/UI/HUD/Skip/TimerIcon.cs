﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View;
using View.UI.OverheadUI;

namespace UI.HUD.Skip
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Skip/Timer Icon")]
    public class TimerIcon : MonoBehaviour
    {
        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<TimerIcon>
        {
            public Holder()
                : base()
            {
            }

            public Holder(TimerIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }
        #endregion

        #region Private data.
        protected DateTime endTime_;
        private Action onTimeout;

        protected Button button;
        protected Text timeText_;
        #endregion

        #region Unity API.
        protected void Awake()
        {
            button = GetComponent<Button>();
        }

        protected void Update()
        {
            PerformUpdate();
        }
        #endregion

        #region Public API.
        public void SetData(DateTime endTime, Action onClick, bool persistentClickHandler, Action onTimeout = null)
        {
            endTime_ = endTime;
            this.onTimeout = onTimeout;
            enabled = true;
            if (button != null)
            {
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(() =>
                {
                    if (!persistentClickHandler)
                    {
                        button.onClick.RemoveAllListeners();
                    }

                    onClick?.Invoke();
                });
            }
            PerformUpdate();
        }

        #endregion

        #region Protected properties.
        protected TimeSpan timeLeft { get { return (endTime_ == DateTime.MaxValue.ToUniversalTime()) ? TimeSpan.MaxValue : endTime_ - DateTime.UtcNow; } }
        protected int timeLeftNoFractions { get { return Mathf.RoundToInt((float)timeLeft.TotalSeconds); } }

        protected string timeValue
        {
            set
            {
                if (timeText_ == null)
                {
                    timeText_ = gameObject.GetComponentInSelfOrChild<Text>("DurationTxt");
                    if (timeText_ == null)
                        return;
                }

                timeText_.text = value;
                timeText_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }
        #endregion

        #region Protected functions.
        protected virtual void PerformUpdate()
        {
            if (endTime_ == DateTime.MaxValue.ToUniversalTime())
            {
                timeValue = "\u221e"; // Infinity char.
            }
            else if (timeLeftNoFractions > 0)
            {
                timeValue = PrintHelper.GetTimeString(TimeSpan.FromSeconds(timeLeftNoFractions));
            }
            else
            {
                timeValue = string.Empty;
                
                enabled = false;
                OnTimeout();
            }
        }

        protected void OnTimeout()
        {
            if (onTimeout != null)
            {
                var tmpCallback = onTimeout;
                onTimeout = null;
                tmpCallback();
            }
        }

        public virtual void Reset()
        {
            onTimeout = null;
            enabled = false;
            if (button != null)
            {
                button.onClick.RemoveAllListeners();
            }
        }
        #endregion
    }
} 
