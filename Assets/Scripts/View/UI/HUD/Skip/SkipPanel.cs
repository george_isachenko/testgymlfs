﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace UI.HUD.Skip
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Skip/Skip Panel")]
    public class SkipPanel : SlidingPanel
    {
        #region Private data.
        private SkipIcon skipIcon;
        private IEnumerator hideCheckJob;
        private bool ignoreEscapeButtonPress;
        private GameObject btnObj;

        #endregion

        #region Unity API.
        void Awake()
        {
            skipIcon = transform.GetComponent<SkipIcon>();
            Assert.IsNotNull(skipIcon);
            btnObj = transform.Find("SkipBtn").gameObject;
        }
            
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape) && !ignoreEscapeButtonPress)
            {
                Hide();
            }
        }

        #endregion

        #region Public API.
        public void Display
            ( TimeSpan totalTime
            , DateTime endTime
            , Func<int, int> getSkipCostCallback
            , Action onClick
            , Func<bool> onHideCheck = null
            , Func<bool> onEnableButtonCheckCallback = null
            , string skipName = "")
        {
            if (skipIcon != null)
            {
                skipIcon.SetData
                    ( totalTime
                    , endTime
                    , getSkipCostCallback
                    , () =>
                    {
                        Hide();
                        onClick?.Invoke();
                    }
                    , false
                    , Hide
                    , onEnableButtonCheckCallback );

                skipIcon.caption = skipName;
            }
            
            CancelHideCheckJob();

            Show();

            if (onHideCheck != null)
            {
                hideCheckJob = HideCheckJob(onHideCheck);
                StartCoroutine(hideCheckJob);
            }
        }

        public override void Hide()
        {
            if (isShown)
            {
                if (skipIcon != null)
                {
                    skipIcon.Reset();
                }

                CancelHideCheckJob();

                base.Hide();
            }
        }

        public void ShowButtonWithAnimation()
        {
            btnObj.transform.localScale = Vector3.zero;
            btnObj.transform.GetComponent<View.Actions.LeanTweenTransform>().RunTween(0);
        }
        #endregion

        #region Private functions.
        void CancelHideCheckJob()
        {
            if (hideCheckJob != null)
            {
                StopCoroutine(hideCheckJob);
                hideCheckJob = null;
            }
        }

        IEnumerator HideCheckJob(Func<bool> onHideCheck)
        {
            yield return null; // Skip first immediate call.

            if (onHideCheck != null)
            {
                yield return new WaitUntil(() => onHideCheck());
            }

            hideCheckJob = null;
            Hide();
            yield break;
        }
        #endregion

        public void SetIgnoreButtonHandlingMode(bool ignore)
        {
            ignoreEscapeButtonPress = ignore;
        }
    }
}