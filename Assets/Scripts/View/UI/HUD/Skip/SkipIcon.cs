﻿using System;
using Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View;

namespace UI.HUD.Skip
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Skip/Skip Icon")]
    public class SkipIcon : TimerIcon
    {
        #region Private data.
        private Func<int, int> getSkipCostCallback;
        private Func<bool> onEnableButtonCheckCallback;

        private Text costText_;
        private Text buttonText_;
        private Image fitBucksIcon_;
        private Slider durationBar_;
        private Text activityName_;

        private TimeSpan totalTime_;
        #endregion

        #region Unity API.
        new void Awake()
        {
            base.Awake();

            if (button == null)
                button = gameObject.GetComponentInChild<Button>("SkipBtn");
            fitBucksIcon_ = gameObject.GetComponentInChild<Image>("FitBucksIcon");
            durationBar_ = gameObject.GetComponentInChild<Slider>("Duration");
            activityName_ = gameObject.GetComponentInChild<Text>("ActivityName");
        }


        #endregion

        #region Public properties
        public string caption { set { activityName_.text = value; } }
        #endregion

        #region Public API.
        public void SetData
            ( TimeSpan totalTime
            , DateTime endTime
            , Func<int, int> getSkipCostCallback
            , Action onClick
            , bool persistentClickHandler
            , Action onTimeout = null
            , Func<bool> onEnableButtonCheckCallback = null)
        {
            this.getSkipCostCallback = getSkipCostCallback;
            this.onEnableButtonCheckCallback = onEnableButtonCheckCallback;
            totalTime_ = totalTime;

            base.SetData(endTime, onClick, persistentClickHandler, onTimeout);
        }
        #endregion

        #region Private properties.
        string costValue
        {
            set
            {
                if (costText_ == null)
                {
                    costText_ = gameObject.GetComponentInSelfOrChild<Text>("SkipCostTxt");
                    if (costText_ == null)
                        return;
                }

                costText_.text = value;
                costText_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }

        string buttonText
        {
            set
            {
                if (buttonText_ == null)
                {
                    buttonText_ = gameObject.GetComponentInSelfOrChild<Text>("SkipTxt");
                    if (buttonText_ == null)
                        return;
                }

                buttonText_.text = value;
                buttonText_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }
        #endregion

        #region Private functions.
        protected override void PerformUpdate()
        {
            if (timeLeft == TimeSpan.MaxValue || timeLeftNoFractions > 0)
            {
                if (getSkipCostCallback != null)
                {
                    var cost = getSkipCostCallback(timeLeftNoFractions);

                    if (cost > 0 && timeLeft != TimeSpan.MaxValue)
                    {
                        buttonText = Loc.Get("idSkip");
                        costValue = cost.ToString();
                        fitBucksIcon_.gameObject.SetActive (true);
                    }
                    else
                    {
                        buttonText = Loc.Get("idFree");
                        costValue = string.Empty;
                        fitBucksIcon_.gameObject.SetActive (false);
                    }

                    if (button != null)
                    {
                        //button.interactable = (cost >= 0);
                        if (onEnableButtonCheckCallback != null)
                        {
                            button.interactable = onEnableButtonCheckCallback();
                        }

                        button.gameObject.SetActive(cost >= 0);
                    }
                }

                timeValue = (timeLeft == TimeSpan.MaxValue)
                    ? "\u221e" // Infinity char.
                    : PrintHelper.GetTimeString(TimeSpan.FromSeconds(timeLeftNoFractions));
            }
            else
            {
                costValue = string.Empty;
                timeValue = string.Empty;
                
                enabled = false;
                OnTimeout();
            }

            UpdateProgresssBar();
        }

        void UpdateProgresssBar()
        {
            if (timeLeft == TimeSpan.MaxValue ||
                totalTime_ == TimeSpan.MaxValue || 
                totalTime_.TotalSeconds <= 0 ||
                timeLeft > totalTime_)
            {
                durationBar_.value = 0;
            }
            else
            {
                durationBar_.value = (float)(1.0f - (timeLeft.TotalSeconds / totalTime_.TotalSeconds));
            }
        }

        public override void Reset()
        {
            base.Reset();
            getSkipCostCallback = null;
        }
        #endregion
    }
} 
