﻿using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;
using View.UI.HUD;

namespace UI.HUD.ExerciseSelector
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Exercise Selector/Exercise Selector Icon")]
    public class ExerciseSelectorIcon : UIGenericMonoBehaviour
    {
        #region Public (Inspector fields).
        #endregion

        #region Public properties.
        public Sprite spriteExerciseIcon        { set { genericView.SetSprite("ExerciseIcon",       value); } }
        public Sprite spriteExerciseLevelIcon   { set { genericView.SetSprite("ExerciseLevelIcon",  value); } }
        public string txtDuration               { set { genericView.SetText("Duration",             value); } }
        public StatsGrid statsGrid              { get { return genericView.GetComponent<StatsGrid>("StatsGrid"); } }
        public StarsIcon stars                  { get { return genericView.GetComponent<StarsIcon>("Stars"); } }
        public UIGenericViewItem itemButton     { get { return genericView.GetComponent<UIGenericViewItem>("Button"); } }
        public UIGenericViewItem itemHideButton { get { return genericView.GetComponent<UIGenericViewItem>("HideButton"); } }
        public bool activeHideButton            { set { genericView.SetActive("HideButton", value); } }
        
        public bool activeMode
        {
            set
            {
                itemButton.GetComponent<Button>().interactable = value;

                activeHideButton = !value;

                var outlines = itemButton.GetComponentsInChildren <Outline>();
                foreach (var outl in outlines)
                    outl.enabled = value;
            }
        }
        #endregion

        #region Unity API.
        private void Awake()
        {
        }
        #endregion
    }
}
