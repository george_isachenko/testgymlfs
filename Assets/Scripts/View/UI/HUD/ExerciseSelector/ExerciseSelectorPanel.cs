﻿using System;
using CustomAssets.Lists;
using Data;
using Data.Estate;
using Data.Estate.Upgrades;
using UnityEngine;
using UnityEngine.Assertions;
using View;
using View.PrefabResources;
using View.UI;

namespace UI.HUD.ExerciseSelector
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Exercise Selector/Exercise Selector Panel")]
    public class ExerciseSelectorPanel : SlidingPanel
    {
        #region Public properties.
        public string txtWorkstationName        { set { genericView.SetText("WorkstationName",      value); } }
        public string txtSelectClientMessage    { set { genericView.SetText("SelectClientMessage",  value); } }
        public bool activeSelectClientMessage   { set { genericView.SetActive("SelectClientMessage",  value); } }
        public ExerciseSelectorIcon exercise1   { get { return genericView.GetComponent<ExerciseSelectorIcon>("Exercise1"); } }
        public ExerciseSelectorIcon exercise2   { get { return genericView.GetComponent<ExerciseSelectorIcon>("Exercise2"); } }
        public ExerciseSelectorIcon exercise3   { get { return genericView.GetComponent<ExerciseSelectorIcon>("Exercise3"); } }
        public UpgradeEquipmentIcon upgrade     { get { return genericView.GetComponent<UpgradeEquipmentIcon>("UpgradeButton"); } }
        #endregion

        #region Public events.
        public event Action exercise1SelectHandler
        {
            add { exercise1.itemButton.SetButtonCallback(value); }
            remove { exercise1.itemButton.ResetButtonCallback(); }
        }

        public event Action exercise2SelectHandler
        {
            add { exercise2.itemButton.SetButtonCallback(value); }
            remove { exercise2.itemButton.ResetButtonCallback(); }
        }

        public event Action exercise3SelectHandler
        {
            add { exercise3.itemButton.SetButtonCallback(value); }
            remove { exercise3.itemButton.ResetButtonCallback(); }
        }

        public event Action upgradeButtonHandler
        {
            add { upgrade.itemButton.SetButtonCallback(value); }
            remove { upgrade.itemButton.ResetButtonCallback(); }
        }
        #endregion

        #region Indexers.
        public ExerciseSelectorIcon this[int index]
        {
            get
            {
                return GetExerciseSelectorIcon(index);
            }
        }
        #endregion

        #region Private properties.
        Sprite[] exerciseSprites
        {
            get { return GUICollections.instance.exerciseSpritesList != null ? GUICollections.instance.exerciseSpritesList.array : null; }
        }
        #endregion

        #region Unity API.
        void Awake()
        {
        }

        protected new void Start ()
        {
            base.Start();

            exercise1.itemHideButton.SetButtonCallback(OnHideButtonClick);
            exercise2.itemHideButton.SetButtonCallback(OnHideButtonClick);
            exercise3.itemHideButton.SetButtonCallback(OnHideButtonClick);
        }
        #endregion

        #region Public API.
        public ExerciseSelectorIcon  GetExerciseSelectorIcon  ( int idx )
        {
            switch (idx)
            {
                case 0: return exercise1;
                case 1: return exercise2;
                case 2: return exercise3;
                default: return null;
            }
        }

        public void UpdateIcons(EstateType estateType)
        {
            Assert.IsNotNull(estateType);

            activeSelectClientMessage = false;

            txtWorkstationName = Loc.Get(estateType.locNameId);

            ExerciseSelectorIcon icon;
            for (int i = 0; (icon = GetExerciseSelectorIcon(i)) != null; i++)
            {
                if (i < estateType.exercises.Length && estateType.exercises[i] != null)
                {
                    if (!icon.gameObject.activeSelf)
                        icon.gameObject.SetActive(true);

                    UpdateIcon(icon, estateType, estateType.exercises[i], exerciseSprites);
                }
                else
                {
                    icon.gameObject.SetActive(false);
                }
            }
        }

        public void SetActiveMode(bool value)
        {
            ExerciseSelectorIcon icon;
            for (int i = 0; (icon = GetExerciseSelectorIcon(i)) != null; i++)
            {
                icon.activeMode = value;
            }
            activeSelectClientMessage = false;
        }

        public void SetUpgradeStates (UpgradeAvailabilityState state, int currentUpgradeLevel, int nextUpgradeProfileLevel)
        {
            upgrade.SetStates(state, currentUpgradeLevel, nextUpgradeProfileLevel);
        }
        #endregion

        #region Private static API.
        private static void UpdateIcon (ExerciseSelectorIcon icon, EstateType estateType, Exercise exercise, Sprite[] exerciseSprites)
        {
            Assert.IsNotNull(icon);
            Assert.IsNotNull(estateType);
            Assert.IsNotNull(exercise);

            TextIconsLibrary textIconsLibrary = GUICollections.instance.textIconsSmallLibrary;
            Assert.IsNotNull(textIconsLibrary);

            if (exerciseSprites != null &&
                exercise.icon >= 0 &&
                exercise.icon < exerciseSprites.Length)
            {
                icon.spriteExerciseIcon = exerciseSprites[exercise.icon];
            }

            icon.txtDuration = PrintHelper.GetShortTimeString(exercise.duration);
            
            // icon.SetExerciseHeaderText(i, Loc.Get(exercise.locNameId));

            //var statValue = eqDataExercise.GetMaxStatProfit (i);
            var statValues = exercise.stats.GetSorted();

            {
                var stat0 = icon.statsGrid[0];
                if (!stat0.gameObject.activeSelf)
                    stat0.gameObject.SetActive(true);
                stat0.spriteStatIcon = textIconsLibrary.GetStatIcon (statValues[0].statIndex);
                stat0.txtStatAmount = statValues[0].statValue.ToString ();
            }

            {
                var stat1 = icon.statsGrid[1];
                if (statValues[1].statValue > 0)
                {
                    if (!stat1.gameObject.activeSelf)
                        stat1.gameObject.SetActive(true);
                    stat1.spriteStatIcon = textIconsLibrary.GetStatIcon (statValues[1].statIndex);
                    stat1.txtStatAmount = statValues[1].statValue.ToString ();
                }
                else
                {
                    stat1.gameObject.SetActive(false);
                }
            }
        }
        #endregion

        #region Private API.
        private void OnHideButtonClick ()
        {
            activeSelectClientMessage = true;
        }
        #endregion
    }
}
