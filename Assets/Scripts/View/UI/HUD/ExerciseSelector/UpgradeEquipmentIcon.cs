﻿using Data.Estate.Upgrades;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;
using View.UI.HUD;

namespace UI.HUD.ExerciseSelector
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Exercise Selector/Upgrade Equipment Icon")]
    public class UpgradeEquipmentIcon : UIGenericMonoBehaviour
    {
        #region Public (Inspector fields).
        #endregion

        #region Public properties.
        public UIGenericViewItem itemButton             { get { return genericView.GetComponent<UIGenericViewItem>("Button"); } }
        #endregion

        #region Private properties.
        protected bool activeUpgradeAvailable           { set { genericView.SetActive("UpgradeAvailable", value); } }
        protected bool activeUpgradeLocked              { set { genericView.SetActive("UpgradeLocked", value); } }
        protected bool activeUpgradeFull                { set { genericView.SetActive("UpgradeFull", value); } }
        protected StarsIcon stars                       { get { return genericView.GetComponent<StarsIcon>("Stars"); } }
//      protected string txtUpgradeLevel                { set { genericView.SetText("UpgradeLevel", value); } }
        protected string txtLevelRequired               { set { genericView.SetText("LevelRequired", value); } }
        #endregion

        #region Unity API.
        private void Awake()
        {
        }
        #endregion

        #region Public methods.
        public void SetStates (UpgradeAvailabilityState state, int currentUpgradeLevel, int nextUpgradeProfileLevel)
        {
            if (state == UpgradeAvailabilityState.FeatureUnavailable)
            {
                gameObject.SetActive(false);
            }
            else
            {
                if (!gameObject.activeSelf)
                    gameObject.SetActive(true);

                activeUpgradeLocked = (state == UpgradeAvailabilityState.EquipmentLocked || state == UpgradeAvailabilityState.FeatureAvailable);
                activeUpgradeAvailable = (state == UpgradeAvailabilityState.EquipmentAvailable);
                activeUpgradeFull = (state == UpgradeAvailabilityState.EquipmentFull);

                stars.level = currentUpgradeLevel;

                txtLevelRequired = nextUpgradeProfileLevel.ToString();
            }
        }
        #endregion
    }
}
