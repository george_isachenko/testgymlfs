﻿using UnityEngine;
using UnityEngine.Events;
using View.UI.Base;

namespace UI.HUD
{
    [DisallowMultipleComponent]
    public abstract class SlidingPanel : UIGenericMonoBehaviour
    {
        #region Public (Inspector) fields.
        public UnityEvent onShow;
        public UnityEvent onHide;
        #endregion

        #region Private data.
        bool shown = false;
        SlidingPanel[] siblings;
        #endregion

        public bool isShown
        {
            get { return shown; }
        }

        protected void Start()
        {
            gameObject.SetActive(false);

            if (transform.parent != null)
            {
                siblings = transform.parent.gameObject.GetComponentsInChildren<SlidingPanel>();
            }
        }

        public virtual void Show()
        {
            if (!shown)
            {
                shown = true;

                // Hide all other SlidingPanels except self.
                if (siblings != null && siblings.Length > 0)
                {
                    foreach (var sibling in siblings)
                    {
                        if (!ReferenceEquals(sibling, this))
                        {
                            sibling.Hide();
                        }
                    }
                }

                gameObject.SetActive(true);

                if (onShow != null)
                    onShow.Invoke();
            }
        }

        public virtual void Hide()
        {
            if (shown)
            {
                shown = false;
                if (onHide != null)
                    onHide.Invoke();
            }
        }
    }
}
