﻿using UnityEngine;

namespace View.UI.HUD.VisitorPanel
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Visitor Panel/Visitor Panel Icon")]
    public class HUDVisitorPanelIcon : HUDGenericCharacterPanelIcon 
    {
    }
}
