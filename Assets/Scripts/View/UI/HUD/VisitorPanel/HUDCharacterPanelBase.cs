﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using UnityEngine.Assertions;
using View.Actors;
using UnityEngine.UI;

namespace View.UI.HUD.VisitorPanel
{
    public class HUDCharacterPanelBase : UIGenericMonoBehaviour 
    {
        public CharacterPortrait    portrait                    { set { SetPortrait(value); } }
        public string               personName                  { set { genericView.SetText("personName", value); } get { return genericView.GetComponent<Text>("personName").text; } }
        public Color                bgColor                     { set { bgImage.color = value; } get { return bgImage.color; } }

        protected CharacterPortrait portraitObject;
        protected Image             bgImage;
        private Transform portraitHolder;

        protected void Awake()
        {
            portraitHolder = transform.Find("PortraitHolder");
            Assert.IsNotNull(portraitHolder);
        }

        void Start()
        {
            gameObject.SetActive(false);
        }

        public void OnCharacterDeselected()
        {
            if (portraitObject != null)
            {
                portraitObject.Hide();
                portraitObject = null;
            }
            gameObject.SetActive(false);
        }

        void SetPortrait(CharacterPortrait portrait)
        {
            if (portraitHolder != null)
            {
                if (this.portraitObject != null)
                {
                    this.portraitObject.Hide();
                    this.portraitObject = null;
                }

                //Bounds portraitHeadBounds;
                if (portrait != null)
                {
                    portrait.Show(portraitHolder);
                    this.portraitObject = portrait;
                }
            }
        }
    }
}
