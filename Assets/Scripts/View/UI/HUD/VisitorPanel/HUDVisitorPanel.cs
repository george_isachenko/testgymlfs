﻿using Core;
using CustomAssets.Lists;
using Data.Person;
using Data.Quests;
using Logic.Quests;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Data.Person.Info;
using View.UI.Base;
using Logic.Persons;
using Presentation.Base;
using Data.Estate.Info;
using Data.Estate;
using Logic.Quests.Visitor;
using Data.Quests.Visitor;
using System;
using Logic.Quests.Base;

namespace View.UI.HUD.VisitorPanel
{
    public delegate void OnKickRequest();
    public delegate void OnClaimRequest();

    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Visitor Panel/Visitor Panel")]
    [RequireComponent(typeof(UIGenericView))]
    public class HUDVisitorPanel : HUDCharacterPanelBase
    {
        #region Protected properties.
        protected QuestRewardView questRewardView       { get { return genericView.GetComponent<QuestRewardView>("reward"); } }
        #endregion

        public Action OnKickRequest     { set { genericView.SetButtonCallback("btnKick", value); } }
        public Action OnClaimRequest    { set { genericView.SetButtonCallback("btnClaim", value); } }
        public Action OnAssignTrainer   { set { 
                genericView.SetButtonCallback("btnAssignTrainer", value); 
                genericView.SetButtonCallback("btnTrainerNotAvailable", value); 
                genericView.SetButtonCallback("btnNotTrainerWithSkill", value); 
            } }

        public bool btnKickOn               { set { genericView.SetActive("btnKick", value); } }
        public bool btnClaimOn              { set { genericView.SetActive("btnClaim", value); } }
        public bool btnAssignTrainerOn      { set { genericView.SetActive("btnAssignTrainer", value); } }
        public bool workingWithTrainerOn    { set { genericView.SetActive("workingWithTrainer", value); } }
        public bool btnTrainerNotAvailable  { set { genericView.SetActive("btnTrainerNotAvailable", value); } }
        public bool btnNotTrainerWithSkill  { set { genericView.SetActive("btnNotTrainerWithSkill", value); } }

        public Color enabledColor;
        public Color emptyColor;

        public QuestEstimatedRewards    rewards    { set { questRewardView.SetRewards(value); } }
        public HUDVisitorPanelIcon[] tasks = new HUDVisitorPanelIcon[3];

        BasePresentationFacade  parentFacade;

        new void Awake()
        {
            bgImage = GetComponent<Image>();

            tasks[0] = genericView.GetComponent<HUDVisitorPanelIcon>("Task1");
            tasks[1] = genericView.GetComponent<HUDVisitorPanelIcon>("Task2");
            tasks[2] = genericView.GetComponent<HUDVisitorPanelIcon>("Task3");

            foreach(var task in tasks)
            {
                task.enabledColor = enabledColor;
                task.emptyColor = emptyColor;
            }
                
            base.Awake();
        }

        void OnDestroy()
        {
            if (parentFacade != null)
            {
                parentFacade.equipmentInteractionProvider.onEquipmentSelected -= OnEquipmentSelected;
                parentFacade.equipmentInteractionProvider.onEquipmentDeselected -= OnEquipmentDeselected;

                parentFacade = null;
            }
        }

        public void Init
            ( BasePresentationFacade parentFacade)
        {
            Assert.IsNotNull(parentFacade);
            if (parentFacade != null)
            {
                this.parentFacade = parentFacade;

                parentFacade.equipmentInteractionProvider.onEquipmentSelected += OnEquipmentSelected;
                parentFacade.equipmentInteractionProvider.onEquipmentDeselected += OnEquipmentDeselected;
            }

            btnAssignTrainerOn = !Logic.DebugHacks.disableTrainers;
        }
    
        void OnEquipmentSelected (int id, IEquipmentInfo equipmentInfo)
        {
            if (parentFacade.characterInteractionProvider.selectedCharacterId != PersonData.InvalidId)
            {
                var visitorQuest = parentFacade.questsInteractionProvider.GetVisitorQuest(parentFacade.characterInteractionProvider.selectedCharacterId);
                if (visitorQuest != null)
                {
                    UpdateQuestRewards(visitorQuest);
                }
            }
        }

        void OnEquipmentDeselected (int id)
        {
            if (parentFacade.characterInteractionProvider.selectedCharacterId != PersonData.InvalidId)
            {
                var visitorQuest = parentFacade.questsInteractionProvider.GetVisitorQuest(parentFacade.characterInteractionProvider.selectedCharacterId);
                if (visitorQuest != null)
                {
                    UpdateQuestRewards(visitorQuest);
                }
            }
        }

        private void UpdateQuestRewards (VisitorQuestLogic visitorQuest)
        {
            IEquipmentInfo selectedEquipmentInfo = null;
            if (parentFacade.equipmentInteractionProvider.selectedEquipmentId != EquipmentData.InvalidId)
            {
                var equipmentActor = parentFacade.equipmentInteractionProvider.GetEquipmentActor(parentFacade.equipmentInteractionProvider.selectedEquipmentId);
                if (equipmentActor != null)
                    selectedEquipmentInfo = equipmentActor.equipmentInfo;
            }

            questRewardView.SetRewards(visitorQuest.CalculateEstimatedRewards(selectedEquipmentInfo));
        }
    }
}
