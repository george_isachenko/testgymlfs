﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using View.UI.HUD.VisitorPanel;

namespace View.UI.HUD
{
    public class HUDPersonPanel : UIGenericMonoBehaviour 
    {
        public HUDVisitorPanel visitor  { get; private set; }
        public HUDTrainerPanel trainer  { get; private set; }

        public bool SomePanelActiveAndEnabled   { get { return visitor.isActiveAndEnabled || trainer.isActiveAndEnabled; } }

        void Awake()
        {
            visitor = GetComponentInChildren<HUDVisitorPanel>(true);
            trainer = GetComponentInChildren<HUDTrainerPanel>(true);

            visitor.gameObject.SetActive(true);
            trainer.gameObject.SetActive(true);
        }

        public void ShowVisitor()
        {
            genericView.IsolateSubView("visitor");
        }

        public void ShowTrainer()
        {
            genericView.IsolateSubView("trainer");
        }

        public void OnCharacterDeselected()
        {
            visitor.OnCharacterDeselected();
            trainer.OnCharacterDeselected();
        }
    }
}