﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using System;
using UnityEngine.UI;
using Helpers;
using UnityEngine.Assertions;

namespace View.UI.HUD.VisitorPanel
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Visitor Panel/Trainer Panel")]
    [RequireComponent(typeof(UIGenericView))]
    public class HUDTrainerPanel : HUDCharacterPanelBase
    {

        const int maxNameSymbols = 18;  

        public Action   OnOpenTrainersView          { set { genericView.SetButtonCallback("btnTrainersView", value); } }
        public Action   OnLevelUpClick              { set { genericView.SetButtonCallback("btnLevelUp", value); } }
        public Action   OnInfoClick                 { set { genericView.SetButtonCallback("btnInfo", value); } }

        public string   level                   { set { genericView.SetText("level", value); } }
        public bool     levelUpOn               { set { genericView.SetActive("btnLevelUp", value); } }
        public string   expTxt                  { set { genericView.SetText("expTxt", value); } }
        public float    expProgress             { set { genericView.SetSliderValue("expProgress", value); } }

        [HideInInspector] public TrainerClientInfoView[]  clients = new TrainerClientInfoView[4];
        public GameObject clientSlotPrefab;

        new void Awake()
        {
            base.Awake();

            var clientsHolder = genericView.GetTransform("clientsHolder");
            TrainersViewItem.CreateClientSlots(clientsHolder, clientSlotPrefab, genericView, clients);
            bgImage = transform.Find("Panel").GetComponent<Image>();
        }
    }
}