﻿using UnityEngine;
using View.UI.Base;

namespace View.UI.HUD
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/HUD/Stat Icon")]
    public class StatIcon : UIGenericMonoBehaviour
    {
        #region Private properties.
        public Sprite   spriteStatIcon      { set { genericView.SetSprite("StatIcon", value); } }
        public bool     activeStatIcon      { set { genericView.SetActive("StatIcon", value); } }
        public string   txtStatAmount       { set { genericView.SetText("StatAmount", value); } }
        public bool     activeStatAmount    { set { genericView.SetActive("StatAmount", value); } }
        #endregion
    }
}