﻿using CustomAssets.Lists;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI.HUD.VisitorPanel
{
    [DisallowMultipleComponent]
    public abstract class HUDGenericCharacterPanelIcon : UIGenericMonoBehaviour 
    {
        #region Public fields.
        public Color enabledColor;
        public Color emptyColor;
        #endregion

        #region Protected data.
        protected Image       _background; 
        protected SpritesList _tierIcons;
        #endregion

        #region Protected properties
        protected Sprite  taskIconSprite  { set { genericView.SetSprite("TaskIcon", value); } }
        protected bool    taskIconActive  { set { genericView.SetActive("TaskIcon", value); } }

        protected string  countText   { set { genericView.SetText("Count", value); } }
        protected bool    countActive { set { genericView.SetActive("Count", value); } }

        protected bool    doneIconActive { set { genericView.SetActive("Done", value); } }

        protected Sprite  imgTierSprite  { set { genericView.SetSprite("ImgTier", value); } }
        protected bool    imgTierActive  { set { genericView.SetActive("ImgTier", value); } }
        #endregion

        #region Public properties.
        public virtual Sprite   exerciseSprite  { set { taskIconSprite = value; _background.color = enabledColor;} }
        public virtual bool     done            { set { doneIconActive = value; countActive = !value; } }
        public virtual string   txtCount        { set { countText = value; } }

        public virtual bool empty
        {
            set
            {
                taskIconActive = !value;
                imgTierActive = !value;
                countActive = !value;
                doneIconActive = !value;
                _background.color = emptyColor;
            }
        }

        public virtual int tier
        {
            set
            {
                if (_tierIcons == null)
                {
                    _tierIcons = GUICollections.instance.exerciseLevelSpritesList;
                }

                if (value >= 0 && value <= _tierIcons.array.Length)
                {
                    imgTierSprite = _tierIcons.array[value];
                    imgTierActive = (value != 0);
                }
                else
                    Debug.Log("Wrong Tier index");
            }
        }
        #endregion

        #region Unity API.
        protected void Awake()
        {
            _background = transform.GetComponent<Image> ();
        }

        protected void Start()
        {
            _tierIcons = GUICollections.instance.exerciseLevelSpritesList;
        }
        #endregion
    }
}
