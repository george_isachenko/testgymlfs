﻿using UnityEngine;
using UnityEngine.UI;
using View.PrefabResources;
using View.UI;
using View.UI.Shop;
using View.UI.Base;

public class LevelUpUnlockItem : UIBaseViewListItem
{
    #region Public fields.
    public  GameObject          wallFloorItemPrefab;
    #endregion

    #region Private fields.
    GameObject                  expand;
    GameObject                  capacity;
    GameObject                  item3d;
    GameObject                  itemWallFloor;
    ShopScrollViewItemWallFloor wallFloor;
    WallFloorTextures           wallFloorTextures;
    GameObject                  coinsGO;
    GameObject                  resourcesGO;
    #endregion

    public string   prefabName      { set { HideAll(); Load3dPrefab(value); } }
    public int      wallFloorIdx    { set { HideAll(); SetWallFloor(value); } }
    public bool     expandOn        { set { HideAll(); expand.SetActive(value); } }
    public bool     capacityOn      { set { HideAll(); capacity.SetActive(value); } }
    public int      coins           { set { HideAll(); SetCoins(value); } }

    new void Awake()
    {
        wallFloorTextures = GUICollections.instance.wallFloorTextures;
        expand              = transform.Find("Expand").gameObject;
        capacity            = transform.Find("Capacity").gameObject;
        coinsGO             = transform.Find("Coins").gameObject;
        resourcesGO = transform.Find("Resources").gameObject;
    }

    void HideAll()
    {
        expand.SetActive(false);
        capacity.SetActive(false);
        coinsGO.SetActive(false);
        resourcesGO.SetActive(false);

        if (item3d != null)
            item3d.SetActive(false);
        
        if (itemWallFloor != null)
            itemWallFloor.SetActive(false);
    }

    private void SetCoins(int value)
    {
        coinsGO.SetActive(true);
        coinsGO.transform.Find("CoinsTxt").GetComponent<Text>().text = "+" + value;
    }

    public void SetResource(Sprite icon, int count)
    {
        HideAll();

        resourcesGO.SetActive(true);
        resourcesGO.GetComponent<Image>().sprite = icon;
        resourcesGO.transform.GetComponentInChildren<Text>().text = "+" + count;
    }

    void SetWallFloor(int idx)
    {
        if (itemWallFloor == null)
        {
            itemWallFloor = Instantiate(wallFloorItemPrefab);
            itemWallFloor.transform.SetParent(transform, false);
            itemWallFloor.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
            itemWallFloor.name = "WallFloor";
            wallFloor = itemWallFloor.GetComponent<ShopScrollViewItemWallFloor>();
        }

        itemWallFloor.SetActive(true);

        
        if (wallFloorTextures != null)
        {
            if (idx >= 0 && idx < wallFloorTextures.array.Length)
            {
                var item = wallFloorTextures.array[idx];
                wallFloor.SetSprites(item.wall, item.floor);
            }
            else
                Debug.LogError("LevelUpUnlock WallFloor index out of range");
        }
        else
            Debug.LogError("Can't set wallFloor item for LevelUpUnlock");
    }

    void Load3dPrefab(string itemPrefab)
    {
        if (itemPrefab.Length > 0)
        {
            var prefab = Resources.Load<GameObject>("UI/EquipmentUI3dPrefabs/" + itemPrefab);
            if (prefab == null)
            {
                Debug.LogError("LevelUpUnlockItem : can't load prefab from resources");
                return;
            }

            if (item3d != null)
            {
                DestroyObject(item3d);
            }

            item3d = Instantiate(prefab);
            item3d.SetActive(true);
            item3d.transform.SetParent(transform, false);
            item3d.transform.localScale = new Vector3(0.6f,0.6f,0.6f);
        }
        else
            Debug.LogError("LevelUpUnlockItem : no prefab name");
    }

}
