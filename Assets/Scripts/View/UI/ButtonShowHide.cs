﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonShowHide : MonoBehaviour
{
    private Image imgArrow;
    private Button _btn;
    private bool _show = true;

    public float tweenTime = 0.2f;

    public bool show
    {
        get
        {
            return _show;
        }

        set
        {
            _show = value;
            if (imgArrow != null)
            {
                imgArrow.transform.rotation = Quaternion.Euler(0, 0, !_show ? -180 : 0);
            }
        }
    }
    public Button btn
    {
        get
        {
            return _btn;
        }
    }

    void Awake()
    {
        imgArrow = transform.Find("ImgArrow").GetComponent<Image>();

        _btn = gameObject.GetComponent<Button>();
        _btn.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        ShowHide();
        _show = !_show;
    }

    void ShowHide()
    {
        if (_show)
            LeanTween.rotate(imgArrow.gameObject, new Vector3(0, 0, -180), tweenTime);
        else
            LeanTween.rotate(imgArrow.gameObject, new Vector3(0, 0, 0), tweenTime);
    }
}
