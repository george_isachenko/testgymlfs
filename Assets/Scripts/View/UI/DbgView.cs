﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Logic;
using Logic.PlayerProfile.Events;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using View.UI.Base;
using Logic.Serialization;

namespace View.UI
{
    public class DbgView : UIGenericMonoBehaviour,
        IDragHandler,
        IBeginDragHandler
    {
        public event Action onHide;

        public event Action<int, int> onAddMoneyEvent;
        public event Action onPerformSaveGameDelete;

        public Action OnDropAchievementsCallback        { set { genericView.SetButtonCallback("btnDropAchievements", value); } }
        public Action OnSyncAchievementsCallback        { set { genericView.SetButtonCallback("btnSyncAchievements", value); } }

        public Button btnUnlocks { get; private set; }
        public Button btnDeleteSave { get; private set; }
        public Button btnZeroMoney { get; private set; }
        public Button btnNewDay { get; private set; }
        public Button btnOneMinuteLeft { get; private set; }
        public Button btnGetExpToNexLevel { get; private set; }
        public Button btnPushTest { get; private set; }
        public Button btnSkipLaneQuest { get; private set; }
        public Button btnAddSportsman { get; private set; }
        public Button btnRateUs { get; private set; }
        public Button btnGameMode { get; private set; }
        public Button btnGooglePlay { get; private set; }

        public IncDecControl profileLvl { get; private set; }
        public IncDecControl fatigue { get; private set; }
        public IncDecControl resources { get; private set; }

        public Slider touchSenseSlider { get; private set; }
        public Slider touchRotateSlider { get; private set; }
        public Slider touchZoomSlider { get; private set; }
        public Slider moveInertSlider { get; private set; }
        public Slider zoomInertSlider { get; private set; }
        public Slider rotationInertSlider { get; private set; }
        public Slider exerciseTimeOverrideSlider { get; private set; }
        public Toggle toggleNoSave { get; private set; }
        public Toggle toggleUseAnyEquipment { get; private set; }
        public Toggle toggleAutoPlay { get; private set; }
        public Toggle fastEquipmentBreakage { get; private set; }
        public Dropdown resourceTypeSelector { get; private set; }

        public InputField inputAddCoins { get; private set; }
        public InputField inputAddFitBucks { get; private set; }

        public Text adTimerLeft { get; private set;}

        static public DbgView instance { get; private set;}

        #region local notifications

        public Button resetNotificationsToDefault
        {
            get { return genericView.GetComponent<Button>("resetNotificationsToDefaultBtn"); }
        }

        private Transform dailyQuestNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_dailyQuestNotification"); }
        }

        private Transform equipAssemblyNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_equipAssemblyNotification"); }
        }

        private Transform visitorFininshExerciseNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_visitorFininshExerciseNotification"); }
        }

        private Transform resourceCooldownFinishNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_resourceCooldownFinishNotification"); }
        }

        private Transform laneQuestCooldownFinishNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_laneQuestCooldownFinishNotification"); }
        }

        private Transform laneQuestPrepareFinishNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_laneQuestPrepareFinishNotification"); }
        }

        private Transform sportsmanFinishExerciseNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_sportsmanFinishExerciseNotification"); }
        }

        private Transform dailyActiveNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_dailyActiveNotification"); }
        }
        private Transform roomUnlockNotificationTransform
        {
            get { return genericView.GetTransform("notificationPanel_roomUnlockedNotification"); }
        }

        public bool isDailyQuestNotificationEnabled
        {
            get { return dailyQuestNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }
        public bool isRoomUnlockActiveNotificationEnabled
        {
            get { return roomUnlockNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isEquipAssemblyNotificationEnabled
        {
            get { return equipAssemblyNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isVisitorFininshExerciseNotificationEnabled
        {
            get { return visitorFininshExerciseNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isResourceCooldownFinishNotificationEnabled
        {
            get { return resourceCooldownFinishNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isLaneQuestCooldownFinishNotificationEnabled
        {
            get { return laneQuestCooldownFinishNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isLaneQuestPrepareFinishNotificationEnabled
        {
            get { return laneQuestPrepareFinishNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isSportsmanFinishExerciseNotificationEnabled
        {
            get { return sportsmanFinishExerciseNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public bool isDailyActiveNotificationEnabled
        {
            get { return dailyActiveNotificationTransform.GetComponentInChildren<Toggle>().isOn; }
        }

        public float dailyQuestNotificationDenominator
        {
            get { return dailyQuestNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float equipAssemblyNotificationDenominator
        {
            get { return equipAssemblyNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float visitorFininshExerciseNotificationDenominator
        {
            get { return visitorFininshExerciseNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float resourceCooldownFinishNotificationDenominator
        {
            get { return resourceCooldownFinishNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float laneQuestCooldownFinishNotificationDenominator
        {
            get { return laneQuestCooldownFinishNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float laneQuestPrepareFinishNotificationDenominator
        {
            get { return laneQuestPrepareFinishNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float sportsmanFinishExerciseNotificationDenominator
        {
            get { return sportsmanFinishExerciseNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float  dailyActiveNotificationDenominator
        {
            get { return dailyActiveNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        public float roomUnlockNotificationDenominator
        {
            get { return roomUnlockNotificationTransform.GetComponentInChildren<Slider>().value; }
        }

        #endregion

        public Color deleteSaveEnabledColor = Color.green;
        public float dragLimitMargin = 50;

        private Color deleteSaveOriginalColor;
        private bool deleteSaveTried = false;

        private float dragStartPos = 0;
        private RectTransform rectTransform;

        private GUIRoot                             _gui;

        public  GUIRoot                             gui { get { return GetGUI(); } }

        private GUIRoot GetGUI()
        {
            if (_gui != null)
                return _gui;

            _gui = GameObjectExtension.FindComponent<GUIRoot>("_GUIRoot");

            return _gui;
        }

        void Awake()
        {
            instance = this;

            rectTransform = GetComponent<RectTransform>();
            Assert.IsNotNull(rectTransform);

            genericView.SetButtonCallback("btnClose",
                () =>
                {
                    gameObject.SetActive(false);
                    if (onHide != null)
                    {
                        onHide();
                    }
                });

            foreach (var toggle in genericView.GetTransform("tabsView").GetComponentsInChildren<Toggle>())
            {
                toggle.onValueChanged.AddListener(isOn =>
                {
                    var tabName = "tab_" + toggle.gameObject.name;
                    var targetTab = genericView.GetTransform(tabName);
                    if (targetTab == null)
                    {
                        Debug.LogErrorFormat(
                            "Can't find dbg tab with generic item alias '{0}', skip dbg tabs switching...",
                            tabName);
                    }
                    else
                    {
                        targetTab.gameObject.SetActive(isOn);
                    }
                });
            }

            btnDeleteSave = genericView.GetComponent<Button>("BtnDeleteSave");
            btnDeleteSave.onClick.AddListener(OnDeleteClick);
            deleteSaveOriginalColor = btnDeleteSave.targetGraphic.color;

            btnUnlocks = genericView.GetComponent<Button>("BtnUnlockWnd");
            var btnMoar = genericView.GetComponent<Button>("BtnMoarMoney");
            btnMoar.onClick.AddListener(OnMoarMoneyClick);
            btnZeroMoney = genericView.GetComponent<Button>("BtnZeroMoney");
            btnNewDay = genericView.GetComponent<Button>("BtnNewDay");
            btnNewDay.onClick.AddListener(OnNewDayClick);
            btnOneMinuteLeft = genericView.GetComponent<Button>("BtnOneMinute");
            btnOneMinuteLeft.onClick.AddListener(OneMinuteLeft);
            btnGetExpToNexLevel = genericView.GetComponent<Button>("GetExpToLevelUp");
            btnPushTest = genericView.GetComponent<Button>("PushTest");
            btnSkipLaneQuest = genericView.GetComponent<Button>("btnSkipLaneQuest");
            btnAddSportsman = genericView.GetComponent<Button>("btnAddSportsman");
            btnRateUs = genericView.GetComponent<Button>("RateUs");
            btnGameMode = genericView.GetComponent<Button>("GameMode");
            btnGooglePlay = genericView.GetComponent<Button>("GooglePlay");

            btnGooglePlay.onClick.AddListener(TestGooglePlayAuth);

            touchSenseSlider = genericView.GetComponent<Slider>("SliderTouchSensetivity");
            touchRotateSlider = genericView.GetComponent<Slider>("RotateTouchSensetivity");
            touchZoomSlider = genericView.GetComponent<Slider>("ZoomTouchSensetivity");
            moveInertSlider = genericView.GetComponent<Slider>("MoveInertion");
            zoomInertSlider = genericView.GetComponent<Slider>("ZoomInertion");
            rotationInertSlider = genericView.GetComponent<Slider>("RotationInertion");
            exerciseTimeOverrideSlider = genericView.GetComponent<Slider>("ExerciseTimeOverride");

            toggleUseAnyEquipment = transform.GetComponentInChild<Toggle>("ToggleUseAnyEquipment");
            toggleAutoPlay = transform.GetComponentInChild<Toggle>("ToggleAutoPlay");
            fastEquipmentBreakage = transform.GetComponentInChild<Toggle>("ToggleFastEquipmentBreakage");
            toggleNoSave = transform.GetComponentInChild<Toggle>("ToggleNoSave");

            profileLvl = genericView.GetComponent<IncDecControl>("ProfileLvl");
            fatigue = genericView.GetComponent<IncDecControl>("Fatigue");
            resources = genericView.GetComponent<IncDecControl>("Resources");

            resourceTypeSelector = genericView.GetComponent<Dropdown>("ResourceTypeSelector");

            inputAddCoins = genericView.GetComponent<InputField>("AddCoins");
            inputAddFitBucks = genericView.GetComponent<InputField>("AddFitBucks");

            adTimerLeft = genericView.GetComponent<Text>("AdTimeLeft");

            dailyQuestNotificationTransform.GetComponentInChildren<Slider>()
                .onValueChanged.AddListener(
                    v =>
                        dailyQuestNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                            v.ToString());

            equipAssemblyNotificationTransform.GetComponentInChildren<Slider>()
                .onValueChanged.AddListener(
                    v =>
                        equipAssemblyNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                            v.ToString());

            visitorFininshExerciseNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    visitorFininshExerciseNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

            resourceCooldownFinishNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    resourceCooldownFinishNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

            laneQuestCooldownFinishNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    laneQuestCooldownFinishNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

            laneQuestPrepareFinishNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    laneQuestPrepareFinishNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

            sportsmanFinishExerciseNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    sportsmanFinishExerciseNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

            dailyActiveNotificationTransform.GetComponentInChildren<Slider>()
                            .onValueChanged.AddListener(
                                v =>
                                    dailyActiveNotificationTransform.Find("DenominatorValue").GetComponent<Text>().text =
                                        v.ToString());

#if DEBUG
            gui.hud.btnDbgNoSave = SaveGame.dbgNoSave;
#endif

            InitLanguagesPanel();
        }

        private void InitLanguagesPanel()
        {
            genericView.SetButtonCallback("quitGameButton",
                () =>
                {
                    PersistentCore.Quit(false);
                });

            var languagesDropdown = genericView.GetComponent<Dropdown>("languageDropdown");
            languagesDropdown.options =
                (new string[] { "Russian", "English" }).Select(i => new Dropdown.OptionData(i)).ToList();
            if (PlayerPrefs.HasKey("LocalizationLanguage"))
            {
                switch (PlayerPrefs.GetString("LocalizationLanguage"))
                {
                    case "ru":
                        languagesDropdown.value = 0;
                        break;

                    default:
                        languagesDropdown.value = 1;
                        break;
                }
            }
            else
            {
                languagesDropdown.value = 1;
            }
            languagesDropdown.onValueChanged.AddListener(id =>
            {
                switch (id)
                {
                    case 0:
                        PlayerPrefs.SetString("LocalizationLanguage","ru");
                        PlayerPrefs.Save();
                        break; 
                    default:
                        PlayerPrefs.SetString("LocalizationLanguage", "eng");
                        PlayerPrefs.Save();
                        break;
                }
            });
        }

        void Start()
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                touchSenseSlider.value = camCtrl.cameraMove.touchMoveMultiplier;
                touchRotateSlider.value = camCtrl.cameraRotation.rotationSpeed;
                touchZoomSlider.value = camCtrl.cameraZoom.zoomMultiplier;
                moveInertSlider.value = camCtrl.cameraMove.inertion;
                zoomInertSlider.value = camCtrl.cameraMove.inertion;
                rotationInertSlider.value = camCtrl.cameraMove.inertion;
            }

            exerciseTimeOverrideSlider.value = DebugHacks.exerciseOverrideTime;

            {
                resourceTypeSelector.ClearOptions();
                var resourcesItems = GUICollections.instance.storageIcons.items;
                List<Dropdown.OptionData> resourceTypeSelectorOptions = new List<Dropdown.OptionData>(resourcesItems.Length);
                foreach (var res in resourcesItems)
                {
                    resourceTypeSelectorOptions.Add ( new Dropdown.OptionData ( res.name, res.sprite ) );
                }
                resourceTypeSelector.AddOptions(resourceTypeSelectorOptions);
                resourceTypeSelector.RefreshShownValue();
            }

            gameObject.SetActive(false);
        }

        void OnEnable()
        {
            touchSenseSlider.onValueChanged.AddListener(TouchSenseChangeValue);
            touchRotateSlider.onValueChanged.AddListener(RotateSenseChangeValue);
            touchZoomSlider.onValueChanged.AddListener(ZoomSenseChangeValue);
            moveInertSlider.onValueChanged.AddListener(MoveInertionChangeValue);
            zoomInertSlider.onValueChanged.AddListener(ZoomInertionChangeValue);
            rotationInertSlider.onValueChanged.AddListener(RotationInertionChangeValue);

#if DEBUG
            toggleNoSave.isOn = SaveGame.dbgNoSave;
            toggleNoSave.onValueChanged.AddListener(OnToggleNoSave);
#endif

            toggleUseAnyEquipment.isOn = DebugHacks.visitorCanUseAnyEquipment;
            toggleUseAnyEquipment.onValueChanged.AddListener(OnToggleUseAnyEquipment);

            toggleAutoPlay.isOn = DebugHacks.visitorsAutoPlay;
            toggleAutoPlay.onValueChanged.AddListener(OnToggleAutoPlay);

            fastEquipmentBreakage.isOn = DebugHacks.fastEquipmentBreakage;
            fastEquipmentBreakage.onValueChanged.AddListener(OnToggleFastEquipmentBreakage);

            exerciseTimeOverrideSlider.onValueChanged.AddListener(ExerciseOverrideTimeChangeValue);

            btnGameMode.onClick.AddListener(OnGameMode);

            if (deleteSaveTried)
            {
                deleteSaveTried = false;
                btnDeleteSave.targetGraphic.color = deleteSaveOriginalColor;
            }

        }

        void OnDisable()
        {
            touchSenseSlider.onValueChanged.RemoveListener(TouchSenseChangeValue);
            touchRotateSlider.onValueChanged.RemoveListener(RotateSenseChangeValue);
            touchZoomSlider.onValueChanged.RemoveListener(ZoomSenseChangeValue);
            moveInertSlider.onValueChanged.RemoveListener(MoveInertionChangeValue);
            zoomInertSlider.onValueChanged.RemoveListener(ZoomInertionChangeValue);
            rotationInertSlider.onValueChanged.RemoveListener(RotationInertionChangeValue);

#if DEBUG
            toggleNoSave.onValueChanged.RemoveListener(OnToggleNoSave);
#endif

            toggleUseAnyEquipment.onValueChanged.RemoveListener(OnToggleUseAnyEquipment);
            toggleAutoPlay.onValueChanged.RemoveListener(OnToggleAutoPlay);

            fastEquipmentBreakage.onValueChanged.RemoveListener(OnToggleFastEquipmentBreakage);

            exerciseTimeOverrideSlider.onValueChanged.RemoveListener(ExerciseOverrideTimeChangeValue);

            btnGameMode.onClick.RemoveListener(OnGameMode);

            dragStartPos = 0;
            rectTransform.anchoredPosition3D = Vector3.zero;
        }

        void TouchSenseChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.SetTouchMoveMultiplier(value);
            }
        }

        void RotateSenseChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.SetRotationSpeed(value);
            }
        }

        void ZoomSenseChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.SetZoomMultiplier(value);
            }
        }

        void MoveInertionChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.cameraMove.inertion = value;
            }
        }

        void ZoomInertionChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.cameraZoom.inertion = value;
            }
        }

        void RotationInertionChangeValue(float value)
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.cameraRotation.inertion = value;
            }
        }

        void ExerciseOverrideTimeChangeValue(float value)
        {
            DebugHacks.exerciseOverrideTime = (int) value;
        }

        void OnToggleUseAnyEquipment(bool value)
        {
            DebugHacks.visitorCanUseAnyEquipment = value;
        }

        private void OnToggleAutoPlay (bool value)
        {
            DebugHacks.visitorsAutoPlay = value;
        }

#if DEBUG
        private void OnToggleNoSave (bool value)
        {
            SaveGame.dbgNoSave = value;
            gui.hud.btnDbgNoSave = value;
        }
#endif

        void OnToggleFastEquipmentBreakage(bool value)
        {
            DebugHacks.fastEquipmentBreakage = value;
        }

        void OnNewDayClick()
        {
            NewDayEvent.Send(1);
        }

        void OneMinuteLeft()
        {
#if DEBUG
            Logic.DbgEvents.OneMinuteLeftForQuestTimers.Send();
            DebugHacks.saveThisFrame = true;
#endif
        }

        void OnDeleteClick()
        {
            if (deleteSaveTried)
            {
                onPerformSaveGameDelete?.Invoke();
            }
            else
            {
                deleteSaveTried = true;
                btnDeleteSave.targetGraphic.color = deleteSaveEnabledColor;
            }
        }

        private void OnMoarMoneyClick()
        {
            int coins = 0;
            int fitBucks = 0;

            try
            {
                coins = Convert.ToInt32(inputAddCoins.text);
                fitBucks = Convert.ToInt32(inputAddFitBucks.text);
            }
            catch (Exception)
            {
                return;
            }

            if ((coins > 0 || fitBucks > 0) && onAddMoneyEvent != null)
            {
                onAddMoneyEvent(coins, fitBucks);
            }
        }

        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            dragStartPos = rectTransform.anchoredPosition3D.y;
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (eventData.pointerCurrentRaycast.isValid && eventData.pointerPressRaycast.isValid)
            {
                var position = rectTransform.anchoredPosition3D;

                var deltaDrag = eventData.pointerCurrentRaycast.screenPosition.y -
                                eventData.pointerPressRaycast.screenPosition.y;
                var pos = dragStartPos + deltaDrag;
                var limit = -(rectTransform.rect.height - dragLimitMargin);
                if (pos < limit)
                    pos = limit;
                else if (pos > 0)
                    pos = 0;

                position.y = pos;
                rectTransform.anchoredPosition3D = position;
            }
        }

        private void OnGameMode()
        {
            PersistentCore.ScheduleGameModeSwitching(PersistentCore.gameMode == GameMode.DemoLevel ? GameMode.MainGame : GameMode.DemoLevel);
        }

        void TestGooglePlayAuth()
        {
            /*
            var sm = GameObject.Find("_GameLoader").GetComponent<ISocialManager>();

            sm.TryLogin (SocialNetwork.GooglePlay, (result) =>
            {
                Debug.Log("GPGS Login: " + result.ToString());

                if (result == LoginResult.OK)
                {
                    Debug.Log("GPGS ID: " + sm.GetSelfUserData(SocialNetwork.GooglePlay, true));
                }
            });
            */
            //FyberScript.instance.ShowVideo();
        }
    }
}