﻿using System;
using Data;
using Data.Person;
using Data.Sport;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.Base;
using View.UI.Shop;
using UnityEngine.UI;

namespace View.UI
{
    [AddComponentMenu("Kingdom/View/UI/Confirm Window")]
    public class ConfirmWnd : UIBaseView
    {
        public enum ConfirmTypes
        {
            Invalid,

            CantExpand,
            ResourceNE,
            Paint,
            Sell,
            Buy,
            MoneyNE,
            Reward,
            StorageNE,

            Custom
        }

        [HideInInspector]
        public ShopScrollViewItemWallFloor wallFloorHasPaint;

        public GameObject wallFloorPrefab;

        public Action OnCancel;  
        public Action OnConfirm;

        private bool   btnOkOn          { set { genericView.SetActive("btnConfirm", value); } }
        private bool   btnCancelOn      { set { genericView.SetActive("btnCancel", value); } }
        private bool   btnCoinsOn       { set { genericView.SetActive("btnCoins", value); } }
        private bool   btnBucksOn       { set { genericView.SetActive("btnBucks", value); } }
        private string btnConfirmText   { set { genericView.SetText("btnConfirmText", value); } }
        private string btnCancelText    { set { genericView.SetText("btnCancelText", value); } }
        private string header           { set { genericView.SetText("header", value); } }
        private string description      { set { genericView.SetText("description", value); } }
        private bool   bucksIcon        { set { genericView.SetActive("BucksIcon", value); } }
        private string bucksText        { set { genericView.SetText("Bucks_Text", value); } }

        private ConfirmTypes curType = ConfirmTypes.Invalid; public ConfirmTypes CurType{get { return curType;}}

        private void Awake()
        {
            var wallFloorObj = genericView.GetTransform("wallFloorIcon");

            var obj = InstantiatePrefab(wallFloorPrefab, wallFloorObj, "WallFloor", 0.6f);
            wallFloorHasPaint = obj.GetComponent<ShopScrollViewItemWallFloor>();

            genericView.SetButtonCallback("btnCancel", OnCancelClick);
            genericView.SetButtonCallback("btnConfirm", OnConfirmClick);

            OnHide_Callback = delegate
            {
                if (OnCancel != null)
                {
                    OnCancel();
                    OnCancel = null;
                }
            };
        }
            
        private void DisableAll()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
                
            btnCoinsOn = false;
            btnBucksOn = false;

            btnConfirmText = string.Empty;
            btnCancelOn = false;

            header = string.Empty;
            description = string.Empty;

            genericView.SetActive("header");
            genericView.SetActive("description");
            genericView.SetActive("icons");
            genericView.HideChildren("icons");
            genericView.SetActive("Bucks_Text");
            genericView.SetActive("levelInfo", false);

            btnCancelText = Loc.Get("idCancel");

            OnConfirm = null;
            OnCancel = null;

            curType = ConfirmTypes.Invalid;

            bucksIcon = false;
            bucksText = "";
        }

        public void OnCantExpandConfirm(int needLevel)
        {
            DisableAll();
            Show();

            header = Loc.Get("expandUnavailableHeader");
            if (needLevel > 0)
                description = Loc.Get("expandAvailableAtLevelMessage");//, needLevel);
            else
                description = Loc.Get("expandAtMaxExpandLevelMessage");

            genericView.SetActive("levelInfo", true);
            genericView.SetText("levelInfoText", needLevel.ToString());

            genericView.SetActive("expandIcon");

            btnOkOn = true;
            btnConfirmText = Loc.Get("idClose");

            curType = ConfirmTypes.CantExpand;
        }

        public void OnNeedSportsmen(SportsmanType type, int bucksPrice)
        {
            var list = new SportsmanCostUnit[1];

            list[0].sportsmanType = type;
            list[0].count = 1;

            OnNeedSportsmen(list, bucksPrice);
        }

        public void OnNeedSportsmen(SportsmanCostUnit[] members, int bucksPrice)
        {
            DisableAll();
            Show();

            header = Loc.Get("laneQuestNotEnoughSportsmans");

            var view = genericView.IsolateSubView("needResources");
            view.HideItems();

            var resIcons = GUICollections.instance.sportsmenIcons;

            int count = 0;
            foreach (var item in members)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, resIcons.items[(int)item.sportsmanType].sprite);
                view.SetText(resTxt, item.count.ToString());

                count++;

                if (count >= 4)
                    break;
            }

            btnOkOn = true;
            btnBucksOn = true;
            btnCancelOn = true;
            btnConfirmText = bucksPrice.ToString();

            curType = ConfirmTypes.ResourceNE;

        }

        public void OnNeedResources2(Cost cost, int bucksExtra)
        {
            OnNeedResources(cost);
            btnBucksOn = true;
            btnCancelOn = true;
            btnConfirmText = bucksExtra.ToString();
        }

        public void OnNeedResources(Cost price)
        {
            DisableAll();
            Show();

            header = Loc.Get("confirmNotEnoughRes");

            var view = genericView.IsolateSubView("needResources");
            view.HideItems();

            var resIcons = GUICollections.instance.storageIcons;

            int count = 0;

            if (price.type == Cost.CostType.ResourcesAndCoins || price.type == Cost.CostType.CoinsOnly)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, Cost.GetSpriteCoins());
                view.SetText(resTxt, price.value.ToString());

                count++;
            }

            foreach (var item in price.resources)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, resIcons.items[(int)item.Key].sprite);
                view.SetText(resTxt, item.Value.ToString());

                count++;

                if (count >= 4)
                    break;
            }

            btnOkOn = true;
            btnConfirmText = Loc.Get("idOk");

            curType = ConfirmTypes.ResourceNE;
        }

        public void OnBuyWallFloorConfirm(ShopItemData item)
        {
            DisableAll();
            Show();

            btnOkOn = true;
            btnCancelOn = true;

            header = Loc.Get("confirmDoYouLikeColors");
            description = Loc.Get("confirmletsPaintGym");
                
            genericView.SetActive("wallFloorIcon");
            SetPrice(item.price);

            curType = ConfirmTypes.Paint;
        }

        public void OnSellConfirm(Cost price, string header, string message)
        {
            DisableAll();
            Show();

            this.header = header;
            this.description = message;

/*
            if (price.type == Cost.CostType.CoinsOnly)
            {
                genericView.SetActive("coinsIcon");
            }
            else if (price.type == Cost.CostType.BucksOnly || price.type == Cost.CostType.ResourcesAndCoins)
            {
                genericView.SetActive("bucksIcon");
            }
            else
            {
                Debug.LogError("Wrong price type " + price.type.ToString());
            }
*/

            genericView.SetActive("sellIcon");
            SetPrice(price);

            curType = ConfirmTypes.Sell;
        }

        public void OnBuyConfirm(Cost price, string header, string message, Sprite customIcon = null)
        {
            DisableAll();
            Show();

            this.header = header;
            this.description = message;

            if (customIcon != null)
            {
                genericView.SetActive("customIcon");
                genericView.GetComponent<Image>("customIcon").sprite = customIcon;
            }
            else if (price.type == Cost.CostType.CoinsOnly)
            {
                genericView.SetActive("coinsIcon");
            }
            else if (price.type == Cost.CostType.BucksOnly || price.type == Cost.CostType.ResourcesAndCoins)
            {
                genericView.SetActive("bucksIcon");
            }
            else
            {
                Debug.LogError("Wrong price type " + price.type.ToString());
            }

            //genericView.SetActive("sellIcon");
            SetPrice(price);

            curType = ConfirmTypes.Buy;
        }

        public void OnNeedMoney(Cost price)
        {
            if (price.type == Cost.CostType.ResourcesOnly)
            {
                OnNeedResources(price);
                return;
            }


            DisableAll();
            Show();

            if (price.type == Cost.CostType.CoinsOnly)
            {
                header = Loc.Get("confirmNotEnoughCoins");
                description = Loc.Get("confirmMoreCoins");
                genericView.SetActive("coinsIcon");
            }
            else if (price.type == Cost.CostType.BucksOnly || price.type == Cost.CostType.ResourcesAndCoins)
            {
                header = Loc.Get("confirmNotEnoughBucks");
                description = Loc.Get("confirmMoreBucks");
                genericView.SetActive("bucksIcon");
            }
            else
            {
                Debug.LogError("Wrong price type " + price.type.ToString());
            }

            btnOkOn = true;
            btnCancelOn = true;

            btnConfirmText = Loc.Get("idAdd");

            curType = ConfirmTypes.MoneyNE;
        }
            
        private void SetPrice(Cost price)
        {
            btnOkOn = true;
            btnCancelOn = true;

            btnCoinsOn = price.type == Cost.CostType.CoinsOnly;
            btnBucksOn = price.type == Cost.CostType.BucksOnly;

            Assert.IsTrue(price.type == Cost.CostType.CoinsOnly || price.type == Cost.CostType.BucksOnly);

            btnConfirmText = price.value.ToString();
        }
            
        public void OnAdReward(int count)
        {
            DisableAll();
            Show();

            genericView.SetActive("adIcon");

            header = Loc.Get("confirmThankYou");
            description = Loc.Get("confirmCollectReward");

            btnOkOn = true;
            //btnBucksOn = true;

            bucksIcon = true;
            bucksText = count.ToString();

            btnConfirmText = Loc.Get("idCollect"); //count.ToString();

            curType = ConfirmTypes.Reward;
        }

        public void OnStorageNotEnough()
        {
            DisableAll();
            Show();

            genericView.SetActive("storageIcon");

            header = Loc.Get("confirmStorageFullHeader");
            description = Loc.Get("confirmStorageFullmsg");

            btnOkOn = true;
            btnConfirmText = Loc.Get("idUpgrade");

            btnCancelOn = true;

            curType = ConfirmTypes.StorageNE;
        }
            
        public void OnCustom(string header, string text, string okButtonText = null, bool cancelOn = false, string cancelButtonText = null, int level = -1)
        {
            if (string.IsNullOrEmpty(okButtonText))
                okButtonText = Loc.Get("idOk");

            DisableAll();
            Show();

            genericView.SetActive("genericIcon");

            if (level > 0)
            {
                genericView.SetActive("levelInfo", true);
                genericView.SetText("levelInfoText", level.ToString());
            }

            this.header = header;
            this.description = text;

            btnOkOn = true;
            btnConfirmText = okButtonText;

            if (cancelButtonText != null)
                btnCancelText = cancelButtonText;


            btnCancelOn = cancelOn;

            curType = ConfirmTypes.Custom;
        }

        public void OnCantAddSportsmen(string canselText = "idKick")
        {
            OnCustom(Loc.Get("confirmCantAddPro"), Loc.Get("confirmBenchFull"), Loc.Get("idUpgrade"), true, Loc.Get(canselText));
            genericView.HideChildren("icons");
            genericView.SetActive("sportsmenCoins");
        }
            
        void OnCancelClick()
        {
            var tmpCancel = OnCancel;
            OnCancel = null;
            OnConfirm = null;

            if (tmpCancel != null)
            {
                tmpCancel();
            }
            else
            {
                Hide();
            }
        }
            
        void OnConfirmClick()
        {
            var tmpConfirm = OnConfirm;
            OnConfirm = null;
            OnCancel = null;

            bool hideAfterConfirm = (curType == ConfirmTypes.Sell);
            bool noHide = (curType == ConfirmTypes.Paint);

            if (!hideAfterConfirm && !noHide)
                Hide();

            if (tmpConfirm != null)
                tmpConfirm();

            if (hideAfterConfirm && !noHide)
                Hide();
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                OnCancelClick();
            }
        }
    }
}