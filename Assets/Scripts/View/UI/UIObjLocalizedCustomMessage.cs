﻿using UnityEngine;
using System.Collections;
using Core;
using Data;
using Presentation.Base;

namespace View.UI
{   
    public class UIObjLocalizedCustomMessage : MonoBehaviour
    {
        BasePresentationCommon  common = new BasePresentationCommon(); // What a hack!
        
        public string locHeaderId;
        public string locMessageId;
        public bool canShowMessage = true;

        public void ShowMessage()
        {
            if (PersistentCore.gameMode == GameMode.MainGame && canShowMessage)
                common.gui.confirmWnd.OnCustom(Loc.Get(locHeaderId), Loc.Get(locMessageId));
        }
    }
}
