﻿using View.UI.Base;
using System;
using Data;
using UnityEngine.UI;
using UnityEngine;

namespace View.UI
{

    public class RankAchievmentsView : UIBaseView 
    {
        public UIGenericView            itemsView       { private set; get; }
        public RankAchievmentsViewList  list            { private set; get; }
        public GameObject               hq;

        public string completedRatio    { set { genericView.SetText("completed", Loc.Get("AchievementsCompleted") + " " + value); } }
        public string rankName          { set { genericView.SetText("rankName", value); } }

        void Awake()
        {
            BindCloseButton();
            itemsView = genericView.GetSubView("items");
            itemsView.DestroyChildren();

            list = genericView.GetComponent<RankAchievmentsViewList>("items");
        }

        new void Start()
        {
            base.Start();

            GetComponentInChildren<ScrollRect>().normalizedPosition = Vector2.one;
        }

        public void UglyHackFixLayout()
        {
            RectTransform rt = itemsView.transform as RectTransform;
            rt.anchoredPosition = new Vector2(0, rt.anchoredPosition.y);
        }
    }
}
