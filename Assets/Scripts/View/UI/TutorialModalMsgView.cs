using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class TutorialModalMsgView : UIBaseView
    {
        private Text txtTitle;
        private Text txtContent;
        
        public string title { set { txtTitle.text = value; } }
        public string content { set { txtContent.text = value; } }

        void Awake()
        {
            txtTitle = genericView.GetComponent<Text>("txtTitle");
            txtContent = genericView.GetComponent<Text>("txtContent");
            BindCloseButton();
        }
    }
}