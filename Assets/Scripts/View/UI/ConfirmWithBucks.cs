﻿using Data;
using System;
using System.Collections.Generic;
using Data.Person;
using UnityEngine;
using View.UI.Base;
using Data.Sport;

namespace View.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Confirm Window Buy With Bucks")]
    public class ConfirmWithBucks : UIBaseView
    {
        public Action OnOpenShop;  
        public Action OnConfirm;
        public Action OnCancel;

        private string header           { set { genericView.SetText("header", value); } }
        private string btnConfirmText   { set { genericView.SetText("btnConfirmText", value); } }

        private bool    btnShopOn       { set { genericView.SetActive("btnShop", value); } }
        private bool    btnConfirmOn    { set { genericView.SetActive("btnConfirm", value); } }

        private void Awake()
        {
            genericView.SetButtonCallback("btnShop", OnOpenShopClick);
            genericView.SetButtonCallback("btnConfirm", OnConfirmClick);
            genericView.SetButtonCallback("btnClose", OnCloseClick);
        }
             
        public void OnNeedSportsmen(SportsmanType type, int bucksPrice)
        {
            var list = new SportsmanCostUnit[1];

            list[0].sportsmanType = type;
            list[0].count = 1;

            OnNeedSportsmen(list, bucksPrice);
        }

        public void OnNeedSportsmen(SportsmanCostUnit[] members, int bucksPrice)
        {
            Show();

            btnShopOn = false;
            btnConfirmOn = true;

            header = Loc.Get("laneQuestNotEnoughSportsmans");

            var view = genericView.GetSubView("needResources");
            view.HideItems();

            var resIcons = GUICollections.instance.sportsmenIcons;

            int count = 0;
            foreach (var item in members)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, resIcons.items[(int)item.sportsmanType].sprite);
                view.SetText(resTxt, "x" + item.count.ToString());

                count++;

                if (count >= 4)
                    break;
            }

            btnConfirmText = bucksPrice.ToString();
        }

        public void OnNeedExercises(Dictionary<Exercise, int> exercises, int bucksPrice)
        {
            Show();

            btnShopOn = false;
            btnConfirmOn = true;

            header = "Complete all objectives";

            var view = genericView.GetSubView("needResources");
            view.HideItems();

            var resIcons = GUICollections.instance.exerciseSpritesList;

            int count = 0;
            foreach (var item in exercises)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, resIcons.array[item.Key.icon]);
                view.SetText(resTxt, item.Value.ToString());

                count++;

                if (count >= 4)
                    break;
            }

            btnConfirmText = bucksPrice.ToString();
        }

        public void OnNeedResources2(Cost costShortage, int bucksExtra, bool shopOn = true, bool confirmOn = false)
        {
            OnNeedResources(costShortage);
            btnConfirmText = bucksExtra.ToString();

            btnShopOn = shopOn;
            btnConfirmOn = confirmOn;
        }

        public void OnNeedResources(Cost costShortage, bool shopOn = true, bool confirmOn = false)
        {
            Show();

            btnShopOn = shopOn;
            btnConfirmOn = confirmOn;

            if (costShortage.type == Cost.CostType.CoinsOnly)
            {
                header = Loc.Get("confirmNotEnoughCoins");
            }
            else
            {
                header = Loc.Get("confirmNotEnoughRes");
            }

            var view = genericView.GetSubView("needResources");
            view.HideItems();

            var resIcons = GUICollections.instance.storageIcons;

            int count = 0;

            if (costShortage.type == Cost.CostType.ResourcesAndCoins || costShortage.type == Cost.CostType.CoinsOnly)
            {
                if (costShortage.value > 0)
                {
                    var resObj = "res" + count.ToString();
                    var resImg = "res" + count.ToString() + "_icon";
                    var resTxt = "res" + count.ToString() + "_text";

                    view.SetActive(resObj);
                    view.SetActive(resImg);
                    view.SetActive(resTxt);

                    view.SetSprite(resImg, Cost.GetSpriteCoins());
                    view.SetText(resTxt, costShortage.value.ToString());

                    count++;
                }
            }

            foreach (var item in costShortage.resources)
            {
                var resObj = "res" + count.ToString();
                var resImg = "res" + count.ToString() + "_icon";
                var resTxt = "res" + count.ToString() + "_text";

                view.SetActive(resObj);
                view.SetActive(resImg);
                view.SetActive(resTxt);

                view.SetSprite(resImg, resIcons.items[(int)item.Key].sprite);
                view.SetText(resTxt, "x" + item.Value.ToString());

                count++;

                if (count >= 4)
                    break;
            }

        }
   
        void OnOpenShopClick()
        {
            if (OnOpenShop != null)
            {
                var tmpCancel = OnOpenShop;
                OnOpenShop = null;
                tmpCancel();
            }
            else
                Hide();
        }
            
        void OnConfirmClick()
        {
            var tmpConfirm = OnConfirm;
            OnConfirm = null;
            OnOpenShop = null;

            if (tmpConfirm != null)
                tmpConfirm();
            if (gameObject.activeSelf)
                Hide();
        }

        void OnCloseClick()
        {
            OnConfirm = null;
            OnOpenShop = null;

            var tmp = OnCancel;
            OnCancel = null;
            if (tmp != null)
                tmp();

            Hide();
        }
    }
}