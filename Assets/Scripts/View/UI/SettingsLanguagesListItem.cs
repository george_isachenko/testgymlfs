using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class SettingsLanguagesListItem : UIBaseViewListItem
    {
        protected override void Awake()
        {
            var btn = GetComponent<Button>();

            if (btn != null)
            {
                btn.onClick.AddListener(
                    delegate ()
                    {
                        if (OnClickCallback != null)
                            OnClickCallback.Invoke(this);
                    });
            }
        }

        public string languageName
        {
            set { genericView.SetText("languageNameLabel", value); }
        }

        public Sprite languageIcon
        {
            set { genericView.SetSprite("languageIconImage", value);}
        }
    }
}