﻿using System;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    public class SportsmenAgencyView : UITabControlledList<SportsmenViewItem>
    {
        public SportsmenAgencyViewListSell slotsSell;
        public SportsmenAgencyViewListBuy slotsBuy;

        public string   textTime            { set { slotsBuy.genericView.SetText("time", value); } }

        public Action updateTimer;

        new void Awake()
        {

            BindCloseButton();

            slotsSell = genericView.GetComponent<SportsmenAgencyViewListSell>("sell");
            slotsSell.itemsRoot = slotsSell.genericView.GetTransform("itemsSell");
            itemsRoot = slotsSell.genericView.GetTransform("items");

            slotsBuy = genericView.GetComponent<SportsmenAgencyViewListBuy>("buy");
            slotsBuy.itemsRoot = slotsBuy.genericView.GetTransform("items");

            foreach(Transform t in slotsBuy.itemsRoot)
            {
                t.gameObject.SetActive(false);
            }



            tabsGenericAliases = new[]
            {
                "tabBtn_sell",
                "tabBtn_buy"
            };

            base.Awake();
        }

        void Update()
        {
            if (updateTimer != null)
                updateTimer();
            else
                Debug.LogError("updateTimer is null");
        }

        public void ShowBuyTab()
        {
            Show();
            SwitchTab(1);
        }
    }
}


