﻿using System;
using Core.Timer;
using Data;
using Data.Person;
using Data.SportAgency;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    public class SportsmenAgencyViewItemSell : UIBaseViewListItem
    {
        public Action<int>                  Sell_Callback;
        public Action<int>                  Claim_Callback;
        public Action<int>                  TimeOut_Callback;
        public Action<int>                  Unlock_Callback;

        private SportsmanType              _sportType;
        public SportsmanType                sportType   { set { SetIcon(value); _sportType = value; }  get { return _sportType; } }
        public SportAgencySellSlot.State    state       { set { SetState(value); } }
        public bool                         btnSellOn   { set { sellView.SetActive("btnSell", value); } }
        public bool                         btnClaimOn  { set { sellView.SetActive("btnClaim", value); isSold = value; } }
        public int                          price       { set { sellView.SetText("price", "+" + value.ToString()); } }
        public Cost                         unlockCost  { set { SetUnlockCost(value); } }

        private TimerFloat                  _timer;
        public TimerFloat                   timer       { set { _timer = value; timerOn = (_timer != null); } get { return _timer; } }

        private UIGenericView sellView;  
        private UIGenericView lockView;

        bool                                timerOn     { set { sellView.SetActive("imgTimer", value); } }
        bool                                isSold      { set { sellView.SetActive("isSold", value); } }

        new void Awake()
        {
            lockView = genericView.GetSubView("locked");
            sellView = genericView.GetSubView("sell");

            genericView.IsolateSubView("locked");

            sellView.SetButtonCallback("btnSell", OnSellClick);
            sellView.SetButtonCallback("btnClaim", OnClaimClick);

            lockView.SetButtonCallback("btnUnlock", OnUnlockClick);
        }

        void SetUnlockCost(Cost cost)
        {
            lockView.SetText("price", cost.value.ToString());
        }

        void OnSellClick()
        {
            if (Sell_Callback != null)
                Sell_Callback(idx);
            else
                Debug.LogError("Sell_Callback is null");
        }

        void OnClaimClick()
        {
            if (Claim_Callback != null)
                Claim_Callback(idx);
            else
                Debug.LogError("Claim_Callback is null");
        }

        void OnUnlockClick()
        {
            if (Unlock_Callback != null)
                Unlock_Callback(idx);
            else
                Debug.LogError("Unlock_Callback is null");
        }

        void SetState(SportAgencySellSlot.State state)
        {
            switch (state)
            {
            case SportAgencySellSlot.State.Empty:
                genericView.IsolateSubView("empty");
                break;
            case SportAgencySellSlot.State.Locked:
                genericView.IsolateSubView("locked");
                break;
            case SportAgencySellSlot.State.Sell:
                genericView.IsolateSubView("sell");
                break;
            }
        }

        void SetIcon(SportsmanType value)
        {
            var icon = GUICollections.instance.sportsmenIcons.items[(int)value].sprite;
            sellView.SetSprite("icon", icon);
        }

        void Update()
        {
            if (timer != null)
            {
                if (timer.secondsToFinish == 0)
                {
                    timer = null;
                    if (TimeOut_Callback != null)
                        TimeOut_Callback(idx);
                    else
                        Debug.LogError("TimeOut_Callback is NULL");
                }
            }
        }
    }
}

