using System;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

public class SportsmanTrainingExerciceView : UIBaseView
{
    public Action<SportsmanTrainingExerciceView> onButtonPressed = a => { };
    private Color baseButtonColor;

    public Transform trainButton
    {
        get { return genericView.GetTransform("buyButton"); }
    }

    void Awake()
    {
        genericView.SetButtonCallback("buyButton",()=> { onButtonPressed(this); });
        baseButtonColor = trainButton.GetComponent<Image>().color;
    }

    public void FillData(SportTrainingExerciseViewData viewData)
    {   
        genericView.SetText("sportName",viewData.sportNameText);
        genericView.SetText("durationText",viewData.durationText);
        genericView.SetSprite("resultSportsman", viewData.targetSportsmanIcon);
        genericView.SetSprite("needResourceIcon",viewData.needResourceIcon);
        genericView.SetText("neededResourceText",viewData.needResourceText);
        genericView.SetSprite("neededSportsmanIcon",viewData.baseSportsmanIcon);
        genericView.SetText("neededSportsmanText", viewData.needSportsmanText);

        trainButton.GetComponent<Image>().color = viewData.buyButtonGrayMode
            ? Color.gray
            : baseButtonColor;
    }
}