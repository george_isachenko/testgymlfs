﻿using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class SportsmenView : UIBaseViewList<SportsmenViewItem>
    {
        public string sportsmenCount    { set { genericView.SetText("count", value); } }

        void Awake()
        {
            itemsRoot = DefaultItemsRoot();
            BindCloseButton();
        }
    }
}
