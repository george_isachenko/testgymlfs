﻿using Logic.Quests;
using UnityEngine;
using View.UI.Base;
using Logic.Quests.Base;

namespace View.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Quest Reward View")]
    public class QuestRewardView : UIGenericMonoBehaviour
    {
        #region Protected properties.
        protected bool activeSportsmenReward                    { set { genericView.SetActive("sportsmenReward", value); } }

        protected bool activeCoinsReward                        { set { genericView.SetActive("coinsReward", value); } }
        protected string txtCoinsReward                         { set { genericView.SetText("coinsRewardTxt", value); } }

        protected bool activeExpReward                          { set { genericView.SetActive("expReward", value); } }
        protected string txtExpReward                           { set { genericView.SetText("expRewardTxt", value); } }
        #endregion

        #region Public fields.
        public string noBonusFormatString;
        public string singleBonusFormatString;
        public string rangeBonusFormatString;
        public Color bonusColor;
        #endregion

        #region Public API.
        public void SetRewards (QuestEstimatedRewards rewards)
        {
            activeSportsmenReward = (rewards.baseReward.rewardType == QuestRewardType.BecameSportsmen);
            
            if (rewards.baseReward.rewardType == QuestRewardType.CoinsAndExperience)
            {
                activeCoinsReward = (rewards.baseReward.rewardType == QuestRewardType.CoinsAndExperience);

                txtCoinsReward = FormatValues(rewards.baseReward.coins, rewards.bonusRewardMin.coins, rewards.bonusRewardMax.coins);
            }
            else
            {
                activeCoinsReward = false;
            }

            activeExpReward = true;
            txtExpReward = FormatValues(rewards.baseReward.experience, rewards.bonusRewardMin.experience, rewards.bonusRewardMax.experience);
        }

        public string FormatValues (int baseValue, int bonusMin, int bonusMax)
        {
            if (bonusMin > 0)
            {
                if (bonusMax > bonusMin)
                {
                    return string.Format(rangeBonusFormatString, baseValue, bonusColor.ToRGBHex(), bonusMin, bonusMax);
                }
                else
                {
                    return string.Format(singleBonusFormatString, baseValue, bonusColor.ToRGBHex(), bonusMin );
                }
            }
            else
            {
                return string.Format(noBonusFormatString, baseValue);
            }
        }
        #endregion
    }
}