﻿using Data;
using Data.Person;
using Presentation.Providers;
using UnityEngine.Assertions;
using Data.Person.Info;
using View.UI.Base;

namespace View.UI
{
    public class SportsmanPanel : UIGenericMonoBehaviour  
    {
        public SportsmanType    icon    { set { SetIcon(value);  } }
        public string           text    { set { genericView.SetText("txtName", value); } }

        private ICharacterInteractionProvider characterInteractionProvider;

        void Awake()
        {
            icon = SportsmanType.Sportsman_0;
        }

        public void Init( ICharacterInteractionProvider characterInteractionProvider)
        {
            Assert.IsNotNull(characterInteractionProvider);
            if (characterInteractionProvider != null)
            {
                this.characterInteractionProvider = characterInteractionProvider;
                characterInteractionProvider.onCharacterSelected += OnCharacterSelected;
                characterInteractionProvider.onCharacterDeselected += OnCharacterDeselected;
            }
        }

        void OnDestroy()
        {
            if (characterInteractionProvider != null)
            {
                characterInteractionProvider.onCharacterSelected -= OnCharacterSelected;
                characterInteractionProvider.onCharacterDeselected -= OnCharacterDeselected;

                characterInteractionProvider = null;
            }
        }

        void SetIcon(SportsmanType value)
        {
            if (GUICollections.instance == null)
            {
                gameObject.SetActive(false);
                return;
            }

            var icon = GUICollections.instance.sportsmenIcons.items[(int)value].sprite;

            if (icon == null)
            {
                gameObject.SetActive(false);
                return;
            }

            genericView.SetSprite("icon", icon);
        }

        public void OnCharacterSelected(IPersonInfo personInfo, bool focus)
        {
            if (!(personInfo is IPersonSportsmanInfo))
                return;

            var sportsmanInfo = personInfo as IPersonSportsmanInfo;
            if (sportsmanInfo != null)
            {
                gameObject.SetActive(true);
                icon = sportsmanInfo.currentOrNextSportsmanType;
                text = sportsmanInfo.name;
            }
        }

        public void OnCharacterDeselected(IPersonInfo personInfo)
        {
            gameObject.SetActive(false);
        }
    }
}
