using System;
using Data;
using View.UI.Base;

namespace View.UI
{
    public class FeatureLockedView : UIBaseView
    {
        public string description { set { genericView.SetText("descriptionText", value); } }

        private void Awake()
        {
            BindCloseButton();
        }
    }
}