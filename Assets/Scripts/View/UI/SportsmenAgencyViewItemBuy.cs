﻿using System;
using Data;
using Data.Person;
using View.UI.Base;

namespace View.UI
{
    public class SportsmenAgencyViewItemBuy : UIBaseViewListItem
    {
        public Action<int>      TimeOut_Callback;

        public SportsmanType    icon        { set { SetIcon(value);  } }
        public string           text        { set { genericView.SetText("name", value); } }
        public Cost             cost        { set { SetCost(value); } }
        public int              count       { set { genericView.SetText("count", value.ToString()); } }
        public bool             isSold      { set { SetIsSold(value); } }

        new protected void Awake()
        {
            base.Awake();
        }

        void Start()
        {
            
        }

        void SetIcon(SportsmanType value)
        {
            var icon = GUICollections.instance.sportsmenIcons.items[(int)value].sprite;
            genericView.SetSprite("icon", icon);
        }

        void SetCost(Cost cost)
        {
            genericView.SetActive("buyCoinsIcon", cost.type == Cost.CostType.CoinsOnly);
            genericView.SetActive("buyBucksIcon", cost.type == Cost.CostType.BucksOnly);

            genericView.SetText("price", cost.value.ToString());
        }

        void SetIsSold(bool value)
        {
            //genericView.SetActive("active", !value);
            genericView.SetActive("sold", value);
            genericView.SetActive("btnBuy", !value);
        }
    }
}

