﻿using UnityEngine;
using System.Collections;
using System;
using View.UI.Base;

namespace View.UI
{
    public class RankLvlUpView : UIBaseView 
    {
        public int      reward    { set { genericView.SetText("reward", value.ToString()); } }
        public int      rank      { set { genericView.SetText("rank", value.ToString()); } }
        public Action   OnClaim;

        void Awake()
        {
            genericView.SetButtonCallback("claim", OnClaimClic);
        }

        void OnClaimClic()
        {
            Hide();
            if (OnClaim != null)
            {
                OnClaim();
            }
        }
    }
}
