using UnityEngine;
using UnityEngine.Assertions;

namespace View.UI.Base
{
    public static class UIViewSubListFactory<TList, TItem>
        where TList : UIViewSubList<TItem>
        where TItem : UIBaseViewListItem
    {
        public static TList CreateUiList(Transform listRoot, string itemPrefabPath)
        {
            var gameObject = listRoot.gameObject;
            var list = gameObject.AddComponent<TList>();
            list.itemPrefab = Resources.Load<GameObject>(itemPrefabPath);
            list.setRoot(listRoot);
            return list;
        }

        public static TList CreateUiList(Transform listRoot, GameObject itemPrefab)
        {
            Assert.IsNotNull(itemPrefab);
            var gameObject = listRoot.gameObject;
            var list = gameObject.AddComponent<TList>();
            list.itemPrefab = itemPrefab;
            list.setRoot(listRoot);
            return list;
        }
    }
}