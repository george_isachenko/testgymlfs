﻿using UnityEngine;
using System.Collections.Generic;

namespace View.UI.Base
{

    public class UIViewList<T> : UIGenericMonoBehaviour where T : UIBaseViewListItem
    {
        [HideInInspector]
        public Transform itemsRoot;

        [HideInInspector] public    List<T> items       = new List<T>();
        private                     List<T> itemsAll    = new List<T>();

        public GameObject itemPrefab;

        private T _selectedItem = null;
        public T selectedItem { get { return _selectedItem; } }

        public void SetSelectedItem(T item, bool needUpdate)
        {
            _selectedItem = item; 
            UpdateSelectection (needUpdate);                        
        }

        public delegate void OnItemSelectedDelegate(T item);
        [HideInInspector] public OnItemSelectedDelegate OnItemSelected_Callback;

        public virtual void UpdateItemsCount(int count)
        {
            if (items.Count == count)
                return;

            if (itemsAll.Count < count)
            {
                int diff = count - itemsAll.Count;
                for (int i = 0; i < diff; i++)
                    CreateNewItem();
            }

            items.Clear();

            foreach (var item in itemsAll)
                item.gameObject.SetActive(false);

            for (int i = 0; i < count; i++)
            {
                itemsAll[i].gameObject.SetActive(true);
                items.Add(itemsAll[i]);
            }
        }

        protected virtual T CreateNewItem()
        {
            T viewItem = UIBaseView.InstantiatePrefab<T>(itemPrefab, itemsRoot, "item_" + itemsAll.Count);

            viewItem.OnClickCallback = OnItemClick;
            viewItem.selected = false;
            itemsAll.Add(viewItem);
            return viewItem;
        }

        protected void OnItemClick(UIBaseViewListItem item)
        {
            SetSelectedItem (item as T, true);
        }
            
        protected void UpdateSelectection(bool needUpdate)
        {
            SetSelected(_selectedItem);

            if (needUpdate) 
            {
                OnItemSelectionUpdate ();
            }
        }

        protected virtual void OnItemSelectionUpdate()
        {
            if (OnItemSelected_Callback != null)
            {
                OnItemSelected_Callback(selectedItem);
            }
        }
            
        protected virtual void SetSelected(UIBaseViewListItem item)
        {
            foreach (var it in items)
            {
                it.selected = it == item;
            }
        }

        public T GetItemByIndex(int index_)
        {
            foreach (var item in itemsAll)
            {
                if (item.idx == index_)
                    return item;
            }
                    
            return null;
        }
        
        public void setRoot(Transform root)
        {
            itemsRoot = root;
        }

        public virtual T this[int i]
        {
            get { return items[i]; }
        }

        public int Count()
        {
            return items.Count;
        }
    }
}
