﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.UI.Base
{
    public class UIBaseHintableListItem : UIBaseViewListItem
    {
        private class HintEvent : UnityEvent<UIBaseViewListItem>
        {
        }

        public readonly UnityEvent<UIBaseViewListItem> startHint = new HintEvent();
        public readonly UnityEvent<UIBaseViewListItem> endHint = new HintEvent();

        private IEnumerator waitCoroutine = null;
        private bool isCalledHandler = false;

        private bool isPressed
        {
            set
            {
                if (value)
                    startHint.Invoke(this);
                else
                    endHint.Invoke(this);

                isCalledHandler = value;
            }
            get { return isCalledHandler; }
        }

        protected override void Awake()
        {
            base.Awake();

            var firstImage = GetComponent<Image>() ?? GetComponentInChildren<Image>();
            var trigger = firstImage.GetComponent<EventTrigger>() ?? firstImage.gameObject.AddComponent<EventTrigger>();
            trigger.triggers.Add(GenerateEntry(EventTriggerType.PointerDown, OnPointerDown));
            trigger.triggers.Add(GenerateEntry(EventTriggerType.PointerUp, OnPointerUp));
            trigger.triggers.Add(GenerateEntry(EventTriggerType.PointerExit, OnPointerExit));
        }

        private static EventTrigger.Entry GenerateEntry(EventTriggerType triggerType, UnityAction<BaseEventData> action)
        {

            var entry = new EventTrigger.Entry {eventID = triggerType};
            entry.callback.AddListener(action);
            return entry;
        }

        private void StopWaitCoroutine()
        {
            if (waitCoroutine == null) return;
            StopCoroutine(waitCoroutine);
            waitCoroutine = null;
        }

        private IEnumerator DelayedPressStart()
        {
            yield return new WaitForSeconds(.1f);
            isPressed = true;
            waitCoroutine = null;
        }

        public void ClearHintCallbacks()
        {
            startHint.RemoveAllListeners();
            endHint.RemoveAllListeners();
        }

        private void OnPointerDown(BaseEventData arg0)
        {
            StopWaitCoroutine();
            waitCoroutine = DelayedPressStart();
            StartCoroutine(waitCoroutine);
        }

        private void OnPointerUp(BaseEventData arg0)
        {
            StopWaitCoroutine();
            if (isPressed)
                isPressed = false;
        }

        private void OnPointerExit(BaseEventData arg0)
        {
            StopWaitCoroutine();
            if (isPressed)
                isPressed = false;
        }
    }
}