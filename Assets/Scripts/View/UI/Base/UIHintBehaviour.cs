﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.UI.Base
{
    public class UIHintBehaviour : Button
    {
        public string title = string.Empty;
        public string desc = string.Empty;
        public RectTransform customTransform;
        public float delay = -1.0f;

        public static TooltipView view = null;

        new void Start()
        {
            base.Start();

            if (view == null)
            {
                view = GUIRoot.instance.tooltip;
            }
        }

        public override void OnPointerDown (PointerEventData eventData) 
        {
            base.OnPointerDown(eventData);
            view.title = title;
            view.description = desc;
            view.delay = delay;
            view.Show(customTransform != null ? customTransform : transform as RectTransform);
        }

        public override void OnPointerUp (PointerEventData eventData) 
        {
            base.OnPointerUp(eventData);
            view.Hide();
        }
    }
}