﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace View.UI.Base
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class UIGenericView : MonoBehaviour 
    {
        public string   alias;
        public bool     skip = false;
        public string   path      { get { return UIGenericViewItem.GetItemPath(rootView, transform); } }

        private Transform   rootView;

        public List<UIGenericViewItem> items { get; private set; }
        public Dictionary<string, UIGenericViewItem>    itemsDic;

        public List<UIGenericView> subViews { get; private set; }
        public Dictionary<string, UIGenericView>    subViewsDic;

        void Awake () 
        {
            if (itemsDic == null)
                Init(transform);
        }

        public void Init(Transform rootView)
        {            
            this.rootView = rootView;

            subViews = new List<UIGenericView>(transform.childCount);
            items = new List<UIGenericViewItem>(transform.childCount + 1);

            FindItemsInChildren(transform);

            var rootItem = transform.GetComponent<UIGenericViewItem>();
            if (rootItem != null)
                items.Add(rootItem);

            itemsDic = new Dictionary<string, UIGenericViewItem>(items.Count);
            subViewsDic = new Dictionary<string, UIGenericView>(subViews.Count);

            // Safely init dictionary with items
            foreach (var item in items)
            {
                if (item != null && item.alias != null) // Can be null in Editor.
                {
                    if (itemsDic.ContainsKey(item.alias))
                    {
                        var prevItem = itemsDic[item.alias];
                        Debug.LogErrorFormat("duplicate alias {0} \n {1} \n {2}", item.alias, item.path, prevItem.path);
                    }
                    else
                    {
                        item.Init(transform);
                        itemsDic.Add(item.alias, item);
                    }
                }
            }

            // Safely init dictionary with subviews
            foreach (var subView in subViews)
            {
                if (subView.skip)
                    continue;
                if (subViewsDic.ContainsKey(subView.alias))
                {
                    var prevItem = subViewsDic[subView.alias];
                    Debug.LogErrorFormat("duplicate alias {0} \n {1} \n {2}", subView.alias, subView.path, prevItem.path);
                }
                else
                {
                    subView.Init(transform);
                    if (!string.IsNullOrEmpty(subView.alias))
                        subViewsDic.Add(subView.alias, subView);
                }
            }
        }

        void FindItemsInChildren(Transform transform)
        {
            foreach (Transform child in transform)
            {
                var view = child.GetComponent<UIGenericView>();

                if (view == null)
                {
                    var item = child.GetComponent<UIGenericViewItem>();
                    if (item != null)
                        items.Add(item);
                    
                    FindItemsInChildren(child);
                }
                else
                {
                    subViews.Add(view);
                }
            }
        }

        public void SetButtonCallback(string alias, Action callback)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetButtonCallback(callback);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void ResetButtonCallback(string alias)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.ResetButtonCallback();
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void SetToggleCallback(string alias, Action<bool> callback)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetToggleCallback(callback);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void ResetToggleCallback(string alias)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.ResetToggleCallback();
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void SetToggelOn(string alias, bool isOn)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetToggleOn(isOn);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void SetSliderValue(string alias, float value)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetSliderValue(value);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public GameObject[] GetGameObjects(string alias, int count)
        {
            var objects = new GameObject[count];
            for(int i = 0; i < 3; i++)
            {
                objects[i] = GetGameObject(alias + (i + 1).ToString());
            }

            return objects;
        }

        public GameObject GetGameObject(string alias, bool noLogError = false)
        {
            var trans = GetTransform(alias, noLogError);
            return trans != null ? trans.gameObject : null;
        }

        public Transform GetTransform(string alias, bool noLogError = false)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                return item.transform;
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    return view.transform;
                }
                else if (!noLogError)
                {
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
                }
            }
            return null;
        }

        public T GetComponent<T>(string alias) where T : Component
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                return item.GetComponent<T>();
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    return view.GetComponent<T>();
                }
                else
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
            return null;
        }

        public T GetComponentInChildren<T>(string alias) where T : Component
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                return item.GetComponentInChildren<T>();
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    return view.GetComponentInChildren<T>();
                }
                else
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
            return null;
        }

        public void SetActive(string alias, bool value = true)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.gameObject.SetActive(value);
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    view.gameObject.SetActive(value);
                }
                else
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public bool GetActive(string alias)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                return item.gameObject.activeSelf;
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    return view.gameObject.activeSelf;
                }
                else
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }

            return false;
        }

        public void SetActiveParent(string alias, bool value = true)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                var parent = item.transform.parent;
                parent.gameObject.SetActive(value);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void SetActiveChildren(string alias, bool value = true)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                var parent = item.transform;

                for (int i = 0; i < parent.childCount; i++)
                {
                    var child = parent.GetChild(i);
                    child.gameObject.SetActive(value);
                }
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void SetSprite(string alias, Sprite sprite)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetSprite(sprite);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }
            
        public void SetText(string alias, string text)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.SetText(text);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void ShowAnimated(string alias, bool show)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                item.ShowAnimated(show);
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public void HideSubViews()
        {
            foreach (var subView in subViews)
                subView.gameObject.SetActive(false);
        }

        public void HideItems()
        {
            foreach (var item in items)
                item.gameObject.SetActive(false);
        }

        public void HideChildren(string alias)
        {
            UIGenericViewItem item;
            if (itemsDic.TryGetValue(alias, out item))
            {
                foreach (Transform child in item.transform)
                    child.gameObject.SetActive(false);
            }
            else
            {
                UIGenericView view;
                if (subViewsDic.TryGetValue(alias, out view))
                {
                    foreach (Transform child in view.transform)
                        child.gameObject.SetActive(false);
                }
                else
                    Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }
        }

        public UIGenericView GetSubView(string alias)
        {
            UIGenericView view;
            if (subViewsDic.TryGetValue(alias, out view))
            {
                return view;
            }
            else
            {
                Debug.LogError(string.Format("Wrong alias: '{0}'.", alias));
            }

            return null;
        }

        public UIGenericView IsolateSubView(string alias)
        {
            HideSubViews();
            SetActive(alias, true);
            return GetSubView(alias);
        }

        void DestroyChildren(Transform transform)
        {
            foreach (Transform child in transform)
            {
                DestroyObject(child.gameObject);
            }
        }

        public void DestroyChildren(string alias = "")
        {
            if (alias == string.Empty)
            {
                DestroyChildren(transform);
            }
            else if (itemsDic.ContainsKey(alias))
            {
                DestroyChildren(itemsDic[alias].transform);
            }
            else if (subViewsDic.ContainsKey(alias))
            {
                DestroyChildren(subViewsDic[alias].transform);
            }
            else
                Debug.LogError("DestroyChildren Wrong alias " + alias);

        }

        public void Rename(string alias, string newAlias)
        {
            if (itemsDic.ContainsKey(alias))
            {
                var oldItem = itemsDic[alias];
                oldItem.alias = newAlias;
                itemsDic.Add(newAlias, oldItem);
                itemsDic.Remove(alias);
                oldItem.gameObject.name = newAlias;
            }
            else if (subViewsDic.ContainsKey(alias))
            {
                var oldView = subViewsDic[alias];
                oldView.alias = newAlias;
                subViewsDic.Add(newAlias, oldView);
                subViewsDic.Remove(alias);
                oldView.gameObject.name = newAlias;
            }
            else
                Debug.LogError("Rename wrong alias " + alias);
        }

        public void Duplicate(string alias, string newAlias)
        {
            if (itemsDic.ContainsKey(alias))
            {
                Duplicate(itemsDic[alias].gameObject, newAlias);
            }
            else if (subViewsDic.ContainsKey(alias))
            {
                Duplicate(subViewsDic[alias].gameObject, newAlias);
            }
            else
                Debug.LogError("Rename wrong alias " + alias);
        }

        GameObject Duplicate(GameObject obj, string alias)
        {
            var newObj = Instantiate(obj, obj.transform.parent);
            var item = newObj.GetComponent<UIGenericViewItem>();
            if (item != null)
            {
                item.alias = alias;
                itemsDic.Add(alias, item);
            }
            else
            {
                var subView = newObj.GetComponent<UIGenericView>();
                if (subView != null)
                {
                    subView.alias = alias;
                    subViewsDic.Add(alias, subView);
                }
                else
                {
                    Debug.LogError("UIGenericView : can't duplicate alias " + alias);
                    Destroy(newObj);
                    return null;
                }
            }

            newObj.name = alias;

            return newObj;
        }
    }
}