﻿namespace View.UI.Base
{
    public class UITabControlledList<T> 
        : UIBaseViewList<T> where T : UIBaseViewListItem
    {
        public delegate void OnTabSwitchAction(int tabIndex);

        public string[] tabsGenericAliases;
        public OnTabSwitchAction onTabSwitchedCallback;
        public int activeTab { get; private set; }

        protected virtual void Awake()
        {
            foreach (var aliase in tabsGenericAliases)
            {
                genericView.SetToggleCallback(aliase, (a) => { if (a) OnTabSwitched(aliase); });
            }
        }

        private void OnTabSwitched(string tabAliase)
        {
            var ind = -1;
            for (var i = 0; i < tabsGenericAliases.Length; i++)
            {
                if (tabsGenericAliases[i] == tabAliase) ind = i;
            }
            activeTab = ind;
            if (onTabSwitchedCallback != null) onTabSwitchedCallback(ind);
        }

        public void SwitchTab(int tabIndex)
        {
            genericView.SetToggelOn(tabsGenericAliases[tabIndex], true);
        }
    }
}