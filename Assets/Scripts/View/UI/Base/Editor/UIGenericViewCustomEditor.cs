﻿using UnityEngine;
using UnityEditor;

namespace View.UI.Base
{
    [CustomEditor(typeof(UIGenericView))]
    public class UIGenericViewCustomEditor : Editor
    {
        bool showChildren = false;

        public override void OnInspectorGUI()
        {
            if (target == null)
                return;
            
            var t = (UIGenericView)target;

            var width1 = 50;
            var width2 = 160;
            var width3 = 80;
            //var width4 = 300;

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label ("alias", GUILayout.Width (width1));
            t.alias = EditorGUILayout.TextField( t.alias);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label ("skip", GUILayout.Width (width1));
            t.skip  = EditorGUILayout.Toggle(t.skip);
            EditorGUILayout.EndHorizontal();

            showChildren = EditorGUILayout.Foldout(showChildren, "Children");

            if (showChildren)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label ("TYPE", GUILayout.Width (width1));
                GUILayout.Label ("ALIAS", GUILayout.Width (width2));
                GUILayout.Label ("VALUE", GUILayout.Width (width3));
                GUILayout.Label ("PATH");
                EditorGUILayout.EndHorizontal();

                t.Init((t.transform.parent != null)
                        ? t.transform.parent.GetComponentInParent<UIGenericView>()?.transform
                        : t.transform);

                foreach (var subView in t.subViews)
                {
                    subView.Init(t.transform);

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label ("[view]", GUILayout.Width (width1));
                    GUILayout.Label (subView.alias, GUILayout.Width (width2));
                    GUILayout.Label ("", GUILayout.Width (width3));
                    GUILayout.Label (subView.path);
                    EditorGUILayout.EndHorizontal();
                }

                foreach (var item in t.items)
                {
                    item.Init(t.transform);

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label (item.itemType, GUILayout.Width (width1));
                    GUILayout.Label (item.alias, GUILayout.Width (width2));
                    GUILayout.Label (item.value, GUILayout.Width (width3));
                    GUILayout.Label (item.path);
                    EditorGUILayout.EndHorizontal();
                }
            }
        }
    }
}