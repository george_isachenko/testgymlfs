﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.Text;

namespace View.UI.Base
{
    [ExecuteInEditMode]
    [Serializable]
    [DisallowMultipleComponent]
    public class UIGenericViewItem : MonoBehaviour
    {
        public string   alias;

        private Transform   rootView;

        private Image       img;
        private Text        txt;
        private Button      btn;
        private Toggle      tgl;
        private Slider      sld;
        private UIAnimator  ani;

        public Sprite sprite    { set { SetSprite(value); } }
        public string itemType  { get { return GetItemType(); } }
        public string path      { get { return GetItemPath(rootView, transform); } }
        public string value     { get { return GetValue(); } }

        public void Init(Transform rootView)
        {
            this.rootView = rootView;

            img = GetComponent<Image>();
            txt = GetComponent<Text>();
            btn = GetComponent<Button>();
            tgl = GetComponent<Toggle>();
            sld = GetComponent<Slider>();
            ani = GetComponent<UIAnimator>();
        }

        string GetItemType()
        {
            if (tgl != null)
                return "[tgl]";
            if (btn != null)
                return "[btn]";
            if (txt != null)
                return "[txt]";
            if (img != null)
                return "[img]";
            
            return "[obj]";
        }

/*
        public static string GetGameObjectPath(Transform transform)
        {
            string path = transform.name;
            while (transform.parent != null)
            {
                if (transform.parent.GetComponent<UIGenericView>() != null)
                    return path;
                
                transform = transform.parent;
                path = transform.name + "/" + path;
            }
            return path;
        }
*/

        public static string GetItemPath(Transform rootViewTransform, Transform transform)
        {
            var pathBuilder = new StringBuilder(128);

            while (transform.parent != null && !ReferenceEquals(rootViewTransform, transform))
            {
                if (pathBuilder.Length > 0)
                    pathBuilder.Insert(0, "/");
                pathBuilder.Insert(0, transform.name);

                transform = transform.parent;
            }
            return pathBuilder.ToString();
        }
    
        string GetValue()
        {
            if (txt != null)
                return txt.text;
            if (img != null)
                return "";

            return "";
        }

        public void SetButtonCallback(Action callback)
        {
            if (btn != null)
            {
                btn.onClick.RemoveAllListeners();
                if (callback != null)
                    btn.onClick.AddListener(new UnityAction(callback));
            }
            else
            {
                Debug.LogErrorFormat("No button available: {0}, {1}", alias, path);
            }
        }

        public void ResetButtonCallback()
        {
            if (btn != null)
            {
                btn.onClick.RemoveAllListeners();
            }
            else
            {
                Debug.LogErrorFormat("No button available: {0}, {1}", alias, path);
            }
        }

        public void SetToggleCallback(Action<bool> callback)
        {
            if (tgl != null)
            {
                tgl.onValueChanged.RemoveAllListeners();
                tgl.onValueChanged.AddListener(new UnityAction<bool>(callback));
            }
            else
            {
                Debug.LogErrorFormat("No toggle available: {0}, {1}", alias, path);
            }
        }

        public void ResetToggleCallback()
        {
            if (tgl != null)
            {
                tgl.onValueChanged.RemoveAllListeners();
            }
            else
            {
                Debug.LogErrorFormat("No toggle available: {0}, {1}", alias, path);
            }
        }

        public void SetToggleOn(bool isOn)
        {
            if (tgl != null)
            {
                tgl.isOn = isOn;
            }
            else
            {
                Debug.LogErrorFormat("No button available: {0}, {1}", alias, path);
            }
        }

        public void SetSliderValue(float value)
        {
            if (sld != null)
            {
                sld.value = value;
            }
            else
            {
                Debug.LogErrorFormat("No slider available: {0}, {1}", alias, path);
            }
        }

        public void SetSprite(Sprite sprite)
        {
            if (img != null)
            {
                img.sprite = sprite;
            }
            else
            {
                Debug.LogErrorFormat("No image available: {0}, {1}", alias, path);
            }
        }

        public void SetText(string text)
        {
            if (txt != null)
            {
                txt.text = text;
            }
            else
            {
                Debug.LogErrorFormat("No text available: {0}, {1}", alias, path);
            }
        }

        public void ShowAnimated(bool show)
        {
            if (ani != null)
            {
                ani.Show(show);
            }
            else
            {
                Debug.LogErrorFormat("No animation available: {0}, {1}", alias, path);
            }
        }

    }
}
