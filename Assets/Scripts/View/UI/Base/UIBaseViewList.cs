﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace View.UI.Base
{
    public class UIBaseViewList<T> : UIBaseView where T : UIBaseViewListItem
    {

        protected Transform itemsRoot;

        [HideInInspector] public    List<T> items       = new List<T>();
        private                     List<T> itemsAll    = new List<T>();

        public GameObject itemPrefab;

        public T selectedItem { get; private set; }

        public void SetSelectedItem(T item, bool needUpdate)
        {
            selectedItem = item; 
            UpdateSelectection (needUpdate);                        
        }

        public delegate void OnItemSelectedDelegate(T item);
        [HideInInspector] public OnItemSelectedDelegate OnItemSelected_Callback;

        public void UpdateItemsCount(int count)
        {
            if (items.Count == count)
                return;

            if (itemsAll.Count < count)
            {
                int diff = count - itemsAll.Count;
                for (int i = 0; i < diff; i++)
                    CreateNewItem();
            }

            items.Clear();

            foreach (var item in itemsAll)
                item.gameObject.SetActive(false);

            for (int i = 0; i < count; i++)
            {
                itemsAll[i].gameObject.SetActive(true);
                items.Add(itemsAll[i]);
            }
        }

        protected virtual T CreateNewItem()
        {
            T viewItem = UIBaseView.InstantiatePrefab<T>(itemPrefab, itemsRoot, "item_" + itemsAll.Count);

            viewItem.OnClickCallback = OnItemClick;
            viewItem.selected = false;
            itemsAll.Add(viewItem);
            return viewItem;
        }

        void OnItemClick(UIBaseViewListItem item)
        {
            SetSelectedItem (item as T, true);
        }
            
        protected void UpdateSelectection(bool needUpdate)
        {
            SetSelected(selectedItem);

            if (needUpdate) 
            {
                OnItemSelectionUpdate ();
            }
        }

        protected virtual void OnItemSelectionUpdate()
        {
            if (OnItemSelected_Callback != null)
            {
                OnItemSelected_Callback(selectedItem);
            }
        }
            
        private void SetSelected(UIBaseViewListItem item){
            foreach (var it in items)
            {
                it.selected = it == item;
            }
        }

        public T GetItemByIndex(int index)
        {
            return itemsAll.FirstOrDefault(item => item.idx == index);
        }
    }
}
