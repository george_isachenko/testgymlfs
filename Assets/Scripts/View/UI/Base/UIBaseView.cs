﻿using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using View.Actions;
using UnityEngine.Assertions;
using View.UI.Base;

namespace View.UI.Base
{
    [DisallowMultipleComponent]
    [RequireComponent (typeof (ButtonInputHandler))]
    public class UIBaseView : UIGenericMonoBehaviour, ICancelHandler
    {
        public bool   isModal;
        public bool   hideOverhead;
        [HideInInspector] public bool enableProcessButtonHandles = true;

        public UnityEvent OnShowHandler;
        public UnityEvent OnHideHandler;

        public Action OnShow_WndManager;
        public Action OnHide_WndManager;
        public Action OnShow_Callback;
        public Action OnHide_Callback;
        public Action OnUpdateContentOnShow_Callback;

        public Action OnClosedByUserClick_Callback;

        public event Action onShowByCityClick;

        public bool     isVisible       { get { return gameObject.activeSelf; } }

        private static GUIRoot guiRoot_ = null;

        private GUIRoot guiRoot
        {
            get
            {
                if (guiRoot_ != null)
                    return guiRoot_;
                guiRoot_ = GameObject.Find("_GUIRoot").GetComponent<GUIRoot>();
                    return guiRoot_;
            }
        } 

        protected void Start()
        {
            var buttonInputHandler = GetComponent<ButtonInputHandler>();
            if (buttonInputHandler)
                buttonInputHandler.onButtonUp.AddListener(() => { if (enableProcessButtonHandles) HideByUserClick(); });
        }

        public void ShowByCityClick()
        {
            onShowByCityClick?.Invoke();
        }

        public void Show()
        {
            Show(true);
        }

        public virtual void Show(bool show)
        {
            if (show)
            {
                if (OnShow_WndManager != null)
                    OnShow_WndManager.Invoke();
                else
                    Debug.LogError("WindowManager : onShow == null");

                RectTransform rt = gameObject.GetComponent<RectTransform>();
                rt.localScale = Vector3.zero;
                LeanTween.scale(rt, Vector3.one, guiRoot.windowOpeningTime).setEase(guiRoot.windowOpeningType);

            }
            else
            {
                if (OnHide_WndManager != null)
                    OnHide_WndManager.Invoke();
                else
                    Debug.LogError("WindowManager : onHide == null");
            }
        }

        public void ShowHideInternal(bool show)
        {
            var wasActive = gameObject.activeSelf;

            if (wasActive != show)
            {
                gameObject.SetActive(show);

                if (show)
                {
                    OnShow();
                }
                else
                {
                    OnHide();
                }
            }
        }

        public void Hide()
        {
            Show(false);
        }

        public virtual void HideByUserClick()
        {
            if (OnClosedByUserClick_Callback != null)
                OnClosedByUserClick_Callback.Invoke();
            Hide();
        }

        protected void BindCloseButton()
        {
            var obj = transform.Find("BtnClose");

            if (obj != null)
            {
                var btnClose = obj.GetComponent<Button>();
                btnClose.onClick.AddListener(HideByUserClick);
                return;
            }

            if (genericView != null)
            {
                if (genericView.itemsDic.ContainsKey("btnClose"))
                {
                    genericView.SetButtonCallback("btnClose", HideByUserClick);
                }
            }
        }

        protected virtual void OnShow()
        {
            if (OnUpdateContentOnShow_Callback != null)
                OnUpdateContentOnShow_Callback.Invoke();
            if (OnShow_Callback != null)
                OnShow_Callback.Invoke();
            if (OnShowHandler != null)
                OnShowHandler.Invoke();
        }

        protected virtual void OnHide()
        {
            if (OnHide_Callback != null)
                OnHide_Callback.Invoke();
            if (OnHideHandler != null)
                OnHideHandler.Invoke();
        }

        void ICancelHandler.OnCancel(BaseEventData eventData)
        {
            Debug.Log("OnCancel");
            Hide();
        }

        public static GameObject InstantiatePrefab(GameObject prefab, Transform parent, string name, float scale = 1.0f)
        {
            var obj = (GameObject)Instantiate(prefab);
            obj.transform.SetParent(parent, false);
            obj.transform.localScale = UnityEngine.Vector3.one * scale;
            obj.name = name;

            return obj;
        }

        public static T InstantiatePrefab<T>(GameObject prefab, Transform parent, string name, float scale = 1.0f)
        {
            if (prefab == null)
            {
                Debug.LogErrorFormat("UIBaseView : can't instantiate prefab '{1}' on parent '{0}'.", parent.name, name);
            }

            var obj = InstantiatePrefab(prefab, parent, name, scale);


            var genericView = obj.GetComponent<UIGenericView>();
            if (genericView != null)
                genericView.alias = name;
            
            var genericItem = obj.GetComponent<UIGenericViewItem>();
            if (genericItem != null)
                genericItem.alias = name;

            return obj.GetComponent<T>();
        }
            

        public void CenterScrollToItem(RectTransform obj)
        {
            var scroll = GetComponentInChildren<ScrollRect>();
            if (scroll)
            {
                var scrollTransform = scroll.transform as RectTransform;

                var halfHeight = 1 / (float)scroll.content.transform.childCount / 1.15f;

                float normalizePosition = scrollTransform.anchorMin.y - obj.anchoredPosition.y;
                normalizePosition += (float)obj.transform.GetSiblingIndex() / (float)scroll.content.transform.childCount;
                normalizePosition /= 1000f;                                                         // what does 1000 mean?
                normalizePosition = Mathf.Clamp01(1 - normalizePosition + halfHeight);

                ScrollToNormalizedPos(normalizePosition);
            }
        }

        void ScrollToNormalizedPos(float normalizedPosition)
        {
            var scroll = GetComponentInChildren<ScrollRect>();

            if (scroll == null)
            {
                Debug.LogError("ScrollToNormalizedPos : no ScrollRect found");
                return;
            }

            if (scroll.vertical)
            {
                scroll.verticalNormalizedPosition = normalizedPosition;
            }
            else
            {
                scroll.horizontalNormalizedPosition = normalizedPosition;
            }
                   
        }

        protected Transform DefaultItemsRoot()
        {
            if (genericView != null)
            {
                var itemsRoot = genericView.GetTransform("items", true);
                if (itemsRoot != null)
                    return itemsRoot;
            }

            return transform.Find("Items").Find("Scroll View").Find("Viewport").Find("Content");
        }

#if UNITY_EDITOR
        [ContextMenu("Destroy all generic views in children")]
        public void DBG_DestroyGenerics()
        {
            if (Application.isPlaying)
            {
                Debug.LogWarning("Can be used only in edit mode.");
                return;
            }
            var count = 0;
            foreach (var inChild in transform.GetComponentsInChildren<UIGenericViewItem>(true))
            {
                Undo.DestroyObjectImmediate(inChild);
                count++;
            }
            Debug.LogFormat("{0} UIGenericViewItem's deleted", count);
        }
#endif
    }
}
