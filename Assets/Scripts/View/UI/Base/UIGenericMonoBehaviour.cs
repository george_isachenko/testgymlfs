﻿using UnityEngine;

namespace View.UI.Base
{
    public abstract class UIGenericMonoBehaviour : MonoBehaviour
    {
        private UIGenericView _genericView;

        public UIGenericView genericView { get { return GetGenericView(); } }

        UIGenericView GetGenericView ()
        {
            if (_genericView == null)
            {
                _genericView = GetComponent<UIGenericView>();
                if (_genericView != null)
                {
                    _genericView.Init((_genericView.transform.parent != null)
                        ? _genericView.transform.parent.GetComponentInParent<UIGenericView>()?.transform
                        : _genericView.transform);
                }
            }

            return _genericView;
        }
    }
}