﻿using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI.Base
{
    public class UIBaseViewListItem : UIGenericMonoBehaviour
    {
        [HideInInspector] public int    idx;

        public delegate void OnUIBaseViewListItemClickDelegate(UIBaseViewListItem item);
        [HideInInspector] public OnUIBaseViewListItemClickDelegate OnClickCallback;

        protected string buyButtonAlias = null;

        protected virtual void Awake()
        {
            if (buyButtonAlias == null)
            {
                var btn = GetComponentInChildren<Button>();

                if (btn != null)
                {
                    btn.onClick.AddListener(
                        delegate()
                        {
                            if (OnClickCallback != null)
                                OnClickCallback.Invoke(this);
                        });
                }
            }
            else
            {
                genericView.SetButtonCallback(buyButtonAlias, () => OnClickCallback(this));
            }
        }

        [HideInInspector] public virtual bool selected { get; set;}    
    }
}
