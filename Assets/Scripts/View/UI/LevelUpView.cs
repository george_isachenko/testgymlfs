﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Data;
using View.UI.Base;

namespace View.UI
{
    public class LevelUpView : UIBaseViewList<LevelUpUnlockItem>
    {

        Text txtInfo;
        Text txtRewardCoins;
        public Button btnContinue{ get; private set;}

        public int level
        {
            set { txtInfo.text = Loc.Get("levelUpYouReachedlevel", value.ToString()); }
        }

        public int rewardCoins { set { txtRewardCoins.text = value.ToString(); } }

        void Awake()
        {
            itemsRoot = DefaultItemsRoot();
            BindCloseButton();

            txtInfo = transform.Find("TxtInfo").GetComponent<Text>();
            txtRewardCoins = transform.Find("RewardCoins").Find("Reward").GetComponent<Text>();
            btnContinue = transform.Find("BtnClose").GetComponent<Button>();
        }

    }
}