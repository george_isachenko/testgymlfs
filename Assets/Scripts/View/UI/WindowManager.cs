﻿using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using View.CameraHelpers;
using UnityEngine.Assertions;

namespace View.UI
{
    public enum WindowTypes
    {
        None,

        Shop,
        Settings,
        PopUp,
        ConfirmWithBucks,
        HUD,
        DailyQuests,
        DailyBonus,
        VisitorQuests,
        LevelUp,
        TutorialMsg,
        Storage,
        BuyWithResources,
        SportShop,
        SportsmansList,
        SportLaneQuest,
        SportsmenAgency,
        SportTimeQuest,
        SportsmanTraining,
        GymUnlock,
        ShopUltimate,
        RateUs,
        Tooltip,
        FriendsList,
        Promo,
        RankAchievements,
        RankLvlUp,
        StyleWindow,
        UpgradeEquipment,
        HQ,
        SportClubLocked,
        JuiceBarLocked,
        MakeJuice,
        SportAgencyLocked,
        Trainers,
        TrainersInfoView
    }

    public delegate void OnWindowOpened_Delegate(bool isModal, bool hideOverheadUI);
    public delegate void OnWindowOpened_Delegate2(WindowTypes type, UIBaseView wnd);
    public delegate void OnWindowClose_Delegate(WindowTypes type);

    public class WindowManager  {
        private Dictionary<WindowTypes, UIBaseView> wnds = new Dictionary<WindowTypes, UIBaseView>();
//        private List<UIBaseView>                    stack = new List<UIBaseView>();
        private Canvas _overheadUICanvas;
        private HUD.HUD _hud;
        private WindowTypes curType = WindowTypes.None;
        private GUIRoot gui;
        private bool recursionShowGuard = false;
        private bool preventWindowClose = false;

        public event OnWindowOpened_Delegate OnWindowOpened_Callback;
        public event OnWindowOpened_Delegate2 OnWindowOpenedEvent;
        public event OnWindowClose_Delegate OnWindowCloseEvent;

        public HUD.HUD hud { get { return _hud; } }

        public void SetGuiRoot(GUIRoot gui_)
        {
            gui = gui_;
        }

        private bool overheadUIOn
        {
            set
            {
                if (_overheadUICanvas != null)
                    _overheadUICanvas.enabled = (value);
                else
                    Debug.LogError("OverheadUI not connected to WindowManager");
            }
        }

        public struct TStates
        {
            public bool inEditMode
            {
                get; set;
            }

            public void Reset()
            {
                inEditMode = false;
            }
        }

        public TStates states;

        public WindowManager(Canvas overheadUICanvas, HUD.HUD hudObj, GUIRoot gui)
        {
            Assert.IsNotNull(gui);
            Assert.IsNotNull(overheadUICanvas);
            Assert.IsNotNull(gui);
            this.gui = gui;
            _overheadUICanvas = overheadUICanvas;
            _hud = hudObj;
        }

        public void AddWindow(WindowTypes id, MonoBehaviour mb)
        {
            Assert.IsNotNull(mb);

            if (mb == null)
            {
                Debug.LogError("AddWindow mb is NULL for " + id.ToString());
            }

            var go = mb.gameObject;
            // it is important to make window active on initialization to make sure Awake called for sub-gameObjects
            go.SetActive(true);

            UIBaseView v = go.GetComponent<UIBaseView>();
        
            if (v != null)
            {
                v.OnShow_WndManager = delegate() { Show(id); };
                v.OnHide_WndManager = delegate() { ShowDefault(); };
                wnds.Add(id, v);

                /*
                if (stackOn)
                {
                    v.OnPush_Callback = delegate() { Push(id); };
                    v.OnPop_Callback = TryPop;
                }*/
            }
            else
            {
                Debug.LogError("WindowManager: object not inherited from UIBaseView");
            }
        }

        /*
        private void Push(WindowTypes id)
        {
            var wnd = wnds[id];

            stack.Add(wnd);
            wnd.ShowHideInternal(true);

            UpdateStackOrder();
        }

        private void UpdateStackOrder()
        {
            if (stack.Count == 0)
                return;

            var last = stack[stack.Count - 1];
            
            last.transform.SetAsLastSibling();
        }

        private bool TryPop(UIBaseView view)
        {
            if (stack.Count == 0)
                return false;

            var last = stack[stack.Count - 1];

            if (last == view)
            {
                Pop();
                return true;
            }
            else
                return false;
        }

        private UIBaseView Pop()
        {
            if (stack.Count == 0)
                return null;
            
            var last = stack[stack.Count - 1];
            last.ShowHideInternal(false);
            stack.Remove(last);

            UpdateStackOrder();

            return last;
        }*/

        private void Hide(WindowTypes id)
        {
            wnds[id].ShowHideInternal(false);
        }


        private void Show(WindowTypes id)
        {
            if (preventWindowClose)
                return;

            if (recursionShowGuard)
            {
                if (id == WindowTypes.None)
                {
                    Debug.LogFormat("Show/Hide recursion, request to hide windows is ignored! Stack:\n\n.{0}"
                        , StackTraceUtility.ExtractStackTrace());
                }
                else
                {
                    Debug.LogWarningFormat("Show/Hide recursion, request to show {0} is ignored! Stack:\n\n.{0}"
                        , id, StackTraceUtility.ExtractStackTrace());
                }
                return;
            }

            if (states.inEditMode && id != WindowTypes.PopUp && id != WindowTypes.None)
                return;

            recursionShowGuard = true;

            hud.wndMode = (id != WindowTypes.None);

            var wasOpened = curType;

            if (curType != WindowTypes.None)
            {
                CloseOpened ();
            }
                
            foreach (var wnd in wnds)
            {
                wnd.Value.ShowHideInternal( wnd.Key == id );
            }

            if (id == WindowTypes.None)
            {
                hud.modalMode = false;
                hud.modalModeTutorial = false;
                hud.modalModeConfirm = false;
                overheadUIOn = true;

                gui.SetRenderUIOnly(false);

                if (CameraController.instance.isCameraStashed)
                    CameraController.instance.UnstashCameraTransform();
            }
            else
            {
                if (id == WindowTypes.PopUp || id == WindowTypes.BuyWithResources)
                {
                    hud.modalModeConfirm = true;
                    hud.modalMode = false;
                }
                else
                    hud.modalMode = wnds[id].isModal;

                if (id == WindowTypes.TutorialMsg)
                {
                    hud.modalMode = false;
                    hud.modalModeTutorial = true;
                    hud.modalModeConfirm = false;
                }
                
                overheadUIOn = !wnds[id].hideOverhead;

                if (OnWindowOpened_Callback != null)
                    OnWindowOpened_Callback(wnds[id].isModal, wnds[id].hideOverhead);

                if (OnWindowOpenedEvent != null)
                    OnWindowOpenedEvent(id, wnds[id]);

                Sound.instance.WindowOpen();

                if (id == WindowTypes.ShopUltimate)
                    gui.SetRenderUIOnly(true);
                else
                    gui.SetRenderUIOnly(false);

                Core.PersistentCore.instance.analytics.DetailGameEvent("Window Open", "Window Name", id.ToString());
            }

            curType = id;

            CameraReaction();

            recursionShowGuard = false;

            if (wasOpened != WindowTypes.None && id == WindowTypes.None)
            {
                if (OnWindowCloseEvent != null)
                    OnWindowCloseEvent(wasOpened);
            }

        }

        public WindowTypes GetCurrentType()
        {
            return curType;
        }

        public void ShowDefault()
        {
            Show(WindowTypes.None);
            hud.UpdateHUD();
        }

        void CloseOpened()
        {
            //TODO: hm..
            curType = WindowTypes.None;         
        }

        public void CloseCurrentByUserClick()
        {
            if (preventWindowClose)
                return;

            if (curType != WindowTypes.None)
                this.wnds[curType].HideByUserClick();
        }

        void CameraReaction()
        {
            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl == null)
                return;

            camCtrl.states.windowOpened = ((curType != WindowTypes.None) && (curType != WindowTypes.VisitorQuests) && (curType != WindowTypes.SportsmansList)); 

            /*
            if (curType == WindowTypes.Storage)
            {
                //Transform tTr = GameObject.Find("_Rooms").transform.Find("Storage");
                //if (tTr != null)

                camCtrl.AutoMoveTo(-1.8f, -0.12f);
            }
            else if (curType == WindowTypes.SportLaneQuest)
            {
                camCtrl.AutoMoveTo(1.7f, -3.58f);
            }
            else if (curType == WindowTypes.SportTimeQuest)
            {
                camCtrl.AutoMoveTo(1.8f, -4.47f);
            }
            else if (curType == WindowTypes.SportShop)
            {
                camCtrl.AutoMoveTo(1.83f, -7.14f);
            }
            else if (curType == WindowTypes.SportsmenAgency)
            {
                camCtrl.AutoMoveTo(2.9f, 0.41f);
            }
            */
        }

        /*public bool IsCanAutoClose()
        {
            if (curType == WindowTypes.PopUp)
            {
                if (gui.confirmWnd.CurType == ConfirmWnd.ConfirmTypes.Reward)
                    return false;
            }

            return true;
        }*/

        public void SetIgnoreButtonHandlingMode(bool enabled)
        {
            foreach (var wnd in wnds)
            {
                wnd.Value.enableProcessButtonHandles = !enabled;
            }
        }

        public void PreventWindowClose(bool val)
        {
            preventWindowClose = val;
        }
    }
}
