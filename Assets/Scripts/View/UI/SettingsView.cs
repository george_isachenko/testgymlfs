﻿using System;
using System.Collections;
using Data;
using UnityEngine;
using UnityEngine.UI;
using Logic.Quests;
using UnityEngine.Events;
using Core;
using Core.SocialAPI;
using View.UI.Base;

namespace View.UI
{
    public class SettingsView : UIBaseView
    {
        public enum SettingsTabs
        {
            None,
            Main,
            Localization,
            Tatem,
            Friends
        }

        public bool musicOn
        {
            set { genericView.GetComponent<Toggle>("musicToggle").isOn = value; }
        }

        public bool soundOn
        {
            set { genericView.GetComponent<Toggle>("soundToggle").isOn = value; }
        }

        public UnityEvent<bool> onMusicHandler
        {
            get { return genericView.GetComponent<Toggle>("musicToggle").onValueChanged; }
        }

        public UnityEvent<bool> onSoundHandler
        {
            get { return genericView.GetComponent<Toggle>("soundToggle").onValueChanged; }
        }

        public Sprite currentLanguageSprite
        {
            set { genericView.SetSprite("languageFlagImage", value); }
        }

        public string currentLanguageName
        {
            set { genericView.SetText("currentLanguageName", value); }
        }

        public UnityEvent currentLanguageBtnClickHandler
        {
            get { return genericView.GetComponent<Button>("languageBtn").onClick; }
        }

        public UnityEvent localizationPanelBackClickHandler
        {
            get {  return genericView.GetComponent<Button>("localizationBackBtn").onClick; }
        }

        public UnityEvent tatemGamesBtnClickHandler
        {
            get {  return genericView.GetComponent<Button>("tatemGamesBtn").onClick; }
        }

        public UnityEvent moreGamesBtnClickHandler
        {
            get {  return genericView.GetComponent<Button>("moreGamesBtn").onClick; }
        }

        public Button btnFriendship
        {
            get {  return genericView.GetComponent<Button>("friendBtn"); }
        }

        public Button btnFeedBack
        {
            get {  return genericView.GetComponent<Button>("feedBackBtn"); }
        }

        public SettingsLanguagesList languagesList { get; private set; }

        //public readonly UnityEvent btnFeedBackClick = new UnityEvent();
/*
        public readonly UnityEvent btnGooglePlayClick = new UnityEvent();
        public readonly UnityEvent btnGameCenterClick = new UnityEvent();
*/

        public Action OnConnectFacebook     { set { genericView.SetButtonCallback("FacebookButton", value); } }
        public Action OnConnectGameCenter   { set { genericView.SetButtonCallback("GameCenterButton", value); } }
        public Action OnConnectGooglePlay   { set { genericView.SetButtonCallback("GooglePlayButton", value); } }

        private bool gameCenterOn { set { genericView.SetActive("GameCenterButton", value); } }
        private bool googlePlayOn { set { genericView.SetActive("GooglePlayButton", value); } }

        public bool facebookBtnInteractable { set { genericView.GetComponent<Selectable>("FacebookButton").interactable = value; } }
        public bool gameCenterBtnInteractable { set { genericView.GetComponent<Selectable>("GameCenterButton").interactable = value; } }
        public bool googlePlayBtnInteractable { set { genericView.GetComponent<Selectable>("GooglePlayButton").interactable = value; } }

        private string facebookButtonLabel { set { genericView.SetText("FacebookButtonLabel", value); } }
        private string gameCenterButtonLabel { set { genericView.SetText("GameCenterButtonLabel", value); } }
        private string googlePlayButtonLabel { set { genericView.SetText("GooglePlayButtonLabel", value); } }

        public Action OnFeedback            { set { genericView.SetButtonCallback("btnFeedback", value); } }

        private string playerId { set { genericView.SetText("playerIDLabel", value); } }

        private SettingsTabs currentTab = SettingsTabs.None;
        private IEnumerator creditsScroll = null;

        void Awake ()
        {
            //genericView.SetButtonCallback("feedBackBtn", () => btnFeedBackClick.Invoke());
/*
            genericView.SetButtonCallback("googlePlusConnectBtn", () => btnGooglePlayClick.Invoke());
            genericView.SetButtonCallback("gameCenterBtn", () => btnGameCenterClick.Invoke());
*/
            genericView.SetText("clientVersionLabel",Application.version);

            languagesList =
            UIViewSubListFactory<SettingsLanguagesList, SettingsLanguagesListItem>.CreateUiList(
                genericView.GetTransform("languagesListRoot"), "UI/ListViewItems/Settings/SettingsLanguagesListItem");

            BindCloseButton();
            
            genericView.SetToggleCallback("settingsTabToggle", isOn => { if (isOn && currentTab != SettingsTabs.Localization) SwitchTab(SettingsTabs.Main); });
            genericView.SetToggleCallback("tatemTabToggle", isOn => { if (isOn) SwitchTab(SettingsTabs.Tatem); });
            genericView.SetToggleCallback("friendsTabToggle", isOn => { if (isOn) SwitchTab(SettingsTabs.Friends); });

            OnShowHandler.AddListener (UpdateView);
        }

        new void Start()
        {
            base.Start();

            UpdateSocialNetworkButtons(true);
        }

        public void SwitchTab(SettingsTabs tab)
        {
            if (currentTab == tab)
                return;

            currentTab = tab;

            genericView.GetComponent<ToggleGroup>("tabsToggleGroup").SetAllTogglesOff();
            foreach (var tabName in new[] {"settingsPanel", "localizationPanel", "tatemPanel", "friendsPanel" })
            {
                genericView.SetActive(tabName, false);
            }

            switch (tab)
            {
                case SettingsTabs.Main:
                    genericView.SetToggelOn("settingsTabToggle", true);
                    genericView.SetActive("settingsPanel");
                    break;
                case SettingsTabs.Localization:
                    genericView.SetToggelOn("settingsTabToggle", true);
                    genericView.SetActive("localizationPanel");
                    break;
                case SettingsTabs.Tatem:
                    genericView.SetToggelOn("tatemTabToggle", true);
                    genericView.SetActive("tatemPanel");
                    genericView.GetComponent<ScrollRect>("creditsScrollView").verticalNormalizedPosition = 1;
                    StartCreditsCoroutine();
                    break;
                case SettingsTabs.Friends:
                    genericView.SetToggelOn("friendsTabToggle", true);
                    genericView.SetActive("friendsPanel");
                    genericView.GetComponent<ScrollRect>("creditsScrollView").verticalNormalizedPosition = 1;
                    StartCreditsCoroutine();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tab), tab, null);
            }
        }

        public void ShowReloadHint()
        {
            genericView.SetActive("gameRestartHint");
        }

        public void StartCreditsCoroutine()
        {
            if (creditsScroll != null)
                StopCreditsCoroutine();
            creditsScroll = AutoScrollCreditsCoroutine();
            StartCoroutine(creditsScroll);
        }

        public void StopCreditsCoroutine()
        {
            if (creditsScroll == null)
                return;
            
            StopCoroutine(creditsScroll);
            creditsScroll = null;
        }

        private void UpdateView()
        {
            string profileId = PersistentCore.instance.networkController.profileId;

            if (profileId != string.Empty)
            {
                playerId = string.Format("Player ID: {0}", profileId);
            }

            {
                var facebookState = PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).GetNetworkState();
                facebookBtnInteractable = (facebookState == NetworkState.LoggedOut);
            }

            UpdateSocialNetworkButtons(false);
        }

        private void UpdateSocialNetworkButtons (bool initial)
        {
            {
                var network = PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook);

                var facebookState = PersistentCore.instance.socialManager.GetNetwork(NetworkType.Facebook).GetNetworkState();
                if (initial)
                    facebookBtnInteractable = (facebookState == NetworkState.LoggedOut);
                facebookButtonLabel = (facebookState == NetworkState.LoggedOut) ? Loc.Get("settingsConnect") : Loc.Get("settingsFBConnected");

                if (initial && facebookState != NetworkState.Unavailable)
                {
                    OnConnectFacebook = () =>
                        {
                            var state = network.GetNetworkState();
                            if (state == NetworkState.LoggedOut)
                            {
                                facebookBtnInteractable = false;
                                network.TryLogin((networkType, result) =>
                                    {
                                        if (this != null)
                                        {
                                            if (result == LoginResult.Failed)
                                            {
                                                facebookBtnInteractable = true;
                                            }
                                            else
                                            {
                                                 facebookButtonLabel = Loc.Get("settingsConnect");
                                            }
                                        }
                                    }, 
                                    ConnectionSource.Settings);
                            }
                        };
                }
            }

            {
                var network = PersistentCore.instance.socialManager.GetNetwork(NetworkType.GameCenter);

                var gameCenterState = network.GetNetworkState();
                if (gameCenterState == NetworkState.Unavailable)
                {
                    gameCenterOn = false;
                }
                else
                {
                    gameCenterOn = true;
                    //gameCenterBtnInteractable = false; //(gameCenterState == NetworkState.LoggedIn);
                }

                if (initial && gameCenterState != NetworkState.Unavailable)
                {
                    if (gameCenterState == NetworkState.LoggedOut)
                    {
                        gameCenterBtnInteractable = false;

                        network.TryLogin((networkType, result) =>
                        {
                            if (this != null)
                            {
                                if (result == LoginResult.OK)
                                {
                                    gameCenterBtnInteractable = true;
                                }
                            }
                        },
                        ConnectionSource.Settings);
                    }
                    else
                    {
                        gameCenterBtnInteractable = true;
                    }

                    OnConnectGameCenter = () =>
                    {
                        PersistentCore.instance.socialManager.GetNetwork(NetworkType.GameCenter).ShowLeaderboardWindow();
                    };
                }
            }

            {
                var network = PersistentCore.instance.socialManager.GetNetwork(NetworkType.GooglePlay);

                var googlePlayState = network.GetNetworkState();
                if (googlePlayState == NetworkState.Unavailable)
                {
                    googlePlayOn = false;
                }
                else
                {
                    googlePlayOn = true;
                }
                googlePlayButtonLabel = (googlePlayState == NetworkState.LoggedOut) ? Loc.Get("settingConnectGooglePLay") : Loc.Get("settingsDisconnect");

                if (initial && googlePlayState != NetworkState.Unavailable)
                {
                    OnConnectGooglePlay = () =>
                        {
                            var state = network.GetNetworkState();
                            if (state == NetworkState.LoggedOut)
                            {
                                googlePlayBtnInteractable = false;
                                network.TryLogin((networkType, result) =>
                                    {
                                        googlePlayBtnInteractable = true;

                                        if (this != null)
                                        {
                                            if (result == LoginResult.OK)
                                            {
                                                // googlePlayBtnInteractable = true;
                                            }
                                            else
                                            {
                                                googlePlayButtonLabel = Loc.Get("settingsDisconnect");
                                            }
                                        }
                                    },
                                    ConnectionSource.Settings);
                            }
                            else
                            {
                                googlePlayBtnInteractable = true;
                                network.Logout();
                                googlePlayButtonLabel = (googlePlayState == NetworkState.LoggedOut) ? Loc.Get("settingConnectGooglePLay") : Loc.Get("settingsDisconnect");
                            }
                        };
                }
            }
        }

        private IEnumerator AutoScrollCreditsCoroutine()
        {
            yield return new WaitForSeconds(3);
            var scroll = genericView.GetComponent<ScrollRect>("creditsScrollView");
            var pos = scroll.verticalNormalizedPosition;
            while (pos > 0)
            {
                pos -= Time.deltaTime * .3f;
                scroll.verticalNormalizedPosition = pos;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}