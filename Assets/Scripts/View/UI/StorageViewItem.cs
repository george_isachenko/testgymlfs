﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using View.UI.Base;

namespace View.UI
{
    public class StorageViewItem : UIBaseViewListItem
    {
        private Image img;
        private Text txt;

        public UIHintBehaviour  hint;

        public Sprite sprite
        {
            set { img.sprite = value; }
        }

        public int count
        {
            set { txt.text = value.ToString() + "x"; }
        }

        protected override void Awake()
        {
            base.Awake();

            hint = GetComponent<UIHintBehaviour>();
            img = transform.Find("Img").GetComponent<Image>();
            txt = transform.Find("Txt").GetComponent<Text>();
        }
    }
}
