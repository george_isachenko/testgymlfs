﻿using System;
using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI.Base;
using View.UI.OverheadUI;

namespace View.UI
{
    public class TutorialMsgView : UIGenericMonoBehaviour
    {
        private class TutorialShadowTargeter
        {
            private readonly RectTransform root;
            private readonly Camera mainCamera;

            private TutorialShadowTargeter() { }

            public TutorialShadowTargeter(RectTransform root, Camera mainCamera)
            {
                this.root = root;
                this.mainCamera = mainCamera;
            }

            public Vector2 rectSize
            {
                get { return rectSizeGetter(); }
            }

            public Vector2 rectScreenCenter
            {
                get { return rectScreenCenterGetter(); }
            }

            private Func<Vector2> rectSizeGetter;
            private Func<Vector2> rectScreenCenterGetter;

            public void SetTarget(IWorldPositionProvider provider, Vector2 targetSize, Vector2 targetOffset)
            {
                rectScreenCenterGetter = () =>
                {
                    var worldPoint = provider.GetWorldPosition();
                    var rootSize = root.rect.size;
                    var screenPos = (Vector2) mainCamera.WorldToViewportPoint(worldPoint);
                    screenPos.x *= rootSize.x;
                    screenPos.y *= rootSize.y;
                    return screenPos + targetOffset;
                };
                rectSizeGetter = () => targetSize;
            }

            public void SetTarget()
            {
                rectScreenCenterGetter = () =>
                {
                    var worldPoint = new Vector3(0, 0, 0);
                    var rootSize = new Vector2(0, 0);
                    var screenPos = (Vector2)mainCamera.WorldToViewportPoint(worldPoint);
                    screenPos.x *= rootSize.x;
                    screenPos.y *= rootSize.y;
                    return screenPos;
                };
                rectSizeGetter = () => new Vector2(0, 0);
            }

            public void SetTarget(RectTransform rectTransform)
            {
                rectScreenCenterGetter = () =>
                {
                    var screenPos = rectTransform.TransformPoint(rectTransform.rect.center);
                    return (Vector2) root.InverseTransformPoint(screenPos) + root.rect.size/2;
                };
                rectSizeGetter = () => rectTransform.rect.size/2;
            }

            Vector2 centerBuffer;
            Vector2 sizeBuffer;

            public bool WasUpdated()
            {
                var center = rectScreenCenter;
                var size = rectSize;

                if (centerBuffer != center || sizeBuffer != size)
                {
                    centerBuffer = center;
                    sizeBuffer = size;
                    return true;
                }
                return false;
            }
        }

        public TutorialModalMsgView modalMsgView
        {
            get { return genericView.GetComponent<TutorialModalMsgView>("modalMsgView"); }
        }

        public UnityEvent onHideHandler = new UnityEvent();

#if DEBUG
        public Vector2 debugAdditiveSize;
        public Vector2 debugAdditiveOffset;
#endif

        private TutorialShadowTargeter shadowTargeter;
        private bool isRectShowingBuffer;

        private bool isRectShowing
        {
            get { return isRectShowingBuffer; }
            set
            {
                if (value)
                    UpdateShadowRect();
                genericView.SetActive("rectRoot",value);
                isRectShowingBuffer = value;
            }
        }

        private string content
        {
            set
            {
                genericView.SetText("descriptionTextLeft", value);
                genericView.SetText("descriptionTextRight", value);
            }
        }

        private RectTransform leftPart;
        private RectTransform rightPart;
        private RectTransform topPart;
        private RectTransform bottomPart;
        private RectTransform centerPart;
        private Color defaultColor;

        private void Awake()
        {
            leftPart = genericView.GetComponent<RectTransform>("leftShadowPart");
            rightPart = genericView.GetComponent<RectTransform>("rightShadowPart");
            topPart = genericView.GetComponent<RectTransform>("topShadowPart");
            bottomPart = genericView.GetComponent<RectTransform>("bottomShadowPart");
            centerPart = genericView.GetComponent<RectTransform>("centerShadowPart");
            defaultColor = leftPart.GetComponent<Image>().color;
            
            isRectShowing = false;
            shadowTargeter = new TutorialShadowTargeter(GetComponent<RectTransform>(), Camera.main);

            modalMsgView.OnHideHandler.AddListener(() => onHideHandler.Invoke());
        }

        public void ShowTextHint(string message, bool anchoredAtLeftSide)
        {
            content = message;
            genericView.SetActive("messagePanelLeft", anchoredAtLeftSide);
            genericView.SetActive("messagePanelRight", !anchoredAtLeftSide);
        }

        public void ShowModalMessage(string title, string message)
        {
            modalMsgView.title = title;
            modalMsgView.content = message;
            modalMsgView.Show();
        }

        public void HideTextHint()
        {
            genericView.SetActive("messagePanelLeft",false);
            genericView.SetActive("messagePanelRight", false);
        }

        public void ShowRectAt(IWorldPositionProvider provider, Vector2 rectTargetSize, Vector2 rectTargetOffset, bool isShadowVisible)
        {
#if DEBUG
            debugAdditiveOffset = Vector2.zero;
            debugAdditiveOffset = Vector2.zero;
#endif
            shadowTargeter.SetTarget(provider, rectTargetSize, rectTargetOffset);
            isRectShowing = true;
            SetShadowVisibility(isShadowVisible);
        }

        public void ShowBlockRect(bool isShadowVisible)
        {
            shadowTargeter.SetTarget();
            isRectShowing = true;
            SetShadowVisibility(isShadowVisible);
        }

        public void ShowRectAt(RectTransform rectTransform, bool isShadowVisible)
        {
            shadowTargeter.SetTarget(rectTransform);
            isRectShowing = true;
            SetShadowVisibility(isShadowVisible);
        }

        private void SetShadowVisibility(bool isShadowVisible)
        {
            var newColor = isShadowVisible ? defaultColor : new Color(0, 0, 0, 0);
            leftPart.GetComponent<Image>().color = newColor;
            rightPart.GetComponent<Image>().color = newColor;
            topPart.GetComponent<Image>().color = newColor;
            bottomPart.GetComponent<Image>().color = newColor;
            centerPart.GetComponent<Image>().color = isShadowVisible ? new Color(1, 1, 1, 1) : new Color(0, 0, 0, 0);
        }


        public void HideRect()
        {
            isRectShowing = false;
        }

        private void Update()
        {
            if (isRectShowing)
            {
                UpdateShadowRect();
            }
        }

        private void UpdateShadowRect()
        {

            if (!shadowTargeter.WasUpdated()
#if DEBUG
                && debugAdditiveOffset == Vector2.zero
                && debugAdditiveSize == Vector2.zero
#endif
                )
                return;

            var screenPos = shadowTargeter.rectScreenCenter;
            var rectSize = shadowTargeter.rectSize;
            var rootSize = GetComponent<RectTransform>().rect.size;

#if DEBUG
            rectSize += debugAdditiveSize;
            screenPos += debugAdditiveOffset;
#endif

            var leftBottom = (Vector2) screenPos - rectSize;
            var rightTop = (Vector2) screenPos + rectSize;

            leftPart.anchoredPosition = Vector2.zero;
            leftPart.sizeDelta = new Vector2(leftBottom.x,rootSize.y);

            rightPart.anchoredPosition = new Vector2(rightTop.x, 0);
            rightPart.sizeDelta = new Vector2(rootSize.x - rightTop.x, rootSize.y);

            bottomPart.anchoredPosition = new Vector2(leftBottom.x, 0);
            bottomPart.sizeDelta = new Vector2(rightTop.x - leftBottom.x, leftBottom.y);

            topPart.anchoredPosition = new Vector2(leftBottom.x, rightTop.y);
            topPart.sizeDelta = new Vector2(rightTop.x - leftBottom.x, rootSize.y - rightTop.y);

            centerPart.anchoredPosition = leftBottom;
            centerPart.sizeDelta = rightTop - leftBottom;
        }
    }
}