﻿using Core.Assets;
using CustomAssets.Lists;
using Data;
using UnityEngine;
using View.PrefabResources;
using View.UI.OverheadUI.Data;

namespace View.UI
{
    [CreateAssetMenu(menuName = "Kingdom/View/UI/GUI Collections", fileName = "GUICollections")]
    [SingletonResource("UI/GUICollections")]
    public class GUICollections : SingletonResource<GUICollections>
    {
        public TextIconsLibrary         textIconsLibrary;
        public TextIconsLibrary         textIconsSmallLibrary;
        public SpritesList              shopBankTextures;
        public SpritesList              exerciseSpritesList;
        public SpritesList              exerciseLevelSpritesList;
        public SpritesList              shopAdsTextures;
        public WallFloorTextures        wallFloorTextures;
        public StorageIconsList         storageIcons;
        public SpritesList              stateSpritesList;
        public SpritesList              commonIcons;
        public SportsmenIconsList       sportsmenIcons;
        public RankAchievementIconList  rankAchievementIconList;

        public static int GetIconIdxForExerciseType(ExersiseType type)
        {
            switch (type)
            {
                case ExersiseType.Arms:     return (int)OverheadUIDataCharacter.State.Arms;
                case ExersiseType.Cardio:   return (int)OverheadUIDataCharacter.State.Cardio;
                case ExersiseType.Legs:     return (int)OverheadUIDataCharacter.State.Legs;
                case ExersiseType.Torso:    return (int)OverheadUIDataCharacter.State.Torso;
            }
            return 0;
        }
    }
}
