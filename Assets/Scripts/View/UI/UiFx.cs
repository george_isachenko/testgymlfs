﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using View.UI.Base;

namespace View.UI
{
    public enum UiFxSprite {
        Coins,
        Bucks,
        Exps,
        Platinum,
        Custom,
    }

    public class UiFx : UIGenericMonoBehaviour {

        public static UiFx instance;
        
        private const int MAX_SPRITE_COUNT = 20;

        public Transform coinsTarget;
        public Transform bucksTarget;
        public Transform expTarget;
        public Transform platinumTarget;
        public Transform storageTarget;
        public Camera    cam;

        public Sprite spriteCoins;
        public Sprite spriteBucks;
        public Sprite spriteExp;
        public Sprite spritePlatinum;

        public float    agression = 0.3f;

        Dictionary<UiFxSprite, CanvasGroup[]>   sprites = new Dictionary<UiFxSprite, CanvasGroup[]>();
        Dictionary<UiFxSprite, CanvasGroup>     headers = new Dictionary<UiFxSprite, CanvasGroup>();
        Dictionary<UiFxSprite, Text>            headerValues = new Dictionary<UiFxSprite, Text>();
        Dictionary<UiFxSprite, Transform>       targets = new Dictionary<UiFxSprite, Transform>();
        Dictionary<UiFxSprite, float>           timePermission = new Dictionary<UiFxSprite, float>();

        /*
        bool coinsOn        { set { genericView.SetActive("coins"); } }
        bool bucksOn        { set { genericView.SetActive("bucks"); } }
        bool expOn          { set { genericView.SetActive("exp"); } }
        bool platinumOn     { set { genericView.SetActive("platinum"); } }*/

        string coinsValue       { set { genericView.SetActive("coinsValue"); } }
        string bucksValue       { set { genericView.SetActive("bucksValue"); } }
        string expValue         { set { genericView.SetActive("expValue"); } }
        string platinumValue    { set { genericView.SetActive("platinumValue"); } }

    	void Start () {

            instance = this;

            if (spriteCoins == null)
                spriteCoins = Data.Cost.GetSpriteCoins();
            
            if (spriteBucks == null)
                spriteBucks = Data.Cost.GetSpriteBucks();
            
            if (spriteExp == null)
                spriteExp = Data.Cost.GetSpriteExp();
            
            if (spritePlatinum == null)
                spritePlatinum = Data.Cost.GetSprite(Data.PlayerProfile.ResourceType.Platinum);


            sprites.Add(UiFxSprite.Coins,       CreateSprites(spriteCoins));
            sprites.Add(UiFxSprite.Bucks,       CreateSprites(spriteBucks));
            sprites.Add(UiFxSprite.Exps,        CreateSprites(spriteExp));
            sprites.Add(UiFxSprite.Platinum,    CreateSprites(spritePlatinum));
            sprites.Add(UiFxSprite.Custom,      CreateSprites(spritePlatinum)); // any sprite suits bcs it will be overriden

            targets.Add(UiFxSprite.Coins,       coinsTarget);
            targets.Add(UiFxSprite.Bucks,       bucksTarget);
            targets.Add(UiFxSprite.Exps,        expTarget);
            targets.Add(UiFxSprite.Platinum,    platinumTarget);
            targets.Add(UiFxSprite.Custom,      storageTarget);

            timePermission.Add(UiFxSprite.Coins, 0);
            timePermission.Add(UiFxSprite.Bucks, 0);
            timePermission.Add(UiFxSprite.Exps, 0);
            timePermission.Add(UiFxSprite.Platinum, 0);
            timePermission.Add(UiFxSprite.Custom, 0);

            genericView.Rename("header", "coins");
            genericView.Duplicate("coins", "bucks");
            genericView.Duplicate("coins", "exp");
            genericView.Duplicate("coins", "platinum");

            genericView.GetComponentInChildren<Image>("coins").sprite = spriteCoins;
            genericView.GetComponentInChildren<Image>("bucks").sprite = spriteBucks;
            genericView.GetComponentInChildren<Image>("exp").sprite = spriteExp;
            genericView.GetComponentInChildren<Image>("platinum").sprite = spritePlatinum;

            headers.Add(UiFxSprite.Coins,       genericView.GetComponent<CanvasGroup>("coins"));
            headers.Add(UiFxSprite.Bucks,       genericView.GetComponent<CanvasGroup>("bucks"));
            headers.Add(UiFxSprite.Exps,        genericView.GetComponent<CanvasGroup>("exp"));
            headers.Add(UiFxSprite.Platinum,    genericView.GetComponent<CanvasGroup>("platinum"));

            headers[UiFxSprite.Coins].alpha = 0;
            headers[UiFxSprite.Bucks].alpha = 0;
            headers[UiFxSprite.Exps].alpha = 0;
            headers[UiFxSprite.Platinum].alpha = 0;

            headerValues.Add(UiFxSprite.Coins, genericView.GetComponentInChildren<Text>("coins"));
            headerValues.Add(UiFxSprite.Bucks, genericView.GetComponentInChildren<Text>("bucks"));
            headerValues.Add(UiFxSprite.Exps, genericView.GetComponentInChildren<Text>("exp"));
            headerValues.Add(UiFxSprite.Platinum, genericView.GetComponentInChildren<Text>("platinum"));
    	}

        [Header("Animation with Numbers")]
        public LeanTweenType animType = LeanTweenType.easeInOutQuart;
        public float animTime = 0.5f;
        public float fadeDuration = 0.5f;
        public float scaleDuration = 0.5f;
        public float startScale = 1f;
        public float endScale = 2f;
        public bool startPosAtHeader = true;
        //public float animDistance = 300f;
        //public Vector3 animDirection = new Vector3(0, 1, 0);
        //public float animDelay = 1f;

        [Header("Flight animation")]
        public float delay = 0.05f;
        public float ANIM_TIME = 1.0f;
        public float sizeOfImage = 72;

        public void ShowHeader(UiFxSprite type, int value)
        {
            var header = headers[type];

            header.transform.parent.SetAsLastSibling();

            var rt = (RectTransform)header.transform;
            var pos = GetMousePos();

            header.transform.parent.localPosition = pos;
            header.transform.localScale = Vector3.one * startScale;
            header.alpha = 1;
            //headerValues[type].text = "+" + value.ToString();
            headerValues[type].text = "+" + value.ToString();
            LeanTween.delayedCall(header.gameObject, animTime, ()=> { LeanTween.alphaCanvas(header, 0, fadeDuration).setEase(animType); });
            // LeanTween.alphaCanvas(header, 0, animTime).setEase(animType);
            //LeanTween.scale(rt, Vector3.one * endScale, animTime).setEase(animType);
            LeanTween.scale(rt, Vector3.one * endScale, scaleDuration).setEase(animType);
            DisableHiddenHeaders();
        }

        void DisableHiddenHeaders()
        {
            foreach( var item in headers)
            {
                var header = item.Value;
                header.gameObject.SetActive(header.alpha != 0);
            }
        }

        public void AddFxResource(Data.PlayerProfile.ResourceType[] list)
        {
            if (Time.time > timePermission[UiFxSprite.Custom])
            {
                try {
                    var sprites = this.sprites[UiFxSprite.Custom];

                    for (int i = 0; i < list.Length; i++)
                    {
                        var res = list[i];
                        var spriteOfRes = GUICollections.instance.storageIcons.items[(int)res].sprite;
                        sprites[i].GetComponent<Image>().sprite = spriteOfRes;
                    }

                    var dealy = AddFx(UiFxSprite.Custom, list.Length, GetMousePos(), true);
                    timePermission[UiFxSprite.Custom] = Time.time + dealy;

                }
                catch (Exception e)
                {
                    Debug.LogError("IMPOSIBRU: AddFxResource " + e.ToString());
                }
            }
        }

        public void AddFxResource(Data.PlayerProfile.ResourceType res, int count)
        {
            if (Time.time > timePermission[UiFxSprite.Custom])
            {
                try {
                    var spriteOfRes = GUICollections.instance.storageIcons.items[(int)res].sprite;
                    var sprites = this.sprites[UiFxSprite.Custom];

                    for (int i = 0; i < count; i++)
                        sprites[i].GetComponent<Image>().sprite = spriteOfRes;

                    var dealy = AddFx(UiFxSprite.Custom, count, GetMousePos(), true);
                    timePermission[UiFxSprite.Custom] = Time.time + dealy;
                }
                catch (Exception e)
                {
                    Debug.LogError("IMPOSIBRU: AddFxResource " + e.ToString());
                }
            }
        }
         

        public void AddFx(UiFxSprite type, int count)
        {
            if (Time.time > timePermission[type])
            {
                try {
                    StartCoroutine(AddFxNextFrame(type, count));
                }
                catch (Exception e)
                {
                    Debug.LogError("IMPOSIBRU: AddFx " + e.ToString());
                }
            }
        }

        IEnumerator AddFxNextFrame(UiFxSprite type, int count)
        {
            timePermission[type] = Time.time + animTime;

            yield return new WaitForSeconds(animTime);
            yield return new WaitForEndOfFrame();

            Vector3 startPos;


            if (startPosAtHeader)
                startPos = headers[type].GetComponentInChildren<Image>().transform.position;
            else
                startPos = GetMousePos();

            var dealy = AddFx(type, count, startPos, !startPosAtHeader);
            timePermission[type] = Time.time + dealy;
        }

        int SpriteCountRemap(UiFxSprite type, int count)
        {
            if (type == UiFxSprite.Custom)
                return count;

            if (type == UiFxSprite.Platinum)
            {
                return (int)(count/2);
            }

            if (type == UiFxSprite.Bucks)
            {
                return (int)Mathf.Log((float)count, 1.3f);
            }

            return (int)(1.7f * Mathf.Pow((float)count, 0.3f));
        }

        public float AddFx(UiFxSprite type, int count, Vector2 startPos, bool isMouse = false)
        {
            transform.SetAsLastSibling();

            var curSprites = sprites[type];
            var curTarget = targets[type];

            if (curTarget == null)
            {
                Debug.LogErrorFormat("UiFx : {0} target == NULL", type.ToString());
                return 0;
            }

            count = SpriteCountRemap(type, count);

            if (count > MAX_SPRITE_COUNT)
                count = MAX_SPRITE_COUNT;

            for (int i = 0; i < count; i++)
            {
                var sprite = curSprites[i];
                sprite.gameObject.SetActive(true);
                var rectTrans = sprite.transform as RectTransform;

                if (isMouse)
                    rectTrans.localPosition = startPos;
                else
                    rectTrans.position = startPos;

                var point1      = rectTrans.position;
                var point2      = curTarget.position;
                var direction   = point2 - point1;
                var distance    = direction.magnitude;
                var vector1to2  = direction.normalized;
                //var vector2to1  = -vector1to2;
                var center      = point1 + vector1to2 * distance / 2.0f;

                var control1    = new Vector3( UnityEngine.Random.Range(point1.x - direction.x * agression, center.x), center.y);
                var control2    = new Vector3( UnityEngine.Random.Range(center.x, point2.x + direction.x * agression), center.y);


                var path = new Vector3[] {point1, control2, control1, point2};

                LeanTween.move(sprite.gameObject, path, ANIM_TIME).setEase(LeanTweenType.easeInCubic).delay += delay*i;

                sprite.alpha = 1;
                var visibilityRatio = 0.9f;
                LeanTween.alphaCanvas(sprite, 0, ANIM_TIME*(1 -visibilityRatio)).setEase(LeanTweenType.easeOutQuad).delay = ANIM_TIME*visibilityRatio + delay*i;
            }

            return delay * ( count - 1) + ANIM_TIME;
        }

        public Vector3 GetMousePos()
        {
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(transform as RectTransform, Input.mousePosition, cam, out localPoint);

            return new Vector3(localPoint.x, localPoint.y, 0);
        }
            
        private CanvasGroup[] CreateSprites(Sprite sprite)
        {
            var sprites = new CanvasGroup[MAX_SPRITE_COUNT];
            for (int i = 0; i < MAX_SPRITE_COUNT; i++)
                sprites[i] = CreateSpriteObject(sprite).GetComponent<CanvasGroup>();
            return sprites;
        }

        private GameObject CreateSpriteObject(Sprite sprite)
        {
            if (sprite == null)
            {
                Debug.LogError("Sprite is NULL");
            }
            var go = new GameObject("spriteObject", new System.Type[] {typeof(Image), typeof(CanvasGroup)});
            var img = go.GetComponent<Image>();
            img.rectTransform.sizeDelta = new Vector2(sizeOfImage, sizeOfImage);
            img.raycastTarget = false;
            img.sprite = sprite;

            go.transform.SetParent(transform, false);
            go.transform.localPosition = Vector3.zero;
            go.SetActive(false);

            return go;
        }
    }
}