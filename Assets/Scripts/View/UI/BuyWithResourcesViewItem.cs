﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class BuyWithResourcesViewItem : UIBaseViewListItem 
    {
        Image       imgIcon;
        Text        txt;
        GameObject  objPrice;
        Text        txtPrice;


        public Sprite sprite        { set { imgIcon.sprite = value; } }
        public string text          { set { txt.text = value; } }
        public bool   priceOn       { set { objPrice.SetActive(value); } }
        public int    price         { set { txtPrice.text = value.ToString(); } }

        new void Awake()
        {
            base.Awake();

            imgIcon         = transform.Find("ImgIcon").GetComponent<Image>();
            txt             = transform.Find("Text").GetComponent<Text>();
            objPrice        = transform.Find("Price").gameObject;
            txtPrice        = objPrice.transform.GetComponentInChildren<Text>();
        }


    }
}
