using Data;
using UnityEngine;
using View.UI.Base;
using System.Collections;

namespace View.UI
{
    public class TooltipView : UIGenericMonoBehaviour
    {
        public static TooltipView instance;

        public Vector2 magnetXMargin = Vector2.zero;
        public float screenMargin = 10;

        private RectTransform magentTarget;
        private bool needUpdate;
        private Camera guiCamera;
        private CanvasGroup canvasGroup;

        public string title
        {
            set
            {
                if (value != null)
                    genericView.SetText("titleText", value);

                genericView.SetActive("titleText", value != null);
            }
        }

        public string description
        {
            set { genericView.SetText("descriptionText", value); }
        }

        public float delay
        {
            get; set;
        }

        void Awake()
        {
            var canvas = (transform as RectTransform).GetComponentInParent<Canvas>();
            guiCamera = canvas.worldCamera;

            canvasGroup = GetComponent<CanvasGroup>();

            instance = this;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show(RectTransform target)
        {
            transform.SetAsLastSibling();

            MagnetTo(target);

            if (gameObject.activeSelf == false)
            {
                gameObject.SetActive(true);
                canvasGroup.alpha = 0;
                StartCoroutine(ShowDelayed(delay >= 0.0f ? delay : 0.5f));
            }
        }

        IEnumerator ShowDelayed(float delay)
        {
            yield return new WaitForSeconds(delay);

            canvasGroup.alpha = 1;
        }

        private void MagnetTo(RectTransform target)
        {
            magentTarget = target;
            needUpdate = true;
        }

        public void LateUpdate()
        {
            if (!needUpdate)
                return;

            needUpdate = false;
            UpdateMagnet();
        }
        
        private Rect RectTransformToScreenSpace(RectTransform rectTransform)
        {
            var worldCenter = rectTransform.TransformPoint(rectTransform.rect.center  + rectTransform.rect.min);
            var worldTopRight = rectTransform.TransformPoint(rectTransform.rect.center);
            var center = RectTransformUtility.WorldToScreenPoint(guiCamera,worldCenter);
            var topRight = RectTransformUtility.WorldToScreenPoint(guiCamera,worldTopRight);
            return new Rect(center, (topRight - center)*2);
        }

        private void UpdateMagnet()
        {
            var targetRect = RectTransformToScreenSpace(magentTarget);
            var myRect = RectTransformToScreenSpace((RectTransform) transform);
            targetRect.xMin -= magnetXMargin.x;
            targetRect.xMax += magnetXMargin.y;

            var horizontalPos = targetRect.center.x;
            if (horizontalPos - myRect.width/2 - screenMargin < 0)
                horizontalPos = myRect.width/2 + screenMargin;
            if (horizontalPos + myRect.width/2 + screenMargin > Screen.width)
                horizontalPos = Screen.width - myRect.width/2 - screenMargin;

            var verticalPos = targetRect.yMax + myRect.height/2;
            if (verticalPos + myRect.height/2 + screenMargin > Screen.height)
                verticalPos = targetRect.yMin - myRect.height/2;
            var myScreenPos = new Vector2()
            {
                x = horizontalPos,
                y = verticalPos
            };
            SetPositionInSreenSpace((RectTransform) transform, myScreenPos);
        }

        private static void SetPositionInSreenSpace(RectTransform transform, Vector2 position)
        {
            var canvasSize = ((RectTransform) transform.GetComponentInParent<Canvas>().transform).rect.size;
            position.x -= Screen.width/2f;
            position.y -= Screen.height/2f;
            position.x *= canvasSize.x / Screen.width;
            position.y *= canvasSize.y / Screen.height;
            transform.anchoredPosition = position;
        }
    }
}