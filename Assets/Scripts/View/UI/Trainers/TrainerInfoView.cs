﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using View.Actors;
using System;
using UnityEngine.UI;
using Helpers;
using Data.Person.Info;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class TrainerInfoView : UIBaseView {

        public CharacterPortrait    portrait                    { set { SetPortrait(value); } }

        protected CharacterPortrait portraitObject;
        private Transform portraitHolder;

        public string   nameOfPerson        { set { genericView.SetText("name", value); } }
        public int      level               { set { genericView.SetText("level", value.ToString()); } }
        public string   expTxt              { set { genericView.SetText("expTxt", value); } }
        public float    expProgress         { set { genericView.SetSliderValue("expProgress", value); } }
        public int      maxClients          { set { genericView.SetText("maxClients", value.ToString()); } }
        public string   bestWorkstation     { set { genericView.SetText("bestWorkstation", value); } }
        public int      coinsBonus          { set { genericView.SetText("coinsBonus", value.ToString() + "%"); } }
        public bool     inputOn             { set { SetInputMode(value); } }
        public Sprite   nextUnlockSprite    { set { genericView.SetSprite("nextUnlockSprite", value); } }
        public string   nextUnlockText      { set { genericView.SetText("nextUnlockText", value); } }

        public Action OnNameEditFinishCallback;
        public Action<IPersonTrainerInfo>   ShowTrainer;

        public Text trainerName { get { return genericView.GetComponent<Text>("name"); } }

        private Action OnTrainerRenameClick { set { genericView.SetButtonCallback("rename", value); } }

        bool inputMode = false;
        Text inputSympol;
        Outline outlineSympol;
        Color nameColor;

        void Awake()
        {
            portraitHolder = genericView.GetTransform("portrait");
            BindCloseButton();

            inputSympol = genericView.GetComponent<Text>("inputSympol");
            genericView.SetActive("inputSympol", false);
            outlineSympol = genericView.GetComponent<Outline>("inputSympol");
            nameColor = trainerName.color;

            OnTrainerRenameClick = () => { if (!inputMode) SetInputMode(true); };
        }

        void Update()
        {
            if (inputMode)
            {
                float tAlpha = Math.Abs((float)Math.Sin(Time.realtimeSinceStartup * 5.0f));
                Color tColor = new Color(nameColor.r, nameColor.g, nameColor.b, tAlpha);
                inputSympol.color = tColor;

                tColor = outlineSympol.effectColor;
                tColor.a = tAlpha;
                outlineSympol.effectColor = tColor;
            }

        }

        void SetPortrait(CharacterPortrait portrait)
        {
            if (portraitHolder != null)
            {
                if (this.portraitObject != null)
                {
                    this.portraitObject.Hide();
                    this.portraitObject = null;
                }

                //Bounds portraitHeadBounds;
                if (portrait != null)
                {
                    portrait.Show(portraitHolder);
                    this.portraitObject = portrait;
                }
            }
        }

        public void SetInputMode(bool val)
        {
            inputMode = val;
            inputSympol.gameObject.SetActive(val);

            //changeNameOn = val;

            if (inputMode)
            {
                OnNameChanged();
                InputHelper._instance.ShowInput(trainerName.text, InputCallback);
            }
            else
            {
                OnNameEditFinish();
            }
        }

        void OnNameEditFinish()
        {
            GUIRoot.ResetIsolatedRect();

            InputHelper.Close();

            if (OnNameEditFinishCallback != null)
                OnNameEditFinishCallback();
            else
                Debug.LogError("OnNameEditFinishCallback == null");
        }

        void OnNameChanged()
        {
            float prefferWidth = LayoutUtility.GetPreferredWidth(trainerName.rectTransform);
            Vector3 tPos = new Vector3(prefferWidth / 2.0f, 0.0f, 0.0f);
            inputSympol.transform.localPosition = tPos;
        }

        void InputCallback(string data)
        {
            if (data != null)
            {
                nameOfPerson = data;
                OnNameChanged();
            }
            else
            {
                inputOn = false;
            }
        }

        protected override void OnHide()
        {
            if (OnHide_Callback != null)
                OnHide_Callback.Invoke();
            if (OnHideHandler != null)
                OnHideHandler.Invoke();

            if (inputMode)
                SetInputMode(false);
        }

    }
}