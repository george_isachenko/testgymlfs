﻿using Data;
using Logic.Trainers;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class TrainerClientInfoView : UIGenericMonoBehaviour
    {
        public TrainerClientType    state               { set { SetState(value); }}
        public int                  lvlUnlock           { set { SetLvlUnlock(value); } }
        public int                  iterationsGoal      { set { EnableGameObjects(value, tasks); } }
        public int                  iterationsDone      { set { EnableGameObjects(value, tasksDone); } }
            
        GameObject[]    tasks;
        GameObject[]    tasksDone;

        void SetState(TrainerClientType infoType)
        {
            genericView.SetActive("locked",     infoType == TrainerClientType.Locked);
            genericView.SetActive("empty",      infoType == TrainerClientType.Empty);
            genericView.SetActive("inProgress", infoType == TrainerClientType.InProgress);
            genericView.SetActive("done",       infoType == TrainerClientType.Done);

            tasks       = genericView.GetGameObjects("task", 3);
            tasksDone   = genericView.GetGameObjects("taskDone", 3);
        }

        void SetLvlUnlock(int value)
        {
            genericView.SetText("lvlUnlock", Loc.Get("idLvl") + " " + value.ToString());
        }

        void EnableGameObjects(int value, GameObject[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
                arr[i].SetActive(i < value);
        }
            
    }
}