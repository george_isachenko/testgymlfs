﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;
using System;
using Data;
using View.Actors;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class TrainersViewItem : UIBaseViewListItem {

        public enum ViewMode {
            Hire,
            Info,
            Locked
        }

        public Action<int>   actionShowInfo;

        public Action       actionHire          { set { genericView.SetButtonCallback("btnHire", value); } }
        public Action       actionAssign        { set { genericView.SetButtonCallback("btnAssign", value); } }
        public Action       actionLevelUp       { set { genericView.SetButtonCallback("btnLvlUp", value); } }

        public bool         canAssign           { set { genericView.SetActive("btnAssign", value); } }
        public bool         levelUpOn           { set { genericView.SetActive("btnLvlUp", value); } }
        public bool         isFull              { set { genericView.SetActive("btnFull", value); } }
        public ViewMode     mode                { set { SetViewMode(value); } }
        public string       nameOfPerson        { set { genericView.SetText("name", value); } }
        public string       level               { set { genericView.SetText("level", value); } }
        public string       expTxt              { set { genericView.SetText("expTxt", value); } }
        public float        expProgress         { set { genericView.SetSliderValue("expProgress", value); } }
        public int          levelUnlock         { set { genericView.SetText("levelUnlock", value.ToString()); } }
        public Cost         hirePrice           { set { SetCost(value); } }
        public bool         hireBtnOn           { set { genericView.SetActive("btnHire", value); genericView.SetActive("hirePrice", value); genericView.SetActive("hirePriceIcon", value);} }

        public CharacterPortrait    portrait                    { set { SetPortrait(value); } }

        protected CharacterPortrait portraitObject;
        private Transform portraitHolder;


        [HideInInspector] public TrainerClientInfoView[]  clients = new TrainerClientInfoView[4];

        public GameObject clientSlotPrefab;

        new void Awake()
        {
            base.Awake();

            var clientsHolder = genericView.GetTransform("clientsHolder");
            portraitHolder = genericView.GetTransform("portrait");

            CreateClientSlots(clientsHolder, clientSlotPrefab, genericView, clients);

            genericView.SetButtonCallback("btnInfo", OnInfoClick);
        }

        void SetCost(Cost cost)
        {
            genericView.SetText("hirePrice", cost.value.ToString()); 
            genericView.SetSprite("hirePriceIcon", cost.GetSprite());
        }

        void OnInfoClick()
        {
            actionShowInfo?.Invoke(idx);
        }

        public static void CreateClientSlots(Transform clientsHolder, GameObject slotPrefab, UIGenericView genericView, TrainerClientInfoView[]  clients)
        {
            for (int i = 0; i < 4; i++)
            {
                var name = "TrainerClientSlot_" + i.ToString();

                var item = genericView.GetGameObject(name, true);

                if (item == null)
                {
                    item = UIBaseView.InstantiatePrefab(slotPrefab, clientsHolder, name);

                    clients[i] = item.GetComponent<TrainerClientInfoView>();

                    clients[i].name = "TrainerClientSlot_" + i.ToString();
                    clients[i].genericView.alias = clients[i].name;
                }
                else
                    clients[i] = item.GetComponent<TrainerClientInfoView>();
            }
        }

        void SetViewMode(ViewMode viewMode)
        {
            genericView.SetActive("info", viewMode == ViewMode.Info);
            genericView.SetActive("hire", viewMode == ViewMode.Hire);
            genericView.SetActive("locked", viewMode == ViewMode.Locked);
        }

        void SetPortrait(CharacterPortrait portrait)
        {
            if (portraitHolder != null)
            {
                if (this.portraitObject != null)
                {
                    this.portraitObject.Hide();
                    this.portraitObject = null;
                }

                //Bounds portraitHeadBounds;
                if (portrait != null)
                {
                    portrait.Show(portraitHolder);
                    this.portraitObject = portrait;
                }
            }
        }
    }
}