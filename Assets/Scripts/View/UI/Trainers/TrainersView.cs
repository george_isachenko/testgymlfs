﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    [RequireComponent(typeof(UIGenericView))]
    public class TrainersView : UIBaseView {

        public TrainersViewList list;

        void Awake()
        {
            BindCloseButton();

            list = genericView.GetComponent<TrainersViewList>("items");
        }
            
    }
}