﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using View.UI.Base;
using Data;
using System;

namespace View.UI
{
    public delegate System.TimeSpan TimerUpdate_delegate();

    public class PromoView : UIBaseView
    {
        Text header;
        Text description;
        Text discount;
        Text txtBucks;
        Text txtCoins;
        Text comboFitBucks;
        Text comboCoins;
        GameObject bucksObj;
        GameObject coinsObj;
        GameObject coinsAndBucksObj;
        Text timeLeft;
        Text oldPrice;
        Text newPrice;

        public Button buyBtn { get; private set;}

        TimerUpdate_delegate timeUpdateFunc = null;

        void Awake()
        {
            BindCloseButton();

            UIGenericView root = genericView;

            header = root.GetComponent<Text>("header");
            description = root.GetComponent<Text>("description");
            discount = root.GetComponent<Text>("discount");
            txtBucks = root.GetComponent<Text>("fitbucks");
            txtCoins = root.GetComponent<Text>("coins");

            comboFitBucks = root.GetComponent<Text>("comboBucksCount");
            comboCoins = root.GetComponent<Text>("comboCoins");

            bucksObj = root.GetTransform("bucksobj").gameObject;
            coinsObj = root.GetTransform("coinsobj").gameObject;
            coinsAndBucksObj = root.GetTransform("coinsAndBucks").gameObject;

            timeLeft = root.GetComponent<Text>("timeleft");

            oldPrice = root.GetComponent<Text>("oldprice");
            newPrice = root.GetComponent<Text>("newprice");

            buyBtn = root.GetComponent<Button>("purchaseButton");
        }


        public void Setup(Data.PromoPurchases setupInfo, decimal oldPrice_, decimal newPrice_, int coins, int bucks, TimerUpdate_delegate timeUpdate_)
        {
            header.text = Loc.Get(setupInfo.header);
            description.text = Loc.Get(setupInfo.description);
            discount.text = "-" + setupInfo.discount.ToString() + "%";

            if (bucks > 0 && coins > 0)
            {
                bucksObj.SetActive(false);
                coinsObj.SetActive(false);
                coinsAndBucksObj.SetActive(true);
                comboFitBucks.text = bucks.ToString();
                comboCoins.text = coins.ToString();
            }
            else
            {
                coinsAndBucksObj.SetActive(false);
                bucksObj.SetActive(bucks > 0);
                coinsObj.SetActive(coins > 0);
                txtBucks.text = bucks.ToString();
                txtCoins.text = coins.ToString();
            }

            timeLeft.text = setupInfo.duration.ToString();

            oldPrice.text = "$" + oldPrice_.ToString();
            newPrice.text = "$" + newPrice_.ToString();

            timeUpdateFunc = timeUpdate_;
        }

        void Update()
        {
            if (timeUpdateFunc != null)
            {
                var tTime = timeUpdateFunc();
                if (tTime > TimeSpan.Zero)
                {
                    timeLeft.text = string.Format("{0} {1}", Loc.Get("idTimeLeft"), PrintHelper.GetTimeString(tTime));
                }
                else
                {
                    timeLeft.text = string.Empty;
                }
            }
        }
    }
}