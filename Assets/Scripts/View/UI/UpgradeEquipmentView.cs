﻿using System;
using Data;
using Data.Estate;
using Data.Estate.Upgrades;
using Data.PlayerProfile;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.Base;
using View.UI.HUD;

namespace View.UI
{
    [AddComponentMenu("Kingdom/View/UI/Upgrade Equipment View")]
    public class UpgradeEquipmentView : UIBaseView 
    {
        #region Protected properties.
        protected Sprite spriteResourceAmount                   { set { genericView.SetSprite("ResourceAmountIcon", value); } }
        protected string txtResourceAmountText                  { set { genericView.SetText("ResourceAmountText", value); } }

        protected Sprite spriteEquipmentIcon                    { set { genericView.SetSprite("EquipmentIcon", value); } }
        protected bool   activeEquipmentIcon                    { set { genericView.SetActive("EquipmentIcon", value); } }
        protected string txtEquipmentName                       { set { genericView.SetText("EquipmentName", value); } }
        protected bool   activePreview3D                        { set { genericView.SetActive("Preview3D", value); } }
        protected Transform transformPreview3D                  { get { return genericView.GetTransform("Preview3D"); } }

        protected bool activeUpgradeButtonAvailable             { set { genericView.SetActive("UpgradeButtonAvailable", value); } }
        protected string txtCoinsCost                           { set { genericView.SetText("CoinsCost", value); } }
        protected Sprite spriteResourceIcon                     { set { genericView.SetSprite("ResourceIcon", value); } }
        protected string txtResourceCost                        { set { genericView.SetText("ResourceCost", value); } }

        protected bool activeUpgradeButtonLocked                { set { genericView.SetActive("UpgradeButtonLocked", value); } }

        protected bool activeUpgradeButtonFull                  { set { genericView.SetActive("UpgradeButtonFull", value); } }

        protected bool activeCurrentInfo                        { set { genericView.SetActive("CurrentInfo", value); } }
        protected StarsIcon starsCurrent                        { get { return genericView.GetComponent<StarsIcon>("StarsCurrent"); } }
        protected string txtCurrentCoinsBonus                   { set { genericView.SetText("CurrentCoinsBonus", value); } }
        protected string txtCurrentExperienceBonus              { set { genericView.SetText("CurrentExperienceBonus", value); } }

        protected bool activeNextAvailableInfo                  { set { genericView.SetActive("NextAvailableInfo", value); } }
        protected StarsIcon starsNextAvailable                  { get { return genericView.GetComponent<StarsIcon>("StarsNextAvailable"); } }
        protected string txtNextAvailableCoinsBonus             { set { genericView.SetText("NextAvailableCoinsBonus", value); } }
        protected string txtNextAvailableExperienceBonus        { set { genericView.SetText("NextAvailableExperienceBonus", value); } }

        protected bool activeNextLockedInfo                     { set { genericView.SetActive("NextLockedInfo", value); } }
        protected string txtNextLockedLevel                     { set { genericView.SetText("NextLockedLevel", value); } }

        protected bool activeFullInfo                           { set { genericView.SetActive("FullInfo", value); } }
        protected StarsIcon starsFull                           { get { return genericView.GetComponent<StarsIcon>("StarsFull"); } }
        protected string txtFullCoinsBonus                      { set { genericView.SetText("FullCoinsBonus", value); } }
        protected string txtFullExperienceBonus                 { set { genericView.SetText("FullExperienceBonus", value); } }
        #endregion

        #region Public fields.
        public ResourceType upgradeResourceType;
        #endregion

        #region Public events.
        public event Action<int> upgradeButtonHandler
        {
            add { genericView.SetButtonCallback("UpgradeButtonAvailable", () => { Hide(); value.Invoke(upgradeCandidateId); }); }
            remove { genericView.ResetButtonCallback("UpgradeButtonAvailable"); }
        }
        #endregion

        #region Private data.
        int upgradeCandidateId = EquipmentData.InvalidId;
        GameObject previewIconInstance = null;
        #endregion

        #region Unity API.
        void Awake ()
        {
            BindCloseButton();
        }
        #endregion

        #region 
        public void Show
            ( int upgradeCandidateId
            , GameObject previewPrefab
            , Sprite equipmentIcon
            , string equipmentName
            , UpgradeAvailabilityState state
            , int currentUpgradeLevel
            , ResourceType upgradeResourceType
            , int upgradeResourceAmount
            , EquipmentUpgradeInfo currentUpgradeInfo
            , EquipmentUpgradeInfo nextUpgradeInfo )
        {
            this.upgradeCandidateId = upgradeCandidateId;

            spriteResourceAmount = GUICollections.instance.storageIcons.items[(int)upgradeResourceType].sprite;
            txtResourceAmountText = PrintHelper.FormatCurrencyAmount(upgradeResourceAmount);

            if (previewIconInstance != null)
            {
                Destroy(previewIconInstance);
                previewIconInstance = null;
            }

            if (previewPrefab != null)
            {
                activeEquipmentIcon = false;
                activePreview3D = true;
                Assert.IsNull(previewIconInstance);
                previewIconInstance = Instantiate(previewPrefab);
                previewIconInstance.SetActive(true);
                previewIconInstance.transform.SetParent(transformPreview3D, false);
            }
            else
            {
                activeEquipmentIcon = true;
                activePreview3D = false;
            }

            // spriteEquipmentIcon = equipmentIcon;
            txtEquipmentName = equipmentName;

            switch (state)
            {
                case UpgradeAvailabilityState.EquipmentAvailable:
                {
                    activeUpgradeButtonAvailable = true;
                    activeUpgradeButtonLocked = false;
                    activeUpgradeButtonFull = false;
                    txtCoinsCost = PrintHelper.FormatCurrencyAmount(nextUpgradeInfo.costAmount);
                    spriteResourceIcon = GUICollections.instance.storageIcons.items[(int)nextUpgradeInfo.resourceType].sprite;
                    txtResourceCost = PrintHelper.FormatCurrencyAmount(nextUpgradeInfo.resourceCost);

                    activeNextAvailableInfo = true;
                    starsNextAvailable.level = currentUpgradeLevel + 1;
                    txtNextAvailableCoinsBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel + 1] * 100.0f));
                    txtNextAvailableExperienceBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel + 1] * 100.0f));

                    activeNextLockedInfo = false;

                    activeFullInfo = false;
                }; break;

                case UpgradeAvailabilityState.FeatureAvailable:
                case UpgradeAvailabilityState.EquipmentLocked:
                {
                    activeUpgradeButtonAvailable = false;
                    activeUpgradeButtonLocked = true;
                    activeUpgradeButtonFull = false;

                    activeNextAvailableInfo = false;

                    activeNextLockedInfo = true;
                    txtNextLockedLevel = nextUpgradeInfo.unlockLevel.ToString();

                    activeFullInfo = false;
                }; break;

                case UpgradeAvailabilityState.EquipmentFull:
                {
                    activeUpgradeButtonAvailable = false;
                    activeUpgradeButtonLocked = false;
                    activeUpgradeButtonFull = true;

                    activeCurrentInfo = false;

                    activeNextAvailableInfo = false;

                    activeNextLockedInfo = false;

                    activeFullInfo = true;
                    starsFull.level = currentUpgradeLevel;
                    txtFullCoinsBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel] * 100.0f));
                    txtFullExperienceBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel] * 100.0f));

                }; break;
            }

            if (state == UpgradeAvailabilityState.FeatureAvailable ||
                state == UpgradeAvailabilityState.EquipmentAvailable ||
                state == UpgradeAvailabilityState.EquipmentLocked)
            {
                activeCurrentInfo = true;
                starsCurrent.level = currentUpgradeLevel;
                txtCurrentCoinsBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel] * 100.0f));
                txtCurrentExperienceBonus = string.Format("{0}%", (int)(DataTables.instance.balanceData.Equipment.Upgrades.coinsRewardMultipliers[currentUpgradeLevel] * 100.0f));
            }
            else
            {
                activeCurrentInfo = false;
            }

            Show();
        }
        #endregion
    }
}
