﻿using View.UI.OverheadUI.Components;

namespace View.UI.OverheadUI.Data
{
    public struct OverheadUIDataCharacter
    {
        public enum State
        {
            Gold = 0,
            Busy,
            Question,
            Happy,
            Angry,
            Subscription,
            Complete,
            TrainerLevelup,
            MemberReturn,
            Energy,
            Bucks,
            Torso,
            Cardio,
            Arms,
            Legs,
            BecameSportsmen,
            TimequestNew,
            TimequestActive,
            Desire,
            EnergyMember,
            Timer,
            Idle,
            TrainerWorkMax,
            Bored,
        }

        public State state;
        public int actionIcon;
        public int level;
        public int timeout;
        public int statRemaining;

        #region Public API.
        public void ApplyData (ActionIcon icon)
        {
            if (icon.backgroundImages != null &&  icon.backgroundImages.array != null && (int)state < icon.backgroundImages.array.Length &&
                icon.backgroundImages.array[(int)state] != null)
            {
                icon.backgroundSprite = icon.backgroundImages.array[(int)state];
            }

            if (state == State.Desire)
            {
                if (icon.exerciseImages != null && icon.exerciseImages.array != null &&
                    actionIcon >= 0 &&
                    actionIcon < icon.exerciseImages.array.Length)
                {
                    icon.iconSprite = icon.exerciseImages.array[actionIcon];
                }
                                
                if (icon.levelImages != null && icon.levelImages.array != null &&
                    level >= 0 &&
                    level < icon.levelImages.array.Length)
                {
                    icon.levelSprite = icon.levelImages.array[level];
                }

                icon.labelText = string.Empty;
            }
            else if (state == State.EnergyMember)
            {
                if (icon.iconsImages != null && icon.iconsImages.array != null && (int)state < icon.iconsImages.array.Length)
                    icon.iconSprite = icon.iconsImages.array[(int)state];

                icon.levelSprite = null;
                icon.labelText = statRemaining.ToString();
            }
            else
            {
                if (icon.iconsImages != null && icon.iconsImages.array != null && (int)state < icon.iconsImages.array.Length)
                    icon.iconSprite = icon.iconsImages.array[(int)state];
                                
                icon.levelSprite = null;

                if (state == State.Torso ||
                    state == State.Cardio ||
                    state == State.Arms ||
                    state == State.Legs)
                {
                    icon.labelText = statRemaining.ToString();
                }
                else
                {
                    icon.labelText = string.Empty;
                }
            }
        }
        #endregion
    }
}