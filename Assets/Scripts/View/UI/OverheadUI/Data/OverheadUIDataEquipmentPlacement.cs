﻿using View.UI.OverheadUI.Components;

namespace View.UI.OverheadUI.Data
{
    public struct OverheadUIDataEquipmentPlacement
    {
        public enum Mode
        {
            InitialPlacement,
            MovementSellAllowed,
            MovementStoreAllowed,
            MovementNoSellOrStore,
            ConfirmCancelOnly,
        }

        public Mode mode;
        public bool allowRotate;
        public bool allowConfirm;

        public OverheadUIDataEquipmentPlacement (Mode mode, bool allowRotate, bool allowConfirm = true)
        {
            this.mode = mode;
            this.allowRotate = allowRotate;
            this.allowConfirm = allowConfirm;
        }

        public void ApplyData(EquipmentPlacementIcon icon)
        {
            switch (mode)
            {
                case Mode.InitialPlacement:
                    icon.UseLeftCancelButton();
                    icon.UseRightConfirmButton();
                    icon.cancelActive = true;
                    icon.confirmActive = allowConfirm;
                    icon.rotateEnabled = true;
                    break;

                case Mode.MovementNoSellOrStore:
                    icon.UseLeftSellButton();
                    if (allowConfirm)
                        icon.UseRightConfirmButton();
                    else
                        icon.UseRightCancelButton();
                    icon.sellActive = false;
                    icon.rotateEnabled = true;
                    break;

                case Mode.MovementSellAllowed:
                    icon.UseLeftSellButton();
                    if (allowConfirm)
                        icon.UseRightConfirmButton();
                    else
                        icon.UseRightCancelButton();
                    icon.sellActive = true;
                    icon.rotateEnabled = true;
                    break;

                case Mode.MovementStoreAllowed:
                    if (allowConfirm)
                        icon.UseRightConfirmButton();
                    else
                        icon.UseRightCancelButton();
                    icon.sellActive = true;
                    icon.rotateEnabled = true;
                    break;

                case Mode.ConfirmCancelOnly:
                    icon.UseLeftCancelButton();
                    icon.UseRightConfirmButton();
                    icon.cancelActive = true;
                    icon.confirmActive = true;
                    icon.rotateEnabled = false;
                    break;
            }

            icon.rotateActive = allowRotate;
        }
    }
}