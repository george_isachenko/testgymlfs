﻿using View.UI.OverheadUI.Components;

namespace View.UI.OverheadUI.Data
{
    public struct OverheadUIDataEquipment
    {
        public enum State
        {
            Gold = 0,
            Dirty,
            Broken,
            Complete,
            Mastering,
            GoldHappy,
            GoldCashRegister,
            VideoAd,
            Busy,
        };

        public State state;

        #region Public API.
        public void ApplyData (SingleActionIcon icon)
        {
            if (icon.backgroundImages != null && icon.backgroundImages.array != null && (int)state < icon.backgroundImages.array.Length &&
                icon.backgroundImages.array[(int)state] != null)
            {
                icon.backgroundSprite = icon.backgroundImages.array[(int)state];
            }

            if (icon.iconsImages != null && icon.iconsImages.array != null && (int)state < icon.iconsImages.array.Length &&
                icon.iconsImages.array[(int)state] != null)
            {
                icon.iconSprite = icon.iconsImages.array[(int)state];
            }

            icon.levelSprite = null;
            icon.labelText = string.Empty;
        }
        #endregion
    }
}