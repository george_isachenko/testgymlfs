﻿using UnityEngine;
using System;
using UnityEngine.Assertions;
using UnityToolbag;

namespace View.UI.OverheadUI
{
    public class OverheadIconInstanceHolder<IconViewT>
        where IconViewT : Component
    {
        #region Inspector fields.
        [Header("Prefab settings")]

        [SerializeField]
        public IconViewT prefab;

        [Header("Sorting layer override")]

        [SerializeField]
        protected bool overrideSortingLayer;

        [SerializeField]
        [SortingLayer]
        protected int sortingLayer;

        [Header("Scale-down in distance override")]

        [SerializeField]
        protected bool overrideScaleDownInDistance;

        [SerializeField]
        protected bool scaleDownInDistance;
        #endregion

        #region Properties.
        public IconViewT instance { get; private set; }
        public OverheadUIIcon icon { get; private set; }
        public bool persistent { get; private set; }
        #endregion

        #region Conversion operators.
        public static implicit operator IconViewT ( OverheadIconInstanceHolder<IconViewT> holder )
        {
            return holder.instance;
        }

/*
        public static implicit operator OverheadUIIcon ( OverheadIconInstanceHolder<IconViewT> holder )
        {
            return holder.icon;
        }
*/
        #endregion

        #region Public API.
        public OverheadIconInstanceHolder ()
        {
        }

        public OverheadIconInstanceHolder (IconViewT prefab, MonoBehaviour owner = null, bool persistent = false)
        {
            this.prefab = prefab;
            instance = null;
            icon = null;
            this.persistent = persistent;

            Instantiate(owner, persistent);
        }

        public void Instantiate (MonoBehaviour owner = null, bool persistent = true)
        {
            Assert.IsNotNull(prefab);
            if (prefab != null && instance == null)
            {
                instance = UnityEngine.Object.Instantiate(prefab);
                Assert.IsNotNull(instance);

                icon = instance.GetComponent<OverheadUIIcon>();
                Assert.IsNotNull(icon);

                if (overrideSortingLayer)
                {
                    icon.sortingLayer = sortingLayer;
                }

                if (overrideScaleDownInDistance)
                {
                    icon.scaleDownInDistance = scaleDownInDistance;
                }

                if (owner)
                {
                    icon.owner = owner;
                }

                this.persistent = persistent;
            }
        }

        public void Cleanup()
        {
            if (instance != null)
            {
                UnityEngine.Object.Destroy(instance);
                instance = null;
            }

            icon = null;
            persistent = false;
        }
        #endregion
    }

    [Serializable]
    public class OverheadIconInstanceHolderTransform : OverheadIconInstanceHolder<Transform>
    {
        public OverheadIconInstanceHolderTransform()
            : base()
        {
        }

        public OverheadIconInstanceHolderTransform(Transform prefab, MonoBehaviour owner = null, bool persistent = false)
            : base(prefab, owner, persistent)
        {
        }
    }
}