﻿using UnityEngine;

namespace View.UI.OverheadUI
{
    public interface IWorldPositionProvider
    {
        bool allowUpdate
        {
            get; set;
        }

        bool enabled
        {
            get; set;
        }

        Vector3 GetWorldPosition();
    }
}