﻿using System;
using UnityEngine;

namespace View.UI.OverheadUI
{
    public class DynamicWorldPositionProvider : IWorldPositionProvider
    {
        public Vector3 cachedPosition;

        private Transform transform;
        private bool _allowUpdate;

        bool IWorldPositionProvider.allowUpdate
        {
            get
            {
                return _allowUpdate;
            }

            set
            {
                if (_allowUpdate != value)
                {
                    if (!value)
                        cachedPosition = transform.position;

                    _allowUpdate = value;
                }
            }
        }

        public bool enabled { get; set; }

        public DynamicWorldPositionProvider (Transform transform, bool enabled = true)
        {
            this.transform = transform;
            this.enabled = enabled;
            _allowUpdate = true;
        }

        Vector3 IWorldPositionProvider.GetWorldPosition()
        {
            return _allowUpdate ? transform.position : cachedPosition;
        }
    }
}