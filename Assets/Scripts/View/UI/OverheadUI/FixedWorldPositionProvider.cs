﻿using System;
using UnityEngine;

namespace View.UI.OverheadUI
{
    public class FixedWorldPositionProvider : IWorldPositionProvider
    {
        public Vector3 position;

        bool IWorldPositionProvider.allowUpdate
        {
            get
            {
                return true;
            }

            set
            {
            }
        }

        public bool enabled { get; set; }

        public FixedWorldPositionProvider (Vector3 pos, bool enabled = true)
        {
            position = pos;
            this.enabled = enabled;
        }

        Vector3 IWorldPositionProvider.GetWorldPosition()
        {
            return position;
        }
    }
}