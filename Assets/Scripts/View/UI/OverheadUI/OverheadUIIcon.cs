﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityToolbag;

namespace View.UI.OverheadUI
{
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Overhead UI Icon")]
    [DisallowMultipleComponent]
    public class OverheadUIIcon : MonoBehaviour
    {
        #region Callbacks.
        public Action onAutoHide;
        #endregion

        #region Properties.
        public bool visible
        {
            get
            {
                return  _visible &&
                        enabled &&
                        (owner == null || (owner.enabled && owner.gameObject.activeInHierarchy)) &&
                        (worldPositionProvider == null || worldPositionProvider.enabled);
            }

            set
            {
                if (value != _visible)
                {
                    cachedWorldPosition = new Vector3 (float.MinValue, float.MinValue, float.MinValue);
                    _visible = value;
                }
            }
        }

        public int sortingLayerIndex { get; private set; }
        public bool frontIcon { get; set; }

        public MonoBehaviour owner
        {
            get
            {
                return _owner;
            }

            set
            {
                _owner = value;
#if DEBUG
                if (value != null)
                {
                    name = value.name;
                }
#endif
            }
        }

        public Vector3 worldPosition
        {
            get
            {
                if (worldPositionProvider != null)
                {
                    return worldPositionProvider.GetWorldPosition();
                }
                else if (owner != null)
                {
                    return owner.transform.position;
                }
                else
                {
                    return transform.position;
                }
            }
        }
        #endregion

        #region Public (Inspector) fields.
        [Header("Sorting")]
        [SortingLayer]
        public int sortingLayer;

        [Header("Positioning")]
        [Tooltip("WorldPositionProvider object, optional. If missing - 'owner' object will be used.")]
        public IWorldPositionProvider worldPositionProvider;
        public Vector3 screenPositionShift;

        [Header("Visibility and auto-hide settings")]
        public float autoHideDelay;
        public bool scaleDownInDistance;
        public float scaleRatio = 1.0f;
        #endregion

        #region Private data.
        private bool _visible = false;
        private MonoBehaviour _owner = null;
        private IEnumerator autoHideJob = null;
        private Vector3 cachedWorldPosition;
        #endregion

        #region Unity API.

        void Awake()
        {
            cachedWorldPosition = new Vector3 (float.MinValue, float.MinValue, float.MinValue);

            SetParentToManager();
        }

        void Start()
        {
            gameObject.SetActive(false);

            if (transform.parent == null)
            {   // Maybe failed at Awake - try again.
                SetParentToManager();
            }
        }

        void OnEnable()
        {
            // Can be overriden after Instantiate() (so Awake() too), so calculate it again.
            sortingLayerIndex = SortingLayer.GetLayerValueFromID(sortingLayer);

            if (autoHideJob != null)
            {
                StopCoroutine(autoHideJob);
                autoHideJob = null;
            }

            if (visible && autoHideDelay > 0)
            {
                autoHideJob = AutoHideJob(DateTime.UtcNow);
                StartCoroutine(autoHideJob);
            }
        }

        void OnDisable()
        {
            if (autoHideJob != null)
            {
                StopCoroutine(autoHideJob);
                autoHideJob = null;
            }
        }
        #endregion

        #region Public API.
        public void UpdatePosition(Camera camera, Camera canvasWorldCamera, bool forceUpdate = false)
        {
            Vector3 worldPosition = Vector3.zero;

            if (worldPositionProvider != null)
            {
                worldPosition = worldPositionProvider.GetWorldPosition();
            }
            else if (owner != null)
            {
                worldPosition = owner.transform.position;
            }
            else
            {
                Debug.LogWarningFormat("OverheadUIIcon: both 'worldPositionProvider' and 'owner' objects are missing for icon '{0}', deactivating itself."
                    , name);
                visible = false;
                return;
            }

            if (forceUpdate || worldPosition != cachedWorldPosition)
            {
                var x = worldPosition.x;
                var y = worldPosition.y;
                var z = worldPosition.z;

                if (float.IsNaN(x) || float.IsInfinity(x) || Math.Abs(x) > 1e8 ||
                    float.IsNaN(y) || float.IsInfinity(y) || Math.Abs(y) > 1e8 ||
                    float.IsNaN(z) || float.IsInfinity(z) || Math.Abs(z) > 1e8)
                {
                    Debug.LogWarningFormat("Wrong World position! worldPosition: {0}, Name: '{1}', Owner name: '{2}'."
                        , worldPosition, name, owner != null ? owner.name : "<none>");
                }
                else
                {
                    cachedWorldPosition = worldPosition;

                    var screenPoint = camera.WorldToScreenPoint(worldPosition);
                    if (screenPositionShift != Vector3.zero)
                    {
                        screenPoint += screenPositionShift;
                    }

                    if (canvasWorldCamera != null)
                    {
                        var backProjectWorldPoint = canvasWorldCamera.ScreenToWorldPoint(screenPoint);
                        // Debug.LogFormat("Screen point: {0}, world point: {1}", screenPoint, backProjectWorldPoint);
                        transform.position = backProjectWorldPoint;
                    }
                    else
                    {
                        transform.position = screenPoint;
                    }
                }
            }
        }
        #endregion

        #region Debug API.
#if DEBUG
        public string DebugGetDebugText()
        {
            var stringBuilder = new StringBuilder(256);

            stringBuilder.AppendFormat("Visible: {0}, Enabled: {1}, Front: {2}", _visible, enabled, frontIcon);

            if (worldPositionProvider != null)
            {
                stringBuilder.AppendFormat("\nProvider: Type: {0}, Enabled: {1}, Allow update: {2}"
                    , worldPositionProvider.GetType().Name, worldPositionProvider.enabled, worldPositionProvider.allowUpdate);
            }

            if (_owner != null)
            {
                stringBuilder.AppendFormat("\nOwner: Type: {0}, Path: '{1}', Enable: {2}, Active: {3}"
                    , _owner.GetType().Name, _owner.transform.GetScenePath(), _owner.enabled, _owner.gameObject.activeInHierarchy);
            }

            return stringBuilder.ToString();
        }
#endif
        #endregion

        #region Private functions.
        IEnumerator AutoHideJob(DateTime startupTime)
        {
            var endTime = startupTime + TimeSpan.FromSeconds(autoHideDelay);

            yield return new WaitUntil(() => DateTime.UtcNow >= endTime);

            visible = false;

            var tmpOnAutoHide = onAutoHide;
            onAutoHide = null;

            autoHideJob = null;

            tmpOnAutoHide?.Invoke();
        }

        private void SetParentToManager()
        {
            var manager = OverheadUIManager.instance;
            if (manager != null)
            {
                transform.SetParent(manager.gameObject.transform, false);
            }
            else
            {
                Debug.LogWarningFormat("Overhead UI manager is not present ('{0}' ({1})).", name, GetType().Name);
            }
        }
        #endregion
    }
}