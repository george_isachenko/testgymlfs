﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using Logic.Quests.Juicer;
using View.UI.Base;
using System.Collections;
using System.Collections.Generic;
using Data.PlayerProfile;
using View.Actions;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Bar Juice Cloud View")]
    public class BarJuiceCloudView : UIGenericMonoBehaviour
    {
        public static BarJuiceCloudView   instance { get; private set; }

        public List<StorageItemInfo>    items       { set { SetItems(value); } }
        public Button                   btn;

        public float                    hideAnimTime { get { return images[0].hideTime; } }

        List<BarJuiceCloudViewItem>     images = new List<BarJuiceCloudViewItem>();

        void Awake()
        {
            instance = this;
            btn = GetComponentInChildren<Button>();

            var img0 = genericView.GetComponent<BarJuiceCloudViewItem>("item");
            images.Add(img0);
        }
            
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<BarJuiceCloudView>
        {
            public Holder()
                : base()
            {
            }

            public Holder(BarJuiceCloudView prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }

        void SetItems(List<StorageItemInfo> itemsInfo)
        {
            var count = 0;
            foreach(var item in itemsInfo)
                count += item.count;

            SetItemsCount(count);

            var idx = 0;
            for(int i = 0; i < itemsInfo.Count; i++)
            {
                for(int j = 0; j < itemsInfo[i].count; j++)
                {
                    images[idx].sprite = itemsInfo[i].sprite;
                    idx++;
                }
            }
        }

        public void OnAnimComplete()
        {
            Debug.LogError("OnAnimComplete");
        }

        int prevItemsCount = 0;
        void SetItemsCount(int count)
        {
            var itemsCountChanged = (prevItemsCount != count);
            while(images.Count < count)
                AddNewItem();

            for(int i = 0; i < images.Count; i++)
            {
                images[i].idleTweenDelay = i * 0.5f;
                images[i].visibility = i < count;
                if (itemsCountChanged && i < count)
                    images[i].ResetIdleAnimation();
            }

            prevItemsCount = count;
            UpdatePositions();
        }

        void AddNewItem()
        {
            var rect = ((RectTransform)transform).rect;
            var obj = images[0].gameObject;
            var newObj = Instantiate(obj,obj.transform.parent);
            newObj.SetActive(true);
            newObj.name = "item" + images.Count.ToString();

            var img = newObj.GetComponent<BarJuiceCloudViewItem>();

            images.Add(img);
        }



        void UpdatePositions()
        {
            var rect = ((RectTransform)transform).rect;
            var rectImg = images[0].rect;

            var enabledItemsCount = 0;
            foreach( var img in images)
            {
                if (img.gameObject.activeSelf)
                    enabledItemsCount++;
            }

            for(int i = 0; i < enabledItemsCount; i++)
            {
                var x = (float)i * rectImg.width / 2.0f - (rectImg.width / 2.0f * enabledItemsCount) / 2.0f;
                var y = 0;

                var img = images[i];
                img.transform.localPosition = new Vector3( x , y , 0);
            }
        }

    }
}
