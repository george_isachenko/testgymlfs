﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using Logic.Quests.Juicer;
using View.UI.Base;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Juicer Order Icon Item")]
    public class JuicerOrderIconItem : UIGenericMonoBehaviour
    {
        public bool     isOn            { set { gameObject.SetActive(value); } }
        public string   amount          { set { genericView.SetText("txt", value); } }
        public bool     checkerOn       { set { genericView.SetActive("ok", value); } }
        public Sprite   sprite          { set { genericView.SetSprite("img", value); } }

    }
} 
