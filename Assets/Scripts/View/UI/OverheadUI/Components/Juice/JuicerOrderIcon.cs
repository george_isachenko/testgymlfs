﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using Logic.Quests.Juicer;
using View.UI.Base;
using System.Collections;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Juicer Order Icon")]
    public class JuicerOrderIcon : UIGenericMonoBehaviour
    {
        public static JuicerOrderIcon   instance { get; private set; }

        [HideInInspector] public JuicerOrderIconItem[]    items = new JuicerOrderIconItem[4];
        [HideInInspector] public JuicerOrderIconItem[]    itemsCopy;
        [HideInInspector] public GameObject               dragObj;
        [HideInInspector] public JuiceDropHandler         dropHandler;
        [HideInInspector] public JuiceOrderDragHandler    dragHandler;

        [HideInInspector] public Text                     rewardCoins;
        [HideInInspector] public Text                     rewardExp;

        public float    compressionTime  = 0.3f;

        public bool arrowOn { set { genericView.SetActive("arrow", value); } }

        void Awake()
        {
            instance = this;

            dropHandler = GetComponentInChildren<JuiceDropHandler>();

            var item0 = genericView.GetComponent<JuicerOrderIconItem>("item");
            items[0] = item0;

            for (int i = 1; i < 4; i++)
            {
                var obj = Instantiate(item0, item0.transform.parent);
                items[i] = obj.GetComponent<JuicerOrderIconItem>();
            }

            rewardCoins = genericView.GetComponent<Text>("juiceRewardCoins");
            rewardExp = genericView.GetComponent<Text>("juiceRewardExp");

            // get dragHandler before making copy
            dragHandler = GetComponentInChildren<JuiceOrderDragHandler>();
            // make Copy of items (this copy will be used to visualize draging)
            dragObj = Instantiate(item0.transform.parent, item0.transform.parent.parent).gameObject;
            dragObj.GetComponentInChildren<JuiceOrderDragHandler>().enabled = false;
            itemsCopy = dragObj.GetComponentsInChildren<JuicerOrderIconItem>();
            // disable background img
            dragObj.GetComponent<Image>().enabled = false;
            dragObj.SetActive(false);


            dragHandler.objectToDrag = dragObj;
        }

        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<JuicerOrderIcon>
        {
            public Holder()
                : base()
            {
            }

            public Holder(JuicerOrderIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }
        #endregion

        public void OnDragBegin()
        {
            foreach(var item in items)
                item.gameObject.SetActive(false);

            dropHandler.isOn = true;

            StartCoroutine(OnDragStartNextFrame());
        }

        IEnumerator OnDragStartNextFrame()
        {
            yield return new WaitForEndOfFrame();

            foreach(var item in itemsCopy)
            {
                LeanTween.moveLocal(item.gameObject, new Vector3(0,0,0), compressionTime);
            }
        }

        public void OnDragEnd(int itemsCount)
        {
            for( int i = 0; i < itemsCount; i++)
                items[i].gameObject.SetActive(true);

            dropHandler.isOn = false;

        }
    }
} 
