﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using View.Actions;
using UnityEngine.UI;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Bar Juice Cloud View Item")]
    public class BarJuiceCloudViewItem : MonoBehaviour 
    {
        
        LeanTweenTransform              leanTweenTransform;
        Image                           img;

        public bool                     visibility      { get { return gameObject.activeSelf; } set { SetVisibility(value); } }
        public Sprite                   sprite          { set { img.sprite = value; } }
        public Rect                     rect            { get { return img.rectTransform.rect; } }

        public float                    hideTime        { get { return leanTweenTransform.tweens[1].time; } }

        [HideInInspector] public float  idleTweenDelay;
        public float                    idleTweenDuration = 2.0f;
        public Vector3                  idleTweenScale  = Vector3.one * 0.7f;
        LTDescr                         tween;

        void Awake()
        {
            leanTweenTransform = GetComponentInChildren<LeanTweenTransform>();
            leanTweenTransform.tweens[1].onComplete.AddListener(VisibilityTurnedOffAnimEnded);
            img = GetComponent<Image>();
        }

        void Start()
        {
            gameObject.SetActive(false); // it is very important to deactivate on start, so VisibilityTurnedOn will be called after activation
        }

        void SetVisibility(bool value)
        {
            if (visibility != value)
            {
                if (value)
                    VisibilityTurnedOn();
                else
                    VisibilityTurnedOff();
            }
        }


        void VisibilityTurnedOn()
        {
            transform.localScale = Vector3.zero;
            gameObject.SetActive(true);
            transform.localScale = Vector3.one;

            //leanTweenTransform.CancelTween(2);
            //LeanTween.delayedCall(delay, () => { leanTweenTransform.RunTween(2); });
            //ResetIdleAnimation();
        }

        public void ResetIdleAnimation()
        {
            if (tween != null)
            {
                tween.reset();
            }

            transform.localScale = Vector3.one;

            tween = LeanTween.scale(gameObject, idleTweenScale, idleTweenDuration).setLoopPingPong();
            tween.delay = idleTweenDelay;
        }

        void VisibilityTurnedOff()
        {
            if (tween != null)
            {
                tween.reset();
                tween = null;
            }
            
            leanTweenTransform.RunTween(1);
        }

        void VisibilityTurnedOffAnimEnded()
        {
            gameObject.SetActive(false);
        }
    }
}