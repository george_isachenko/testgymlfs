﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Fading Single Action Icon")]
    public class AnimatedActionIcon : ActionIcon
    {
        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<SingleActionIcon>
        {
            public Holder()
                : base()
            {
            }

            public Holder(SingleActionIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }

        public enum AnimationType
        {
            VerticalFading,
            JumpingTimer
        }
        #endregion

        #region Private data.
        private Button button;
        private Image backgroundImage;
        private Image iconImage;
        private Image levelImage;
        private Text labelText_;
        private AnimationType _animationType;
        private bool firstUpdate = true;

        #endregion

        #region Public properties.
        public override UnityEvent onClick
        {
            get
            {
                if (button == null)
                    button = GetComponent<Button>();

                return (button != null) ? button.onClick : null;
            }
        }

        public override bool interactable
        {
            get
            {
                if (button == null)
                    button = GetComponent<Button>();

                return (button != null) ? button.interactable : false;
            }

            set
            {
                if (button == null)
                    button = GetComponent<Button>();

                if (button != null)
                    button.interactable = value;
            }
        }

        public override Sprite backgroundSprite
        {
            set
            {
                if (backgroundImage == null)
                {
                    backgroundImage = gameObject.GetComponentInSelfOrChild<Image>("Panel");
                    if (backgroundImage == null)
                        return;
                }

                backgroundImage.sprite = value;
                // backgroundImage.SetNativeSize();
            }
        }

        public override Sprite levelSprite
        {
            set
            {
                if (levelImage == null)
                {
                    levelImage = gameObject.GetComponentInChild<Image>("Level");
                    if (levelImage == null)
                        return;
                }

                levelImage.sprite = value;
                // levelImage.SetNativeSize();
                levelImage.gameObject.SetActive(value != null);
            }
        }

        public override Sprite iconSprite
        {
            set
            {
                if (iconImage == null)
                {
                    iconImage = gameObject.GetComponentInChild<Image>("Icon");
                    if (iconImage == null)
                        return;
                }

                iconImage.sprite = value;
                // iconImage.SetNativeSize();
                iconImage.gameObject.SetActive(value != null);
            }
        }

        public override string labelText
        {
            set
            {
                if (labelText_ == null)
                {
                    labelText_ = gameObject.GetComponentInChild<Text>("Label");
                    if (labelText_ == null)
                        return;
                }

                labelText_.text = value;
                labelText_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }

        public virtual AnimationType animationType
        {
            set
            {
                _animationType = value;
            }
        }
        #endregion
        
        void Update()
        {
            if (!firstUpdate)
                return;

            firstUpdate = false;
            var animator = GetComponent<Animator>();
            var p = (int)_animationType;
            animator.SetInteger("AnimationType", p);
        }
    }
}