﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Single Action Animated Icon")]
    public class SingleActionAnimatedIcon : SingleActionIcon
    {
        public enum Animations
        {
            None,
            Shaking
        }

        public Animations curAnimation;

        Image iconImage_;

        void Start()
        {
            iconImage_ = gameObject.GetComponentInChild<Image>("Icon");
            //SetupForAnimation(curAnimation);
        }

        void SetupForAnimation(Animations animType)
        {
            switch (animType)
            {
                case Animations.Shaking:
                    iconImage_.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

                    iconImage_.rectTransform.anchorMin = new Vector2(0.5f, 0.2f);
                    iconImage_.rectTransform.anchorMax = new Vector2(0.5f, 0.2f);
                    iconImage_.rectTransform.pivot = new Vector2(0.5f, 0);
                    break;
            }

        }

        /*
        void Update()
        {
            
            switch (curAnimation)
            {
                case Animations.Shaking:
                    {
                        float tF = Mathf.Abs(((float)Math.Sin(Time.realtimeSinceStartup) * 0.1f));
                        float tA = (float)Math.Sin(Time.realtimeSinceStartup * tF) * 15.0f;
                        iconImage_.transform.rotation = Quaternion.Euler(0, 0, tA);
                        break;
                    }
            }

        }
        */
         
    }
} 
