﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Equipment Placement Icon")]
    public class EquipmentPlacementIcon : MonoBehaviour
    {
        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<EquipmentPlacementIcon>
        {
            public Holder()
                : base()
            {
            }

            public Holder(EquipmentPlacementIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }
        #endregion

        #region Public properties.
        public UnityEvent onSellClick
        {
            get
            {
                return (btnSell != null) ? btnSell.onClick : null;
            }
        }

        public UnityEvent onRotateClick
        {
            get
            {
                return (btnRotate != null) ? btnRotate.onClick : null;
            }
        }

        public UnityEvent onConfirmClick
        {
            get
            {
                return (btnConfirm != null) ? btnConfirm.onClick : null;
            }
        }

        public UnityEvent onCancelClick
        {
            get
            {
                return onCancelEvent;
            }
        }

        public bool sellActive
        {
            set
            {
                if (btnSell != null)
                    btnSell.interactable = value;
            }
        }

        public bool rotateActive
        {
            set
            {
                if (btnRotate != null)
                    btnRotate.interactable = value;
            }
        }

        public bool rotateEnabled
        {
            set
            {
                if (btnRotate != null)
                    btnRotate.gameObject.SetActive(value);
            }
        }

        public bool confirmActive
        {
            set
            {
                if (btnConfirm != null)
                    btnConfirm.interactable = value;
            }
        }

        public bool cancelActive
        {
            set
            {
                if (btnCancel != null)
                    btnCancel.interactable = value;

                if (btnCancelLeft != null)
                    btnCancelLeft.interactable = value;
            }
        }
        #endregion

        #region Private data.
        private Button btnSell;
        private Button btnRotate;
        private Button btnConfirm;
        private Button btnCancel;
        private Button btnCancelLeft;
        private UnityEvent onCancelEvent; 
        #endregion

        #region Unity API.
        private void Awake()
        {
            btnSell = gameObject.GetComponentInChild<Button>("Sell");
            btnRotate = gameObject.GetComponentInChild<Button>("Rotate");
            btnConfirm = gameObject.GetComponentInChild<Button>("Confirm");
            btnCancel = gameObject.GetComponentInChild<Button>("Cancel");
            btnCancelLeft = gameObject.GetComponentInChild<Button>("Cancel_left");

            onCancelEvent = new UnityEvent();

            if (btnCancel != null)
            {
                btnCancel.onClick.AddListener (OnCancelClick);
            }

            if (btnCancelLeft != null)
            {
                btnCancelLeft.onClick.AddListener (OnCancelClick);
            }
        }

        void Update()
        {
            //if (Application.platform == RuntimePlatform.Android)
            //{
            if (Input.GetKey(KeyCode.Escape))
            {
                if (onCancelEvent != null)
                {
                    onCancelEvent.Invoke();
                }
            }
            //}
        }
        #endregion

        #region Public API.
        public void UseLeftCancelButton()
        {
            if (btnCancelLeft != null)
                btnCancelLeft.gameObject.SetActive(true);

            if (btnSell != null)
                btnSell.gameObject.SetActive(false);
        }

        public void UseLeftSellButton()
        {
            if (btnCancelLeft != null)
                btnCancelLeft.gameObject.SetActive(false);

            if (btnSell != null)
                btnSell.gameObject.SetActive(true);
        }
            
        public void UseRightCancelButton()
        {
            if (btnCancel != null)
                btnCancel.gameObject.SetActive(true);

            if (btnConfirm != null)
                btnConfirm.gameObject.SetActive(false);
        }

        public void UseRightConfirmButton()
        {
            if (btnCancel != null)
                btnCancel.gameObject.SetActive(false);

            if (btnConfirm != null)
                btnConfirm.gameObject.SetActive(true);
        }
        #endregion

        #region Private functions.
        void OnCancelClick()
        {
            if (onCancelEvent != null)
            {
                onCancelEvent.Invoke();
            }
        }
        #endregion
    }
}