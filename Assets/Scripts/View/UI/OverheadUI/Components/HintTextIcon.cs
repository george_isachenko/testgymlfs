﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Hint Text Icon")]
    public class HintTextIcon : MonoBehaviour
    {
        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<HintTextIcon>
        {
            public Holder()
            {
            }

            public Holder(HintTextIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }
        #endregion

        public Text txtHint{ get; private set;}
        public Image sprIcon{ get; private set;}

        void Awake()
        {
            var items = transform.Find("Items");

            txtHint = items.Find("Text").GetComponent<Text>();
            sprIcon = items.Find("Image").GetComponent<Image>();

            sprIcon.gameObject.SetActive(false);

			//LeanTween.moveLocalY(txtHint.gameObject, tweenDistance, tweenTime).setLoopPingPong().setEase(LeanTweenType.easeInOutSine);
        }
    }
}