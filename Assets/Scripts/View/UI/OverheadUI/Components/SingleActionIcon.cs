﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

namespace View.UI.OverheadUI.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Components/Single Action Icon")]
    public class SingleActionIcon : ActionIcon
    {
        #region Types.
        [Serializable]
        public class Holder : OverheadIconInstanceHolder<SingleActionIcon>
        {
            public Holder()
                : base()
            {
            }

            public Holder(SingleActionIcon prefab, MonoBehaviour owner = null, bool persistent = false)
                : base(prefab, owner, persistent)
            {
            }
        }
        #endregion

        #region Private data.
        private Button button;
        private Image backgroundImage;
        private Image iconImage;
        private Image levelImage;
        private Text labelText_;
        #endregion

        #region Public properties.
        public override UnityEvent onClick
        {
            get
            {
                if (button == null)
                    button = GetComponent<Button>();

                return (button != null) ? button.onClick : null;
            }
        }

        public override bool interactable
        {
            get
            {
                if (button == null)
                    button = GetComponent<Button>();

                return (button != null) ? button.interactable : false;
            }

            set
            {
                if (button == null)
                    button = GetComponent<Button>();

                if (button != null)
                    button.interactable = value;
            }
        }

        public override Sprite backgroundSprite
        {
            set
            {
                if (backgroundImage == null)
                {
                    backgroundImage = gameObject.GetComponentInSelfOrChild<Image>("Panel");
                    if (backgroundImage == null)
                        return;
                }

                backgroundImage.sprite = value;
                // backgroundImage.SetNativeSize();
            }
        }

        public override Sprite levelSprite
        {
            set
            {
                if (levelImage == null)
                {
                    levelImage = gameObject.GetComponentInChild<Image>("Level");
                    if (levelImage == null)
                        return;
                }

                levelImage.sprite = value;
                // levelImage.SetNativeSize();
                levelImage.gameObject.SetActive(value != null);
            }
        }

        public override Sprite iconSprite
        {
            set
            {
                if (iconImage == null)
                {
                    iconImage = gameObject.GetComponentInChild<Image>("Icon");
                    if (iconImage == null)
                        return;
                }

                iconImage.sprite = value;
                // iconImage.SetNativeSize();
                iconImage.gameObject.SetActive(value != null);
            }
        }

        public override string labelText
        {
            set
            {
                if (labelText_ == null)
                {
                    labelText_ = gameObject.GetComponentInChild<Text>("Label");
                    if (labelText_ == null)
                        return;
                }

                labelText_.text = value;
                labelText_.gameObject.SetActive(value != null && value != string.Empty);
            }
        }
        #endregion
    }
} 
