using CustomAssets.Lists;
using UnityEngine;
using UnityEngine.Events;

namespace View.UI.OverheadUI.Components
{
    public abstract class ActionIcon : MonoBehaviour
    {
        #region Public (Inspector) fields.
        public SpritesList backgroundImages;
        public SpritesList iconsImages;
        public SpritesList exerciseImages;
        public SpritesList levelImages;
        #endregion

        #region Public properties.
        public virtual UnityEvent onClick { get; }

        public virtual bool interactable { get; set; }

        public virtual Sprite backgroundSprite
        {
            set { }
        }

        public virtual Sprite levelSprite
        {
            set { }
        }

        public virtual Sprite iconSprite
        {
            set { }
        }

        public virtual string labelText
        {
            set { }
        }

        #endregion
    }
}