﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

#if UNITY_EDITOR

using UnityEditor;
using View.EditorHelpers;

#endif

namespace View.UI.OverheadUI
{
    [AddComponentMenu("Kingdom/View/UI/Overhead UI/Overhead UI Manager")]
    [DisallowMultipleComponent]
    public class OverheadUIManager : MonoBehaviour
    {
        #region Inspector fields.
        [SerializeField]
        protected float zoomThreshold;

        [SerializeField]
        protected float zoomThresholdGap;
        #endregion

        #region Static instance.
        public static OverheadUIManager instance { get; private set; }
        #endregion

        #region Private data.
        private List<OverheadUIIcon> icons = new List<OverheadUIIcon>(128);
        private Canvas canvas;
        private Vector3 cachedMainCameraPosition = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity); // Forces update in LateUpdate().
        private Quaternion cachedMainCameraRotation = Quaternion.identity;
        private bool defaultDisplayMode = true;
        //private bool nextDisplayMode = true;
        private bool childrensListChanged = false;
        private bool forceUpdatePosition = false;
        private List<int> sortingLayerExceptions = new List<int>(8);
        #endregion

        #region Unity API.
        void Awake()
        {
            if (instance != null)
            {
                // only one system tracker allowed per scene
                Debug.LogWarningFormat("Destroying duplicate '{0}' object - only one is allowed per scene!", gameObject.name);
                Destroy(gameObject);
                return;
            }

            canvas = GetComponent<Canvas>();
            instance = this;
        }

        void Start()
        {
        }

        void OnDestroy()
        {
            Assert.IsNotNull(instance);
            instance = null;
        }

        void OnEnable()
        {
            cachedMainCameraPosition = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity); // Forces update in LateUpdate().
        }

        void OnDisable()
        {

        }

        void OnTransformChildrenChanged()
        {
            childrensListChanged = true;
        }

        private void LateUpdate()
        {
            // Update on-screen positions by world positions.
            var mainCamera = Camera.main;
            if (mainCamera == null)
                return;

            var canvasWorldCamera = canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : canvas.worldCamera;

            if (mainCamera.transform.position != cachedMainCameraPosition || mainCamera.transform.rotation != cachedMainCameraRotation)
            {
                cachedMainCameraPosition = mainCamera.transform.position;
                cachedMainCameraRotation = mainCamera.transform.rotation;
                forceUpdatePosition = true;
            }

            if (childrensListChanged)
            {
                gameObject.GetComponentsInChildren(true, icons);
                forceUpdatePosition = true;
                childrensListChanged = false;
            }

            foreach (var icon in icons)
            {
                var active = icon.visible && CheckLayervisibility(icon.sortingLayer);

                if (active)
                {
                    icon.UpdatePosition(mainCamera, canvasWorldCamera, forceUpdatePosition);

                    bool needScale = icon.scaleDownInDistance && !sortingLayerExceptions.Contains(icon.sortingLayer);

                    if (needScale && Camera.main != null)
                    {
                        float distToCam = (icon.worldPosition - Camera.main.transform.position).magnitude;
                        if (distToCam > zoomThreshold)
                        {
                            if (distToCam > zoomThreshold + zoomThresholdGap)
                            {
                                icon.transform.localScale = Vector3.zero;
                            }
                            else
                            {
                                float zoomCoef = (zoomThreshold + zoomThresholdGap - distToCam) / zoomThresholdGap * icon.scaleRatio;
                                icon.transform.localScale = new Vector3(zoomCoef, zoomCoef, zoomCoef);
                            }
                        }
                        else
                        {
                            icon.transform.localScale = Vector3.one * icon.scaleRatio;
                        }
                    }
                }

                active = active && (icon.transform.position.z > 0);
                if (icon.gameObject.activeSelf != active)
                {
                    icon.gameObject.SetActive(active);
                }
            }

            if (icons.Count > 1)
            {
                // Sorting: 1). By sorting layer; 2). Then by "front" state. 3). Then by z.
                icons.Sort(
                    (a, b) =>
                        (a.sortingLayerIndex == b.sortingLayerIndex)
                        ? (a.frontIcon == b.frontIcon)
                            ? Math.Sign(b.transform.position.z - a.transform.position.z)
                            : a.frontIcon ? 1 : -1
                        : Math.Sign(a.sortingLayerIndex - b.sortingLayerIndex)
                );
            }

            int siblingIndex = 0;

            foreach (var icon in icons)
            {
                if (icon.gameObject.activeSelf)
                {
                    icon.transform.SetSiblingIndex(siblingIndex++);
                }
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR && DEBUG
        void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                bool show = false;
                foreach (var sel in Selection.gameObjects)
                {
                    var obj = sel.transform;
                    while (obj != null)
                    {
                        if (ReferenceEquals(obj, gameObject.transform))
                        {
                            show = true;
                            break;
                        }
                        obj = obj.transform.parent;
                    }

                    if (show)
                        break;
                }

                if (show)
                {
                    var icons = GetComponentsInChildren<OverheadUIIcon>(true); // Always get 'live' representation here, instead of optimized for game.

                    foreach (var icon in icons)
                    {
                        if (Selection.Contains(icon.gameObject) || (icon.owner != null && Selection.Contains(icon.owner)))
                        {
                            var debugText = icon.DebugGetDebugText();

                            var position = icon.worldPosition;

                            if (debugText != null)
                            {
                                Handles.Label(position, debugText, EditorGizmoHelper.instance.GetOverheadLabelStyle());
                            }

                            // Draw line between it's representative object and world position.
                            if (icon.visible)
                            {
                                if (icon.owner != null)
                                {
                                    var target = icon.owner.transform.position;

                                    EditorGizmoHelper.DrawArrow(position, target, Color.white, 0.0f, 0.25f);
                                }

                                EditorGizmoHelper.DrawArrow(icon.transform.position, position, Color.cyan, 0.0f, 0.25f);
                            }
                        }
                    }
                }
            }
        }
#endif
        #endregion

        #region Public API.
        public void SetDefaultDisplayMode(bool defaultToShowAll)
        {
            defaultDisplayMode = defaultToShowAll;
            forceUpdatePosition = true;
        }

        public void ClearLayerExceptions()
        {
            sortingLayerExceptions.Clear();
            forceUpdatePosition = true;
        }

        public void AddSortingLayerException(int sortingLayerID)
        {
            if (!sortingLayerExceptions.Contains(sortingLayerID))
                sortingLayerExceptions.Add(sortingLayerID);
            forceUpdatePosition = true;
        }
        #endregion

        #region Private API.
        private bool CheckLayervisibility(int sortingLayerID)
        {
            return sortingLayerExceptions.Contains(sortingLayerID) ? !defaultDisplayMode : defaultDisplayMode;
        }
        #endregion
    }
}