﻿using System;
using Data;
using UnityEngine;
using UnityEngine.UI;
using View.Actions;
using View.UI.Base;

namespace View.UI
{
    public class SplashScreenView : UIGenericMonoBehaviour
    {
        #region Public properties.
        public string progressText
        {
            set
            {
                if (value != null)
                    genericView.SetText("txtProgress", value != string.Empty ? value : Loc.Get("idLoadingBar") );
                
                genericView.SetActive("txtProgress", (value != null));
            }
        }

        public string tipText
        {
            set
            {
                if (value != null)
                    genericView.SetText("txtTip", value);
                
                genericView.GetComponent<LocalizationHelper>("txtTip").enabled = (value == null);
            }
        }

        public bool tipOn
        {
            set
            {
                genericView.SetActive("txtTip", value);
            }
        }

        public string versionText
        {
            set
            {
                if (value != null)
                    genericView.SetText("txtVersion", value);
            }

            get
            {
                var txt = genericView.GetComponent<Text>("txtVersion");
                return txt != null ? txt.text : string.Empty;
            }
        }

        public bool   progressOn
        {
            set
            {
                genericView.SetActive("sliderProgress", value);
            }
        }

        public float   progressValue
        {
            set
            {
                genericView.SetSliderValue("sliderProgress", value);
            }
        }

/*
        public Transform teaserSportsmanHolder
        {
            get
            {
                return genericView.GetTransform("teaserSportsman");
            }
        }
*/
        #endregion

        #region Private fields.
        #endregion

        #region Unity API.
        private void Awake()
        {
        }
        #endregion
            
        #region Public API.
        public void Show(Action onComplete = null, bool forceImmediate = false)
        {
            gameObject.SetActive(true);
            transform.parent.gameObject.SetActive(true);

            var ltt = genericView.GetComponent<LeanTweenTransform>("splashBG");
            if (!forceImmediate && ltt != null)
            {
                ltt.CancelTween(0);
                ltt.RunTween(1, onComplete);
            }
            else
            {
                onComplete?.Invoke();
            }
        }

        public void Hide(Action onComplete = null, bool forceImmediate = false)
        {
            progressOn = false;
            tipOn = false;

            var ltt = genericView.GetComponent<LeanTweenTransform>("splashBG");
            if (!forceImmediate && ltt != null)
            {
                ltt.CancelTween(1);
                ltt.RunTween(0, () =>
                    {
                        gameObject.SetActive(false);
                    
                        onComplete?.Invoke();
                    });
            }
            else
            {
                gameObject.SetActive(false);
                onComplete?.Invoke();
            }
        }

        #endregion

        #region Private functions.
        #endregion
   }
}