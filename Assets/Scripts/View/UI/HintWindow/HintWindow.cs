﻿using System;
using UnityEngine;

namespace View.UI.HintWindow
{
    [AddComponentMenu("Kingdom/View/UI/Hint Window/Hint Window")]
    [Obsolete("Update code before use!")]
    public class HintWindow : MonoBehaviour
    {
        // Use this for initialization
        private void Start()
        {
        }

        public void KillWindow()
        {
            Destroy(gameObject);
        }
    }
}
