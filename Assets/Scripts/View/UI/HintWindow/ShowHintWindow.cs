﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace View.UI.HintWindow
{
    [AddComponentMenu("Kingdom/View/UI/Hint Window/Show Hint Window")]
    [Obsolete("Update code before use!")]
    public class ShowHintWindow : MonoBehaviour
    {
        private Canvas canvas;
        public GameObject hintWindow;
        public string message;

        private void Awake()
        {
            canvas = GameObject.Find("_GUIRoot").GetComponent<Canvas>();
        }

        public void DisplayHintMessage()
        {
            // TODO: Protection from spawning too many hint windows!

            if (canvas != null && hintWindow != null)
            {
                var window = Instantiate(hintWindow);
                Assert.IsNotNull(window);
                if (window != null)
                {
                    window.transform.SetParent(canvas.transform, false);

                    var text = window.GetComponent<Text>();
                    Assert.IsNotNull(text);
                    if (text != null)
                    {
                        text.text = message;
                        text.Rebuild(CanvasUpdate.PostLayout);
                    }

                    /*
                                    var animator = window.GetComponent<Animator>();
                                    Assert.IsNotNull (animator);
                                    if (animator != null)
                                    {
                                        //animator.
                                    }
                    */
                }
            }
        }
    }
}
