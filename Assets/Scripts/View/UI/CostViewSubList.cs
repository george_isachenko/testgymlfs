using System;
using System.Collections.Generic;
using System.Linq;
using Core.Timer;
using Data;
using Logic.Facades;
using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;
using Presentation.Providers;
using UnityEngine.Assertions;
using UnityEngine.UI;
using View.UI.Base;
using Data.PlayerProfile;
using Logic.Quests.Tutorial;

namespace View.UI
{
    public class CostViewSubList : UIViewSubList<CostViewSubListItem>
    {
        public class CostViewSubListMediator
        {
            public event Action<ResourceType, RectTransform> onShowHintRequest;
            public event Action onHideTooltipRequest;

            private CostViewSubList view;

            public CostViewSubListMediator(CostViewSubList costViewSubList)
            {
                view = costViewSubList;
            }

            public void SetView(TimerFloat timer)
            {
                view.genericView.SetActive("resourcesPanel", false);
                view.genericView.SetActive("timerPanel");
                view.timer = timer;
                view.UpdateTimerView();
            }

            public void SetView(Cost cost, IPlayerProfileInteractionProvider profile)
            {
                view.genericView.SetActive("resourcesPanel");
                view.genericView.SetActive("timerPanel", false);
                view.timer = null;
                KeyValuePair<ResourceType, int>[] resources = null;

                var itemsCount = 0;
                switch (cost.type)
                {
                    case Cost.CostType.None:
                        itemsCount = 0;
                        break;
                    case Cost.CostType.CoinsOnly:
                        itemsCount = 1;
                        break;
                    case Cost.CostType.BucksOnly:
                        itemsCount = 1;
                        break;
                    case Cost.CostType.ResourcesOnly:
                        resources = cost.resources.ToArray();
                        itemsCount = cost.resources.Count;
                        break;
                    case Cost.CostType.ResourcesAndCoins:
                        resources = cost.resources.ToArray();
                        itemsCount = cost.resources.Count + 1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                view.UpdateItemsCount(itemsCount);
                foreach (var item in view.items)
                {
                    item.ClearHintCallbacks();
                }
                for (int i = 0, resourceIdx = 0; i < itemsCount; i++)
                {
                    var itemView = view[i];
                    if (cost.type != Cost.CostType.ResourcesOnly && i==0)
                    {
                        if (cost.type == Cost.CostType.BucksOnly)
                        {
                            SetBucks(itemView, cost.value, profile);
                        }
                        else
                        {
                            SetCoins(itemView, cost.value, profile);
                        }
                        continue;
                    }
                    Assert.IsNotNull(resources);
                    SetResource(itemView, resources[resourceIdx], profile);
                    resourceIdx++;
                }
            }

            private void SetResource(CostViewSubListItem itemView, KeyValuePair<ResourceType, int> resource, IPlayerProfileInteractionProvider profile)
            {
                var currentCount = profile.storage.GetResourcesCount(resource.Key);
                var goalCount = resource.Value;
                var fitbucksCost = DataTables.instance.resourceShop[resource.Key].bucks*Mathf.Max(0, resource.Value - profile.storage.GetResourcesCount(resource.Key));

                var goalReached = currentCount >= goalCount;
                var textColor = goalReached ? "#B0E727FF" : "red";
                var outlineColor = goalReached ? Color.black : Color.white;
                var countText = goalReached
                    ? $"<color={textColor}>{currentCount}/{goalCount}</color>"
                    : $"<color=#333333><color={textColor}>{currentCount}</color>/{goalCount}</color>";

                itemView.genericView.GetComponent<Outline>("itemCount").effectColor = outlineColor;
                itemView.genericView.SetSprite("itemIcon", Cost.GetSprite(resource.Key));
                itemView.genericView.SetText("itemCount", countText);
                itemView.genericView.SetActive("goalReachedPanel", goalReached);
                itemView.genericView.SetActive("fitbucksCostPanel", !goalReached);
                itemView.genericView.SetText("fitbucksCostText", fitbucksCost.ToString());

                itemView.idx = (int) resource.Key;
                itemView.startHint.AddListener(item =>
                {
                    if (onShowHintRequest != null)
                        onShowHintRequest((ResourceType) item.idx, (RectTransform) item.transform);
                });
                itemView.endHint.AddListener(item =>
                {
                    if (onHideTooltipRequest != null)
                        onHideTooltipRequest();
                });
            }

            private void SetBucks(UIGenericMonoBehaviour itemView, int count, IPlayerProfileInteractionProvider profile)
            {
                var icon = GUICollections.instance.commonIcons.array[0];

                var goalReached = profile.fitBucks >= count;
                var outlineColor = Color.white;
                var countText = $"<color=#333333>{count}</color>";

                itemView.genericView.GetComponent<Outline>("itemCount").effectColor = outlineColor;
                itemView.genericView.SetSprite("itemIcon", icon);
                itemView.genericView.SetText("itemCount", countText);
                itemView.genericView.SetActive("goalReachedPanel", goalReached);
                itemView.genericView.SetActive("fitbucksCostPanel", !goalReached);
                itemView.genericView.SetText("fitbucksCostText", count.ToString());
            }

            private void SetCoins(UIGenericMonoBehaviour itemView, int count, IPlayerProfileInteractionProvider profile)
            {
                var currentCount = profile.coins;
                var goalCount = count;
                var fitbucksCost = Cost.CoinsToFitBucks(goalCount - currentCount);

                var goalReached = currentCount >= goalCount;
                var textColor = goalReached ? "#B0E727FF" : "red";
                var outlineColor = goalReached ? Color.black : Color.white;
                var countText = goalReached
                    ? $"<color={textColor}>{currentCount}/{goalCount}</color>"
                    : $"<color=#333333><color={textColor}>{currentCount}</color>/{goalCount}</color>";

                itemView.genericView.GetComponent<Outline>("itemCount").effectColor = outlineColor;
                itemView.genericView.SetSprite("itemIcon", Cost.GetSpriteCoins());
                itemView.genericView.SetText("itemCount", countText);
                itemView.genericView.SetActive("goalReachedPanel", goalReached);
                itemView.genericView.SetActive("fitbucksCostPanel", !goalReached);
                itemView.genericView.SetText("fitbucksCostText", fitbucksCost.ToString());
                itemView.genericView.GetComponent<Text>("itemCount").fontSize = 40;
            }
        }

        public CostViewSubListMediator mediator
        {
            get { return mediatorInstance ?? (mediatorInstance = new CostViewSubListMediator(this)); }
        }

        public event Action<ResourceType, RectTransform> onShowHintRequest;
        public event Action onHideTooltipRequest;

        public UnityEvent onTimerFinished = new UnityEvent();

        private TimerFloat timer;
        private CostViewSubListMediator mediatorInstance;

        private void Awake()
        {
            itemsRoot = genericView.GetTransform("resourcesPanel");

            mediator.onShowHintRequest += (type, transform) => onShowHintRequest(type, transform);
            mediator.onHideTooltipRequest += () => onHideTooltipRequest();
        }

        private void Update()
        {
            if (timer == null) return;
            UpdateTimerView();
            if (timer.secondsToFinish > 0) return;
            timer = null;
            onTimerFinished.Invoke();
        }

        private void UpdateTimerView()
        {
            var timeToFininsh = 0;
            if (timer != null && timer.secondsToFinish >= 0)
                timeToFininsh = (int) timer.secondsToFinish;
            genericView.SetText("timeText", PrintHelper.GetTimeString(new TimeSpan(0, 0, timeToFininsh)));
        }
    }
}