using System;
using Core;
using Core.Timer;
using UnityEngine;
using View.UI;
using View.UI.Base;

public class GymUnlockView : UIBaseView
{
    private Action<string> onUnlockPressed;
    // private Action setDirty = () => { };

    private TimerFloat unlockTimer;
    public CostViewSubList costView { get; private set; }
    
    private string roomName;

    private void Awake()
    {
        BindCloseButton();
        genericView.SetButtonCallback("unlockButton", () =>
        {
            if (onUnlockPressed != null)
            {
                var tmpAction = onUnlockPressed;
                onUnlockPressed = null;
                tmpAction.Invoke(roomName);
            }

            SetForceShowSkipAnimationState(false);
            ActivateDelayedSkipShowing();
        });

        OnShowHandler.AddListener(() =>
        {
            SetForceShowSkipAnimationState(true);
        });

        costView = genericView.GetComponent<CostViewSubList>("costViewSubList");
        // costView.onTimerFinished.AddListener(() => setDirty());
    }

    public void UpdateView(GymUnlockViewData viewData, Action<string> onUnlockPressed)
    {
        this.onUnlockPressed = onUnlockPressed;
        this.roomName = viewData.roomName;

        switch (viewData.showType)
        {
            case GymUnlockViewData.GymUnlockViewDataType.Timer:
                genericView.SetText("titleText", viewData.title);
                genericView.SetText("descriptionText", viewData.messageText);
                genericView.SetActive("priceText");
                genericView.SetActive("buttonBucksIcon");
                genericView.SetActive("unlockText", false);
                genericView.SetActive("buttonCoinIcon", false);
                genericView.SetActive("skipLabel");
                costView.mediator.SetView(viewData.timerViewData);
                unlockTimer = viewData.timerViewData;
                UpdateTimerView();
                break;

            case GymUnlockViewData.GymUnlockViewDataType.Resources:
                unlockTimer = null;
                genericView.SetText("titleText", viewData.title);
                genericView.SetText("descriptionText", viewData.messageText);
                genericView.SetText("priceText", viewData.priceText);
                genericView.SetActive("priceText", viewData.priceTextActive);
                genericView.SetActive("buttonBucksIcon", viewData.buttonBucksIconActive);
                genericView.SetActive("unlockText", viewData.unlockTextActive);
                genericView.SetActive("buttonCoinIcon", false);
                genericView.SetActive("skipLabel", false);
                costView.mediator.SetView(viewData.unlockCost, viewData.playerProfile);
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Update()
    {
        if (unlockTimer == null) return;
        UpdateTimerView();
        if (unlockTimer.secondsToFinish>0)return;
        unlockTimer = null;
    }

    private void UpdateTimerView()
    {
        var cost = DynamicPrice.GetSkipCost((int) unlockTimer.secondsToFinish);
        genericView.SetActive("skipLabel", cost>0);
        genericView.SetActive("buttonBucksIcon", cost>0);
        genericView.SetActive("priceText", cost>0);
        genericView.SetActive("unlockText", cost <= 0);
        genericView.SetText("priceText", cost.ToString());
    }

    public void ActivateDelayedSkipShowing()
    {
        var anim = genericView.GetComponent<Animator>("unlockButton");
        if (!anim.isInitialized)
            return;
        anim.SetTrigger("animateShowing");
    }

    public void SetForceShowSkipAnimationState(bool isShown)
    {
        var anim = genericView.GetComponent<Animator>("unlockButton");
        if (!anim.isInitialized)
            return;
        anim.SetBool("forceShow", isShown);
    }
}