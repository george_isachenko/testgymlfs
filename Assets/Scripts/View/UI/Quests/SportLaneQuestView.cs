﻿using System;
using Core;
using Core.Timer;
using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using View;
using View.UI;
using View.UI.Base;
using System.Linq;
using Data;
using View.Actions;

public class SportLaneQuestView : UIBaseView
{
    public delegate void OnQuestSelectedDelegate(int questIndex);
    public OnQuestSelectedDelegate onQuestSelected;
    public Action onByCompleteButtonPressed;
    public Action onClaimButtonPressed      { set { genericView.SetButtonCallback("claimButton", value); } }
    public Action onHelpButtonPressed       { set { genericView.SetButtonCallback("helpButton", value); } }
    public UnityEvent onOpen = new Button.ButtonClickedEvent();
    public UnityEvent onSkipCooldown = new UnityEvent();

    private LaneQuestNamesList namesList;
    private LaneQuestObjectivesList objectivesList;
    private LaneQuestObjectivesList nextObjectivesList;
    private LaneQuestRewardsList rewardsList;
    private TimerFloat timer;
    private CostViewSubList cooldownCostView;

    public float progress   { set { genericView.SetSliderValue("sliderProgress", value); } }
    public bool  progressOn { set { genericView.SetActive("sliderProgress", value); } }

    public CanvasGroup  sequenceRewardCanvas    { get { return genericView.GetComponent<CanvasGroup>("sequenceRewardOn"); } }
    public Vector2      sequenceRewardPos       { get { return genericView.GetTransform("sequenceRewardImage").position; } }
    public bool         sequenceRewardOn        { set { genericView.SetActive("sequenceRewardOn", value); } }
    public Cost         sequenceReward          { set { SetReward(value); } }

    int      sequenceRewardValue { set { genericView.SetText("sequenceReward", value.ToString()); } }
    Sprite   sequenceRewardImage { set { genericView.SetSprite("sequenceRewardImage", value); } }

    #region Public fields.
    public Color normalQuestNameColor;
    public Color claimedQuestNameColor;
    #endregion

    private void SetReward(Cost reward)
    {
        if (reward.type == Cost.CostType.BucksOnly)
        {
            sequenceRewardImage = Cost.GetSpriteBucks();
            sequenceRewardValue = reward.value;
        }
        else
        {
            sequenceRewardImage = Cost.GetSprite(Data.PlayerProfile.ResourceType.Platinum);
            sequenceRewardValue = reward.resources[Data.PlayerProfile.ResourceType.Platinum];
        }
                
    }

    private void Awake()
    {
        namesList =
            UIViewSubListFactory<LaneQuestNamesList, LaneQuestNameItem>.CreateUiList(
                genericView.GetTransform("questNamesRoot"), "UI/ListViewItems/QuestNameItem");

        objectivesList =
            UIViewSubListFactory<LaneQuestObjectivesList, LaneQuestObjectiveItem>.CreateUiList(
                genericView.GetTransform("objectivesRoot"), "UI/ListViewItems/QuestObjectiveItem");

        nextObjectivesList =
            UIViewSubListFactory<LaneQuestObjectivesList, LaneQuestObjectiveItem>.CreateUiList(
                genericView.GetTransform("nextObjectivesRoot"), "UI/ListViewItems/QuestObjectiveItem");

        rewardsList =
            UIViewSubListFactory<LaneQuestRewardsList, LaneQuestRewardItem>.CreateUiList(
                genericView.GetTransform("rewardsRoot"), "UI/ListViewItems/QuestRewardItem");

        cooldownCostView = genericView.GetComponent<CostViewSubList>("cooldownCostView");

        genericView.SetButtonCallback("btnClose", () =>
        {
            if (OnClosedByUserClick_Callback != null) OnClosedByUserClick_Callback();
            Hide();
        });

//        genericView.SetButtonCallback("btnClose", Hide);

        namesList.OnItemSelected_Callback += v => onQuestSelected(v.idx);

        /*genericView.SetButtonCallback("buyCompleteButton", () =>
        {
            if (onByCompleteButtonPressed != null)
                onByCompleteButtonPressed();
        });*/
        genericView.SetButtonCallback("skipCooldownButton", () => onSkipCooldown.Invoke());
    }

    public void SetQuestInfo(SportLaneQuestInfoViewData viewData)
    {
        if (viewData.showCooldownQuestInfo)
        {
            nextObjectivesList.SetObjectives(viewData);
            cooldownCostView.mediator.SetView(viewData.currentStateTimer);
            genericView.SetActive("laneQuestTimerPanel", false);
            timer = viewData.currentStateTimer;
        }
        else
        {
            SetRewardsVisibility(viewData.isRewardsHidden);
            objectivesList.SetObjectives(viewData);
            rewardsList.SetReward(viewData);

            timer = viewData.currentStateTimer;
            if (viewData.currentStateTimer != null && viewData.currentStateTimer.secondsToFinish <= 0)
            {
                timer = null;
            }
            genericView.SetActive("laneQuestTimerPanel", timer != null);
        }

//        genericView.SetActive("laneQuestTimerPanel", !viewData.showCooldownQuestInfo);
        genericView.SetText("selectedQuestName", viewData.questName);
        //genericView.SetText("buyCompleteButtonText", viewData.buyButtonText);
        genericView.SetActive("claimButton", viewData.isClaimButtonEnabled);
        //genericView.SetActive("buyCompleteButton", viewData.isBuyCompleteButtonEnabled);
        genericView.SetActive("helpButton", false);
        genericView.SetActive("completeLabel", viewData.isCompletedLabelShown);
        genericView.SetActive("buttonsPlaceholder", !viewData.isCompletedLabelShown);
        //genericView.GetComponent<Button>("buyCompleteButton").interactable = false;
        genericView.SetActive("skipCooldownButton");
        genericView.SetText("characterTextBubble", viewData.bubbleText);
        genericView.SetText("laneQuestsHeader",viewData.laneQuestHeaderText);
        genericView.SetText("cooldownResultDescription", viewData.cooldownResultDescription);

        genericView.SetActive("cooldownQuestViewPanel", viewData.showCooldownQuestInfo);
        genericView.SetActive("questNamesRoot", !viewData.showCooldownQuestInfo);
        genericView.SetActive("normalQuestViewPanel", !viewData.showCooldownQuestInfo);
        genericView.GetComponent<LeanTweenTransform>("claimButton").enabled = viewData.isCompleted; 

        //var total = viewData.objectives.Length;
        //var completed = viewData.objectives.Count( o => o.

        //

        // skipCompleteButtonText
    }

    private void SetRewardsVisibility(bool isRewardsHidden)
    {
        genericView.SetActive("rewardsRoot", !isRewardsHidden);
        genericView.SetActive("rewardsLabel", !isRewardsHidden);
    }

    public void SetQuestNames(string[] names, int selectedQuestIndex)
    {
        genericView.SetActive("questNamesRoot");
//        namesList.UpdateItemsCount(names.Length);
        namesList.UpdateItemsCount(5);

        for (var i = 0; i < namesList.Count(); i++)
        {
            namesList[i].idx = i;

            if (i < names.Length)
            {
                namesList[i].panelState = (i == selectedQuestIndex) ? LaneQuestNameItem.PanelState.Selected : LaneQuestNameItem.PanelState.Active;
                namesList[i].questName = names[i];
            }
            else
            {
                namesList[i].panelState = LaneQuestNameItem.PanelState.Inactive;
            }
        }
    }

    public bool SetSelectedQuest(int index)
    {
        if (namesList[index].panelState != LaneQuestNameItem.PanelState.Inactive)
        {
            for (var i = 0; i < namesList.Count(); i++)
            {
                if (namesList[i].panelState != LaneQuestNameItem.PanelState.Inactive)
                {
                    namesList[i].panelState = (i == index) ? LaneQuestNameItem.PanelState.Selected : LaneQuestNameItem.PanelState.Active;
                }
            }

            return true;
        }

        return false;
    }

    public void SetQuestComplete(LaneQuestNameItem.QuestState[] states)
    {
        for (var i = 0; i < Mathf.Min(states.Length, namesList.Count()); i++)
        {
            namesList[i].questState = states[i];
            namesList[i].questNameColor = (states[i] == LaneQuestNameItem.QuestState.Claimed) ? claimedQuestNameColor : normalQuestNameColor;
        }
    }

    protected override void OnShow()
    {
        onOpen.Invoke();
        base.OnShow();
    }

    private void Update()
    {
        if (timer != null)
        {
            UpdateTimerPanel();
        }
    }

    private void UpdateTimerPanel()
    {
        var timeToFinish = new TimeSpan(0,0,0);
        if (timer.secondsToFinish <= 0)
        {
            timer = null;
        }
        else
        {
            timeToFinish = timer.timeSpan;
        }

        var skipCost = DynamicPrice.GetSkipCost((int)timeToFinish.TotalSeconds);
        genericView.SetActive("skipCompleteButtonText", skipCost > 0);
        genericView.SetActive("skipBtnCashIcon", skipCost > 0);
        genericView.SetText("skipCooldownCost", skipCost == 0 ? Loc.Get("idFree") : skipCost.ToString());
        genericView.SetText("laneQuestTimer", PrintHelper.GetTimeString(timeToFinish));
    }

    public void HideQuestNames()
    {
        genericView.SetActive("questNamesRoot", false);
    }
}