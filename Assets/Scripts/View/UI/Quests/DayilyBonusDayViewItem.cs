﻿using UnityEngine;
using System.Collections;
using Data;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace View.UI {
    public class DayilyBonusDayViewItem : MonoBehaviour {
        public enum DayType {
            PastDay,
            ToDay,
            NextDay
        }

        Text[] texts;

        GameObject pastDay;
        GameObject toDay;
        GameObject nextDay;

        public int      dayIdx  { set { SetDayIdx(value);   } }
        public DayType  type    { set { SetDayType(value);  } }

        void Awake()
        {
            pastDay = transform.Find("PastDay").gameObject;
            toDay = transform.Find("ToDay").gameObject;
            nextDay = transform.Find("NextDay").gameObject;

            texts = gameObject.GetComponentsInChildren<Text>(transform);
            Assert.IsTrue(texts.Length == 3);
        }

        void SetDayIdx(int idx)
        {
            foreach(var txt in texts)
            {
                txt.text = Loc.Get("dailyBonusDay")+" "+ idx.ToString();
            }
        }

        void SetDayType(DayType type)
        {
            pastDay.SetActive   (type == DayType.PastDay);
            toDay.SetActive     (type == DayType.ToDay);
            nextDay.SetActive   (type == DayType.NextDay);
        }

    }
}