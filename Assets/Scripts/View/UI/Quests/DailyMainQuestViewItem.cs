﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace View.UI
{
    public class DailyMainQuestViewItem : DailyQuestViewItem
    {
        Slider slider;


        public int iterations = 50;
        public float progress { set { SetProgress(value); } }

        new void Awake()
        {
            slider =  genericView.GetComponent<Slider>("progressSlider");
        }

        public override void SetState(DailyQuestViewItemState newState)
        {
            state = newState;
        }

        void SetProgress(float newValue)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(SetProgressAnimated(newValue));
            else
                slider.value = newValue;
            
        }

        IEnumerator SetProgressAnimated(float newValue)
        {
            var delta = newValue - slider.value;
            var sign = delta > 0 ? 1 : -1;

            // iterate while sign not changed
            while ((newValue - slider.value > 0 ? 1 : -1) == sign)
            {
                slider.value += delta / 50.0f;
                yield return new WaitForEndOfFrame();
            }

            slider.value = newValue;
        }
    }
}