﻿using Logic.Facades;
using View.UI;
using Data;
using View.UI.Base;
using Data.Quests.SportQuest;
using Logic.Quests.SportQuest;

public class LaneQuestObjectiveItem : UIBaseViewListItem
{
    public void SetObjective(
        BaseSportQuestInfoViewData<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
            viewData,
        int objectiveIndex)
    {
        genericView.SetActive("completedIcon", viewData.isQuestClaimed);
        genericView.SetSprite("objectiveIcon", viewData.objectives[objectiveIndex].icon);
        genericView.SetText("objectiveCount", viewData.objectives[objectiveIndex].countText);
        genericView.SetActive("objectiveCount", !viewData.isQuestClaimed);
    }

    public void SetObjective(
        BaseSportQuestInfoViewData<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
            viewData,
        int objectiveIndex)
    {
        genericView.SetActive("completedIcon", viewData.isQuestClaimed);
        genericView.SetSprite("objectiveIcon", viewData.objectives[objectiveIndex].icon);
        genericView.SetText("objectiveCount", viewData.objectives[objectiveIndex].countText);
        genericView.SetActive("objectiveCount", viewData.isObjectivesCountShowing);
    }
}