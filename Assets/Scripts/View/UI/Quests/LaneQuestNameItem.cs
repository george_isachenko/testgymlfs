﻿using System;
using UnityEngine;
using UnityEngine.UI;
using View.UI;
using View.UI.Base;

public class LaneQuestNameItem : UIBaseViewListItem
{
    #region Types.
    public enum PanelState
    {
        Inactive,
        Active,
        Selected
    }

    public enum QuestState
    {
        New,
        Completed,
        Claimed
    }
    #endregion

    #region Public properties.
    public PanelState panelState
    {
        get
        {
            return panelState_;
        }

        set
        {
            panelState_ = value;

            UpdatePanelState();
            UpdateQuestState();
        }
    }

    public QuestState questState
    {
        get
        {
            return questState_;
        }

        set
        {
            questState_ = value;

            UpdatePanelState();
            UpdateQuestState();
        }
    }

    public Color questNameColor
    {
        set { genericView.GetComponent<Text>("questName").color = value; }
    }

    public string questName
    {
        set { genericView.SetText("questName", value); }
    }
    #endregion
    
    #region Private fields.
    private PanelState panelState_ = PanelState.Inactive;
    private QuestState questState_ = QuestState.New;
    #endregion

    #region Private methods.
    private void UpdatePanelState()
    {
        genericView.SetActive("inactiveBack",   panelState_ == PanelState.Inactive);
        genericView.SetActive("normalBack",     panelState_ == PanelState.Active);
        genericView.SetActive("selectedBack",   panelState_ == PanelState.Selected);
        genericView.SetActive("questName",      panelState_ != PanelState.Inactive);
    }

    private void UpdateQuestState ()
    {
        genericView.SetActive("newIcon",        panelState_ != PanelState.Inactive && questState_ == QuestState.New);
        genericView.SetActive("completedIcon",  panelState_ != PanelState.Inactive && questState_ == QuestState.Completed);
        genericView.SetActive("claimedIcon",    panelState_ != PanelState.Inactive && questState_ == QuestState.Claimed);
    }
    #endregion
}