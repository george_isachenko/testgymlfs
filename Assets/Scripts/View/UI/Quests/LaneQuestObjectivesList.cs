﻿using System.Linq;
using Logic.Facades;
using View.UI;
using Random = UnityEngine.Random;
using Data;
using View.UI.Base;
using Data.Quests.SportQuest;
using Logic.Quests.SportQuest;

internal class LaneQuestObjectivesList : UIViewSubList<LaneQuestObjectiveItem>
{
    public void SetObjectives(BaseSportQuestInfoViewData<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData) 
    {
        UpdateItemsCount(viewData.objectivesCount);
        for (var i = 0; i < viewData.objectivesCount; i++)
        {
            this[i].SetObjective(viewData, i);
        }
    }

    public void SetObjectives(BaseSportQuestInfoViewData<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData)
    {
        UpdateItemsCount(viewData.objectivesCount);
        for (var i = 0; i < viewData.objectivesCount; i++)
        {
            this[i].SetObjective(viewData, i);
        }
    }

    protected override LaneQuestObjectiveItem CreateNewItem()
    {
        var item = base.CreateNewItem();
        item.GetComponent<UIGenericView>().alias = string.Concat(
            Enumerable.Repeat(0, 30).Select(i => Random.Range(0, 99999).ToString()[0]));
        return item;
    }
}