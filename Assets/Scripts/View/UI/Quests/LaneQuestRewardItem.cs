﻿using Logic.Facades;
using View.UI;
using Data;
using View.UI.Base;
using Data.Quests.SportQuest;
using Logic.Quests.SportQuest;

public class LaneQuestRewardItem : UIBaseViewListItem
{
    public void SetReward(BaseSportQuestInfoViewData<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData, int rewardIndex)
    {
        genericView.SetSprite("rewardIcon", viewData.rewards[rewardIndex].icon);
        genericView.SetText("rewardCount", viewData.rewards[rewardIndex].countTxt);
        genericView.SetActive("completedIcon",false);
    }

    public void SetReward(BaseSportQuestInfoViewData<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData, int rewardIndex)
    {
        genericView.SetSprite("rewardIcon", viewData.rewards[rewardIndex].icon);
        genericView.SetText("rewardCount", viewData.rewards[rewardIndex].countTxt);
        genericView.SetActive("completedIcon", false);
    }

    public void SetReward(SportQuestRewardViewData viewData)
    {
        genericView.SetSprite("rewardIcon", viewData.icon);
        genericView.SetText("rewardCount", viewData.countTxt);
        genericView.SetActive("completedIcon", false);
    }
}