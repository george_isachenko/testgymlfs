using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;

namespace View.UI.Quests
{
    public class DailyQusetBackpackAnimationListener : MonoBehaviour
    {
        public UnityEvent backPackOpenedEvent = new UnityEvent();
        public void OnBackpackOpenedEvent()
        {
            backPackOpenedEvent.Invoke();
        }
    }
}