﻿using System.Linq;
using Data;
using Data.Quests.SportQuest;
using Logic.Facades;
using Logic.Quests.SportQuest;
using UnityEngine;
using View.UI.Base;

internal class LaneQuestRewardsList : UIViewSubList<LaneQuestRewardItem>
{
    public void SetReward(BaseSportQuestInfoViewData<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData)
    {
        UpdateItemsCount(viewData.rewardsCount);
        for (var i = 0; i < viewData.rewardsCount; i++)
        {
            this[i].SetReward(viewData, i);
        }
    }

    public void SetReward(BaseSportQuestInfoViewData<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData> viewData)
    {
        UpdateItemsCount(viewData.rewardsCount);
        for (var i = 0; i < viewData.rewardsCount; i++)
        {
            this[i].SetReward(viewData, i);
        }
    }

    protected override LaneQuestRewardItem CreateNewItem()
    {
        var item = base.CreateNewItem();
        item.GetComponent<UIGenericView>().alias = string.Concat(
            Enumerable.Repeat(0, 30).Select(i => Random.Range(0, 99999).ToString()[0]));
        return item;
    }
}