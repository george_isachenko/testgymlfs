﻿using System;
using UnityEngine;
using View.UI.Base;

namespace View.UI
{
    public class DailyQuestViewItem : UIBaseViewListItem
    {   
        public enum DailyQuestViewItemState
        {
            Normal,
            New,
            Waiting,
            Completed
        }
        
        public string title
        {
            set { genericView.SetText("questTitle",value); }
        }

        public string localizationKey
        {
            set { genericView.GetComponent<LocalizationHelper>("questTitle").localizationKeyString = value; }
        }

        public DailyQuestViewItemState state { get; protected set; }

        [HideInInspector]
        public override bool selected
        {
            set { genericView.SetActive("selectedBack",value); }
            get { return genericView.GetTransform("selectedBack").gameObject.activeSelf; }
        }

        public virtual void SetState(DailyQuestViewItemState newState)
        {
            genericView.SetActiveChildren("questStatusesRoot", false);
            switch (newState)
            {
                case DailyQuestViewItemState.Normal:
                    genericView.SetActive("normalQuestStateIcon");
                    break;
                case DailyQuestViewItemState.New:
                    genericView.SetActive("newQuestStateIcon");
                    break;
                case DailyQuestViewItemState.Waiting:
                    genericView.SetActive("waitingQuestStateIcon");
                    break;
                case DailyQuestViewItemState.Completed:
                    genericView.SetActive("completedQuestStateIcon");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }
            state = newState;
        }
    }
}