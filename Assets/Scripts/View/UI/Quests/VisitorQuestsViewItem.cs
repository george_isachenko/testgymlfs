﻿using System;
using Data;
using Logic.Quests;
using Logic.Quests.Base;
using UnityEngine;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/UI/Visitor Quests View Item")]
    public class VisitorQuestsViewItem : UIBaseViewListItem
    {
        #region Types.
        public enum ItemType
        {
            Normal,
            Time,
            NextVisitor
        }
        #endregion

        #region Public fields.
        public Color questObjectiveEnabledColor;
        public Color questObjectiveEmptyColor;
        public Color colorTrainerClient = Color.cyan;
        #endregion

        #region Public properties.
        public ItemType itemType        { get; private set; }

        public string visitorName       { set { txtNormalVisitorNameText = value; } }
        public string time              { set { txtTimeTimerText = value; } }
        public int skipPrice            { set { SetSkipPrice(value); } }
        public bool   isTrainerClient   { set { GetComponent<Image>().color = value ? colorTrainerClient : Color.white; } }

        [HideInInspector]
        public override bool selected { set { genericView.SetActive("NormalSelected", value); } get { return genericView.GetActive("NormalSelected"); } }
        #endregion

        #region Public events.
        public event Action skipHandler
        {
            add { genericView.SetButtonCallback("TimeSkipButton", value); }
            remove { genericView.ResetButtonCallback("TimeSkipButton"); }
        }
        #endregion

        #region Protected properties.
        protected bool activeNormalState                    { set { genericView.SetActive("NormalState", value); } }
        protected string txtNormalVisitorNameText           { set { genericView.SetText("NormalVisitorNameText", value); } }
        protected QuestRewardView viewNormalReward          { get { return genericView.GetComponent<QuestRewardView>("NormalReward"); } }

        protected bool activeTimeState                      { set { genericView.SetActive("TimeState", value); } }
        protected string txtTimeTimerText                   { set { genericView.SetText("TimeTimerText", value); } }
        protected bool activeTimeSkipButtonIcon             { set { genericView.SetActive("TimeSkipButtonIcon", value); } }
        protected string txtTimeSkipButtonAmountText        { set { genericView.SetText("TimeSkipButtonAmountText", value); } }

        protected bool activeNextVisitorState               { set { genericView.SetActive("NextVisitorState", value); } }
        protected string txtNextVisitorStyleText            { set { genericView.SetText("NextVisitorStyleText", value); } }
        #endregion

        #region Unity API.
        protected new void Awake ()
        {
            base.Awake();
        }
        #endregion

        #region Public API.
        public void SetNormalMode (int objectivesCount, int objectivesCompleted, QuestEstimatedRewards rewards)
        {
            SetMainState(ItemType.Normal);
            SetObjectivesCount(objectivesCount);
            SetObjectivesCompleted(objectivesCompleted);
            viewNormalReward.SetRewards (rewards);
        }


        public void SetTimerMode ()
        {
            SetMainState(ItemType.Time);
        }

        public void SetNextVisitorMode (int nextVisitorStyle)
        {
            SetMainState(ItemType.NextVisitor);
            txtNextVisitorStyleText = nextVisitorStyle.ToString();
        }
        #endregion

        #region Private methods.
        private void SetObjectivesCompleted (int value)
        {
            for (int i = 0; i < 3; i++)
            {
                genericView.SetActive(string.Format("NormalTask{0}Icon", i + 1), (i < value));
            }
        }

        private void SetObjectivesCount (int value)
        {
            for (int i = 0; i < 3; i++)
            {
                genericView.GetComponent<Image>(string.Format("NormalTask{0}BgIcon", i + 1)).color = ((i < value) ? questObjectiveEnabledColor : questObjectiveEmptyColor);
            }
        }

        private void SetSkipPrice (int value)
        {
            if (value == 0)
            {
                txtTimeSkipButtonAmountText = Loc.Get("idFree");
                activeTimeSkipButtonIcon = false;
            }
            else
            {
                txtTimeSkipButtonAmountText = value.ToString();
                activeTimeSkipButtonIcon = true;
            }
        }

        void SetMainState (ItemType itemType)
        {
            this.itemType = itemType;

            activeNormalState = (itemType == ItemType.Normal);
            activeTimeState = (itemType == ItemType.Time);
            activeNextVisitorState = (itemType == ItemType.NextVisitor);
        }
        #endregion
    }
}