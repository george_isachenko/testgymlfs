using System;
using System.Linq;
using Core.Timer;
using Data;
using Data.Person;
using Data.Quests.SportQuest;
using Logic.Facades;
using Logic.Quests.SportQuest;
using Logic.Quests.SportQuest.SportLaneQuest;
using UnityEngine;
using View.UI;

public abstract class BaseSportQuestInfoViewData<TQuestData, TObjectiveLogic, TObjectiveData>
    where TQuestData : SportQuestData
    where TObjectiveLogic : SportObjectiveLogic<TObjectiveData>
    where TObjectiveData : SportObjectiveData
{
    public string questName;
    public string buyButtonText;
    public bool isHelpButtonEnabled;
    public bool isClaimButtonEnabled;
    //public bool isBuyCompleteButtonEnabled;
    public int objectivesCount;
    public bool isBuyButtonInteractable = true;
    public bool isObjectivesCountShowing = true;
    public bool isCompleted = false;

    public SportQuestObjectiveViewData[] objectives;
    public bool isQuestClaimed;

    public int rewardsCount;
    public SportQuestRewardViewData[] rewards;

    public TimerFloat currentStateTimer;

    protected BaseSportQuestInfoViewData(
        SportQuest<TQuestData,TObjectiveLogic,TObjectiveData> quest,
        LogicSportsmen sportsmanManager)
    {
        // main
        var needSporstmansCount =
            quest.objectives != null && quest.objectives.Count > 0
                ? quest.objectives.Sum(
                    o =>
                    {
                        var goal = o.iterationsGoal;
                        var current = sportsmanManager.GetSportsmanCount(o.sportsmanType,
                            null,
                            PersonState.Idle,
                            // PersonState.InQueue,
                            PersonState.Entering,
                            PersonState.ExerciseComplete);
                        return Mathf.Max(0, goal - current);
                    })
                : 0;
        isCompleted = needSporstmansCount <= 0;
		questName = quest.claimed ? Loc.Get("laneQuestParticipated") : Loc.Get("laneQuestRequiredPros");
//        buyButtonText = $"{needSporstmansCount*10}$ complete";
        buyButtonText = "CLAIM";
        isHelpButtonEnabled = !isCompleted && !quest.claimed;
        isClaimButtonEnabled = !(quest.claimed);
//        isBuyCompleteButtonEnabled = quest.claimed;

        // objectives
        objectivesCount = quest.objectivesCount;
        isQuestClaimed = quest.claimed;
        objectives = new SportQuestObjectiveViewData[objectivesCount];
        for (var i = 0; i < objectivesCount; i++)
        {
            var objective = quest.objectives[i];
            var currentCount = sportsmanManager.GetSportsmanCount(objective.sportsmanType, null, PersonState.Idle,
                PersonState.Entering, PersonState.InQueue, PersonState.ExerciseComplete);
            var goalCount = objective.iterationsGoal;
            objectives[i] = new SportQuestObjectiveViewData()
            {
                icon = GUICollections.instance.sportsmenIcons.items[(int) objective.sportsmanType].sprite,
                countText =
                    String.Format("<color={0}>{1}/{2}</color>",
						currentCount >= goalCount || quest.claimed ? "#A7E500FF" : "#ff8c8dFF",
                        quest.claimed ? goalCount : currentCount,
                        goalCount)
            };
        }

        // rewards
        if (quest.rewards != null)
        {
            rewardsCount = quest.rewards.Length;
            rewards = new SportQuestRewardViewData[rewardsCount];
            for (var i = 0; i < rewardsCount; i++)
            {
                rewards[i] = new SportQuestRewardViewData();
                switch (quest.rewards[i].rewardType)
                {
                    case SportQuestRewardType.Coin:
                        rewards[i].icon = Cost.GetSpriteCoins();
                        break;
                    case SportQuestRewardType.Resource:
                        rewards[i].icon = Cost.GetSprite(quest.rewards[i].itemId); // GUICollections.instance.storageIcons.items[(int) quest.rewards[i].itemId].sprite;
                        break;
                    case SportQuestRewardType.Experience:
                        rewards[i].icon = Cost.GetSpriteExp();
                        break;
                    case SportQuestRewardType.FitPoints:
                        rewards[i].icon = GUICollections.instance.commonIcons.array[5];
                        break;
                    case SportQuestRewardType.Fitbucks:
                        rewards[i].icon = Cost.GetSpriteBucks();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                rewards[i].countTxt = quest.rewards[i].count.ToString();
            }
        }
    }
}

public class SportLaneQuestInfoViewData :
    BaseSportQuestInfoViewData<SportLaneQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
{
    public string bubbleText;
    public string laneQuestHeaderText;
    public bool isRewardsHidden;
    public bool isCompletedLabelShown;
    public string cooldownResultDescription;
    public bool showCooldownQuestInfo;

    public SportLaneQuestInfoViewData(SingleSportLaneQuest quest, LogicSportsmen sportsmanManager, LaneQuestLogic laneQuest)
        : base(quest, sportsmanManager)
    {
        currentStateTimer = laneQuest.currentStateTimer;

        switch (laneQuest.state)
        {
            case LaneQuestState.Tutorial:
                laneQuestHeaderText = Loc.Get("laneQuestApplyForChamp");
                showCooldownQuestInfo = false;
                isRewardsHidden = false;
                isObjectivesCountShowing = true;
                break;
            case LaneQuestState.ProgressAccumulation:
                laneQuestHeaderText = Loc.Get("laneQuestApplyForChamp");
                showCooldownQuestInfo = false;
                isRewardsHidden = false;
                isObjectivesCountShowing = true;
                break;
            case LaneQuestState.WaitForNewQuests:
                laneQuestHeaderText = Loc.Get("laneQuestEventsEnded");
                cooldownResultDescription = laneQuest.isLastProgressAccumulationFailed
                    ? Loc.Get("laneQuestEventsEnded")
                    : Loc.Get("laneQuestSuccessHeader");
                questName = "Next objectives";
                showCooldownQuestInfo = true;
                isRewardsHidden = true;
//                isBuyCompleteButtonEnabled = false;
                isClaimButtonEnabled = false;
                isObjectivesCountShowing = false;
                break;
        }

        isCompletedLabelShown = quest.claimed;
    }
}

public class SportTimeQuestInfoViewData : BaseSportQuestInfoViewData<SportTimeQuestData, SportObjectiveLogic<SportObjectiveData>, SportObjectiveData>
{
    public bool isSkipWaitingEnable;
    public TimerFloat newQuestTimer;

    public SportTimeQuestInfoViewData(SingleSportTimeQuest quest, LogicSportsmen sportsmanManager) : base(quest, sportsmanManager)
    {
        isSkipWaitingEnable = quest.claimed;
        newQuestTimer = quest.newQuestTimer;
    }
}

public class SportQuestObjectiveViewData
{
    public Sprite icon;
    public string countText;
}

public class SportQuestRewardViewData
{
    public Sprite icon;
    public string countTxt;
}