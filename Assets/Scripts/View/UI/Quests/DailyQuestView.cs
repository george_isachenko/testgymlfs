﻿using System.Collections.Generic;
using System.Linq;
using Core.Timer;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;
using View.UI.Base;
using View.UI.Quests;
using View.Actions;

namespace View.UI
{
    public delegate void OnClaimClick_Delegate(DailyQuestViewItem item);
    public delegate void OnLiveUpdate_Delegate();

    public class DailyQuestView : UIBaseView
    {
        [HideInInspector] public OnClaimClick_Delegate onClaimClickCallback;
        [HideInInspector] public OnClaimClick_Delegate onSkipClickCallback;
        [HideInInspector] public OnLiveUpdate_Delegate onLiveUpdateCallback;

        public bool canClaim    { set { SetCanClaim(value); } }
        public bool claimed     { set { SetClaimed(value); } }
        
        public UnityEvent   resourcesJumpAnimationStart     { get { return presentView.resourcesJumpAnimationStart; } }
        public UnityEvent   resourcesJumpAnimationFinish    { get { return presentView.resourcesJumpAnimationFinish; } }

        public DailyQuestNamesList          questsNamesList { get; private set; }
        private DailyQuestRewardsList       questRewardsList;
        private DailyQuestMainPresentView   presentView;
        private Transform                   presentParentDefault;

        void Awake()
        {
            questsNamesList = UIViewSubListFactory<DailyQuestNamesList, DailyQuestViewItem>.CreateUiList
                (genericView.GetTransform("questNamesRoot"),
                    "UI/ListViewItems/DailyQuestItem");
            questsNamesList.AddSeparatedItems(new[] {genericView.GetComponent<DailyQuestViewItem>("mainQuestItem")});

            questRewardsList = UIViewSubListFactory<DailyQuestRewardsList, LaneQuestRewardItem>.CreateUiList
                (genericView.GetTransform("rewardsRoot"),
                    "UI/ListViewItems/QuestRewardItem");

            presentView = genericView.GetComponent<DailyQuestMainPresentView>("presentView");
            presentView.onPresentClicked.AddListener(() => onClaimClickCallback?.Invoke(questsNamesList.separatedItems[0]));
            
            genericView.SetButtonCallback("BtnClaim",OnBtnCliamClick);
            genericView.SetButtonCallback("btnClose", HideByUserClick);
            genericView.SetButtonCallback("buySkipButton", OnBtnSkipClick);

            presentParentDefault = presentView.transform.parent;
        }

        public void ActivateDelayedSkipShowing()
        {
            var anim = genericView.GetComponent<Animator>("buySkipButton");
            if (!anim.isInitialized)
                return;
            anim.SetTrigger("animateShowing");
        }

        public void SetForceShowSkipAnimationState(bool isShown)
        {
            var anim = genericView.GetComponent<Animator>("buySkipButton");
            if (!anim.isInitialized)
                return;
            anim.SetBool("forceShow",isShown);
        }

        private void OnBtnSkipClick()
        {
            foreach (var item in questsNamesList.items)
            {
                if (item.selected)
                {
                    onSkipClickCallback(item);
                    return;
                }
            }
        }

        private void OnBtnCliamClick()
        {
            foreach (var item in questsNamesList.items)
            {
                if (item.selected)
                {
                    onClaimClickCallback(item);
                    return;
                }
            }
        }

        void Update()
        {
            if (onLiveUpdateCallback != null)
                onLiveUpdateCallback.Invoke();
        }

        void SetCanClaim(bool value)
        {
            genericView.GetComponent<Button>("BtnClaim").interactable = value;
            genericView.GetComponent<LeanTweenTransform>("BtnClaim").enabled = value;
        }

        void SetClaimed(bool value)
        {
            genericView.SetActive("costViewSubList", value);
            questRewardsList.gameObject.SetActive(!value);
            genericView.SetActive("BtnClaim",!value);
            genericView.SetActive("buySkipButton", value);
        }

        public void SetRewards(List<SportQuestRewardViewData> rewards)
        {
            questRewardsList.UpdateItemsCount(rewards.Count);
            for (int i = 0; i < rewards.Count; i++)
            {
                questRewardsList[i].SetReward(rewards[i]);
            }
        }

        public void SetTextDescription(string description)
        {
            // Debug.LogFormat("DailyQuestView: update quest description to '{0}'",description);
            genericView.SetText("questDescription",description);
        }

        public void SetTimer(TimerFloat timer)
        {
            genericView.GetComponent<CostViewSubList>("costViewSubList")
                .mediator.SetView(timer);
        }

        public void SetSkipCost(int skipCost)
        {
            genericView.SetActive("btnSkipFitBucksIcon", skipCost>0);
            genericView.SetActive("btnSkipDesc", skipCost>0);
            genericView.SetText("buySkipButtonCostText", skipCost > 0 ? skipCost.ToString() : "Free");
        }

        public void SetMainQuestProgress(float progress)
        {
            Assert.IsTrue(questsNamesList.separatedItems.Count>0);
            var mainItem = questsNamesList.separatedItems[0] as DailyMainQuestViewItem;
            Assert.IsNotNull(mainItem);
            mainItem.progress = progress;
        }

        public void SetPresentIdleState(bool isActive)
        {
            presentView.SetIdleState(isActive);
        }

        public void InitBackPackAnimation(Sprite res1Sprite, Sprite res2Sprite, Transform resourcesJumpFinishPoint)
        {
            presentView.StartBackpackOpening(res1Sprite, res2Sprite, resourcesJumpFinishPoint);
        }

        public void SetPresentTapTrigger()
        {
            presentView.SetPresentTapTrigger();
        }

        public void SetPresentParent(Transform parent)
        {
            presentView.transform.SetParent(parent);
        }

        public void SetPresentDefaultParent()
        {
            SetPresentParent(presentParentDefault);
        }
    }

    public class DailyQuestRewardsList : UIViewSubList<LaneQuestRewardItem>
    {
    }

    public class DailyQuestNamesList : UIViewSeparatedSubList<DailyQuestViewItem>
    {
    }

    public class UIViewSeparatedSubList<T> : UIViewSubList<T>
        where T : UIBaseViewListItem
    {
        public List<T> separatedItems { get; } = new List<T>();

        public new List<T> items = new List<T>();

        public override void UpdateItemsCount(int count)
        {
            base.UpdateItemsCount(count - separatedItems.Count);
            UpdateItemsList();
        }

        private void UpdateItemsList()
        {
            items = separatedItems.Concat(base.items).ToList();
        }

        public void AddSeparatedItems(IEnumerable<T> myItems)
        {
            foreach (var item in myItems)
            {
                item.OnClickCallback += OnItemClick;
                separatedItems.Add(item);
            }
            UpdateItemsList();
        }

        public void RemoveAllSeparatedItems()
        {
            separatedItems.Clear();
            UpdateItemsList();
        }

        protected override void SetSelected(UIBaseViewListItem item)
        {
            foreach (var it in separatedItems)
            {
                it.selected = it == item;
            }
            base.SetSelected(item);
        }

        public override T this[int i]
        {
            get { return items[i]; }
        }
    }
}