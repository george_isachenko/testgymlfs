using System;
using Core.Timer;
using View.UI;
using View.UI.Base;

public class SportTimeQuestView : UIBaseView
{
    // public event Action onBuyCompleteButtonPressed;
    public event Action onClaimButtonPressed;
    public event Action onHelpButtonPressed;
    public event Action onTimerFinished;
    public event Action onSkipWaitingForNewQuest;

    private TimerFloat waitForNewQuestTimer;
    private LaneQuestObjectivesList objectivesList;
    private LaneQuestRewardsList rewardsList;
    
    private void Awake()
    {
        objectivesList =
            UIViewSubListFactory<LaneQuestObjectivesList, LaneQuestObjectiveItem>.CreateUiList(
                genericView.GetTransform("objectivesRoot"), "UI/ListViewItems/QuestObjectiveItem");

        rewardsList =
            UIViewSubListFactory<LaneQuestRewardsList, LaneQuestRewardItem>.CreateUiList(
                genericView.GetTransform("rewardsRoot"), "UI/ListViewItems/QuestRewardItem");

        genericView.SetButtonCallback("btnClose", () =>
        {
            if (OnClosedByUserClick_Callback != null) OnClosedByUserClick_Callback();
            Hide();
        });

        genericView.SetButtonCallback("helpButton", () =>
        {
            if (onHelpButtonPressed != null)
                onHelpButtonPressed();
        });
        genericView.SetButtonCallback("claimButton", () =>
        {
            if (onClaimButtonPressed != null)
                onClaimButtonPressed();
        });
/*        genericView.SetButtonCallback("buyCompleteButton", () =>
        {
            if (onByCompleteButtonPressed != null)
                onByCompleteButtonPressed();
        });*/
        genericView.SetButtonCallback("skipWaiting", () =>
        {
            if (onSkipWaitingForNewQuest != null)
                onSkipWaitingForNewQuest();
        });
    }

    public void SetQuestInfo(SportTimeQuestInfoViewData viewData)
    {
        // buttons
        genericView.SetText("buyCompleteButtonText", viewData.buyButtonText);
        genericView.SetActive("helpButton", viewData.isHelpButtonEnabled);
        genericView.SetActive("claimButton", viewData.isClaimButtonEnabled);
//        genericView.SetActive("buyCompleteButton", viewData.isBuyCompleteButtonEnabled);
        genericView.SetActive("skipWaiting", viewData.isSkipWaitingEnable);

        if (viewData.newQuestTimer != null)
        {
            waitForNewQuestTimer = viewData.newQuestTimer;
            objectivesList.UpdateItemsCount(0);
            rewardsList.UpdateItemsCount(0);
        }
        else
        {
            genericView.SetText("manQuote", "To be the Best\nYou Have to be Fast");
            genericView.SetText("timeText", "1d");
            objectivesList.SetObjectives(viewData);
            rewardsList.SetReward(viewData);
        }
    }

    private void Update()
    {
        if (waitForNewQuestTimer != null)
        {
            if (waitForNewQuestTimer.secondsToFinish <= 0)
            {
                waitForNewQuestTimer = null;
                genericView.SetText("manQuote", "To be the Best\nYou Have to be Fast");
                genericView.SetText("timeText", "1d");
                if (onTimerFinished != null) onTimerFinished();
            }
            else
            {
                var timeTxt = View.PrintHelper.GetTimeString(waitForNewQuestTimer.timeSpan);
                genericView.SetText("timeText", timeTxt);
                genericView.SetText("manQuote", "Time to train!");
            }
        }
    }
}