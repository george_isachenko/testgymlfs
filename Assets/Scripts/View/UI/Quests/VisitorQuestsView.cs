﻿using UnityEngine;
using UnityEngine.UI;
using System;
using View.UI.Base;

namespace View.UI
{
    public class VisitorQuestsView : UIBaseViewList<VisitorQuestsViewItem>  
    {
        #region Public fields.
        public Color enabledColor;
        public Color emptyColor;
        #endregion

        #region Public properties.
        public string txtVisitorCount                         { set { genericView.SetText("TxtVisitorCount", value); } }
        #endregion

        [HideInInspector]
        public OnLiveUpdate_Delegate OnLiveUpdate_Callback;

        void Awake()
        {
            itemsRoot = DefaultItemsRoot();

            BindCloseButton();
        }
       
        void LateUpdate()
        {
            if (OnLiveUpdate_Callback != null)
                OnLiveUpdate_Callback.Invoke();
        }
    }
}
