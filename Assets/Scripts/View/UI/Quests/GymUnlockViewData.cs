using System;
using System.Linq;
using Core.Timer;
using Data;
using UnityEngine;
using View.UI;
using Presentation.Providers;

public class GymUnlockViewData
{
    public string roomName;
    public string title;
    public string messageText;
    public string priceText;
    public bool priceTextActive;
    public bool buttonBucksIconActive;
    public bool unlockTextActive;
    public GymUnlockViewDataType showType;
    public Cost unlockCost;
    public IPlayerProfileInteractionProvider playerProfile;
    public TimerFloat timerViewData;

    public class GymUnlockResourceViewData
    {
        public Sprite sprite;
        public bool isGreen;
        public int value;

        public GymUnlockResourceViewData(Sprite sprite, int value, bool isGreen)
        {
            this.sprite = sprite;
            this.value = value;
            this.isGreen = isGreen;
        }
    }

    public GymUnlockViewData(string roomName, string title, string message, Cost unlockCost, IPlayerProfileInteractionProvider profile)
    {
        this.roomName = roomName;
        this.title = title;
        messageText = message;
        showType = GymUnlockViewDataType.Resources;
        playerProfile = profile;
        this.unlockCost = unlockCost;

        var canUnlock = profile.CanSpend(unlockCost);
        var unlockBucksPrice = 0;
        switch (unlockCost.type)
        {
            case Cost.CostType.None:
                break;
            case Cost.CostType.CoinsOnly:
                if (!canUnlock) unlockBucksPrice += Cost.CoinsToFitBucks(unlockCost.value);
                break;
            case Cost.CostType.BucksOnly:
                if (!canUnlock) unlockBucksPrice += unlockCost.value;
                break;
            case Cost.CostType.ResourcesOnly:
                unlockBucksPrice += (from resource in unlockCost.resources
                    let needMoreCount = Mathf.Max(0, resource.Value - profile.storage.GetResourcesCount(resource.Key))
                    where needMoreCount != 0
                    select Cost.RecourcePriceInFitbucks(resource.Key)*needMoreCount).Sum();
                break;
            case Cost.CostType.ResourcesAndCoins:
                unlockBucksPrice += (from resource in unlockCost.resources
                                     let needMoreCount = Mathf.Max(0, resource.Value - profile.storage.GetResourcesCount(resource.Key))
                                     where needMoreCount != 0
                                     select Cost.RecourcePriceInFitbucks(resource.Key) * needMoreCount).Sum();
                if (profile.coins < unlockCost.value)
                    unlockBucksPrice += Cost.CoinsToFitBucks(unlockCost.value - profile.coins);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        priceText = unlockBucksPrice.ToString();
        priceTextActive = !canUnlock;
        buttonBucksIconActive = !canUnlock;
        unlockTextActive = true;
    }

    public GymUnlockViewData(string roomName, string title, string message, TimerFloat expandTimer)
    {
        this.roomName = roomName;
        this.title = title;
        messageText = message;
        showType = GymUnlockViewDataType.Timer;
        timerViewData = expandTimer;
    }

    public enum GymUnlockViewDataType
    {
        Timer,
        Resources
    }
}