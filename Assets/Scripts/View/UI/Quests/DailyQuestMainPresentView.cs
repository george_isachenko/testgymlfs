using System.Collections;
using Logic.Quests;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI.Quests
{
    public class DailyQuestMainPresentView : UIGenericMonoBehaviour
    {
        private const string AnimatorIdleStateName = "isIdleActive";
        private const string AnimatorOpenStateName = "isAtOpenState";
        private const string AnimatorTapTriggerName = "tap";

        public UnityEvent onPresentClicked = new UnityEvent();
        public UnityEvent resourcesJumpAnimationStart = new UnityEvent();
        public UnityEvent resourcesJumpAnimationFinish = new UnityEvent();

        public float resourcesJumpAnimationDuration = 1f;
        public AnimationCurve resourcesJumpScaleCurve = new AnimationCurve();
        public AnimationCurve resourcesJumpPositionXCurve = new AnimationCurve();
        public AnimationCurve resourcesJumpPositionYCurve = new AnimationCurve();

        private Animator presentAnimator;
        private Sprite res1Sprite;
        private Sprite res2Sprite;
        private Transform resourcesJumpFinishPoint;

        void Awake()
        {
            presentAnimator = genericView.GetComponent<Animator>("presentAnimator");
            genericView.SetButtonCallback("presentButton", () => onPresentClicked.Invoke());
            genericView.GetComponent<DailyQusetBackpackAnimationListener>("presentAnimator")
                .backPackOpenedEvent.AddListener(OnBackpackOpenedEvent);

            var entry = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerDown,
                callback = new EventTrigger.TriggerEvent()
            };
            UnityAction<BaseEventData> call = a => SetPresentTapTrigger();
            entry.callback.AddListener(call);
            genericView.GetComponent<EventTrigger>("presentButton").triggers.Add(entry);
        }

        public void OnEnable()
        {
            var resource1 = genericView.GetTransform("resource1Icon");
            resource1.gameObject.SetActive(false);
            var resource2 = genericView.GetTransform("resource2Icon");
            resource2.gameObject.SetActive(false);
        }

        public void SetIdleState(bool isActive)
        {
            if (!presentAnimator.isInitialized)
                return;

            Debug.LogFormat("Daily quest: set present idle state to {0}", isActive ? "active" : "inactive");
            presentAnimator.SetBool(AnimatorIdleStateName, isActive);
        }

        public void StartBackpackOpening(Sprite res1Sprite, Sprite res2Sprite, Transform resourcesJumpFinishPoint)
        {
            SetIdleState(true);
            Debug.Log("Daily quest: start backpack animation");
            this.res1Sprite = res1Sprite;
            this.res2Sprite = res2Sprite;
            this.resourcesJumpFinishPoint = resourcesJumpFinishPoint;
            presentAnimator.SetBool(AnimatorOpenStateName, true);
        }

        private void OnBackpackOpenedEvent()
        {
            StartCoroutine(ResourcesJumpAnimation());
        }

        private IEnumerator ResourcesJumpAnimation()
        {
            resourcesJumpAnimationStart.Invoke();

            var startPointRes1 = genericView.GetTransform("resource1JumpStart").position;
            var startPointRes2 = genericView.GetTransform("resource2JumpStart").position;

            var resource1 = genericView.GetTransform("resource1Icon");
            resource1.GetComponent<Image>().sprite = res1Sprite;
            resource1.gameObject.SetActive(true);
            var res1StartZ = resource1.position.z;
            var resource2 = genericView.GetTransform("resource2Icon");
            resource2.GetComponent<Image>().sprite = res2Sprite;
            resource2.gameObject.SetActive(true);
            var res2StartZ = resource2.position.z;

            var duration = 0f;
            var startTime = Time.time;
            for (; duration < resourcesJumpAnimationDuration; duration = Time.time - startTime)
            {
                var normalizedDuration = (duration == 0 ? 0.001f : duration) / resourcesJumpAnimationDuration;
                var scale = resourcesJumpScaleCurve.Evaluate(normalizedDuration);
                var pos1 = new Vector3(
                        UnclampedLerp(startPointRes1.x, resourcesJumpFinishPoint.position.x, resourcesJumpPositionXCurve.Evaluate(normalizedDuration)),
                        UnclampedLerp(startPointRes1.y, resourcesJumpFinishPoint.position.y, resourcesJumpPositionYCurve.Evaluate(normalizedDuration)),
                        res1StartZ
                    );
                var pos2 = new Vector3(
                        UnclampedLerp(startPointRes2.x, resourcesJumpFinishPoint.position.x, resourcesJumpPositionXCurve.Evaluate(normalizedDuration)),
                        UnclampedLerp(startPointRes2.y, resourcesJumpFinishPoint.position.y, resourcesJumpPositionYCurve.Evaluate(normalizedDuration)),
                        res2StartZ
                    );

                resource1.position = pos1;
                resource1.localScale = Vector3.one * scale;
                resource2.position = pos2;
                resource2.localScale = Vector3.one * scale;

                yield return new WaitForFixedUpdate();
            }


            resource1.gameObject.SetActive(false);
            resource2.gameObject.SetActive(false);

            presentAnimator.SetBool(AnimatorOpenStateName, false);
            presentAnimator.SetBool(AnimatorIdleStateName, false);

            resourcesJumpAnimationFinish.Invoke();
        }

        float UnclampedLerp(float a, float b, float t)
        {
            return a + (b - a) * t;
        }

        public void SetPresentTapTrigger()
        {
            presentAnimator.SetTrigger(AnimatorTapTriggerName);
        }
    }
}