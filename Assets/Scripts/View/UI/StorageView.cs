﻿using System;
using UnityEngine.UI;
using View.UI.Base;

namespace View.UI
{
    public class StorageView : UITabControlledList<StorageViewItem>
    {
        Text txtRatio;
        Slider sliderCapacity;

        public string capacityRatioStr  { set { txtRatio.text = value; } }
        public float  capacityRatio     { set { sliderCapacity.value = value; } }

        protected override void Awake()
        {
            itemsRoot = DefaultItemsRoot();
            tabsGenericAliases = new string[]
            {
                "storageTab_Main",
                "storageTab_Sports"
            };
            BindCloseButton();

            var sliderRoot = transform.Find("Slider");
            sliderCapacity = sliderRoot.Find("SliderCapacity").GetComponent<Slider>();
            txtRatio = sliderRoot.Find("TxtRatio").GetComponent<Text>();

            base.Awake();
        }

        /*
        public void ClearHintCallbacks()
        {
            foreach (var item in items)
            {
                item.ClearHintCallbacks();
            }
        }*/
    }
}
