﻿using Core;
using Data;
using UnityEngine;
using View.Actions;
using View.UI.OverheadUI;
using View.UI.Shop;

namespace View.UI
{
    [AddComponentMenu("Kingdom/View/UI/GUI Root")]
    public class GUIRoot : MonoBehaviour
    {
        public static GUIRoot instance;

        public LayerMask UIOnlyLayerMask;
        public LeanTweenType windowOpeningType;
        public float windowOpeningTime;

        public WindowManager windows { get; private set; }
        public HUD.HUD hud { get; private set; }
        public SportShopView sportShop { get; private set; }
        public ConfirmWnd confirmWnd { get; private set; }
        public ConfirmWithBucks confirmWithBucks { get; private set; }
        public DailyQuestView dailyQuests { get; private set; }
        public DailyBonusView dailyBonus { get; private set; }
        public VisitorQuestsView visitorQuests { get; private set; }
        public LevelUpView levelUp { get; private set; }
        public SettingsView settingsWnd { get; private set; }
        public TutorialMsgView tutorialMsg { get; private set; }
        public UiFx fx { get; private set; }
        public StorageView storage { get; private set; }
        public BuyWithResourcesView buyWithResources { get; private set; }
        public DbgView dbgView { get; private set; }
        public SportsmenView sportsmen { get; private set; }
        public SportLaneQuestView sportLaneQuest { get; private set; }
        public SportsmenAgencyView sportsmenAgency { get; private set; }
        public SportTimeQuestView sportTimeQuest { get; private set; }
        public SportsmanTrainingView sportsmanTrainingView  { get; private set; }
        public GymUnlockView        gymUnlock               { get; private set; }
        public ShopUltimateView     shopUltimate            { get; private set; }
        public RateUsView           rateUs                  { get; private set; }
        public PromoView            promo                   { get; private set; }
        public RankAchievmentsView  rankAchievements        { get; private set; }
        public RankLvlUpView        rankLvlUp               { get; private set; }
        public UpgradeEquipmentView upgradeEquipment        { get; private set; }
        public HQView               hq                      { get; private set; }
        public FeatureLockedView    sportClubLockedView     { get; private set; }
        public FeatureLockedView    juiceBarLockedView      { get; private set; }
        public FeatureLockedView    sportAgencyLockedView   { get; private set; }
        public MakeJuiceView        makeJuice               { get; private set; }
        public TrainersView         trainers                { get; private set; }
        public TrainerInfoView      trainerInfo             { get; private set; }

        public TooltipView tooltip { get; private set; }
        public FriendsListView friendsList { get; private set; }
        public StyleWindowView styleWindow { get; private set; }

        private Canvas overheadUICanvas;
        private LayerMask defaultMainCameraLayerMask;

        public Camera   UICamera;

        private void Awake()
        {
            instance = this;

            hud                     = transform.Find("HUD").GetComponent<HUD.HUD>();
            confirmWnd              = transform.Find("Confirms").GetComponent<ConfirmWnd>();
            confirmWithBucks        = transform.Find("ConfirmBuyWithBucks").GetComponent<ConfirmWithBucks>();
            sportShop               = transform.Find("SportsmenShop").GetComponent<SportShopView>();
            dbgView                 = transform.Find("Dbg").GetComponent<DbgView>();
            dailyQuests             = transform.Find("DailyQuestView").GetComponent<DailyQuestView>();
            dailyBonus              = transform.Find("DailyBonus").GetComponent<DailyBonusView>();
            visitorQuests           = transform.Find("VisitorQuestsView").GetComponent<VisitorQuestsView>();
            levelUp                 = transform.Find("LevelUp").GetComponent<LevelUpView>();
            settingsWnd             = transform.Find("SettingsWindow").GetComponent<SettingsView>();
            tutorialMsg             = transform.Find("TutorialMsgView").GetComponent<TutorialMsgView>();
            fx                      = transform.Find("UiFx").GetComponent<UiFx>();
            storage                 = transform.Find("Storage").GetComponent<StorageView>();
            buyWithResources        = transform.Find("BuyWithResources").GetComponent<BuyWithResourcesView>();
            sportLaneQuest          = transform.Find("SportLaneQuest").GetComponent<SportLaneQuestView>();
            sportsmen               = transform.Find("SportsmenView").GetComponent<SportsmenView>();
            sportsmenAgency         = transform.Find("SportsmenAgency").GetComponent<SportsmenAgencyView>();
            sportTimeQuest          = transform.Find("SportTimeQuest").GetComponent<SportTimeQuestView>();
            sportsmanTrainingView   = transform.Find("SportsmanTraining").GetComponent<SportsmanTrainingView>();
            gymUnlock               = transform.Find("GymUnlock").GetComponent<GymUnlockView>();
            shopUltimate            = transform.Find("UltimateShop").GetComponent<ShopUltimateView>();
            rateUs                  = transform.Find("RateUs").GetComponent<RateUsView>();
            tooltip                 = transform.Find("Tooltip").GetComponent<TooltipView>();
            friendsList             = transform.Find("FriendListWindow").GetComponent<FriendsListView>();
            promo                   = transform.Find("PromoPurchaseWindow").GetComponent<PromoView>();
            rankAchievements        = transform.Find("RankAchievements").GetComponent<RankAchievmentsView>();
            rankLvlUp               = transform.Find("RankLvlUp").GetComponent<RankLvlUpView>();
            styleWindow             = transform.Find("StyleWindow").GetComponent<StyleWindowView>();
            upgradeEquipment        = transform.Find("UpgradeEquipment").GetComponent<UpgradeEquipmentView>();
            hq                      = transform.Find("HQ").GetComponent<HQView>();
            sportClubLockedView     = transform.Find("SportClubLocked").GetComponent<FeatureLockedView>();
            juiceBarLockedView      = transform.Find("JuiceBarLocked").GetComponent<FeatureLockedView>();
            makeJuice               = transform.Find("MakeJuiceView").GetComponent<MakeJuiceView>();
            sportAgencyLockedView   = transform.Find("SportAgencyLocked").GetComponent<FeatureLockedView>();
            trainers                = transform.Find("TrainersView").GetComponent<TrainersView>();
            trainerInfo         = transform.Find("TrainerInfoView").GetComponent<TrainerInfoView>();

            if (Camera.main != null)
            {
                defaultMainCameraLayerMask = Camera.main.cullingMask;
            }

            tooltip.gameObject.SetActive(true);

            UICamera = GetComponentInChildren<Camera>();
        }

        private void Start()
        {
            if (dbgView != null)
            {
                dbgView.gameObject.SetActive(true);
            }

            overheadUICanvas = OverheadUIManager.instance.gameObject.GetComponent<Canvas>();

            windows = new WindowManager(overheadUICanvas, hud, this);

            windows.AddWindow(WindowTypes.SportShop, sportShop);
            windows.AddWindow(WindowTypes.PopUp, confirmWnd);
            windows.AddWindow(WindowTypes.ConfirmWithBucks, confirmWithBucks);
            windows.AddWindow(WindowTypes.DailyQuests, dailyQuests);
            windows.AddWindow(WindowTypes.VisitorQuests, visitorQuests);
            windows.AddWindow(WindowTypes.LevelUp, levelUp);
            windows.AddWindow(WindowTypes.Settings, settingsWnd);
            windows.AddWindow(WindowTypes.TutorialMsg, tutorialMsg.modalMsgView);
            windows.AddWindow(WindowTypes.Storage, storage);
            windows.AddWindow(WindowTypes.DailyBonus, dailyBonus);
            windows.AddWindow(WindowTypes.BuyWithResources, buyWithResources);
            windows.AddWindow(WindowTypes.SportsmansList, sportsmen);
            windows.AddWindow(WindowTypes.SportLaneQuest, sportLaneQuest);
            windows.AddWindow(WindowTypes.SportsmenAgency, sportsmenAgency);
            windows.AddWindow(WindowTypes.SportTimeQuest, sportTimeQuest);
            windows.AddWindow(WindowTypes.SportsmanTraining, sportsmanTrainingView);
            windows.AddWindow(WindowTypes.GymUnlock, gymUnlock);
            windows.AddWindow(WindowTypes.ShopUltimate, shopUltimate);
            windows.AddWindow(WindowTypes.RateUs, rateUs);
            windows.AddWindow(WindowTypes.FriendsList, friendsList);
            windows.AddWindow(WindowTypes.Promo, promo);
            windows.AddWindow(WindowTypes.RankAchievements, rankAchievements);
            windows.AddWindow(WindowTypes.RankLvlUp, rankLvlUp);
            windows.AddWindow(WindowTypes.StyleWindow, styleWindow);
            windows.AddWindow(WindowTypes.UpgradeEquipment, upgradeEquipment);
            windows.AddWindow(WindowTypes.HQ, hq);
            windows.AddWindow(WindowTypes.SportClubLocked, sportClubLockedView);
            windows.AddWindow(WindowTypes.JuiceBarLocked, juiceBarLockedView);
            windows.AddWindow(WindowTypes.MakeJuice, makeJuice);
            windows.AddWindow(WindowTypes.SportAgencyLocked, sportAgencyLockedView);
            windows.AddWindow(WindowTypes.Trainers, trainers);
            windows.AddWindow(WindowTypes.TrainersInfoView, trainerInfo);

#if DEBUG
            hud.btnDbg.onClick.AddListener
            (delegate ()
             {
                 dbgView.gameObject.SetActive(!dbgView.gameObject.activeSelf);
                 hud.btnDbg.gameObject.SetActive(!dbgView.gameObject.activeSelf);
             }
            );
            dbgView.onHide += delegate ()
            {
                hud.btnDbg.gameObject.SetActive(true);
            };
#endif
            hud.OnBackgroundClick += windows.CloseCurrentByUserClick;
            hud.btnSettings.onClick.AddListener(settingsWnd.Show);
            hud.btnDailyQuests.onClick.AddListener(dailyQuests.Show);
            hud.btnDailyBonus.onClick.AddListener(dailyBonus.Show);
            hud.btnVisitorQuests.onClick.AddListener(visitorQuests.Show);
            hud.btnLaneQuests.onClick.AddListener(sportLaneQuest.Show);
            hud.btnSportQuests.onClick.AddListener(sportTimeQuest.Show);
            hud.btnShop.onClick.AddListener(shopUltimate.Show);
            hud.OnFriendsClick = friendsList.Show;
            hud.btnStorage.onClick.AddListener(storage.Show);

            windows.ShowDefault();
            tooltip.gameObject.SetActive(false);

            {
                var bih = GetComponent<ButtonInputHandler>();
                if (bih != null)
                {
                    bih.onButtonUp.AddListener (OnQuitGameRequest);
                }
            }
        }

        private void OnDestroy ()
        {
            {
                var bih = GetComponent<ButtonInputHandler>();
                if (bih != null)
                {
                    bih.onButtonUp.RemoveListener (OnQuitGameRequest);
                }
            }

            instance = null;
        }

        #if DEBUG
        void Update()
        {
            if (Input.touches.Length == 5)
            {
                hud.btnDbg.gameObject.SetActive(false);
            }
            else if (Input.touches.Length == 4)
            {
                hud.btnDbg.gameObject.SetActive(true);
            }
        }
        #endif

        void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                
            }
        }

        public void ToggleOverheadUI(bool enable)
        {
            if (overheadUICanvas != null)
            {
                overheadUICanvas.enabled = enable;
            }
        }

        public void SetRenderUIOnly(bool uiOnly)
        {
            if (Camera.main != null)
            {
                Camera.main.cullingMask = uiOnly ? UIOnlyLayerMask : defaultMainCameraLayerMask;
            }
        }

        public void NeedMoneyMsg(Data.Cost price, bool spendBucksIfNecessary = false)
        {
            if (spendBucksIfNecessary)
                price.type = Data.Cost.CostType.BucksOnly;

            confirmWnd.OnNeedMoney(price);
            if (price.type == Data.Cost.CostType.CoinsOnly)
            {
                confirmWnd.OnConfirm = delegate
                {
                    shopUltimate.Show();
                    shopUltimate.ShowBankCoins();
                    //shop.Show();
                    //shop.SetCategory(Data.ShopItemCategory.BANK, 0);
                    Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PopUp");
                };
            } else if (price.type == Data.Cost.CostType.BucksOnly || spendBucksIfNecessary)
            {
                confirmWnd.OnConfirm = delegate
                {
                    shopUltimate.Show();
                    shopUltimate.ShowBankBucks();
                    //shop.Show();
                    //shop.SetCategory(Data.ShopItemCategory.BANK, 5);
                    Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PopUp");
                };
            } else
            {
                confirmWnd.OnConfirm = delegate
                {
                    shopUltimate.Show();
                    shopUltimate.ShowBankBucks();
                    //shop.Show();
                    //shop.SetCategory(Data.ShopItemCategory.BANK, 5);
                    Core.PersistentCore.instance.analytics.DetailGameEvent("Bank Open", "From", "PopUp");
                };
            }

            ToggleOverheadUI(false);
        }

        private void OnQuitGameRequest ()
        {
            if (windows.GetCurrentType() == WindowTypes.None &&
                PersistentCore.instance.modalMessagesQueueEmpty &&
                !PersistentCore.instance.modalMessagesWindowShown &&
                !hud.btnBack.gameObject.activeSelf)
            {
                PersistentCore.instance.QueueModalConfirmMessage
                    ( Loc.Get("QuitGameHeader")
                    , Loc.Get("QuitGameMessage")
                    , () =>
                    {
                        PersistentCore.Quit(false);
                    }
                    , Loc.Get("idYes")
                    , () =>
                    {
                    }
                    , Loc.Get("idNo"));
            }
        }

        public static void IsolateRect(RectTransform rect, bool isShadowVisible)
        {
            instance.tutorialMsg.ShowRectAt(rect, isShadowVisible);
        }

        public static void ResetIsolatedRect()
        {
            instance.tutorialMsg.HideRect();
        }

        public static void HideWindows()
        {
            instance.windows.ShowDefault();
        }
    }
}
