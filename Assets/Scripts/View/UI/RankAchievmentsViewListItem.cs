﻿using UnityEngine;
using System.Collections;
using System;
using View.UI.Base;

namespace View.UI
{
    public class RankAchievmentsViewListItem : UIBaseViewListItem
    {
        public UIHintBehaviour  hint;
        public Action<int> OnClaim_Callback;

        public string title     { set { genericView.SetText("txtTitle", value); } }
        public string subTitle  { set { genericView.SetText("txtSubTitle", value); } }
        public string steps     { set { genericView.SetText("txtSteps", value); } }
        public float  progress  { set { genericView.SetSliderValue("progress", value); } }
        public Sprite icon      { set { genericView.SetSprite("icon", value); } }
        public int reward       { set { genericView.SetText("reward", value.ToString()); } }


        public bool claimOn        { set { genericView.SetActive("btnClaim", value); genericView.SetActive("bigBtnClaim", value); } }
        public bool progressOn     { set { genericView.SetActive("progress", value); } }
        public bool completedOn    { set { genericView.SetActive("completed", value);} }

        public bool         dbgOn        { set { genericView.SetActive("dbg", value); } }
        #if DEBUG
        public Action<int>  dbgAction; 
        public string       dbgText      { set { genericView.SetText("dbgText", value); } }
        #endif

        new void Awake()
        {
            hint = GetComponentInChildren<UIHintBehaviour>();
            genericView.SetButtonCallback("btnClaim", OnCliam);
            genericView.SetButtonCallback("bigBtnClaim", OnCliam);
                
            #if DEBUG
            genericView.SetButtonCallback("dbg", OnDebugClick);
            #else
            dbgOn = false;
            #endif
        }

        void OnCliam()
        {
            if (OnClaim_Callback != null)
            {
                OnClaim_Callback(idx);
            }
            else
            {
                Debug.LogError("RankAchievmentsViewListItem OnClaim_Callback == null");
            }
        }

        #if DEBUG
        void OnDebugClick()
        {
            dbgAction(idx);
        }
        #endif
    }
}
