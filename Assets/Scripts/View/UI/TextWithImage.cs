﻿using UnityEngine;
using UnityEngine.UI;
using View.PrefabResources;
using System.Text.RegularExpressions;
using System;
using CustomAssets.Lists;

namespace View.UI
{
    [AddComponentMenu("Kingdom/View/UI/Text with Image")]
    public class TextWithImage : MonoBehaviour
    {
        public float lineSpacing = 1.0f;

        private float curPositionX;
        private float curPositionY;
        private float curWidth;
        private float curHeight;
        private Text txtTemplate;
        private float imageSize = 1.0f;

        //--- some magic
        private const float cTextHeightCoef = 1.25f;
        private const float cImageHeightCoef = cTextHeightCoef - 1.0f;

        private TextIconsLibrary textIconsLibrary = null;
        private RectTransform rectBase = null;
        RectTransform containerRect = null;

        private void Awake()
        {
            txtTemplate = gameObject.GetComponent<Text>();
            txtTemplate.enabled = false;

            rectBase = gameObject.GetComponent<RectTransform>();

            GameObject containerGo = new GameObject("Text_Container");
            containerRect = containerGo.AddComponent<RectTransform>();
        }

        private void Start()
        {
            textIconsLibrary = GUICollections.instance.textIconsLibrary;
            if (textIconsLibrary == null)
                Debug.Log("TextIconsLibrary not found");

            FillText();
        }

        private void Reset()
        {
            foreach (Transform child in txtTemplate.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            curPositionX = 0.0f;
            curPositionY = 0.0f;
            curWidth = 0.0f;
            curHeight = txtTemplate.fontSize;
            imageSize = 1.0f;
        }

        public void FillText()
        {
            if (textIconsLibrary == null)
                return;

            Reset();

            containerRect.SetParent(rectBase, false);
            containerRect.transform.position = new Vector3 (0.0f, 0.0f, 0.0f);
            containerRect.position = rectBase.position;
            containerRect.sizeDelta = rectBase.sizeDelta;

            int textPos = 0;
            int searchPos = 0;

            while (textPos < txtTemplate.text.Length)
            {
                int scopePos = txtTemplate.text.IndexOf('@', searchPos);

                if (scopePos < 0)
                    break;

                searchPos = scopePos;

                string subStr = txtTemplate.text.Substring (scopePos);

                int tResult = DefineIconSize (subStr); 
                if (tResult > 0)
                {
                    if (scopePos > textPos)
                        PrintText(txtTemplate.text.Substring(textPos, scopePos - textPos), containerRect);
                    
                    textPos = scopePos + tResult;
                    searchPos = textPos;
                    continue;
                }

                int iIndex = textIconsLibrary.GetIconIndex(subStr);

                if (iIndex >= 0)
                {
                    if (scopePos > textPos)
                        PrintText(txtTemplate.text.Substring(textPos, scopePos - textPos), containerRect);

                    PrintImage(curPositionX, textIconsLibrary.GetIcon(iIndex), containerRect);

                    textPos = scopePos + textIconsLibrary.GetIconTextLength(iIndex);
                    searchPos = textPos;
                }
                else
                {
                    ++searchPos;
                }
            }

            if (textPos < txtTemplate.text.Length - 1)
                PrintText(txtTemplate.text.Substring(textPos), containerRect);

            //--- align
            AlignTextContainer();
        }

        private void AlignTextContainer()
        {
            containerRect.position = new Vector3(0.0f, 0.0f, 0.0f);
            containerRect.position = rectBase.position;
            containerRect.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

            float centerX = rectBase.rect.xMin + rectBase.rect.width / 2.0f - curWidth / 2.0f;
            float centerY = rectBase.rect.yMax - rectBase.rect.height / 2.0f + curHeight / 2.0f - txtTemplate.fontSize / 2.0f;

            float rightX = rectBase.rect.xMax - curWidth;
            float bottomY = rectBase.rect.yMin + curHeight;

            switch (txtTemplate.alignment)
            {
                case TextAnchor.UpperLeft:
                    containerRect.localPosition = new Vector3(rectBase.rect.xMin, rectBase.rect.yMax, 0.0f);
                    break;

                case TextAnchor.UpperCenter:
                        containerRect.localPosition = new Vector3(centerX, rectBase.rect.yMax, 0.0f);
                    break;

                case TextAnchor.UpperRight:
                    containerRect.localPosition = new Vector3(rightX, rectBase.rect.yMax, 0.0f);
                    break;

                case TextAnchor.MiddleLeft:
                    containerRect.localPosition = new Vector3(rectBase.rect.xMin, centerY, 0.0f);
                    break;

                case TextAnchor.MiddleCenter:
                    containerRect.localPosition = new Vector3(centerX, centerY, 0.0f);
                    break;

                case TextAnchor.MiddleRight:
                    containerRect.localPosition = new Vector3(rightX, centerY, 0.0f);
                    break;

                case TextAnchor.LowerLeft:
                    containerRect.localPosition = new Vector3(rectBase.rect.xMin, bottomY, 0.0f);
                    break;

                case TextAnchor.LowerCenter:
                    containerRect.localPosition = new Vector3(centerX, bottomY, 0.0f);
                    break;

                case TextAnchor.LowerRight:
                    containerRect.localPosition = new Vector3(rightX, bottomY, 0.0f);
                    break;

                default:
                    //containerRect.localPosition = new Vector3(-curWidth / 2.0f, curHeight / 2.0f, 0.0f);
                    break;
            }
        }

        private int DefineIconSize(string subString)
        {
            if (subString.IndexOf ("@iconSize") == 0) 
            {
                int secondScope = subString.IndexOf('@', 1);
                string tStr = subString.Substring (0, secondScope + 1);

                var resultString = Regex.Match (tStr, @"\d+").Value;

                imageSize = (float)(Int32.Parse(resultString)) / 100.0f;

                return tStr.Length;
            }          

            return 0;
        }

        private void PrintText(string text_, RectTransform containerTransform)
        {
            int pSearch = 0;
            int endlPos = text_.IndexOf('\n');
            string subText = text_;

            while (endlPos >= 0)
            {
                if (endlPos > pSearch)
                {
                    subText = text_.Substring(pSearch, endlPos - pSearch);
                    PrintTextPart(curPositionX, subText, containerTransform);
                }

                SetNewLine();
                pSearch = endlPos + 1;
                endlPos = text_.IndexOf('\n', pSearch);
            }

            subText = text_.Substring(pSearch);
            PrintTextPart(curPositionX, subText, containerTransform);
        }

        private void SetNewLine()
        {
            curPositionX = 0.0f;
            curPositionY += txtTemplate.fontSize * lineSpacing;
            curHeight += txtTemplate.fontSize * lineSpacing;
        }

        private void PrintTextPart(float posX, string text_, RectTransform containerTransform)
        {
            GameObject EmptyGo = new GameObject("Text");
            EmptyGo.AddComponent<RectTransform>();

            GameObject newEmpty = Instantiate(EmptyGo, Vector3.zero, Quaternion.identity) as GameObject;
			Destroy (EmptyGo);

            RectTransform tRect = newEmpty.GetComponent<RectTransform>();
            tRect.SetParent(containerTransform, false);

            tRect.pivot = new Vector2 (0.0f, 0.5f);//Vector2.zero;

            Text txt = newEmpty.AddComponent<Text>();
            txt.font = txtTemplate.font;
            txt.color = txtTemplate.color;
            txt.fontSize = txtTemplate.fontSize;
            txt.text = text_;

            float prefferWidth = LayoutUtility.GetPreferredWidth(txt.rectTransform);
            float prefferHeight = txtTemplate.fontSize * cTextHeightCoef;
            tRect.sizeDelta = new Vector2(prefferWidth, prefferHeight);

            txt.rectTransform.position = Vector3.zero;

            txt.rectTransform.localPosition = new Vector3(posX, /*-containerTransform.rect.height / 2*/ - curPositionY - txtTemplate.fontSize / 2.0f * 1.5f, 0.0f);
            txt.rectTransform.localScale = Vector3.one;

            curPositionX += prefferWidth;

            if (curPositionX > curWidth)
                curWidth = curPositionX;

            Debug.Log(text_ + " : " + curPositionX.ToString() + " / " + rectBase.rect.width);

            if (curPositionX >= rectBase.rect.width / 2.0f)
                SetNewLine();

            //return prefferWidth;
        }

        private void PrintImage(float posX, Sprite image_, RectTransform containerTransform)
        {
            float tSize = txtTemplate.fontSize * cTextHeightCoef * imageSize;

            GameObject EmptyGo = new GameObject("Image");
            EmptyGo.AddComponent<RectTransform>();

            GameObject newEmpty = Instantiate(EmptyGo, new Vector3(posX, containerTransform.position.y, 0), Quaternion.identity) as GameObject;
			Destroy (EmptyGo);

			RectTransform tRect = newEmpty.GetComponent<RectTransform>();
            tRect.SetParent(containerTransform, false);

            //tRect.anchoredPosition = rectBase.anchoredPosition;
            //tRect.anchorMax = rectBase.anchorMax;
            //tRect.anchorMin = rectBase.anchorMin;

            tRect.pivot = new Vector2 (0.0f, 0.5f);//Vector2.zero;

            Image img = newEmpty.AddComponent<Image>();
            img.sprite = image_;

            newEmpty.GetComponent<RectTransform>().sizeDelta = new Vector2(tSize, tSize);

            img.rectTransform.localPosition = new Vector3(posX, -curPositionY - txtTemplate.fontSize / 2.0f, 0.0f);

            imageSize = 1.0f;

            curWidth += tSize;
            curPositionX += tSize;

            Debug.Log("@@ : " + curPositionX.ToString() + " / " + rectBase.rect.width);

            if (curPositionX >= rectBase.rect.width / 2.0f)
                SetNewLine();

            //return tSize;
        }
       
    }
}