﻿using UnityEngine;
using InspectorHelpers;
using View.EditorHelpers;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.WaypointsWalker
{
    [System.Serializable]
    public class Waypoint
    {
        public enum MovementSwitchMode
        {
            Jump,
            Interpolate
        }

        public Vector3 position
        {
            get { return transform != null ? transform.position : position_; }
        }

        public Quaternion rotation
        {
            get { return transform != null ? transform.rotation : rotation_; }
        }

        public Transform transform;
        public bool atWaypointSignal;
        public float rotationSpeed;
        public float movementSpeedMultiplier;
        public MovementSwitchMode movementSwitchMode;

        private Vector3 position_;
        private Quaternion rotation_;

        public Waypoint()
        {
            transform = null;
            atWaypointSignal = false;
            rotationSpeed = 90.0f;
            movementSpeedMultiplier = 1.0f;
            movementSwitchMode = MovementSwitchMode.Jump;

            position_ = Vector3.zero;
            rotation_ = Quaternion.identity;
        }

        public Waypoint(Vector3 customPosition, Quaternion customRotation)
        {
            transform = null;
            atWaypointSignal = false;
            rotationSpeed = 90.0f;
            movementSpeedMultiplier = 1.0f;
            movementSwitchMode = MovementSwitchMode.Jump;

            position_ = customPosition;
            rotation_ = customRotation;
        }
    }

    [AddComponentMenu("Kingdom/View/Waypoints Walker/Waypoints Route Definition")]
    public class WaypointsRouteDefinition : MonoBehaviour
    {
        // Fields.
        [ReorderableList]
        public Waypoint[] waypointsList;

#if UNITY_EDITOR
        // Gizmo drawer
        public class Gizmo
        {
            private static float arrowMagnitude = 0.1f;
            private static float dottedLineSize = 5.0f;

            [DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.NonSelected | GizmoType.NotInSelectionHierarchy | GizmoType.InSelectionHierarchy)]
            private static void DrawGizmo(WaypointsRouteDefinition scr, GizmoType gizmoType)
            {
                if (scr.waypointsList == null || scr.waypointsList.Length == 0)
                    return; // Nothing to do.

                bool ownerSelected = (Selection.Contains(scr.gameObject));
                bool consumerSelected = false;

                if (Selection.activeGameObject != null)
                {
                    var waypointsWalker = Selection.activeGameObject.GetComponent<WaypointsWalker>();
                    if (waypointsWalker != null && waypointsWalker.routes != null)
                    {
                        for (var i = 0; i < waypointsWalker.routes.Length; i++)
                        {
                            var route = waypointsWalker.routes[i];
                            if (route != null && route.Equals(scr))
                            {
                                consumerSelected = true;
                                break;
                            }
                        }
                    }
                }

                // Draw waypoint marks.
                {
                    Color waypointColor = Color.gray;
                    waypointColor.a = 0.2f;

                    if (ownerSelected || consumerSelected)
                    {
                        waypointColor = Color.red;
                        waypointColor.a = 0.3f;
                    }

                    Handles.color = waypointColor;

                    for (var i = 0; i < scr.waypointsList.Length; i++)
                    {
                        var waypoint = scr.waypointsList[i];

                        if (Selection.Contains(waypoint.transform.gameObject))
                        {
                            Color waypointColor2 = Color.white;
                            waypointColor2.a = 0.3f;
                            Handles.color = waypointColor2;
                        }

                        Handles.DrawSolidDisc(waypoint.transform.position, Vector3.up, 0.05f);

                        Handles.color = waypointColor;
                    }
                }

                // Draw waypoint links.
                if (scr.waypointsList.Length > 1)
                {
                    Color linkColor = Color.gray;
                    linkColor.a = 0.35f;

                    if (ownerSelected || consumerSelected)
                    {
                        linkColor = Color.red;
                        linkColor.a = 0.75f;
                    }

                    for (var i = 0; i < scr.waypointsList.Length; i++)
                    {
                        var waypoint1 = scr.waypointsList[(i == 0) ? scr.waypointsList.Length - 1 : i - 1];
                        var waypoint2 = scr.waypointsList[i];

                        if (Selection.Contains(waypoint1.transform.gameObject))
                        {
                            linkColor = Color.white;
                            linkColor.a = 0.75f;
                        }

                        EditorGizmoHelper.DrawArrow(waypoint1.transform.position, waypoint2.transform.position, linkColor, (i == 0) ? dottedLineSize : 0.0f, arrowMagnitude);
                    }
                }
            }
        }
#endif // UNITY_EDITOR
    }
}