﻿using UnityEngine;

namespace View.WaypointsWalker.StateMachineBehaviours
{
    public class WaypointsWalkerStateBehaviour : StateMachineBehaviour
    {
        // Fields.
        public string speedsTag = string.Empty;
        public string atWaypointParameter = string.Empty;
        public string speedMultiplierParameter = string.Empty;

        // Private vars.
        private WaypointsWalker waypointsWalker = null;
        private MovementSpeeds movementSpeeds = null;
        private float cachedAnimSpeedMultiplier = 0.0f;
        private bool entered = false;
        private int atWaypointParamId;
        private int speedMultiplierParamId;

        private void OnEnable()
        {
            if (atWaypointParameter.Length > 0 && atWaypointParamId == 0)
                atWaypointParamId = Animator.StringToHash(atWaypointParameter);
            if (speedMultiplierParameter.Length > 0 && speedMultiplierParamId == 0)
                speedMultiplierParamId = Animator.StringToHash(speedMultiplierParameter);
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (entered)
                return;

            if (waypointsWalker == null)
                waypointsWalker = animator.gameObject.GetComponent<WaypointsWalker>();

            if (waypointsWalker != null)
            {
                if (movementSpeeds == null)
                    movementSpeeds = GetMovementSpeeds();

                if (movementSpeeds != null && speedMultiplierParamId != 0)
                {
                    UpdateSpeedMultiplierParam(animator, GetMovementSpeedMultiplier(0.0f));
                    entered = true;
                }
            }

            if (atWaypointParamId != 0)
                animator.SetBool(atWaypointParamId, false);
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!entered)
                return;

            if (atWaypointParamId != 0)
                animator.SetBool(atWaypointParamId, false);
            entered = false;
            cachedAnimSpeedMultiplier = 0.0f;
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!entered)
                return;

            var targetWaypoint = waypointsWalker.targetWaypoint;
            var prevWaypoint = waypointsWalker.previousWaypoint;
            if (targetWaypoint != null && prevWaypoint != null)
            {
                float step = Time.deltaTime;

                switch (targetWaypoint.movementSwitchMode)
                {
                    case Waypoint.MovementSwitchMode.Jump:
                    {
                        step *= movementSpeeds.moveSpeed;
                    }
                    break;

                    case Waypoint.MovementSwitchMode.Interpolate:
                    {
                        var prevToNextDistance = (prevWaypoint.position - targetWaypoint.position).magnitude;

                        var movementSpeedMultiplier = (prevToNextDistance > Mathf.Epsilon)
                            ? GetMovementSpeedMultiplier((waypointsWalker.transform.position - prevWaypoint.position).magnitude / prevToNextDistance) : 1.0f;

                        if (speedMultiplierParamId != 0)
                            UpdateSpeedMultiplierParam(animator, movementSpeedMultiplier);
                        step *= movementSpeeds.moveSpeed * movementSpeedMultiplier;
                    }
                    break;
                }

                var newPos = Vector3.MoveTowards(waypointsWalker.transform.position, targetWaypoint.position, step);

                var waypointDistance = Vector3.Distance (newPos, waypointsWalker.transform.position);
                if (waypointDistance > Mathf.Epsilon)
                {
                    waypointsWalker.transform.position = newPos;

                    var rotateStep = targetWaypoint.rotationSpeed * Time.deltaTime;
                    waypointsWalker.transform.rotation = Quaternion.RotateTowards(waypointsWalker.transform.rotation, targetWaypoint.rotation, rotateStep);

                    if (atWaypointParamId != 0 && animator.GetBool(atWaypointParamId))
                    {
                        var prevWaypointDistance = Vector3.Distance (newPos, prevWaypoint.position);
                        if (prevWaypointDistance >= waypointsWalker.waypointProximityDistance)
                        {
                            animator.SetBool(atWaypointParamId, false);
                        }
                    }
                }
                else
                {
                    if (targetWaypoint.atWaypointSignal && atWaypointParamId != 0)
                        animator.SetBool(atWaypointParamId, true);

                    waypointsWalker.SetNextTargetWaypoint();
                    UpdateSpeedMultiplierParam(animator, GetMovementSpeedMultiplier(0.0f));
                }
            }
        }

        // Private functions.
        private MovementSpeeds GetMovementSpeeds()
        {
            if (speedsTag.Length > 0)
            {
                for (var i = 0; i < waypointsWalker.movementSpeeds.Length; i++)
                {
                    var ms = waypointsWalker.movementSpeeds[i];
                    if (speedsTag == ms.name)
                        return ms;
                }
            }
            return null;
        }

        private float GetAnimationSpeedMultiplier(MovementSpeeds ms)
        {
            if (waypointsWalker != null && ms != null)
            {
                var targetWaypoint = waypointsWalker.targetWaypoint;
                if (targetWaypoint != null)
                {
                    return ms.animMultiplier * ms.moveSpeed;
                }
            }
            return 0.0f;
        }

        private float GetMovementSpeedMultiplier(float factor)
        {
            var targetWaypoint = waypointsWalker.targetWaypoint;
            if (targetWaypoint != null)
            {
                switch (targetWaypoint.movementSwitchMode)
                {
                    case Waypoint.MovementSwitchMode.Jump:
                    return targetWaypoint.movementSpeedMultiplier;

                    case Waypoint.MovementSwitchMode.Interpolate:
                    {
                        var previousWaypoint = waypointsWalker.previousWaypoint;
                        if (previousWaypoint != null)
                        {
                            return Mathf.Lerp(previousWaypoint.movementSpeedMultiplier, targetWaypoint.movementSpeedMultiplier, factor);
                        }
                        else
                            return targetWaypoint.movementSpeedMultiplier;
                    }
                }
            }
            return 0.0f;
        }

        private void UpdateSpeedMultiplierParam(Animator animator, float movementSpeedMultiplier)
        {
            var animSpeedMultiplier = movementSpeeds.moveSpeed * movementSpeeds.animMultiplier * movementSpeedMultiplier;

            if (cachedAnimSpeedMultiplier != animSpeedMultiplier && animSpeedMultiplier > Mathf.Epsilon)
            {
                animator.SetFloat(speedMultiplierParamId, animSpeedMultiplier);
                cachedAnimSpeedMultiplier = animSpeedMultiplier;
            }
        }
    }
}