﻿using UnityEngine;
using InspectorHelpers;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.WaypointsWalker
{
    [System.Serializable]
    public class MovementSpeeds
    {
        public string name;
        public float moveSpeed = 1.0f;
        public float animMultiplier = 1.0f;
    }

    [AddComponentMenu("Kingdom/View/Waypoints Walker/Waypoints Walker")]
    [DisallowMultipleComponent]
    public class WaypointsWalker : MonoBehaviour
    {
        // Types.
        public enum RouteSwitchMode
        {
            Next,
            Random
        }

        public int currentRoute
        {
            get { return currentRoute_; }
        }

        public int previousWaypointIndex
        {
            get { return previousWaypointIndex_; }
        }

        public int targetWaypointIndex
        {
            get {  return targetWaypointIndex_; }
        }

        public Waypoint previousWaypoint
        {
            get
            {
                if (fixupPreviousWaypoint != null)
                    return fixupPreviousWaypoint;

                return (currentRoute_ >= 0 && previousWaypointIndex_ >= 0)
                    ? routes[currentRoute_].waypointsList[previousWaypointIndex_] : null;
            }
        }

        public Waypoint targetWaypoint
        {
            get
            {
                if (fixupTargetWaypoint != null)
                    return fixupTargetWaypoint;

                return (currentRoute_ >= 0 && targetWaypointIndex_ >= 0)
                    ? routes[currentRoute_].waypointsList[targetWaypointIndex_] : null;
            }
        }

        // Fields.
        [ReorderableList]
        public WaypointsRouteDefinition[] routes;
        public RouteSwitchMode routeSwitchMode = RouteSwitchMode.Next;
        public float waypointProximityDistance = 0.1f;
        public bool setToStartWaypoint = true;
        [ReorderableList]
        public MovementSpeeds[] movementSpeeds;
        public string fixupRouteParameterName;

        // Private vars.
        private int currentRoute_ = -1;          // Means: requires initialization.
        private int previousWaypointIndex_ = -1;  // Means: requires initialization.
        private int targetWaypointIndex_ = -1;   // Means: requires initialization.
        private Waypoint fixupPreviousWaypoint = null;
        private Waypoint fixupTargetWaypoint = null;
        private int fixupRouteParameterId = 0;
        private Animator animator = null;

        // Methods.
        void Awake()
        {
            if (fixupRouteParameterName != null && fixupRouteParameterName != string.Empty)
                fixupRouteParameterId = Animator.StringToHash(fixupRouteParameterName); 

            animator = GetComponent<Animator>();
        }

        void Start()
        {
            if (routes.Length > 0)
            {
                SelectNextRoute();

                if (previousWaypointIndex_ >= 0 && targetWaypointIndex_ >= 0 && currentRoute_ >= 0)
                {
                    if (routes[currentRoute_].waypointsList.Length > 1)
                    {
                        if (setToStartWaypoint)
                            SetToStartWaypoint();
                    }
                }
            }
        }

        void Update()
        {
            if (currentRoute_ >= 0 && previousWaypointIndex_ >= 0 && targetWaypointIndex_ >= 0 && routes != null && routes.Length > 0)
            {
                var route = routes[currentRoute_];
                if (route != null)
                {
                    var prevWP = route.waypointsList[previousWaypointIndex_];
                    var targetWP = route.waypointsList[targetWaypointIndex_];
                    if (prevWP != null && targetWP != null && prevWP.transform.hasChanged || targetWP.transform.hasChanged)
                    {
                        if (Vector3.Distance (targetWP.transform.position, transform.position) > Mathf.Epsilon)
                        {
                            Vector3 fixupStartPos = transform.position;
                            Vector3 fixupEndPos = FindClosestPointOnLine(prevWP.position, targetWP.position, fixupStartPos);

                            var lookVector = (fixupEndPos - fixupStartPos);
                            if (lookVector != Vector3.zero && lookVector.sqrMagnitude > Mathf.Epsilon)
                            {
                                fixupPreviousWaypoint = new Waypoint(fixupStartPos, transform.rotation);
                                fixupPreviousWaypoint.rotationSpeed = prevWP.rotationSpeed;
                                fixupTargetWaypoint = new Waypoint(fixupEndPos,  Quaternion.LookRotation(lookVector.normalized));
                                fixupTargetWaypoint.rotationSpeed = targetWP.rotationSpeed;

                                if (animator != null && fixupRouteParameterId != 0)
                                {
                                    animator.SetInteger(fixupRouteParameterId, 1);
                                }
                            }
                        }

                        foreach (var wp in routes[currentRoute_].waypointsList)
                        {
                            wp.transform.hasChanged = false;
                        }
                    }
                }
            }
        }

        public void SetNextTargetWaypoint()
        {
            if (currentRoute_ >= 0)
            {
                if (fixupPreviousWaypoint != null || fixupTargetWaypoint != null)
                {
                    fixupPreviousWaypoint = null;
                    fixupTargetWaypoint = null;
                    if (animator != null && fixupRouteParameterId != 0)
                    {
                        animator.SetInteger(fixupRouteParameterId, 0);
                    }
                }
                else
                {
                    var selectNextRoute = (targetWaypointIndex_ >= routes[currentRoute_].waypointsList.Length - 1);

                    if (selectNextRoute && routes.Length > 1)
                    {
                        if (SelectNextRoute())
                        {
                            // TODO: Next route signaling.
                            if (setToStartWaypoint)
                            {
                                SetToStartWaypoint();
                            }
                        }
                    }
                    else
                    {
                        previousWaypointIndex_ = targetWaypointIndex_;
                        targetWaypointIndex_ = selectNextRoute ? 0 : targetWaypointIndex_ + 1;
                    }
                }
            }
        }

        // Private functions.
        private bool SelectNextRoute()
        {
            int previousRoute = currentRoute_;
            switch (routeSwitchMode)
            {
                case RouteSwitchMode.Next:
                currentRoute_ = (currentRoute_ >= routes.Length) ? 0 : currentRoute_ + 1;
                break;

                case RouteSwitchMode.Random:
                currentRoute_ = Random.Range(0, routes.Length);
                break;
            }

            if (currentRoute_ != previousRoute)
            {
                if (routes[currentRoute_].waypointsList.Length > 1)
                {
                    previousWaypointIndex_ = 0;
                    targetWaypointIndex_ = 1;
                }
                else
                {
                    previousWaypointIndex_ = 0;
                    targetWaypointIndex_ = routes[currentRoute_].waypointsList.Length;
                }

                return true;
            }
            else
                return false;
        }

        private void SetToStartWaypoint()
        {
            transform.position = routes[currentRoute_].waypointsList[previousWaypointIndex_].transform.position;
            transform.rotation = routes[currentRoute_].waypointsList[targetWaypointIndex_].transform.rotation;

            foreach (var wp in routes[currentRoute_].waypointsList)
            {
                wp.transform.hasChanged = false;
            }
        }

        private static Vector3 FindClosestPointOnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
        {
            var vVector1 = vPoint - vA;
            var vVector2 = (vB - vA).normalized;
     
            var d = Vector3.Distance(vA, vB);
            var t = Vector3.Dot(vVector2, vVector1);
     
            if (t <= 0)
                return vA;
     
            if (t >= d)
                return vB;
     
            var vVector3 = vVector2 * t;
     
            var vClosestPoint = vA + vVector3;
     
            return vClosestPoint;
        }


#if UNITY_EDITOR
        // Gizmo drawer
        public class Gizmo
        {
            private static float arrowMagnitude = 0.1f;
            private static float dottedLineSize = 5.0f;

            [DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.NonSelected | GizmoType.NotInSelectionHierarchy | GizmoType.InSelectionHierarchy)]
            private static void DrawGizmo(WaypointsWalker scr, GizmoType gizmoType)
            {
                bool ownerSelected = (Selection.Contains(scr.gameObject));

                if (!ownerSelected || scr.routes.Length == 0)
                    return;

                if (Application.isPlaying)
                {
                    var targetWaypoint = scr.targetWaypoint;
                    if (targetWaypoint != null)
                    {
                        Color linkColor = Color.green;
                        Handles.color = linkColor;

                        Vector3 shift = new Vector3(0.0f, 0.005f, 0.0f);
                        DrawArrow(scr.transform.position + shift, targetWaypoint.transform.position + shift, false);
                    }
                }
                else
                {
                    for (var i = 0; i < scr.routes.Length; i++)
                    {
                        var route = scr.routes[i];
                        if (route != null)
                        {
                            if (route.waypointsList.Length > 0)
                            {
                                Color linkColor = Color.green;
                                //linkColor.a = 0.75f;

                                Handles.color = linkColor;

                                if (scr.setToStartWaypoint)
                                {
                                    var wp = route.waypointsList[0];
                                    DrawArrow(scr.transform.position, wp.transform.position, !scr.setToStartWaypoint);
                                }
                                else if (route.waypointsList.Length > 1)
                                {
                                    var wp = route.waypointsList[1];
                                    DrawArrow(scr.transform.position, wp.transform.position, !scr.setToStartWaypoint);
                                }
                            }
                        }
                    }
                }
            }

            private static void DrawArrow(Vector3 p1, Vector3 p2, bool dotted)
            {
                Vector3 line = (p2 - p1);
                var magnitude = line.magnitude;
                if (magnitude > arrowMagnitude)
                {
                    magnitude = magnitude - arrowMagnitude;
                    line.Normalize();
                    Vector3 endPos = p1 + line * magnitude;

                    if (dotted)
                        Handles.DrawDottedLine(p1, endPos, dottedLineSize);
                    else
                        Handles.DrawLine(p1, endPos);

#if UNITY_5_6_OR_NEWER
                    Handles.ArrowHandleCap(0, endPos, Quaternion.LookRotation(line, Vector3.up), arrowMagnitude, EventType.Repaint);
#else
                    Handles.ArrowCap(0, endPos, Quaternion.LookRotation(line, Vector3.up), arrowMagnitude);
#endif
                }
                else if (magnitude > Mathf.Epsilon)
                {
                    Handles.DrawLine(p1, p2);
                }
            }
        }
#endif // UNITY_EDITOR
    }

}