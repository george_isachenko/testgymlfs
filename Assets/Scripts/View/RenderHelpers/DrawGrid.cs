﻿using UnityEngine;
using UnityEngine.Assertions;
using System;

namespace View.RenderHelpers
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Render Helpers/Draw Grid")]
    [RequireComponent(typeof(LineSetRenderer))]
    public class DrawGrid : MonoBehaviour
    {
        #region Public fields.
        public int sizeX = 0;
        public int sizeY = 0;
        public float cellSize = 1.0f;

        public Color color = new Color();
        #endregion

        #region Private variables
        private LineSetRenderer lineSetRenderer;
        #endregion

        #region Unity API.
        void Awake()
        {
            lineSetRenderer = GetComponent<LineSetRenderer>();
            Assert.IsNotNull(lineSetRenderer);
        }

        void Start ()
        {
            UpdateLines();
            UpdateColor();
        }

        void OnEnable()
        {
            if (lineSetRenderer != null)
            {
                lineSetRenderer.enabled = true;
            }
        }

        void OnDisable()
        {
            if (lineSetRenderer != null)
            {
                lineSetRenderer.enabled = false;
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void OnValidate()
        {
            if (lineSetRenderer == null)
                lineSetRenderer = GetComponent<LineSetRenderer>();

            sizeX = Math.Max(1, sizeX);
            sizeY = Math.Max(1, sizeY);
            cellSize = Math.Max(Mathf.Epsilon, cellSize);

            UpdateLines();
            UpdateColor();
        }

        void Update ()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                UpdateLines();
                UpdateColor();
            }
        }
#endif
        #endregion

        #region Public API.
        public void SetSize (int sizeX, int sizeY)
        {
            if (this.sizeX != sizeX || this.sizeY != sizeY)
            {
                this.sizeX = sizeX;
                this.sizeY = sizeY;

                UpdateLines();
                UpdateColor();
            }
        }

        public void SetSize (int sizeX, int sizeY, float cellSize)
        {
            if (this.sizeX != sizeX || this.sizeY != sizeY || cellSize != this.cellSize)
            {
                this.cellSize = cellSize;
                this.sizeX = sizeX;
                this.sizeY = sizeY;

                UpdateLines();
                UpdateColor();
            }
        }

        public void Animate (Color fromColor, Color toColor, float time)
        {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);

            if (time > 0)
            {
                enabled = true;
                color = fromColor;
                LeanTween.cancel(gameObject);
                LeanTween.value(gameObject, fromColor, toColor, time).setOnUpdate(OnTweenUpdate).setOnComplete(OnTweenComplete);
            }
            else
            {
                color = toColor;
                enabled = (color.a > Mathf.Epsilon);
            }
            UpdateColor();
        }

        public void SetColor(Color color)
        {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);

            LeanTween.cancel(gameObject);
            this.color = color;
            enabled = (color.a > Mathf.Epsilon);
            UpdateColor();
        }
        #endregion

        #region Private methods.
        void OnTweenUpdate (Color color)
        {
            this.color = color;
            UpdateColor();
        }

        void OnTweenComplete ()
        {
            enabled = (color.a > Mathf.Epsilon);
        }

        void UpdateColor()
        {
            if (lineSetRenderer != null)
            {
                lineSetRenderer.materialPropertyBlock.SetColor("_Color", color);
            }
        }

        void UpdateLines()
        {
            if (lineSetRenderer != null)
            {
                if (sizeX > 0 && sizeY > 0 && cellSize > Mathf.Epsilon)
                {
                    var linesCount = sizeX + sizeY + 2;

                    lineSetRenderer.Reset(linesCount);

/*
                    float cellSizeX = 1.0f / (float)sizeX;
                    float cellSizeY = 1.0f / (float)sizeY;
*/

                    for (var i = 0; i <= sizeX; i++)
                    {
                        lineSetRenderer.AddLine(new Vector3(i * cellSize, 0.0f, 0.0f), new Vector3(i * cellSize, 0.0f, sizeY * cellSize)); 
                    }
//                     lineSetRenderer.AddLine(new Vector3(0.0f, 0.0f, 1.0f), new Vector3(1.0f, 0.0f, 1.0f));

                    for (var i = 0; i <= sizeY; i++)
                    {
                        lineSetRenderer.AddLine(new Vector3(0.0f, 0.0f, i * cellSize), new Vector3(sizeX * cellSize, 0.0f, i * cellSize));
                    }
//                     lineSetRenderer.AddLine(new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.0f, 1.0f));

                    lineSetRenderer.Finish();
                }
                else
                {
                    lineSetRenderer.Reset(0);
                }
            }
        }
        #endregion

/*
#if UNITY_EDITOR
        // Gizmo drawer
        public class Gizmo
        {
            [DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.NonSelected | GizmoType.NotInSelectionHierarchy | GizmoType.InSelectionHierarchy)]
            private static void DrawGizmo(DrawGrid scr, GizmoType gizmoType)
            {
                if (Camera.current != null && scr.lineSetRenderer != null)
                {
                    scr.lineSetRenderer.Draw(Camera.current);
                }
            }
        }
#endif
*/
    }
}