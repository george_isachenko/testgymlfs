﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace View.RenderHelpers
{
    [ExecuteInEditMode]
    [AddComponentMenu("Kingdom/View/Render Helpers/Line Set Renderer")]
    public class LineSetRenderer : MonoBehaviour
    {
        #region Static constants.
        public static readonly int initialReserve = 8; // In Quads.
        #endregion

        #region Public fields.
        public Material material;
        public float lineWidth = 1.0f;
        #endregion

        #region Private data.
        Mesh mesh;
        bool dirty;
        MaterialPropertyBlock materialPropertyBlock_;
        List<Vector3> vertices;
        List<Vector2> uvs;
        List<int> indices;
        #endregion

        #region Public properties.
        public MaterialPropertyBlock materialPropertyBlock
        {
            get
            {
                if (materialPropertyBlock_ == null)
                    materialPropertyBlock_ = new MaterialPropertyBlock();

                return materialPropertyBlock_;
            }
        }

        public bool isValid
        {
            get
            {
                return (vertices != null &&
                        vertices.Count > 0 &&
                        indices != null &&
                        indices.Count > 0 &&
                        uvs != null &&
                        uvs.Count > 0 &&
                        mesh != null &&
                        mesh.subMeshCount > 0);
            }
        }
        #endregion

        #region Unity API.
        void Awake()
        {
            mesh = new Mesh();
            mesh.MarkDynamic();
        }

        void Start ()
        {   // Declared to allow enabling/disabling in Inspector.
        }

        // NOTE! OnRenderObject() + Graphics.Draw() combination seems to be broken now on Android/iOS (but works in Editor),
        // but subscribing to camera onPreCull event works. Some mystic shit!
        // We using this method only when playing, in Editor when not playing old method used.
        void OnEnable ()
        {
            if (Application.isPlaying)
            {
                Camera.onPreCull += OnCameraPreRender;
            }
        }

        void OnDisable()
        {
            if (Application.isPlaying)
            {
                Camera.onPreCull -= OnCameraPreRender;
            }
        }

#if UNITY_EDITOR
        void OnRenderObject()
        {
            if (!Application.isPlaying)
            {
                Draw(Camera.main);
            }
        }
#endif
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void OnValidate()
        {
            if (lineWidth <= 0.0f)
                lineWidth = 1.0f;
        }

        private void OnDrawGizmos()
        {
            DrawImmediately();
        }
#endif
        #endregion

        #region Public API.
        public void Reset (int reserveQuads = 0)
        {
            if (mesh == null)
            {
                mesh = new Mesh();
                mesh.MarkDynamic();
            }

            if (reserveQuads <= initialReserve)
                reserveQuads = initialReserve;

            SetCapacities(reserveQuads);
            vertices.Clear();
            indices.Clear();
            uvs.Clear();
            dirty = true;
        }

        public void AddLine(Vector3 start, Vector3 end, float width = 0.0f)
        {
            var delta = end - start;

            if (delta.sqrMagnitude >= Mathf.Epsilon &&
                transform.localScale.x != 0.0f &&
                transform.localScale.y != 0.0f && 
                transform.localScale.z != 0.0f)
            {
                if (width <= 0.0f)
                    width = lineWidth;

                var w = width * 0.5f;

                if (vertices == null || indices == null || uvs == null)
                    Reset();

                var vertexIdx = vertices.Count;

                var deltaNorm = delta.normalized;
                var l = Vector3.Cross(delta, Vector3.up).normalized;

                // Fill vertices.
                vertices.Add (start - deltaNorm * w + l * w);
                vertices.Add (start - deltaNorm * w + l * -w);
                vertices.Add (end + deltaNorm * w + l * -w);
                vertices.Add (end + deltaNorm * w + l * w);

                // Fill uvs.
                uvs.Add (new Vector2(-1.0f, 0.0f));
                uvs.Add (new Vector2(1.0f, 0.0f));
                uvs.Add (new Vector2(1.0f, 0.0f));
                uvs.Add (new Vector2(-1.0f, 0.0f));

                // Fill indices.
                indices.Add (vertexIdx + 3);
                indices.Add (vertexIdx + 1);
                indices.Add (vertexIdx + 0);
                indices.Add (vertexIdx + 2);
                indices.Add (vertexIdx + 1);
                indices.Add (vertexIdx + 3);

                dirty = true;
            }
        }

        public void Finish()
        {
            if (dirty && mesh != null && indices != null && uvs != null && vertices != null && vertices.Count > 0 && indices.Count > 0 && uvs.Count > 0)
            {
                mesh.MarkDynamic();
                mesh.Clear(false);
                mesh.SetVertices(vertices);
                mesh.SetTriangles(indices, 0, true);
                mesh.SetUVs(0, uvs);
                //mesh.RecalculateNormals();
                mesh.UploadMeshData(false);

                dirty = false;
            }
        }

        public void ReserveMore (int newLinesCountReserve = 0)
        {
            var reserveQuads = (vertices == null ? 0 : vertices.Count / 4);

            if (newLinesCountReserve <= 0)
            {
                reserveQuads += Math.Max(1, (int)Math.Sqrt(reserveQuads));
            }
            else if (newLinesCountReserve <= reserveQuads)
            {
                return;
            }
            else
            {
                reserveQuads = newLinesCountReserve;
            }

            SetCapacities (reserveQuads);
        }
        #endregion

        #region Private methods.
        private void SetCapacities (int reserveQuads)
        {
            var vertCount = reserveQuads * 4;
            if (vertices == null)
            {
                vertices = new List<Vector3>(vertCount);
            }
            else if (vertCount > vertices.Capacity)
            {
                vertices.Capacity = vertCount;
            }

            if (uvs == null)
            {
                uvs = new List<Vector2>(vertCount);
            }
            else if (vertCount > uvs.Capacity)
            {
                uvs.Capacity = vertCount;
            }

            var indicesCount = reserveQuads * 6;
            if (indices == null)
            {
                indices = new List<int>(indicesCount);
            }
            else if (indicesCount > indices.Capacity)
            {
                indices.Capacity = indicesCount;
            }
        }

        private void OnCameraPreRender (Camera camera)
        {
            if (Application.isPlaying)
            {
                Draw(camera);
            }
        }

        private void Draw (Camera camera)
        {
            if (material != null)
            {
                if (dirty)
                    Finish();

                if (isValid)
                {
                    if (materialPropertyBlock_ != null)
                    {
                        Graphics.DrawMesh(mesh, transform.localToWorldMatrix, material, gameObject.layer, camera, 0, materialPropertyBlock_);
                    }
                    else
                    {
                        Graphics.DrawMesh(mesh, transform.localToWorldMatrix, material, gameObject.layer, camera);
                    }
                }
            }
        }

        private void DrawImmediately()
        {
            if (material != null)
            {
                if (dirty)
                    Finish();

                if (isValid)
                {
                    for (var i = 0; i < material.passCount; i++)
                    {
                        if (material.SetPass(i))
                        {
                            Graphics.DrawMeshNow(mesh, transform.localToWorldMatrix);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
