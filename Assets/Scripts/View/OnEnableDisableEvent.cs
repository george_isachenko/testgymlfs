﻿using UnityEngine;
using UnityEngine.Events;

namespace View
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/On Enable Disable Event")]
    public class OnEnableDisableEvent : MonoBehaviour
    {
        public UnityEvent onEnable;
        public UnityEvent onDisable;

        void OnEnable()
        {
            if (onEnable != null)
                onEnable.Invoke();
        }

        void OnDisable()
        {
            if (onDisable != null)
                onDisable.Invoke();
        }
    }
}