﻿using System;
using UnityEngine;
using MathHelpers;
using InspectorHelpers;
using Data.Room;
using View.Actors;
using View.RenderHelpers;
using View.EditorHelpers;
using System.Linq;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Rooms
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Room Conductor")]
    public class RoomConductor : MonoBehaviour
    {
        #region Static/readonly data.
        // private static readonly int equipmentActorsInitialCapacity = 128;
#if UNITY_EDITOR
        private static readonly Color gridOccupiedColor = new Color(1.0f, 0.3f, 0.3f, 0.66f);
        private static readonly Color gridFreeColor = new Color(0.3f, 1.0f, 0.3f, 0.66f);
        private static readonly Color normalColor = new Color(0.0f, 0.5f, 0.0f, 1.0f);
        private static readonly Color overlapColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        private static readonly Color wrongSizeColor = new Color(1.0f, 0.647f, 0.0f, 1.0f);
        private static readonly Color outsideRoomColor = new Color(1.0f, 0.27f, 0.0f, 1.0f);
        private static readonly Color maxSizeColor = new Color(0.695f, 0.132f, 0.132f, 0.75f);
        private static readonly Color initialSizeColor = new Color(0.0f, 0.5f, 0.0f, 0.75f);
#endif
        #endregion

        #region Properties.
        public bool gridActive
        {
            get
            {
                return drawGrid != null ? (drawGrid.gameObject.activeInHierarchy && drawGrid.enabled) : false;
            }
        }

        public Color gridColor
        {
            get
            {
                return drawGrid != null ? drawGrid.color : Color.white;
            }
        }

        public RoomCoordinates roomSize
        {
            get
            {
                return currentSize;
            }
        }

        public IRoomSetupDataInfo roomSetupDataInfo
        {
            get
            {
                return roomSetupData;
            }
        }

#if UNITY_EDITOR
        public RoomSetupData editorRoomSetupData
        {
            get
            {
                return roomSetupData;
            }
        }
#endif
        #endregion

        #region Public (Inspector) data.
        [SerializeField]
        private RoomSetupData roomSetupData = new RoomSetupData(); // Do not rename! Exposed via custom editor.

        [Tooltip("Optional. If None - GameObject on which RoomConductor is placed will be used.")]
        public GameObject roomObject;
        public CoordsMapper coordsMapper;
        [ReorderableList]
        public RoomZone[] zones;
        #endregion

        #region Private data.
        //      private Spiral2DIterator spiralIterator;
        private RoomCoordinates currentSize;
        private DrawGrid drawGrid;
        private OccupationBitMap occupationMap;
        private OccupationBitMap wallOccupationMap;
        private OccupationBitMap unlockedCellsMap;
        #endregion

        #region Editor-only data.
#if UNITY_EDITOR
        private bool dbgShowOccupationMap = false;
        private bool dbgShowUnlockedCellsMap = false;
#endif
        #endregion

        #region Unity API.
        private void Awake()
        {
            if (roomObject == null)
                roomObject = transform.gameObject;

            if (drawGrid == null)
                drawGrid = roomObject.GetComponentInChildren<DrawGrid>();

            coordsMapper.Update(true);

            if (roomSetupData == null)
                roomSetupData = new RoomSetupData();

            currentSize = roomSetupDataInfo.initialSizeInCells;
            var maxSizeInCells = roomSetupDataInfo.maxSizeInCells;
            occupationMap = new OccupationBitMap(maxSizeInCells.x, maxSizeInCells.y);
            wallOccupationMap = new OccupationBitMap(maxSizeInCells.x, maxSizeInCells.y);
            unlockedCellsMap = new OccupationBitMap(maxSizeInCells.x, maxSizeInCells.y);
        }

        private void Start()
        {
            if (drawGrid != null)
            {
                if (Application.isPlaying)
                    drawGrid.gameObject.SetActive(false);
            }

            // Give inactivated in editor static equipment chance to auto-init. When game start it can deactivate them later again.
            var equipmentActors = roomObject.GetComponentsInChildren<IEquipmentActor>(true);
            if (equipmentActors != null)
            {
                foreach (var actor in equipmentActors)
                {
                    actor.gameObject.SetActive(true);
                }
            }
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                UpdateEquipmentCoords();
            }
        }
#endif
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        private void OnValidate()
        {
            if (roomSetupData == null)
                roomSetupData = new RoomSetupData();

            roomSetupData.Validate();

            if (!Application.isPlaying && gameObject.scene.IsValid() && gameObject.scene.isLoaded)
            {
                if (roomObject == null)
                    roomObject = transform.gameObject;

                drawGrid = roomObject.GetComponentInChildren<DrawGrid>();

                coordsMapper.Update(true);

                currentSize = roomSetupDataInfo.initialSizeInCells;

                var maxSizeInCells = roomSetupDataInfo.maxSizeInCells;
                occupationMap.Resize(maxSizeInCells.x, maxSizeInCells.y);

                UpdateOccupationMap();
                UpdateEquipmentCoords();
                UpdateGrid();

                if (drawGrid != null)
                    drawGrid.gameObject.SetActive(true);

                var roomView = gameObject.GetComponentInSelfOrParents<IRoomView>();
                if (roomView != null)
                {
                    roomView.EditorUpdate();
                }
            }
        }

        private void OnDrawGizmos()
        {
            bool selected = (Selection.Contains(roomObject) || Selection.Contains(gameObject));
            DrawActorsGizmos(selected);

            if (selected && roomSetupData != null)
            {
                var initialSizeInCells = roomSetupDataInfo.initialSizeInCells;
                var maxSizeInCells = roomSetupDataInfo.maxSizeInCells;

                if (initialSizeInCells.x > 0 && initialSizeInCells.y > 0)
                {
                    Gizmos.color = initialSizeColor;
                    Gizmos.DrawWireCube
                        ( roomObject.transform.position + coordsMapper.ConvertPositionFromLogic
                            ( new Vector3(initialSizeInCells.x  * 0.5f, 0.25f, initialSizeInCells.y * 0.5f))
                        , coordsMapper.ConvertPositionFromLogic
                            ( new Vector3(initialSizeInCells.x, 0.5f, initialSizeInCells.y)));
                }

                if (maxSizeInCells.x > 0 && maxSizeInCells.y > 0)
                {
                    Gizmos.color = maxSizeColor;
                    Gizmos.DrawWireCube
                        ( roomObject.transform.position + coordsMapper.ConvertPositionFromLogic
                            ( new Vector3(maxSizeInCells.x  * 0.5f, 0.25f, maxSizeInCells.y * 0.5f))
                        , coordsMapper.ConvertPositionFromLogic
                            ( new Vector3(maxSizeInCells.x, 0.5f, maxSizeInCells.y)));
                }

                /*
                                int x, y;
                                if (spiralIterator != null && spiralIterator.Next(out x, out y))
                                {
                                    int realX = x + currentSize.x / 2;
                                    int realY = y + currentSize.y / 2;
                                    Assert.IsTrue(realX >= 0);
                                    Assert.IsTrue(realX < currentSize.x);
                                    Assert.IsTrue(realY >= 0);
                                    Assert.IsTrue(realY < currentSize.y);

                                    Gizmos.color = new Color(1.0f, 1.0f, 0.0f, 1.0f);
                                    Gizmos.DrawSphere(roomObject.transform.position + ConvertPositionFromCellCooordinates(new RoomCoordinates(realX, realY)), 0.05f);
                                }
                                else
                                {
                                    spiralIterator = new Spiral2DIterator((uint)currentSize.x, (uint)currentSize.y);
                                }
                */
            }

            if (dbgShowOccupationMap)
            {
                var size = coordsMapper.AlignPositionFromCellCooordinates(new RoomCoordinates(1, 1), new RoomCoordinates(1, 1)) * 0.5f;
                size.y = 0.01f;
                for (int x = 0; x < currentSize.x; x++)
                {
                    for (int y = 0; y < currentSize.y; y++)
                    {
                        Vector3 position = coordsMapper.AlignPositionFromCellCooordinates(new RoomCoordinates(x, y), new RoomCoordinates(1, 1));
                        //Handles.Label(position + roomObject.transform.position, occupationMap.MaxOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        //Handles.Label(position + roomObject.transform.position, occupationMap.IsOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        Gizmos.color = occupationMap.IsOccupied(x, y, 1, 1) ? gridOccupiedColor : gridFreeColor;
                        Gizmos.DrawCube(position + roomObject.transform.position, size);
                    }
                }

                size.y = 0.45f;
                for (int x = 0; x < currentSize.x; x++)
                {
                    for (int y = 0; y < currentSize.y; y++)
                    {
                        Vector3 position = coordsMapper.AlignPositionFromCellCooordinates(new RoomCoordinates(x, y), new RoomCoordinates(1, 1));
                        //Handles.Label(position + roomObject.transform.position, occupationMap.MaxOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        //Handles.Label(position + roomObject.transform.position, occupationMap.IsOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        if (wallOccupationMap.IsOccupied(x, y, 1, 1))
                        {
                            Gizmos.color = gridOccupiedColor;

                            Gizmos.DrawCube(position + roomObject.transform.position + new Vector3(0.0f, 0.25f, 0.0f), size);
                        }
                    }
                }
            }

            if (dbgShowUnlockedCellsMap)
            {
                var size = coordsMapper.AlignPositionFromCellCooordinates(new RoomCoordinates(1, 1), new RoomCoordinates(1, 1)) * 0.5f;
                size.y = 0.01f;
                for (int x = 0; x < currentSize.x; x++)
                {
                    for (int y = 0; y < currentSize.y; y++)
                    {
                        Vector3 position = coordsMapper.AlignPositionFromCellCooordinates(new RoomCoordinates(x, y), new RoomCoordinates(1, 1));
                        //Handles.Label(position + roomObject.transform.position, occupationMap.MaxOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        //Handles.Label(position + roomObject.transform.position, occupationMap.IsOccupied(x, y, 1, 1).ToString(), GetOverheadLabelStyle());
                        Gizmos.color = unlockedCellsMap.IsOccupied(x, y, 1, 1) ? gridOccupiedColor : gridFreeColor;
                        Gizmos.DrawCube(position + roomObject.transform.position + new Vector3(0.0f, 0.025f, 0.0f), size);
                    }
                }
            }
        }

        public void ToggleDbgShowOccupationMap()
        {
            dbgShowOccupationMap = !dbgShowOccupationMap;
        }

        public void ToggleDbgShowUnlockedCellsMap()
        {
            dbgShowUnlockedCellsMap = !dbgShowUnlockedCellsMap;
        }
#endif
        #endregion

        #region Public API.
        public void SetCurrentSize(RoomCoordinates size)
        {
            currentSize = size;
            UpdateGrid();
        }

        public PlacementResult AddEquipmentToOccupationMap(IEquipmentActor actor)
        {
            var position = actor.placement.position;
            var orientedSize = actor.orientedSize;
            var occupiesFloorSpace = actor.occupiesFloorSpace;
            var wallMounted = actor.wallMounted;

            if (!occupiesFloorSpace && !wallMounted)
                return new PlacementResult(PlacementResult.State.NoOccupation);    // 'Shallow' actor that doesn't occupy anything.

            if (occupiesFloorSpace && occupationMap.IsOccupied(position.x, position.y, orientedSize.x, orientedSize.y))
                return new PlacementResult(PlacementResult.State.Invalid); 

            var wallPositionWithMargin = actor.wallPositionWithMargin;
            var wallOrientedSizeWithMargin = actor.orientedWallSizeWithMargin;

            if (wallMounted && wallOccupationMap.IsOccupied(wallPositionWithMargin.x, wallPositionWithMargin.y, wallOrientedSizeWithMargin.x, wallOrientedSizeWithMargin.y))
                return new PlacementResult(PlacementResult.State.Invalid); 

            var result = new PlacementResult
                (
                    (occupiesFloorSpace && wallMounted)
                    ? PlacementResult.State.OccupiesFloorAndWall
                    : (occupiesFloorSpace
                        ? PlacementResult.State.OccupiesFloor
                        : PlacementResult.State.OccupiesWall)
                );

            if (occupiesFloorSpace)
            {
                occupationMap.Set (position.x, position.y, orientedSize.x, orientedSize.y, true);
                result.occupationMapPosition = position;
                result.occupationMapOrientedSize = orientedSize;
            }

            if (wallMounted)
            {
                var wallPosition = actor.wallPosition;
                var wallOrientedSize = actor.orientedWallSize;

                wallOccupationMap.Set (wallPosition.x, wallPosition.y, wallOrientedSize.x, wallOrientedSize.y, true);
                result.wallOccupationMapPosition = wallPosition;
                result.wallOccupationMapOrientedSize = wallOrientedSize;
            }

            return result;
        }

        public void RemoveEquipmentFromOccupationMap(ref PlacementResult placementResult)
        {
            if (placementResult.state == PlacementResult.State.OccupiesFloor || placementResult.state == PlacementResult.State.OccupiesFloorAndWall)
            {
                var coords = placementResult.occupationMapPosition;
                var orientedSize = placementResult.occupationMapOrientedSize;

                occupationMap.Set(coords.x, coords.y, orientedSize.x, orientedSize.y, false);
            }

            if (placementResult.state == PlacementResult.State.OccupiesWall || placementResult.state == PlacementResult.State.OccupiesFloorAndWall)
            {
                var wallPosition = placementResult.wallOccupationMapPosition;
                var wallOrientedSize = placementResult.wallOccupationMapOrientedSize;

                wallOccupationMap.Set (wallPosition.x, wallPosition.y, wallOrientedSize.x, wallOrientedSize.y, false);
            }

            placementResult.Reset();
        }

        public PlacementResult CheckOccupationMap(IEquipmentActor actor, bool alsoCheckConstraints = true)
        {
            var position = actor.placement.position;
            var orientedSize = actor.orientedSize;
            var occupiesFloorSpace = actor.occupiesFloorSpace;
            var wallMounted = actor.wallMounted;

            if (alsoCheckConstraints && !position.IsInConstraints(orientedSize, currentSize))
                return new PlacementResult(PlacementResult.State.Invalid); 

            if (alsoCheckConstraints && !CheckUnlockedCellsMap(position, orientedSize))
                return new PlacementResult(PlacementResult.State.Invalid); 
                
            if (!occupiesFloorSpace && !wallMounted)
                return new PlacementResult(PlacementResult.State.NoOccupation);    // 'Shallow' actor that doesn't occupy anything.

            if (occupiesFloorSpace && occupationMap.IsOccupied(position.x, position.y, orientedSize.x, orientedSize.y))
                return new PlacementResult(PlacementResult.State.Invalid); 

            var wallPositionWithMargin = actor.wallPositionWithMargin;
            var wallOrientedSizeWithMargin = actor.orientedWallSizeWithMargin;

            if (wallMounted && wallOccupationMap.IsOccupied(wallPositionWithMargin.x, wallPositionWithMargin.y, wallOrientedSizeWithMargin.x, wallOrientedSizeWithMargin.y))
                return new PlacementResult(PlacementResult.State.Invalid); 

            return new PlacementResult
            (
                (occupiesFloorSpace && wallMounted)
                ? PlacementResult.State.OccupiesFloorAndWall
                : (occupiesFloorSpace
                    ? PlacementResult.State.OccupiesFloor
                    : PlacementResult.State.OccupiesWall)
            );
        }

        public void ResetUnlockedCellsMap()
        {
            var maxSizeInCells = roomSetupDataInfo.maxSizeInCells;
            if (unlockedCellsMap.sizeX != maxSizeInCells.x || unlockedCellsMap.sizeY != maxSizeInCells.y)
            {
                unlockedCellsMap.Resize(maxSizeInCells.x, maxSizeInCells.y);
            }

            unlockedCellsMap.Reset();
        }

        public void SetUnlockedCellsMap(RoomCoordinates origin, RoomCoordinates size)
        {
            unlockedCellsMap.Set(origin.x, origin.y, size.x, size.y, true);
        }

        public bool CheckUnlockedCellsMap(RoomCoordinates origin, RoomCoordinates orientedSize)
        {
            return ! unlockedCellsMap.IsOccupied (origin.x, origin.y, orientedSize.x, orientedSize.y);
        }

        public EquipmentPlacement FindFreeSpace (RoomCoordinates startingPosition, RoomCoordinates objectSize)
        {
            if (objectSize.isValidSize)
            {
                // var isSquare = (objectSize.x == objectSize.y);

                // Clamp starting position to bounds.
                startingPosition = new RoomCoordinates
                    ( Math.Max(0, Math.Min(currentSize.x - 1, startingPosition.x))
                    , Math.Max(0, Math.Min(currentSize.y - 1, startingPosition.y)) );
       
                // Search size should cover max possible size, even when starting from corner (worst case, 2x size or currentSize).
                var searchSize = new RoomCoordinates
                    ( Math.Max(currentSize.x - startingPosition.x, startingPosition.x) * 2
                    , Math.Max(currentSize.y - startingPosition.y, startingPosition.y) * 2);

                foreach (var coords in Spiral2DIterator.Iterate(searchSize.x, searchSize.y))
                {
                    var newPosition = new RoomCoordinates(coords.x + startingPosition.x, coords.y + startingPosition.y);

                    // Check that it fits bounds.
                    if (newPosition.x >= 0 && newPosition.y >= 0)
                    {
                        if ((newPosition.x + objectSize.x <= currentSize.x && newPosition.y + objectSize.y <= currentSize.y) &&
                            !occupationMap.IsOccupied(newPosition.x, newPosition.y, objectSize.x, objectSize.y) && 
                            !unlockedCellsMap.IsOccupied (newPosition.x, newPosition.y, objectSize.x, objectSize.y))
                        {
                            return new EquipmentPlacement(newPosition, 0);
                        }
                        else if (objectSize.x != objectSize.y)
                        {   // Try rotated position but only if object is not square.
                            var sizeRotated = objectSize.sizeRotated;
                            if  ((newPosition.x + sizeRotated.x <= currentSize.x && newPosition.y + sizeRotated.y <= currentSize.y) &&
                                !occupationMap.IsOccupied(newPosition.x, newPosition.y, sizeRotated.x, sizeRotated.y) &&
                                !unlockedCellsMap.IsOccupied (newPosition.x, newPosition.y, sizeRotated.x, sizeRotated.y))
                            {
                                return new EquipmentPlacement(newPosition, 1);
                            }
                        }
                    }
                }
            }

            // Cannot find free space or equipment has zero size - just drop at center!
            // return new EquipmentPlacement(new RoomCoordinates(currentSize.x / 2, currentSize.y / 2), 0);

            // Cannot find free space or equipment has zero size - return invalid position.
            return new EquipmentPlacement(new RoomCoordinates(-1, -1), 0);
        }

        public EquipmentPlacement FindFreeSpaceOnWall (RoomCoordinates size, RoomCoordinates wallSize, RoomCoordinates wallSizeWithMargin, bool occupiesFloorSpace)
        {
            EquipmentPlacement placement = new EquipmentPlacement();

            var marginDelta = (wallSizeWithMargin.x - wallSize.x) / 2;

            // Check heading = 0 [0 .. maxX; 0].
            {
                bool noMargin = true;
                for (int x = 0; x < currentSize.x - wallSize.x; x++)
                {
                    if (!wallOccupationMap.IsOccupied(x, 0, wallSizeWithMargin.x, wallSizeWithMargin.y))
                    {
                        if (occupiesFloorSpace && occupationMap.IsOccupied(x, 0, size.x, size.y))
                        {
                            noMargin = true;
                            continue;
                        }

                        placement.position.x = x + (noMargin ? 0 : marginDelta);
                        return placement;
                    }
                    else
                        noMargin = false;
                }
            }

            var orientedSize = size.GetOrientedSize(1);
            var orientedWallSizeWithMargin = wallSizeWithMargin.GetOrientedSize(1);

            // Check heading = 1 [0, 0 .. maxY].
            {
                bool noMargin = true;
                for (int y = 0; y < currentSize.y - wallSize.y; y++)
                {
                    if (!wallOccupationMap.IsOccupied(0, y, orientedWallSizeWithMargin.x, orientedWallSizeWithMargin.y))
                    {
                        if (occupiesFloorSpace && occupationMap.IsOccupied(0, y, orientedSize.x, orientedSize.y))
                        {
                            noMargin = true;
                            continue;
                        }

                        placement.position.y = y + (noMargin ? 0 : marginDelta);
                        placement.heading = 1;
                        return placement;
                    }
                    else
                        noMargin = false;
                }
            }

            var originWall = currentSize.y - wallSize.y;
            var originFloor = currentSize.y - size.y;

            // Check heading = 2 [0 .. maxX; 0].
            {
                bool noMargin = true;
                for (int x = 0; x < currentSize.x - wallSize.x; x++)
                {
                    if (!wallOccupationMap.IsOccupied(x, originWall, wallSizeWithMargin.x, wallSizeWithMargin.y))
                    {
                        if (occupiesFloorSpace && occupationMap.IsOccupied(x, originFloor, size.x, size.y))
                        {
                            noMargin = true;
                            continue;
                        }

                        placement.position.x = x + (noMargin ? 0 : marginDelta);
                        placement.position.y = originFloor;
                        placement.heading = 2;
                        return placement;
                    }
                    else
                        noMargin = false;
                }
            }

            // Check heading = 3 [0, 0 .. maxY].
            {
                bool noMargin = true;
                for (int y = 0; y < currentSize.y - wallSize.y; y++)
                {
                    if (!wallOccupationMap.IsOccupied(originWall, y, orientedWallSizeWithMargin.x, orientedWallSizeWithMargin.y))
                    {
                        if (occupiesFloorSpace && occupationMap.IsOccupied(originFloor, y, orientedSize.x, orientedSize.y))
                        {
                            noMargin = true;
                            continue;
                        }

                        placement.position.x = originFloor;
                        placement.position.y = y + (noMargin ? 0 : marginDelta);
                        placement.heading = 3;
                        return placement;
                    }
                    else
                        noMargin = false;
                }
            }

            // Cannot find free space or equipment has zero size - just drop at zero coordinates!
            // placement.position = new RoomCoordinates(0, 0);

            // Cannot find free space or equipment has zero size - return invalid position.
            placement.position = new RoomCoordinates(-1, -1);

            return placement;
        }

        public bool ClampToContraints
            ( RoomCoordinates position
            , RoomCoordinates size
            , bool wallMounted
            , out RoomCoordinates result
            , ref int heading)
        {
            var orientedSize = size.GetOrientedSize(heading);

            if (!position.IsInConstraints(size, currentSize))
            {
                result = default(RoomCoordinates);
                return false;
            }

            if (!CheckUnlockedCellsMap(position, orientedSize))
            {
                result = default(RoomCoordinates);
                return false;
            }

            if (wallMounted)
            {
                var maxWall = new RoomCoordinates(currentSize.x - orientedSize.x, currentSize.y - orientedSize.y);

                // First, try to keep it on the same wall. This solves potential conflicts in corners.
                if ((heading == 0 && position.y <= 0) ||
                    (heading == 1 && position.x <= 0) ||
                    (heading == 2 && position.y >= maxWall.y) ||
                    (heading == 3 && position.x >= maxWall.x))
                {
                    result = position.ClampToContraints(orientedSize, currentSize);
                    return true;
                }

                // Now try to stick it to wall by changing heading, if needed. It uses more relaxed
                // conditions with additional 1-cell margin for better sticking.
                if (position.y <= 1)
                {
                    heading = 0;
                    position.y = 0;
                    result = position.ClampToContraints(size.GetOrientedSize(heading), currentSize);
                    return true;
                }
                else if (position.x <= 1)
                {
                    heading = 1;
                    position.x = 0;
                    result = position.ClampToContraints(size.GetOrientedSize(heading), currentSize);
                    return true;
                }
                else if (position.y >= (currentSize.y - size.y - 1))
                {
                    heading = 2;
                    position.y = currentSize.y - size.y;
                    result = position.ClampToContraints(size.GetOrientedSize(heading), currentSize);
                    return true;
                }
                else if (position.x >= (currentSize.x - size.y - 1))
                {
                    heading = 3;
                    position.x = currentSize.x - size.y;
                    result = position.ClampToContraints(size.GetOrientedSize(heading), currentSize);
                    return true;
                }

                result = default(RoomCoordinates);
                return false;  // Return original placement.
            }
            else
            {
                result = position.ClampToContraints(orientedSize, currentSize);
                return true;
            }
        }

        public void AnimateGrid (Color fromColor, Color toColor, float time)
        {
            if (drawGrid != null)
                drawGrid.Animate (fromColor, toColor, time);
        }

        public void SetGridColor (Color color)
        {
            if (drawGrid != null)
            {
                drawGrid.SetColor(color);
            }
        }

        public bool GetZoneCenter(string zoneName, out Vector3 position)
        {
            foreach (var zone in zones)
            {
                if (zone.name == zoneName)
                {
                    var point = zone.position;
                    position = transform.TransformPoint(new Vector3(point.x, 0.0f, point.y));
                    return true;
                }
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                position = transform.TransformPoint ( new Vector3
                    ( coordsMapper.cellSize * currentSize.x * 0.5f
                    , 0.0f
                    , coordsMapper.cellSize * currentSize.y * 0.5f));
                return true;
            }

            position = default(Vector3);
            return false;
        }

        public bool GetZoneCenterLocal(string zoneName, out Vector3 position)
        {
            var zone = zones.Where(x => x.name == zoneName).FirstOrDefault();
            if (zone != null)
            {
                var point = zone.position;
                position = new Vector3(point.x, 0.0f, point.y);
                return true;
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                position = new Vector3
                    ( coordsMapper.cellSize * currentSize.x * 0.5f
                    , 0.0f
                    , coordsMapper.cellSize * currentSize.y * 0.5f);
                return true;
            }

            position = default(Vector3);
            return false;
        }

        public bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position)
        {
            var zonesList = zones.Where(x => x.name == zoneName);
            var zonesCount = zonesList.Count();
            if (zonesCount > 0)
            {
                var zone = (zonesCount > 1) ? zonesList.ElementAt(UnityEngine.Random.Range(0, zonesCount)) : zonesList.First();

                position = transform.TransformPoint(zone.GenerateRandomPointInsideZone());
                return true;
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                position = transform.TransformPoint ( new Vector3
                    ( UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.x)
                    , 0.0f
                    , UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.y)));
                return true;
            }

            position = default(Vector3);
            return false;
        }

        public bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition)
        {
            var zonesList = zones.Where(x => x.name == zoneName);
            var zonesCount = zonesList.Count();
            if (zonesCount > 0)
            {
                var zone = (zonesCount > 1) ? zonesList.ElementAt(UnityEngine.Random.Range(0, zonesCount)) : zonesList.First();

                localPosition = zone.GenerateRandomPointInsideZone();
                return true;
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                localPosition = new Vector3
                    ( UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.x)
                    , 0.0f
                    , UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.y));
                return true;
            }

            localPosition = default(Vector3);
            return false;
        }

        public bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position, out Quaternion rotation)
        {
            var zonesList = zones.Where(x => x.name == zoneName);
            var zonesCount = zonesList.Count();
            if (zonesCount > 0)
            {
                var zone = (zonesCount > 1) ? zonesList.ElementAt(UnityEngine.Random.Range(0, zonesCount)) : zonesList.First();

                float heading;
                var point = zone.GenerateRandomPointInsideZone(out heading);
                rotation = Quaternion.AngleAxis(heading, Vector3.up);
                position = transform.TransformPoint(point);
                return true;
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                position = transform.TransformPoint ( new Vector3
                    ( UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.x)
                    , 0.0f
                    , UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.y)));
                rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360.0f - Mathf.Epsilon), Vector3.up);
                return true;
            }

            position = default(Vector3);
            rotation = default(Quaternion);
            return false;
        }

        public bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition, out Quaternion rotation)
        {
            var zonesList = zones.Where(x => x.name == zoneName);
            var zonesCount = zonesList.Count();
            if (zonesCount > 0)
            {
                var zone = (zonesCount > 1) ? zonesList.ElementAt(UnityEngine.Random.Range(0, zonesCount)) : zonesList.First();

                float heading;
                localPosition = zone.GenerateRandomPointInsideZone(out heading);
                rotation = Quaternion.AngleAxis(heading, Vector3.up);
                return true;
            }

            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                localPosition = new Vector3
                    ( UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.x)
                    , 0.0f
                    , UnityEngine.Random.Range(0, coordsMapper.cellSize * currentSize.y));
                rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360.0f - Mathf.Epsilon), Vector3.up);
                return true;
            }

            localPosition = default(Vector3);
            rotation = default(Quaternion);
            return false;
        }
        #endregion

#if UNITY_EDITOR
        public int EditorGetStateHashCode()
        {
            int hashCode = 0;

            hashCode ^= (roomSetupData != null ? roomSetupData.EditorGetStateHashCode() : 0);
            hashCode ^= (roomObject != null ? roomObject.GetInstanceID() : 0);
            hashCode ^= coordsMapper.EditorGetStateHashCode();
            // Zones are not essential for this state.

            return hashCode;
        }

        public void UpdateOccupationMap()
        {
            var initialSizeInCells = roomSetupDataInfo.initialSizeInCells;
            var maxSizeInCells = roomSetupDataInfo.maxSizeInCells;

            if (occupationMap.sizeX != maxSizeInCells.x || occupationMap.sizeY != maxSizeInCells.y)
            {
                var childCount = roomObject.transform.childCount;
                if (occupationMap.sizeX > 0 && occupationMap.sizeY > 0)
                {
                    for (var i = 0; i < childCount; i++)
                    {
                        var child = roomObject.transform.GetChild(i);
                        var iEquipmentActor = child.GetComponent<IEquipmentActor>();
                        if (iEquipmentActor != null && iEquipmentActor.size.isValidSize)
                        {
                            iEquipmentActor.ResetPlacement();
                        }
                    }
                }

                occupationMap.Resize(maxSizeInCells.x, maxSizeInCells.y);
                occupationMap.Reset();
                wallOccupationMap.Resize(maxSizeInCells.x, maxSizeInCells.y);
                wallOccupationMap.Reset();

                for (var i = 0; i < childCount; i++)
                {
                    var child = roomObject.transform.GetChild(i);
                    var iEquipmentActor = child.GetComponent<IEquipmentActor>();
                    if (iEquipmentActor != null && iEquipmentActor.size.isValidSize)
                    {
                        iEquipmentActor.SetRoomConductor ( this );
                        iEquipmentActor.SetPlacement ( iEquipmentActor.placement);
                    }
                }
            }
        }
#endif

#if UNITY_EDITOR
        private void UpdateEquipmentCoords()
        {
//          bool someTransformChanged = false;

            var childCount = roomObject.transform.childCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = roomObject.transform.GetChild(i);
                var actor = child.GetComponent<IEquipmentActor>();
                if (actor != null)
                {
                    if (child.hasChanged)
                    {
                        actor.SetRoomConductor ( this );
                        var size = actor.size;
                        if (size.isValidSize)
                        {
                            var placement = actor.CalculateCurrentPlacement();
                            // Debug.LogFormat(">>> CALCULATED PLACEMENT: ({0}) = {1}", actor.gameObject.name, placement);
                            actor.SetPlacement(placement);
                        }

                        child.hasChanged = false;
//                      someTransformChanged = true;
                    }
                }
            }

/*
            if (someTransformChanged)
            {
                UpdateOccupationMap();
            }
*/
        }
#endif // #if UNITY_EDITOR

        private void UpdateGrid()
        {
            if (drawGrid != null)
            {
                // var gridSize = coordsMapper.ConvertPosition2DFromRoomCooordinates(new RoomCoordinates(currentSize.x, currentSize.y));

                //drawGrid.transform.localScale = new Vector3(gridSize.x, 1.0f, gridSize.y);
                drawGrid.SetSize(currentSize.x, currentSize.y, coordsMapper.cellSize);
            }
        }

        /*
                private void UpdateOverlappingActorsFlags()
                {
                    for (var i = 0; i < equipmentInfoList.Count; i++)
                    {
                        var actorInfo1 = equipmentInfoList[i];
                        actorInfo1.overlaps = false;
                        equipmentInfoList[i] = actorInfo1;
                    }

                    for (var i = 0; i < equipmentInfoList.Count; i++)
                    {
                        var actorInfo1 = equipmentInfoList[i];
                        for (var j = i + 1; j < equipmentInfoList.Count; j++)
                        {
                            var actorInfo2 = equipmentInfoList[j];

                            if (actorInfo1.validBounds &&
                                actorInfo2.validBounds &&
                                actorInfo1.worldBounds.Intersects(actorInfo2.worldBounds) )
                            {
                                actorInfo1.overlaps = true;
                                actorInfo2.overlaps = true;

                                equipmentInfoList[j] = actorInfo2;
                                equipmentInfoList[i] = actorInfo1;
                            }
                        }
                    }
                }
        */

#if UNITY_EDITOR
        private void DrawActorsGizmos(bool drawAll)
        {
            var originalColor = Gizmos.color;

            var childCount = roomObject.transform.childCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = roomObject.transform.GetChild(i);
                var iEquipmentActor = child.GetComponent<IEquipmentActor>();
                if (iEquipmentActor != null)
                {
                    var placement = iEquipmentActor.placement;
                    bool selected = iEquipmentActor.editorSelected;
                    var orientedSize = iEquipmentActor.orientedSize;
                    var position3D = roomObject.transform.position + coordsMapper.AlignPositionFromCellCooordinates(placement.position, orientedSize);

                    if (!orientedSize.isValidSize)
                    {
                        var bounds = iEquipmentActor.visibleBounds;
                        if (bounds.size.sqrMagnitude > Mathf.Epsilon)
                        {
                            Gizmos.color = wrongSizeColor;
                            Gizmos.DrawWireCube(bounds.center, bounds.size);
                        }
                    }
                    else if (!placement.position.IsInConstraints(orientedSize, currentSize))
                    {
                        var worldBounds = CalculateWorldBounds(orientedSize, position3D);
                        Gizmos.color = outsideRoomColor;
                        Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                    }
                    else if (!iEquipmentActor.placementState.isValid)
                    {
                        var worldBounds = CalculateWorldBounds(orientedSize, position3D);
                        Gizmos.color = overlapColor;
                        Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                    }
                    else if (drawAll || selected)
                    {
                        var worldBounds = CalculateWorldBounds(orientedSize, position3D);
                        Gizmos.color = normalColor;
                        Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                    }

                    if (iEquipmentActor.wallMounted && orientedSize.isValidSize)
                    {
                        var wallPositionWithMargin = iEquipmentActor.wallPositionWithMargin;
                        var wallOrientedSizeWithMargin = iEquipmentActor.orientedWallSizeWithMargin;
                        var wallPosition3D = roomObject.transform.position + coordsMapper.AlignPositionFromCellCooordinates(wallPositionWithMargin, wallOrientedSizeWithMargin);

                        if (!placement.position.IsInConstraints(orientedSize, currentSize))
                        {
                            var worldBounds = CalculateWorldBounds(wallOrientedSizeWithMargin, wallPosition3D, true);
                            Gizmos.color = outsideRoomColor;
                            Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                        }
                        else if (!iEquipmentActor.placementState.isValid)
                        {
                            var worldBounds = CalculateWorldBounds(wallOrientedSizeWithMargin, wallPosition3D, true);
                            Gizmos.color = overlapColor;
                            Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                        }
                        else if (drawAll || selected)
                        {
                            var worldBounds = CalculateWorldBounds(wallOrientedSizeWithMargin, wallPosition3D, true);
                            Gizmos.color = normalColor;
                            Gizmos.DrawWireCube(worldBounds.center, worldBounds.size);
                        }
                    }

                    if (orientedSize.isValidSize && selected)
                    {
                        //Handles.color = style.normal.textColor;
                        //var worldBounds = CalculateWorldBounds(orientedSize, position);
                        Vector3 labelPosition = roomObject.transform.position + coordsMapper.ConvertPositionFromRoomCooordinates(placement.position);
                        //labelPosition.y = worldBounds.size.y;
                        Handles.Label(labelPosition, string.Format("{0}:{1}", placement.position.x, placement.position.y), EditorGizmoHelper.instance.GetOverheadLabelStyle());
                    }
                }
            }

            Gizmos.color = originalColor;
        }

        private Bounds CalculateWorldBounds(RoomCoordinates orientedSize, Vector3 worldPosition, bool wallMounted = false)
        {
            if (orientedSize.isValidSize)
            {
                var center = worldPosition;
                center.y += wallMounted ? 0.25f : 0.25f;
                var size = coordsMapper.ConvertPositionFromRoomCooordinates(orientedSize);
                size.y = wallMounted ? 0.25f : 0.5f;
                if (size.x < 0)
                    size.x = -size.x;
                //size.x -= Mathf.Abs(roomCoordsScale.x * 0.01f);
                if (size.z < 0)
                    size.z = -size.z;
                //size.z -= Mathf.Abs(roomCoordsScale.z * 0.01f);
                return new Bounds(center, size);
            }
            else
            {
                return new Bounds();
            }
        }
#endif // #if UNITY_EDITOR
    }
}
