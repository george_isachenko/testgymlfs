﻿using CustomAssets;
using InspectorHelpers;
using UnityEngine;
using View.Actions;

namespace View.Rooms.LinearExpandRoom
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Linear Expand Room/Linear Expand Toggle")]
    public class LinearExpandToggle : UnlockToggleBase
    {
        #region Inspector fields
        [SerializeField]
        [ReorderableList]
        protected Mode[] expandLevelsModes;
        #endregion

        #region Public API
        public void SetToggleState 
            ( int expandLevel
            , bool active
            , string inactiveMaterialReplacementPolicyName
            , MaterialsReplacementPolicy materialsReplacementPolicy = null)
        {
            if (expandLevelsModes != null && expandLevelsModes.Length > 0)
            {
                var mode = expandLevelsModes[(expandLevel < expandLevelsModes.Length) ? expandLevel : expandLevelsModes.Length - 1];

                SetMode ( mode, active, inactiveMaterialReplacementPolicyName, materialsReplacementPolicy);
            }
        }
        #endregion

        #region Editor-only Public API
#if UNITY_EDITOR
        public int EditorGetExpandModesLength()
        {
            return expandLevelsModes.Length;
        }
#endif
        #endregion
    }
}