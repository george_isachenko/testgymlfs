﻿using Data.Room;
using UnityEngine;
using View.UI.OverheadUI;
using CustomAssets;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Rooms.LinearExpandRoom
{
    [RequireComponent(typeof(RoomConductor))]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Linear Expand Room/Linear Expand Room View")]
    public class LinearExpandRoomView : RoomView
    {
        private static readonly string unlockLookObjectName = "unlock_look";
        private static readonly string homeLookObjectName = "home_look";

        #region Public properties.
        public override Vector3 homeFocusPosition
        {
            get
            {
                var homeLookObject = transform.Find(homeLookObjectName);
                return homeLookObject != null ? homeLookObject.transform.position : transform.position;
            }
        }
        #endregion

        #region Inspector fields.
        [SerializeField]
        [Header("Room parts settings")]
        protected Transform[] partRoots;

        [SerializeField]
        protected MaterialsReplacementPolicy expandTogglesMaterialsReplacementPolicy;

        [SerializeField]
        protected string expandToggleInactivePolicyName;
        #endregion

        #region Private data.
        private LinearExpandToggle[] linearExpandToggles;

#if UNITY_EDITOR
        private int editorOverrideExpandLevel = -1;
#endif
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            linearExpandToggles = GetComponentsInChildren<LinearExpandToggle>(true);
        }
        #endregion

        #region Public virtual API.
        public override void SetActiveState(bool state)
        {
            base.SetActiveState(state);

            UpdateExpandToggles();
            UpdateActiveRoomToggles(isActivated);

            if (expandOverheadIcon != null && expandOverheadIcon.icon != null)
            {
                expandOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
            }

            if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.icon != null)
            {
                expandPendingOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
            }
        }

        public override void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            UpdateExpandOverheadIconsPosition(expandLevel);

            base.SetNewSize (expandLevel, newSize, unlockedBlocks, animate);

            UpdateExpandToggles();
            UpdateActiveRoomToggles(isActivated);
        }

        public override void TransitionToUnlockedRoom()
        {
            var partRoot = (expandLevel > 0)
                ? (expandLevel - 1 < partRoots.Length
                    ? partRoots[expandLevel - 1]
                    : null)
                : transform;

            if (partRoot != null)
            {
                var lookTransform = partRoot.Find(unlockLookObjectName);
                if (lookTransform != null)
                {
                    TransitionCameraTo(lookTransform.position);
                    return;
                }
            }

            // Custom attempts failed, use default mechanism.
            base.TransitionToUnlockedRoom();
        }
        #endregion

        #region Protected virtual API.
        public override void StartExpand()
        {
            switch (expandAvailabilityState)
            {
                case ExpandAvailabilityState.ExpandAvailable:
                case ExpandAvailabilityState.ExpandPending:
                case ExpandAvailabilityState.ExpandComplete:
                {
                    if (expandLevel == 0)
                    {
                        StartTransitionToRoom(); // Performs all that's needed.
                    }
                    else
                    {
                        RequestUnlock();
                    }
                }; break;
            }
        }
        #endregion

        #region Private functions.
        private void UpdateExpandOverheadIconsPosition(int expandLevel)
        {
            if ((expandOverheadIcon != null && expandOverheadIcon.instance != null) ||
                (expandPendingOverheadIcon != null && expandPendingOverheadIcon.instance != null))
            {
                var partRoot = (expandLevel > 0)
                    ? (expandLevel - 1 < partRoots.Length
                        ? partRoots[expandLevel - 1]
                        : null)
                    : transform;

                if (partRoot != null)
                {
                    var iconAttachment = partRoot.Find(iconAttachmentObjectName);
                    if (iconAttachment != null)
                    {
                        if (expandOverheadIcon != null)
                        {
                            expandOverheadIcon.icon.worldPositionProvider = new DynamicWorldPositionProvider(iconAttachment);
                            expandOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
                        }

                        if (expandPendingOverheadIcon != null)
                        {
                            expandPendingOverheadIcon.icon.worldPositionProvider = new DynamicWorldPositionProvider(iconAttachment);
                            expandPendingOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
                        }
                        return;
                    }
                }

                if (expandOverheadIcon != null && expandOverheadIcon.icon != null && expandOverheadIcon.icon.worldPositionProvider != null)
                    expandOverheadIcon.icon.worldPositionProvider.enabled = false;

                if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.icon != null && expandPendingOverheadIcon.icon.worldPositionProvider != null)
                    expandPendingOverheadIcon.icon.worldPositionProvider.enabled = false;
            }
        }

        void UpdateExpandToggles()
        {
#if UNITY_EDITOR
            var expandLevel = (editorOverrideExpandLevel >= 0) ? editorOverrideExpandLevel : this.expandLevel;
            linearExpandToggles = GetComponentsInChildren<LinearExpandToggle>(true);
#endif

            if (linearExpandToggles != null)
            {
                foreach (var let in linearExpandToggles)
                {
                    let.SetToggleState (expandLevel, isActivated, expandToggleInactivePolicyName, expandTogglesMaterialsReplacementPolicy);
                }
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        public override void EditorUpdate()
        {
            if (roomConductor != null && roomConductor.editorRoomSetupData.roomExpandMode != RoomExpandMode.Linear)
                roomConductor.editorRoomSetupData.roomExpandMode = RoomExpandMode.Linear;

            base.EditorUpdate();
        }

        public void EditorVisualizeExpandToggles(int expandLevel)
        {
            editorOverrideExpandLevel = expandLevel;

            UpdateExpandToggles();
            UpdateActiveRoomToggles(isActivated);
        }

        public void EditorVisualizeExpandToggles(int expandLevel, bool isActive)
        {
            editorOverrideExpandLevel = expandLevel;

            UpdateExpandToggles();
            UpdateActiveRoomToggles(isActive);
        }

        public int EditorGetMaxExpandLevel()
        {
            int result = 2;

            linearExpandToggles = GetComponentsInChildren<LinearExpandToggle>(true);

            if (linearExpandToggles != null && linearExpandToggles.Length > 0)
            {
                foreach (var et in linearExpandToggles)
                {
                    if (et.EditorGetExpandModesLength() > result)
                    {
                        result = et.EditorGetExpandModesLength();
                    }
                }
            }

            return result - 1;
        }
#endif
        #endregion
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(LinearExpandRoomView), true)]
    public class LinearExpandRoomViewEditor : Editor
    {
        private static bool activeRoomToggglesVisualize = false;
        private static bool expandToggglesVisualize = false;
        private static int expandLevel = 0;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var ucv = (LinearExpandRoomView)target;

            DrawDefaultInspector();

            if (ucv.gameObject.scene.IsValid() && ucv.gameObject.scene.isLoaded/* && Application.isPlaying*/)
            {
                GUILayout.Space(8);
                GUIStyle headerLabel = new GUIStyle(GUI.skin.label);
                headerLabel.fontStyle = FontStyle.Bold;
                GUILayout.Label("Debug and Visualization", headerLabel);

                expandToggglesVisualize = GUILayout.Toggle(expandToggglesVisualize, "Visualize Expand and Active Room Toggles");

                if (expandToggglesVisualize)
                {
                    GUILayout.Label(string.Format("Level: {0}", expandLevel.ToString()));

                    if (!Application.isPlaying)
                    {
                        activeRoomToggglesVisualize = GUILayout.Toggle(activeRoomToggglesVisualize, "Active Room");
                    }

                    expandLevel = Mathf.RoundToInt(GUILayout.HorizontalSlider(expandLevel, 0, ucv.EditorGetMaxExpandLevel()));

                    if (Application.isPlaying)
                    {
                        ucv.EditorVisualizeExpandToggles(expandLevel);
                    }
                    else
                    {
                        ucv.EditorVisualizeExpandToggles(expandLevel, activeRoomToggglesVisualize);
                    }
                }
                else
                {
                    ucv.EditorVisualizeExpandToggles(-1);
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif // #if UNITY_EDITOR
}
