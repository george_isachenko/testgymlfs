﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Rooms
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Auto Hider")]
    public class AutoHider : MonoBehaviour
    {
        #region Public (Inspector) data.
        public float[] directions;
        public bool invert;
        #endregion

        #region Private data.
        Plane[] planes;
        Renderer[] renderers;
        Collider[] colliders;
        Vector3 cachedOwnPosition = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        Quaternion cachedOwnRotation = new Quaternion(0, 0, 0, -1);
        Vector3 cachedCameraPosition = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        bool? wasVisible;
        #endregion

        #region Unity API.
        void Start()
        {
            planes = null;
            if (!Init())
            {
                if (Application.isPlaying)
                    enabled = false;
            }

            if (Application.isPlaying)
            {
                SetState(!invert);
            }
        }

        void OnDisable()
        {
            wasVisible = null;

            if (Application.isPlaying)
            {
                SetState(!invert);
            }
        }

        void OnEnable()
        {
            planes = null;
            Init();
        }

        void Update()
        {
            if (!Application.isPlaying)
            {
                if (cachedOwnPosition != transform.position ||
                    cachedOwnRotation != transform.rotation)
                {
                    planes = null;
                    if (!Init())
                    {
                        if (Application.isPlaying)
                            enabled = false;
                    }
                }
            }

            if (Application.isPlaying)
            {
                if (planes != null && planes.Length > 0 && (renderers != null || colliders != null))
                {
                    var cameraPosition = Camera.main.transform.position;
                    if (wasVisible == null || cachedCameraPosition != cameraPosition)
                    {
                        bool visible = CalculateVisibility(cameraPosition);

                        if (wasVisible == null || wasVisible.Value != visible)
                        {
                            SetState(visible);

                            wasVisible = visible;
                        }

                        cachedCameraPosition = cameraPosition;
                    }
                }
                else
                {
                    enabled = false;
                }
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void OnValidate()
        {
            planes = null;
            renderers = null;
            colliders = null;
            Init();
        }

        void OnDrawGizmos() 
        {
            if (this != null && gameObject != null && planes != null && planes.Length > 0 && (renderers != null || colliders != null))
            {
                var sceneView = SceneView.currentDrawingSceneView;
                if (sceneView != null && sceneView.camera != null)
                {
                    var cameraPosition = sceneView.camera.transform.position;

                    bool visible = CalculateVisibility(cameraPosition);

                    var topPos = transform.position;
                    if (renderers != null)
                    {
                        foreach (var renderer in renderers)
                        {
                            if (renderer.bounds.max.y > topPos.y)
                                topPos.y = renderer.bounds.max.y;
                        }
                    }

                    if (colliders != null)
                    {
                        foreach (var collider in colliders)
                        {
                            if (collider.bounds.max.y > topPos.y)
                                topPos.y = collider.bounds.max.y;
                        }
                    }

                    Gizmos.DrawIcon(topPos, visible ? "EyeOpen" : "EyeClosed", true);
                }
            }
        }
#endif
        #endregion

        #region Private functions.
        bool Init()
        {
            cachedOwnPosition = transform.position;
            cachedOwnRotation = transform.rotation;

            if (directions != null && directions.Length > 0)
            {
                planes = new Plane[directions.Length];

                for (int i = 0; i < directions.Length; i++)
                {
                    var directionAxis = (transform.rotation * Quaternion.AngleAxis(directions[i], Vector3.up) * Vector3.back).normalized;
                    planes[i] = new Plane(directionAxis, transform.position);
                }

                renderers = GetComponentsInChildren<Renderer>(true);
                colliders = GetComponentsInChildren<Collider>(true);

                return (planes != null && planes.Length > 0 && renderers != null && renderers.Length > 0);
            }

            return false;
        }

        bool CalculateVisibility(Vector3 cameraPosition)
        {
            bool visible = false;

            foreach (var plane in planes)
            {
                visible |= !plane.GetSide(cameraPosition);
            }

            visible ^= invert;

            return visible;
        }

        void SetState(bool state)
        {
            if (renderers != null)
            {
                foreach (var renderer in renderers)
                {
                    renderer.enabled = state;
                }
            }

            if (colliders != null)
            {
                foreach (var collider in colliders)
                {
                    collider.enabled = state;
                }
            }
        }
        #endregion
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(AutoHider), true)]
    [CanEditMultipleObjects]
    public class AutoHiderEditor : Editor
    {
        private static readonly Color rotationColor = new Color(0.929f, 0.203f, 0.215f, 1.0f);

        public void OnSceneGUI()
        {
            var autoHider = target as AutoHider;

            if (autoHider != null && autoHider.directions != null && autoHider.directions.Length > 0)
            {
                foreach (var direction in autoHider.directions)
                {
                    var originalColor = Handles.color;

                    Handles.color = rotationColor;
#if UNITY_5_6_OR_NEWER
                    Handles.ArrowHandleCap (-1, autoHider.transform.position, autoHider.transform.rotation * Quaternion.AngleAxis(direction, Vector3.up), 1.0f, EventType.Repaint);
#else
                    Handles.ArrowCap (-1, autoHider.transform.position, autoHider.transform.rotation * Quaternion.AngleAxis(direction, Vector3.up), 1.0f);
#endif
                    Handles.color = originalColor;
                }
            }
        }
    }
#endif // #if UNITY_EDITOR

}