﻿using CustomAssets;
using Data.Room;
using UnityEngine;

namespace View.Rooms.FixedSizeRoom
{
    [RequireComponent(typeof(RoomConductor))]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Fixed Size Room/Fixed Size Room View")]
    public class FixedSizeRoomView : RoomView
    {
        // private static readonly string unlockLookObjectName = "unlock_look";
        private static readonly string homeLookObjectName = "home_look";

        #region Public properties
        public override Vector3 homeFocusPosition
        {
            get
            {
                var homeLookObject = transform.Find(homeLookObjectName);
                return homeLookObject != null ? homeLookObject.transform.position : transform.position;
            }
        }
        #endregion

        #region Inspector fields
        [SerializeField]
        protected MaterialsReplacementPolicy unlockTogglesMaterialsReplacementPolicy;

        [SerializeField]
        protected string unlockToggleInactivePolicyName;
        #endregion

        #region Private data.
        private FixedSizeUnlockToggle[] fixedSizeUnlockToggles;
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            fixedSizeUnlockToggles = GetComponentsInChildren<FixedSizeUnlockToggle>(true);
        }
        #endregion

        #region Public virtual API.
        public override void SetActiveState(bool state)
        {
            base.SetActiveState(state);

            UpdateUnlockToggles();
            UpdateActiveRoomToggles(isActivated);
        }

        public override void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            base.SetNewSize (expandLevel, newSize, unlockedBlocks, animate);

            UpdateUnlockToggles();
            UpdateActiveRoomToggles(isActivated);
        }
        #endregion

        #region Protected virtual API.
        public override void StartExpand()
        {
            switch (expandAvailabilityState)
            {
                case ExpandAvailabilityState.ExpandAvailable:
                case ExpandAvailabilityState.ExpandPending:
                case ExpandAvailabilityState.ExpandComplete:
                {
                    if (expandLevel == 0)
                    {
                        StartTransitionToRoom(); // Performs all that's needed.
                    }
                }; break;
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        public override void EditorUpdate()
        {
            if (roomConductor != null && roomConductor.editorRoomSetupData.roomExpandMode != RoomExpandMode.Fixed)
                roomConductor.editorRoomSetupData.roomExpandMode = RoomExpandMode.Fixed;

            base.EditorUpdate();
        }
#endif
        #endregion

        #region Private methods.
        void UpdateUnlockToggles()
        {
            if (fixedSizeUnlockToggles != null)
            {
                foreach (var let in fixedSizeUnlockToggles)
                {
                    let.SetToggleState (expandLevel > 0, isActivated, unlockToggleInactivePolicyName, unlockTogglesMaterialsReplacementPolicy);
                }
            }
        }
        #endregion
    }
}
