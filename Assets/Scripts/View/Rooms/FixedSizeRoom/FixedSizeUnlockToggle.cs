﻿using CustomAssets;
using UnityEngine;
using View.Actions;

namespace View.Rooms.FixedSizeRoom
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Fixed Size Room/Fixed Size Unlock Toggle")]
    public class FixedSizeUnlockToggle : UnlockToggleBase
    {
        #region Inspector fields.
        [SerializeField]
        protected Mode lockedMode;

        [SerializeField]
        protected Mode unlockedMode;
        #endregion

        #region Public API.
        public void SetToggleState 
            ( bool unlocked
            , bool active
            , string inactiveMaterialReplacementPolicyName
            , MaterialsReplacementPolicy materialsReplacementPolicy = null)
        {
            var mode = unlocked ? unlockedMode : lockedMode;

            SetMode ( mode, active, inactiveMaterialReplacementPolicyName, materialsReplacementPolicy);
        }
        #endregion
    }
}