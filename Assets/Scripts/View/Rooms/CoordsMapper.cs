﻿using Data.Room;
using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace View.Rooms
{
    [Serializable]
    public struct CoordsMapper
    {
        #region Static/readonly data.
        public static readonly float defaultCellSize = 0.1f;
        #endregion

        #region Public (Inspector) fields.
        [Range(0.01f, 10.0f)]
        public float cellSize;
        public bool flipX;
        public bool flipY;
        #endregion

        #region Private data.
        private Vector3 roomCoordsScale;
        private Vector2 roomCoordsScale2;
        private Vector3 invRoomCoordsScale;
        private Vector2 invRoomCoordsScale2;
        #endregion

        public void Update(bool force)
        {
            if (cellSize <= 0)
                cellSize = defaultCellSize;

            if (force || roomCoordsScale == Vector3.zero)
            {
                roomCoordsScale = new Vector3 (cellSize * (flipX ? -1.0f : 1.0f), 1.0f, cellSize * (flipY ? -1.0f : 1.0f));
                roomCoordsScale2 = new Vector2(roomCoordsScale.x, roomCoordsScale.z);
                invRoomCoordsScale = new Vector3
                    (roomCoordsScale.x != 0 ? 1.0f / roomCoordsScale.x : 0.0f
                    , roomCoordsScale.y != 0 ? 1.0f / roomCoordsScale.y : 0.0f
                    , roomCoordsScale.z != 0 ? 1.0f / roomCoordsScale.z : 0.0f);
                invRoomCoordsScale2 = new Vector2
                    (invRoomCoordsScale.x, invRoomCoordsScale.z);
            }
        }

        //--------------------------------------------------------------------------------------

        public Vector3 ConvertPositionFromLogic(Vector3 posInLogicScale)
        {
            Assert.IsFalse(roomCoordsScale == Vector3.zero);
            return Vector3.Scale(posInLogicScale, roomCoordsScale);
        }

        public Vector2 ConvertPositionFromLogic(Vector2 posInLogicScale)
        {
            Assert.IsFalse(roomCoordsScale2 == Vector2.zero);
            return Vector2.Scale(posInLogicScale, roomCoordsScale2);
        }

        public Vector3 ConvertPositionToLogic(Vector3 posInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            return Vector3.Scale(posInWorldScale, invRoomCoordsScale);
        }

        public Vector2 ConvertPositionToLogic(Vector2 posInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            return Vector2.Scale(posInWorldScale, invRoomCoordsScale2);
        }

        //--------------------------------------------------------------------------------------

        public RoomCoordinates ConvertPositionToRoomCoordinates(Vector2 localPosInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x), Mathf.FloorToInt(scaled.y));
        }

        public RoomCoordinates ConvertPositionToRoomCoordinatesRounded(Vector2 localPosInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x), Mathf.RoundToInt(scaled.y));
        }

        //--------------------------------------------------------------------------------------

        public RoomCoordinates ConvertPositionToRoomCoordinates(Vector3 localPosInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x), Mathf.FloorToInt(scaled.z));
        }

        public RoomCoordinates ConvertPositionToRoomCoordinatesRounded(Vector3 localPosInWorldScale)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x), Mathf.RoundToInt(scaled.z));
        }

        //--------------------------------------------------------------------------------------

        public RoomCoordinates AlignPositionToRoomCoordinates(Vector2 localPosInWorldScale, RoomCoordinates orientedSize)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x - orientedSize.x * 0.5f), Mathf.FloorToInt(scaled.y - orientedSize.y * 0.5f));
        }

        public RoomCoordinates AlignPositionToRoomCoordinates(Vector2 localPosInWorldScale, RoomCoordinates orientedSize, Vector3 shift)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x - orientedSize.x * 0.5f - shift.x), Mathf.FloorToInt(scaled.y - orientedSize.y * 0.5f - shift.z));
        }

        public RoomCoordinates AlignPositionToRoomCoordinatesRounded(Vector2 localPosInWorldScale, RoomCoordinates orientedSize)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x - orientedSize.x * 0.5f), Mathf.RoundToInt(scaled.y - orientedSize.y * 0.5f));
        }

        public RoomCoordinates AlignPositionToRoomCoordinatesRounded(Vector2 localPosInWorldScale, RoomCoordinates orientedSize, Vector3 shift)
        {
            Assert.IsFalse(invRoomCoordsScale2 == Vector2.zero);
            Vector2 scaled = Vector2.Scale(invRoomCoordsScale2, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x - orientedSize.x * 0.5f - shift.x), Mathf.RoundToInt(scaled.y - orientedSize.y * 0.5f - shift.z));
        }

        //--------------------------------------------------------------------------------------

        public RoomCoordinates AlignPositionToRoomCoordinates(Vector3 localPosInWorldScale, RoomCoordinates orientedSize)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x - orientedSize.x * 0.5f), Mathf.FloorToInt(scaled.z - orientedSize.y * 0.5f));
        }

        public RoomCoordinates AlignPositionToRoomCoordinates(Vector3 localPosInWorldScale, RoomCoordinates orientedSize, Vector3 shift)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.FloorToInt(scaled.x - orientedSize.x * 0.5f - shift.x), Mathf.FloorToInt(scaled.z - orientedSize.y * 0.5f - shift.z));
        }

        public RoomCoordinates AlignPositionToRoomCoordinatesRounded(Vector3 localPosInWorldScale, RoomCoordinates orientedSize)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x - orientedSize.x * 0.5f), Mathf.RoundToInt(scaled.z - orientedSize.y * 0.5f));
        }

        public RoomCoordinates AlignPositionToRoomCoordinatesRounded(Vector3 localPosInWorldScale, RoomCoordinates orientedSize, Vector3 shift)
        {
            Assert.IsFalse(invRoomCoordsScale == Vector3.zero);
            Vector3 scaled = Vector3.Scale(invRoomCoordsScale, localPosInWorldScale);
            return new RoomCoordinates(Mathf.RoundToInt(scaled.x - orientedSize.x * 0.5f - shift.x), Mathf.RoundToInt(scaled.z - orientedSize.y * 0.5f - shift.z));
        }

        //--------------------------------------------------------------------------------------

        public Vector3 ConvertPositionFromRoomCooordinates(RoomCoordinates cellCoords)
        {
            Assert.IsFalse(roomCoordsScale == Vector3.zero);
            return Vector3.Scale(roomCoordsScale, new Vector3(cellCoords.x, 0.0f, cellCoords.y));
        }

        public Vector2 ConvertPosition2DFromRoomCooordinates(RoomCoordinates cellCoords)
        {
            Assert.IsFalse(roomCoordsScale2 == Vector2.zero);
            return Vector2.Scale(roomCoordsScale2, new Vector2(cellCoords.x, cellCoords.y));
        }

        public Vector3 AlignPositionFromCellCooordinates(RoomCoordinates cellCoords, RoomCoordinates orientedSize)
        {
            Assert.IsFalse(roomCoordsScale == Vector3.zero);
            return Vector3.Scale(roomCoordsScale, new Vector3(cellCoords.x + orientedSize.x * 0.5f, 0.0f, cellCoords.y + orientedSize.y * 0.5f));
        }

        public Vector3 AlignPositionFromRoomCooordinates(RoomCoordinates cellCoords, RoomCoordinates orientedSize, Vector3 shift)
        {
            Assert.IsFalse(roomCoordsScale == Vector3.zero);
            return Vector3.Scale(roomCoordsScale, new Vector3(cellCoords.x + orientedSize.x * 0.5f + shift.x, shift.y, cellCoords.y + orientedSize.y * 0.5f + shift.z));
        }

        #region Public Editor-only API.
#if UNITY_EDITOR
        public int EditorGetStateHashCode()
        {
            return (cellSize.GetHashCode() ^ flipX.GetHashCode() ^ flipY.GetHashCode());
        }
#endif
        #endregion
    }
}
