﻿using Data.Room;

namespace View.Rooms
{
    public struct PlacementResult
    {
        #region Types.
        public enum State
        {
            NoOccupation = 0,
            Invalid,
            OccupiesFloor,
            OccupiesWall,
            OccupiesFloorAndWall
        }
        #endregion

        #region Public data.
        public State state;
        public RoomCoordinates occupationMapPosition;
        public RoomCoordinates occupationMapOrientedSize;
        public RoomCoordinates wallOccupationMapPosition;
        public RoomCoordinates wallOccupationMapOrientedSize;
        #endregion

        #region Properties.
        public bool isValid
        {
            get { return state != State.Invalid; }
        }

        public bool hasOccupation
        {
            get { return state == State.OccupiesFloor || state == State.OccupiesWall || state == State.OccupiesFloorAndWall; }
        }
        #endregion

        #region Constructors.
        public PlacementResult(State state)
        {
            this.state = state;
            occupationMapPosition = new RoomCoordinates();
            occupationMapOrientedSize = new RoomCoordinates();
            wallOccupationMapPosition = new RoomCoordinates();
            wallOccupationMapOrientedSize = new RoomCoordinates();
        }
        #endregion

        #region Public API
        public void Reset()
        {
            state = State.NoOccupation;
        }
        #endregion

        #region System.Object interface.
        public override bool Equals(object obj) 
        {
            if (!(obj is PlacementResult))
                return false;

            PlacementResult str = (PlacementResult) obj;
            return (str == this);
        }

        public override int GetHashCode()
        {
            return state.GetHashCode() ^
                occupationMapPosition.GetHashCode() ^
                occupationMapOrientedSize.GetHashCode() ^
                wallOccupationMapPosition.GetHashCode() ^
                wallOccupationMapOrientedSize.GetHashCode();
        }
        
        public static bool operator == (PlacementResult p1, PlacementResult p2)
        {
            return (p1.state == p2.state);
        }

        public static bool operator != (PlacementResult p1, PlacementResult p2)
        {
            return (p1.state != p2.state);
        }
        #endregion
    }
}