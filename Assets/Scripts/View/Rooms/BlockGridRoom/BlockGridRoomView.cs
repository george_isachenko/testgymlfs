﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomAssets;
using Data.Room;
using InspectorHelpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using View.Actions;
using View.CameraHelpers;
using View.EditorHelpers;
using View.EventHelpers;

#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.SceneManagement;

#endif

namespace View.Rooms.BlockGridRoom
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RoomConductor))]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Block Grid Room/Block Grid Room View")]
    public class BlockGridRoomView : RoomView
    {
        #region Types.
        public enum BlockSides
        {
            North = 0,
            NorthEast,
            East,
            SouthEast,
            South,
            SouthWest,
            West,
            NorthWest
        }

        [Serializable]
        public struct BlockSetupInfo
        {
            public BlockSides[] premadeSides;
            public bool premadeRoof;
            public bool premadeFloor;
            public bool debugUnlocked;

#if UNITY_EDITOR
            public int EditorGetStateHashCode()
            {
                int hashCode = 0;

                if (premadeSides != null && premadeSides.Length > 0)
                {
                    foreach (var p in premadeSides)
                    {
                        hashCode ^= p.GetHashCode();
                    }
                }

                hashCode ^= premadeRoof.GetHashCode();
                hashCode ^= premadeFloor.GetHashCode();
                hashCode ^= debugUnlocked.GetHashCode();

                return hashCode;
            }
#endif
        }

        public struct BlockRuntimeData
        {
            public bool unlocked;
            public RoomCoordinates blockCoords;
            public GameObject[] expandMarkers;

            public BlockRuntimeData(RoomCoordinates blockCoords, bool unlocked)
            {
                this.blockCoords = blockCoords;
                this.unlocked = unlocked;
                this.expandMarkers = null;
            }
        }

        [Serializable]
        public struct PrefabWithVariations
        {
            public GameObject[] prefabs;
            public int multiplier;
            public int modulus;
            public int shift;

            public GameObject GetVariation (int inputIndex)
            {
                if (prefabs != null && prefabs.Length > 0)
                {
                    if (multiplier > 0)
                        inputIndex *= multiplier;

                    if (modulus > 0)
                        inputIndex %= modulus;

                    return prefabs[(inputIndex + shift) % prefabs.Length];
                }
                return null;
            }

#if UNITY_EDITOR
            public int EditorGetStateHashCode()
            {
                int hashCode = 0;

                if (prefabs != null && prefabs.Length > 0)
                {
                    foreach (var p in prefabs)
                    {
                        hashCode ^= (p != null ? p.GetInstanceID() : 0);
                    }
                }

                hashCode ^= multiplier;
                hashCode ^= modulus;
                hashCode ^= shift;

                return hashCode;
            }
#endif
        }

        enum FindRoomBlockMode
        {
            Any,                // Any block.
            LockedAny,          // Any locked block.
            LockedEdge,         // Locked block with unlocked block nearby on any edge (including diagonal).
            LockedSideOnly,     // Locked block with unlocked block nearby on any side (North, East, West, South).
            Unlocked,           // Any unlocked block.
        }
        #endregion

        #region Static/readonly data.
        private static readonly string roomBlocksObjectName = "_Blocks";
        private static readonly Quaternion rotationNorth    = Quaternion.AngleAxis(0, Vector3.up);
        private static readonly Quaternion rotationEast     = Quaternion.AngleAxis(90, Vector3.up);
        private static readonly Quaternion rotationSouth    = Quaternion.AngleAxis(180, Vector3.up);
        private static readonly Quaternion rotationWest     = Quaternion.AngleAxis(270, Vector3.up);
        private static readonly Quaternion[] rotationAngles = new Quaternion[] { rotationNorth, rotationEast, rotationSouth, rotationWest };
        private static readonly BlockSides[] siblingBlocksSides = new BlockSides[] { BlockSides.North, BlockSides.East, BlockSides.South, BlockSides.West };
        private static readonly BlockSides[] siblingBlocksCorners = new BlockSides[] { BlockSides.NorthWest, BlockSides.NorthEast, BlockSides.SouthEast, BlockSides.SouthWest };
        #endregion

        #region Public properties.
        public override Vector3 homeFocusPosition
        {
            get
            {
                var cameraController = Camera.main.GetComponent<CameraController>();
                if (cameraController != null)
                {
                    var cameraLookAtPosition = cameraController.GetLookAtPoint();

                    var blockIndex = FindRoomBlockClosestToPosition(cameraLookAtPosition, FindRoomBlockMode.Unlocked);
                    if (blockIndex >= 0)
                    {
                        var position = GetBlockCenterWorldPosition(blockIndex);
                        return position;
                    }
                }

                return base.homeFocusPosition;
            }
        }
        #endregion

        #region Inspector data.
        //------------------------------------------------------------
        [Header("Room blocks setup")]

        [ReorderableList]
        [Tooltip("Maximum size in a number of blocks by each side")]
        [SerializeField]
        protected BlockSetupInfo[] blocksSetup;

        //------------------------------------------------------------
        [Header("Room blocks parts setup")]

        [SerializeField]
        protected PrefabWithVariations roomPartCornerInwardPrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartCornerOutwardPrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartFloorPrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartRoofPrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartWallCenterPrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartWallSidePrefabs;

        [SerializeField]
        protected PrefabWithVariations roomPartLockedFarBlockPrefab;

        [SerializeField]
        protected PrefabWithVariations roomPartLockedNearBlockPrefab;

        [SerializeField]
        protected GameObject roomPartExpandMarkerPrefab;

        [SerializeField]
        protected GameObject roomPartExpandSideMarkerPrefab;

        [SerializeField]
        protected int rotationVariativityMultiplier = 1;

        [SerializeField]
        protected int rotationVariativityModulus = 3;

        //------------------------------------------------------------
        [Header("Expand marker material replacement settings.")]

        [SerializeField]
        protected MaterialsReplacementPolicy expandMarkerMaterialsReplacementPolicy;

        [SerializeField]
        protected string selectedExpandMarkerMaterialsReplacementPolicyName;
        #endregion

        #region Private data.
        private GameObject[] roofObjects;
        private BlockRuntimeData[] blocksRuntimeData;
        private Transform blocksRoot;
        private TiledSpriteRenderer[] floorSpriteRenderers;
        private TiledSpriteRenderer[] wallSpriteRenderers;
        private Sprite lastWallSprite;
        private Sprite lastFloorSprite;
        private int? editorStateHashCode;
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
            {
                if (blocksSetup == null)
                {
                    blocksSetup = new BlockSetupInfo[ maxDimensions.x * maxDimensions.y];
                }

                blocksRuntimeData = new BlockRuntimeData[blocksSetup.Length];
            }
        }

        protected new void Start()
        {
            base.Start();

            UpdateRuntimeBlocksData();

            if (!Application.isPlaying) // Will be rebuilt when SetNewSize() is called.
            {
                RebuildRoom();
            }
        }
        #endregion

        #region Public virtual API.
        public override void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            UpdateRuntimeBlocksData(unlockedBlocks);
            RebuildRoom();

            if (lastWallSprite != null)
                SetWallsSprite(lastWallSprite);

            if (lastFloorSprite != null)
                SetFloorSprite(lastFloorSprite);

            base.SetNewSize(expandLevel, newSize, unlockedBlocks, animate);

            UpdateActiveRoomToggles(isActivated);
        }

        public override void SetActiveState(bool state)
        {
            base.SetActiveState(state);

            // UpdateExpandToggles();
            UpdateActiveRoomToggles(isActivated);

            if (expandOverheadIcon != null && expandOverheadIcon.icon != null)
            {
                expandOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
            }

            if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.icon != null)
            {
                expandPendingOverheadIcon.icon.worldPositionProvider.enabled = (isActivated || expandLevel == 0);
            }
        }

        public override void SetWallsSprite(Sprite sprite)
        {
            if (wallSpriteRenderers != null && sprite != null)
            {
                foreach (var tsr in wallSpriteRenderers)
                {
                    tsr.sprite = sprite;
                    tsr.UpdateAll();
                }

                lastWallSprite = sprite;
            }
        }

        public override void SetFloorSprite(Sprite sprite)
        {
            if (floorSpriteRenderers != null && sprite != null)
            {
                foreach (var tsr in floorSpriteRenderers)
                {
                    tsr.sprite = sprite;
                    tsr.UpdateAll();
                }

                lastFloorSprite = sprite;
            }
        }

        public override void StartExpand()
        {
            if (isActivated)
            {
                if (expandAvailabilityState == ExpandAvailabilityState.ExpandAvailable)
                {
                    // TODO: Check, if we are already at limit?
                    if (SendPrepareToExpandRequest())
                    {
                        for (var i = 0; i < blocksRuntimeData.Length; i++)
                        {
                            if (blocksRuntimeData[i].expandMarkers != null)
                            {
                                foreach (var expandMarker in blocksRuntimeData[i].expandMarkers)
                                {
                                    expandMarker.SetActive(true);
                                }
                            }
                        }

                        // Focus camera on locked side block nearest to its projected view point.
                        var cameraController = Camera.main.GetComponent<CameraController>();
                        if (cameraController != null)
                        {
                            var cameraLookAtPosition = cameraController.GetLookAtPoint();

                            int blockIndex = FindRoomBlockClosestToPosition(cameraLookAtPosition, FindRoomBlockMode.LockedSideOnly);

                            if (blockIndex >= 0)
                            {
                                var position = GetBlockCenterWorldPosition(blockIndex);
                                cameraController.AutoMoveTo(position.x, position.z);
                            }
                        }
                    }
                }
                else
                {
                    OnCannotExpand();
                }
            }
        }

        public override bool CheckForExpandStart (bool canExpandX, bool canExpandY, int blockIndex)
        {
            var cameraController = Camera.main.GetComponent<CameraController>();
            if (cameraController != null)
            {
                if (blockIndex >= 0)
                {
                    var halfBlockCoords = new RoomCoordinates(roomConductor.roomSetupDataInfo.blockSize / 2, roomConductor.roomSetupDataInfo.blockSize / 2);

                    var coords = new RoomCoordinates
                            ( blocksRuntimeData[blockIndex].blockCoords.x * roomConductor.roomSetupDataInfo.blockSize
                            , blocksRuntimeData[blockIndex].blockCoords.y * roomConductor.roomSetupDataInfo.blockSize) + halfBlockCoords;

                    var position = roomConductor.roomObject.transform.TransformPoint(roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(coords));
                    Vector3 offset = cameraController.GetWorldOffsetForNormalizedScreenPoint (gui.hud.focusPointLeft025.anchorMin);
                    cameraController.AutoMoveTo(position.x + offset.x, position.z + offset.y);
                }
            }

            return true;
        }

        public override void ExpandEnded()
        {
            for (var i = 0; i < blocksRuntimeData.Length; i++)
            {
                if (blocksRuntimeData[i].expandMarkers != null)
                {
                    foreach (var expandMarker in blocksRuntimeData[i].expandMarkers)
                    {
                        if (expandMarkerMaterialsReplacementPolicy != null &&
                            selectedExpandMarkerMaterialsReplacementPolicyName != null &&
                            selectedExpandMarkerMaterialsReplacementPolicyName != string.Empty)
                        {
                            var materialsReplacement = expandMarker.GetComponent<MaterialsReplacement>();
                            if (materialsReplacement != null)
                            {
                                materialsReplacement.ResetBackMaterials(selectedExpandMarkerMaterialsReplacementPolicyName);
                            }
                        }

                        expandMarker.SetActive(false);
                    }
                }
            }

            base.ExpandEnded();
        }

        public override bool GetZoneCenter(string zoneName, out Vector3 position)
        {
            return base.GetZoneCenter(zoneName, out position);
        }

        public override bool GetZoneCenterLocal(string zoneName, out Vector3 position)
        {
            return base.GetZoneCenterLocal(zoneName, out position);
        }

        public override bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position)
        {
            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                RoomCoordinates tmpPos;
                if (GenerateRandomPointInsideUnlockedSpace(out tmpPos))
                {
                    position = roomConductor.roomObject.transform.TransformPoint(roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(tmpPos));
                    return true;
                }
                else
                {
                    position = Vector3.zero;
                    return false;
                }
            }

            return base.GenerateRandomPointInsideZone(zoneName, out position);
        }

        public override bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition)
        {
            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                RoomCoordinates tmpPos;
                if (GenerateRandomPointInsideUnlockedSpace(out tmpPos))
                {
                    localPosition = roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(tmpPos);
                    return true;
                }
                else
                {
                    localPosition = Vector3.zero;
                    return false;
                }
            }

            return base.GenerateRandomLocalPointInsideZone(zoneName, out localPosition);
        }

        public override bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position, out Quaternion rotation)
        {
            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                RoomCoordinates tmpPos;
                if (GenerateRandomPointInsideUnlockedSpace(out tmpPos))
                {
                    position = roomConductor.roomObject.transform.TransformPoint(roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(tmpPos));
                    rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360.0f - Mathf.Epsilon), Vector3.up);
                    return true;
                }
                else
                {
                    position = Vector3.zero;
                    rotation = Quaternion.identity;
                    return false;
                }
            }

            return base.GenerateRandomPointInsideZone(zoneName, out position, out rotation);
        }

        public override bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition, out Quaternion rotation)
        {
            if (zoneName == CharacterDestination.wholeRoom) // Special case, means whole active room size.
            {
                RoomCoordinates tmpPos;
                if (GenerateRandomPointInsideUnlockedSpace(out tmpPos))
                {
                    localPosition = roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(tmpPos);
                    rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360.0f - Mathf.Epsilon), Vector3.up);
                    return true;
                }
                else
                {
                    localPosition = Vector3.zero;
                    rotation = Quaternion.identity;
                    return false;
                }
            }

            return base.GenerateRandomLocalPointInsideZone(zoneName, out localPosition, out rotation);
        }

/*
        bool GenerateRandomPointInsideUnlockedSpace (out Vector3 position)
        {
            var unlockedCells = (from brd in blocksRuntimeData
                                    where brd.unlocked
                                    select brd).ToArray();

            if (unlockedCells != null && unlockedCells.Length > 0)
            {
                var selectedCell = unlockedCells[UnityEngine.Random.Range(0, unlockedCells.Length - 1)];

                position = new Vector3
                    ( selectedCell.blockCoords.x * roomConductor.coordsMapper.cellSize +
                        UnityEngine.Random.Range(roomConductor.coordsMapper.cellSize * 0.1f, roomConductor.coordsMapper.cellSize * (roomConductor.roomSetupDataInfo.blockSize - 1))
                    , 0.0f
                    , selectedCell.blockCoords.y * roomConductor.coordsMapper.cellSize +
                        UnityEngine.Random.Range(roomConductor.coordsMapper.cellSize * 0.1f, roomConductor.coordsMapper.cellSize * (roomConductor.roomSetupDataInfo.blockSize - 1)));
                return true;
            }

            position = Vector3.zero;
            return false;
        }
*/

        bool GenerateRandomPointInsideUnlockedSpace (out RoomCoordinates position)
        {
            var unlockedCells = (from brd in blocksRuntimeData
                                    where brd.unlocked
                                    select brd).ToArray();

            if (unlockedCells != null && unlockedCells.Length > 0)
            {
                var selectedCell = unlockedCells[UnityEngine.Random.Range(0, unlockedCells.Length - 1)];

                position = new RoomCoordinates
                    ( selectedCell.blockCoords.x * roomConductor.roomSetupDataInfo.blockSize + UnityEngine.Random.Range(0, roomConductor.roomSetupDataInfo.blockSize - 1)
                    , selectedCell.blockCoords.y * roomConductor.roomSetupDataInfo.blockSize + UnityEngine.Random.Range(0, roomConductor.roomSetupDataInfo.blockSize - 1));
                return true;
            }

            position = RoomCoordinates.zero;
            return false;
        }

        public override EquipmentPlacement FindFreeSpaceForEquipment(RoomCoordinates size)
        {
            var cameraController = Camera.main.GetComponent<CameraController>();
            if (cameraController != null)
            {
                var cameraLookAtPosition = cameraController.GetLookAtPoint();

                int blockIdx = FindRoomBlockClosestToPosition(cameraLookAtPosition, FindRoomBlockMode.Unlocked);

                if (blockIdx >= 0)
                {
                    Debug.LogFormat("Block idx: {0}", blockIdx);

                    var blockCoords = blocksRuntimeData[blockIdx].blockCoords;

                    var halfBlockSize = new RoomCoordinates(roomConductor.roomSetupDataInfo.blockSize / 2, roomConductor.roomSetupDataInfo.blockSize / 2);

                    var startingSearchPosition = new RoomCoordinates
                        (blockCoords.x * roomConductor.roomSetupDataInfo.blockSize, blockCoords.y * roomConductor.roomSetupDataInfo.blockSize)
                        + halfBlockSize - new RoomCoordinates(size.x / 2, size.y / 2);

                    return roomConductor.FindFreeSpace(startingSearchPosition, size);
                }
            }

            return base.FindFreeSpaceForEquipment(size);
        }
        #endregion

        #region Protected API.
        protected override void UpdateActiveRoomToggles(bool state)
        {
            base.UpdateActiveRoomToggles(state);

            if (roofObjects != null)
            {
                foreach (var roofObject in roofObjects)
                {
                    roofObject.SetActive(!state);
                }
            }
        }
        #endregion

        #region Private properties.
        RoomCoordinates maxDimensions
        {
            get
            {
                return (roomConductor != null)
                    ? roomConductor.roomSetupDataInfo.maxSizeInBlocks
                    : RoomCoordinates.one;
            }
        }
        #endregion

        #region Private functions.
        private static RoomCoordinates GetCoordinatesForSide (RoomCoordinates from, BlockSides side)
        {
            switch (side)
            {
                case BlockSides.North:       return new RoomCoordinates(from.x,      from.y - 1);
                case BlockSides.NorthEast:   return new RoomCoordinates(from.x - 1,  from.y - 1);
                case BlockSides.East:        return new RoomCoordinates(from.x - 1,  from.y );
                case BlockSides.SouthEast:   return new RoomCoordinates(from.x - 1,  from.y + 1);
                case BlockSides.South:       return new RoomCoordinates(from.x,      from.y + 1);
                case BlockSides.SouthWest:   return new RoomCoordinates(from.x + 1,  from.y + 1);
                case BlockSides.West:        return new RoomCoordinates(from.x + 1,  from.y);
                case BlockSides.NorthWest:   return new RoomCoordinates(from.x + 1,  from.y - 1);
            }
            return from;
        }

        private static int GetBlockIdxForRoomCoordinates (RoomCoordinates maxSize, RoomCoordinates coords)
        {
            Assert.IsTrue(maxSize.isValidSize);
            Assert.IsTrue(coords.isValid);

            var xClamped = Math.Min(maxSize.x - 1, coords.x);
            var yClamped = Math.Min(maxSize.y - 1, coords.y);
            var maxXOrY = Math.Max(xClamped, yClamped);

            var value = Math.Min(maxSize.x - 1, maxXOrY) *
                        Math.Min(maxSize.x - 1, maxXOrY) +
                        Math.Min(maxSize.y - 1, coords.y) + 
                        (maxXOrY - coords.x) +
                        (maxSize.x - 2) * (coords.y - Math.Min(maxSize.x - 1, coords.y));

            Assert.IsTrue(value >= 0 && value <= (maxSize.x * maxSize.y) - 1);
            return value;
        }

        private Vector3 GetBlockLocalPos (RoomCoordinates blockCoords)
        {
            Assert.IsNotNull(roomConductor);

            var blockSize = (float)(roomConductor.roomSetupDataInfo.blockSize) * roomConductor.coordsMapper.cellSize;

            return new Vector3 (  (blockCoords.x * blockSize + blockSize * 0.5f) * (roomConductor.coordsMapper.flipX ? -1.0f : 1.0f)
                                , 0
                                , (blockCoords.y * blockSize + blockSize * 0.5f) * (roomConductor.coordsMapper.flipY ? -1.0f : 1.0f));
        }

        private void UpdateRuntimeBlocksData(bool[] unlockedBlocks = null)
        {
            if (blocksRuntimeData == null)
                blocksRuntimeData = new BlockRuntimeData[blocksSetup.Length];
            else if (blocksRuntimeData.Length != blocksSetup.Length)
                Array.Resize(ref blocksRuntimeData, blocksSetup.Length);

            var maxDimension = maxDimensions; // Cache it.

            for (int y = 0; y < maxDimensions.y; y++)
            {
                for (int x = 0; x < maxDimensions.x; x++)
                {
                    var blockCoords = new RoomCoordinates(x, y);
                    var blockIdx = GetBlockIdxForRoomCoordinates(maxDimensions, blockCoords);

                    if (unlockedBlocks != null)
                    {
                        Assert.IsTrue(blockIdx < unlockedBlocks.Length);
                        if (blockIdx < unlockedBlocks.Length)
                            blocksRuntimeData[blockIdx].unlocked = unlockedBlocks[blockIdx];
                    }
                    else
                    {
                        blocksRuntimeData[blockIdx].unlocked = blocksSetup[blockIdx].debugUnlocked;
                    }

                    blocksRuntimeData[blockIdx].blockCoords = blockCoords;
                }
            }
        }

        private Quaternion CalculateRotationVariativity (int inputIndex)
        {
            if (rotationVariativityMultiplier > 0)
                inputIndex *= rotationVariativityMultiplier;

            if (rotationVariativityModulus > 0)
                inputIndex %= rotationVariativityModulus;

            return rotationAngles[(inputIndex) % rotationAngles.Length];
        }

        private bool CleanupBlocksRoot()
        {
            // Check self, we can be destroyed already when starting play mode from Editor (because called from EditorApplication.delayCall callback..
            if (!this || !transform || !transform.gameObject)
                return false;

            blocksRoot = transform.Find(roomBlocksObjectName);
            if (blocksRoot == null)
            {
                blocksRoot = new GameObject(roomBlocksObjectName).transform;
                blocksRoot.SetParent(transform, false);

                if (roomConductor != null)
                {
                    blocksRoot.position = roomConductor.roomObject.transform.position;
                    blocksRoot.rotation = roomConductor.roomObject.transform.rotation;
                }
                else
                {
                    blocksRoot.localPosition = Vector3.zero;
                    blocksRoot.localRotation = Quaternion.identity;
                }
            }

            var blockTransformsList = new List<Transform>(blocksRoot.childCount);
            for (int x = 0; x < blocksRoot.childCount; x++)
            {
                blockTransformsList.Add(blocksRoot.GetChild(x));
            }

            foreach (var blockRootTransform in blockTransformsList)
            {
                blockRootTransform.tag = "Untagged";
                blockRootTransform.gameObject.SetActive(false);
                blockRootTransform.parent = null; // So it doesn't gets in the search later.

                if (Application.isPlaying)
                    Destroy(blockRootTransform.gameObject);
                else
                    DestroyImmediate(blockRootTransform.gameObject);
            }

            roofObjects = null;
            floorSpriteRenderers = null;
            wallSpriteRenderers = null;

            return true;
        }

        private void SetNoSaveFlags (GameObject go, bool recursive = true)
        {
            if (!Application.isPlaying)
            {
                var tr = go.transform;
                go.hideFlags = go.hideFlags | HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor | HideFlags.NotEditable;
                tr.hideFlags = tr.hideFlags | HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor | HideFlags.NotEditable;

                if (recursive)
                {
                    int childCount = tr.childCount;
                    for (int i = 0; i < childCount; i++)
                    {
                        var child = tr.GetChild(i);
                        if (child != null && child.gameObject != null)
                        {
                            SetNoSaveFlags(child.gameObject);
                        }
                    }
                }
            }
        }

        private void RebuildRoom()
        {
            if (!CleanupBlocksRoot())
                return;

            if (roomConductor == null)
            {
                Debug.LogWarningFormat("{0}: missing Room Conductor component, cannot rebuild room!", gameObject != null ? gameObject.name : "?");
                return;
            }

            roomConductor.ResetUnlockedCellsMap();

            var maxDimension = maxDimensions; // Cache it.

            roofObjects = null;
            floorSpriteRenderers = null;
            wallSpriteRenderers = null;

            List<GameObject> tmpObjectsList = new List<GameObject>(16);

            var blockSidesEnumLength = Enum.GetValues(typeof(BlockSides)).Length;

            // Spawn room parts.
            if (blocksRoot != null && blocksSetup != null && blocksSetup.Length > 0)
            {
                for (var i = 0; i < blocksSetup.Length; i++)
                {
                    var blockSetup = blocksSetup[i];
                    var blockNumberString = i.ToString("D3");
                    var blockCoords = blocksRuntimeData[i].blockCoords;

                    var roomBlock = new GameObject(blockNumberString);
                    SetNoSaveFlags(roomBlock);
                    roomBlock.transform.SetParent(blocksRoot);
                    roomBlock.transform.localPosition = GetBlockLocalPos(blockCoords);

                    blocksRuntimeData[i].expandMarkers = null;

                    if (blocksRuntimeData[i].unlocked)
                    {
                        // Spawn floors.
                        {
                            var roomPartFloorPrefab = roomPartFloorPrefabs.GetVariation(i);
                            if (roomPartFloorPrefab != null && !blockSetup.premadeFloor)
                            {
                                var instance = Instantiate(roomPartFloorPrefab);
                                instance.transform.SetParent(roomBlock.transform, false);
                                SetNoSaveFlags(instance);

                                // Hack and tweaks for tiling.
                                var tsr = instance.GetComponentInChildren<TiledSpriteRenderer>();
                                if (tsr != null)
                                {
                                    var offsetX = (blockCoords.x % 2 == 0) ? 0.0f : 0.5f;
                                    var offsetY = (blockCoords.y % 2 == 0) ? 0.0f : 0.5f;
                                    tsr.SetTiling(tsr.size, tsr.sprite, tsr.tileAnchor, new Vector2(offsetX, offsetY));
                                }
                            }
                        }

                        // Spawn roofs.
                        {
                            var roomPartRoofPrefab = roomPartRoofPrefabs.GetVariation(i);
                            if (roomPartRoofPrefab != null && !blockSetup.premadeRoof)
                            {
                                var instance = Instantiate(roomPartRoofPrefab);
                                SetNoSaveFlags(instance);
                                instance.transform.SetParent(roomBlock.transform, false);
                                instance.transform.localRotation = CalculateRotationVariativity(i);
                            }
                        }

                        for (var j = 0; j < 4; j++)
                        {
                            var sideId = siblingBlocksSides[j];
                            var cornerId = siblingBlocksCorners[j];
                            var upIdx = j % 4;
                            var leftIdx = (j + 3) % 4;

                            var siblingUpCoords = GetCoordinatesForSide(blockCoords, siblingBlocksSides[upIdx]);
                            var siblingLeftCoords = GetCoordinatesForSide(blockCoords, siblingBlocksSides[leftIdx]);
                            var siblingUpLeftCoords = GetCoordinatesForSide(blockCoords, siblingBlocksCorners[upIdx]);
                            var siblingUpRtd = siblingUpCoords.IsInConstraints(maxDimensions)
                                ? blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingUpCoords)]
                                : new BlockRuntimeData(new RoomCoordinates(-1, -1), false);
                            var siblingLeftRtd = siblingLeftCoords.IsInConstraints(maxDimensions)
                                ? blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingLeftCoords)]
                                : new BlockRuntimeData(new RoomCoordinates(-1, -1), false);
                            var siblingUpLeftRtd = siblingUpLeftCoords.IsInConstraints(maxDimensions)
                                ? blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingUpLeftCoords)]
                                : new BlockRuntimeData(new RoomCoordinates(-1, -1), false);

                            // Spawn wall center.
                            {
                                var roomPartWallCenterPrefab = roomPartWallCenterPrefabs.GetVariation(i * 4 + j);
                                if (roomPartWallCenterPrefab != null &&
                                    (blockSetup.premadeSides == null || !blockSetup.premadeSides.Contains(sideId)) &&
                                    !siblingUpRtd.unlocked)
                                {
                                    var instance = Instantiate(roomPartWallCenterPrefab);
                                    instance.name = string.Format("{0} ({1})"
                                        , roomPartWallCenterPrefab.name, sideId);
                                    SetNoSaveFlags(instance);
                                    instance.transform.SetParent(roomBlock.transform, false);
                                    instance.transform.localRotation = rotationAngles[j];
                                }
                            }

                            // Spawn wall side.
                            {
                                var roomPartWallSidePrefab = roomPartWallSidePrefabs.GetVariation(i * 4 + j);
                                if (roomPartWallSidePrefab != null &&
                                    (blockSetup.premadeSides == null || !blockSetup.premadeSides.Contains(cornerId)) &&
                                    !siblingUpRtd.unlocked &&
                                    siblingLeftRtd.unlocked &&
                                    !siblingUpLeftRtd.unlocked)
                                {
                                    var instance = Instantiate(roomPartWallSidePrefab);
                                    instance.name = string.Format("{0} ({1})"
                                        , roomPartWallSidePrefab.name, sideId);
                                    SetNoSaveFlags(instance);
                                    instance.transform.SetParent(roomBlock.transform, false);
                                    instance.transform.localRotation = rotationAngles[j];
                                }
                            }

                            // Spawn outward corner.
                            {
                                var roomPartCornerOutwardPrefab = roomPartCornerOutwardPrefabs.GetVariation(i * 4 + j);
                                if (roomPartCornerOutwardPrefab != null &&
                                    (blockSetup.premadeSides == null || !blockSetup.premadeSides.Contains(cornerId)) &&
                                    !siblingUpRtd.unlocked &&
                                    !siblingLeftRtd.unlocked)
                                {
                                    var instance = Instantiate(roomPartCornerOutwardPrefab);
                                    instance.name = string.Format("{0} ({1})"
                                        , roomPartCornerOutwardPrefab.name, cornerId);
                                    SetNoSaveFlags(instance);
                                    instance.transform.SetParent(roomBlock.transform, false);
                                    instance.transform.localRotation = rotationAngles[j];
                                }
                            }

                            // Spawn inward corner.
                            {
                                var roomPartCornerInwardPrefab = roomPartCornerInwardPrefabs.GetVariation(i * 4 + j);
                                if (roomPartCornerInwardPrefab != null &&
                                    (blockSetup.premadeSides == null || !blockSetup.premadeSides.Contains(cornerId)) &&
                                    siblingUpRtd.unlocked && siblingLeftRtd.unlocked && !siblingUpLeftRtd.unlocked)
                                {
                                    var instance = Instantiate(roomPartCornerInwardPrefab);
                                    instance.name = string.Format("{0} ({1})"
                                        , roomPartCornerInwardPrefab.name, cornerId);
                                    SetNoSaveFlags(instance);
                                    instance.transform.SetParent(roomBlock.transform, false);
                                    instance.transform.localRotation = rotationAngles[j];
                                }
                            }
                        }
                    }
                    else
                    {
                        roomConductor.SetUnlockedCellsMap
                            ( new RoomCoordinates(blockCoords.x * roomConductor.roomSetupDataInfo.blockSize - 1, blockCoords.y * roomConductor.roomSetupDataInfo.blockSize - 1)
                            , new RoomCoordinates(roomConductor.roomSetupDataInfo.blockSize + 1, roomConductor.roomSetupDataInfo.blockSize + 1));

                        // Spawn locked block (only if all 8 sibling sides are free).
                        {
                            var anyUnlockedBlockNearby = false;

                            for (int x = 0; x < blockSidesEnumLength; x++)
                            {
                                var siblingCoords = GetCoordinatesForSide(blockCoords, (BlockSides)x);
                                if (siblingCoords.IsInConstraints(maxDimensions))
                                {
                                    if (blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingCoords)].unlocked)
                                    {
                                        anyUnlockedBlockNearby = true;
                                        break;
                                    }
                                }
                            }

                            var roomPartLockedBlockPrefab = anyUnlockedBlockNearby
                                ? roomPartLockedNearBlockPrefab.GetVariation(i)
                                : roomPartLockedFarBlockPrefab.GetVariation(i);

                            if (roomPartLockedBlockPrefab != null)
                            {
                                var instance = Instantiate(roomPartLockedBlockPrefab);
                                SetNoSaveFlags(instance);
                                instance.transform.SetParent(roomBlock.transform, false);
                                instance.transform.localRotation = CalculateRotationVariativity(i);
                            }
                        }

                        // Spawn expand marker (hidden), only if main sides (North, West, South, East) are free.
                        {
                            tmpObjectsList.Clear();

                            bool anyUnlockedBlockNearby = false;
                            for (int j = 0; j < siblingBlocksSides.Length; j++)
                            {
                                var siblingCoords = GetCoordinatesForSide(blockCoords, siblingBlocksSides[j]);
                                if (siblingCoords.IsInConstraints(maxDimensions))
                                {
                                    if (blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingCoords)].unlocked)
                                    {
                                        anyUnlockedBlockNearby = true;
                                        if (roomPartExpandSideMarkerPrefab != null)
                                        {
                                            var instance = Instantiate(roomPartExpandSideMarkerPrefab);
                                            SetNoSaveFlags(instance);
                                            instance.transform.SetParent(roomBlock.transform, false);
                                            instance.transform.localRotation = rotationAngles[j];

                                            if (Application.isPlaying)
                                            {
                                                instance.SetActive(false);
                                                tmpObjectsList.Add(instance);
                                            }
                                        }
                                    }
                                }
                            }

                            if (anyUnlockedBlockNearby && roomPartExpandMarkerPrefab != null)
                            {
                                var instance = Instantiate(roomPartExpandMarkerPrefab);
                                SetNoSaveFlags(instance);
                                instance.transform.SetParent(roomBlock.transform, false);

                                var pointerEventsRouter = instance.GetComponent<IPointerEventsRouter>();
                                if (pointerEventsRouter != null)
                                {
                                    var blockIdx = i; // Copy it to local var, so it doesn't catches a variable that increases in a loop into closure scope .
                                    pointerEventsRouter.onPointerClick += delegate(PointerEventData ped) { OnExpandBlockClicked(blockIdx); };
                                }

                                if (Application.isPlaying)
                                {
                                    instance.SetActive(false);
                                    tmpObjectsList.Add(instance);
                                }
                            }

                            if (tmpObjectsList.Count > 0)
                            {
                                blocksRuntimeData[i].expandMarkers = tmpObjectsList.ToArray();
                            }
                        }
                    }
                }

                roofObjects = transform.FindDeepChildsWithTag(roomRoofTag).ToArray();

                if (Application.isPlaying)
                {
                    if (roofObjects != null)
                    {
                        foreach (var roofObject in roofObjects)
                        {
                            var pointerEventsRouter = roofObject.GetComponent<IPointerEventsRouter>();
                            if (pointerEventsRouter != null)
                            {
                                pointerEventsRouter.onPointerClick += (PointerEventData ped) => { StartTransitionToRoom(); };
                            }
                        }
                    }

                    wallSpriteRenderers = transform.GetComponentsInChildsForObjectsWithTag<TiledSpriteRenderer>
                        (roomWallTag, blocksSetup.Length * 50).ToArray();

                    floorSpriteRenderers = transform.GetComponentsInChildsForObjectsWithTag<TiledSpriteRenderer>
                        (roomFloorTag, blocksSetup.Length).ToArray();
                }
            }
        }

        int FindRoomBlockClosestToPosition(Vector3 worldPosition, FindRoomBlockMode findMode)
        {
            float minDistance = float.MaxValue;
            int bestBlockCandidate = -1;

            var blockSidesEnumLength = Enum.GetValues(typeof(BlockSides)).Length;

            for (int i = 0; i < blocksRuntimeData.Length; i++)
            {
                var brd = blocksRuntimeData[i];

                bool evaluateBlock = false;

                switch (findMode)
                {
                    case FindRoomBlockMode.Any:                // Any block.
                    {
                        evaluateBlock = true;
                    }; break;

                    case FindRoomBlockMode.LockedAny:          // Any locked block.
                    {
                        if (!brd.unlocked)
                            evaluateBlock = true;
                    }; break;

                    case FindRoomBlockMode.LockedEdge:         // Locked block with unlocked block nearby on any edge (including diagonal).
                    {
                        for (int j = 0; j < blockSidesEnumLength; j++)
                        {
                            var siblingCoords = GetCoordinatesForSide(brd.blockCoords, (BlockSides)j);
                            if (siblingCoords.IsInConstraints(maxDimensions))
                            {
                                if (blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingCoords)].unlocked)
                                {
                                    evaluateBlock = true;
                                    break;
                                }
                            }
                        }
                    }; break;

                    case FindRoomBlockMode.LockedSideOnly:     // Locked block with unlocked block nearby on any side (North, East, West, South).
                    {
                        for (int j = 0; j < siblingBlocksSides.Length; j++)
                        {
                            var siblingCoords = GetCoordinatesForSide(brd.blockCoords, siblingBlocksSides[j]);
                            if (siblingCoords.IsInConstraints(maxDimensions))
                            {
                                if (blocksRuntimeData[GetBlockIdxForRoomCoordinates(maxDimensions, siblingCoords)].unlocked)
                                {
                                    evaluateBlock = true;
                                    break;
                                }
                            }
                        }
                    }; break;

                    case FindRoomBlockMode.Unlocked:           // Any unlocked block.
                    {
                        if (brd.unlocked)
                            evaluateBlock = true;
                    }; break;
                }

                if (evaluateBlock)
                {
                    var blockCenterWorldPos = GetBlockCenterWorldPosition (i);

                    var distance = Vector3.Distance(blockCenterWorldPos, worldPosition);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        bestBlockCandidate = i;
                    }
                }
            }

            return bestBlockCandidate;
        }

        Vector3 GetBlockCenterWorldPosition (int blockIndex)
        {
            Assert.IsTrue(blockIndex >= 0 && blockIndex < blocksRuntimeData.Length);
            if (blockIndex >= 0 && blockIndex < blocksRuntimeData.Length)
            {
                var halfBlockCoords = new RoomCoordinates(roomConductor.roomSetupDataInfo.blockSize / 2, roomConductor.roomSetupDataInfo.blockSize / 2);

                var coords = new RoomCoordinates
                        ( blocksRuntimeData[blockIndex].blockCoords.x * roomConductor.roomSetupDataInfo.blockSize
                        , blocksRuntimeData[blockIndex].blockCoords.y * roomConductor.roomSetupDataInfo.blockSize) + halfBlockCoords;

                var position = roomConductor.roomObject.transform.TransformPoint(roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(coords));
                return position;
            }

            return (roomConductor != null) ? roomConductor.roomObject.transform.position : gameObject.transform.position;
        }

        void OnExpandBlockClicked(int blockIdx)
        {
            for (var i = 0; i < blocksRuntimeData.Length; i++)
            {
                if (blocksRuntimeData[i].expandMarkers != null)
                {
                    foreach (var expandMarker in blocksRuntimeData[i].expandMarkers)
                    {
                        if (i == blockIdx)
                        {
                            expandMarker.SetActive(true);

                            if (expandMarkerMaterialsReplacementPolicy != null &&
                                selectedExpandMarkerMaterialsReplacementPolicyName != null &&
                                selectedExpandMarkerMaterialsReplacementPolicyName != string.Empty)
                            {
                                var materialsReplacement = expandMarker.GetComponent<MaterialsReplacement>();
                                if (materialsReplacement != null)
                                {
                                    materialsReplacement.ReplaceMaterials(selectedExpandMarkerMaterialsReplacementPolicyName, expandMarkerMaterialsReplacementPolicy);
                                }
                            }
                        }
                        else
                        {
                            expandMarker.SetActive(false);
                        }
                    }
                }
            }

            RequestStartExpand(blockIdx);
        }

        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            if (blocksRuntimeData != null && blocksRoot != null)
            {
                for (var i = 0; i < blocksRuntimeData.Length; i++)
                {
                    var blockRTD = blocksRuntimeData[i];

                    if (UnityEditor.Selection.Contains(gameObject))
                    {
                        var blockLocalPos = GetBlockLocalPos(blockRTD.blockCoords);

                        Handles.Label
                            ( blocksRoot.transform.position + blockLocalPos
                            , string.Format("{0} - ({1})", i.ToString("D3"), blockRTD.blockCoords)
                            , EditorGizmoHelper.instance.GetOverheadLabelStyle());
                    }
                }
            }
        }
#endif // #if UNITY_EDITOR
        #endregion

        #region Private Editor-only functions.
#if UNITY_EDITOR
        public override void EditorUpdate()
        {
            if (roomConductor != null && roomConductor.editorRoomSetupData.roomExpandMode != RoomExpandMode.BlockGrid)
                roomConductor.editorRoomSetupData.roomExpandMode = RoomExpandMode.BlockGrid;

            base.EditorUpdate();

            var maxDimensions = this.maxDimensions; // Cache it.

            bool forceUpdate = false;
            if (blocksSetup == null)
            {
                blocksSetup = new BlockSetupInfo[maxDimensions.x * maxDimensions.y];
                forceUpdate = true;
            }
            else if (blocksSetup.Length != maxDimensions.x * maxDimensions.y)
            {
                Array.Resize(ref blocksSetup, maxDimensions.x * maxDimensions.y);
                forceUpdate = true;
            }

            // Rebuild room only if essential editor state is changed: our fields, base fields, Room Conductor etc.
            var currentEditorState = EditorGetStateHashCode();
            if (forceUpdate || editorStateHashCode == null || currentEditorState != editorStateHashCode.Value)
            {
                editorStateHashCode = currentEditorState;

                // Must be done via delay call or else bad thing happen!
                EditorApplication.delayCall += () =>
                {
                    UpdateRuntimeBlocksData();
                    RebuildRoom();
                    editorStateHashCode = EditorGetStateHashCode();
                };
            }
        }

        public void EditorVisualize(bool isActive)
        {
            if (Application.isEditor && gameObject.scene.IsValid() && gameObject.scene.isLoaded && !Application.isPlaying)
            {
                EditorApplication.delayCall += () =>
                {
                    UpdateRuntimeBlocksData();
                    RebuildRoom();
                    editorStateHashCode = EditorGetStateHashCode();
                    UpdateActiveRoomToggles(isActive);
                };
            }
        }

        public BlockRuntimeData[] EditorGetRoomBlocksRuntimeData()
        {
            return blocksRuntimeData;
        }

        public override int EditorGetStateHashCode()
        {
            int hashCode = base.EditorGetStateHashCode();

            if (blocksSetup != null && blocksSetup.Length > 0)
            {
                foreach (var b in blocksSetup)
                {
                    hashCode ^= b.EditorGetStateHashCode();
                }
            }

            hashCode ^= roomPartCornerInwardPrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartCornerOutwardPrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartFloorPrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartRoofPrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartWallCenterPrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartWallSidePrefabs.EditorGetStateHashCode();
            hashCode ^= roomPartLockedFarBlockPrefab.EditorGetStateHashCode();
            hashCode ^= roomPartLockedNearBlockPrefab.EditorGetStateHashCode();
            hashCode ^= (roomPartExpandMarkerPrefab != null) ? roomPartExpandMarkerPrefab.GetInstanceID() : (0).GetHashCode();
            hashCode ^= (roomPartExpandSideMarkerPrefab != null) ? roomPartExpandSideMarkerPrefab.GetInstanceID() : (0).GetHashCode();
            hashCode ^= rotationVariativityMultiplier.GetHashCode();
            hashCode ^= rotationVariativityModulus.GetHashCode();

            return hashCode;
        }

#endif // #if UNITY_EDITOR
        #endregion
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(BlockGridRoomView), true)]
    public class BlockGridRoomViewEditor : Editor
    {
        private static bool activeRoomToggglesVisualize = false;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var gcv = (BlockGridRoomView)target;

            DrawDefaultInspector();

            if (gcv.gameObject.scene.IsValid() && gcv.gameObject.scene.isLoaded && !Application.isPlaying)
            {
                GUILayout.Space(8);
                GUIStyle headerLabel = new GUIStyle(GUI.skin.label);
                headerLabel.fontStyle = FontStyle.Bold;
                GUILayout.Label("Debug and Visualization", headerLabel);

                var newActiveRoomToggglesVisualize = activeRoomToggglesVisualize;
                if (!Application.isPlaying)
                {
                    newActiveRoomToggglesVisualize = GUILayout.Toggle(activeRoomToggglesVisualize, "Active Room");
                }

                if (newActiveRoomToggglesVisualize != activeRoomToggglesVisualize)
                {
                    activeRoomToggglesVisualize = newActiveRoomToggglesVisualize;
                    gcv.EditorVisualize(activeRoomToggglesVisualize);
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif // #if UNITY_EDITOR
}
