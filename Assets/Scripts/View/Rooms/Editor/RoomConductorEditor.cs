﻿using System;
using UnityEditor;
using UnityEngine;
using View.EditorHelpers;

namespace View.Rooms.Editor
{
    [CustomEditor(typeof(RoomConductor), true)]
    public class RoomConductorEditor : UnityEditor.Editor
    {
        private static readonly Color rotationColor = new Color(0.929f, 0.203f, 0.215f, 1.0f);
        private static readonly Color zoneColor = new Color(0.07f, 0.789f, 0.777f, 0.5f);
        private static readonly Color zoneOutlineColor = new Color(0.175f, 0.4179f, 0.574f, 0.7f);
        private static readonly Color handleColor = new Color(0.5f, 0.5f, 0.5f, 0.7f);

        private static bool visualZonesEditor = false;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var roomConductor = (RoomConductor)target;

            DrawDefaultInspector();

/*
            var roomSetupDataProperty = serializedObject.FindProperty("roomSetupData");
            if (roomSetupDataProperty != null)
            {
                EditorGUILayout.PropertyField(roomSetupDataProperty);
            }
*/

            if (roomConductor.gameObject.scene.IsValid() && roomConductor.gameObject.scene.isLoaded)
            {
                visualZonesEditor = GUILayout.Toggle(visualZonesEditor, "Toggle Visual Zones Editor");

                if (GUILayout.Button("Toggle Occupation Map"))
                {
                    roomConductor.ToggleDbgShowOccupationMap();
                }

                if (GUILayout.Button("Update Occupation Map"))
                {
                    roomConductor.UpdateOccupationMap();
                }

                if (GUILayout.Button("Toggle Unlocked Cells Map"))
                {
                    roomConductor.ToggleDbgShowUnlockedCellsMap();
                }
            }
            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            if (visualZonesEditor)
            {
                var roomConductor = (RoomConductor)target;

                if (roomConductor.zones != null)
                {
                    var originalColor = Handles.color;

                    foreach (var zone in roomConductor.zones)
                    {
                        var zonePos = roomConductor.transform.TransformPoint(new Vector3(zone.position.x, zone.height, zone.position.y));
                        var newPosition = roomConductor.transform.InverseTransformPoint(Handles.PositionHandle (zonePos, roomConductor.transform.rotation));
                        zone.position = new Vector2(newPosition.x, newPosition.z);
                        zone.height = newPosition.y;

                        if (zone.name != null && zone.name != string.Empty)
                            Handles.Label(zonePos, zone.name, EditorGizmoHelper.instance.GetOverheadLabelStyle());

                        Handles.color = zoneColor;

                        switch (zone.type)
                        {
                            case RoomZone.ZoneType.Point:
                            {
                            }; break;

                            case RoomZone.ZoneType.Circle:
                            {
                                Handles.DrawSolidDisc(zonePos, Vector3.up, zone.size.x);
                                Handles.color = zoneOutlineColor;
                                Handles.DrawWireDisc(zonePos, Vector3.up, zone.size.x);
                                Handles.color = handleColor;
                                var radius = Math.Max(CoordsMapper.defaultCellSize * 0.1f, Handles.RadiusHandle(roomConductor.transform.rotation, zonePos, zone.size.x));
                                zone.size = new Vector2(radius, radius);
                            }; break;

                            case RoomZone.ZoneType.Rect:
                            {
                                var sizePos1 = zonePos + new Vector3(  zone.size.x * 0.5f,   0.0f,   zone.size.y * 0.5f);
                                var sizePos2 = zonePos + new Vector3(- zone.size.x * 0.5f,   0.0f,   zone.size.y * 0.5f);
                                var sizePos3 = zonePos + new Vector3(  zone.size.x * 0.5f,   0.0f, - zone.size.y * 0.5f);
                                var sizePos4 = zonePos + new Vector3(- zone.size.x * 0.5f,   0.0f, - zone.size.y * 0.5f);
                                var newSizePos = Handles.PositionHandle (sizePos1, roomConductor.transform.rotation) - zonePos;
                                Handles.DrawSolidRectangleWithOutline ( new Vector3[] { sizePos1, sizePos3, sizePos4, sizePos2 }, zoneColor, zoneOutlineColor );
                                zone.size = new Vector2(Math.Max(CoordsMapper.defaultCellSize * 0.1f, newSizePos.x * 2.0f), Math.Max(CoordsMapper.defaultCellSize * 0.1f, newSizePos.z * 2.0f));
                            }; break;
                        }

                        Handles.color = rotationColor;
                        if (zone.headingRandomizationAngle > 0)
                        {
                            Handles.DrawWireArc ( zonePos
                                                , Vector3.up
                                                , (Quaternion.AngleAxis(zone.heading - zone.headingRandomizationAngle * 0.5f, Vector3.up) * Vector3.forward).normalized
                                                , zone.headingRandomizationAngle
                                                , roomConductor.coordsMapper.cellSize > 0 ? roomConductor.coordsMapper.cellSize : 1.0f);
                            Handles.DrawLine ( zonePos
                                                , zonePos + (Quaternion.AngleAxis(zone.heading - zone.headingRandomizationAngle * 0.5f, Vector3.up) * Vector3.forward).normalized *
                                                    ((roomConductor.coordsMapper.cellSize > 0.0f) ? roomConductor.coordsMapper.cellSize : 1.0f));
                            Handles.DrawLine ( zonePos
                                                , zonePos + (Quaternion.AngleAxis(zone.heading + zone.headingRandomizationAngle * 0.5f, Vector3.up) * Vector3.forward).normalized *
                                                    ((roomConductor.coordsMapper.cellSize > 0.0f) ? roomConductor.coordsMapper.cellSize : 1.0f));
                        }
                        else
                        {
                            Handles.DrawLine ( zonePos
                                                , zonePos + (Quaternion.AngleAxis(zone.heading, Vector3.up) * Vector3.forward).normalized *
                                                    ((roomConductor.coordsMapper.cellSize > 0.0f) ? roomConductor.coordsMapper.cellSize : 1.0f));
                        }
                    }

                    Handles.color = originalColor;
                }

                if (GUI.changed)
                    EditorUtility.SetDirty(target);
            }
        }
    }
}
