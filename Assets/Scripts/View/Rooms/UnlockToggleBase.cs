﻿using CustomAssets;
using UnityEngine;
using View.Actions;

namespace View.Rooms
{
    [DisallowMultipleComponent]
    public abstract class UnlockToggleBase : MonoBehaviour
    {
        #region Types.
        public enum Mode
        {
            AlwaysShowDefaultMaterials = 0,
            AlwaysHide = 1,
            AutoHideDefaultMaterials = 2,
            AlwaysShowReplaceMaterials = 3,
            AutoHideReplaceMaterials = 4,
            NoChangeReplaceMaterials = 5,
            NoChangeDefaultMaterials = 6,
        }
        #endregion
        
        #region Protected properties.
        protected MaterialsReplacement materialsReplacement
        {
            get
            {
                if (cachedMaterialsReplacement == null)
                    cachedMaterialsReplacement = GetComponent<MaterialsReplacement>();

                return cachedMaterialsReplacement;
            }
        }

        protected AutoHider autoHider
        {
            get
            {
                if (cachedAutoHider == null)
                    cachedAutoHider = GetComponent<AutoHider>();

                return cachedAutoHider;
            }
        }
        #endregion

        #region Private data.
        private MaterialsReplacement cachedMaterialsReplacement;
        private AutoHider cachedAutoHider;
        private bool replacedMaterials = false;
        private Mode? cachedMode;
        private bool? cachedActiveState;
        #endregion

        #region Public API.
        protected void SetMode
            ( Mode mode
            , bool active
            , string inactiveMaterialReplacementPolicyName
            , MaterialsReplacementPolicy materialsReplacementPolicy = null)
        {
            if (cachedMode == null || cachedActiveState == null || cachedMode.Value != mode || cachedActiveState.Value != active)
            {
                cachedMode = mode;

                if (autoHider != null && mode != Mode.NoChangeDefaultMaterials && mode != Mode.NoChangeReplaceMaterials)
                {
                    autoHider.enabled = (active && (mode == Mode.AutoHideDefaultMaterials || mode == Mode.AutoHideReplaceMaterials));
                }

                if (materialsReplacementPolicy != null &&
                    materialsReplacement != null &&
                    inactiveMaterialReplacementPolicyName != null &&
                    inactiveMaterialReplacementPolicyName != string.Empty)
                {
                    if (mode == Mode.AutoHideReplaceMaterials || mode == Mode.AlwaysShowReplaceMaterials || mode == Mode.NoChangeReplaceMaterials)
                    {
                        materialsReplacement.ReplaceMaterials(inactiveMaterialReplacementPolicyName, materialsReplacementPolicy);
                        replacedMaterials = true;
                    }
                    else
                    {
                        if (replacedMaterials)
                        {
                            materialsReplacement.ResetBackMaterials(inactiveMaterialReplacementPolicyName);
                            replacedMaterials = false;
                        }
                    }
                }

                if (mode != Mode.NoChangeDefaultMaterials && mode != Mode.NoChangeReplaceMaterials)
                {
                    cachedActiveState = active;

                    var activate = mode != Mode.AlwaysHide;
                    if (gameObject.activeSelf != activate)
                        gameObject.SetActive(activate);
                }
            }
        }
        #endregion
    }
}