﻿using System;
using Data.Room;
using UnityEngine;
using UnityEngine.EventSystems;
using View.Actors;

namespace View.Rooms
{
    public delegate bool TransitionToRoomCheckRequest(string roomName);
    public delegate void RoomUnlockRequest(string roomName, int desiredExpandLevel);
    public delegate bool PrepareToExpandRequest(string roomName);
    public delegate void StartExpandRequest(string roomName, int blockIndex);
    public delegate void DeactivateRoomRequest(string roomName); 
    public delegate void ActivateRoomRequest(string roomName); 

    public delegate void FloorPointerClickEvent(PointerEventData pointerEventData, string roomName);
    public delegate void FloorPointerDownEvent(PointerEventData pointerEventData, string roomName);
    public delegate void FloorPointerUpEvent(PointerEventData pointerEventData, string roomName);

    public delegate void OnCannotExpandEvent();

    public enum ExpandAvailabilityState
    {
        None = 0,
        ExpandAvailable,
        ExpandPending,
        ExpandComplete
    }

    public interface IRoomView
    {
        event TransitionToRoomCheckRequest transitionToRoomCheckRequest;
        event RoomUnlockRequest roomUnlockRequest;
        event PrepareToExpandRequest prepareToExpandRequest;
        event StartExpandRequest startExpandRequest;
        event DeactivateRoomRequest deactivateRoomRequest;
        event ActivateRoomRequest activateRoomRequest;

        event FloorPointerClickEvent floorPointerClickEvent;
        event FloorPointerDownEvent floorPointerDownEvent;
        event FloorPointerUpEvent floorPointerUpEvent;

        event OnCannotExpandEvent onCannotExpand;

        bool isActivated
        {
            get;
        }

        RoomConductor roomConductor
        {
            get;
        }

        string roomName
        {
            get;
        }

        GameObject viewObject
        {
            get;
        }

        GameObject roomObject
        {
            get;
        }

        DisplayMode actorsDisplayMode
        {
            get;
        }

        Vector3 homeFocusPosition
        {
            get;
        }

#if UNITY_EDITOR
        void EditorUpdate();
#endif // #if UNITY_EDITOR

        void SetActiveState(bool state);
        void SetExpandAvailableState(ExpandAvailabilityState state, float timeLeft);
        void StartTransitionToRoom();
        void TransitionToUnlockedRoom();
        void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate);
        void SetWallsSprite(Sprite sprite);
        void SetFloorSprite(Sprite sprite);
        void StartExpand();
        bool CheckForExpandStart(bool canExpandX, bool canExpandY, int blockIndex);
        void ExpandEnded();
        void UpdateExpand();
        bool GetZoneCenter(string zoneName, out Vector3 position);
        bool GetZoneCenterLocal(string zoneName, out Vector3 position);
        bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position);
        bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition);
        bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position, out Quaternion rotation);
        bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition, out Quaternion rotation);
        EquipmentPlacement FindFreeSpaceForEquipment(RoomCoordinates objectSize);
        void ShowLockedView (string message);
        int GetRoomZonesCount (string zoneNamePrefix);
    }
}
