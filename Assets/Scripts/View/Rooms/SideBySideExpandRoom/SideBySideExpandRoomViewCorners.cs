﻿using UnityEngine;

namespace View.Rooms.SideBySideExpandRoom
{
    public class SideBySideExpandRoomViewCorners
    {
        private GameObject[] topCorner      = new GameObject[4];
        private GameObject[] baseTopCorner  = new GameObject[4];
        private GameObject[,] corner        = new GameObject[4,4];

        private SideBySideExpandRoomViewSizes sz    = null;

        public void Init(Transform parent, SideBySideExpandRoomViewSizes _buildingSz)
        {
            sz = _buildingSz;

            if (parent != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    topCorner[i] = parent.Find("WallTopCorner" + (i + 1).ToString()).gameObject;
                    baseTopCorner[i] = parent.Find("WallBaseTopCorner" + (i + 1).ToString()).gameObject;

                    for (int j = 0; j < 4; j++)
                    {
                        corner[i, j] = parent.Find("WallCorner" + (i + 1).ToString() + (j + 1).ToString()).gameObject;
                    }
                }
            }
        }

        public void Update()
        {
            for (int i = 0; i < 4; i++)
            {
                UpdateTopCorner(i);
                UpdateBaseTopCorner(i);
                UpdateCorner(i);
            }
        }

        private void UpdateTopCorner_(GameObject plane, int idx, float height)
        {
            Vector3 scale = new Vector3 (sz.wallWidth, sz.wallWidth, 1);
            Vector2 tile = new Vector2 (1, 1);

            var rect = sz.corners[idx];

            Vector3 pos = new Vector3(rect.center.x, height, rect.center.y);

            plane.transform.localScale = scale;
            plane.transform.localPosition = pos;
            plane.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = tile;
        }

        public void UpdateTopCorner(int idx)
        {
            var plane = topCorner[idx];
            UpdateTopCorner_(plane, idx, sz.wallHeight);
        }

        public void UpdateBaseTopCorner(int idx)
        {
            var plane = baseTopCorner[idx];
            UpdateTopCorner_(plane, idx, sz.baseHeight);
        }

        public void UpdateCorner(int idx)
        {
            var rect = sz.corners[idx];

            Vector3 pos = new Vector3(rect.center.x, sz.wallHeight / 2, rect.y);

            corner[idx, 0].transform.localPosition = pos;

            pos.x = rect.xMax;
            pos.z = rect.center.y;

            corner[idx, 1].transform.localPosition = pos;

            pos.x = rect.center.x;
            pos.z = rect.yMax;

            corner[idx, 2].transform.localPosition = pos;

            pos.x = rect.x;
            pos.z = rect.center.y;

            corner[idx, 3].transform.localPosition = pos;
        }

        public void UpdateVisibility(bool[] visibility)
        {
            SetVisibible(0, visibility[0] || visibility[3]);
            SetVisibible(1, visibility[0] || visibility[1]);
            SetVisibible(2, visibility[1] || visibility[2]);
            SetVisibible(3, visibility[2] || visibility[3]);
        }

        private void SetVisibible(int idx, bool visibility)
        {
            topCorner[idx].SetActive(visibility);

            for (int i = 0; i < 4; i++)
            {
                corner[idx, i].SetActive(visibility);
            }
        }
    }
}