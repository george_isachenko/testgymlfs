﻿using UnityEngine;

namespace View.Rooms.SideBySideExpandRoom
{
    public class SideBySideExpandRoomViewBase
    {
        private GameObject[] baseTop        = new GameObject[4];
        private GameObject[] baseSide       = new GameObject[4];

        private MaterialPropertyBlock materialPropBlock;
        private int _MainTex_ST;

        private SideBySideExpandRoomViewSizes sz;

        public void Init(Transform parent, SideBySideExpandRoomViewSizes _sz)
        {

            materialPropBlock = new MaterialPropertyBlock();
            _MainTex_ST = Shader.PropertyToID("_MainTex_ST");


            sz = _sz;
            if (parent != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    baseTop[i] = parent.Find("WallBaseTop" + (i + 1).ToString()).gameObject;
                    baseSide[i] = parent.Find("WallBaseSide" + (i + 1).ToString()).gameObject;
                }
            }
        }

        public void Update()
        {
            for (int i = 0; i < 4; i++)
            {
                UpdateTop(i);
                UpdateSide(i);
            }
        }

        private void UpdateTop(int idx)
        {
            // POS
            var rect = sz.sides[idx];
            Vector3 pos = new Vector3(rect.center.x, sz.baseHeight, rect.center.y);
            baseTop[idx].transform.localPosition = pos;
            // SCALE
            float length = idx % 2 == 0 ? sz.sizeGeom.x : sz.sizeGeom.y;
            Vector3 scale = new Vector3(sz.wallWidth, length , 1);
            baseTop[idx].transform.localScale = scale;
            // TILE
            Vector4 tile = new Vector4 (1f, length * 10f / 2f, 0, 0);
            //materialTop[idx].Clear();
            materialPropBlock.SetVector(_MainTex_ST, tile);
            baseTop[idx].GetComponent<MeshRenderer>().SetPropertyBlock(materialPropBlock);
        }

        private void UpdateSide(int idx)
        {
            // POS
            Vector3 pos = sz.GetOutsideWallPos(idx);
            pos.y = sz.baseHeight / 2f;
            baseSide[idx].transform.localPosition = pos;
            // SCALE
            float length = (idx % 2 == 0 ? sz.sizeGeom.x : sz.sizeGeom.y) + sz.wallWidth*2f;
            Vector3 scale = new Vector3(length, sz.baseHeight, 1);
            baseSide[idx].transform.localScale = scale;
            // TILE
            Vector4 tile = new Vector4 (length * 7f, 0.2f, 0, 0);
            materialPropBlock.SetVector(_MainTex_ST, tile);
            baseSide[idx].GetComponent<MeshRenderer>().SetPropertyBlock(materialPropBlock);
        }

    }

}