﻿using UnityEngine;
using System;
using Data.Room;

namespace View.Rooms.SideBySideExpandRoom
{
    [Serializable]
    public class SideBySideExpandRoomViewSizes
    {
        public Action animationUpdateCallback;
        public Action animationCompleteCallback;

        public const float defaultCellSize = 0.1f;

        public Rect[] corners { get; private set; }
        public Rect[] sides { get; private set; }

        public RoomConductor conductor;

        public float wallWidth = 0.06f;
        public float wallHeight = 0.6f;
        public float baseHeight = 0.12f;

        private GameObject parent = null;
        private float sizeAnimationTime = 1.0f;
        private Vector2 _sizeGeom;
        private Vector2 _maxSizeCells;
        private Vector2 _sizeCells;
        private float _cellSize = defaultCellSize;
        int sizeAnimationTweenId = 0;

        public SideBySideExpandRoomViewSizes (GameObject parent, RoomConductor conductor, float sizeAnimationTime)
        {
            corners   = new Rect[4];
            sides     = new Rect[4];

            this.parent = parent;
            this.conductor = conductor;
            this.sizeAnimationTime = sizeAnimationTime;
        }

        public float cellSize
        {
            get { return _cellSize; }
        }

        public Vector2 sizeGeom
        {
            get
            {
                return _sizeGeom;
            }
        }

        public Vector2 sizeCells
        {
            get
            {
                return _sizeCells;
            }
        }

        public Vector2 maxSizeCells
        {
            get
            {
                return _maxSizeCells;
            }
        }

        public void Update(RoomCoordinates maxSize, RoomCoordinates size, bool animate, float cellSize = defaultCellSize)
        {
            //Debug.LogFormat("ClubViewSizes.Update: Old Size: {0}, size: {1}.", _sizeCells, size);

            _maxSizeCells = new Vector2(maxSize.x, maxSize.y);
            var newSize = new Vector2(size.x, size.y);

            if (_sizeCells != newSize)
            {
                if (animate)
                {
                    if (sizeAnimationTweenId != 0)
                    {
                        LeanTween.cancel(sizeAnimationTweenId);
                        sizeAnimationTweenId = 0;
                    }

                    sizeAnimationTweenId = LeanTween.value (parent, UpdateSizes, _sizeCells, newSize, sizeAnimationTime)
                        .setOnComplete(UpdateSizesComplete).id;
                }
                else
                {
                    UpdateSizes (newSize);
                    UpdateSizesComplete();
                }
            }
        }

        void UpdateSizes(Vector2 size)
        {
            _sizeCells = size;
            _sizeGeom = new Vector2(size.x * cellSize, size.y * cellSize);

            for (int i = 0; i < 4; i++)
            {
                corners[i].width = wallWidth;
                corners[i].height = wallWidth;
            }

            var indentX2 = wallWidth + sizeGeom.x;
            var indentY2 = wallWidth + sizeGeom.y;

            corners[0].x = 0;
            corners[0].y = 0;

            corners[1].x = indentX2;
            corners[1].y = 0;

            corners[2].x = indentX2;
            corners[2].y = indentY2;

            corners[3].x = 0;
            corners[3].y = indentY2;

            sides[0].width = sides[2].width = sizeGeom.x;
            sides[0].height = sides[2].height = wallWidth;
            sides[1].width = sides[3].width = wallWidth;
            sides[1].height = sides[3].height = sizeGeom.y;

            sides[0].x = wallWidth;
            sides[0].y = 0;

            sides[1].x = indentX2;
            sides[1].y = wallWidth;

            sides[2].x = wallWidth;
            sides[2].y = indentY2;

            sides[3].x = 0;
            sides[3].y = wallWidth;

            if (animationUpdateCallback != null)
                animationUpdateCallback();
        }

        void UpdateSizesComplete()
        {
            if (animationCompleteCallback != null)
                animationCompleteCallback();
        }

        public Vector3 GetOutsideWallPos(int idx)
        {
            var rect = sides[idx];
            Vector3 pos = new Vector3();

            switch (idx)
            {
                case 0:
                pos.x = rect.center.x;
                pos.z = rect.yMin;
                break;
                case 1:
                pos.x = rect.xMax;
                pos.z = rect.center.y;
                break;
                case 2:
                pos.x = rect.center.x;
                pos.z = rect.yMax;
                break;
                case 3:
                pos.x = rect.xMin;
                pos.z = rect.center.y;
                break;
            }

            return pos;
        }
    }
}