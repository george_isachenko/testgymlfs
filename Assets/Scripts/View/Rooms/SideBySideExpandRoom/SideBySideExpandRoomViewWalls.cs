﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace View.Rooms.SideBySideExpandRoom
{
    public static class SideBySideExpandRoomPlaneResizer
    {
        static private MaterialPropertyBlock materialPropBlock  = new MaterialPropertyBlock();
        static private int _MainTex_ST = Shader.PropertyToID("_MainTex_ST");

        public static void UpdatePlane(TiledSpriteRenderer plane, Vector3 scale, Vector3 pos, Vector4 tile)
        {
            if (plane != null)
            {
                plane.transform.localScale = scale;
                plane.transform.localPosition = pos;
                plane.size = tile;
            }
        }

        public static void UpdatePlane(GameObject plane, Vector3 scale, Vector3 pos, Vector4 tile)
        {
            if (plane != null)
            {
                plane.transform.localScale = scale;
                plane.transform.localPosition = pos;

                materialPropBlock.SetVector(_MainTex_ST, tile);
                plane.GetComponent<MeshRenderer>().SetPropertyBlock(materialPropBlock);
            }
        }
    }

    public class ClubViewWalls
    {
        private TiledSpriteRenderer[]   wall          = new TiledSpriteRenderer[4];
        private TiledSpriteRenderer[]   wallOut       = new TiledSpriteRenderer[4];
        private GameObject[]            top           = new GameObject[4];
        private Transform[]             collider      = new Transform[4];
        private NavMeshObstacle         wall2Obstacle;
        private NavMeshObstacle         wall3Obstacle;
        private SideBySideExpandRoomViewSizes           sz;

        public void Init(Transform parent, SideBySideExpandRoomViewSizes _sz)
        {
            sz = _sz;
            if (parent != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    wall[i] = parent.GetComponentInChild<TiledSpriteRenderer>(string.Format("Wall{0}", (i + 1)));
                    wallOut[i] = parent.GetComponentInChild<TiledSpriteRenderer>(string.Format("Wall{0}_Out", (i + 1)));
                    top[i] = parent.Find("WallTop" + (i + 1).ToString()).gameObject;
                    collider[i] = wall[i].transform.Find("Collider");
                }

                wall2Obstacle = parent.GetComponentInChild<NavMeshObstacle>("Wall2_Obstacle");
                wall3Obstacle = parent.GetComponentInChild<NavMeshObstacle>("Wall3_Obstacle");
            }
        }

        public void Update()
        {
            var actualSize = sz.sizeGeom;
            var sizeCells = sz.sizeCells;
            var maxSizeCells = sz.maxSizeCells;
            var halfSize = actualSize * 0.5f;
            var cellSize = sz.cellSize;

            var scaleX = new Vector3 (sz.wallHeight, sz.wallHeight, 1);
            var scaleY = new Vector3 (sz.wallHeight, sz.wallHeight, 1);

            var tileX = new Vector2 (sizeCells.x / 6.0f, 1f);
            var tileY = new Vector2 (sizeCells.y / 6.0f, 1f);

            var localPos1 = new Vector3 (sz.wallWidth + halfSize.x, sz.wallHeight * 0.5f, sz.wallWidth);
            var localPos2 = new Vector3 (sz.wallWidth + actualSize.x, sz.wallHeight * 0.5f, halfSize.y + sz.wallWidth);
            var localPos3 = new Vector3 (sz.wallWidth + halfSize.x, sz.wallHeight * 0.5f, actualSize.y + sz.wallWidth);
            var localPos4 = new Vector3 (sz.wallWidth, sz.wallHeight * 0.5f, halfSize.y + sz.wallWidth);

            SideBySideExpandRoomPlaneResizer.UpdatePlane(wall[0], scaleX, localPos1, tileX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wall[1], scaleY, localPos2, tileY);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wall[2], scaleX, localPos3, tileX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wall[3], scaleY, localPos4, tileY);

            var _localPos1 = new Vector3 (sz.wallWidth + halfSize.x, sz.wallHeight * 0.5f, 0);
            var _localPos2 = new Vector3 (sz.wallWidth*2 + actualSize.x, sz.wallHeight * 0.5f, halfSize.y + sz.wallWidth);
            var _localPos3 = new Vector3 (sz.wallWidth + halfSize.x, sz.wallHeight * 0.5f, actualSize.y + sz.wallWidth*2);
            var _localPos4 = new Vector3 (0, sz.wallHeight * 0.5f, halfSize.y + sz.wallWidth);

            SideBySideExpandRoomPlaneResizer.UpdatePlane(wallOut[0], scaleX, _localPos1, tileX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wallOut[1], scaleY, _localPos2, tileY);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wallOut[2], scaleX, _localPos3, tileX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(wallOut[3], scaleY, _localPos4, tileY);

            var scaleWallWidthf = sz.wallWidth;
            var scaleWallTopX = new Vector3 (scaleWallWidthf, actualSize.x, 1);
            var scaleWallTopY = new Vector3 (scaleWallWidthf, actualSize.y, 1);
            var tileWallTopX = new Vector4 (1f, cellSize * 0.5f, 0, 0);
            var tileWallTopY = new Vector4 (1f, cellSize * 0.5f, 0, 0);

            localPos1.y = sz.wallHeight;
            localPos1.z -= sz.wallWidth * 0.5f;

            localPos2.y = sz.wallHeight;
            localPos2.x += sz.wallWidth * 0.5f;

            localPos3.y = sz.wallHeight;
            localPos3.z += sz.wallWidth * 0.5f;

            localPos4.y = sz.wallHeight;
            localPos4.x -= sz.wallWidth * 0.5f;

            SideBySideExpandRoomPlaneResizer.UpdatePlane(top[0], scaleWallTopX, localPos1, tileWallTopX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(top[1], scaleWallTopY, localPos2, tileWallTopY);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(top[2], scaleWallTopX, localPos3, tileWallTopX);
            SideBySideExpandRoomPlaneResizer.UpdatePlane(top[3], scaleWallTopY, localPos4, tileWallTopY);

            for (int i = 0; i < 4; i++)
            {
                float scale1d = (i % 2 == 0 ? sz.sizeGeom.x : sz.sizeGeom.y) / wall[i].transform.localScale.y;
                Vector3 scale = new Vector3(scale1d ,1 ,1);
                collider[i].localScale = scale;
            }

            if (wall2Obstacle != null)
            {
                wall2Obstacle.center = new Vector3 ( maxSizeCells.x * 0.5f * cellSize + sz.wallWidth * 0.5f
                                                   , wall2Obstacle.center.y
                                                   , (sizeCells.y + ((maxSizeCells.y - sizeCells.y) * 0.5f)) * cellSize + sz.wallWidth * 0.75f);
                wall2Obstacle.size =   new Vector3 ( Math.Max(0.0001f, maxSizeCells.x * cellSize - sz.wallWidth)
                                                   , wall2Obstacle.size.y
                                                   , Math.Max(0.0001f, (maxSizeCells.y - sizeCells.y) * cellSize));
                wall2Obstacle.gameObject.SetActive(true);
            }

            if (wall3Obstacle != null)
            {
                wall3Obstacle.center = new Vector3  ( (sizeCells.x + ((maxSizeCells.x - sizeCells.x) * 0.5f)) * cellSize  + sz.wallWidth * 0.75f
                                                    , wall2Obstacle.center.y
                                                    , maxSizeCells.y * 0.5f * cellSize + sz.wallWidth * 0.5f);
                wall3Obstacle.size =   new Vector3  ( Math.Max(0.0001f, (maxSizeCells.x - sizeCells.x) * cellSize)
                                                    , wall2Obstacle.size.y
                                                    , Math.Max(0.0001f, maxSizeCells.y * cellSize - sz.wallWidth));
                wall3Obstacle.gameObject.SetActive(true);
            }
        }

        public bool[] GetVisibility(Vector3 viewPoint)
        {
            bool[] visibility = new bool[4];
            for (int i = 0; i < 4; i++)
            {
                visibility[i] = IsWallVisible(i, viewPoint);
            }

            return visibility;
        }

        public bool IsWallVisible(int wallIdx, Vector3 viewPoint)
        {
            if (wallIdx > 3)
                return false;

            if (Application.isPlaying)
            {

                var wallDir = wall [wallIdx].transform.forward;
                var viewDir = viewPoint - wall [wallIdx].transform.position;

                return Vector3.Angle(wallDir, viewDir) > 90;
            }
            else {
                return true;
            }
        }

        public void UpdateVisibility(bool[] visibility)
        {
            for (int i = 0; i < 4; i++)
            {
                wall[i].enabled = visibility[i];
                top[i].SetActive(visibility[i]);
            }
        }

        public void SetSprite(Sprite wallSprite)
        {
            if (wallSprite == null)
                return;

            for (int i = 0; i < 4; i++)
            {
                wall[i].sprite = wallSprite;
                wall[i].UpdateAll();
            }
        }

        public void ShowOut(bool show)
        {
            for (int i = 0; i < 4; i++)
            {
                wallOut[i].gameObject.SetActive(show);
            }
        }
    }
}