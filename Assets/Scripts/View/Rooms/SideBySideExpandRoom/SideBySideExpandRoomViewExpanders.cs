﻿using UnityEngine;

namespace View.Rooms.SideBySideExpandRoom
{
    public class SideBySideExpandRoomViewExpanders
    {
        private GameObject expanderX;
        private GameObject expanderY;
        // private Transform parent;
        private RoomConductor roomConductor;

        public SideBySideExpandRoomViewSizes sz;

        private MaterialPropertyBlock materialPropBlock;
        private int _MainTex_ST;

        public void Init(Transform parent, RoomConductor roomConductor, SideBySideExpandRoomViewSizes _buildingSz)
        {
            // this.parent = parent;
            this.roomConductor = roomConductor;

            materialPropBlock = new MaterialPropertyBlock();
            _MainTex_ST = Shader.PropertyToID("_MainTex_ST");

            sz = _buildingSz;
            expanderX = parent.Find("ExpanderX").gameObject;
            expanderY = parent.Find("ExpanderY").gameObject;
        }

        public void Update()
        {
            Vector2 buildingSize = new Vector2 (sz.sizeGeom.x + sz.wallWidth * 2, sz.sizeGeom.y + sz.wallWidth * 2);
            Vector3 halfSize = sz.sizeGeom / 2f;

            if (expanderY != null)
            {
                Vector2 expanderSize = new Vector2 (sz.sizeGeom.x, sz.cellSize * 2);
                Vector3 scale = new Vector3 (expanderSize.x, expanderSize.y, 1);
                Vector3 pos = new Vector3 (halfSize.x + sz.wallWidth, 0.01f, buildingSize.y + expanderSize.y / 2f);

                expanderY.transform.localScale = scale;
                expanderY.transform.localPosition = pos;
            }

            if (expanderX != null)
            {
                Vector2 expanderSize = new Vector2 (sz.sizeGeom.y, sz.cellSize * 2);
                Vector3 scale = new Vector3 (expanderSize.x, expanderSize.y, 1);
                Vector3 pos = new Vector3 (buildingSize.x + expanderSize.y / 2f, 0.01f, halfSize.y + sz.wallWidth);

                expanderX.transform.localScale = scale;
                expanderX.transform.localPosition = pos;
            }

            //Vector3 pos1 = new Vector3 (sz.cellSize * 2 * 2, 0, sz.sizeGeom.y + sz.wallWidth * 2 + sz.cellSize * 4);

/*
            if (expanderX.activeSelf)
            {
                if (!sz.CanExpandX())
                    expanderX.SetActive(false);
            }

            if (expanderY.activeSelf)
            {
                if (!sz.CanExpandY())
                    expanderY.SetActive(false);
            }
*/
        }

        public bool IsVisible()
        {
            return expanderX.activeSelf || expanderY.activeSelf;
        }

        public void Show (bool canExpandX, bool canExpandY, float sizeX, float sizeY)
        {
            expanderX.SetActive(canExpandX);
            expanderY.SetActive(canExpandY);

            UpdateTiling (sizeX, sizeY);

            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.states.expand = true;
                //camCtrl.SetupForExpand ();

                const float cCameraShift = -5;
                Vector3 convertedPos = roomConductor.coordsMapper.ConvertPositionFromLogic (new Vector3(sizeX + cCameraShift, 0, sizeY + cCameraShift));
                Vector3 expandFocus = roomConductor.roomObject.transform.position + convertedPos + new Vector3(0.59f, 0, -0.33f);

                camCtrl.AutoMoveTo (expandFocus.x, expandFocus.z);
                camCtrl.AutoRotateTo (0);
            } 
        }

        public void UpdateTiling(float sizeX, float sizeY)
        {
            Vector4 tile = new Vector4 (sizeX / 4, 1f, 0, 0);
            materialPropBlock.SetVector(_MainTex_ST, tile);
            expanderY.GetComponent<MeshRenderer>().SetPropertyBlock(materialPropBlock);

            tile = new Vector4 (sizeY / 4, 1f, 0, 0);
            materialPropBlock.SetVector(_MainTex_ST, tile);
            expanderX.GetComponent<MeshRenderer>().SetPropertyBlock(materialPropBlock);
        }

        public void Hide ()
        {
            expanderX.SetActive(false);
            expanderY.SetActive(false);

            var camCtrl = Camera.main.GetComponent<CameraHelpers.CameraController>();
            if (camCtrl != null)
            {
                camCtrl.states.expand = false;
            }
        }
    }
}