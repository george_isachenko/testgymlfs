﻿using Data.Room;
using UnityEngine;

/*
#if UNITY_EDITOR

using UnityEditor.SceneManagement;

#endif // #if UNITY_EDITOR
*/

namespace View.Rooms.SideBySideExpandRoom
{
    // [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Rooms/Side-By-Side Expand Room/Side-By-Side Expand Room View")]
    public class SideBySideExpandRoomView : RoomView
    {
        public static readonly RoomCoordinates defaultRoomSize = new RoomCoordinates(9, 9);

        public GameObject floorCollider
        {
            get
            {
                if (floorCollider_ == null)
                    Init();

                return floorCollider_ != null ? floorCollider_.gameObject : null;
            }
        }

        [Header("Sizes")]
        public SideBySideExpandRoomViewSizes sizes;

        [HideInInspector]
        public RoomCoordinates  cachedSize;  // Used only to detect changes!
        [HideInInspector]
        public RoomCoordinates  cachedMaxSize;  // Used only to detect changes!
        [HideInInspector]
        public float            cachedCellSize;  // Used only to detect changes!
        [HideInInspector]
        public Transform        expandSign;
        public float            sizeAnimationTime = 1.0f;

        private ClubViewWalls walls;
        private SideBySideExpandRoomViewCorners corners;
        private SideBySideExpandRoomViewBase wallBase;
        private SideBySideExpandRoomViewExpanders expanders;

        private TiledSpriteRenderer floor;
        private TiledSpriteRenderer roof;
        private Transform floorCollider_;
        private Transform floorShadow;

// #if UNITY_EDITOR
//         bool _editorMarkSceneDirty = false;
// #endif

        protected new void Awake()
        {
            //Debug.Log("ClubView: Awake.");

            sizes = new SideBySideExpandRoomViewSizes(gameObject, roomConductor, sizeAnimationTime);
            sizes.animationUpdateCallback += OnUpdateSizes;
            sizes.animationCompleteCallback += OnUpdateSizesComplete;

            expandSign = transform.FindDeepChild("ExpandSign");

            base.Awake();

#if UNITY_EDITOR
            if (Application.isEditor)
            {
                Init();
                EditorUpdate();
            }
            else
            {
                Init();
            }
#else
            Init();
#endif
        }

        protected new void Start()
        {
            base.Start();

            //Debug.Log("ClubView: Start.");
#if UNITY_EDITOR
            if (Application.isEditor)
            {
                Init();
                EditorUpdate();
            }
            else
            {
                Init();
            }
#else
            Init();
#endif
            expanders.Hide();
        }

        protected new void OnDestroy()
        {
            base.OnDestroy();

            if (sizes != null)
            {
                sizes.animationUpdateCallback -= OnUpdateSizes;
                sizes.animationCompleteCallback -= OnUpdateSizesComplete;
            }
        }

        protected void Update()
        {
            if (Application.isPlaying)
            {
                UpdateVisibility();
            }

#if UNITY_EDITOR
//             if (_editorMarkSceneDirty)
//             {
//                 Debug.Log("ClubView: Update: Marking as dirty.");
//                 EditorSceneManager.MarkSceneDirty(gameObject.scene);
//                 _editorMarkSceneDirty = false;
//             }
#endif
        }

#if UNITY_EDITOR
        protected new void OnValidate()
        {
            //Debug.Log("ClubView: OnValidate");
            if (gameObject.activeSelf)
                base.OnValidate();
        }

        public override void EditorUpdate()
        {
            //Debug.LogFormat("ClubView: EditorUpdate: IsLoaded: {0}, IsValid: {1}", gameObject.scene.isLoaded , gameObject.scene.IsValid());

            if (roomConductor != null && roomConductor.editorRoomSetupData.roomExpandMode != RoomExpandMode.BlockGrid)
                roomConductor.editorRoomSetupData.roomExpandMode = RoomExpandMode.BlockGrid;

            base.EditorUpdate();

            if (gameObject.activeInHierarchy)
            {
                var dirty = Init();
                if (!dirty)
                {
                    if (roomConductor != null)
                    {
                        var maxSize = roomConductor.roomSetupDataInfo.maxSizeInCells;
                        var initialSize = roomConductor.roomSetupDataInfo.initialSizeInCells;

                        dirty = ((cachedMaxSize != new RoomCoordinates(maxSize.x, maxSize.y)) ||
                                 (cachedSize != new RoomCoordinates(initialSize.x, initialSize.y)) ||
                                  cachedCellSize != roomConductor.coordsMapper.cellSize);

//                         Debug.LogFormat("ClubView: Conductor. Max size: {0}, Size: {1}, cell size: {2}."
//                             , new RoomCoordinates(roomConductor.roomSetupData.maxSizeX, roomConductor.roomSetupData.maxSizeY)
//                             , new RoomCoordinates(roomConductor.roomSetupData.initialSizeX, roomConductor.roomSetupData.initialSizeY)
//                             , roomConductor.coordsMapper.cellSize );

//                         Debug.LogFormat("ClubView: Max size: {0}, Size: {1}, cell size: {2}."
//                             , cachedMaxSize
//                             , cachedSize
//                             , cachedCellSize );
                    }
                    else
                    {
                        dirty = (cachedSize != defaultRoomSize || cachedMaxSize != defaultRoomSize || cachedCellSize != SideBySideExpandRoomViewSizes.defaultCellSize);
                        //Debug.Log("ClubView: No conductor...");
                    }
                }

                if (dirty)
                {
                    if (roomConductor != null)
                    {
                        var maxSize = roomConductor.roomSetupDataInfo.maxSizeInCells;
                        var initialSize = roomConductor.roomSetupDataInfo.initialSizeInCells;

                        UpdateAllObjects(new RoomCoordinates(initialSize.x, initialSize.y));
                        cachedSize = new RoomCoordinates(initialSize.x, initialSize.y);
                        cachedMaxSize = new RoomCoordinates(maxSize.x, maxSize.y);
                        cachedCellSize = roomConductor.coordsMapper.cellSize;
                    }
                    else
                    {
                        UpdateAllObjects(defaultRoomSize);
                        cachedSize = defaultRoomSize;
                        cachedMaxSize = defaultRoomSize;
                        cachedCellSize = roomConductor.coordsMapper.cellSize;
                    }
//                     _editorMarkSceneDirty = true;
                    //Debug.Log("ClubView: DIRTY.");
                }
                else
                {
                    //Debug.Log("ClubView: CLEAN.");
                }
            }
//             else
//             {
//                 Debug.Log("ClubView: Scene not loaded yet or not valid...");
//             }
        }
#endif // #if UNITY_EDITOR

        public override void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            base.SetNewSize(expandLevel, newSize, unlockedBlocks, animate);

            UpdateAllObjects(newSize, animate);
        }

        public override void SetWallsSprite(Sprite sprite)
        {
            if (sprite != null)
                walls.SetSprite(sprite);
        }

        public override void SetFloorSprite(Sprite sprite)
        {
            if (sprite != null)
            {
                floor.sprite = sprite;
                floor.UpdateAll();
            }
        }

        public override bool CheckForExpandStart(bool canExpandX, bool canExpandY, int blockIndex)
        {
            if (!expanders.IsVisible())
            {
                expanders.Show(canExpandX, canExpandY, roomConductor.roomSize.x, roomConductor.roomSize.y);
                return true;
            }
            else
                return false;
        }

        public override void ExpandEnded()
        {
            expanders.Hide();
        }

        public override void UpdateExpand()
        {
            expanders.UpdateTiling (roomConductor.roomSize.x, roomConductor.roomSize.y);
        }

        public override void SetActiveState(bool state)
        {
            base.SetActiveState(state);

            if (roof != null)
            {
                roof.gameObject.SetActive(!state);
            }

            walls.ShowOut(!state);
        }

        private void UpdateVisibility()
        {
            bool[] visibility;
            if (isActivated && walls != null && Camera.main != null)
            {
                visibility = walls.GetVisibility(Camera.main.transform.position);
            }
            else
            {
                visibility = new bool[] { true, true, true, true };
            }

            if (walls != null)
                walls.UpdateVisibility(visibility);
            
            if (corners != null)
                corners.UpdateVisibility(visibility);
        }

        private bool Init()
        {
            bool changed = false;

            var newFloor = transform.GetComponentInChild<TiledSpriteRenderer>("Floor");
            if (!ReferenceEquals(floor, newFloor))
            {
                floor = newFloor;
                changed = true;
            }

            if (floor != null)
            {
                var newFloorCollider = floor.transform.Find("_Collider");
                if (!ReferenceEquals(newFloorCollider, floorCollider_))
                {
                    floorCollider_ = newFloorCollider;
                    changed = true;
                }

                var newFloorShadow = floor.transform.Find("Shadow");
                if (!ReferenceEquals(newFloorShadow, floorShadow))
                {
                    floorShadow = newFloorShadow;
                    changed = true;
                }
            }

            var newRoof = transform.GetComponentInChild<TiledSpriteRenderer>("Roof");
            if (!ReferenceEquals(roof, newRoof))
            {
                roof = newRoof;
                changed = true;
            }

            if (walls == null)
            {
                walls = new ClubViewWalls();
                walls.Init(transform.Find("Walls"), sizes);
                changed = true;
            }

            if (corners == null)
            {
                corners = new SideBySideExpandRoomViewCorners();
                corners.Init(transform.Find("Corners"), sizes);
                changed = true;
            }

            if (wallBase == null)
            {
                wallBase = new SideBySideExpandRoomViewBase();
                wallBase.Init(transform.Find("Base"), sizes);
                changed = true;
            }

            if (expanders == null)
            {
                expanders = new SideBySideExpandRoomViewExpanders();
                expanders.Init(transform.Find("Expand"), roomConductor, sizes);
                changed = true;
            }

            return changed;
        }

        private void UpdateAllObjects(RoomCoordinates size, bool animate = false)
        {
            if (sizes != null)
            {
                if (roomConductor != null)
                {
                    sizes.Update(roomConductor.roomSetupDataInfo.maxSizeInCells, size, animate, roomConductor.coordsMapper.cellSize);
                }
                else
                {
                    sizes.Update(size, size, animate);
                }
            }
        }

        void OnUpdateSizes()
        {
            UpdateFloorSize();

            expanders.Hide();
            corners.Update();
            wallBase.Update();
            floor.UpdateAll();
            walls.Update();
        }

        void OnUpdateSizesComplete()
        {
            expanders.Update();
            ExpandEnded();
        }

        private void UpdateFloorSize()
        {
            var actualSize = sizes.sizeGeom;
            var halfSize = actualSize * 0.5f;
            //var sizeCells = sizes.sizeCells;
            var cellSize = sizes.cellSize;

            Vector3 scale = Vector3.one;
            Vector3 pos = new Vector3(halfSize.x + sizes.wallWidth, 0, halfSize.y + sizes.wallWidth);

            floor.transform.localScale = scale;
            floor.transform.localPosition = pos;
            floor.size = actualSize;
            floorCollider_.localScale = new Vector3(floor.size.x, floor.size.y, 1);
            floorShadow.localScale = floorCollider_.localScale;

            pos.y = sizes.wallHeight;

            roof.transform.localScale = scale;
            roof.transform.localPosition = pos;
            roof.size = actualSize;
        }
    }
}