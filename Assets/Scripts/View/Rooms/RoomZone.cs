﻿using System;
using UnityEngine;

namespace View.Rooms
{
    [Serializable]
    public class RoomZone
    {
        public enum ZoneType
        {
            Point = 0,
            Circle,
            Rect,
        }

        public string name;
        public ZoneType type;
        public Vector2 position;
        public float height;
        [Range(0.0f, 360.0f)]
        public float heading;
        [Range(0.0f, 360.0f)]
        public float headingRandomizationAngle;
        public Vector2 size;

        public Vector3 GenerateRandomPointInsideZone()
        {
            switch (type)
            {
                case ZoneType.Point:
                {
                    return new Vector3(position.x, height, position.y);
                }

                case ZoneType.Circle:
                {
                    var randomPoint = position + UnityEngine.Random.insideUnitCircle * size.x;
                    return new Vector3(randomPoint.x, height, randomPoint.y);
                }

                case ZoneType.Rect:
                {
                    var randomPoint = position + new Vector2(size.x * (UnityEngine.Random.value - 0.5f), size.y * (UnityEngine.Random.value - 0.5f));
                    return new Vector3(randomPoint.x, height, randomPoint.y);
                }
            }
            return default(Vector3);
        }

        public Vector3 GenerateRandomPointInsideZone(out float heading)
        {
            heading = (headingRandomizationAngle > 0.0f) ? (this.heading + headingRandomizationAngle * (UnityEngine.Random.value - 0.5f)) % 360.0f : this.heading;

            return GenerateRandomPointInsideZone ();
        }
    }
}