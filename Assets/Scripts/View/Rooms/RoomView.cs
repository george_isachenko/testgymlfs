﻿using System;
using System.Linq;
using Data.Room;
using InspectorHelpers;
using UI.HUD.Skip;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Actors;
using View.CameraHelpers;
using View.EventHelpers;
using View.UI;
using View.UI.OverheadUI;
using View.UI.OverheadUI.Components;

namespace View.Rooms
{
    [RequireComponent(typeof(RoomConductor))]
    [ExecuteInEditMode]
    public abstract class RoomView : MonoBehaviour, IRoomView
    {
        #region Types.
        [Serializable]
        public struct ToggleInfo
        {
            public GameObject gameObject;
            [Tooltip("When enabled - hide when active, show when inactive.")]
            public bool invert;

#if UNITY_EDITOR
            public int EditorGetStateHashCode()
            {
                return (gameObject != null ? gameObject.GetInstanceID() : 0) ^ invert.GetHashCode();
            }
#endif
        }
        #endregion

        public readonly static string roomFloorTag = "RoomFloor";
        public readonly static string roomRoofTag = "RoomRoof";
        public readonly static string roomWallTag = "RoomWall";
        protected static readonly string iconAttachmentObjectName = "icon_attachment";

        #region Events.
        public event TransitionToRoomCheckRequest transitionToRoomCheckRequest;
        public event RoomUnlockRequest roomUnlockRequest;
        public event PrepareToExpandRequest prepareToExpandRequest;
        public event StartExpandRequest startExpandRequest;
        public event DeactivateRoomRequest deactivateRoomRequest;
        public event ActivateRoomRequest activateRoomRequest;

        public event FloorPointerClickEvent floorPointerClickEvent;
        public event FloorPointerDownEvent floorPointerDownEvent;
        public event FloorPointerUpEvent floorPointerUpEvent;

        public event OnCannotExpandEvent onCannotExpand;
        #endregion

        #region Inspector fields.
        [SerializeField]
        protected bool hideActorsWhenInactive;

        [SerializeField]
        [ReorderableList]
        protected ToggleInfo[] activeRoomToggles;

        [SerializeField]
        [Header("Expand/unlock Overhead UI")]
        protected SingleActionIcon.Holder expandOverheadIcon;

        [SerializeField]
        [ReorderableList]
        [Header("Expand icons for each expand level.")]
        public Sprite[] expandAvailableIcons;

        [SerializeField]
        protected Sprite expandCompleteIcon;

        [SerializeField]
        protected TimerIcon.Holder expandPendingOverheadIcon;

        [SerializeField]
        protected Transform iconAttachment;

        [SerializeField]
        protected FeatureLockedView lockedView;
        #endregion

        #region Private data.
        private RoomConductor roomConductor_;
        private AutoHider[] autoHiders;
        bool activated = false;
        private GUIRoot gui_;
        private int activeRoomTriggersCount;
        #endregion

        #region Protected data.
        protected ExpandAvailabilityState expandAvailabilityState = ExpandAvailabilityState.None;
        #endregion

        #region Public properties.
        public int expandLevel { get; protected set; }

        public bool isActivated
        {
            get
            {
                return activated;
            }
        }

        public RoomConductor roomConductor
        {
            get
            {
                if (roomConductor_ == null)
                {
                    roomConductor_ = (this != null && gameObject != null) ? gameObject.GetComponentInChildren<RoomConductor>(true) : null;
                }

                return roomConductor_;
            }
        }

        public string roomName
        {
            get
            {
                return (roomConductor != null) ? roomConductor.roomSetupDataInfo.name : null;
            }
        }

        public GameObject viewObject
        {
            get
            {
                return gameObject;
            }
        }

        public GameObject roomObject
        {
            get
            {
                var conductor = roomConductor;
                if (conductor != null)
                {
                    return (conductor.roomObject != null) ? conductor.roomObject : conductor.gameObject;
                }
                return gameObject;  // Dirty workaround.
            }
        }

        public DisplayMode actorsDisplayMode
        {
            get
            {
                return (activated)
                    ? DisplayMode.ShowActorAndOverheadUI
                    : hideActorsWhenInactive ? DisplayMode.Hidden : DisplayMode.ShowActorOnly;
            }
        }

        public virtual Vector3 homeFocusPosition
        {
            get
            {
                var floorSize = roomConductor_.coordsMapper.ConvertPositionFromRoomCooordinates(roomConductor_.roomSize);
                return roomConductor_.roomObject.transform.position + (floorSize * 0.5f);
            }
        }
        #endregion

        #region Protected properties.
        protected  GUIRoot gui
        {
            get
            {
                return GetGUI();
            }
        }
        #endregion

        #region IRoomView interface and public virtual API.
        public virtual void SetNewSize(int expandLevel, RoomCoordinates newSize, bool[] unlockedBlocks, bool animate)
        {
            if (roomConductor != null)
            {
                roomConductor.SetCurrentSize(newSize);
            }

            FindAllObjects();

            this.expandLevel = expandLevel;

            if (expandOverheadIcon != null && expandOverheadIcon.instance != null)
                expandOverheadIcon.icon.visible = false;

            if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.instance != null)
                expandPendingOverheadIcon.icon.visible = false;

            expandAvailabilityState = ExpandAvailabilityState.None;
        }

        public virtual void SetWallsSprite(Sprite sprite)
        {
        }

        public virtual void SetFloorSprite(Sprite sprite)
        {
        }

        public virtual void StartExpand()
        {
            switch (expandAvailabilityState)
            {
                case ExpandAvailabilityState.ExpandAvailable:
                {
                    RequestStartExpand(-1);
                }; break;

                case ExpandAvailabilityState.ExpandPending:
                {
                    StartTransitionToRoom();
                }; break;

                case ExpandAvailabilityState.ExpandComplete:
                {
                    StartTransitionToRoom();
                }; break;
            }
        }

        public virtual bool CheckForExpandStart(bool canExpandX, bool canExpandY, int blockIndex)
        {
            return false;
        }

        public virtual void ExpandEnded()
        {
        }

        public virtual void UpdateExpand()
        {
            UpdateCameraController();
        }

        public virtual void SetActiveState(bool state)
        {
            activated = state;

            if (state)
            {
                var mainCamera = Camera.main;
                if (mainCamera != null)
                {
                    var cameraController = mainCamera.GetComponent<CameraController>();

                    if (cameraController != null)
                    {
                        var homePosition = homeFocusPosition; // Can be heavy call, really.
                        //cameraController.SetHomePosition(homePosition.x, homePosition.z);
                    }
                }
            }

            UpdateActiveRoomToggles(state);
            UpdateAutoHiders();

            if (state)
            {
                UpdateCameraController();

/*
                Vector3 initialCameraPosition;
                if (!GetZoneCenter("initial_camera_focus", out initialCameraPosition))
                {
                    GetZoneCenter(CharacterDestination.wholeRoom, out initialCameraPosition);
                }
*/

            }
        }

        public virtual void SetExpandAvailableState(ExpandAvailabilityState state, float timeLeft)
        {
            if (expandAvailabilityState != state)
            {
                expandAvailabilityState = state;

                if (expandOverheadIcon != null && expandOverheadIcon.instance != null)
                {
                    switch (state)
                    {
                        case ExpandAvailabilityState.None:
                        {
                            expandOverheadIcon.icon.visible = false;
                        }; break;

                        case ExpandAvailabilityState.ExpandAvailable:
                        {
                            if (expandAvailableIcons != null && expandAvailableIcons.Length > 0)
                                expandOverheadIcon.instance.iconSprite = expandAvailableIcons[Mathf.Clamp(expandLevel, 0, expandAvailableIcons.Length - 1)];
                            expandOverheadIcon.icon.visible = true;
                        }; break;

                        case ExpandAvailabilityState.ExpandPending:
                        {
                            expandOverheadIcon.icon.visible = false;
                        }; break;

                        case ExpandAvailabilityState.ExpandComplete:
                        {
                            expandOverheadIcon.instance.iconSprite = expandCompleteIcon;
                            expandOverheadIcon.icon.visible = true;
                        }; break;
                    }
                }

                if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.instance != null)
                {
                    switch (state)
                    {
                        case ExpandAvailabilityState.None:
                        {
                            expandPendingOverheadIcon.icon.visible = false;
                        }; break;

                        case ExpandAvailabilityState.ExpandAvailable:
                        {
                            expandPendingOverheadIcon.icon.visible = false;
                        }; break;

                        case ExpandAvailabilityState.ExpandPending:
                        {
                            expandPendingOverheadIcon.instance.SetData
                                ( DateTime.UtcNow + TimeSpan.FromSeconds(timeLeft)
                                , StartExpand
                                , true
                                , OnPendingExpandIconTimerComplete);
                            expandPendingOverheadIcon.icon.visible = true;
                        }; break;

                        case ExpandAvailabilityState.ExpandComplete:
                        {
                            expandPendingOverheadIcon.icon.visible = false;
                        }; break;
                    }
                }
            }
        }

        public virtual void TransitionToUnlockedRoom()
        {
            StartTransitionToRoom();
        }

        public virtual bool GetZoneCenter(string zoneName, out Vector3 position)
        {
            return roomConductor.GetZoneCenter(zoneName, out position);
        }

        public virtual bool GetZoneCenterLocal(string zoneName, out Vector3 position)
        {
            return roomConductor.GetZoneCenterLocal(zoneName, out position);
        }

        public virtual bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position)
        {
            return roomConductor.GenerateRandomPointInsideZone(zoneName, out position);
        }

        public virtual bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition)
        {
            return roomConductor.GenerateRandomLocalPointInsideZone(zoneName, out localPosition);
        }

        public virtual bool GenerateRandomPointInsideZone(string zoneName, out Vector3 position, out Quaternion rotation)
        {
            return roomConductor.GenerateRandomPointInsideZone(zoneName, out position, out rotation);
        }

        public virtual bool GenerateRandomLocalPointInsideZone(string zoneName, out Vector3 localPosition, out Quaternion rotation)
        {
            return roomConductor.GenerateRandomLocalPointInsideZone(zoneName, out localPosition, out rotation);
        }

        public virtual EquipmentPlacement FindFreeSpaceForEquipment(RoomCoordinates objectSize)
        {
            return roomConductor.FindFreeSpace(new RoomCoordinates(roomConductor.roomSize.x, roomConductor.roomSize.y), objectSize);
        }

        public virtual void ShowLockedView (string message)
        {
            if (lockedView != null)
            {
                lockedView.description = message;
                lockedView.Show();
            }
            else
            {
                Debug.LogWarningFormat("No 'Locked View' set for RoomView {0}. Message: '{1}'.", gameObject.name, message);
            }
        }

        int IRoomView.GetRoomZonesCount (string zoneNamePrefix)
        {
            return (roomConductor != null) ? roomConductor.zones.Count(x => x.name.StartsWith(zoneNamePrefix)) : 0;
        }
        #endregion

        #region Protected API.
        protected void UpdateAutoHiders()
        {
            if (autoHiders != null && autoHiders.Length > 0)
            {
                foreach (var autoHider in autoHiders)
                {
                    autoHider.enabled = activated;
                }
            }
        }

        protected virtual void UpdateActiveRoomToggles(bool state)
        {
            if (activeRoomToggles != null && activeRoomToggles.Length > 0)
            {
                foreach (var art in activeRoomToggles)
                {
                    if (art.gameObject != null)
                    {
                        art.gameObject.SetActive(state ^ art.invert);
                    }
                }
            }
        }

        protected void TransitionCameraTo (Vector3 position)
        {
            var mainCamera = Camera.main;
            if (mainCamera != null)
            {
                var cameraController = mainCamera.GetComponent<CameraController>();

                if (cameraController != null)
                {
                    cameraController.AutoMoveTo ( position.x, position.z);
                }
            }
        }

        protected void OnCannotExpand()
        {
            onCannotExpand?.Invoke();
        }

        #endregion

        #region Public API.
        public void StartTransitionToRoom()
        {
            var canTransition = false;

            if (transitionToRoomCheckRequest != null)
            {
                canTransition = transitionToRoomCheckRequest(roomConductor.roomSetupDataInfo.name);
                if (!canTransition && roomUnlockRequest != null)
                {
                    roomUnlockRequest(roomConductor.roomSetupDataInfo.name, -1);
                }
            }

            if (canTransition)
            {
                TransitionCameraTo(homeFocusPosition);
            }
        }

        // Note: Do not remove, can be used from Inspector events.
        public bool SendPrepareToExpandRequest()
        {
            if (prepareToExpandRequest != null)
            {
                return prepareToExpandRequest(roomConductor.roomSetupDataInfo.name);
            }
            return false;
        }

        // Note: Do not remove, can be used from Inspector events.
        public void RequestStartExpand(int blockIndex)
        {
            startExpandRequest?.Invoke(roomConductor.roomSetupDataInfo.name, blockIndex);
        }

        // Note: Do not remove, can be used from Inspector events
        public void RequestUnlock()
        {
            roomUnlockRequest?.Invoke(roomConductor.roomSetupDataInfo.name, -1);
        }

        // Note: Do not remove, can be used from Inspector events
        public void RequestUnlock(int desiredExpandLevel)
        {
            roomUnlockRequest?.Invoke(roomConductor.roomSetupDataInfo.name, desiredExpandLevel);
        }

        // Note: Do not remove, can be used from Inspector events.
        public void RoomZoneTriggerEnter()
        {
            activeRoomTriggersCount++;
            if (activeRoomTriggersCount == 1)
            {
                activateRoomRequest?.Invoke(roomConductor.roomSetupDataInfo.name);
            }
        }

        // Note: Do not remove, can be used from Inspector events.
        public void RoomZoneTriggerExit()
        {
            Assert.IsTrue(activeRoomTriggersCount > 0);
            activeRoomTriggersCount--;
            if (activeRoomTriggersCount == 0)
            {
                deactivateRoomRequest?.Invoke(roomConductor.roomSetupDataInfo.name);
            }
        }

        #endregion

        #region Unity API.
        protected void Awake()
        {
        }

        protected void Start()
        {
            if (Application.isPlaying)
            {
                FindAllObjects();

                if (iconAttachment != null)
                {
                    if (expandOverheadIcon != null && expandOverheadIcon.prefab != null)
                    {
                        expandOverheadIcon.Instantiate();
                        expandOverheadIcon.instance.onClick.AddListener (StartExpand);
                        expandOverheadIcon.icon.worldPositionProvider = new DynamicWorldPositionProvider(iconAttachment);
                        expandOverheadIcon.icon.owner = this;
                        expandOverheadIcon.icon.visible = false;
                    }
                    else
                    {
                        Debug.LogWarningFormat("{0}: Missing Expand Overhead UI Icon prefab.", GetType().Name);
                    }

                    if (expandPendingOverheadIcon != null && expandPendingOverheadIcon.prefab != null)
                    {
                        expandPendingOverheadIcon.Instantiate();
                        expandPendingOverheadIcon.icon.worldPositionProvider = new DynamicWorldPositionProvider(iconAttachment);
                        expandPendingOverheadIcon.icon.owner = this;
                        expandPendingOverheadIcon.icon.visible = false;
                    }
                    else
                    {
                        Debug.LogWarningFormat("{0}: Missing Expand Pending Overhead UI Icon prefab.", GetType().Name);
                    }
                }
            }
        }

        protected void OnDestroy()
        {
            var floorEventRouters = GetComponentsInChildren<IPointerEventsRouter>(true);
            if (floorEventRouters != null)
            {
                foreach (var fer in floorEventRouters)
                {
                    if (fer != null && fer.gameObject.CompareTag(roomFloorTag))
                    {
                        fer.onPointerClick  -= OnFloorPointerClick;
                        fer.onPointerDown   -= OnFloorPointerDown;
                        fer.onPointerUp     -= OnFloorPointerUp;
                    }
                }
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        protected void OnValidate()
        {
            // Call this only in editor, only when not playing and only for objects instantiated in scene (not prefabs).
            if (Application.isEditor && !Application.isPlaying && gameObject.scene.IsValid() && gameObject.scene.isLoaded)
            {   
                EditorUpdate();

                FindAllObjects();
            }
        }
#endif // #if UNITY_EDITOR
        #endregion

        #region Private Editor-only functions.
#if UNITY_EDITOR
        public virtual void EditorUpdate()
        {
        }

        public virtual int EditorGetStateHashCode()
        {
            int hashCode = (roomConductor != null ? roomConductor.EditorGetStateHashCode() : 0);

            if (activeRoomToggles != null && activeRoomToggles.Length > 0)
            {
                foreach (var p in activeRoomToggles)
                {
                    hashCode ^= p.EditorGetStateHashCode();
                }
            }
            
            return hashCode;
        }
#endif
        #endregion

        #region Private functions.
        private GUIRoot GetGUI()
        {
            if (gui_ != null)
                return gui_;

            gui_ = GameObject.Find("_GUIRoot").GetComponent<GUIRoot>();
            Assert.IsNotNull(gui_);

            return gui_;
        }

        private void FindAllObjects()
        {
            autoHiders = transform.GetComponentsInChildren<AutoHider>();

            var floorEventRouters = GetComponentsInChildren<IPointerEventsRouter>(true);
            if (floorEventRouters != null)
            {
                foreach (var fer in floorEventRouters)
                {
                    if (fer.gameObject.CompareTag(roomFloorTag))
                    {
                        fer.onPointerClick  += OnFloorPointerClick;
                        fer.onPointerDown   += OnFloorPointerDown;
                        fer.onPointerUp     += OnFloorPointerUp;
                    }
                }
            }
        }

        private void UpdateCameraController()
        {
/*
            var camCtrl = Camera.main.GetComponent<CameraController>();
            if (camCtrl != null)
            {
                var floorSize = roomConductor_.coordsMapper.ConvertPositionFromRoomCooordinates(roomConductor_.roomSize);
                var floorCenterWorld = roomConductor_.roomObject.transform.TransformPoint(floorSize * 0.5f);
                var floorSizeAbs = new Vector2 (Math.Abs(floorSize.x), Math.Abs(floorSize.z));

                camCtrl.UpdateCurrentRoom(new Vector2(floorCenterWorld.x, floorCenterWorld.z), floorSizeAbs);
            }
*/
        }

        void OnFloorPointerClick (PointerEventData pointerEventData)
        {
            if (activated)
            {
                if (floorPointerClickEvent != null)
                    floorPointerClickEvent(pointerEventData, roomConductor.roomSetupDataInfo.name);
            }
        }

        void OnFloorPointerDown (PointerEventData pointerEventData)
        {
            if (activated)
            {
                if (floorPointerDownEvent != null)
                    floorPointerDownEvent(pointerEventData, roomConductor.roomSetupDataInfo.name);
            }
        }

        void OnFloorPointerUp (PointerEventData pointerEventData)
        {
            if (activated)
            {
                if (floorPointerUpEvent != null)
                    floorPointerUpEvent(pointerEventData, roomConductor.roomSetupDataInfo.name);
            }
        }

        void OnPendingExpandIconTimerComplete()
        {
            expandPendingOverheadIcon.icon.visible = false;

            if (expandOverheadIcon != null && expandOverheadIcon.instance != null)
            {
                expandOverheadIcon.instance.iconSprite = expandCompleteIcon;
                expandOverheadIcon.icon.visible = true;
                expandAvailabilityState = ExpandAvailabilityState.ExpandComplete;
            }
        }
        #endregion
    }
}