﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace View.EventHelpers
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Event Helpers/Pointer Events Router")]
    public class PointerEventsRouter
        : MonoBehaviour
        , IPointerEventsRouter
        , IPointerClickHandler
        , IPointerDownHandler
        , IPointerUpHandler
    {
        #region IPointerEventsRouter interface.
        public event Action<PointerEventData> onPointerClick;
        public event Action<PointerEventData> onPointerDown;
        public event Action<PointerEventData> onPointerUp;

        GameObject IPointerEventsRouter.gameObject
        {
            get
            {
                return gameObject;
            }
        }
        #endregion

        #region IPointerClickHandler interface.
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
//             Debug.LogFormat("PointerClickRouter: Pointer Click on '{0}'. Distance: {1}"
//                 , gameObject.name, (eventData.pressPosition - eventData.position).magnitude);
            if (onPointerClick != null)
            {
                onPointerClick(eventData);
            }
        }
        #endregion

        #region IPointerDownHandler interface.
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
//             Debug.LogFormat("PointerClickRouter: Pointer Down on '{0}'.", gameObject.name);

            if (onPointerDown != null)
                onPointerDown.Invoke(eventData);
        }
        #endregion

        #region IPointerUpHandler interface.
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
//             Debug.LogFormat("PointerClickRouter: Pointer Up on '{0}'.", gameObject.name);

            if (onPointerUp != null)
                onPointerUp(eventData);
        }
        #endregion
    }
}