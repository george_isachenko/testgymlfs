﻿using System;
using UnityEngine.EventSystems;
using UnityEngine;

namespace View.EventHelpers
{
    public interface IPointerEventsRouter
    {
        event Action<PointerEventData> onPointerClick;
        event Action<PointerEventData> onPointerDown;
        event Action<PointerEventData> onPointerUp;

        GameObject gameObject
        {
            get;
        }
    }
}