﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

namespace View.EventHelpers
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Event Helpers/Drag event Router")]
    public class DragEventRouter
        : MonoBehaviour
        , IDragHandler
    {
        public class PointerEventDataEvent : UnityEvent<PointerEventData>
        {
        }

        public UnityEvent<PointerEventData> dragHandler
        {
            get
            {
                if (_dragHandler == null)
                    _dragHandler = new PointerEventDataEvent();
                return _dragHandler;
            }
        }

        PointerEventDataEvent _dragHandler;

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (_dragHandler != null)
            {
                _dragHandler.Invoke(eventData);
            }
        }
    }
}