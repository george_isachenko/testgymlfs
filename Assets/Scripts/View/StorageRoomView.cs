﻿using System;
using InspectorHelpers;
using UnityEngine;

namespace View
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Storage Room View")]
    public class StorageRoomView : MonoBehaviour
    {
        [Serializable]
        public struct PalletInfo
        {
            [Range(0.0f, 1.0f)]
            public float capacityRatioToShow;

            public GameObject palletObject;
        }

        [ReorderableList]
        public PalletInfo[] pallets;

        void Awake()
        {
            foreach (var pallet in pallets)
            {
                if (pallet.palletObject != null)
                {
                    pallet.palletObject.SetActive(false);
                }
            }
        }

        public void OnStorageUpdated(float capacityRatio)
        {
            if (pallets != null)
            {
                foreach (var pallet in pallets)
                {
                    if (pallet.palletObject != null)
                    {
                        pallet.palletObject.SetActive(capacityRatio >= pallet.capacityRatioToShow);
                    }
                }
            }
        }
    }
}
