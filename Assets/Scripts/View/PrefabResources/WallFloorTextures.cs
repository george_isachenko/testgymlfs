﻿using InspectorHelpers;
using System;
using UnityEngine;

namespace View.PrefabResources
{
    [AddComponentMenu("Kingdom/View/Prefab Resources/Wall and Floor Textures")]
    [DisallowMultipleComponent]
    public class WallFloorTextures : MonoBehaviour
    {
        public Sprite FloorOnly;
        public Sprite WallOnly;
        public Sprite FoorWallSet;

        [Serializable]
        public struct WallFloor
        {
            public Sprite wall;
            public Sprite floor;
        }

        [ReorderableList]
        public WallFloor[] array;
    }
}