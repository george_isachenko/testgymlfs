﻿using UnityEngine;

namespace View.PrefabResources
{
    [AddComponentMenu("Kingdom/View/Prefab Resources/UI Sprite Selector")]
    [DisallowMultipleComponent]
    public class UISpriteSelector : MonoBehaviour
    {
        public Sprite coinsIcon;
        public Sprite fitbucksIcon;
    }
}
