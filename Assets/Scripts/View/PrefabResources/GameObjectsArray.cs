﻿using InspectorHelpers;
using UnityEngine;

namespace View.PrefabResources
{
    [AddComponentMenu("Kingdom/View/Prefab Resources/GameObject Array")]
    [DisallowMultipleComponent]
    public class GameObjectsArray : MonoBehaviour
    {
        [ReorderableList]
        public GameObject[] array;
    }
}