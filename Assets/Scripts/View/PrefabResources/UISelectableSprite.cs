﻿using UnityEngine;
using UnityEngine.UI;

namespace View.PrefabResources
{
    [AddComponentMenu("Kingdom/View/Prefab Resources/UI Selectable Sprite")]
    public class UISelectableSprite : MonoBehaviour
    {
        public GameObject spritesPrefab;

        private UISpriteSelector sprites;
        private Image img;

        void Awake()
        {
            img = GetComponent<Image>();
            sprites = spritesPrefab.GetComponent<UISpriteSelector>();
        }

        public void Coins()
        {
            gameObject.SetActive(true);
            img.sprite = sprites.coinsIcon;
        }

        public void FitBucks()
        {
            gameObject.SetActive(true);
            img.sprite = sprites.fitbucksIcon;
        }

        public void Empty()
        {
            gameObject.SetActive(false);
        }
    }

}