﻿using UnityEngine;

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Equipment Decor Actor")]
    public class EquipmentDecorActor : EquipmentActor
    {
    }
}