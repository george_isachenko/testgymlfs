using System;
using InspectorHelpers;
using UnityEngine;
using Data.Person;

namespace View.Actors.Settings
{
    [CreateAssetMenu(menuName = "Kingdom/View/Actors/Settings/Character Actor Settings")]
    public class CharacterActorSettings : ScriptableObject
    {
        private int velocityAnimatorParameterHashId_;

        public int velocityAnimatorParameterHashId
        {
            get
            {
                if (velocityAnimatorParameterHashId_ == 0)
                    velocityAnimatorParameterHashId_ = Animator.StringToHash(velocityAnimatorParameterName);

                return velocityAnimatorParameterHashId_;
            }
        }

        #region Serializable inspector fields.
        [Tooltip("Speed of character rotation towards camera (in degrees per second) when selected.")]
        public float rotateToCameraSpeed;

        [Tooltip("Speed of character rotation towards other character on interactions.")]
        public float interactionRotationSpeed;

        public Vector3 selectionPingScale;

        public float selectionPingTime;

        [Tooltip("Speed of character rotation towards destination (in degrees per second) when started moving.")]
        public float moveStartRotationSpeed;

        [ReorderableList(LabelsEnumType = typeof(MovementMode))]
        public float[] movementSpeeds;

        public string portraitMaterialPolicyName;

        public string velocityAnimatorParameterName;
        #endregion
    }
}
