using System;
using CustomAssets;
using UnityEngine;
using UnityToolbag;

namespace View.Actors.Settings
{
    [Serializable]
    public class EquipmentActorSettings
    {
        [Header("Selection settings")]
        public Vector3 selectionPingScale;
        public float selectionPingTime;
        public LeanTweenType selectionPingEaseType;
        public Vector3 selectionMarkerScale;
        public float selectionMarkerScaleTime;
        public LeanTweenType selectionMarkerEaseType;
        public Color inactiveSelection;
        public Color activeSelection;
        public Color wrongPlacement;
        [SortingLayer]
        public int selectionNormalLayer;
        [SortingLayer]
        public int selectionActiveLayer;
        [SortingLayer]
        public int carpetsNormalLayer;
        [SortingLayer]
        public int carpetsActiveLayer;
        [Header("Materials replacement settings")]
        public MaterialsReplacementPolicy defaultMaterialReplacementPolicy;
        public string wrongPlacementMaterialPolicyTag;
        public string lockedEquipmentMaterialPolicyTag;
        [Header("Upgrade settings")]
        public Vector3 upgradePingScale;
        public float upgradePingTime;
        public LeanTweenType upgradePingEaseType;
        public Vector3 upgradePingJump;
        public float upgradePingJumpTime;
        public LeanTweenType upgradePingJumpEaseType;
    }
}