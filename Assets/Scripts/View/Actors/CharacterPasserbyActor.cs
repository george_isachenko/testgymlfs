﻿using UnityEngine;

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Character Passerby Actor")]
    public class CharacterPasserbyActor : CharacterActor
    {
    }
}