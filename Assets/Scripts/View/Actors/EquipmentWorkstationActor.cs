﻿using UnityEngine;

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Equipment Workstation Actor")]
    public class EquipmentWorkstationActor : EquipmentActor
    {
    }
}