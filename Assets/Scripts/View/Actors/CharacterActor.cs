﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Data.Person;
using Data.Person.Info;
using Presentation.Providers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using View.Actions;
using View.Actors.Settings;
using View.Actors.Setup;
using View.AnimatorHelpers;
using View.NavigationHelpers;
using View.Rooms;
using View.EditorHelpers;

#if  UNITY_5_5_OR_NEWER

using UnityEngine.AI;
using Data.Room;
using View.Actors.Helpers;
using System.Linq;
using Logic.Persons;

#endif

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Actors
{
    public abstract class CharacterActor
        : Actor<int>
        , ICharacterActor
    {
        #region Types.
        protected struct JobInfo
        {
            public IEnumerator job;
#if DEBUG
            public string debugSubmitStack;
#endif

            public JobInfo(IEnumerator job)
            {
                this.job = job;
                debugSubmitStack = string.Empty;
            }

#if DEBUG
            public JobInfo(IEnumerator job, string debugSubmitStack)
            {
                this.job = job;
                this.debugSubmitStack = debugSubmitStack;
            }
#endif
        }
        #endregion

        #region Static/readonly data.
        private static readonly int initialJobsCount = 8;
#if DEBUG
        private static readonly int maxJobsWarningCount = 20;
#endif

#if UNITY_EDITOR
        private static readonly int debugMaxLogEntries = 20;
#endif
        #endregion

        #region ICharacterActor events
        public event CharacterActorPositionUpdated onCharacterActorPositionUpdated;
        #endregion

        #region ICharacterActor properties.
        public IPersonInfo personInfo 
        {
            get; private set;
        }

        public int navigationAreaMask
        {
            get
            {
                return navMeshAgent != null ? navMeshAgent.areaMask : NavMesh.AllAreas;
            }
        }

        public float navigationAgentRadius
        {
            get
            {
                return navMeshAgent != null ? navMeshAgent.radius : 0.0f;
            }
        }

        #endregion

        #region Protected properties.
        protected new ICharacterActorSharedDataProvider sharedDataProvider
        {
            get
            {
                return (ICharacterActorSharedDataProvider)(base.sharedDataProvider);
            }
        }
        #endregion

        #region Inspector data.
        [SerializeField]
        protected float dynamicPositionUpdateRate = 0.333f;

        [SerializeField]
        protected CharacterCursorType cursorType;
        #endregion

        #region Protected data.
        protected RuntimeCharacterSetupData runtimeCharacterSetupData;
        protected CharacterBodyController bodyController;
        protected Animator animator;
        protected NavMeshAgent navMeshAgent;
        protected NavMeshObstacle navMeshObstacle;
        protected List<JobInfo> jobsQueue;
        protected NavigationController navigationController;
        protected CharacterActorSettings actorSettings;
        protected GameObject assignedCursor;
        protected CharacterPortrait portrait;
        protected AnimationCompleteRouter animationCompleteRouter;
        protected Action<int, MovementResult> onMovementComplete;
        protected int originalLayer;
        protected CharacterDestination movementDestination;
        protected Vector3 movementWorldPosition;
        protected bool blendAnimations;
        protected bool runningExercise;
        protected float dynamicPositionUpdateTimeLeft;

#if UNITY_EDITOR
        protected string debugLastCompleteJobName;
        protected List<string> debugLastLogEntries = new List<string>(debugMaxLogEntries);
#endif

#if DEBUG
        protected bool debugInJob;
#endif
        #endregion

        #region Unity API.
        protected new void Awake()
        {
            base.Awake();
            originalLayer = gameObject.layer;
        }

        protected void Start()
        {
/*
            if (navMeshAgent != null)
            {
                navMeshAgent.enabled = (personInfo.state != PersonState.Workout);
            }
*/
        }

        protected void Update()
        {
            if (navigationController != null && navigationController.enabled)
            {
                if (movementDestination.isDynamic)
                {
                    dynamicPositionUpdateTimeLeft -= Time.deltaTime;

                    if (dynamicPositionUpdateTimeLeft <= 0)
                    {
                        dynamicPositionUpdateTimeLeft = dynamicPositionUpdateRate;

                        Vector3 newWorldPosition;

                        if (sharedDataProvider.GetWorldPositionForDestination(this, movementDestination, out newWorldPosition))
                        {
                            if (Vector3.Distance(newWorldPosition, movementWorldPosition) >= navMeshAgent.radius * 0.5f)  // Do not change position too often.
                            {
                                if (navigationController.ChangeDestination(newWorldPosition))
                                    movementWorldPosition = newWorldPosition;
                            }
                        }
                        else
                        {
                            Debug.LogWarningFormat("Failed to calculate new destination for actor {0} ('{1}'), destination: {2}. Movement failed."
                                , personInfo.id, gameObject.name, movementDestination);

                            PerformStopMovement();

                            ReportMovementCompleteResult(MovementResult.Failed);
                        }
                    }
                }

                if (animator != null && blendAnimations && navigationController.enabled) // Navigation controller can be disabled already.
                {
                    animator.SetFloat(actorSettings.velocityAnimatorParameterHashId, navigationController.velocity.magnitude);
                }
            }

#if UNITY_EDITOR
            if (navMeshAgent != null && navMeshAgent.enabled && navMeshAgent.hasPath && navigationController != null && !navigationController.enabled)
            {
                Debug.LogWarningFormat("NavMeshAgent is active and has path while Navigation Controller is not! Name: {0}."
                    , gameObject.name);
            }
#endif
        }
        #endregion

        #region ICharacterActor API.
        void ICharacterActor.Init
            ( ICharacterActorSharedDataProvider sharedDataProvider
            , int id
            , IPersonInfo personInfo
            , RoomConductor roomConductor
            , DisplayMode displayMode
            , RuntimeCharacterSetupData runtimeCharacterSetupData
            , CharacterActorSettings actorSettings
            , Vector3 worldPosition
            , Quaternion rotation)
        {
            transform.position = worldPosition;
            transform.rotation = rotation;

            base.Init (sharedDataProvider, id, roomConductor, displayMode, true);

//             transform.position = worldPosition;
//             transform.rotation = rotation;

            Assert.IsNull(this.personInfo); // Cannot be re-initialized!
            this.personInfo = personInfo;

            this.runtimeCharacterSetupData = runtimeCharacterSetupData;
            this.actorSettings = actorSettings;
            this.assignedCursor = null;
            jobsQueue = new List<JobInfo>(initialJobsCount);
            animator = GetComponent<Animator>();
            navMeshAgent = GetComponent<NavMeshAgent>();
            navMeshObstacle = GetComponent<NavMeshObstacle>();
            navigationController = GetComponent<NavigationController>();
            animationCompleteRouter = GetComponent<AnimationCompleteRouter>();
            bodyController = GetComponent<CharacterBodyController>();
            if (bodyController == null)
            {
                bodyController = gameObject.AddComponent<CharacterBodyController>();
            }

            if (navigationController != null)
            {
                navigationController.onNavigationUpdate += OnNavigationUpdate;
                navigationController.onNavigationComplete += OnNavigationComplete;
                navigationController.onGetStoppedAvoidanceDelta = OnGetStoppedAvoidanceDelta;
            }

            CustomizeCharacter();

            if (animator != null)
            {
                if (animator.runtimeAnimatorController != null)
                {
                    animator.runtimeAnimatorController = Instantiate(animator.runtimeAnimatorController);
                }
            }

            if (!personInfo.isTraining)
            {
                navMeshAgent.enabled = true;
            }

            navMeshAgent.areaMask = SelectNavMeshAgentAreaMask();
            navMeshAgent.avoidancePriority = navMeshAgent.avoidancePriority - personInfo.stoppedAvoidanceDelta;
            
            if (personInfo.state == PersonState.Idle || personInfo.state == PersonState.InQueue)
            {
                SetIdle();
            }

            UpdateVisibility();
        }

        void ICharacterActor.Move (CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo, Action<int, MovementResult> onMovementComplete)
        {
            Vector3 worldPosition;

            if (navMeshAgent != null &&
                navigationController != null &&
                sharedDataProvider.GetWorldPositionForDestination(this, destination, out worldPosition))
            {
                movementDestination = destination;
                movementWorldPosition = worldPosition;
                dynamicPositionUpdateTimeLeft = dynamicPositionUpdateRate;

                QueueJob(StartMovementJob(worldPosition, movementSettingsInfo, onMovementComplete));
            }
            else
            {
                Debug.LogErrorFormat("Failed to get world position for destination '{0}', actor: {1} ('{2}').", destination, id, personInfo.name);

                ReportMovementCompleteResult(MovementResult.Failed, onMovementComplete);
            }
        }

        void ICharacterActor.StopMovement ()
        {
            DebugLogLocal("StopMovement (Start)");

            if (runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is running exercise! Cannot do 'stop movement'.", id);
                return;
            }

            PerformStopMovement();

            ReportMovementCompleteResult(MovementResult.Stopped);
        }

        void ICharacterActor.ChangeMovementMode (MovementMode mode)
        {
            if (navMeshAgent != null &&
                navigationController != null &&
                navigationController.enabled)
            {
                if (animator != null && IsAnimation(CharacterBodyController.animCategoryWalking))
                {
                    var animationId = (int)GetWalkingAnimation(mode, blendAnimations);
                    SetAnimation (CharacterBodyController.animCategoryWalking, animationId, 1.0f);
                }

                var speed = GetMovementSpeed(mode);
                navigationController.SetSpeed(speed);
            }
        }

        public override void Destroy()
        {
            HideSelectionCursor();

            if (portrait != null)
            {
                Destroy(portrait);
                portrait = null;
            }

            this.personInfo = null;
            if (navigationController != null)
            {
                navigationController.onNavigationUpdate -= OnNavigationUpdate;
                navigationController.onNavigationComplete -= OnNavigationComplete;
            }

            base.Destroy();
        }

        public override void SetSelection (bool selected)
        {
            if (selected)
            {

/*
                // Experimental FPS fun camera. ;-)
                var headTransform = animator.GetBoneTransform(HumanBodyBones.Head);
                if (headTransform != null)
                {
                    Camera.main.transform.SetParent(headTransform);
                    Camera.main.transform.localPosition = Vector3.zero;
                    Camera.main.transform.localRotation = Quaternion.identity;
                }
*/

                if (personInfo.state == PersonState.Idle || personInfo.state == PersonState.ExerciseComplete)
                {
                    ShowSelectionCursor();
                    SelectionPingScale();

                    if (personInfo.isPlayerControllable)
                        RotateTowardsCamera();
                }

                Sound.instance.VisitorSelect();
            }
            else
            {
/*
                // Experimental FPS fun camera. ;-)
                Camera.main.transform.SetParent(null);
*/

                HideSelectionCursor();
            }

            if (currentOverheadUIIcon != null)
                currentOverheadUIIcon.frontIcon = selected;
        }

        public override void SetDisplayMode (DisplayMode displayMode)
        {
            base.SetDisplayMode(displayMode);
            UpdateVisibility();
        }

        public void QueuePlayEmoteAnimation ( AnimationSocial animationId, int loopCount, bool rotateTowardsCamera, Action onComplete )
        {
            if (rotateTowardsCamera)
            {
                DoLookAt(Camera.main.transform.position, actorSettings.rotateToCameraSpeed);
            }

            QueueJob(PlayEmoteAnimationJob(animationId, loopCount, onComplete));
        }

        public void QueueLookAt ( Vector3 worldLookAtPosition, Action onComplete)
        {
            DoLookAt(worldLookAtPosition, actorSettings.interactionRotationSpeed, onComplete);
        }

        void ICharacterActor.QueueEquipmentExercise
            ( int equipmentId
            , IEquipmentInteractionProvider equipmentInteractionProvider
            , int exerciseTier
            , int exerciseAnimation
            , ExerciseInitiator initiator
            , TimeSpan duration
            , ExerciseFlags flags
            , float speed)
        {
            QueueJob(StartEquipmentExerciseJob(equipmentInteractionProvider, equipmentId, exerciseTier, exerciseAnimation, initiator, duration, flags, speed));
        }

        void ICharacterActor.QueueSwitchEquipmentExercise
            ( IEquipmentInteractionProvider equipmentInteractionProvider
            , int exerciseTier
            , int exerciseAnimation
            , float speed)
        {
            QueueJob(SwitchEquipmentExerciseJob(equipmentInteractionProvider, exerciseTier, exerciseAnimation, speed));
        }

        void ICharacterActor.QueueEquipmentExerciseEnd
            ( int equipmentId
            , IEquipmentInteractionProvider equipmentInteractionProvider
            , ExerciseFlags flags)
        {
            QueueJob(EndEquipmentExerciseJob(equipmentInteractionProvider, equipmentId, flags));
        }

        public virtual CharacterPortrait GetPortrait()
        {
            if (portrait == null)
            {
                portrait = runtimeCharacterSetupData.CreatePortrait(transform, personInfo.appearance, actorSettings.portraitMaterialPolicyName);
            }

            return portrait;
        }

        public Vector3 GetNearestWorldPoint(Vector3 position, float otherAgentRadius, float distanceMultiplier)
        {
            Vector3 result = transform.position;
            if (distanceMultiplier > 0)
            {
                var distanceVector = position - transform.position;
                var distanceFromActor = (otherAgentRadius * distanceMultiplier + navigationAgentRadius * distanceMultiplier);
                result = (transform.position + distanceVector.normalized * distanceFromActor);
            }

            return result;
        }

        void ICharacterActor.UpdateCharacterType (IPersonInfo personInfo)
        {
            this.personInfo = personInfo;
            CustomizeCharacter();
        }

        void ICharacterActor.UpdateAppearance()
        {
            CustomizeCharacter();
        }

        void ICharacterActor.TransferToRoom (RoomConductor roomConductor)
        {
            Assert.IsNotNull(roomConductor);

            if (this.roomConductor != roomConductor)
            {
                this.roomConductor = roomConductor;
                transform.SetParent(roomConductor.roomObject.transform, true);
                NotifyPositionUpdate();
            }
        }
        #endregion

        #region Private static methods.
        static AnimationWalking GetWalkingAnimation (MovementMode mode, bool blendAnimations)
        {
            if (blendAnimations)
            {
                return AnimationWalking.Auto;
            }
            else
            {
                switch(mode)
                {
                    case MovementMode.Walking:      return AnimationWalking.Walking;
                    case MovementMode.WalkingFast:  return AnimationWalking.WalkingFast;
                    case MovementMode.Running:      return AnimationWalking.Running;
                }
            }
            return AnimationWalking.Auto;
        }
        #endregion

        #region Private methods.
        void CustomizeCharacter()
        {
            bodyController.Customize(runtimeCharacterSetupData, personInfo.appearance);

            if (portrait != null)
            { 
                portrait.bodyController.Customize(runtimeCharacterSetupData, personInfo.appearance);
            }
        }

        void UpdateVisibility()
        {
            var layer = (personInfo != null && (personInfo.state == PersonState.InQueue || personInfo.state == PersonState.Entering || personInfo.state == PersonState.Leaving))
                ? originalLayer
                : (displayMode == DisplayMode.Hidden) ? LayerMask.NameToLayer("Hidden") : originalLayer;

            gameObject.layer = layer;

            foreach (Transform child in transform)
            {
                if (portrait == null || !ReferenceEquals(child, portrait.transform))
                {
                    child.SetLayerRecursive(layer);
                }
            }
        }

        int SetAnimation(string categoryName, int animationIdx, float speed)
        {
            if (navMeshAgent != null && navMeshAgent.enabled && navMeshAgent.hasPath && categoryName != CharacterBodyController.animCategoryWalking)
            {
                Debug.LogWarningFormat("NavMeshAgent is active when you're trying to set animation! Actor: {0} ({1}), GO: '{2}', Category: '{3}', AnimationIdx: {4}."
                    , id, personInfo.name, gameObject.name, categoryName, animationIdx);
            }

            return bodyController.SetAnimation(categoryName, animationIdx, speed);
        }

        bool IsAnimation(string categoryName)
        {
            return bodyController.IsAnimation(categoryName);
        }

        bool IsAnimation(string categoryName, int animationIdx)
        {
            return bodyController.IsAnimation(categoryName, animationIdx);
        }

        bool IsAnimation(int categoryIdx, int animationIdx)
        {
            return bodyController.IsAnimation(categoryIdx, animationIdx);
        }

        int GetAnimation()
        {
            return bodyController.GetAnimation();
        }

        int GetAnimation(out int animationIdx)
        {
            return bodyController.GetAnimation(out animationIdx);
        }

        bool IsOneOfAnimationCategories(params string[] categories)
        {
            return bodyController.IsOneOfAnimationCategories(categories);
        }

        void SetIdle ()
        {
            var useExtendedIdleAnimation = personInfo.useExtendedIdleAnimation;

            SetAnimation( useExtendedIdleAnimation ? CharacterBodyController.animCategoryIdleComplete : CharacterBodyController.animCategoryIdlePassive
                        , useExtendedIdleAnimation ? (int)AnimationIdleComplete.Random : (int)AnimationIdlePassive.Random
                        , 1.0f);
        }

        void ReportMovementCompleteResult(MovementResult result, Action<int, MovementResult> newOnMovementComplete = null)
        {
            var currentOnComplete = onMovementComplete;
            onMovementComplete = null; // Reset it before invocation of callbacks to prevent callback loops.

            currentOnComplete?.Invoke(id, result);
            newOnMovementComplete?.Invoke(id, result);
        }

        void QueueJob(IEnumerator jobCoroutine)
        {
            DebugLogLocal("QueueJob (Start)", "Job: '{0}'", jobCoroutine);

            {
#if DEBUG
                jobsQueue.Add(new JobInfo(jobCoroutine, StackTraceUtility.ExtractStackTrace()));

                if (jobsQueue.Count >= maxJobsWarningCount)
                {
                    var jobsDump = new StringBuilder();
                    foreach (var job in jobsQueue)
                    {
                        jobsDump.AppendLine(job.ToString());
                    }

                    Debug.LogWarningFormat("CharacterActor.QueueJob: Name: '{0}'. Queued {1} jobs already, probably some jobs are stuck! Jobs list:\n{2}"
                        , gameObject.name, jobsQueue.Count, jobsDump.ToString());
                }
#else
                jobsQueue.Add(new JobInfo(jobCoroutine));
#endif
                if (jobsQueue.Count == 1)
                {
                    StartCoroutine(JobWrapper(jobsQueue[0]));
                }
            }
        }

        IEnumerator JobWrapper(JobInfo jobInfo)
        {
#if DEBUG
            debugInJob = true;
#endif
            DebugLogLocal("JobWrapper (Start)", "Job: '{0}'.", jobInfo.job);

            while (jobInfo.job.MoveNext())
            {
                yield return jobInfo.job.Current;
            }

#if UNITY_EDITOR
            debugLastCompleteJobName = jobInfo.job.ToString();
#endif

#if DEBUG
            debugInJob = false;
#endif
            DebugLogLocal("JobWrapper (End)", "Job: '{0}'.", jobInfo.job);

            // End current job and try to start next, if needed.
            Assert.IsTrue(jobsQueue.Count > 0);
            if (jobsQueue.Count > 0)
            {
                jobsQueue.RemoveAt(0);
                if (jobsQueue.Count > 0)
                    StartCoroutine(JobWrapper(jobsQueue[0]));
            }

            yield break;
        }

        IEnumerator LookAtJob(float time, Vector3 eulerAngles, Action onComplete)
        {
            DebugLogLocal("LookAtJob (Schedule)", "time: {0}, eulerAngles: {1}", time, eulerAngles);

            yield return null;

            DebugLogLocal("LookAtJob (Start)", "time: {0}, eulerAngles: {1}", time, eulerAngles);

            if (runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is running exercise! Cannot do 'look at'.", id);
                yield break;
            }

            if ((navigationController == null || !navigationController.enabled) &&
                IsOneOfAnimationCategories(CharacterBodyController.animCategoryIdlePassive, CharacterBodyController.animCategoryIdleComplete, CharacterBodyController.animCategorySocial))
            {
                var id = LeanTween.rotate (gameObject, eulerAngles, time ).id;
                yield return new WaitUntil(() =>
                {   // TODO: Temporary exception handling, looking for a cause of this issue!
                    try
                    {
                        return !LeanTween.isTweening (id);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogErrorFormat("LeanTween exception! id: {0}, Exception: {1}", id, ex);
                    }
                    return true;
                });
            }
            else
            {
                Debug.LogWarningFormat("Trying to perform 'look at' in a wrong state (navigating or wrong animation). Actor: {0} ({1}), Navigation: {2}, Current animation category: {3}."
                    , id, personInfo.name, navigationController != null ? navigationController.enabled : false, GetAnimation());
            }

            onComplete?.Invoke();

            DebugLogLocal("LookAtJob (End)");
            yield break;
        }

        IEnumerator StartEquipmentExerciseJob
            ( IEquipmentInteractionProvider equipmentInteractionProvider
            , int equipmentId
            , int exerciseTier
            , int exerciseAnimation
            , ExerciseInitiator initiator
            , TimeSpan duration
            , ExerciseFlags flags
            , float speed)
        {
            Assert.IsNotNull(equipmentInteractionProvider);

            DebugLogLocal("StartEquipmentExerciseJob (Schedule)", "equipmentId: {0}, exerciseTier: {1}, exerciseAnimation: {2}, Duration: {3}, Flags: {4}, speed: {5}"
                , equipmentId, exerciseTier, exerciseAnimation, duration, flags, speed);

            yield return null;

            DebugLogLocal("StartEquipmentExerciseJob (Start)", "equipmentId: {0}, exerciseTier: {1}, exerciseAnimation: {2}, Duration: {3}, Flags: {4}, speed: {5}"
                , equipmentId, exerciseTier, exerciseAnimation, duration, flags, speed);

            if (runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is already running exercise! Cannot start exercise.", id);
                yield break;
            }

            if (navMeshAgent != null)
            {
                navMeshAgent.enabled = false;
            }

            if (navMeshObstacle != null)
            {
                navMeshObstacle.enabled = true;
            }

            if ((flags & (ExerciseFlags.Restored | ExerciseFlags.EndsInOffline)) == 0)
            {
                var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(equipmentId);
                if (equipmentActor == null)
                {
                    Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot start exercise.", equipmentId);
                    yield break;
                }

                var lastClosestApproachingPoint = equipmentActor.GetLastClosestApproachingPoint();

                NavigationPoint navPoint = (lastClosestApproachingPoint != null)
                    ? lastClosestApproachingPoint.Value
                    : equipmentActor.GetClosestApproachingPoint(transform.position, navigationAreaMask, exerciseTier);

                equipmentActor = null; // Release it before yield, it can be destroyed while waiting.

                if (runtimeCharacterSetupData.csd.exerciseApproachTransitionTime > 0)
                {
                    yield return navPoint.InterpolateTo(gameObject, runtimeCharacterSetupData.csd.exerciseApproachTransitionTime, runtimeCharacterSetupData.csd.exerciseApproachTransitionTime);
                }
                else
                {
                    transform.position = navPoint.position;
                    transform.rotation = navPoint.rotation;
                }
            }

            {
                var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(equipmentId);
                if (equipmentActor == null)
                {
                    Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot start exercise.", equipmentId);
                    yield break;
                }

                var workingPoint = equipmentActor.StartUsingEquipment(transform, exerciseTier);

                if ((flags & ExerciseFlags.EndsInOffline) == 0)
                {
                    if (shadowRenderer != null)
                    {
                        shadowRenderer.gameObject.SetActive(false);
                    }

                    SetAnimation(CharacterBodyController.animCategoryEquipment, exerciseAnimation, speed);

                    eventRedirectionTarget = equipmentActor.gameObject;

                    equipmentActor = null; // Release it before yield, it can be destroyed while waiting.

                    if ((flags & ExerciseFlags.Restored) == 0)
                    {
                        yield return workingPoint.InterpolateTo(gameObject);
                    }
                    else
                    {   // 'Restored' exercise - do quick positioning, without transitions.
                        transform.position = workingPoint.position;
                        transform.rotation = workingPoint.rotation;
                    }

//                     if (navMeshObstacle != null)
//                     {
//                         navMeshObstacle.carving = true;
//                     }
                }
                else
                {   // 'Offline' exercise end - do quick positioning, without transitions.
                    transform.position = workingPoint.position;
                    transform.rotation = workingPoint.rotation;
                }
            }

            runningExercise = true;

            DebugLogLocal("StartEquipmentExerciseJob (End)");
            yield break;
        }

        IEnumerator SwitchEquipmentExerciseJob
            ( IEquipmentInteractionProvider equipmentInteractionProvider
            , int exerciseTier
            , int exerciseAnimation
            , float speed)
        {
            Assert.IsNotNull(equipmentInteractionProvider);

            DebugLogLocal("SwitchEquipmentExerciseJob (Schedule)", "exerciseTier: {0}, exerciseAnimation: {1}, speed: {2}", exerciseTier, exerciseAnimation, speed);

            yield return null;

            DebugLogLocal("SwitchEquipmentExerciseJob (Start)", "exerciseTier: {0}, exerciseAnimation: {1}, speed: {2}", exerciseTier, exerciseAnimation, speed);

            if (!runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is not running exercise! Cannot switch exercise.", id);
                yield break;
            }

            var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(personInfo.workoutEquipmentId);
            if (equipmentActor == null)
            {
                Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot switch exercise.", personInfo.workoutEquipmentId);
                yield break;
            }

            var workingPoint = equipmentActor.StartUsingEquipment(transform, exerciseTier);
            SetAnimation(CharacterBodyController.animCategoryEquipment, exerciseAnimation, speed);

            equipmentActor = null; // Release it before yield, it can be destroyed while waiting.

            yield return workingPoint.InterpolateTo(gameObject);

//             if (navMeshObstacle != null)
//             {
//                 navMeshObstacle.carving = true;
//             }

            DebugLogLocal("SwitchEquipmentExerciseJob (End)");
            yield break;
        }

        IEnumerator EndEquipmentExerciseJob(IEquipmentInteractionProvider equipmentInteractionProvider, int equipmentId, ExerciseFlags flags)
        {
            Assert.IsNotNull(equipmentInteractionProvider);

            DebugLogLocal("EndEquipmentExerciseJob (Schedule)", "equipmentId: {0}", equipmentId);

            yield return null;

            DebugLogLocal("EndEquipmentExerciseJob (Start)", "equipmentId: {0}", equipmentId);

            var equipmentActor = equipmentInteractionProvider.GetEquipmentActor(equipmentId);
            if (equipmentActor == null)
            {
                Debug.LogErrorFormat("Equipment actor with id {0} not found! Cannot end exercise.", equipmentId);
                yield break;
            }

//             if (navMeshObstacle != null)
//             {
//                 navMeshObstacle.carving = false;
//             }

            if (shadowRenderer != null)
            {
                shadowRenderer.gameObject.SetActive(true);
            }

            if (navMeshObstacle != null)
            {
                navMeshObstacle.enabled = false;
            }

            eventRedirectionTarget = null;

            NavigationPoint navPoint = equipmentActor.GetLeavingPoint(transform.position, navigationAreaMask);

            if ((flags & ExerciseFlags.EndsInOffline) == 0)
            {
                if (!runningExercise)
                {
                    Debug.LogErrorFormat("Person actor with id {0} is not running exercise! Cannot end exercise.", id);
                    yield break;
                }

                SetIdle();

                equipmentActor.StopUsingEquipment();

                yield return navPoint.InterpolateTo(gameObject);

                yield return new WaitUntil(() => !animator.IsInTransition(0));
            }
            else
            {   
                // 'Offline' exercise end - do quick positioning, without transitions.
                transform.position = navPoint.position;
                transform.rotation = navPoint.rotation;

                if (IsAnimation(CharacterBodyController.animCategoryEquipment))
                {
                    SetIdle();

                    yield return new WaitUntil(() => !animator.IsInTransition(0));
                }
            }

            if (navMeshAgent != null)
            {
                navMeshAgent.enabled = true;
            }

            runningExercise = false;

            DebugLogLocal("EndEquipmentExerciseJob (End)");
            yield break;
        }

        IEnumerator StartMovementJob
            ( Vector3 targetWorldPos
            , IMovementSettingsInfo movementSettingsInfo
            , Action<int, MovementResult> onMovementComplete)
        {
            Assert.IsNotNull(navMeshAgent);
            Assert.IsNotNull(navigationController);

            DebugLogLocal("StartMovementJob (Schedule)", "targetWorldPos: {0}, Movement settings: [{1}]"
                    , targetWorldPos, movementSettingsInfo);

            yield return null;

            DebugLogLocal("StartMovementJob (Start)", "targetWorldPos: {0}, Movement settings: [{1}]"
                    , targetWorldPos, movementSettingsInfo);

            if (runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is running exercise! Cannot start movement.", id);
                yield break;
            }

            var distanceVector = (targetWorldPos - transform.position);

            var areaMask = SelectNavMeshAgentAreaMask();

            var distaneMagnitude = distanceVector.magnitude;
            if (distaneMagnitude > Math.Max(navMeshAgent.stoppingDistance, Mathf.Epsilon))
            {
                var speed = GetMovementSpeed(movementSettingsInfo.mode);

                if (navigationController.NavigateTo
                    (targetWorldPos
                    , speed
                    , movementSettingsInfo.priorityDelta
                    , areaMask))
                {
                    var rotateTweenId = -1;

                    if (navigationController.enabled && navMeshAgent.pathPending)
                    {
                        yield return new WaitWhile(() => navigationController.enabled && navMeshAgent.pathPending);
                    }

                    var failed = false;

                    if (navigationController.enabled) // Can be already failed because of yield.
                    {
                        if (actorSettings.moveStartRotationSpeed > 0)
                        {
                            var direction = (navMeshAgent.steeringTarget - transform.position).normalized;
                            direction.y = 0.0f; // No pitch changes!
                            if (direction != Vector3.zero)
                            {
                                Quaternion newRotation = Quaternion.LookRotation(direction);

                                var angleDegrees = Quaternion.Angle(newRotation, transform.rotation);
                                if (angleDegrees > 0)
                                {
                                    navMeshAgent.updateRotation = false;

                                    var time = (1.0f / actorSettings.moveStartRotationSpeed) * angleDegrees;
                                    rotateTweenId = LeanTween.rotate(gameObject, newRotation.eulerAngles, time).id;
                                }
                            }
                        }

                        blendAnimations = movementSettingsInfo.blendAnimations;

                        if (animator != null)
                        {
                            var animationId = (int)GetWalkingAnimation(movementSettingsInfo.mode, movementSettingsInfo.blendAnimations);

                            SetAnimation(CharacterBodyController.animCategoryWalking, animationId, 1.0f);
                            //Debug.LogFormat("StartMovementJob(Transition wait): Name: '{0}'.", gameObject.name);
                            // yield return new WaitUntil (() => !animator.IsInTransition(0));
                            //Debug.LogFormat("StartMovementJob(Transition wait end): Name: '{0}'.", gameObject.name);
                        }

                    }
                    else
                    {
                        failed = true;
                    }

                    if (rotateTweenId >= 0)
                    {
                        if (navigationController.enabled)
                        {
                            yield return new WaitWhile(() => LeanTween.isTweening(rotateTweenId));
                        }
                        else
                        {
                            LeanTween.cancel(rotateTweenId);
                        }
                    }

                    navMeshAgent.updateRotation = true;

                    if (!navigationController.enabled)
                        failed = true;

                    if (failed)
                    {
                        DebugLogLocal("StartMovementJob (No move, failed while finding path)");
                        PerformStopMovement();
                        ReportMovementCompleteResult(MovementResult.Failed, onMovementComplete);
                    }
                    else
                    {
                        DebugLogLocal("StartMovementJob (Done)", "existing CB: {0}, new CB: {1}"
                                , this.onMovementComplete != null ? this.onMovementComplete.Method.ToString() : "<none>"
                                , onMovementComplete != null ? onMovementComplete.Method.ToString() : "<none>");

                        ReportMovementCompleteResult(MovementResult.UpdatedDestination);
                        this.onMovementComplete += onMovementComplete;
                    }
                }
                else
                {
                    DebugLogErrorLocal("StartMovementJob (No move, failed)", "Destination: [{0}], Target: {1}, Movement settings: [{2}]."
                        , movementDestination, targetWorldPos, movementSettingsInfo);
                    PerformStopMovement();
                    ReportMovementCompleteResult(MovementResult.Failed, onMovementComplete);
                }
            }
            else
            {
                if (runtimeCharacterSetupData.csd.quickInterpolationDistance > Mathf.Epsilon &&
                    distaneMagnitude > runtimeCharacterSetupData.csd.quickInterpolationDistance)
                {
                    DebugLogLocal("StartMovementJob (No move but quick interpolate)");

                    var navPoint = new NavigationPoint(targetWorldPos, transform.rotation, runtimeCharacterSetupData.csd.quickInterpolationTime, runtimeCharacterSetupData.csd.quickInterpolationTime);
                    yield return navPoint.InterpolateTo(gameObject);
                }
                else
                {
                    DebugLogLocal("StartMovementJob (No move, too close)");
                }

                PerformStopMovement();
                ReportMovementCompleteResult(MovementResult.Complete, onMovementComplete);
            }

            DebugLogLocal("StartMovementJob (End)");
            yield break;
        }

        IEnumerator PlayEmoteAnimationJob(AnimationSocial animationId, int loopCount, Action onComplete)
        {
            DebugLogLocal("PlayEmoteAnimationJob (Schedule)", "animationId: {0}, loopCount: {1}"
                , animationId, loopCount);

            yield return null;

            DebugLogLocal("PlayEmoteAnimationJob (Start)", "animationId: {0}, loopCount: {1}"
                , animationId, loopCount);

            if (runningExercise)
            {
                Debug.LogErrorFormat("Person actor with id {0} is running exercise! Cannot play emote animation.", id);
                yield break;
            }

            if (animator != null && (navigationController == null || !navigationController.enabled) &&
                IsOneOfAnimationCategories(CharacterBodyController.animCategoryIdlePassive, CharacterBodyController.animCategoryIdleComplete, CharacterBodyController.animCategorySocial))
            {
                SetAnimation(CharacterBodyController.animCategorySocial, (int)animationId, 1.0f);
                yield return new WaitUntil (() => !animator.IsInTransition(0));

                if (animationCompleteRouter != null &&
                    loopCount >= 0 &&
                    (navigationController == null || !navigationController.enabled) &&
                    IsAnimation(CharacterBodyController.animCategorySocial)) // Can be switched to another already.
                {
                    bool finished = false;

                    UnityAction<int> tmpAction = null;

                    animationCompleteRouter.animationCompleteEvent.AddListener (tmpAction = new UnityAction<int>(
                        delegate (int currentLoop)
                        { 
                            if (loopCount == 0 || currentLoop >= loopCount /*|| !IsAnimation(socialCategoryIdx, (int)animationId)*/)
                                finished = true;
                        } 
                    ));

                    yield return new WaitUntil (() => finished || (navigationController != null && navigationController.enabled) || !IsAnimation(CharacterBodyController.animCategorySocial));

                    animationCompleteRouter.animationCompleteEvent.RemoveListener(tmpAction);

                    SetIdle();
                    yield return new WaitUntil (() => !animator.IsInTransition(0));

                    onComplete?.Invoke();
                }
                else
                {
                    onComplete?.Invoke();
                }
            }
            else
            {
                Debug.LogWarningFormat("Trying to perform emote in a wrong state (navigating or wrong animation). Actor: {0} ({1}), Navigation: {2}, Current animation category: {3}, Emote animation: {4}."
                    , id, personInfo.name, navigationController != null ? navigationController.enabled : false, GetAnimation(), animationId);

                onComplete?.Invoke();
            }

            DebugLogLocal("PlayEmoteAnimationJob (End)");
            yield break;
        }

        private int OnGetStoppedAvoidanceDelta()
        {
            return personInfo != null ? personInfo.stoppedAvoidanceDelta : 0;
        }

        void OnNavigationUpdate()
        {
            NotifyPositionUpdate();

#if DEBUG
            if (Application.isEditor && animator != null && jobsQueue.Count == 0)
            {
                int categoryIdx;
                var animationSetupData = bodyController.GetAnimationSetupData(CharacterBodyController.animCategoryWalking, out categoryIdx);
                if (animationSetupData != null)
                {
                    var currentCategory = GetAnimation();
                    if (currentCategory != animationSetupData.categoryParameterValue)
                    {
                        Debug.LogWarningFormat("CharacterActor.OnNavigationUpdated: Name: '{0}'. Wrong animation category {1} while navigating (should be {2})."
                            , gameObject.name, currentCategory, animationSetupData.categoryParameterValue);
                    }
                }
            }
#endif
        }

        void OnNavigationComplete(NavigationController.NavigationResult result)
        {
            DebugLogLocal("OnNavigationComplete", "CB: {0}, result: {0}"
                , onMovementComplete != null ? onMovementComplete.Method.ToString() : "<none>", result);

            Assert.IsFalse(navigationController.enabled);

            if (animator != null)
            {
                if (blendAnimations)
                    animator.SetFloat(actorSettings.velocityAnimatorParameterHashId, 0);

                blendAnimations = false;

                SetIdle();
            }

            NotifyPositionUpdate();

            switch (result)
            {
                case NavigationController.NavigationResult.Complete:    ReportMovementCompleteResult(MovementResult.Complete);  break;
                case NavigationController.NavigationResult.Failed:      ReportMovementCompleteResult(MovementResult.Failed);    break;
                case NavigationController.NavigationResult.Stuck:       ReportMovementCompleteResult(MovementResult.Stuck);     break;
            }
        }

        void RotateTowardsCamera()
        {
            DoLookAt(Camera.main.transform.position, actorSettings.rotateToCameraSpeed);
        }

        void SelectionPingScale()
        {
            if (actorSettings.selectionPingScale.magnitude > 0 && actorSettings.selectionPingTime > 0)
            {
                LeanTween.scale (gameObject, actorSettings.selectionPingScale, actorSettings.selectionPingTime).setLoopPingPong(1);
            }
        }

        void ShowSelectionCursor()
        {
            if (assignedCursor == null)
            {
                var cursorObject = sharedDataProvider.GetCursorObject(cursorType);
                if (cursorObject != null)
                {
                    cursorObject.transform.SetParent(transform, false);
                    cursorObject.SetActive(true);
                    assignedCursor = cursorObject;
                }
            }
        }

        void NotifyPositionUpdate ()
        {
            if (onCharacterActorPositionUpdated != null)
            {
                var position = roomConductor.coordsMapper.ConvertPositionToRoomCoordinatesRounded(transform.localPosition);
                var heading = transform.localRotation.eulerAngles.y;

                onCharacterActorPositionUpdated(id, position, heading);
            }
        }

        int SelectNavMeshAgentAreaMask ()
        {
            return (personInfo.state == PersonState.InQueue || personInfo.state == PersonState.Entering || personInfo.state == PersonState.Leaving)
                ? runtimeCharacterSetupData.transferAreaMask
                : runtimeCharacterSetupData.normalWalkAreaMask;
        }

        void HideSelectionCursor()
        {
            if (assignedCursor != null)
            {
                var rememberParent = assignedCursor.GetComponent<RememberParent>();
                if (rememberParent != null)
                    rememberParent.ResetParent();
                assignedCursor.SetActive(false);
                assignedCursor = null;
            }
        }

        void DoLookAt(Vector3 worldLookAtPosition, float rotationSpeed, Action onComplete = null)
        {
            var direction = worldLookAtPosition - transform.position;
            direction.y = 0.0f; // No pitch changes!
            if (direction != Vector3.zero)
            {
                direction.Normalize();

                Quaternion newRotation = Quaternion.LookRotation ( direction );
                if (rotationSpeed > 0)
                {
                    var angleDegrees =  Quaternion.Angle (newRotation, transform.rotation);
                    if (angleDegrees > 0)
                    {
                        var time = (1.0f / rotationSpeed) * angleDegrees;
                        QueueJob(LookAtJob(time, newRotation.eulerAngles, onComplete));
                        return;
                    }
                }
                else
                {
                    transform.rotation = newRotation;
                }
            }

            onComplete?.Invoke();
        }

        void PerformStopMovement ()
        {
            if (navMeshAgent != null &&
                navigationController != null &&
                navigationController.enabled)
            {
                navigationController.StopMovement();

                NotifyPositionUpdate();
            }

            if (animator != null)
            {
                if (blendAnimations)
                    animator.SetFloat(actorSettings.velocityAnimatorParameterHashId, 0);

                blendAnimations = false;

                SetIdle();
            }
        }

        float GetMovementSpeed(MovementMode mode)
        {
            var idx = Math.Min(actorSettings.movementSpeeds.Length - 1, Math.Max(0, (int)mode));

            return actorSettings.movementSpeeds[idx];
        }
        #endregion

        #region Editor-only methods.
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogLocal(string label)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

            if (Selection.Contains(gameObject))
            {
                Debug.LogFormat("Frame: {0}, Name: '{1}' - <color=blue>{2}</color>."
                    , Time.frameCount, gameObject.name, label);
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - {2}."
                    , Time.frameCount, gameObject.name, label));
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogLocal(string label, string format, params object[] args)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

            if (Selection.Contains(gameObject))
            {
                Debug.LogFormat("Frame: {0}, Name: '{1}' - <color=blue>{2}: {3}</color>."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args));
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - {2}: {3}."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args)));
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogWarningLocal(string label)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

#if DEBUG
            if (debugInJob && jobsQueue.Count > 0)
            {
                Debug.LogWarningFormat("Frame: {0}, Name: '{1}' - <color=yellow>{2}</color>. Job submit stack:\n{3}"
                    , Time.frameCount, gameObject.name, label, jobsQueue[0].debugSubmitStack);
            }
            else
#endif
            {
                Debug.LogWarningFormat("Frame: {0}, Name: '{1}' - <color=yellow>{2}</color>."
                    , Time.frameCount, gameObject.name, label);
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - ! {2}."
                    , Time.frameCount, gameObject.name, label));
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogWarningLocal(string label, string format, params object[] args)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

#if DEBUG
            if (debugInJob && jobsQueue.Count > 0)
            {
                Debug.LogWarningFormat("Frame: {0}, Name: '{1}' - <color=yellow>{2}: {3}</color>. Job submit stack:\n{4}"
                    , Time.frameCount, gameObject.name, label, string.Format(format, args), jobsQueue[0].debugSubmitStack);
            }
            else
#endif
            {
                Debug.LogWarningFormat("Frame: {0}, Name: '{1}' - <color=yellow>{2}: {3}</color>."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args));
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - ! {2}: {3}."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args)));
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogErrorLocal(string label)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

#if DEBUG
            if (debugInJob && jobsQueue.Count > 0)
            {
                Debug.LogErrorFormat("Frame: {0}, Name: '{1}' - <color=red>{2}</color>. Job submit stack:\n{3}"
                    , Time.frameCount, gameObject.name, label, jobsQueue[0].debugSubmitStack);
            }
            else
#endif
            {
                Debug.LogErrorFormat("Frame: {0}, Name: '{1}' - <color=red>{2}</color>."
                    , Time.frameCount, gameObject.name, label);
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - !!! {2}."
                    , Time.frameCount, gameObject.name, label));
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void DebugLogErrorLocal(string label, string format, params object[] args)
        {
#if UNITY_EDITOR
            Assert.IsNotNull(label);

#if DEBUG
            if (debugInJob && jobsQueue.Count > 0)
            {
                Debug.LogErrorFormat("Frame: {0}, Name: '{1}' - <color=red>{2}: {3}</color>. Job submit stack:\n{4}"
                    , Time.frameCount, gameObject.name, label, string.Format(format, args), jobsQueue[0].debugSubmitStack);
            }
            else
#endif
            {
                Debug.LogErrorFormat("Frame: {0}, Name: '{1}' - <color=red>{2}: {3}</color>."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args));
            }

            while (debugLastLogEntries.Count >= debugMaxLogEntries)
                debugLastLogEntries.RemoveAt(0);

            debugLastLogEntries.Add(string.Format("Frame: {0}, Name: '{1}' - !!! {2}: {3}."
                    , Time.frameCount, gameObject.name, label, string.Format(format, args)));
#endif
        }
        #endregion

        #region Unity Editor API
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (Application.isPlaying && roomConductor != null && worldPositionProvider != null)
            {
                var selected = Selection.Contains(gameObject);

                if (personInfo != null && ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) || selected))
                {
                    var stringBuilder = new StringBuilder(256);

                    stringBuilder.AppendFormat("{0}: '{1}' ({2}) {3} @{4}. Room: {5}"
                        , personInfo.id, personInfo.name, personInfo.state, personInfo.position, Mathf.RoundToInt(personInfo.heading), personInfo.roomName);

                    if (selected && navigationController.enabled)
                    {
                        stringBuilder.AppendFormat("\nDestination: {0}, target: {1}", movementDestination, movementWorldPosition);
                    }

                    if (selected && jobsQueue.Count > 0)
                    {
                        stringBuilder.AppendFormat("\nJob: {0}", jobsQueue[0]);
                    }

                    if (selected && onMovementComplete != null)
                    {
                        stringBuilder.AppendFormat("\nHas movement CB: {0}", onMovementComplete.Method.ToString());
                    }

                    if (debugLastCompleteJobName != null)
                        stringBuilder.AppendFormat("\nLast complete job: {0}.", debugLastCompleteJobName);

                    var dbgInfo = personInfo.GetDebugInfo(selected);
                    if (dbgInfo != null)
                    {
                        stringBuilder.AppendLine();
                        stringBuilder.Append(dbgInfo);
                    }

                    if (selected && debugLastLogEntries.Count > 0)
                    {
                        for (int i = 0; i < debugLastLogEntries.Count; i++)
                        {
                            stringBuilder.AppendFormat("\n#{0:D2}: {1}", i + 1, debugLastLogEntries[i]);
                        }
                    }

                    var position = worldPositionProvider.GetWorldPosition();
                    position.y += 0.15f;

                    Handles.Label(position, stringBuilder.ToString(), EditorGizmoHelper.instance.GetOverheadLabelStyle());
                }
            }
        }
#endif
        #endregion
    }
}