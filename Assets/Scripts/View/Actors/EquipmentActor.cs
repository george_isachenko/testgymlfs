﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using Data.Estate;
using Data.Room;
using Data.Estate.Info;
using View.Rooms;
using View.Actions;
using View.Actors.Settings;
using View.Actors.Events;
using View.EditorHelpers;
using System.Linq;
using UnityEngine.AI;
using System.Collections.Generic;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Actors
{
    [ExecuteInEditMode]
    public abstract class EquipmentActor
        : Actor<int>
        , IEquipmentActor
        , IDragHandler
        , IInitializePotentialDragHandler
        , IEquipmentActorEvents
    {
        static readonly float impostorCooldownTime = 5.0f;
        static readonly string approachPointPrefix = "approach_point";
        static readonly string workingPointPrefix = "working_point";
        static readonly string leavingPointPrefix = "leaving_point";

        public event EquipmentActorPlacementUpdated onEquipmentActorPlacementUpdated;

        IEquipmentInfo IEquipmentActor.equipmentInfo
        {
            get
            {
                return equipmentInfo;
            }
        }

        int IEquipmentActor.selectedEquipmentAnimation
        {
            set
            {
                selectedEquipmentAnimation = value;
            }
        }

        float IEquipmentActor.animationSpeed
        {
            set
            {
                animationSpeed = value;
            }
        }

        public Vector3 rotationEuler
        {
            get
            {
                var eulerAngles = transform.localRotation.eulerAngles;
                if (additionalRotation != 0)
                    eulerAngles.y = (eulerAngles.y + 360.0f - additionalRotation) % 360.0f;
                return eulerAngles;
            }
        }

        EquipmentPlacement IEquipmentActor.placement
        {
            get
            {
                return editingThisEquipment ? pendingPlacement : placement;
            }
        }

        public RoomCoordinates wallPosition
        {
            get
            {
                if (wallMounted)
                {
                    var p = editingThisEquipment ? pendingPlacement : placement;
                    var os = orientedSize;
                    switch (p.heading)
                    {
                        case 0: return new RoomCoordinates(p.position.x, p.position.y);
                        case 1: return new RoomCoordinates(p.position.x, p.position.y);
                        case 2: return new RoomCoordinates(p.position.x, p.position.y + os.y - 1);
                        case 3: return new RoomCoordinates(p.position.x + os.x - 1, p.position.y);
                    }
                }
                return editingThisEquipment ? pendingPlacement.position : placement.position;
            }
        }

        public RoomCoordinates wallPositionWithMargin
        {
            get
            {
                if (wallMounted)
                {
                    var p = editingThisEquipment ? pendingPlacement : placement;
                    var os = orientedSize;
                    var marginShift = hasWallMargin ? 1 : 0;
                    switch (p.heading)
                    {
                        case 0: return new RoomCoordinates(p.position.x - marginShift, p.position.y);
                        case 1: return new RoomCoordinates(p.position.x, p.position.y - marginShift);
                        case 2: return new RoomCoordinates(p.position.x - marginShift, p.position.y + os.y - 1);
                        case 3: return new RoomCoordinates(p.position.x + os.x - 1, p.position.y - marginShift);
                    }
                }
                return editingThisEquipment ? pendingPlacement.position : placement.position;
            }
        }

        RoomCoordinates IEquipmentActor.size
        {
            get
            {
                return size;
            }
        }

        RoomCoordinates IEquipmentActor.wallSize
        {
            get
            {
                if (wallMounted)
                {
                    return new RoomCoordinates(size.x, 1);
                }
                else
                    return size;
            }
        }

        RoomCoordinates IEquipmentActor.wallSizeWithMargin
        {
            get
            {
                if (wallMounted)
                {
                    return new RoomCoordinates(hasWallMargin ? size.x + 2 : size.x, 1);
                }
                else
                    return size;
            }
        }

        public RoomCoordinates orientedSize
        {
            get
            {
                return size.GetOrientedSize (editingThisEquipment ? pendingPlacement.heading : placement.heading);
            }
        }

        public RoomCoordinates orientedWallSize
        {
            get
            {
                if (wallMounted)
                {
                    var heading = editingThisEquipment ? pendingPlacement.heading : placement.heading;
                    return RoomCoordinates.GetOrientedSize(new RoomCoordinates(size.x, 1), heading);
                }
                else
                    return default(RoomCoordinates);
            }
        }

        public RoomCoordinates orientedWallSizeWithMargin
        {
            get
            {
                if (wallMounted)
                {
                    var heading = editingThisEquipment ? pendingPlacement.heading : placement.heading;
                    return RoomCoordinates.GetOrientedSize(new RoomCoordinates(hasWallMargin ? size.x + 2 : size.x, 1), heading);
                }
                else
                    return default(RoomCoordinates);
            }
        }

        bool IEquipmentActor.occupiesFloorSpace
        {
            get
            {
                return occupiesFloorSpace;
            }
        }

        bool IEquipmentActor.wallMounted
        {
            get
            {
                return wallMounted;
            }
        }

        Vector3 IEquipmentActor.shift
        {
            get
            {
                return shift;
            }
        }

        public Vector3 shiftRotated
        {
            get
            {
                return placement.GetShiftRotated(shift);
            }
        }

        int IEquipmentActor.estateType
        {
            get
            {
                return estateType;
            }
        }

        PlacementResult IEquipmentActor.placementState
        {
            get
            {
                return placementState;
            }
        }

        EquipmentActorDragEvent IEquipmentActor.dragEvent
        {
            get
            {
                if (dragEvent == null)
                {
                    dragEvent = new EquipmentActorDragEvent();
                }
                return dragEvent;
            }
        }

        public bool useDragThreshold
        {
            get; set;
        }

        int IEquipmentActor.layer
        {
            get
            {
                return gameObject.layer;
            }

            set
            {
                gameObject.layer = value;
            }
        }

        string IEquipmentActor.name
        {
            get
            {
                return gameObject.name;
            }
        }

        Vector2 size2D
        {
            get
            {
                return (roomConductor != null)
                    ? roomConductor.coordsMapper.ConvertPosition2DFromRoomCooordinates(wallMounted ? orientedWallSize : orientedSize)
                    : Vector2.zero;
            }
        }

        bool IEquipmentActor.initiallyLocked
        {
            get
            {
                return initiallyLocked;
            }
        }

        bool IEquipmentActor.editable
        {
            get
            {
                return editable;
            }
        }

        bool IEquipmentActor.selectable
        {
            get
            {
                return selectable;
            }
        }

        int IEquipmentActor.showAtLevel
        {
            get
            {
                return showAtLevel;
            }
        }

        SkinnedMeshRenderer IEquipmentActor.skinnedMeshRenderer
        {
            get
            {
                if (!alwaysAnimated && GetComponent<Animator>() != null)
                {
                    var childSMR = transform.GetComponentInChildren<SkinnedMeshRenderer>(true);
                    if (childSMR != null && childSMR.sharedMesh != null)
                    {
                        return childSMR;
                    }
                }
                return null;
            }
        }

#if UNITY_EDITOR
        bool IEquipmentActor.editorSelected
        {
            get
            {
                return Selection.Contains(gameObject);
            }
        }

        Bounds IEquipmentActor.visibleBounds
        {
            get
            {
                return (currentMeshRenderer != null) ? currentMeshRenderer.bounds : default(Bounds);
            }
        }
#endif // #if UNITY_EDITOR

        #region Protected properties.
        protected new IEquipmentActorSharedDataProvider sharedDataProvider
        {
            get
            {
                return (IEquipmentActorSharedDataProvider)(base.sharedDataProvider);
            }
        }
        #endregion

        #region Public (Inspector) data
        [HideInInspector]
        public int estateType;

        //------------------------------------------------------------------------
        [Header("Sizes and orientation")]

        public RoomCoordinates size;
        [Tooltip("Additional shift from pivot (X and Z are in room cell scale!, Y is in native scale).")]
        public Vector3 shift;
        [Tooltip("This shift is applied for carpets and selection markers.")]
        public Vector3 carpetAndMarkersShift;
        [Tooltip("Additional rotation (in degrees) that is applied to object.")]
        public float additionalRotation;

        //------------------------------------------------------------------------
        [Header("Occupation settings.")]

        [Tooltip("Equipment uses floor space specified bt size (default). Turn off for pure wall-mounted equipment. It's size is still used for placement and must be valid but it doesn't gets 'occupied'.")]
        public bool occupiesFloorSpace = true;
        [Tooltip("Width that it occupies on floor (in room cells). Optional, set to 0 if it doesn't use wall space.")]
        public bool wallMounted;
        [Tooltip("Uses additional 1-cell wall margin disallowing placement of other wall objects too close.")]
        public bool hasWallMargin;

        //------------------------------------------------------------------------
        [Header("Prefabs of child objects.")]

        public GameObject carpetPrefab;
        public GameObject selectionPrefab;

        //------------------------------------------------------------------------
        [Header("Gameplay settings.")]

        public bool alwaysAnimated = false;
        public bool editable = true;
        public bool selectable = true;
        public bool initiallyLocked = false;
        public int showAtLevel = 0;

        //------------------------------------------------------------------------
        [Header("Animation transition settings.")]

        public float exerciseMovementTransitionTime;
        public float exerciseRotationTransitionTime;
        #endregion

        #region Private/protected data
        protected EquipmentPlacement placement;
        protected EquipmentActorDragEvent dragEvent;
        protected NavMeshObstacle[] obstacles;
        protected Transform[] approachingPoints;
        protected Transform[] leavingPoints;
        protected Transform[] workingPoints;
        protected IEquipmentInfo equipmentInfo;
        protected EquipmentPlacement pendingPlacement;
        protected PlacementResult placementState;
        protected GameObject carpet;
        protected GameObject selectionMarker;
        protected Animator animator;
        protected IEnumerator switchToImpostorJob;
        protected int selectedEquipmentAnimation;
        protected float animationSpeed;
        protected Renderer mainMeshRenderer;
        protected Renderer impostorMeshRenderer;
        protected Renderer currentMeshRenderer;
        protected EquipmentActorSettings actorSettings;
        protected MaterialsReplacement materialsReplacement;
        protected GameObject deliveryImpostorInstance;
        protected GameObject repairOrLockedMarkerInstance;
        protected bool editingThisEquipment;
        protected int originalLayer;
        protected MaterialPropertyBlock shadowNormalMPB;
        protected MaterialPropertyBlock shadowUsingMPB;
        protected Transform workingCharacterTransform;
        protected int workingCharacterExerciseTier;
        protected NavigationPoint? lastClosestApproachingPoint;
        #endregion

        new void Awake()
        {
            base.Awake();

            if (shadowRenderer != null)
            {
                shadowNormalMPB = new MaterialPropertyBlock();
                shadowRenderer.GetPropertyBlock(shadowNormalMPB);
                shadowNormalMPB.SetFloat("ColorSelector", 0);
                shadowUsingMPB = new MaterialPropertyBlock();
                shadowRenderer.GetPropertyBlock(shadowUsingMPB);
                shadowUsingMPB.SetFloat("ColorSelector", 1);

                shadowRenderer.SetPropertyBlock(shadowNormalMPB);
            }

            if (roomConductor != null)
            {
                roomConductor.coordsMapper.Update(false);
                placement = CalculateCurrentPlacement();
                //Debug.LogFormat(">>> Awake PLACEMENT: ({0}) = {1}", gameObject.name, placement);
            }

            //Debug.LogFormat("IEquipmentActor.Awake(): {0}", GetInstanceID());

            animator = gameObject.GetComponent<Animator>();
            mainMeshRenderer = transform.GetComponentInChildren<SkinnedMeshRenderer>(true);
            if (mainMeshRenderer == null)
            {
                mainMeshRenderer = transform.GetComponentInChildren<MeshRenderer>(true);
            }
            Assert.IsNotNull(mainMeshRenderer, "Equipment Actor object or its childs should contain at least one SkinnedMeshRender or MeshRenderer component.");
            currentMeshRenderer = mainMeshRenderer;

            materialsReplacement = gameObject.GetComponent<MaterialsReplacement>();
            originalLayer = gameObject.layer;

            approachingPoints = transform.GetChildsWithNamePrefix(approachPointPrefix).ToArray();
            leavingPoints = transform.GetChildsWithNamePrefix(leavingPointPrefix).ToArray();
            workingPoints = transform.GetChildsWithNamePrefix(workingPointPrefix).OrderBy(t => t.name).ToArray(); // Must be sorted by name: "working_point1", "working_point2" etc.
        }

        public override void Destroy()
        {
            EndEditMode(true);

            if (dragEvent != null)
                dragEvent.RemoveAllListeners();

            if (roomConductor != null && !editingThisEquipment)
            {
                roomConductor.RemoveEquipmentFromOccupationMap(ref placementState);
                roomConductor = null;
            }

            base.Destroy();
        }

        void OnDestroy()
        {
            if (roomConductor != null && !editingThisEquipment)
            {
                roomConductor.RemoveEquipmentFromOccupationMap(ref placementState);
                roomConductor = null;
            }

            CleanupCarpetAndSelection();
            carpetPrefab = null;
            selectionPrefab = null;
        }

#if UNITY_EDITOR
        void OnTransformParentChanged()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                CleanupCarpetAndSelection();

                if (roomConductor != null)
                {
                    roomConductor.RemoveEquipmentFromOccupationMap(ref placementState);
                    roomConductor = null;
                }
            }
            else
            {
                if (roomConductor != null)
                {
                    placement = CalculateCurrentPlacement();
                }
            }
        }

        void OnValidate()
        {
            additionalRotation = Math.Max(0.0f, Math.Min(360.0f, additionalRotation));
        }

        private void OnDrawGizmos()
        {
            if (Application.isPlaying && selectionMarker != null && selectionMarker.activeSelf && roomConductor != null)
            {
                //Vector3 labelPosition = roomConductor.roomObject.transform.position + roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(placement.position);
                //labelPosition.y = worldBounds.size.y;
                Handles.Label(transform.position, string.Format("{0}: HP: {1}", id, equipmentInfo.health), EditorGizmoHelper.instance.GetOverheadLabelStyle());
            }
        }
#endif // #if UNITY_EDITOR

        void IEquipmentActor.Init
            ( IEquipmentActorSharedDataProvider sharedDataProvider
            , int id
            , IEquipmentInfo equipmentInfo
            , RoomConductor roomConductor
            , DisplayMode displayMode
            , GameObject staticMeshImpostorPrefab
            , GameObject deliveryImpostorPrefab
            , GameObject repairOrLockedMarkerPrefab
            , EquipmentActorSettings actorSettings)
        {
            //Debug.LogFormat("IEquipmentActor.Init(): {0}", GetInstanceID());

            base.Init(sharedDataProvider, id, roomConductor, displayMode, false);

            Assert.IsNotNull(equipmentInfo);

            this.equipmentInfo = equipmentInfo;
            this.estateType = equipmentInfo.estateType.id;
            this.actorSettings = actorSettings;
            this.placement = equipmentInfo.placement;

            selectedEquipmentAnimation = -1;
            animationSpeed = 1.0f;
            editingThisEquipment = false;

            UpdateLocalTransform (equipmentInfo.placement);

            var obstaclesList = GetComponentsInChildren<NavMeshObstacle>();
            var thisObstacle = GetComponent<NavMeshObstacle>();

            if (obstaclesList != null && thisObstacle != null)
            {
                obstacles = new NavMeshObstacle[(obstaclesList != null ? obstaclesList.Length : 0) + (thisObstacle != null ? 1 : 0) ];
                int idx = 0;
                if (thisObstacle != null)
                {
                    obstacles[0] = thisObstacle;
                    idx++;
                }
                if (obstaclesList != null)
                {
                    Array.Copy(obstaclesList, 0, obstacles, idx, obstaclesList.Length);
                }
            }


            if (animator != null)
            {
                animator.Rebind();

                if (!alwaysAnimated)
                {
                    animator.enabled = false;

                    if (staticMeshImpostorPrefab != null)
                    {
                        var staticMeshImpostor = Instantiate(staticMeshImpostorPrefab, transform, false);
                        impostorMeshRenderer = staticMeshImpostor.GetComponentInChildren<MeshRenderer>(true);
                        if (impostorMeshRenderer != null)
                        {
                            staticMeshImpostor.SetActive(true);
                            impostorMeshRenderer.enabled = true;
                            mainMeshRenderer.enabled = false;
                            currentMeshRenderer = impostorMeshRenderer;
                        }
                        else
                        {
                            Destroy(staticMeshImpostor);
                        }
                    }
                }
            }

            if (repairOrLockedMarkerPrefab)
            {
                InstantiateRepairMarker(repairOrLockedMarkerPrefab);
            }

            if (deliveryImpostorPrefab != null && (equipmentInfo.isInDelivery || equipmentInfo.isLocked))
            {
                InstantiateDeliveryImpostor(deliveryImpostorPrefab);
            }

            if (equipmentInfo.isLocked)
            {
                // HideMainModel();
                SetLockedEquipmentMaterials(true);
                ShowRepairOrUnlockMarker(true);
            }
            else if (equipmentInfo.isInDelivery)
            {
                ShowDeliveryImpostor(true);
                HideMainModel();
                ShowRepairOrUnlockMarker(false);
            }

            InstantiateCarpetAndSelection();
            UpdateVisibility();
            UpdateAvailability();
        }

        public EquipmentPlacement CalculateCurrentPlacement ()
        {
            if (roomConductor != null)
            {
                var heading = Mathf.RoundToInt (rotationEuler.y / 90.0f) % 4;
                return new EquipmentPlacement (roomConductor.coordsMapper.AlignPositionToRoomCoordinatesRounded(localPosition, size.GetOrientedSize(heading), EquipmentPlacement.GetShiftRotated(shift, heading)), heading);
            }
            else
            {
                return default(EquipmentPlacement);
            }
        }

        void IEquipmentActor.SetPlacement(EquipmentPlacement newPlacement)
        {
            // Note. Can be called in edit mode when not playing game.

            newPlacement.heading = newPlacement.heading % 4;
            if (roomConductor != null && size.isValidSize)
            {
                UpdateLocalTransform (newPlacement);

                if (editingThisEquipment)
                {
                    //if (newPlacement != pendingPlacement)
                    if (editable)
                    {
                        var prevHeading = pendingPlacement.heading;
                        pendingPlacement = newPlacement;

                        var prevPlacementState = placementState;
                        placementState = roomConductor.CheckOccupationMap(this);
                
                        if (prevPlacementState != placementState)
                        {
                            ToggleSelectionMarker
                                ( true
                                , placementState.isValid ? actorSettings.activeSelection : actorSettings.wrongPlacement
                                , actorSettings.selectionActiveLayer
                                , false);

                            SetWrongPlacementMaterials (!placementState.isValid);
                        }

                        var movementMarker = sharedDataProvider.movementMarker;
                        if (movementMarker != null)
                        {
                            if (prevHeading != newPlacement.heading)
                                movementMarker.UpdateSize(size2D, newPlacement.heading, additionalRotation);
                            movementMarker.UpdateState(newPlacement, size, roomConductor.roomSize, wallMounted);
                        }
                    }
                }
                else
                {
                    //if (newPlacement != placement)
                    {
                        roomConductor.RemoveEquipmentFromOccupationMap (ref placementState);

                        placement = newPlacement;

                        placementState = roomConductor.AddEquipmentToOccupationMap (this);
                        if (placementState.isValid && onEquipmentActorPlacementUpdated != null)
                        {
                            onEquipmentActorPlacementUpdated(id, newPlacement);
                        }

                        SetWrongPlacementMaterials (!placementState.isValid);
                    }
                }
            }
            else
            {
                if (newPlacement != placement)
                {
                    placement = newPlacement;
                    transform.localRotation = Quaternion.Euler(0, newPlacement.heading * 90.0f + additionalRotation, 0);
                }
            }
        }

        void IEquipmentActor.ResetPlacement ()
        {
            if (roomConductor != null && size.isValidSize)
            {
                if (!editingThisEquipment)
                {
                    roomConductor.RemoveEquipmentFromOccupationMap (ref placementState);
                }
            }

            if (editable)
            {
                pendingPlacement = placement;
                UpdateLocalTransform(placement);

                if (editingThisEquipment)
                {
                    placementState = roomConductor.CheckOccupationMap(this);

                    SetWrongPlacementMaterials (!placementState.isValid);
                }
            }
        }

#if UNITY_EDITOR
        void IEquipmentActor.SetRoomConductor (RoomConductor roomConductor)
        {
            this.roomConductor = roomConductor;
        }

#endif // #if UNITY_EDITOR

        public override void SetSelection (bool selected)
        {
            ToggleSelectionMarker
                ( selected
                , actorSettings.activeSelection
                , selected ? actorSettings.selectionActiveLayer : actorSettings.selectionNormalLayer
                , selected);

            if (currentOverheadUIIcon != null)
                currentOverheadUIIcon.frontIcon = selected;
        }

        public override void SetDisplayMode (DisplayMode displayMode)
        {
            base.SetDisplayMode(displayMode);
            UpdateVisibility();
        }

        public void UpdateAvailability ()
        {
            if (equipmentInfo != null && equipmentInfo.isStatic && showAtLevel > 0)
            {
                gameObject.SetActive(equipmentInfo.meetsLevelRequirement);
            }
        }

        void IEquipmentActor.SetEditMode
            ( bool editMode
            , bool editThisObject )
        {
            if (editMode)
            {
                if (editThisObject)
                {
                    StartEditMode();
                }
                else
                {
                    EndEditMode();
                }

                ToggleSelectionMarker
                    ( true
                    , editThisObject ? actorSettings.activeSelection : actorSettings.inactiveSelection
                    , editThisObject ? actorSettings.selectionActiveLayer : actorSettings.selectionNormalLayer
                    , editThisObject);

                if (carpet != null)
                {
                    carpet.SetSpriteRenderersSortingLayer(editThisObject ? actorSettings.carpetsActiveLayer : actorSettings.carpetsNormalLayer);
                }
            }
            else
            {
                if (editingThisEquipment)
                    EndEditMode();

                ToggleSelectionMarker
                    ( false
                    , actorSettings.activeSelection
                    , actorSettings.selectionNormalLayer
                    , false);

                if (carpet != null)
                {
                    carpet.SetSpriteRenderersSortingLayer(actorSettings.carpetsNormalLayer);
                }
            }
        }

        NavigationPoint IEquipmentActor.StartUsingEquipment (Transform characterTransform, int exerciseTier)
        {
            Assert.IsNotNull(characterTransform);

            workingCharacterTransform = characterTransform;
            workingCharacterExerciseTier = exerciseTier;

            if (animator != null)
            {
                if (switchToImpostorJob != null)
                {
                    StopCoroutine(switchToImpostorJob);
                    switchToImpostorJob = null;
                }

                bool enabled = (selectedEquipmentAnimation >= 0);
                animator.enabled = true;
                animator.SetInteger(sharedDataProvider.animationSelectorParameterHash, selectedEquipmentAnimation);
                if (!alwaysAnimated && !enabled)
                    animator.enabled = false;

                animator.speed = animationSpeed;

                if (impostorMeshRenderer != null)
                {
                    mainMeshRenderer.enabled = enabled;
                    impostorMeshRenderer.enabled = !enabled;
                    currentMeshRenderer = enabled ? mainMeshRenderer : impostorMeshRenderer;
                }
            }

            if (shadowRenderer != null)
            {
                shadowRenderer.SetPropertyBlock(shadowUsingMPB);
            }

            return GetWorkingPoint(exerciseTier);
        }

        void IEquipmentActor.StopUsingEquipment()
        {
            Assert.IsNotNull(workingCharacterTransform);

            workingCharacterTransform = null;
            lastClosestApproachingPoint = null; // Not needed anymore;

            if (animator != null)
            {
                animator.SetInteger(sharedDataProvider.animationSelectorParameterHash, -1);
                animator.speed = 1.0f;

                if (impostorMeshRenderer != null)
                {
                    if (switchToImpostorJob != null)
                    {
                        StopCoroutine(switchToImpostorJob);
                        switchToImpostorJob = null;
                    }

                    switchToImpostorJob = SwitchToImpostorJob();
                    StartCoroutine(switchToImpostorJob);
                }
            }

            if (shadowRenderer != null)
            {
                shadowRenderer.SetPropertyBlock(shadowNormalMPB);
            }
        }

        void IEquipmentActor.SetRepairMode(bool repair)
        {
            ShowRepairOrUnlockMarker(repair);
        }

        void IEquipmentActor.StartedUnlocking()
        {
            // ShowMainModel();
            SetLockedEquipmentMaterials(false);
            ShowRepairOrUnlockMarker(false);
        }

        NavigationPoint IEquipmentActor.GetLeavingPoint(Vector3 fromPosition, int areaMask)
        {
            Assert.IsNotNull(roomConductor);
            var sizeConverted = roomConductor.coordsMapper.ConvertPosition2DFromRoomCooordinates(size);
            float maxSearchDistance = Mathf.Sqrt(sizeConverted.x * sizeConverted.x + sizeConverted.y * sizeConverted.y);
            Vector3 candidateHitPosition;

            // Try leaving points, if exists and not obstructed.
            if (leavingPoints != null && leavingPoints.Length > 0)
            {
                var pointCandidate = GetClosestTransformPoint
                    ( leavingPoints
                    , fromPosition
                    , maxSearchDistance
                    , areaMask
                    , out candidateHitPosition);
                if (pointCandidate != null)
                {
                    return new NavigationPoint
                        ( candidateHitPosition
                        , pointCandidate.rotation
                        , exerciseMovementTransitionTime
                        , exerciseRotationTransitionTime);
                }
            }

            // Next try approachingPoints points, if exists and not obstructed.
            if (approachingPoints != null && approachingPoints.Length > 0)
            {
                var pointCandidate = GetClosestTransformPoint
                    ( approachingPoints
                    , fromPosition
                    , maxSearchDistance
                    , areaMask
                    , out candidateHitPosition);
                if (pointCandidate != null)
                {
                    return new NavigationPoint
                        ( candidateHitPosition
                        , Quaternion.LookRotation(-pointCandidate.forward)
                        , exerciseMovementTransitionTime
                        , exerciseRotationTransitionTime);
                }
            }

            // Use transform but look for closest free position nearby.
            var resultPosition = transform.position;

            NavMeshHit hit;
            if (NavMesh.SamplePosition(transform.position, out hit, maxSearchDistance, areaMask))
            {
                resultPosition = hit.position;
            }

            return new NavigationPoint
                ( resultPosition
                , transform.rotation
                , exerciseMovementTransitionTime
                , exerciseRotationTransitionTime);
        }

        NavigationPoint IEquipmentActor.GetClosestApproachingPoint(Vector3 fromPosition, int areaMask, int exerciseTier)
        {
            Assert.IsNotNull(roomConductor);
            var sizeConverted = roomConductor.coordsMapper.ConvertPosition2DFromRoomCooordinates(size);
            float maxSearchDistance = Mathf.Sqrt(sizeConverted.x * sizeConverted.x + sizeConverted.y * sizeConverted.y);

            NavMeshHit hit;
            if (exerciseTier > 0 && workingPoints != null && workingPoints.Length > 1)
            {
                var result = GetWorkingPoint(exerciseTier);

                if (NavMesh.SamplePosition(result.position, out hit, maxSearchDistance, areaMask))
                {
                    result.position = hit.position;
                }
                lastClosestApproachingPoint = result;
                return result;
            }

            Vector3 candidateHitPosition;
            if (approachingPoints != null && approachingPoints.Length > 0)
            {
                var pointCandidate = GetClosestTransformPoint
                    ( approachingPoints.OrderBy(p => Vector3.Distance(fromPosition, p.position))
                    , fromPosition
                    , maxSearchDistance
                    , areaMask
                    , out candidateHitPosition);
                if (pointCandidate != null)
                {
                    lastClosestApproachingPoint = new NavigationPoint
                        ( candidateHitPosition
                        , pointCandidate.rotation
                        , exerciseMovementTransitionTime
                        , exerciseRotationTransitionTime);
                    return lastClosestApproachingPoint.Value;
                }
            }

            // Use closest to fromPosition unobstructed position on perimeter.
            var resultPosition = GetClosestPerimeterPosition(fromPosition);
            if (NavMesh.SamplePosition(resultPosition, out hit, maxSearchDistance, areaMask))
            {
                resultPosition = hit.position;
            }

            var resultRotation = Quaternion.LookRotation((resultPosition - transform.position).normalized, Vector3.up);

            lastClosestApproachingPoint = new NavigationPoint(resultPosition, resultRotation, exerciseMovementTransitionTime, exerciseRotationTransitionTime);
            return lastClosestApproachingPoint.Value;
        }

        NavigationPoint IEquipmentActor.GetClosestNavigationPoint(Vector3 fromPosition, int areaMask, string namePrefix)
        {
            Assert.IsNotNull(roomConductor);
            var sizeConverted = roomConductor.coordsMapper.ConvertPosition2DFromRoomCooordinates(size);
            float maxSearchDistance = Mathf.Sqrt(sizeConverted.x * sizeConverted.x + sizeConverted.y * sizeConverted.y);

            NavMeshHit hit;

            Vector3 candidateHitPosition;

            var points = transform.GetChildsWithNamePrefix(namePrefix);
            if (points != null)
            {
                var pointCandidate = GetClosestTransformPoint
                    ( points.OrderBy(p => Vector3.Distance(fromPosition, p.position))
                    , fromPosition
                    , maxSearchDistance
                    , areaMask
                    , out candidateHitPosition);
                if (pointCandidate != null)
                {
                    return new NavigationPoint
                        (candidateHitPosition
                        , pointCandidate.rotation
                        , exerciseMovementTransitionTime
                        , exerciseRotationTransitionTime);
                }
            }

            // Use approaching points as a fallback.
            if (approachingPoints != null && approachingPoints.Length > 0)
            {
                var pointCandidate = GetClosestTransformPoint
                    (approachingPoints.OrderBy(p => Vector3.Distance(fromPosition, p.position))
                    , fromPosition
                    , maxSearchDistance
                    , areaMask
                    , out candidateHitPosition);
                if (pointCandidate != null)
                {
                    lastClosestApproachingPoint = new NavigationPoint
                        (candidateHitPosition
                        , pointCandidate.rotation
                        , exerciseMovementTransitionTime
                        , exerciseRotationTransitionTime);
                    return lastClosestApproachingPoint.Value;
                }
            }

            // Use closest to fromPosition unobstructed position on perimeter.
            var resultPosition = GetClosestPerimeterPosition(fromPosition);
            if (NavMesh.SamplePosition(resultPosition, out hit, maxSearchDistance, areaMask))
            {
                resultPosition = hit.position;
            }

            var resultRotation = Quaternion.LookRotation((resultPosition - transform.position).normalized, Vector3.up);

            return new NavigationPoint(resultPosition, resultRotation, exerciseMovementTransitionTime, exerciseRotationTransitionTime);
        }

        NavigationPoint? IEquipmentActor.GetLastClosestApproachingPoint()
        {
            return lastClosestApproachingPoint;
        }

        #region IEquipmentActorEvents interface.
        void IEquipmentActorEvents.OnAssemblyStarted()
        {
            ShowDeliveryImpostor(true);
            ShowRepairOrUnlockMarker(false);
            HideMainModel();

            if (deliveryImpostorInstance != null)
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(deliveryImpostorInstance, null, (target, eventData) => target.OnAssemblyStarted());
            } 
        }

        void IEquipmentActorEvents.OnAssemblyComplete()
        {
            ShowDeliveryImpostor(true);
            ShowRepairOrUnlockMarker(false);
            HideMainModel();

            if (deliveryImpostorInstance != null)
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(deliveryImpostorInstance, null, (target, eventData) => target.OnAssemblyComplete());
            } 
        }

        void IEquipmentActorEvents.OnAssemblyCompleteConfirmed()
        {
            CleanupDeliveryImpostor();
            ShowRepairOrUnlockMarker(false);
            ShowMainModel();

            if (deliveryImpostorInstance != null)
            {
                ExecuteEvents.Execute<IEquipmentActorEvents>(deliveryImpostorInstance, null, (target, eventData) => target.OnAssemblyCompleteConfirmed());
            } 
        }

        void IEquipmentActorEvents.OnUpgraded(UpgradeEventData eventData)
        {
            if (!LeanTween.isTweening(gameObject))
            {
                LeanTween.scale(gameObject, actorSettings.upgradePingScale, actorSettings.upgradePingTime)
                    .setEase(actorSettings.upgradePingEaseType)
                    .setLoopPingPong(1);
                LeanTween.moveLocal(gameObject, gameObject.transform.localPosition + actorSettings.upgradePingJump, actorSettings.upgradePingJumpTime)
                    .setEase(actorSettings.upgradePingJumpEaseType)
                    .setLoopPingPong(1);
            }
        }
        #endregion

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            eventData.useDragThreshold = useDragThreshold;
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (dragEvent != null)
            {
                dragEvent.Invoke(id, eventData);
            }
        }

        IEnumerator SwitchToImpostorJob()
        {
            yield return new WaitForSeconds(impostorCooldownTime);

            Assert.IsNotNull(animator);
            Assert.IsNotNull(impostorMeshRenderer);

            animator.enabled = false;
            impostorMeshRenderer.enabled = mainMeshRenderer.enabled;
            mainMeshRenderer.enabled = false;
            currentMeshRenderer = impostorMeshRenderer;

            Assert.IsNotNull(switchToImpostorJob);
            switchToImpostorJob = null;
        }

        NavigationPoint GetWorkingPoint(int exerciseTier)
        {
            if (workingPoints != null && workingPoints.Length > 0)
            {
                var wp = workingPoints[Mathf.Clamp(exerciseTier - 1, 0, workingPoints.Length - 1)];

                return new NavigationPoint(wp.transform.position, wp.transform.rotation, exerciseMovementTransitionTime, exerciseRotationTransitionTime);
            }
            
            return new NavigationPoint(transform.position, transform.rotation, exerciseMovementTransitionTime, exerciseRotationTransitionTime);
        }

        Transform GetClosestTransformPoint
            ( IEnumerable<Transform> points
            , Vector3 fromPosition
            , float maxSearchDistance
            , int areaMask
            , out Vector3 candidateHitPosition)
        {
            candidateHitPosition = default(Vector3);
            float currentDistance = float.MaxValue;
            Transform pointCandidate = null;

            if (points != null)
            {
                foreach (var point in points)
                {
                    NavMeshHit hit;
                    if (NavMesh.SamplePosition(point.position, out hit, maxSearchDistance, areaMask))
                    {
                        if (hit.distance < currentDistance)
                        {
                            pointCandidate = point;
                            candidateHitPosition = hit.position;

                            if (hit.distance <= roomConductor.coordsMapper.cellSize)
                                break; // Quick search - first position within cell size range is OK.
                        }
                    }
                }
            }

            return pointCandidate;
        }

        Vector3 GetClosestPerimeterPosition(Vector3 fromPosition)
        {
            var resultPosition = transform.position;

            var positionLocal = roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(placement.position);
            var positionMaxLocal = roomConductor.coordsMapper.ConvertPositionFromRoomCooordinates(placement.position + orientedSize);
            var min = roomConductor.roomObject.transform.position + positionLocal;
            var max = roomConductor.roomObject.transform.position + positionMaxLocal;

            var maxX = min.x < max.x ? max.x : min.x;
            var minX = min.x < max.x ? min.x : max.x;
            var maxZ = min.z < max.z ? max.z : min.z;
            var minZ = min.z < max.z ? min.z : max.z;

            resultPosition.x = Math.Max(minX, Math.Min(fromPosition.x, maxX));
            resultPosition.z = Math.Max(minZ, Math.Min(fromPosition.z, maxZ));

            return resultPosition;
        }

        void StartEditMode()
        {
            if (roomConductor != null)
            {
                if (!editingThisEquipment)
                {
                    if (editable)
                    {
                        roomConductor.RemoveEquipmentFromOccupationMap (ref placementState);
                        SetObstaclesState(false);

                        var movementMarker = sharedDataProvider.movementMarker;
                        var size2D_ = size2D;
                        if (movementMarker != null && size2D_ != Vector2.zero)
                        {
                            movementMarker.transform.SetParent(transform, false);
                            movementMarker.UpdateSize(size2D_, placement.heading, additionalRotation);
                            movementMarker.UpdateState(placement, size, roomConductor.roomSize, wallMounted);
                            movementMarker.gameObject.SetActive(true);
                        }

                        placementState = roomConductor.CheckOccupationMap (this);

                        SetWrongPlacementMaterials (!placementState.isValid);

                        pendingPlacement = placement;
                    }

                    editingThisEquipment = true;
                }
            }
        }

        void EndEditMode(bool destroying = false)
        {
            if (roomConductor != null)
            {
                if (editingThisEquipment)
                {
                    if (!destroying && editable)
                    {
                        var ps = roomConductor.CheckOccupationMap(this);

                        if (ps.isValid)
                        {
                            placement = pendingPlacement;
                        }
                        else
                        {
                            pendingPlacement = placement;
                        }
                        UpdateLocalTransform(placement);
                        placementState = roomConductor.AddEquipmentToOccupationMap (this);

                        SetWrongPlacementMaterials (!placementState.isValid);
                        SetObstaclesState(true);
                    }

                    if (editable)
                    {
                        var movementMarker = sharedDataProvider.movementMarker;
                        if (movementMarker != null)
                        {
                            movementMarker.transform.SetParent(null, false);
                            movementMarker.gameObject.SetActive(false);
                        }
                    }

                    editingThisEquipment = false;

                    if (!destroying)
                    {
                        if (placementState.isValid && onEquipmentActorPlacementUpdated != null)
                        {
                            onEquipmentActorPlacementUpdated(id, placement);
                        }
                    }
                }
            }
        }

        void CleanupCarpetAndSelection()
        {
            if (carpet != null)
            {
                var carpetTmp = carpet;
                carpet = null;
                carpetTmp.transform.SetParent(null);

                DestroyImmediate(carpetTmp);
            }

            if (selectionMarker != null)
            {
                var selectionMarkerTmp = selectionMarker;
                selectionMarker = null;
                selectionMarkerTmp.transform.SetParent(null);
                DestroyImmediate(selectionMarkerTmp);
            }
        }

        void InstantiateCarpetAndSelection()
        {
            if (carpet == null && carpetPrefab != null)
            {
                carpet = Instantiate(carpetPrefab);
                carpet.transform.SetParent(transform, false);

                carpet.SetSpriteRenderersSortingLayer(actorSettings.carpetsNormalLayer);
            }

            if (selectionMarker == null && selectionPrefab != null)
            {
                selectionMarker = Instantiate(selectionPrefab);
                selectionMarker.SetActive(Application.isEditor && !Application.isPlaying);
                selectionMarker.transform.SetParent(transform, false);
            }

            UpdateCarpetAndSelectionMarkerTransform();
        }

        void ShowRepairOrUnlockMarker(bool show)
        {
            if (repairOrLockedMarkerInstance != null)
            {
                repairOrLockedMarkerInstance.SetActive(show);
            }
        }

        void ShowDeliveryImpostor(bool show)
        {
            if (deliveryImpostorInstance != null)
            {
                deliveryImpostorInstance.SetActive(show);
            }
        }

        void OnSecondaryPointerClick(PointerEventData eventData)
        {
            if (pointerClickEvent != null)
            {
                pointerClickEvent.Invoke(id, eventData);
            }
        }

        void OnSecondaryPointerDown(PointerEventData eventData)
        {
            if (pointerDownEvent != null)
            {
                pointerDownEvent.Invoke(id, eventData);
            }
        }

        void OnSecondaryPointerUp(PointerEventData eventData)
        {
            if (pointerUpEvent != null)
            {
                pointerUpEvent.Invoke(id, eventData);
            }
        }

        void OnSecondaryDrag(PointerEventData eventData)
        {
            if (dragEvent != null)
            {
                dragEvent.Invoke(id, eventData);
            }
        }

        void ToggleSelectionMarker (bool state, Color color, int sortingLayer, bool animated = false)
        {
            if (selectionMarker != null)
            {
                if (state)
                {
                    var selectionMarkerRenderers = selectionMarker.GetComponentsInChildren<SpriteRenderer>();
                    if (selectionMarkerRenderers != null)
                    {
                        foreach (var renderer in selectionMarkerRenderers)
                        {
                            renderer.color = color;
                            renderer.sortingLayerID = sortingLayer;
                        }
                    }

                    selectionMarker.SetActive(true);
                    if (animated)
                    {
                        if (!LeanTween.isTweening(selectionMarker))
                        {
                            LeanTween.scale(selectionMarker, actorSettings.selectionMarkerScale, actorSettings.selectionMarkerScaleTime)
                                .setEase(actorSettings.selectionMarkerEaseType)
                                .setLoopPingPong();
                        }
                    }
                }
                else
                {
                    LeanTween.cancel(selectionMarker);
                    selectionMarker.transform.localScale = Vector3.one;
                    selectionMarker.SetActive(false);
                }
            }

            if (state)
            {
                if (animated)
                {
                    if (!LeanTween.isTweening(gameObject))
                    {
                        LeanTween.scale(gameObject, actorSettings.selectionPingScale, actorSettings.selectionPingTime)
                            .setEase(actorSettings.selectionPingEaseType)
                            .setLoopPingPong(1);
                    }
                }
            }
            else
            {
                if (LeanTween.isTweening(gameObject))
                {
                    LeanTween.cancel(gameObject);
                    transform.localScale = Vector3.one;
                }
            }
        }

        void SetWrongPlacementMaterials (bool set)
        {
            if (materialsReplacement != null && actorSettings != null && actorSettings.wrongPlacementMaterialPolicyTag != string.Empty)
            {
                if (set)
                {
                    materialsReplacement.ReplaceMaterials(actorSettings.wrongPlacementMaterialPolicyTag, actorSettings.defaultMaterialReplacementPolicy);
                }
                else
                {
                    materialsReplacement.ResetBackMaterials(actorSettings.wrongPlacementMaterialPolicyTag);
                }
            }
        }

        void SetLockedEquipmentMaterials (bool set)
        {
            if (materialsReplacement != null && actorSettings.lockedEquipmentMaterialPolicyTag != string.Empty)
            {
                if (set)
                {
                    materialsReplacement.ReplaceMaterials(actorSettings.lockedEquipmentMaterialPolicyTag, actorSettings.defaultMaterialReplacementPolicy);
                }
                else
                {
                    materialsReplacement.ResetBackMaterials(actorSettings.lockedEquipmentMaterialPolicyTag);
                }
            }
        }

        void SetObstaclesState(bool enabled)
        {
            if (obstacles != null)
            {
                foreach (var obstacle in obstacles)
                {
                    obstacle.enabled = enabled;
                }
            }
        }

        private void UpdateLocalTransform (EquipmentPlacement placement)
        {
            if (roomConductor != null)
            {
                var rotationDegrees = placement.heading * 90.0f;
                var rotationRaw = Quaternion.Euler(0, rotationDegrees, 0);
                transform.localPosition = roomConductor.coordsMapper.AlignPositionFromRoomCooordinates
                    (placement.position, size.GetOrientedSize(placement.heading), rotationRaw * shift);
                transform.localRotation = Quaternion.Euler(0, rotationDegrees + additionalRotation, 0);

                if (workingCharacterTransform != null)
                {
                    var workingPoint = GetWorkingPoint(workingCharacterExerciseTier);

                    workingCharacterTransform.position = workingPoint.position;
                    workingCharacterTransform.rotation = workingPoint.rotation;
                }

                UpdateCarpetAndSelectionMarkerTransform();
            }
        }

        private void UpdateCarpetAndSelectionMarkerTransform()
        {
            if (carpet != null || selectionMarker != null)
            {
                var rotationCarpetAndMarker = Quaternion.Euler(0, -additionalRotation, 0);

                var pos = Vector3.zero;
                if (shift != Vector3.zero)
                {
                    //var rotationDegrees = placement.heading * 90.0f;
                    //var rotationRaw = Quaternion.Euler(0, rotationDegrees, 0);
                    pos -= roomConductor.coordsMapper.ConvertPositionFromLogic (rotationCarpetAndMarker * shift);
                }

                if (carpetAndMarkersShift != Vector3.zero)
                {
                    pos += rotationCarpetAndMarker * carpetAndMarkersShift;
                }

                if (carpet != null)
                {
                    carpet.transform.localRotation = rotationCarpetAndMarker;
                    carpet.transform.localPosition = pos;
                }

                if (selectionMarker != null)
                {
                    selectionMarker.transform.localRotation = rotationCarpetAndMarker;
                    selectionMarker.transform.localPosition = pos;
                }
            }
        }

        private void InstantiateDeliveryImpostor(GameObject deliveryPrefab)
        {
            Assert.IsNotNull(deliveryPrefab);

            deliveryImpostorInstance = Instantiate(deliveryPrefab);

            deliveryImpostorInstance.transform.SetParent(transform, false);
            deliveryImpostorInstance.transform.localPosition = Vector3.zero;

            deliveryImpostorInstance.SetActive(false);
        }

        private void InstantiateRepairMarker(GameObject repairMarkerPrefab)
        {
            Assert.IsNotNull(repairMarkerPrefab);

            repairOrLockedMarkerInstance = Instantiate(repairMarkerPrefab);

            repairOrLockedMarkerInstance.transform.SetParent(transform, false);
            repairOrLockedMarkerInstance.transform.localPosition = Vector3.zero;
            repairOrLockedMarkerInstance.SetActive(false);
        }

        private void CleanupDeliveryImpostor()
        {
            if (deliveryImpostorInstance != null)
            {
                deliveryImpostorInstance.transform.SetParent(null);
                Destroy(deliveryImpostorInstance);
                deliveryImpostorInstance = null;
            }
        }

        private void HideMainModel()
        {
            currentMeshRenderer.enabled = false;
            mainMeshRenderer.enabled = false;

            if (shadowRenderer != null)
            {
                shadowRenderer.gameObject.SetActive(false);
            }
        }

        private void ShowMainModel()
        {
            currentMeshRenderer.enabled = true;

            if (shadowRenderer != null)
            {
                shadowRenderer.gameObject.SetActive(true);
            }
        }

        private void UpdateVisibility()
        {
            if (equipmentInfo != null && equipmentInfo.estateType.itemType != EstateType.Subtype.Door)
            {
                var layer = (displayMode == DisplayMode.Hidden) ? LayerMask.NameToLayer("Hidden") : originalLayer;
                var goOriginalLayer = gameObject.layer;
                gameObject.layer = layer;

                foreach (Transform child in transform)
                {
                    if (child.gameObject.layer == goOriginalLayer)  // Helps to filter out person children that are placed inside of us while performing exercise.
                    {
                        child.SetLayerRecursive(layer);
                    }
                }
            }
        }

#if UNITY_EDITOR
        public void ToggleCarpetsAndSelection()
        {
            if (transform.parent != null && roomConductor != null)
            {
                if (carpet != null || selectionMarker != null)
                {
                    CleanupCarpetAndSelection();
                }
                else
                {
                    InstantiateCarpetAndSelection();
                }
            }
        }
#endif // #if UNITY_EDITOR
    }
}