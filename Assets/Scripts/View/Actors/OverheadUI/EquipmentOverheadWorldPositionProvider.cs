﻿using UnityEngine;
using View.UI.OverheadUI;

namespace View.Actors.OverheadUI
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Overhead UI/Equipment Overhead World Position Provider")]
    public class EquipmentOverheadWorldPositionProvider : MonoBehaviour, IWorldPositionProvider
    {
        #region Public fields.
        public bool useBottomPoint;
        public Vector3 positionOffset = Vector3.zero;
        #endregion

        #region Private data.
        private Vector3 cachedPosition;
        private bool updateDisabled;
        private float cachedHeight;
        #endregion

        #region IWorldPositionProvider properties.
        bool IWorldPositionProvider.allowUpdate
        {
            get
            {
                return !updateDisabled;
            }

            set
            {
                if (!updateDisabled != value)
                {
                    updateDisabled = !value;
                    if (!value)
                    {
                        cachedPosition = CalculatePosition();
                    }
                }
            }
        }
        #endregion

        #region Unity API.
        void Start()
        {
            if (!useBottomPoint)
            {
                Bounds bounds;

                if (!GetBoundsFromMeshRenderers(gameObject, out bounds))
                {
                    for (var i = 0; i < transform.childCount; i++)
                    {
                        if (GetBoundsFromMeshRenderers(transform.GetChild(i).gameObject, out bounds))
                            break;
                    }
                }

                cachedHeight = bounds.size.y;
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
/*
        private void OnDrawGizmos()
        {
            var p = ((IWorldPositionProvider) this).GetWorldPosition();
            Debug.DrawLine(p + Vector3.left, p + Vector3.right);
            Debug.DrawLine(p + Vector3.forward, p + Vector3.back);
            Debug.DrawLine(p + Vector3.down, p + Vector3.up);
        }
*/
#endif
        #endregion

        #region IWorldPositionProvider interface.
        Vector3 IWorldPositionProvider.GetWorldPosition()
        {
            return updateDisabled ? cachedPosition : CalculatePosition();
        }
        #endregion

        #region Private functions.
        private Vector3 CalculatePosition()
        {
            var pos = transform.position + positionOffset;

            if (!useBottomPoint)
                pos.y += cachedHeight;

            return pos;
        }

        private bool GetBoundsFromMeshRenderers(GameObject obj, out Bounds bounds)
        {
            var meshRenderer = obj.GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                bounds = meshRenderer.bounds;
                return true;
            }

            var skinnedMeshRenderer = obj.GetComponent<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer != null)
            {
                bounds = skinnedMeshRenderer.bounds;
                return true;
            }

            bounds = default(Bounds);
            return false;
        }
        #endregion
    }
} 
