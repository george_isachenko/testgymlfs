﻿using UnityEngine;
using View.UI.OverheadUI;

namespace View.Actors.OverheadUI
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Overhead UI/Character Overhead World Position Provider")]
    public class CharacterOverheadWorldPositionProvider : MonoBehaviour, IWorldPositionProvider
    {
        #region Public (Inspector) fields.
        public bool fixedOffset;
        #endregion

        #region Private data.
        private bool updateDisabled;
        private Vector3 cachedPosition;
        private Collider cachedCollider;
        private float fixedOffsetValue;
        #endregion

        #region IWorldPositionProvider interface.
        bool IWorldPositionProvider.allowUpdate
        {
            get
            {
                return !updateDisabled;
            }

            set
            {
                if (!updateDisabled != value)
                {
                    updateDisabled = !value;
                    if (!value)
                    {
                        cachedPosition = CalculatePosition();
                    }
                }
            }
        }

        Vector3 IWorldPositionProvider.GetWorldPosition()
        {
            return updateDisabled ? cachedPosition : CalculatePosition();
        }
        #endregion

        #region Unity API.
        void Awake()
        {
            cachedCollider = gameObject.GetComponentInChildren<Collider>();
            if (fixedOffset)
            {
                fixedOffsetValue = (cachedCollider.bounds.max.y - transform.position.y);
            }
        }
        #endregion

        #region Private functions.
        private Vector3 CalculatePosition()
        {
            var pos = transform.position;

            if (cachedCollider != null)
            {
                pos.y = fixedOffset ? (pos.y + fixedOffsetValue) : cachedCollider.bounds.max.y;
            }

            return pos;
        }
        #endregion
    }
} 
