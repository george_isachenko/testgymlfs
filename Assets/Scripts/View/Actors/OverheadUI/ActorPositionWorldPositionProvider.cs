﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using View.UI.OverheadUI;

namespace View.Actors.OverheadUI
{
    public class ActorPositionWorldPositionProvider<IDType> : IWorldPositionProvider
    {
        #region Private data.
        private Vector3 cachedPosition;
        private Vector3 shift;
        private bool updateDisabled;
        private bool alwaysEnabled;
        private bool chainActorsProvider;
        private IActor<IDType> trackedActor;
        #endregion

        #region IWorldPositionProvider properties.
        bool IWorldPositionProvider.allowUpdate
        {
            get
            {
                return !updateDisabled;
            }

            set
            {
                updateDisabled = !value;
            }
        }

        bool IWorldPositionProvider.enabled
        {
            get
            {
                return (alwaysEnabled || trackedActor.displayMode == DisplayMode.ShowActorAndOverheadUI);
            }

            set
            {
                // Ignored.
            }
        }
        #endregion

        #region Constructors.
        public ActorPositionWorldPositionProvider (IActor<IDType> actor, bool chainActorsProvider = false, bool alwaysEnabled = false)
        {
            Assert.IsNotNull(actor);
            updateDisabled = false;
            trackedActor = actor;
            this.chainActorsProvider = chainActorsProvider;
            this.alwaysEnabled = alwaysEnabled;

            cachedPosition = CalculatePosition();
        }

        public ActorPositionWorldPositionProvider (IActor<IDType> actor, Vector3 shift, bool chainActorsProvider = false, bool alwaysEnabled = false)
        {
            Assert.IsNotNull(actor);
            updateDisabled = false;
            this.shift = shift;
            trackedActor = actor;
            this.chainActorsProvider = chainActorsProvider;
            this.alwaysEnabled = alwaysEnabled;

            cachedPosition = CalculatePosition();
        }
        #endregion

        #region IWorldPositionProvider interface.
        Vector3 IWorldPositionProvider.GetWorldPosition()
        {
            if (!updateDisabled)
            {
                cachedPosition = CalculatePosition();;
            }

            return cachedPosition;
        }
        #endregion

        #region Private methods.
        private Vector3 CalculatePosition()
        {
            Assert.IsNotNull(trackedActor);

            if (chainActorsProvider)
            {
                var defaultActorProvider = trackedActor.defaultWorldPositionProvider;
                if (defaultActorProvider != null)
                {
                    return (defaultActorProvider.GetWorldPosition() + shift);
                }
            }

            return (trackedActor.position + shift);
        }
        #endregion
    }
} 
