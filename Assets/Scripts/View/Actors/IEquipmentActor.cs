﻿using Data.Estate.Info;
using Data.Room;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using View.Actors.Events;
using View.Actors.Helpers;
using View.Actors.Settings;
using View.Rooms;

namespace View.Actors
{
    public class EquipmentActorDragEvent : UnityEvent<int, PointerEventData>
    {
    }

    public interface IEquipmentActorSharedDataProvider : IActorSharedDataProvider
    {
        EquipmentMovementMarker movementMarker
        {
            get;
        }

        int animationSelectorParameterHash
        {
            get;
        }
    }

    public delegate void EquipmentActorPlacementUpdated(int id, EquipmentPlacement placement);

    public interface IEquipmentActor : IActor<int>, IEquipmentActorEvents
    {
        event EquipmentActorPlacementUpdated onEquipmentActorPlacementUpdated;

        IEquipmentInfo equipmentInfo
        {
            get;
        }

        int selectedEquipmentAnimation
        {
            set;
        }

        float animationSpeed
        {
            set;
        }

        Vector3 rotationEuler
        {
            get;
        }

        EquipmentPlacement placement
        {
            get;
        }

        RoomCoordinates wallPosition
        {
            get;
        }

        RoomCoordinates wallPositionWithMargin
        {
            get;
        }

        RoomCoordinates size
        {
            get;
        }

        RoomCoordinates wallSize
        {
            get;
        }

        RoomCoordinates wallSizeWithMargin
        {
            get;
        }

        RoomCoordinates orientedSize
        {
            get;
        }

        RoomCoordinates orientedWallSize
        {
            get;
        }

        RoomCoordinates orientedWallSizeWithMargin
        {
            get;
        }

        bool occupiesFloorSpace
        {
            get;
        }

        bool wallMounted
        {
            get;
        }

        Vector3 shift
        {
            get;
        }

        Vector3 shiftRotated
        {
            get;
        }

        int estateType
        {
            get;
        }

        PlacementResult placementState
        {
            get;
        }

        EquipmentActorDragEvent dragEvent
        {
            get;
        }

        bool useDragThreshold
        {
            get; set;
        }

        int layer
        {
            get; set;
        }

        string name
        {
            get;
        }

        bool initiallyLocked
        {
            get;
        }

        int showAtLevel
        {
            get;
        }

        bool editable
        {
            get;
        }

        bool selectable
        {
            get;
        }

        SkinnedMeshRenderer skinnedMeshRenderer
        {
            get;
        }

#if UNITY_EDITOR
        bool editorSelected
        {
            get;
        }

        Bounds visibleBounds
        {
            get;
        }
#endif // #if UNITY_EDITOR

        void Init   ( IEquipmentActorSharedDataProvider sharedDataProvider
                    , int id
                    , IEquipmentInfo equipmentInfo
                    , RoomConductor roomConductor
                    , DisplayMode displayMode
                    , GameObject staticMeshImpostorPrefab
                    , GameObject deliveryImpostorPrefab
                    , GameObject repairOrLockedMarkerPrefab
                    , EquipmentActorSettings actorSettings);
        EquipmentPlacement CalculateCurrentPlacement ();
        void SetPlacement (EquipmentPlacement placement);
        void ResetPlacement ();
#if UNITY_EDITOR
        void SetRoomConductor (RoomConductor roomConductor);
#endif
        void SetEditMode
            ( bool editMode
            , bool editThisObject );
        NavigationPoint StartUsingEquipment (Transform characterTransform, int exerciseTier = -1);
        void StopUsingEquipment ();
        void SetRepairMode(bool repair);
        void StartedUnlocking();
        void UpdateAvailability ();
        NavigationPoint GetLeavingPoint(Vector3 fromPosition, int areaMask);
        NavigationPoint GetClosestApproachingPoint(Vector3 fromPosition, int areaMask, int exerciseTier = -1);
        NavigationPoint GetClosestNavigationPoint(Vector3 fromPosition, int areaMask, string namePrefix);
        NavigationPoint? GetLastClosestApproachingPoint();
    }
}