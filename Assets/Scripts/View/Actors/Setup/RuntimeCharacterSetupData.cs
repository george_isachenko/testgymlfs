﻿using Data.Person;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using View.Actions;

namespace View.Actors.Setup
{
    public class RuntimeCharacterSetupData
    {
        #region Types.
        public struct RuntimeBodyPartData
        {
            public GameObject[][] partSets; // First index: Mood or BodyType, second: selected index.
            public Texture2D[] partTextures;
        }
        #endregion

        #region Public fields.
        public readonly CharacterSetupData csd;

        public RuntimeBodyPartData[] bodyParts; // Index: Body Part type.
        public AnimationClip[][] animations; // First index: Category, second: animationIdx.
        public int animationCategoryParameterHash;
        public int animationSelectorParameterHash;
        public int animationForceSwitchParameterHash;
        public int normalWalkAreaMask = NavMesh.AllAreas;
        public int transferAreaMask = NavMesh.AllAreas;
        #endregion

        #region Constructors.
        public RuntimeCharacterSetupData (CharacterSetupData csd)
        {
            Assert.IsNotNull(csd);
            this.csd = csd;
        }
        #endregion

        #region Public API.
        public static int GetNavigationAreaMask(string[] areaNames)
        {
            int result = 0;

            if (areaNames != null && areaNames.Length > 0)
            {
                foreach (var name in areaNames)
                {
                    int areaIdx = NavMesh.GetAreaFromName(name);
                    if (areaIdx != -1)
                    {
                        result |= (1 << areaIdx);
                    }
                }
            }

            return result;
        }

        public CharacterPortrait CreatePortrait (Transform parent, PersonAppearance appearance, string portraitMaterialPolicyName)
        {
            CharacterPortrait portrait = null;

            if (csd.portraitPrefab != null)
            {
                var portraitObject = Object.Instantiate(csd.portraitPrefab, parent, false);

                portrait = portraitObject.AddComponent<CharacterPortrait>();

                portrait.bodyController.Customize(this, appearance);

                if (portraitMaterialPolicyName != null && portraitMaterialPolicyName != string.Empty)
                {
                    var materialsReplacement = portraitObject.GetComponent<MaterialsReplacement>();
                    if (materialsReplacement != null)
                    {
                        materialsReplacement.ReplaceMaterials(portraitMaterialPolicyName);
                    }
                }
            }

            return portrait;
        }
        #endregion
    }
}
