﻿using System;
using CustomAssets.Lists;
using Data.Person;
using Data.Person.Info;
using InspectorHelpers;
using UnityEngine;

namespace View.Actors.Setup
{
    public enum CharacterTypeKeyMatchType
    {
        GenderMatch,
        TypeNameMatch,
        FullMatch
    }

    [Serializable]
    public struct CharacterTypeKey
    {
        public CharacterTypeKeyMatchType matchType;
        public PersonAppearance.Gender gender;
        public string typeName;

        public bool MatchPerson(PersonAppearance appearance, string typeName)
        {
            switch (matchType)
            {
                case CharacterTypeKeyMatchType.GenderMatch:
                    return gender == appearance.gender;

                case CharacterTypeKeyMatchType.TypeNameMatch:
                    return this.typeName == typeName;

                case CharacterTypeKeyMatchType.FullMatch:
                    return gender == appearance.gender && this.typeName == typeName;

                default:
                    return false;
            }
        }
    }

    [Serializable]
    public class CharacterSetupData
    {
        [Serializable]
        public class AnimationsSetupData
        {
            public enum Mode
            {
                IgnoreCategory,
                OverrideAnimationClip,
                SetSelectorParameterIndex
            }

            [Tooltip("Name of category to handle")]
            public string name;

            public Mode mode;
            public int categoryParameterValue;

            [Tooltip("Name of original animation clip to override")]
            public string overrideClipName;

            [Tooltip("List of animation clips to override. Optional.")]
            public AnimationClipsList overrideAnimations;
        };

        [Serializable]
        public struct BodyPart
        {
            //public Person.Apperance.BodyPart.PartType partType;
            [Tooltip("Array where each item corresponds to PersonAppearance.VisibleMood enum (for head) or PersonAppearance.BodyType (for other parts) and points to GameObjectsList asset with a list of body part prefabs.")]
            [ReorderableList /*(LabelsEnumType = typeof(BodyType))*/]
            public GameObjectsList[] appearanceSets;

            [Tooltip("Per race base (naked) materials with Clothes shader. Unused in Heads.")]
            [ReorderableList (LabelsEnumType = typeof(PersonAppearance.Race))]
            public Material[] baseMaterials;

            [Tooltip("Name of Cloth shader parameter to set Clothes texture. Unused in Heads.")]
            public string clothMaterialParameterName;

            [Tooltip("List of textures to use as Clothes layer. Unused in Heads.")]
            public Texture2DList texturesList;
        }

        //------------------------------------------------------------------------
        [Header("Key data (type matching)")]

        [Tooltip("Name (must be unique).")]
        public string name;

        public CharacterTypeKey characterTypeKey;

        //------------------------------------------------------------------------
        [Header("Prefabs")]

        public GameObject characterPrefab;

        public GameObject portraitPrefab;

        //------------------------------------------------------------------------

        [Header("Body Parts: 0: Heads, 1: Torsos, 2: Legs.")]
        
        [ReorderableList(LabelsEnumType = typeof (PersonAppearance.BodyPart.PartType))]
        public BodyPart[] bodyParts;

        //------------------------------------------------------------------------
        [Header("Animations")]
        
        [Tooltip("Animator Controller parameter name to set to index of selected category")]
        public string animationCategoryParameter;

        [Tooltip("Animator Controller parameter name to set to index of animation in selected category")]
        public string animationSelectorParameter;

        [Tooltip("Animator Controller parameter name used to force switch same equipment animation again")]
        public string animationForceSwitchParameter;

        [Tooltip("List of animation setup data to map animation categories to animation states, one for each category")]
        [ReorderableList]
        public AnimationsSetupData[] animationsSetup;

        //------------------------------------------------------------------------
        [Header("Exercise transitions")]

        public float exerciseApproachTransitionTime;

        //------------------------------------------------------------------------
        [Header("Navigation")]

        public float quickInterpolationDistance;
        public float quickInterpolationTime;

        [ReorderableList]
        [Tooltip("List of navigation area names when performing normal walk (within room).")]
        public string[] normalWalkNavigationAreas;

        [ReorderableList]
        [Tooltip("List of navigation area names when performing room transfers, entering or leaving.")]
        public string[] transferNavigationAreas;
    }
}
