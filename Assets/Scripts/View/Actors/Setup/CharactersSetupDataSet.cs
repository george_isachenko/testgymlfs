﻿using InspectorHelpers;
using UnityEngine;

namespace View.Actors.Setup
{
    [CreateAssetMenu(menuName = "Kingdom/View/Actors/Setup/Characters Setup Data Set", fileName = "Characters Setup")]
    public class CharactersSetupDataSet : ScriptableObject
    {
        [ReorderableList]
        public CharacterSetupData[] charactersSetupData;
    }
}
