﻿using UnityEngine;

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Character Visitor Actor")]
    public class CharacterVisitorActor : CharacterActor
    {
    }
}