﻿using View.Actors.Events;
using View.Rooms;
using System;
using UnityEngine;
using Data;

namespace View.Actors
{
    public interface IFruitTreeActorSharedDataProvider : IActorSharedDataProvider
    {
        GameObject GetCursorObject (IFruitTreeActor actor);
    }

    public interface IFruitTreeActor : IActor<string>, IFruitTreeActorEvents
    {
        bool unlocked
        {
            get;
        }

        bool canUnlock
        {
            get;
        }

        int unlockLevel
        {
            get;
        }

        Cost unlockCost
        {
            get;
        }

        int numFruits
        {
            get;
        }

        int maxFruits
        {
            get;
        }

        TimeSpan regenTotalTime
        {
            get;
        }

        DateTime regenEndNextTime
        {
            get;
        }

        Sprite fruitTypeSprite
        {
            get;
        }

        bool isBusy
        {
            get;
        }

        void Init   ( IFruitTreeActorSharedDataProvider sharedDataProvider
                    , RoomConductor roomConductor
                    , DisplayMode displayMode
                    , bool unlocked
                    , bool canUnlock
                    , int unlockLevel
                    , Cost unlockCost
                    , int numFruits
                    , int maxFruits
                    , TimeSpan regenTotalTime
                    , TimeSpan regenNextTimeLeft );

        void UpdateState ( bool unlocked
                    , bool canUnlock
                    , int numFruits
                    , TimeSpan regenTotalTime
                    , TimeSpan regenNextTimeLeft );
    }
}