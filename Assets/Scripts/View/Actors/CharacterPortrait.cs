﻿// #define USE_WARMUP

using Data.Person;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using View.Actors.Helpers;

namespace View.Actors
{
    public class CharacterPortrait : MonoBehaviour
    {
        #region Private data.
        private Transform originalParent;
        private Animator animator;
        private CharacterBodyController bodyController_;

#if USE_WARMUP
        private IEnumerator warmupJob;
#endif
        #endregion

        #region Public properties.
        public CharacterBodyController bodyController
        {
            get
            {
                if (bodyController_ == null)
                {
                    bodyController_ = GetComponent<CharacterBodyController>();
                    if (bodyController_ == null)
                    {
                        bodyController_ = gameObject.AddComponent<CharacterBodyController>();
                    }
                }

                return bodyController_;
            }
        }
        #endregion

        #region Unity API.
        void Awake ()
        {
            animator = GetComponent<Animator>();
            if (animator != null)
            {
                animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            }

            originalParent = transform.parent;
            transform.SetLayerRecursive(LayerMask.NameToLayer("Hidden"));

#if USE_WARMUP
            warmupJob = Warmup();
            StartCoroutine(warmupJob);
#endif
        }

        void OnEnable()
        {
            StartCoroutine(RandomizeAnimations());
        }
        #endregion

        #region Public API.
        public void Show(Transform newParent)
        {
            Assert.IsNotNull(newParent);

#if USE_WARMUP
            if (warmupJob != null)
            {
                StopCoroutine(warmupJob);
                warmupJob = null;
            }
#endif

            transform.SetLayerRecursive(newParent.gameObject.layer);

            transform.SetParent(newParent, false);

            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }

        public void Hide()
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }

            transform.SetParent(originalParent, false);
        }

        public Bounds GetHeadBounds()
        {
            return default(Bounds);
        }
#endregion

        #region Private methods.
#if USE_WARMUP
        private IEnumerator Warmup ()
        {
            yield return new WaitForSeconds(2.0f);

            gameObject.SetActive(false);

            warmupJob = null;
        }
#endif

        private IEnumerator RandomizeAnimations()
        {
            yield return new WaitForSeconds(1.0f);

            // Randomize animations and animation speeds each time window is shown, because animator parameters can be reset after disabling.
            if (enabled && gameObject.activeInHierarchy)
            {
                var bc = bodyController;
                if (bc != null && bc.runtimeCharacterSetupData != null)
                {
                    bc.SetAnimation(CharacterBodyController.animCategoryIdlePassive, (int)AnimationIdlePassive.Random, UnityEngine.Random.Range(0.98f, 1.05f));
                }
            }
        }
        #endregion
    }
}