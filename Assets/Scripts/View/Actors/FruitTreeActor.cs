﻿using System;
using System.Collections;
using Data;
using UnityEngine;
using UnityEngine.Assertions;
using View.Actions;
using View.Actors.Events;
using View.Rooms;
using System.Text;

#if UNITY_EDITOR

using UnityEditor;
using View.EditorHelpers;

#endif

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Fruit Tree Actor")]
    public class FruitTreeActor
        : Actor<string>
        , IFruitTreeActor
        , IFruitTreeActorEvents
    {
        #region IJuiceTreeActor properties.
        bool IFruitTreeActor.unlocked
        {
            get
            {
                return unlocked;
            }
        }

        bool IFruitTreeActor.canUnlock
        {
            get
            {
                return canUnlock;
            }
        }

        int IFruitTreeActor.unlockLevel
        {
            get
            {
                return unlockLevel;
            }
        }

        Cost IFruitTreeActor.unlockCost
        {
            get
            {
                return unlockCost;
            }
        }

        int IFruitTreeActor.numFruits
        {
            get
            {
                return numFruits;
            }
        }

        int IFruitTreeActor.maxFruits
        {
            get
            {
                return maxFruits;
            }
        }

        TimeSpan IFruitTreeActor.regenTotalTime
        {
            get
            {
                return regenTotalTime;
            }
        }

        DateTime IFruitTreeActor.regenEndNextTime
        {
            get
            {
                return regenEndNextTime;
            }
        }

        Sprite IFruitTreeActor.fruitTypeSprite
        {
            get
            {
                return fruitTypeSprite;
            }
        }

        public bool isBusy
        {
            get
            {
                return (fsm != null) ? fsm.FsmVariables.GetFsmBool("Busy").Value : true;
            }

            set
            {
                if (fsm != null)
                    fsm.FsmVariables.GetFsmBool("Busy").Value = value;
            }
        }
        #endregion

        #region Public (Inspector) data.
        [SerializeField]
        protected Sprite fruitTypeSprite;

        [SerializeField]
        protected GameObject fruitPrefab;

        [SerializeField]
        protected GameObject treePrefab;
        #endregion

        #region Private properties.
        int realFruitsCount
        {
            get
            {
                return (fsm != null) ? fsm.FsmVariables.GetFsmInt("FruitCount").Value : 0;
            }
        }
        #endregion

        #region Private/protected data
        protected bool unlocked;
        protected bool canUnlock;
        protected int unlockLevel;
        protected Cost unlockCost;
        protected int numFruits;
        protected int maxFruits;
        protected TimeSpan regenTotalTime;
        protected DateTime regenEndNextTime;

        protected PlayMakerFSM fsm;
        protected GameObject assignedCursor;

        IEnumerator regenJob;
        IEnumerator collectJob;
        #endregion

        #region IFruitTreeActor API.
        void IFruitTreeActor.Init
            ( IFruitTreeActorSharedDataProvider sharedDataProvider
            , RoomConductor roomConductor
            , DisplayMode displayMode
            , bool unlocked
            , bool canUnlock
            , int unlockLevel
            , Cost unlockCost
            , int numFruits
            , int maxFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft)
        {
            base.Init (sharedDataProvider, gameObject.name, roomConductor, displayMode, false);

            this.unlocked = unlocked;
            this.canUnlock = canUnlock;
            this.unlockLevel = unlockLevel;
            this.unlockCost = unlockCost;
            this.numFruits = numFruits;
            this.maxFruits = maxFruits;
            this.regenTotalTime = regenTotalTime;
            this.regenEndNextTime = DateTime.UtcNow + regenNextTimeLeft;

            // fsm = PlayMakerFSM.FindFsmOnGameObject(gameObject, "FruitTree");
            fsm = gameObject.GetComponentInChildren<PlayMakerFSM>();
            Assert.IsNotNull(fsm, "FruitTreeActor: No PlayMaker FSM found in children!");

            if (fsm != null)
            {
                fsm.enabled = true;

                fsm.FsmVariables.GetFsmGameObject("FruitPrefab").Value = fruitPrefab;
                fsm.FsmVariables.GetFsmGameObject("TreePrefab").Value = treePrefab;
                fsm.FsmVariables.GetFsmObject("TreeIconSprite").Value = fruitTypeSprite;

                StartCoroutine(ResetAndSaveRestoreJob(unlocked));
            }
        }

        void IFruitTreeActor.UpdateState
            ( bool unlocked
            , bool canUnlock
            , int numFruits
            , TimeSpan regenTotalTime
            , TimeSpan regenNextTimeLeft )
        {
            if (!this.unlocked && unlocked)
            {
                StartCoroutine(PlantTreeJob());
            }
            else if (unlocked)
            {
                if (numFruits > this.numFruits)
                {
                    StartRegenJob();
                }
                else if (numFruits < this.numFruits)
                {
                    StopRegenJob();
                    StartCollectJob();
                }
            }

            this.unlocked = unlocked;
            this.canUnlock = canUnlock;
            this.numFruits = numFruits;
            this.regenTotalTime = regenTotalTime;
            this.regenEndNextTime = DateTime.UtcNow + regenNextTimeLeft;
        }
        #endregion

        #region IActor API.
        public override void SetSelection (bool selected)
        {
            if (selected)
            {
                ShowSelectionCursor();

                Sound.instance.VisitorSelect();
            }
            else
            {
                HideSelectionCursor();
            }

            if (currentOverheadUIIcon != null)
                currentOverheadUIIcon.frontIcon = selected;
        }
        #endregion

        #region Unity Editor API
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (Application.isPlaying)
            {
                var stringBuilder = new StringBuilder(256);

                stringBuilder.AppendFormat("{0}: Unlocked: {1}, Can unlock: {2}, Unlock level: {3}, Fruits: [{4}/{5}], Busy: {6}"
                    , gameObject.name, unlocked, canUnlock, unlockLevel, numFruits, maxFruits, isBusy);

                var position = transform.position;
                position.y += 0.15f;

                Handles.Label(position, stringBuilder.ToString(), EditorGizmoHelper.instance.GetOverheadLabelStyle());
            }
        }
#endif
        #endregion

        #region Private methods.
        void StartRegenJob()
        {
            if (regenJob == null)
            {
                regenJob = RegenJob();
                StartCoroutine(regenJob);
            }
        }

        void StopRegenJob()
        {
            if (regenJob != null)
            {
                StopCoroutine(regenJob);
                regenJob = null;
            }
        }

        void StartCollectJob()
        {
            if (collectJob == null)
            {
                collectJob = CollectJob();
                StartCoroutine(collectJob);
            }
        }

        IEnumerator ResetAndSaveRestoreJob(bool unlocked)
        {
            yield return null;

            isBusy = true;

            fsm.SendEvent("FullReset");

            if (unlocked)
            {
                yield return null;

                isBusy = true;

                fsm.FsmVariables.GetFsmInt("FruitCount").Value = numFruits;
                fsm.SendEvent("SaveRestore");
            }
        }

        IEnumerator PlantTreeJob()
        {
            yield return null;

            Assert.IsTrue(unlocked);

            isBusy = true;
            fsm.SendEvent("PlantTree");
        }

        IEnumerator RegenJob ()
        {
            yield return null;

            while (realFruitsCount < numFruits)
            {
                if (isBusy)
                {
                    yield return new WaitWhile(() => isBusy);
                }
                
                isBusy = true;
                fsm.SendEvent("AddFruit");
            }

            regenJob = null;
        }

        IEnumerator CollectJob ()
        {
            yield return null;

            if (isBusy)
            {
                yield return new WaitWhile(() => isBusy);
            }

            var delta = realFruitsCount - numFruits;
            if (delta > 0)
            {
                isBusy = true;
                fsm.FsmVariables.GetFsmInt("CollectDelta").Value = delta;
                fsm.SendEvent("CollectFruits");
            }

            collectJob = null;
        }

        void ShowSelectionCursor()
        {
            if (assignedCursor == null)
            {
                var cursorObject = ((IFruitTreeActorSharedDataProvider)sharedDataProvider).GetCursorObject(this);
                if (cursorObject != null)
                {
                    cursorObject.transform.SetParent(transform, false);
                    cursorObject.SetActive(true);
                    assignedCursor = cursorObject;
                }
            }
        }

        void HideSelectionCursor()
        {
            if (assignedCursor != null)
            {
                var rememberParent = assignedCursor.GetComponent<RememberParent>();
                if (rememberParent != null)
                    rememberParent.ResetParent();
                assignedCursor.SetActive(false);
                assignedCursor = null;
            }
        }
        #endregion
   }
}