﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace View.Actors.Events
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Events/Fruit Tree Actor Events Handler")]
    public class FruitTreeActorEvents : MonoBehaviour, IFruitTreeActorEvents
    {
        #region Public (Inspector) fields.
        #endregion

        #region IEquipmentActorEvents interface.
        #endregion
    }
}