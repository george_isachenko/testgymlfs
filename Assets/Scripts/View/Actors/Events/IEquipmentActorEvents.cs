﻿using UnityEngine.EventSystems;

namespace View.Actors.Events
{
    public class UpgradeEventData : BaseEventData
    {
        public int upgradeLevel { get; private set; } 

        public UpgradeEventData (int upgradeLevel)
            : base(null)
        {
            this.upgradeLevel = upgradeLevel;
        }
    }

    public interface IEquipmentActorEvents : IEventSystemHandler
    {
        void OnAssemblyStarted();
        void OnAssemblyComplete();
        void OnAssemblyCompleteConfirmed();
        void OnUpgraded(UpgradeEventData eventData);
    }
}
