﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace View.Actors.Events
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Events/Equipment Actor Events Handler")]
    public class EquipmentActorEventsHandler : MonoBehaviour, IEquipmentActorEvents
    {
        #region Public (Inspector) fields.
        public UnityEvent onAssemblyStarted;
        public UnityEvent onAssemblyComplete;
        public UnityEvent onAssemblyCompleteConfirmed;
        public UnityEvent onUpgraded;
        #endregion

        #region IEquipmentActorEvents interface.
        void IEquipmentActorEvents.OnAssemblyStarted()
        {
            onAssemblyStarted?.Invoke();
        }

        void IEquipmentActorEvents.OnAssemblyComplete()
        {
            onAssemblyComplete?.Invoke();
        }

        void IEquipmentActorEvents.OnAssemblyCompleteConfirmed()
        {
            onAssemblyCompleteConfirmed?.Invoke();
        }

        void IEquipmentActorEvents.OnUpgraded(UpgradeEventData eventData)
        {
            onUpgraded?.Invoke();
        }
        #endregion
    }
}