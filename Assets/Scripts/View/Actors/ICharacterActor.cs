﻿using System;
using Data.Person;
using Data.Person.Info;
using Data.Room;
using Presentation.Providers;
using UnityEngine;
using View.Actors.Settings;
using View.Actors.Setup;
using View.Rooms;
using Logic.Persons;

namespace View.Actors
{
    public enum CharacterCursorType
    {
        Player = 0,
        Autobot,
        NPC,
    }

    public interface ICharacterActorSharedDataProvider : IActorSharedDataProvider
    {
        GameObject GetCursorObject (CharacterCursorType type);
        bool GetWorldPositionForDestination (ICharacterActor actor, CharacterDestination destination, out Vector3 worldPosition);
    }

    public delegate void CharacterActorPositionUpdated (int id, RoomCoordinates position, float heading);

    public interface ICharacterActor : IActor<int>
    {
        event CharacterActorPositionUpdated onCharacterActorPositionUpdated;

        IPersonInfo personInfo { get; }

        int navigationAreaMask { get; }
        float navigationAgentRadius { get; }

        void Init (ICharacterActorSharedDataProvider sharedDataProvider
                    , int id
                    , IPersonInfo personInfo
                    , RoomConductor roomConductor
                    , DisplayMode displayMode
                    , RuntimeCharacterSetupData runtimeCharacterSetup
                    , CharacterActorSettings tweenSettings
                    , Vector3 worldPosition
                    , Quaternion rotation);
        void Move (CharacterDestination destination, IMovementSettingsInfo movementSettingsInfo, Action<int, MovementResult> onMovementComplete);
        void StopMovement ();
        void ChangeMovementMode (MovementMode mode);
        void UpdateCharacterType (IPersonInfo personInfo);
        void UpdateAppearance ();
        void QueuePlayEmoteAnimation (AnimationSocial animationId, int loopCount, bool rotateTowardsCamera, Action onComplete);
        void QueueLookAt (Vector3 worldLookAtPosition, Action onComplete);
        void QueueEquipmentExercise (int equipmentId, IEquipmentInteractionProvider equipmentInteractionProvider, int exerciseTier, int exerciseAnimation, ExerciseInitiator initiator, TimeSpan duration, ExerciseFlags flags, float speed);
        void QueueSwitchEquipmentExercise (IEquipmentInteractionProvider equipmentInteractionProvider, int exerciseTier, int exerciseAnimation, float speed);
        void QueueEquipmentExerciseEnd (int equipmentId, IEquipmentInteractionProvider equipmentInteractionProvider, ExerciseFlags flags);
        CharacterPortrait GetPortrait ();
        Vector3 GetNearestWorldPoint (Vector3 position, float otherAgentRadius, float distanceMultiplier);
        void TransferToRoom (RoomConductor roomConductor);
    }
}