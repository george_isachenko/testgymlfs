﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace View.Actors
{
    public struct NavigationPoint
    {
        public Vector3 position;
        public Quaternion rotation;
        public float movementTransitionTime;
        public float rotationTransitionTime;

        public NavigationPoint(Vector3 position, Quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
            this.movementTransitionTime = 0;
            this.rotationTransitionTime = 0;
        }

        public NavigationPoint(Vector3 position, Quaternion rotation, float movementTransitionTime, float rotationTransitionTime)
        {
            this.position = position;
            this.rotation = rotation;
            this.movementTransitionTime = movementTransitionTime;
            this.rotationTransitionTime = rotationTransitionTime;
        }

        public IEnumerator InterpolateTo(GameObject go, float? movementTransitionTimeOverride = null, float? rotationTransitionTimeOverride = null)
        {
            Assert.IsNotNull(go);

            var mtt = movementTransitionTimeOverride != null ? movementTransitionTimeOverride.Value : movementTransitionTime;
            var rtt = rotationTransitionTimeOverride != null ? rotationTransitionTimeOverride.Value : rotationTransitionTime;

            var transform = go.transform;

            var doMove = (position - transform.position).sqrMagnitude > Mathf.Epsilon;
            var doRotate = Quaternion.Angle(rotation, transform.rotation) > Mathf.Epsilon;

            if (doMove || doRotate)
            {
                var movementTweenId = 0;
                if (doMove && mtt > 0)
                {
                    movementTweenId = LeanTween.move(go, position, mtt).id;
                }
                else if (mtt == 0)
                {
                    transform.position = position;
                }

                var rotationTweenId = 0;
                if (doRotate && rtt > 0)
                {
                    var originalRotation = transform.rotation;
                    var rotation = this.rotation;
                    rotationTweenId = LeanTween.value(go, 0.0f, 1.0f, rtt).
                        setOnUpdate(delegate (float x) { transform.rotation = Quaternion.LerpUnclamped(originalRotation, rotation, x); }).id;
                }
                else if (rtt == 0)
                {
                    transform.rotation = rotation;
                }

                if (movementTweenId != 0 || rotationTweenId != 0)
                {
                    yield return new WaitUntil(() =>
                           (movementTweenId == 0 || !LeanTween.isTweening(movementTweenId)) &&
                           (rotationTweenId == 0 || !LeanTween.isTweening(rotationTweenId)));
                }
            }
        }
    }
}