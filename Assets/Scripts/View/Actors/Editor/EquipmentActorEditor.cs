﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DataProcessing.CSV;
using FileSystem;
using UnityEditor;
using UnityEngine;

namespace View.Actors.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(EquipmentActor), true)]
    public class EquipmentActorEditor : UnityEditor.Editor
    {
        private class OldEstateType
        {
            public int id;
            [CSVName("ItemType")]
            public string type;
            [CSVName("")]
            public string name;
            public int resourceId;

            public OldEstateType()
            {
                id = -1;
                type = string.Empty;
                name = string.Empty;
                resourceId = -1;
            }

            public override string ToString()
            {
                return string.Format("{0}: [{2}] {1} ", id.ToString("00"), name, resourceId.ToString("000"));
            }

            [CSVName("locNameId")]
            private void setName(string value)
            {
                name = Regex.Replace(value, @"\n", " ");
            }
        }

        private class PerTypeEstateData
        {
            public struct TypeData
            {
                public List<int> ids;
                public List<string> names;
            }

            public List<string> typesNames;
            public List<TypeData> typesData;

            public PerTypeEstateData()
            {
                typesData = new List<TypeData>(16);
                typesNames = new List<string>(16);
            }

            public bool FindEstateById(int id, out int typeIdx, out int listIdx)
            {
                for (int i = 0; i < typesData.Count; i++)
                {
                    var typeData = typesData[i];
                    for (int j = 0; j < typeData.ids.Count; j++)
                    {
                        if (typeData.ids[j] == id)
                        {
                            typeIdx = i;
                            listIdx = j;
                            return true;
                        }
                    }
                }

                typeIdx = -1;
                listIdx = -1;
                return false;
            }
        }

        enum SortMode
        {
            ByName = 0,
            ByID,
            ByResourceId
        }

        static PerTypeEstateData estateData = null;
        static bool loadTried = false;
        static SortMode sortMode = SortMode.ByName;

        int selectedTypeIdx = -1;

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var equipmentActor = (EquipmentActor)target;
            var estateTypeProperty = serializedObject.FindProperty("estateType");

            if (estateData == null && !loadTried)
            {
                LoadEstateData();
                loadTried = true;
            }

            DrawDefaultInspector();

            if (serializedObject.isEditingMultipleObjects)
            {
                EditorGUILayout.HelpBox("Editing 'Estate Type' property in multi-edit mode is not supported yet.", MessageType.Warning, true);
            }
            else
            {
                if (equipmentActor != null && estateTypeProperty != null)
                {
                    if (estateData == null)
                    {
                        EditorGUILayout.PropertyField(estateTypeProperty);
                        EditorGUILayout.HelpBox(string.Format("Failed to parse equipments data resources file \"{0}\".\nCheck file for format errors. Advanced Estate Type editing disabled.", ResourcePaths.BalanceTables.estateTypes), MessageType.Error, true);
                    }
                    else
                    {
                        int typeIdx, listIdx;
                        estateData.FindEstateById(estateTypeProperty.intValue, out typeIdx, out listIdx);

                        EditorGUILayout.BeginHorizontal(); // ---
                        selectedTypeIdx = EditorGUILayout.Popup("Estate Type", selectedTypeIdx >= 0 ? selectedTypeIdx : typeIdx, estateData.typesNames.ToArray());
                        if (selectedTypeIdx >= 0)
                        {
                            int newListIdx = EditorGUILayout.Popup(listIdx, estateData.typesData[selectedTypeIdx].names.ToArray());
                            if (newListIdx >= 0 && newListIdx < estateData.typesData[selectedTypeIdx].ids.Count)
                            {
                                estateTypeProperty.intValue = estateData.typesData[selectedTypeIdx].ids[newListIdx];
                            }
                        }
                        else
                        {
                            EditorGUILayout.Popup(-1, new string[] { });
                        }

                        string sortLabel = string.Empty;
                        switch (sortMode)
                        {
                            case SortMode.ByName:       sortLabel = "\x2191 Name"; break;
                            case SortMode.ByID:         sortLabel = "\x2191 ID"; break;
                            case SortMode.ByResourceId: sortLabel = "\x2191 Res ID"; break;
                        }

                        if (GUILayout.Button(sortLabel, GUILayout.Height(14)))
                        {
                            if (sortMode == SortMode.ByResourceId)
                                sortMode = SortMode.ByName;
                            else
                                sortMode = (SortMode)((int)sortMode + 1);
                            LoadEstateData();
                        }
                        EditorGUILayout.EndHorizontal();   // ---
                    }

                    if (equipmentActor.gameObject.scene.IsValid() && equipmentActor.gameObject.scene.isLoaded)
                    {
                        var placement = ((IEquipmentActor)equipmentActor).placement;
                        EditorGUILayout.LabelField("Placement", string.Format("Position: {0}:{1}, Heading: {2}", placement.position.x, placement.position.y, placement.heading));

                        if (GUILayout.Button("Toggle Carpet and Selection"))
                        {
                            equipmentActor.ToggleCarpetsAndSelection();
                        }
                    }
                }
            }
            serializedObject.ApplyModifiedProperties();
        }

        private void LoadEstateData()
        {
            var estateList = Parser.ParseResourceToObjects<OldEstateType>(ResourcePaths.BalanceTables.estateTypes).ToArray();
            switch (sortMode)
            {
                case SortMode.ByName:
                {
                    Array.Sort(estateList,
                        (estate1, estate2) =>
                            estate1.type == estate2.type
                            ? estate1.name.CompareTo(estate2.name)
                            : estate1.type.CompareTo(estate2.type));
                }; break;

                case SortMode.ByID:
                {
                    Array.Sort(estateList,
                        (estate1, estate2) =>
                            estate1.type == estate2.type
                            ? estate1.id.CompareTo(estate2.id)
                            : estate1.type.CompareTo(estate2.type));
                }; break;

                case SortMode.ByResourceId:
                {
                    Array.Sort(estateList,
                        (estate1, estate2) =>
                            estate1.type == estate2.type
                            ? estate1.resourceId.CompareTo(estate2.resourceId)
                            : estate1.type.CompareTo(estate2.type));
                }; break;

            }
            estateData = new PerTypeEstateData();
            foreach (var estate in estateList)
            {
                int typeIdx = estateData.typesNames.IndexOf(estate.type);
                if (typeIdx < 0)
                {
                    estateData.typesNames.Add(estate.type);
                    var td = new PerTypeEstateData.TypeData();
                    td.ids = new List<int>(64);
                    td.names = new List<string>(64);
                    estateData.typesData.Add(td);
                    typeIdx = estateData.typesNames.Count - 1;
                }

                var typeData = estateData.typesData[typeIdx];
                typeData.ids.Add(estate.id);
                typeData.names.Add(estate.ToString());
            }
        }
    }
}