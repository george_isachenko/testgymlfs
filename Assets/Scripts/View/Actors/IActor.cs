﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using View.Rooms;
using View.UI.OverheadUI;

namespace View.Actors
{
    public class ActorPointerClickEvent<IDType> : UnityEvent<IDType, PointerEventData>
    {
    }

    public class ActorPointerDownEvent<IDType> : UnityEvent<IDType, PointerEventData>
    {
    }

    public class ActorPointerUpEvent<IDType> : UnityEvent<IDType, PointerEventData>
    {
    }

    public interface IActorSharedDataProvider
    {
    }

    public enum DisplayMode
    {
        Hidden = 0,
        ShowActorOnly,
        ShowActorAndOverheadUI
    }

    public interface IActor<IDType>
    {
        IDType id
        {
            get;
        }

        GameObject gameObject
        {
            get;
        }

        OverheadUIIcon overheadUIIcon
        {
            get;
        }

        RoomConductor roomConductor
        {
            get;
        }

        Vector3 position
        {
            get;
        }

        Quaternion rotation
        {
            get;
        }

        Vector3 localPosition
        {
            get;
        }

        Quaternion localRotation
        {
            get;
        }

        ActorPointerClickEvent<IDType> pointerClickEvent
        {
            get;
        }

        ActorPointerDownEvent<IDType> pointerDownEvent
        {
            get;
        }

        ActorPointerUpEvent<IDType> pointerUpEvent
        {
            get;
        }

        IWorldPositionProvider defaultWorldPositionProvider
        {
            get;
        }

        DisplayMode displayMode
        {
            get;
        }

        GameObject eventRedirectionTarget
        {
            get; set;
        }

        void Destroy();
        void SetDisplayMode (DisplayMode displayMode);
        void SetSelection (bool selected);
        void SetOverheadUIIcon(OverheadUIIcon icon, bool useActorWorldPositionProvider = true, bool persistent = false);
        void SetOverheadUIIcon<IconViewT> (OverheadIconInstanceHolder<IconViewT> iconInstanceHolder, bool useActorWorldPositionProvider = true) where IconViewT: Component;
        void RemoveOverheadUIIcon();
        void AssignChildObject(Transform objTransform, bool worldPositionStays);
    }
}