﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using View.Rooms;
using View.UI.OverheadUI;

namespace View.Actors
{
    [SelectionBase]
    public abstract class Actor<IDType>
        : MonoBehaviour
        , IActor<IDType>
        , IPointerClickHandler
        , IPointerDownHandler
        , IPointerUpHandler
    {
        #region IActor properties.
        IDType IActor<IDType>.id
        {
            get
            {
                return id;
            }
        }

        GameObject IActor<IDType>.gameObject
        {
            get
            {
                return gameObject;
            }
        }

        OverheadUIIcon IActor<IDType>.overheadUIIcon
        {
            get
            {
                return currentOverheadUIIcon;
            }
        }

        RoomConductor IActor<IDType>.roomConductor
        {
            get
            {
                return roomConductor;
            }
        }

        Vector3 IActor<IDType>.position
        {
            get
            {
                return transform.position;
            }
        }

        Quaternion IActor<IDType>.rotation
        {
            get
            {
                return transform.rotation;
            }
        }

        public Vector3 localPosition
        {
            get
            {
                return transform.localPosition;
            }
        }

        public Quaternion localRotation
        {
            get
            {
                return transform.localRotation;
            }
        }

        ActorPointerClickEvent<IDType> IActor<IDType>.pointerClickEvent
        {
            get
            {
                if (pointerClickEvent == null)
                {
                    pointerClickEvent = new ActorPointerClickEvent<IDType>();
                }
                return pointerClickEvent;
            }
        }

        ActorPointerDownEvent<IDType> IActor<IDType>.pointerDownEvent
        {
            get
            {
                if (pointerDownEvent == null)
                {
                    pointerDownEvent = new ActorPointerDownEvent<IDType>();
                }
                return pointerDownEvent;
            }
        }

        ActorPointerUpEvent<IDType> IActor<IDType>.pointerUpEvent
        {
            get
            {
                if (pointerUpEvent == null)
                {
                    pointerUpEvent = new ActorPointerUpEvent<IDType>();
                }
                return pointerUpEvent;
            }
        }

        IWorldPositionProvider IActor<IDType>.defaultWorldPositionProvider
        {
            get
            {
                return worldPositionProvider;
            }
        }

        GameObject IActor<IDType>.eventRedirectionTarget
        {
            get
            {
                return eventRedirectionTarget;
            }

            set
            {
                eventRedirectionTarget = value;
            }
        }

        public DisplayMode displayMode { get; set; }
        #endregion

        #region Protected properties.
        protected IActorSharedDataProvider sharedDataProvider { get; private set; }
        #endregion

        #region Protected data.
        protected IDType id;
        protected OverheadUIIcon currentOverheadUIIcon;
        protected bool persistentOverheadUIIcon;
        protected RoomConductor roomConductor;
        protected ActorPointerClickEvent<IDType> pointerClickEvent;
        protected ActorPointerDownEvent<IDType> pointerDownEvent;
        protected ActorPointerUpEvent<IDType> pointerUpEvent;
        protected IWorldPositionProvider worldPositionProvider;
        protected SpriteRenderer shadowRenderer;
        protected GameObject eventRedirectionTarget;
        #endregion

        #region Unity API.
        protected void Awake()
        {
            roomConductor = gameObject.GetComponentInParent<RoomConductor>();
            var shadowObject = transform.Find("shadow");
            if (shadowObject != null)
            {
                shadowRenderer = shadowObject.GetComponent<SpriteRenderer>();
            }
        }
        #endregion

        #region Protected API.
        protected void Init
            ( IActorSharedDataProvider sharedDataProvider
            , IDType id
            , RoomConductor roomConductor
            , DisplayMode displayMode
            , bool worldPositionStays)
        {
            Assert.IsNotNull(sharedDataProvider);
            Assert.IsNotNull(roomConductor);

            this.id = id;
            this.sharedDataProvider = sharedDataProvider;
            this.roomConductor = roomConductor;
            worldPositionProvider = gameObject.GetComponent<IWorldPositionProvider>();

            if (transform.parent == null || transform.parent.GetInstanceID() != roomConductor.roomObject.transform.GetInstanceID())
                transform.SetParent(roomConductor.roomObject.transform, worldPositionStays);

            SetDisplayMode(displayMode);
        }
        #endregion

        #region Public virtual API.
        virtual public void Destroy()
        {
            if (pointerClickEvent != null)
                pointerClickEvent.RemoveAllListeners();
            if (pointerDownEvent != null)
                pointerDownEvent.RemoveAllListeners();
            if (pointerUpEvent != null)
                pointerUpEvent.RemoveAllListeners();
            RemoveOverheadUIIcon();
            transform.SetParent(null);
            Destroy(gameObject);
            pointerClickEvent = null;
        }

        public abstract void SetSelection (bool selected);

        public virtual void SetDisplayMode (DisplayMode displayMode)
        {
            this.displayMode = displayMode;
            
            // Controls overhead icon visibility.
            if (worldPositionProvider != null)
                worldPositionProvider.enabled = (displayMode == DisplayMode.ShowActorAndOverheadUI);
        }

        public virtual void SetOverheadUIIcon(OverheadUIIcon icon, bool useActorWorldPositionProvider, bool persistent)
        {
            if (currentOverheadUIIcon != null)
            {
                RemoveOverheadUIIcon();
            }

            if (useActorWorldPositionProvider)
            {
                icon.worldPositionProvider = worldPositionProvider;
            }

            icon.owner = this;
            icon.visible = true;

            currentOverheadUIIcon = icon;
            persistentOverheadUIIcon = persistent;
        }

        public virtual void SetOverheadUIIcon<IconViewT> (OverheadIconInstanceHolder<IconViewT> iconInstanceHolder, bool useActorWorldPositionProvider)
            where IconViewT: Component
        {
            if (currentOverheadUIIcon != null)
            {
                RemoveOverheadUIIcon();
            }

            if (useActorWorldPositionProvider && worldPositionProvider != null)
            {
                iconInstanceHolder.icon.worldPositionProvider = worldPositionProvider;
            }

            iconInstanceHolder.icon.owner = this;
            iconInstanceHolder.icon.visible = true;

            currentOverheadUIIcon = iconInstanceHolder.icon;
            persistentOverheadUIIcon = iconInstanceHolder.persistent;

        }

        public virtual void RemoveOverheadUIIcon()
        {
            if (currentOverheadUIIcon != null)
            {
                if (!persistentOverheadUIIcon)
                {
                    Destroy(currentOverheadUIIcon.gameObject);
                }
                else
                {
                    currentOverheadUIIcon.visible = false;
                }
                currentOverheadUIIcon = null;
            }
        }
        #endregion

        #region Public API.
        public void AssignChildObject(Transform objTransform, bool worldPositionStays)
        {
            Assert.IsNotNull(objTransform);
            if (objTransform != null)
                objTransform.SetParent(transform, worldPositionStays);
        }
        #endregion

        #region IPointerClickHandler interface.
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            //Debug.LogFormat("Pointer click:\n{0}", eventData.ToString());
            if (eventRedirectionTarget != null)
            {
                var pointerClickHandler = eventRedirectionTarget.GetComponent<IPointerClickHandler>();
                if (pointerClickHandler != null)
                {
                    ExecuteEvents.pointerClickHandler(pointerClickHandler, eventData);
                    return;
                }
            }

            if (pointerClickEvent != null)
            {
                pointerClickEvent.Invoke(id, eventData);
            }
        }
        #endregion
        
        #region IPointerDownHandler interface.
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            //Debug.LogFormat("Pointer down:\n{0}", eventData.ToString());
            if (eventRedirectionTarget != null)
            {
                var pointerDownHandler = eventRedirectionTarget.GetComponent<IPointerDownHandler>();
                if (pointerDownHandler != null)
                {
                    ExecuteEvents.pointerDownHandler(pointerDownHandler, eventData);
                    return;
                }
            }

            if (pointerDownEvent != null)
            {
                pointerDownEvent.Invoke(id, eventData);
            }
        }
        #endregion

        #region IPointerUpHandler interface.
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            //Debug.LogFormat("Pointer up:\n{0}", eventData.ToString());
            if (eventRedirectionTarget != null)
            {
                var pointerUpHandler = eventRedirectionTarget.GetComponent<IPointerUpHandler>();
                if (pointerUpHandler != null)
                {
                    ExecuteEvents.pointerUpHandler(pointerUpHandler, eventData);
                    return;
                }
            }

            if (pointerUpEvent != null)
            {
                pointerUpEvent.Invoke(id, eventData);
            }
        }
        #endregion
    }
}