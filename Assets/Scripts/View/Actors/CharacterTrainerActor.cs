﻿using System;
using System.Collections;
using Logic.Persons;
using Logic.Persons.Tasks;
using UnityEngine;
using View.UI.OverheadUI;
using Random = UnityEngine.Random;

namespace View.Actors
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actors/Character Trainer Actor")]
    public class CharacterTrainerActor : CharacterActor
    {
    }
}