﻿using System;
using Data.Person;
using UnityEngine;
using UnityEngine.Assertions;
using View.Actors.Setup;
using BodyPartType = Data.Person.PersonAppearance.BodyPart.PartType;
using System.Linq;

namespace View.Actors.Helpers
{
    public class CharacterBodyController : MonoBehaviour
    {
        #region Public static readonly data.
        public static readonly string animCategorySocial       = "Social";
        public static readonly string animCategoryIdleComplete = "IdleComplete";
        public static readonly string animCategoryIdlePassive  = "IdlePassive";
        public static readonly string animCategoryEquipment    = "Equipment";
        public static readonly string animCategoryWalking      = "Walking";
        #endregion

        #region Public properties.
        public RuntimeCharacterSetupData runtimeCharacterSetupData { get; private set; }
        #endregion

        #region Private fields.
        PersonAppearance appearance;
        GameObject[][] bodyParts;
        MaterialPropertyBlock commonMaterialPropertyBlock;
        Animator animator;
        #endregion

        #region Unity API.
        void Awake ()
        {
            commonMaterialPropertyBlock = new MaterialPropertyBlock();
            animator = GetComponent<Animator>();
        }
        #endregion

        #region Public API.
        public void Customize
            ( RuntimeCharacterSetupData rcsd
            , PersonAppearance appearance )
        {
            this.runtimeCharacterSetupData = rcsd;
            this.appearance = appearance;

            var bodyPartsNumber = Enum.GetValues(typeof(BodyPartType)).Length;

            if (bodyParts != null)
            {
                // Update heads.
                {
                    int variationsLength = bodyParts[(int)BodyPartType.Head].Length;
                    for (int variationIdx = 0; variationIdx < variationsLength; variationIdx++)
                    {
                        var variation = bodyParts[(int)BodyPartType.Head][variationIdx];
                        if (variation != null)
                        {
                            if (variationIdx == (int)appearance.visibleMood)
                            {
                                UpdateBodyPartVariation(variation, BodyPartType.Head);
                            }

                            variation.SetActive(variationIdx == (int)appearance.visibleMood);
                        }
                    }
                }

                // Update torso.
                {
                    int variationsLength = bodyParts[(int)BodyPartType.Torso].Length;
                    for (int variationIdx = 0; variationIdx < variationsLength; variationIdx++)
                    {
                        var variation = bodyParts[(int)BodyPartType.Torso][variationIdx];
                        if (variation != null)
                        {
                            if (variationIdx == (int)appearance.bodyType)
                            {
                                UpdateBodyPartVariation(variation, BodyPartType.Torso);
                            }

                            variation.SetActive(variationIdx == (int)appearance.bodyType);
                        }
                    }
                }

                // Update legs.
                {
                    int variationsLength = bodyParts[(int)BodyPartType.Legs].Length;
                    for (int variationIdx = 0; variationIdx < variationsLength; variationIdx++)
                    {
                        var variation = bodyParts[(int)BodyPartType.Legs][variationIdx];
                        if (variation != null)
                        {
                            if (variationIdx == (int)appearance.bodyType)
                            {
                                UpdateBodyPartVariation(variation, BodyPartType.Legs);
                            }

                            variation.SetActive(variationIdx == (int)appearance.bodyType);
                        }
                    }
                }
            }
            else
            {
                bodyParts = new GameObject[bodyPartsNumber][];

                Assert.IsNotNull(rcsd.bodyParts);
                Assert.IsTrue(rcsd.bodyParts.Length == bodyPartsNumber);

                // Setup heads.
                var moodEnumNames = Enum.GetNames(typeof(PersonAppearance.VisibleMood));
                {
                    bodyParts[(int)BodyPartType.Head] = SetupCharacterBodyPartVariationsList
                            ( BodyPartType.Head
                            , commonMaterialPropertyBlock
                            , moodEnumNames
                            , (int)appearance.visibleMood);
                    Assert.IsNotNull(bodyParts[(int)BodyPartType.Head]);
                }

                var bodyTypeEnumNames = Enum.GetNames(typeof(PersonAppearance.BodyType));
                // Setup Torsos.
                {
                    bodyParts[(int)BodyPartType.Torso] = SetupCharacterBodyPartVariationsList
                            ( BodyPartType.Torso
                            , commonMaterialPropertyBlock
                            , bodyTypeEnumNames
                            , (int)appearance.bodyType);
                    Assert.IsNotNull(bodyParts[(int)BodyPartType.Torso]);
                }

                // Setup Legs.
                {
                    bodyParts[(int)BodyPartType.Legs] = SetupCharacterBodyPartVariationsList
                            ( BodyPartType.Legs
                            , commonMaterialPropertyBlock
                            , bodyTypeEnumNames
                            , (int)appearance.bodyType);
                    Assert.IsNotNull(bodyParts[(int)BodyPartType.Legs]);
                }
            }

            if (animator != null)
            {
                animator.Rebind();
            }
        }

        public Bounds GetBodyPartBounds(BodyPartType bodyPart)
        {
            var currentVisibleMoodIdx = (bodyPart == BodyPartType.Head)
                ? (int)appearance.visibleMood
                : (int)appearance.bodyType;

            var heads = bodyParts[(int)bodyPart];
            var currentHead = heads[currentVisibleMoodIdx];
            Assert.IsNotNull(currentHead);
            if (currentHead != null)
            {
                var headRenderer = currentHead.GetComponent<Renderer>();
                if (headRenderer != null)
                {
                    var bounds = headRenderer.bounds;
                    bounds.center = headRenderer.transform.localPosition - gameObject.transform.localPosition;
                    return bounds;
                }
            }

            return default(Bounds);
        }

        public CharacterSetupData.AnimationsSetupData GetAnimationSetupData
            (string category, out int runtimeCategoryIdx)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            var csd = runtimeCharacterSetupData.csd;

            for (var i = 0; i < csd.animationsSetup.Length; i++)
            {
                if (csd.animationsSetup[i].name == category)
                {
                    runtimeCategoryIdx = i;
                    return csd.animationsSetup[i];
                }
            }

            runtimeCategoryIdx = -1;
            return null;
        }

        public int SetAnimation(string categoryName, int animationIdx, float speed)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            if (animator == null)
                return -1;

            int categoryIdx;
            var animationSetupData = GetAnimationSetupData(categoryName, out categoryIdx);
            if (animationSetupData == null)
                return categoryIdx;

            animator.speed = speed;

            var currentCategory = animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash);
            var currentAnimationSelector = animator.GetInteger(runtimeCharacterSetupData.animationSelectorParameterHash);

            if (currentCategory == animationSetupData.categoryParameterValue && currentAnimationSelector == animationIdx)
            {
                return categoryIdx;
            }

            switch (animationSetupData.mode)
            {
                case CharacterSetupData.AnimationsSetupData.Mode.IgnoreCategory:
                {
                    // Just ignore request, it's not valid for us.
                    // TODO: Issue warning?
                }
                break;

                case CharacterSetupData.AnimationsSetupData.Mode.OverrideAnimationClip:
                {
                    if (animationSetupData.overrideClipName.Length > 0)
                    {
                        var animatorOverrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
                        if (animatorOverrideController != null)
                        {
                            var animationClip = SelectAnimationClip(categoryIdx, animationIdx);
                            if (animationClip != null)
                            {
                                animatorOverrideController[animationSetupData.overrideClipName] = animationClip;
                                animator.Rebind();
                            }
                        }

/*
                        Debug.LogFormat("SetAnimation(OverrideAnimationClip): Name: '{0}', Category: {1}, animationIdx: {2}."
                            , gameObject.name, animationSetupData.categoryParameterValue, animationIdx);
*/

                        if (currentCategory == animationSetupData.categoryParameterValue)
                        {
                            animator.SetTrigger(runtimeCharacterSetupData.animationForceSwitchParameterHash);
                        }
                        else
                        {
                            animator.ResetTrigger(runtimeCharacterSetupData.animationForceSwitchParameterHash);
                        }

                        animator.SetInteger(runtimeCharacterSetupData.animationCategoryParameterHash, animationSetupData.categoryParameterValue);
                        animator.SetInteger(runtimeCharacterSetupData.animationSelectorParameterHash, animationIdx);
                    }
                }
                break;

                case CharacterSetupData.AnimationsSetupData.Mode.SetSelectorParameterIndex:
                {
/*
                    Debug.LogFormat("SetAnimation(SetSelectorParameterIndex): Name: '{0}', Category: {1}, animationIdx: {2}."
                        , gameObject.name, animationSetupData.categoryParameterValue, animationIdx);
*/

                    animator.SetInteger(runtimeCharacterSetupData.animationCategoryParameterHash, animationSetupData.categoryParameterValue);
                    animator.SetInteger(runtimeCharacterSetupData.animationSelectorParameterHash, animationIdx);
                }
                break;
            }
            return categoryIdx;
        }

        public bool IsAnimation(string categoryName)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            if (animator != null)
            {
                int categoryIdx;
                var animationSetupData = GetAnimationSetupData(categoryName, out categoryIdx);
                if (animationSetupData == null)
                    return false;
            
                return (animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash) == animationSetupData.categoryParameterValue);
            }
            return false;
        }

        public bool IsAnimation(string categoryName, int animationIdx)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            if (animator != null)
            {
                int categoryIdx;
                var animationSetupData = GetAnimationSetupData(categoryName, out categoryIdx);
                if (animationSetupData == null)
                    return false;
            
                return (animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash) == animationSetupData.categoryParameterValue &&
                        animator.GetInteger(runtimeCharacterSetupData.animationSelectorParameterHash) == animationIdx);
            }
            return false;
        }

        public bool IsAnimation(int categoryIdx, int animationIdx)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            var csd = runtimeCharacterSetupData.csd;

            if (animator != null)
            {
                if (categoryIdx >= 0 && categoryIdx < csd.animationsSetup.Length)
                {
                    var animationSetupData = csd.animationsSetup[categoryIdx];

                    return (animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash) == animationSetupData.categoryParameterValue &&
                            animator.GetInteger(runtimeCharacterSetupData.animationSelectorParameterHash) == animationIdx);
                }
            }
            return false;
        }

        public int GetAnimation()
        {
            Assert.IsNotNull(runtimeCharacterSetupData);

            if (animator != null)
            {
                return animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash);
            }

            return -1;
        }

        public int GetAnimation(out int animationIdx)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);

            if (animator != null)
            {
                animationIdx = animator.GetInteger(runtimeCharacterSetupData.animationSelectorParameterHash);
                return animator.GetInteger(runtimeCharacterSetupData.animationCategoryParameterHash);
            }

            animationIdx = -1;
            return -1;
        }

        public bool IsOneOfAnimationCategories(params string[] categories)
        {
            Assert.IsNotNull(runtimeCharacterSetupData);
            Assert.IsNotNull(runtimeCharacterSetupData.csd);

            var csd = runtimeCharacterSetupData.csd;

            for (var i = 0; i < csd.animationsSetup.Length; i++)
            {
                var animSetup = csd.animationsSetup[i];
                if (categories.Contains(animSetup.name))
                    return true;
            }

            return false;
        }
        #endregion

        #region Private methods.
        private void UpdateBodyPartVariation
            ( GameObject variation
            , BodyPartType bodyPartType)
        {
            var rbpd = runtimeCharacterSetupData.bodyParts[(int)bodyPartType];
            var bp = runtimeCharacterSetupData.csd.bodyParts[(int)bodyPartType];
            var abp = appearance.bodyParts[(int)bodyPartType];

            Material customMaterial = null; // Optional.
            if (rbpd.partTextures != null && rbpd.partTextures.Length > 0 &&
                bp.baseMaterials != null && bp.baseMaterials.Length > 0)
            {
                commonMaterialPropertyBlock.Clear();
                customMaterial = bp.baseMaterials[(int)appearance.race];
                if (rbpd.partTextures != null && abp.clothesIdx < rbpd.partTextures.Length)
                {
                    var clothTexture = rbpd.partTextures[abp.clothesIdx];
                    if (clothTexture != null)
                    {
                        commonMaterialPropertyBlock.SetTexture(bp.clothMaterialParameterName, clothTexture);
                    }
                }
            }

            if (customMaterial != null)
            {
                var smr = variation.GetComponent<SkinnedMeshRenderer>();
                if (smr != null)
                {
                    smr.sharedMaterial = customMaterial;
                    smr.SetPropertyBlock(commonMaterialPropertyBlock);
                }
            }
        }

        private GameObject[] SetupCharacterBodyPartVariationsList
            ( BodyPartType partType
            , MaterialPropertyBlock commonMaterialPropertyBlock
            , string[] variationNames
            , int activeVariationIdx)
        {
            var rbpd = runtimeCharacterSetupData.bodyParts[(int)partType];
            var bp = runtimeCharacterSetupData.csd.bodyParts[(int)partType];
            var abp = appearance.bodyParts[(int)partType];

            var listSize = rbpd.partSets.Length;
            if (listSize > 0)
            {
                var list = new GameObject[listSize];

                for (var i = 0; i < listSize; i++)
                {
                    if (abp.meshIdx < 0 || abp.meshIdx >= rbpd.partSets[i].Length)
                    {
                        Debug.LogError(string.Format("Mesh idx {0} is out of range (size: {1}).", abp.meshIdx, rbpd.partSets[i].Length));
                        return null;
                    }

                    var partPrefab = rbpd.partSets[i][abp.meshIdx];
                    Assert.IsNotNull(partPrefab);
                    if (partPrefab != null)
                    {
                        Material customMaterial = null; // Optional.
                        if (rbpd.partTextures != null && rbpd.partTextures.Length > 0 &&
                            bp.baseMaterials != null && bp.baseMaterials.Length > 0)
                        {
                            commonMaterialPropertyBlock.Clear();
                            customMaterial = bp.baseMaterials[(int)appearance.race];
                            if (rbpd.partTextures != null && abp.clothesIdx < rbpd.partTextures.Length)
                            {
                                var clothTexture = rbpd.partTextures[abp.clothesIdx];
                                if (clothTexture != null)
                                {
                                    commonMaterialPropertyBlock.SetTexture(bp.clothMaterialParameterName, clothTexture);
                                }
                            }
                        }

                        var part = SetupCharacterBodyPart
                            ( partPrefab
                            , customMaterial
                            , (commonMaterialPropertyBlock != null) ? commonMaterialPropertyBlock : null
                            , partType.ToString() + variationNames[i]
                            , i == activeVariationIdx);
                        list[i] = part;
                    }
                }
                return list;
            }
            return null;
        }

        private GameObject SetupCharacterBodyPart
            ( GameObject partPrefab
            , Material material
            , MaterialPropertyBlock customMaterialPropertyBlock
            , string partName
            , bool active)
        {
            Assert.IsNotNull(partPrefab);
            if (partPrefab != null)
            {
                var bodyPartObject = new GameObject();
                Assert.IsNotNull(bodyPartObject);
                if (bodyPartObject != null)
                {
                    bodyPartObject.name = partPrefab.name;
                    bodyPartObject.transform.localPosition = Vector3.zero;

                    var prefabSkinnedRenderer = partPrefab.GetComponent<SkinnedMeshRenderer>();
                    if (prefabSkinnedRenderer != null)
                    {
                        var mesh = prefabSkinnedRenderer.sharedMesh;

                        var skinnedRenderer = bodyPartObject.AddComponent<SkinnedMeshRenderer>();
                        skinnedRenderer.updateWhenOffscreen = true;
                        skinnedRenderer.sharedMesh = mesh;
                        skinnedRenderer.sharedMaterial = (material != null) ? material : prefabSkinnedRenderer.sharedMaterial;
                        skinnedRenderer.localBounds = prefabSkinnedRenderer.localBounds;
                        if (customMaterialPropertyBlock != null)
                            skinnedRenderer.SetPropertyBlock(customMaterialPropertyBlock);

                        bodyPartObject.transform.SetParent(gameObject.transform, false);
                        bodyPartObject.SetActive(active);
                        return bodyPartObject;
                    }
                    else
                    {
                        Destroy(bodyPartObject);
                    }
                }
            }
            return null;
        }

        AnimationClip SelectAnimationClip (int runtimeCategoryIdx, int animationIdx)
        {
            if (runtimeCategoryIdx < runtimeCharacterSetupData.animations.Length &&
                runtimeCharacterSetupData.animations[runtimeCategoryIdx] != null)
            {
                var categoryClips = runtimeCharacterSetupData.animations[runtimeCategoryIdx];
                if (animationIdx >= 0 && animationIdx < categoryClips.Length && categoryClips[animationIdx] != null)
                {
                    return categoryClips[animationIdx];
                }
            }

            return null;
        }
        #endregion
    }
}