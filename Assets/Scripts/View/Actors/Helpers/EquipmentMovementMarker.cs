﻿using UnityEngine;

using Data.Room;

namespace View.Actors.Helpers
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Equipment Movement Marker")]
    public class EquipmentMovementMarker : MonoBehaviour
    {
        public GameObject minXMarker;
        public GameObject maxXMarker;
        public GameObject minYMarker;
        public GameObject maxYMarker;

        public void UpdateSize(Vector2 size, int heading, float additionalRotation)
        {
            transform.localRotation = Quaternion.Euler(0, (-heading) * 90.0f  - additionalRotation, 0);

            if (size.sqrMagnitude > Mathf.Epsilon)
            {
                var halfSize = size * 0.5f;

                if (minXMarker != null)
                {
                    minXMarker.transform.localPosition = new Vector3(-halfSize.x, 0.0f, 0.0f);
                }

                if (minYMarker != null)
                {
                    minYMarker.transform.localPosition = new Vector3(0.0f, 0.0f, -halfSize.y);
                }

                if (maxXMarker != null)
                {
                    maxXMarker.transform.localPosition = new Vector3(halfSize.x, 0.0f, 0.0f);
                }

                if (maxYMarker != null)
                {
                    maxYMarker.transform.localPosition = new Vector3(0.0f, 0.0f, halfSize.y);
                }
            }
        }

        public void UpdateState(EquipmentPlacement placement, RoomCoordinates size, RoomCoordinates roomSize, bool wallMounted)
        {
            var heading = placement.heading;
            var orientedSize = size.GetOrientedSize(heading);

            if (wallMounted && (heading == 1 || heading == 3))
            {
                minXMarker.SetActive(false);
                maxXMarker.SetActive(false);
            }
            else
            {
                var minXEnabled = placement.position.x > 0;
                if (minXMarker != null && minXMarker.activeSelf != minXEnabled)
                    minXMarker.SetActive(minXEnabled);

                var maxXEnabled = placement.position.x + orientedSize.x < roomSize.x;
                if (maxXMarker != null && maxXMarker.activeSelf != maxXEnabled)
                    maxXMarker.SetActive(maxXEnabled);
            }

            if (wallMounted && (heading == 0 || heading == 2))
            {
                minYMarker.SetActive(false);
                maxYMarker.SetActive(false);
            }
            else
            {
                var minYEnabled = placement.position.y > 0;
                if (minYMarker != null && minYMarker.activeSelf != minYEnabled)
                    minYMarker.SetActive(minYEnabled);

                var maxYEnabled = placement.position.y + orientedSize.y < roomSize.y;
                if (maxYMarker != null && maxYMarker.activeSelf != maxYEnabled)
                    maxYMarker.SetActive(maxYEnabled);
            }
        }
    }
}