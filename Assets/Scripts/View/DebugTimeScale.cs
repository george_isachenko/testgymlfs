﻿using UnityEngine;

public class DebugTimeScale : MonoBehaviour
{
    #region Types.
    enum Mode
    {
        Normal,
        SpeedUp,
        Slowdown
    }
    #endregion

    #region Public (Inspector) fields.
    public KeyCode keySpeedUp = KeyCode.LeftShift;
    public float timeSpeedUp = 5.0f;
    public KeyCode keySlowDown = KeyCode.LeftAlt;
    public float timeSlowDown = 0.2f;
    #endregion

    #region Private data.
#if DEBUG
    private Mode mode = Mode.Normal;
#endif // #if DEBUG
    #endregion

#if DEBUG
    #region Unity API.
    void Update()
    {
        if (Input.GetKeyDown(keySpeedUp))
        {
            Time.timeScale = timeSpeedUp;
            mode = Mode.SpeedUp;
        }
        else if (Input.GetKeyDown(keySlowDown))
        {
            Time.timeScale = timeSlowDown;
            mode = Mode.Slowdown;
        }
        else if (Input.GetKeyUp(keySpeedUp) && mode == Mode.SpeedUp)
        {
            Time.timeScale = 1.0f;
            mode = Mode.Normal;
        }
        else if (Input.GetKeyUp(keySlowDown) && mode == Mode.Slowdown)
        {
            Time.timeScale = 1.0f;
            mode = Mode.Normal;
        }
    }
    #endregion
#endif // #if DEBUG
}
