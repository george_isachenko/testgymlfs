﻿using UnityEngine;

namespace View.AnimatorHelpers
{
    [AddComponentMenu("Kingdom/View/Animator Helpers/Animator Parameter Proxy")]
    public class AnimatorParameterProxy : MonoBehaviour
    {
        // Fields.
        public string parameterName;
        [Tooltip("Use Animator from this object if empty.")]
        public Animator animator = null;

        // Properties.
        public bool boolParameter
        {
            set
            {
                if (parameterId != 0)
                    animator.SetBool(parameterId, value);
            }

            get
            {
                return (parameterId != 0) ? animator.GetBool(parameterId) : false;
            }
        }

        public int intParameter
        {
            set
            {
                if (parameterId != 0)
                    animator.SetInteger(parameterId, value);
            }

            get
            {
                return (parameterId != 0) ? animator.GetInteger(parameterId) : 0;
            }
        }

        public float floatParameter
        {
            set
            {
                if (parameterId != 0)
                    animator.SetFloat(parameterId, value);
            }

            get
            {
                return (parameterId != 0) ? animator.GetFloat(parameterId) : 0.0f;
            }
        }

        public bool triggerParameter
        {
            set
            {
                if (parameterId != 0)
                {
                    if (value)
                        animator.SetTrigger(parameterId);
                    else
                        animator.ResetTrigger(parameterId);
                }
            }
        }

        // Private variables.
        private int parameterId = 0;

        private void Start()
        {
            if (parameterName.Length > 0)
            {
                if (animator == null)
                    animator = GetComponent<Animator>();

                if (animator != null)
                {
                    parameterId = Animator.StringToHash(parameterName);
                    return;
                }
            }

            enabled = false; // Disable self, if not property initialized.
        }

        private void SetBoolParameter(bool value)
        {
            boolParameter = value;
        }

        private bool GetBoolParameter()
        {
            return boolParameter;
        }

        private void SetIntParameter(int value)
        {
            intParameter = value;
        }

        private int GetIntParameter()
        {
            return intParameter;
        }

        private void SetFloatParameter(float value)
        {
            floatParameter = value;
        }

        private float GetFloatParameter()
        {
            return floatParameter;
        }

        private void SetTriggerParameter(bool value)
        {
            triggerParameter = value;
        }
    }
}