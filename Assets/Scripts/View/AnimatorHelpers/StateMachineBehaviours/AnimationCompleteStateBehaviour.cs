﻿using UnityEngine;

namespace View.AnimatorHelpers.StateMachineBehaviours
{
    public class AnimationCompleteStateBehaviour : StateMachineBehaviour
    {
        public bool signalOnUpdate = true;
        public bool signalOnExit = false;

        //     bool entered = false;
        private int lastLoop = 0;

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //         if (entered)
            //             return;
            //
            //         entered = true;
            lastLoop = 0;
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //         if (!entered)
            //             return;
            //
            //         entered = false;

            if (signalOnExit && !stateInfo.loop && lastLoop == 0)
            {
                SendSignal(animator);
            }

            lastLoop = 0;
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //         if (entered)
            {
                //Debug.LogFormat("Animator {0}: Normalized time: {1}", animator.GetInstanceID(), stateInfo.normalizedTime);
                if (signalOnUpdate && (stateInfo.loop || lastLoop == 0) && (stateInfo.normalizedTime >= ((float)lastLoop + 1.0f - Mathf.Epsilon)))
                {
                    lastLoop++;

                    SendSignal(animator);
                }
            }
        }

        private void SendSignal(Animator animator)
        {
            if (animator.gameObject != null)
            {
                var iAnimationComplete = animator.gameObject.GetComponent<IAnimationComplete>();
                if (iAnimationComplete != null)
                {
                    iAnimationComplete.OnAnimationComplete(lastLoop + 1);
                }
            }
        }
    }
}