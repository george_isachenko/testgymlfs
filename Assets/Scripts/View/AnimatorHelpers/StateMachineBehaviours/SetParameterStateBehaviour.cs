﻿using UnityEngine;
using UnityEngine.Assertions;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.AnimatorHelpers.StateMachineBehaviours
{
    public class SetParameterStateBehaviour : StateMachineBehaviour
    {
        // Fields.
        public enum SetOn
        {
            Self,
            Parent,
            Siblings,
            SelfAndSiblings,
            Childs,
            ChildsTree
        }

        [System.Serializable]
        public class MultiTypeValue
        {
            public AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Float;
            public bool boolOrTriggerValue = false;
            public int integerValue = 0;
            public float floatValue = 0.0f;

            public void SetParameter(int parameterId, Animator animator)
            {
                switch (parameterType)
                {
                    case AnimatorControllerParameterType.Float:
                    {
                        animator.SetFloat(parameterId, floatValue);
                    }
                    break;

                    case AnimatorControllerParameterType.Int:
                    {
                        animator.SetInteger(parameterId, integerValue);
                    }
                    break;

                    case AnimatorControllerParameterType.Bool:
                    {
                        animator.SetBool(parameterId, boolOrTriggerValue);
                    }
                    break;

                    case AnimatorControllerParameterType.Trigger:
                    {
                        if (boolOrTriggerValue)
                            animator.SetTrigger(parameterId);
                        else
                            animator.ResetTrigger(parameterId);
                    }
                    break;
                }
            }

#if UNITY_EDITOR
            [CustomPropertyDrawer(typeof(MultiTypeValue))]
            public class MultiTypePropertyDrawer : PropertyDrawer
            {
                public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
                {
                    // Using BeginProperty / EndProperty on the parent property means that
                    // prefab override logic works on the entire property.
                    EditorGUI.BeginProperty(position, label, property);

                    // Draw label
                    position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

                    // Draw type enum.
                    var enumRect = new Rect(position.x, position.y, 70, position.height);
                    var parameterTypeProp = property.FindPropertyRelative("parameterType");
                    EditorGUI.PropertyField(enumRect, parameterTypeProp, GUIContent.none);

                    var valueRect = new Rect(position.x + 75, position.y, 100, position.height);

                    switch (parameterTypeProp.enumValueIndex)
                    {
                        case 0: // AnimatorControllerParameterType.Float
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("floatValue"), GUIContent.none);
                        break;

                        case 1: // AnimatorControllerParameterType.Int:
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("integerValue"), GUIContent.none);
                        break;

                        case 2: // AnimatorControllerParameterType.Bool:
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("boolOrTriggerValue"), GUIContent.none);
                        break;

                        case 3: // AnimatorControllerParameterType.Trigger:
                        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("boolOrTriggerValue"), GUIContent.none);
                        break;
                    }
                    EditorGUI.EndProperty();
                }
            }
#endif
        }

        public string parameterName = string.Empty;
        public SetOn setFor = SetOn.Self;
        [Tooltip("Object path, relative to object with animator and setFor value (parent of object for 'Parent' value, self for others).")]
        public string objectPath = string.Empty;
        public MultiTypeValue value;
        [Tooltip("Copy value from other animator parameter of this object instead of setting it directly. Not valid for trigger values.")]
        public string otherParameter = string.Empty;

        public bool setOnEnter = true;
        public bool setOnExit = false;
        public bool setOnUpdate = false;

        // Private vars.
        private bool entered = false;

        private int paramId;
        private int otherParamId;
        private string[] objectPaths;

        private void OnEnable()
        {
            if (parameterName.Length > 0 && paramId == 0)
            {
                paramId = Animator.StringToHash(parameterName);
            }

            if (otherParameter.Length > 0 && otherParamId == 0)
            {
                otherParamId = Animator.StringToHash(otherParameter);
            }

            // Split Object path into array for easier matching.
            if (objectPath.Length > 0 && objectPaths == null)
            {
                objectPaths = objectPath.Split('/');
                if (objectPaths == null || objectPaths.Length == 0)
                {
                    objectPaths = new string[1];
                    objectPaths[0] = objectPath;
                }
            }
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (paramId == 0 || entered)
                return;

            entered = true;
            if (setOnEnter)
            {
                PropagateProperty(animator);
            }
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!entered || paramId == 0)
                return;

            entered = false;
            if (setOnExit)
            {
                PropagateProperty(animator);
            }
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (paramId == 0)
                return;

            if (entered && setOnUpdate)
            {
                PropagateProperty(animator);
            }
        }

        public void PropagateProperty(Animator animator)
        {
            Assert.IsNotNull(animator);
            switch (setFor)
            {
                case SetOn.Self:
                {
                    SetParameter(animator, animator);
                }
                break;

                case SetOn.Parent:
                {
                    var parentAnimator = animator.transform.parent.GetComponent<Animator>();
                    Assert.IsNotNull(parentAnimator, "No animator in parent object to set.");

                    if (parentAnimator != null)
                    {
                        SetParameter(animator, parentAnimator);
                    }
                }
                break;

                case SetOn.Siblings:
                {
                    SetOnChilds(animator, null, animator.transform.parent, 0, false);
                }
                break;

                case SetOn.SelfAndSiblings:
                {
                    SetParameter(animator, animator);
                    SetOnChilds(animator, animator.transform, animator.transform.parent, 0, false);
                }
                break;

                case SetOn.Childs:
                {
                    SetOnChilds(animator, null, animator.transform, 0, false);
                }
                break;

                case SetOn.ChildsTree:
                {
                    SetOnChilds(animator, null, animator.transform, 0, true);
                }
                break;
            }
        }

        public void SetOnChilds(Animator thisAnimator, Transform ignore, Transform transform, int depth, bool deep)
        {
            if (transform != null)
            {
                for (var i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i);
                    if (child != null &&
                            (ignore == null || !ReferenceEquals(ignore, child)))
                    {
                        if (objectPaths == null || objectPaths.Length == 0)
                        {
                            var siblingAnimator = child.GetComponent<Animator>();
                            if (siblingAnimator != null)
                            {
                                SetParameter(thisAnimator, siblingAnimator);
                            }

                            if (deep)
                                SetOnChilds(thisAnimator, ignore, child, depth + 1, deep);
                        }
                        else if (depth < objectPaths.Length && child.name == objectPaths[depth])
                        {
                            if (depth == objectPaths.Length - 1)
                            {
                                var siblingAnimator = child.GetComponent<Animator>();
                                if (siblingAnimator != null)
                                {
                                    SetParameter(thisAnimator, siblingAnimator);
                                }
                            }

                            if (deep && (depth < objectPaths.Length - 1))
                                SetOnChilds(thisAnimator, ignore, child, depth + 1, deep);
                        }
                    }
                }
            }
        }

        public void SetParameter(Animator fromAnimator, Animator toAnimator)
        {
            if (otherParamId != 0)
            {
                for (var i = 0; i < fromAnimator.parameterCount; i++)
                {
                    var otherParam = fromAnimator.GetParameter(i);
                    if (otherParam.nameHash == otherParamId)
                    {
                        Assert.AreNotEqual(otherParam.type, AnimatorControllerParameterType.Trigger, "Cannot get value from other parameter of trigger type.");

                        switch (otherParam.type)
                        {
                            case AnimatorControllerParameterType.Float:
                            {
                                toAnimator.SetFloat(paramId, fromAnimator.GetFloat(otherParamId));
                            }
                            break;

                            case AnimatorControllerParameterType.Int:
                            {
                                toAnimator.SetInteger(paramId, fromAnimator.GetInteger(otherParamId));
                            }
                            break;

                            case AnimatorControllerParameterType.Bool:
                            {
                                toAnimator.SetBool(paramId, fromAnimator.GetBool(otherParamId));
                            }
                            break;
                        }

                        break;
                    }
                }
            }
            else
            {
                value.SetParameter(paramId, toAnimator);
            }
        }
    }
}