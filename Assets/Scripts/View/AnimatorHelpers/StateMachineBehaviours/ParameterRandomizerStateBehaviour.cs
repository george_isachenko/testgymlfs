﻿using UnityEngine;
using UnityEngine.Assertions;

namespace View.AnimatorHelpers.StateMachineBehaviours
{
    public class ParameterRandomizerStateBehaviour : StateMachineBehaviour
    {
        // Fields.
        public string parameterName = string.Empty;

        public bool generateOnEnter = true;
        public bool generateOnExit = false;
        public bool generateOnUpdate = false;
        public float min = 0.0f;
        public float max = 1.0f;

        // Private vars.
        private Animator cachedAnimator = null;

        private AnimatorControllerParameterType cachedParameterType = AnimatorControllerParameterType.Float;
//      private bool entered = false;
        private int paramId;

        private void OnEnable()
        {
            Assert.IsTrue(min < max, "'min' value must be smaller than 'max' value. Component will not work.");
            if (min < max)
            {
                if (parameterName.Length > 0 && paramId == 0)
                {
                    paramId = Animator.StringToHash(parameterName);
                }
            }
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
//             if (paramId == 0 || entered)
//                 return;

//          entered = true;
            if (generateOnEnter)
            {
                UpdateCachedAnimator(animator);
                GenerateNewParamValue(animator);
            }
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
//             if (!entered || paramId == 0)
//                 return;

//          entered = false;
            if (generateOnExit)
            {
                UpdateCachedAnimator(animator);
                GenerateNewParamValue(animator);
            }
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (paramId == 0)
                return;

            if (/*entered && */generateOnUpdate)
            {
                UpdateCachedAnimator(animator);
                GenerateNewParamValue(animator);
            }
        }

        private void UpdateCachedAnimator(Animator animator)
        {
            if (!ReferenceEquals(animator, cachedAnimator))
            {
                cachedAnimator = animator;
                cachedParameterType = GetParameterType(animator);
            }
        }

        private AnimatorControllerParameterType GetParameterType(Animator animator)
        {
            for (var i = 0; i < animator.parameters.Length; i++)
            {
                if (animator.parameters[i].nameHash == paramId)
                    return animator.parameters[i].type;
            }

            return AnimatorControllerParameterType.Float;
        }

        private void GenerateNewParamValue(Animator animator)
        {
            switch (cachedParameterType)
            {
                case AnimatorControllerParameterType.Float:
                {
                    var value = Random.Range(min, max);
                    //Debug.LogFormat("ParameterRandomizerStateBehaviour: Generated new {0} float field value: {1}.", parameterName, value);
                    animator.SetFloat(paramId, value);
                }
                break;

                case AnimatorControllerParameterType.Int:
                {
                    var value = Random.Range((int)min, (int)max + 1);
                    //Debug.LogFormat("ParameterRandomizerStateBehaviour: Generated new {0} int field value: {1}.", parameterName, value);
                    animator.SetInteger(paramId, value);
                }
                break;

                case AnimatorControllerParameterType.Bool:
                {
                    var value = Random.Range(0, 2);
                    //Debug.LogFormat("ParameterRandomizerStateBehaviour: Generated new {0} bool field value: {1}.", parameterName, value);
                    animator.SetBool(paramId, value == 0);
                }
                break;

                case AnimatorControllerParameterType.Trigger:
                {
                    var value = Random.Range(0, 2);
                    //Debug.LogFormat("ParameterRandomizerStateBehaviour: Generated new {0} trigger field value: {1}.", parameterName, value);
                    if (value == 0)
                        animator.SetTrigger(paramId);
                    else
                        animator.ResetTrigger(paramId);
                };
                break;
            }
        }
    }
}