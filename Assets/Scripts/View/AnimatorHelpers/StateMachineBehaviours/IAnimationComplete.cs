﻿namespace View.AnimatorHelpers.StateMachineBehaviours
{
    public interface IAnimationComplete
    {
        void OnAnimationComplete(int loopCount);
    }
}