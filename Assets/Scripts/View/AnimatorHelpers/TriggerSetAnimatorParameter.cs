﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace View.AnimatorHelpers
{
    [AddComponentMenu("Kingdom/View/Animator Helpers/Set Animator Parameter On Trigger")]
    public class TriggerSetAnimatorParameter : MonoBehaviour
    {
        private static readonly int collidersListInitialReserve = 8;

        #region Public (Inspector) fields.
        [Tooltip("Use Animator from this object if empty.")]
        public Animator animator = null;
        public string parameterName = string.Empty;
        public AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Float;

        public bool boolOrTriggerValueOnEnter = true;
        public int integerValueOnEnter = 1;
        public float floatValueOnEnter = 1.0f;

        public bool boolOrTriggerValueOnExit = false;
        public int integerValueOnExit = 0;
        public float floatValueOnExit = 0.0f;
        #endregion

        #region Private data.
        private int paramId = 0;
        private List<Collider> enteredColliders;
        #endregion

        // Methods.
        #region Unity API.
        private void Start()
        {
            if (animator == null)
                animator = GetComponent<Animator>();

            Assert.IsNotNull(animator, "No animator in object to use.");
            if (animator != null && parameterName != string.Empty)
            {
                paramId = Animator.StringToHash(parameterName);

                enteredColliders = new List<Collider>(collidersListInitialReserve);
            }
            else
            {
                enabled = false; // Invalid setup, turn self off.
            }
        }

        void Update()
        {
            // Update() is used in addition to handling OnTriggerExit() because Unity doesn't handles well
            // when object is destroyed while it's entered trigger - corresponding OnTriggerExit() function is not invoked.

            if (animator != null && enteredColliders != null)
            {
                if (enteredColliders.Count > 0)
                {
                    enteredColliders.RemoveAll((x) => x == null);

                    if (enteredColliders.Count == 0)
                    {
                        SetParameterOnExit();
                    }
                }
            }
            else
            {
                enabled = false; // Invalid setup, turn self off.
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (animator != null && enteredColliders != null)
            {
                Assert.IsFalse(enteredColliders.Contains(other), "Duplicate Trigger Enter!");

                if (!enteredColliders.Contains(other))
                {
                    enteredColliders.Add(other);
                }

                if (enteredColliders.Count == 1)
                {
                    SetParameterOnEnter();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (animator != null && enteredColliders != null)
            {
                Assert.IsTrue(enteredColliders.Contains(other), "Exiting Trigger without entering it first!");

                enteredColliders.Remove(other);

                if (enteredColliders.Count == 0)
                {
                    SetParameterOnExit();
                }
            }
        }
        #endregion

        #region Private functions.
        private void SetParameterOnEnter()
        {
            switch (parameterType)
            {
                case AnimatorControllerParameterType.Float:
                {
                    animator.SetFloat(paramId, floatValueOnEnter);
                }
                break;

                case AnimatorControllerParameterType.Int:
                {
                    animator.SetInteger(paramId, integerValueOnEnter);
                }
                break;

                case AnimatorControllerParameterType.Bool:
                {
                    animator.SetBool(paramId, boolOrTriggerValueOnEnter);
                }
                break;

                case AnimatorControllerParameterType.Trigger:
                {
                    if (boolOrTriggerValueOnEnter)
                        animator.SetTrigger(paramId);
                    else
                        animator.ResetTrigger(paramId);
                };
                break;
            }
        }

        private void SetParameterOnExit()
        {
            switch (parameterType)
            {
                case AnimatorControllerParameterType.Float:
                {
                    animator.SetFloat(paramId, floatValueOnExit);
                }
                break;

                case AnimatorControllerParameterType.Int:
                {
                    animator.SetInteger(paramId, integerValueOnExit);
                }
                break;

                case AnimatorControllerParameterType.Bool:
                {
                    animator.SetBool(paramId, boolOrTriggerValueOnExit);
                }
                break;

                case AnimatorControllerParameterType.Trigger:
                {
                    if (boolOrTriggerValueOnExit)
                        animator.SetTrigger(paramId);
                    else
                        animator.ResetTrigger(paramId);
                };
                break;
            }
        }
        #endregion
    }
}