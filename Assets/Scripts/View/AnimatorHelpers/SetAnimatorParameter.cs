﻿using UnityEngine;
using UnityEngine.Assertions;

namespace View.AnimatorHelpers
{
    [AddComponentMenu("Kingdom/View/Animator Helpers/Set Animator Parameter")]
    public class SetAnimatorParameter : MonoBehaviour
    {
        // Fields.
        [Tooltip("Use Animator from this object if empty.")]
        public Animator animator = null;
        public string parameterName = string.Empty;
        public AnimatorControllerParameterType parameterType = AnimatorControllerParameterType.Float;

        public bool boolOrTriggerValue = false;
        public int integerValue = 0;
        public float floatValue = 0.0f;

        // Private variables.
        private int paramId;

        // Methods.
        private void Start()
        {
            if (animator == null)
                animator = GetComponent<Animator>();

            Assert.IsNotNull(animator, "No animator in object to use.");
            if (animator != null)
            {
                paramId = Animator.StringToHash(parameterName);
                SetParameter();
            }
        }

        // Private functions.
        private void SetParameter()
        {
            switch (parameterType)
            {
                case AnimatorControllerParameterType.Float:
                {
                    animator.SetFloat(paramId, floatValue);
                }
                break;

                case AnimatorControllerParameterType.Int:
                {
                    animator.SetInteger(paramId, integerValue);
                }
                break;

                case AnimatorControllerParameterType.Bool:
                {
                    animator.SetBool(paramId, boolOrTriggerValue);
                }
                break;

                case AnimatorControllerParameterType.Trigger:
                {
                    if (boolOrTriggerValue)
                        animator.SetTrigger(paramId);
                    else
                        animator.ResetTrigger(paramId);
                };
                break;
            }
        }
    }
}