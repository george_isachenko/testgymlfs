﻿using UnityEngine;
using UnityEngine.Events;
using View.AnimatorHelpers.StateMachineBehaviours;

namespace View.AnimatorHelpers
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Animator Helpers/Animation Complete Router")]
    public class AnimationCompleteRouter : MonoBehaviour, IAnimationComplete
    {
        public class AnimationCompleteEvent : UnityEvent<int>
        {
        }

        public AnimationCompleteEvent animationCompleteEvent
        {
            get
            {
                if (_animationCompleteEvent == null)
                {
                    _animationCompleteEvent = new AnimationCompleteEvent();
                }
                return _animationCompleteEvent;
            }
        }

        private AnimationCompleteEvent _animationCompleteEvent;

        void IAnimationComplete.OnAnimationComplete(int loopCount)
        {
            if (_animationCompleteEvent != null)
            {
                _animationCompleteEvent.Invoke(loopCount);
            }
        }
    }
} 