﻿using Core.Assets;
using UnityEngine;

namespace View.EditorHelpers
{
    [CreateAssetMenu(menuName = "Kingdom/Editor/EditorGizmoHelper", fileName = "EditorGizmoHelper")]
    [SingletonResource("Editor/EditorGizmoHelper")]
    public class EditorGizmoHelper : SingletonResource<EditorGizmoHelper>
    {
        [SerializeField]
        protected GUIStyle overheadLabelStyle;

        public GUIStyle GetOverheadLabelStyle()
        {
            return overheadLabelStyle;
        }

#if UNITY_EDITOR
        public static void DrawArrow(Vector3 p1, Vector3 p2, Color color, float dottedLineSize = 0.0f, float arrowMagnitude = 0.1f)
        {
            var originalColor = UnityEditor.Handles.color;

            UnityEditor.Handles.color = color;

            Vector3 line = (p2 - p1);
            var magnitude = line.magnitude;
            if (magnitude > arrowMagnitude)
            {
                magnitude = magnitude - arrowMagnitude;
                line.Normalize();
                Vector3 endPos = p1 + line * magnitude;

                if (dottedLineSize > 0)
                    UnityEditor.Handles.DrawDottedLine(p1, endPos, dottedLineSize);
                else
                    UnityEditor.Handles.DrawLine(p1, endPos);
#if UNITY_5_6_OR_NEWER
                UnityEditor.Handles.ArrowHandleCap(0, endPos, Quaternion.LookRotation(line, Vector3.up), arrowMagnitude, EventType.Repaint);
#else
                UnityEditor.Handles.ArrowCap(0, endPos, Quaternion.LookRotation(line, Vector3.up), arrowMagnitude);
#endif
            }
            else if (magnitude > Mathf.Epsilon)
            {
                UnityEditor.Handles.DrawLine(p1, p2);
            }

            UnityEditor.Handles.color = originalColor;
        }
#endif
    }
}