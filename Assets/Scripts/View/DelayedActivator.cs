﻿using UnityEngine;
using System.Collections;
using System;

namespace View
{
    [AddComponentMenu("Kingdom/View/Delayed Activator")]
    public class DelayedActivator : MonoBehaviour
    {
        #region Public fields.
        public GameObject objectToActivate;
        public float activationDelay = 1.0f;
        #endregion

        #region Unity API.
        void Start ()
        {
            if (objectToActivate != null)
            {
                if (activationDelay > 0)
                {
                    StartCoroutine(ActivateJob());
                }
                else
                {
                    objectToActivate.SetActive(true);
                }
            }
        }
        #endregion

        #region Private methods.
        private IEnumerator ActivateJob ()
        {
            if (activationDelay > 0)
            {
                yield return new WaitForSeconds(activationDelay);
            }

            if (objectToActivate != null)
            {
                objectToActivate.SetActive(true);
            }
        }
        #endregion
    }
}