﻿using System;
using InspectorHelpers;
using UnityEngine;
using UnityEngine.Events;
using View.UI;

namespace View.Actions
{
    [AddComponentMenu("Kingdom/View/Actions/Button Input Handler")]
    public class ButtonInputHandler : MonoBehaviour
    {
        [Tooltip("Virtual button names to handle. See Input module for details.")]
        [ReorderableList]
        public string[] buttonNames;
        public UnityEvent onButtonHeld;
        public UnityEvent onButtonDown;
        public UnityEvent onButtonUp;

        // Use this for initialization
        void Start()
        {
            // Nothing to handle.
            if (!IsButtonListValid() ||
                (onButtonHeld == null && onButtonDown == null && onButtonUp == null))
            {
                enabled = false;
            }

        }

        // Update is called once per frame
        void Update()
        {
            if (buttonNames != null && buttonNames.Length > 0)
            {
                if (onButtonHeld != null)
                {
                    foreach (var buttonName in buttonNames)
                    {
                        if (Input.GetButton(buttonName))
                        {
                            onButtonHeld.Invoke();
                            break;
                        }
                    }
                }

                if (onButtonDown != null)
                {
                    foreach (var buttonName in buttonNames)
                    {
                        if (Input.GetButtonDown(buttonName))
                        {
                            onButtonDown.Invoke();
                            break;
                        }
                    }
                }

                if (onButtonUp != null)
                {
                    foreach (var buttonName in buttonNames)
                    {
                        if (Input.GetButtonUp(buttonName))
                        {
                            onButtonUp.Invoke();
                            break;
                        }
                    }
                }
            }
        }

        bool IsButtonListValid()
        {
            return (buttonNames != null && buttonNames.Length > 0 && Array.Exists(buttonNames, i => (i != null && i != string.Empty)));
        }
    }
}