﻿using UnityEngine;
using UnityEngine.Events;

namespace View.Actions
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actions/Trigger Handler")]
    public class TriggerHandler : MonoBehaviour
    {
        public UnityEvent onTriggerEnter;
        public UnityEvent onTriggerExit;

        void OnTriggerEnter(Collider other)
        {
            onTriggerEnter.Invoke();
        }

        void OnTriggerExit(Collider other)
        {
            onTriggerExit.Invoke();
        }
    }
}