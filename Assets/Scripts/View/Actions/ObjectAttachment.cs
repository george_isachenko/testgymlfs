﻿using UnityEngine;
using System.Collections.Generic;

namespace View.Actions
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actions/Object Attachment")]
    public class ObjectAttachment : MonoBehaviour
    {
        struct AttachmentInfo
        {
            public Transform attachedTransform;
            public Vector3 translationDelta;
            public Quaternion rotationDelta;

            public AttachmentInfo(Transform attachedTransform, Transform transform)
            {
                this.attachedTransform = attachedTransform;
                translationDelta = Vector3.zero;
                rotationDelta = Quaternion.identity;
                UpdateDeltas(transform);
                attachedTransform.hasChanged = false;
            }

            public void UpdateDeltas(Transform transform)
            {
                translationDelta = attachedTransform.position - transform.position;
                rotationDelta = Quaternion.Inverse(attachedTransform.rotation) * transform.rotation;
            }
        }

        private List<AttachmentInfo> attachments = new List<AttachmentInfo>(8);

        void LateUpdate()
        {
            if (attachments.Count > 0)
            {
                if (transform.hasChanged)
                {
                    attachments.ForEach(t =>
                        {
                            t.attachedTransform.position = transform.position + t.translationDelta;
                            t.attachedTransform.rotation = transform.rotation * t.rotationDelta;
                            t.attachedTransform.hasChanged = false;
                        } );
                    transform.hasChanged = false;
                }
                else
                {
                    for (var i = 0; i < attachments.Count; i++)
                    {
                        var copy = attachments[i];
                        if (copy.attachedTransform.hasChanged)
                        {
                            copy.UpdateDeltas(transform);
                            copy.attachedTransform.hasChanged = false;
                            attachments[i] = copy;
                        }
                    }
                }
            }
            else
            {
                enabled = false;
            }
        }

        public void AttachObject (Transform attachedTransform)
        {
            if (!attachments.Exists(t => t.attachedTransform == attachedTransform))
            {
                attachments.Add(new AttachmentInfo(attachedTransform, transform));
            }
            enabled = true;
        }

        public void DetachObject (Transform attachedTransform)
        {
            attachments.RemoveAll(t => t.attachedTransform == attachedTransform);
            if (attachments.Count == 0)
                enabled = false;
        }

    }
}