﻿using UnityEngine;
using System.Collections;

namespace View.Actions
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    class AutoDestroyer : MonoBehaviour
    {
        void Update()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
