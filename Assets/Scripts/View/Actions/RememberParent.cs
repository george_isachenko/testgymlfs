﻿using UnityEngine;

namespace View.Actions
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actions/Remember Parent")]
    public class RememberParent : MonoBehaviour
    {
        private Transform originalParent;

        void OnTransformParentChanged()
        {
            if (Application.isPlaying && originalParent == null && transform.parent != null)
            {
                originalParent = transform.parent;
            }
        }

        public void RememberCurrentParent()
        {
            if (Application.isPlaying)
                originalParent = transform.parent;
        }

        public void ResetParent(bool worldPositionStays = false)
        {
            if (Application.isPlaying && originalParent != null)
                transform.SetParent(originalParent, false);
        }
    }
}