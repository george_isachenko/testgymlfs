﻿using System.Linq;
using UnityEngine;

namespace View.Actions
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actions/Auto Disable When All Other Objects Disabled")]
    public class AutoDisableWhenAllOtherObjectsDisabled : MonoBehaviour
    {
        public GameObject[] otherObjects;

        void LateUpdate()
        {
            if ((otherObjects != null && !otherObjects.Any(x => x.activeSelf)) && gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }
}