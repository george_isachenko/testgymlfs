﻿using System;
using InspectorHelpers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

namespace View.Actions
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Actions/LeanTween Transform")]
    public class LeanTweenTransform : MonoBehaviour
    {
        public enum Mode
        {
            Move = 0,
            MoveX,
            MoveY,
            MoveZ,
            Rotate,
            RotateX,
            RotateY,
            RotateZ,
            ScaleLocal,
            ScaleLocalX,
            ScaleLocalY,
            ScaleLocalZ,
            MoveLocal,
            MoveLocalX,
            MoveLocalY,
            MoveLocalZ,
            RotateLocal,
            AlphaUI_Image,
            AlphaUI_CanvasGroup,
        }

        public enum RotationMode
        {
            EulerAnglesClosest = 0,
            EulerAnglesLongest,
            QuaternionLerp,
            QuaternionSlerp,
        }

        [Serializable]
        public struct Tween
        {
            public Mode mode;
            [Tooltip("Quaternion rotations valid only in mode 'Rotate' or 'RotateLocal'")]
            public RotationMode rotationMode;
            public Vector3 targetTransform;
            public float time;
            public LeanTweenType loopType;
            public LeanTweenType easeType;
            public float delay;
            public int repeat;
            public bool autoRun;
            public bool pauseOnDisable;
            public bool callOnCompleteWhenCanceled;
            public UnityEvent onComplete;
        }

        struct TweenRuntimeInfo
        {
            public Vector3 originalVector;
            public Quaternion originalQuaternion;
            public bool originalTransformKept;
            public LTDescr tween;
            public Action onCompleteRuntime;
        }

        [ReorderableList]
        public Tween[] tweens;
        TweenRuntimeInfo[] runtimeInfos;

        // Use this for initialization
        void Awake()
        {
//             Debug.LogFormat("Awake (): {0}", GetInstanceID());

            Init();
        }

        void OnDisable()
        {
            Init();

            for (int i = 0; i < tweens.Length; i++)
            {
                if (tweens[i].pauseOnDisable)
                {
                    if (runtimeInfos[i].tween != null)
                        LeanTween.pause(runtimeInfos[i].tween.id);
                }
                else
                {
                    if (runtimeInfos[i].tween != null)
                    {
                        DoCancelTween(ref runtimeInfos[i], tweens[i].callOnCompleteWhenCanceled);

                        DoResetToOriginalTransform (ref tweens[i], ref runtimeInfos[i]);
                    }
                }
            }
        }

        void OnEnable()
        {
            Init();

            for (int i = 0; i < tweens.Length; i++)
            {
                if (tweens[i].pauseOnDisable)
                {
                    if (runtimeInfos[i].tween != null)
                        LeanTween.resume(runtimeInfos[i].tween.id);
                }
                else
                {
                    if (runtimeInfos[i].tween == null && tweens[i].autoRun)
                        DoRunTween(i, ref tweens[i], ref runtimeInfos[i], tweens[i].targetTransform, tweens[i].time);
                }
            }
        }

        public void CancelTween(int tweenIndex = 0)
        {
            if (tweenIndex >= 0 && tweenIndex < tweens.Length)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
            }
        }

        public void RememberCurrentTransform(int tweenIndex = 0)
        {
            if (tweenIndex >= 0 && tweenIndex < tweens.Length)
            {
                Init();
                DoRememberCurrentTransform (ref tweens[tweenIndex], ref runtimeInfos[tweenIndex]);
            }
        }

        public void ResetToOriginalTransform(int tweenIndex = 0)
        {
//             Debug.LogFormat("ResetToOriginalTransform (): {0}", GetInstanceID());
            if (tweenIndex >= 0 && tweenIndex < tweens.Length)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
                DoResetToOriginalTransform (ref tweens[tweenIndex], ref runtimeInfos[tweenIndex]);
            }
        }

        public void RunTween(int tweenIndex = 0)
        {
            if (tweenIndex >= 0 && tweenIndex < tweens.Length && tweens[tweenIndex].time > Mathf.Epsilon)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
                DoRunTween(tweenIndex, ref tweens[tweenIndex], ref runtimeInfos[tweenIndex], tweens[tweenIndex].targetTransform, tweens[tweenIndex].time, null);
            }
        }

        public void RunTween(int tweenIndex = 0, Action onComplete = null)
        {
            if (tweenIndex >= 0 && tweenIndex < tweens.Length && tweens[tweenIndex].time > Mathf.Epsilon)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
                DoRunTween(tweenIndex, ref tweens[tweenIndex], ref runtimeInfos[tweenIndex], tweens[tweenIndex].targetTransform, tweens[tweenIndex].time, onComplete);
            }
        }

        public void RunTween(int tweenIndex, Vector3 customTransform, Action onComplete = null)
        {
            if (tweenIndex >= 0 && tweenIndex < tweens.Length && tweens[tweenIndex].time > Mathf.Epsilon)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
                DoRunTween(tweenIndex, ref tweens[tweenIndex], ref runtimeInfos[tweenIndex], customTransform, tweens[tweenIndex].time, onComplete);
            }
        }

        public void RunTween(int tweenIndex, Vector3 customTransform, float customTime, Action onComplete = null)
        {
            if (customTime > Mathf.Epsilon && tweenIndex >= 0 && tweenIndex < tweens.Length)
            {
                Init();
                DoCancelTween(ref runtimeInfos[tweenIndex], tweens[tweenIndex].callOnCompleteWhenCanceled);
                DoRunTween(tweenIndex, ref tweens[tweenIndex], ref runtimeInfos[tweenIndex], customTransform, customTime, onComplete);
            }
        }

        void Init()
        {
            if (tweens.Length > 0 && runtimeInfos == null)
            {
                runtimeInfos = new TweenRuntimeInfo[tweens.Length];

                for (int i = 0; i < tweens.Length; i++)
                {
                    runtimeInfos[i] = new TweenRuntimeInfo();
                    DoRememberCurrentTransform(ref tweens[i], ref runtimeInfos[i]);
                }
            }
        }

        void DoRememberCurrentTransform(ref Tween tween, ref TweenRuntimeInfo ri)
        {
            switch (tween.mode)
            {
                case Mode.Move:
                case Mode.MoveX:
                case Mode.MoveY:
                case Mode.MoveZ:
                ri.originalVector = transform.position;
                break;

                case Mode.Rotate:
                if (tween.rotationMode == RotationMode.EulerAnglesClosest || tween.rotationMode == RotationMode.EulerAnglesLongest)
                {
                    ri.originalVector = transform.rotation.eulerAngles;
                }
                else
                {
                    ri.originalQuaternion = transform.rotation;
                }
                break;

                case Mode.RotateX:
                case Mode.RotateY:
                case Mode.RotateZ:
                ri.originalVector = transform.rotation.eulerAngles;
                break;

                case Mode.ScaleLocal:
                case Mode.ScaleLocalX:
                case Mode.ScaleLocalY:
                case Mode.ScaleLocalZ:
                ri.originalVector = transform.localScale;
                break;

                case Mode.MoveLocal:
                case Mode.MoveLocalX:
                case Mode.MoveLocalY:
                case Mode.MoveLocalZ:
                ri.originalVector = transform.localPosition;
                break;

                case Mode.RotateLocal:
                if (tween.rotationMode == RotationMode.EulerAnglesClosest || tween.rotationMode == RotationMode.EulerAnglesLongest)
                {
                    ri.originalVector = transform.localRotation.eulerAngles;
                }
                else
                {
                    ri.originalQuaternion = transform.localRotation;
                }
                break;

                case Mode.AlphaUI_Image:
                {
                    var image = gameObject.GetComponent<Image>();
                    if (image != null)
                    {
                        ri.originalVector = new Vector3(image.color.a, 0, 0);
                    }
                }; break;

                case Mode.AlphaUI_CanvasGroup:
                {
                    var canvasGroupt = gameObject.GetComponent<CanvasGroup>();
                    if (canvasGroupt != null)
                    {
                        ri.originalVector = new Vector3(canvasGroupt.alpha, 0, 0);
                    }
                }; break;
            }
            ri.originalTransformKept = true;
        }

        void DoCancelTween(ref TweenRuntimeInfo ri, bool callOnCompleteWhenCanceled)
        {
            if (ri.tween != null)
            {
                var t = ri.tween;
                LeanTween.cancel(t.id, callOnCompleteWhenCanceled);

                ri.tween = null;
                ri.onCompleteRuntime = null;
            }
        }

        void DoResetToOriginalTransform(ref Tween tween, ref TweenRuntimeInfo ri)
        {
            if (ri.originalTransformKept)
            {
                switch (tween.mode)
                {
                    case Mode.Move:
                    transform.position = ri.originalVector;
                    break;

                    case Mode.MoveX:
                    {
                        var position = transform.position;
                        position.x = ri.originalVector.x;
                        transform.position = position;
                    }
                    break;

                    case Mode.MoveY:
                    {
                        var position = transform.position;
                        position.y = ri.originalVector.y;
                        transform.position = position;
                    }
                    break;

                    case Mode.MoveZ:
                    {
                        var position = transform.position;
                        position.z = ri.originalVector.z;
                        transform.position = position;
                    }
                    break;

                    case Mode.Rotate:
                    if (tween.rotationMode == RotationMode.EulerAnglesClosest || tween.rotationMode == RotationMode.EulerAnglesLongest)
                    {
                        transform.rotation = Quaternion.Euler(ri.originalVector);
                    }
                    else
                    {
                        transform.rotation = ri.originalQuaternion;
                    }
                    break;

                    case Mode.RotateX:
                    {
                        var rotation = transform.rotation.eulerAngles;
                        rotation.x = ri.originalVector.x;
                        transform.rotation = Quaternion.Euler(rotation);
                    }
                    break;

                    case Mode.RotateY:
                    {
                        var rotation = transform.rotation.eulerAngles;
                        rotation.y = ri.originalVector.y;
                        transform.rotation = Quaternion.Euler(rotation);
                    }
                    break;

                    case Mode.RotateZ:
                    {
                        var rotation = transform.rotation.eulerAngles;
                        rotation.z = ri.originalVector.z;
                        transform.rotation = Quaternion.Euler(rotation);
                    }
                    break;

                    case Mode.ScaleLocal:
                    transform.localScale = ri.originalVector;
                    break;

                    case Mode.ScaleLocalX:
                    {
                        var scale = transform.localScale;
                        scale.x = ri.originalVector.x;
                        transform.localScale = scale;
                    }
                    break;

                    case Mode.ScaleLocalY:
                    {
                        var scale = transform.localScale;
                        scale.y = ri.originalVector.y;
                        transform.localScale = scale;
                    }
                    break;

                    case Mode.ScaleLocalZ:
                    {
                        var scale = transform.localScale;
                        scale.z = ri.originalVector.z;
                        transform.localScale = scale;
                    }
                    break;

                    case Mode.MoveLocal:
                    transform.localPosition = ri.originalVector;
                    break;

                    case Mode.MoveLocalX:
                    {
                        var localPosition = transform.localPosition;
                        localPosition.x = ri.originalVector.x;
                        transform.localPosition = localPosition;
                    }
                    break;

                    case Mode.MoveLocalY:
                    {
                        var localPosition = transform.localPosition;
                        localPosition.y = ri.originalVector.y;
                        transform.localPosition = localPosition;
                    }
                    break;

                    case Mode.MoveLocalZ:
                    {
                        var localPosition = transform.localPosition;
                        localPosition.z = ri.originalVector.z;
                        transform.localPosition = localPosition;
                    }
                    break;

                    case Mode.RotateLocal:
                    {
                        if (tween.rotationMode == RotationMode.EulerAnglesClosest || tween.rotationMode == RotationMode.EulerAnglesLongest)
                        {
                            transform.localRotation = Quaternion.Euler(ri.originalVector);
                        }
                        else
                        {
                            transform.localRotation = ri.originalQuaternion;
                        }
                    }; break;

                    case Mode.AlphaUI_Image:
                    {
                        var image = gameObject.GetComponent<Image>();
                        if (image != null)
                        {
                            var color = image.color;
                            color.a = ri.originalVector.x;
                            image.color = color;
                        }
                    }; break;

                    case Mode.AlphaUI_CanvasGroup:
                    {
                        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
                        if (canvasGroup != null)
                        {
                            canvasGroup.alpha = ri.originalVector.x;
                        }
                    }; break;
                }
            }
        }

        void DoRunTween (int tweenIdx, ref Tween tween, ref TweenRuntimeInfo ri, Vector3 to, float time, Action onComplete = null)
        {
            if (!ri.originalTransformKept)
                DoRememberCurrentTransform(ref tween, ref ri);

            LTDescr descr = null;

            if ((tween.mode == Mode.Rotate || tween.mode == Mode.RotateLocal) &&
                (tween.rotationMode == RotationMode.QuaternionLerp || tween.rotationMode == RotationMode.QuaternionSlerp))
            {
                Quaternion origRotation = (tween.mode == Mode.Rotate) ? transform.rotation : transform.localRotation;
                Quaternion targetRotationQuaternion = Quaternion.Euler(tween.targetTransform);

                descr = LeanTween.value (gameObject, 0.0f, 1.0f, time);
                descr.setOnUpdate((value) =>
                {
                    var rotation = (tweens[tweenIdx].rotationMode == RotationMode.QuaternionLerp)
                        ? Quaternion.LerpUnclamped(origRotation, targetRotationQuaternion, value) 
                        : Quaternion.SlerpUnclamped(origRotation, targetRotationQuaternion, value);

                    if (tweens[tweenIdx].mode == Mode.Rotate)
                    {
                        transform.rotation = rotation;
                    }
                    else if (tweens[tweenIdx].mode == Mode.RotateLocal)
                    {
                        transform.localRotation = rotation;
                    }
                });
            }
            else if (tween.rotationMode == RotationMode.EulerAnglesLongest &&
                (tween.mode == Mode.Rotate || tween.mode == Mode.RotateLocal || tween.mode == Mode.RotateX || tween.mode == Mode.RotateY || tween.mode == Mode.RotateZ))
            {
                Vector3 originalVector = (tween.mode == Mode.Rotate) ? transform.rotation.eulerAngles : transform.localRotation.eulerAngles;
                Vector3 targetTransform = tween.targetTransform;

                descr = LeanTween.value (gameObject, 0.0f, 1.0f, time);
                descr.setOnUpdate((value) =>
                {
                    switch (tweens[tweenIdx].mode)
                    {
                        case Mode.Rotate:
                        {
                            Vector3 rotation = new Vector3(Mathf.LerpAngle(originalVector.x, targetTransform.x, value)
                                                         , Mathf.LerpAngle(originalVector.y, targetTransform.y, value)
                                                         , Mathf.LerpAngle(originalVector.z, targetTransform.z, value));
                            transform.rotation = Quaternion.Euler(rotation);
                        }; break;


                        case Mode.RotateLocal:
                        {
                            Vector3 rotation = new Vector3(Mathf.LerpAngle(originalVector.x, targetTransform.x, value)
                                                         , Mathf.LerpAngle(originalVector.y, targetTransform.y, value)
                                                         , Mathf.LerpAngle(originalVector.z, targetTransform.z, value));
                            transform.localRotation = Quaternion.Euler(rotation);
                        }; break;

                        case Mode.RotateX:
                        {
                            Vector3 rotation = transform.rotation.eulerAngles;
                            rotation.x = Mathf.LerpUnclamped(originalVector.x, targetTransform.x, value) % 360.0f;
                            transform.rotation = Quaternion.Euler(rotation);
                        }; break;

                        case Mode.RotateY:
                        {
                            Vector3 rotation = transform.rotation.eulerAngles;
                            rotation.y = Mathf.LerpUnclamped(originalVector.y, targetTransform.y, value) % 360.0f;
                            transform.rotation = Quaternion.Euler(rotation);
                        }; break;

                        case Mode.RotateZ:
                        {
                            Vector3 rotation = transform.rotation.eulerAngles;
                            rotation.z = Mathf.LerpUnclamped(originalVector.z, targetTransform.z, value) % 360.0f;
                            transform.rotation = Quaternion.Euler(rotation);
                        }; break;
                    }
                });
            }
            else
            {
                switch (tween.mode)
                {
                    case Mode.Move:
                    descr = LeanTween.move (gameObject, to, time);
                    break;

                    case Mode.MoveX:
                    descr = LeanTween.moveX (gameObject, to.x, time);
                    break;

                    case Mode.MoveY:
                    descr = LeanTween.moveY (gameObject, to.y, time);
                    break;

                    case Mode.MoveZ:
                    descr = LeanTween.moveZ (gameObject, to.z, time);
                    break;

                    case Mode.Rotate:
                    descr = LeanTween.rotate (gameObject, to, time);
                    break;

                    case Mode.RotateX:
                    descr = LeanTween.rotateX (gameObject, to.x, time);
                    break;

                    case Mode.RotateY:
                    descr = LeanTween.rotateY (gameObject, to.y, time);
                    break;

                    case Mode.RotateZ:
                    descr = LeanTween.rotateZ (gameObject, to.z, time);
                    break;

                    case Mode.ScaleLocal:
                    descr = LeanTween.scale (gameObject, to, time);
                    break;

                    case Mode.ScaleLocalX:
                    descr = LeanTween.scaleX (gameObject, to.x, time);
                    break;

                    case Mode.ScaleLocalY:
                    descr = LeanTween.scaleY (gameObject, to.y, time);
                    break;

                    case Mode.ScaleLocalZ:
                    descr = LeanTween.scaleZ (gameObject, to.z, time);
                    break;

                    case Mode.MoveLocal:
                    descr = LeanTween.moveLocal (gameObject, to, time);
                    break;

                    case Mode.MoveLocalX:
                    descr = LeanTween.moveLocalX (gameObject, to.x, time);
                    break;

                    case Mode.MoveLocalY:
                    descr = LeanTween.moveLocalY (gameObject, to.y, time);
                    break;

                    case Mode.MoveLocalZ:
                    descr = LeanTween.moveLocalZ (gameObject, to.z, time);
                    break;

                    case Mode.RotateLocal:
                    descr = LeanTween.rotateLocal (gameObject, to, time);
                    break;

                    case Mode.AlphaUI_Image:
                    {
                        var rectTransform = gameObject.GetComponent<RectTransform>();
                        if (rectTransform != null)
                        {
                            descr = LeanTween.alpha (rectTransform, to.x, time);
                        }
                    }; break;

                    case Mode.AlphaUI_CanvasGroup:
                    {
                        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
                        if (canvasGroup != null)
                        {
                            descr = LeanTween.alphaCanvas (canvasGroup, to.x, time);
                        }
                    }; break;
                }
            }

            if (descr != null)
            {
                if (tween.delay > 0)
                    descr.setDelay(tween.delay);

                if (tween.loopType != LeanTweenType.notUsed)
                    descr.setLoopType(tween.loopType);
                if (tween.easeType != LeanTweenType.notUsed)
                    descr.setEase(tween.easeType);
                
                descr.setRepeat(tween.repeat)
                    .setOnComplete(() =>
                    {
                        if (runtimeInfos[tweenIdx].tween != null && !(runtimeInfos[tweenIdx].tween.loopCount < 0 && runtimeInfos[tweenIdx].tween.type == TweenAction.CALLBACK) )
                        {
                            runtimeInfos[tweenIdx].tween = null;
                            var onCompleteRuntime = runtimeInfos[tweenIdx].onCompleteRuntime;
                            runtimeInfos[tweenIdx].onCompleteRuntime = null;

                            if (tweens[tweenIdx].onComplete != null)
                            {
                                tweens[tweenIdx].onComplete.Invoke();
                            }

                            if (onCompleteRuntime != null)
                            {
                                onCompleteRuntime.Invoke();
                            }
                        }
                    })
                    .setOnCompleteOnRepeat(false);

                ri.onCompleteRuntime = onComplete;
            }

            Assert.IsNull(ri.tween);
            ri.tween = descr;
        }
    }
}