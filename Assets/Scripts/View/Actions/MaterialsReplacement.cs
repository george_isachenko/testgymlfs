﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using View.EditorHelpers;
using CustomAssets;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace View.Actions
{
    [AddComponentMenu("Kingdom/View/Actions/Materials Replacement")]
    [DisallowMultipleComponent]
    public class MaterialsReplacement : MonoBehaviour
    {
        #region Public (Inspector) fields.
        [Tooltip("Policy that will be used when no explicit policy passed from code.")]
        public MaterialsReplacementPolicy customPolicy;
        #endregion

        #region Private data.
        private string currentPolicyTagName;
        private Dictionary<int, Material[]> originalSharedMaterialsCache = new Dictionary<int, Material[]>(64); // Instance ID to Material[] map.
        #endregion

        #region Public API.
        public void ReplaceMaterials(string policyTagName, MaterialsReplacementPolicy policy = null)
        {
            var policyUsed = (policy != null) ? policy : this.customPolicy;

            if (policyUsed != null &&
                policyUsed.array != null &&
                policyUsed.array.Length > 0 &&
                policyTagName != string.Empty &&
                policyTagName != currentPolicyTagName)
            {
                var policyDataIdx = Array.FindIndex(policyUsed.array, pData => pData.name == policyTagName);
                if (policyDataIdx >= 0)
                {
                    var newSubsts = policyUsed.array[policyDataIdx].substitutions;

                    if (currentPolicyTagName != null && currentPolicyTagName != string.Empty)
                    {
                        RevertMaterials();
                    }

                    ReplaceMaterialsOnRenderers(gameObject.GetComponentsInChildren<Renderer>(true), newSubsts);
                    currentPolicyTagName = policyTagName;
                }
            }
        }

        public void ResetBackMaterials()
        {
            if (currentPolicyTagName != null && currentPolicyTagName != string.Empty)
            {
                RevertMaterials();
                currentPolicyTagName = string.Empty;
            }
        }

        public void ResetBackMaterials(string policyTagName)
        {
            if (currentPolicyTagName != null && currentPolicyTagName != string.Empty && currentPolicyTagName == policyTagName)
            {
                RevertMaterials();
                currentPolicyTagName = string.Empty;
            }
        }
        #endregion

        #region Private functions.
        private void ReplaceMaterialsOnRenderers (Renderer[] renderers, MaterialsReplacementPolicy.PolicyData.Substitution[] substitutions)
        {
            if (Application.isPlaying)
            {
                if (renderers != null && renderers.Length > 0)
                {
                    foreach (var renderer in renderers)
                    {
                        Material[] replacedSharedMaterials = null;

                        for (int i = 0; i < renderer.sharedMaterials.Length; i++)
                        {
                            // Debug.LogFormat("Renderer {0} Material: {1} - {2}", i, renderer.sharedMaterials[i].name, renderer.sharedMaterials[i].GetInstanceID());
                            var originalMat = renderer.sharedMaterials[i];

                            if (originalMat != null)
                            {
                                var substIndex = Array.FindIndex (substitutions
                                    , substitution =>
                                        (substitution.originalShader != null && substitution.substituteShader != null && substitution.originalShader.GetInstanceID() == originalMat.shader.GetInstanceID()) ||
                                        (substitution.original != null && substitution.substitute != null && substitution.original.GetInstanceID() == originalMat.GetInstanceID()));

                                if (substIndex >= 0)
                                {
                                    var substitution = substitutions[substIndex];

                                    // Debug.LogFormat("Substituting Material: {0} - {1} to {2} - {3}", renderer.sharedMaterials[i].name, renderer.sharedMaterials[i].GetInstanceID(), substitutions[substIndex].substitute.name, substitutions[substIndex].substitute.GetInstanceID() );

                                    if (replacedSharedMaterials == null)
                                        replacedSharedMaterials = (Material[])renderer.sharedMaterials.Clone();

                                    Material newMaterial = null;

                                    if (substitution.originalShader != null && substitution.substituteShader != null)
                                    {
                                        if (substitution.substitute != null)
                                        {   // Use material and shader from substitution definition but copy shader properties over from original material.
                                            newMaterial = new Material(substitution.substitute);
                                            newMaterial.shader = substitution.substituteShader;
                                            newMaterial.CopyPropertiesFromMaterial(originalMat);

                                            if (substitution.propertiesFromSubstitute != null && substitution.propertiesFromSubstitute.Length > 0)
                                            {
                                                CopyProperties(substitution.substitute, newMaterial, ref substitution.propertiesFromSubstitute);
                                            }

                                            if (substitution.useRenderQueueFromSubstitute)
                                            {
                                                newMaterial.renderQueue = substitution.substitute.renderQueue;
                                            }
                                        }
                                        else
                                        {   // Use a copy of original material and replace only shader.
                                            newMaterial = new Material(originalMat);
                                            newMaterial.shader = substitution.substituteShader;
                                        }
                                    }
                                    else
                                    {
                                        newMaterial = substitution.substitute;
                                    }

                                    replacedSharedMaterials[i] = newMaterial;
                                }
                            }
                        }

                        if (replacedSharedMaterials != null)
                        {
                            Assert.IsFalse(originalSharedMaterialsCache.ContainsKey(renderer.GetInstanceID()));

                            originalSharedMaterialsCache.Add(renderer.GetInstanceID(), renderer.sharedMaterials);


                            // foreach (var m in renderer.sharedMaterials)
                            // {
                            //     Debug.LogFormat("Original shared mats: {0} - {1}", m.name, m.GetInstanceID());
                            // }

                            renderer.materials = replacedSharedMaterials;

                            // foreach (var m in renderer.sharedMaterials)
                            // {
                            //     Debug.LogFormat("New shared mats: {0} - {1}", m.name, m.GetInstanceID());
                            // }
                        }
                    }
                }
            }
        }

        private void RevertMaterials()
        {
            if (Application.isPlaying)
            {
                if (originalSharedMaterialsCache.Count > 0)
                {
                    RevertMaterialsOnRenderers(gameObject.GetComponentsInChildren<Renderer>(true));

                    originalSharedMaterialsCache.Clear();
                }
            }
        }

        private void RevertMaterialsOnRenderers (Renderer[] renderers)
        {
            if (renderers != null && renderers.Length > 0)
            {
                foreach (var renderer in renderers)
                {
                    Material[] originalSharedMaterials;
                    if (originalSharedMaterialsCache.TryGetValue(renderer.GetInstanceID(), out originalSharedMaterials))
                    {
                        renderer.sharedMaterials = originalSharedMaterials;

                        // foreach (var m in renderer.sharedMaterials)
                        // {
                        //     Debug.LogFormat("Reverted shared mats: {0} - {1}", m.name, m.GetInstanceID());
                        // }
                    }
                }
            }
        }

        private static void CopyProperties
            (Material from, Material to, ref MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode[] properties)
        {
            Assert.IsNotNull(properties);
            Assert.IsNotNull(from);
            Assert.IsNotNull(to);

            foreach (var prop in properties)
            {
                var hasFrom = from.HasProperty(prop.name);

                if (hasFrom)
                {
                    switch (prop.type)
                    {
                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Color:
                            to.SetColor(prop.name, from.GetColor(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.ColorArray:
                            to.SetColorArray(prop.name, from.GetColorArray(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Float:
                            to.SetFloat(prop.name, from.GetFloat(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.FloatArray:
                            to.SetFloatArray(prop.name, from.GetFloatArray(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Int:
                            to.SetInt(prop.name, from.GetInt(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Matrix:
                            to.SetMatrix(prop.name, from.GetMatrix(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.MatrixArray:
                            to.SetMatrixArray(prop.name, from.GetMatrixArray(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Texture:
                            to.SetTexture(prop.name, from.GetTexture(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.TextureOffset:
                            to.SetTextureOffset(prop.name, from.GetTextureOffset(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.TextureScale:
                            to.SetTextureScale(prop.name, from.GetTextureScale(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.Vector:
                            to.SetVector(prop.name, from.GetVector(prop.name));
                            break;

                        case MaterialsReplacementPolicy.PolicyData.Substitution.PropertyMode.Type.VectorArray:
                            to.SetVectorArray(prop.name, from.GetVectorArray(prop.name));
                            break;
                    }
                }
                else
                {
                    Debug.LogWarningFormat("Materials Replacement: Source material missing material property named '{0}', property is ignored."
                        , prop.name);
                }
            }
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (currentPolicyTagName != null && currentPolicyTagName != string.Empty)
            {
                Handles.Label(transform.position, string.Format("{0}\nPolicy: {1}", name, currentPolicyTagName), EditorGizmoHelper.instance.GetOverheadLabelStyle());
            }
        }
#endif

        #endregion
    }
}