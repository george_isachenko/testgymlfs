﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace View.Actions
{
    [AddComponentMenu("Kingdom/View/Actions/Key Input Handler")]
    public class KeyInputHandler : MonoBehaviour
    {
        public KeyCode[] keyCodes;
        public UnityEvent onKeyHeld;
        public UnityEvent onKeyDown;
        public UnityEvent onKeyUp;

        // Use this for initialization
        void Start()
        {
            // Nothing to handle.
            if (!IsKeyListValid() ||
                (onKeyHeld == null && onKeyDown == null && onKeyUp == null))
            {
                enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (keyCodes != null && keyCodes.Length > 0)
            {
                if (onKeyHeld != null)
                {
                    foreach (var keyCode in keyCodes)
                    {
                        if (Input.GetKey(keyCode))
                        {
                            onKeyHeld.Invoke();
                            break;
                        }
                    }
                }

                if (onKeyDown != null)
                {
                    foreach (var keyCode in keyCodes)
                    {
                        if (Input.GetKeyDown(keyCode))
                        {
                            onKeyDown.Invoke();
                            break;
                        }
                    }
                }

                if (onKeyUp != null)
                {
                    foreach (var keyCode in keyCodes)
                    {
                        if (Input.GetKeyUp(keyCode))
                        {
                            onKeyUp.Invoke();
                            break;
                        }
                    }
                }
            }
        }

        bool IsKeyListValid()
        {
            return (keyCodes != null && keyCodes.Length > 0 && Array.Exists(keyCodes, i => (i != KeyCode.None)));
        }
    }
}