﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

namespace View
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Kingdom/View/Animated Counter")]
    public class AnimatedCounter : MonoBehaviour
    {
        public UnityEvent onSetValue;
        public UnityEvent onIncreaseStep;
        public UnityEvent onDecreaseStep;

        Button _button;
        Transform imgPlus;
        Transform imgIcon;
        Text text;
        int currentValue = int.MinValue;
        int targetValue = int.MinValue;
        int animStepCount = 1;

        public Button button
        {
            get
            {
                Init();
                return _button;
            }
        }

        public bool enableButton
        {
            set
            {
                Init();
                if (_button != null)
                    _button.enabled = value;

                if (imgPlus != null)
                    imgPlus.gameObject.SetActive(value);
            }
        }

        public int value
        {
            set
            {
                Init();
                if (targetValue != value)
                {
                    targetValue = value;

                    if (onSetValue != null)
                    {
                        onSetValue.Invoke();
                    }

                    if (currentValue != int.MinValue)
                    {
                        var absDelta = Mathf.Abs(targetValue - currentValue);
                        animStepCount = Mathf.Max(1, (int)(Mathf.Sqrt(absDelta) * Mathf.Log10(absDelta)));
                        Animate();
                    }
                    else
                    {
                        ResetAnim();
                        FormatCurrentValue(targetValue);
                    }
                }
            }
        }

        void Awake()
        {
            Init();
        }

        void Init()
        {
            if (_button == null)
                _button = GetComponent<Button>();
            if (imgPlus ==  null)
                imgPlus = transform.Find("ImgPlus");
            if (imgIcon == null)
                imgIcon = transform.Find("ImgIcon");
            if (text == null)
                text = transform.GetComponentInChild<Text>("Text");
        }

        void Animate()
        {
            ResetAnim();
            if (targetValue != currentValue &&
                imgIcon != null &&
                animStepCount > 0)
            {
                if (targetValue > currentValue)
                {
                    if (onIncreaseStep != null)
                        onIncreaseStep.Invoke();
                }
                else
                {
                    if (onDecreaseStep != null)
                        onDecreaseStep.Invoke();
                }
            }
            else
            {
                FormatCurrentValue(targetValue);
            }
        }

        void ResetAnim()
        {
            if (imgIcon != null)
            {
                imgIcon.transform.localScale = Vector3.one;
            }
        }

        public void AnimStepComplete()
        {
            //tweenId = 0;

            int value;
            if (targetValue > currentValue)
            {
                if (targetValue - currentValue > animStepCount)
                    value = currentValue + animStepCount;
                else
                    value = targetValue;
            }
            else
            {
                if (currentValue - targetValue > animStepCount)
                    value = currentValue - animStepCount;
                else
                    value = targetValue;
            }

            FormatCurrentValue(value);
            Animate();
        }

        void FormatCurrentValue(int value)
        {
            if (text != null)
            {
                text.text = PrintHelper.FormatCurrencyAmount(value);
                currentValue = value;
            }
        }
    }
}