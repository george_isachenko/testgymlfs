﻿using UnityEngine;

namespace View.CameraHelpers
{
    [AddComponentMenu("Kingdom/View/Camera/Extended Flycam")]
    [RequireComponent(typeof(Camera))]
    [DisallowMultipleComponent]
    public class ExtendedFlycam : MonoBehaviour
    {
        /*
        EXTENDED FLYCAM
            Desi Quintans (CowfaceGames.com), 17 August 2012.
            Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.

        LICENSE
            Free as in speech, and free as in beer.

        FEATURES
            WASD/Arrows:    Movement
                      Q:    Climb
                      E:    Drop
                          Shift:    Move faster
                        Control:    Move slower
                            End:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
        */

        //public bool movementEnabled { get; set; }

        public float cameraSensitivity = 90;
        public float climbSpeed = 4;
        public float normalMoveSpeed = 10;
        public float slowMoveFactor = 0.25f;
        public float fastMoveFactor = 3;

        private float rotationX = 0.0f;
        private float rotationY = 0.0f;

        private bool developerUpdateEnabled = true;

        private void Start()
        {
            //Cursor.lockState = CursorLockMode.Locked;

            developerUpdateEnabled = false;
            Vector3 currentEuler = transform.localRotation.eulerAngles;

            /*
                    print (currentEuler.x);
                    print (currentEuler.y);
                    print (currentEuler.z);
            */

            rotationX = currentEuler.y;
            rotationY = Mathf.Clamp(currentEuler.x > 180 ? 360 - currentEuler.x : -currentEuler.x, -90, 90);
        }

        private void Update()
        {
            if (developerUpdateEnabled)
            {
                rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
                rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
                rotationY = Mathf.Clamp(rotationY, -90, 90);

                transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
                transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                    transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                {
                    transform.position += transform.forward * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
                    transform.position += transform.right * (normalMoveSpeed * slowMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
                }
                else {
                    transform.position += transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                    transform.position += transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    transform.position += transform.up * climbSpeed * Time.deltaTime;
                }
                if (Input.GetKey(KeyCode.E))
                {
                    transform.position -= transform.up * climbSpeed * Time.deltaTime;
                }
            }

            if (Input.GetKeyDown(KeyCode.End))
            {
                Cursor.lockState = (Cursor.lockState == CursorLockMode.Locked)
                    ? CursorLockMode.None
                    : CursorLockMode.Locked;

                // Cursor.visible = (Cursor.lockState == CursorLockMode.None);
            }

            if (Input.GetKeyDown(KeyCode.ScrollLock) || Input.GetKeyDown(KeyCode.F12))
            {
                developerUpdateEnabled = !developerUpdateEnabled;
                Cursor.lockState = developerUpdateEnabled
                    ? CursorLockMode.Locked
                    : CursorLockMode.None;

                Cursor.visible = true; // (Cursor.lockState == CursorLockMode.None);
            }
        }

    }
}