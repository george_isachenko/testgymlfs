﻿#define SHOW_CAM_LOOKAT

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using System;

namespace View.CameraHelpers
{
    public delegate void OnCameraMoveEvent(float moveDist, bool overThreshold);
    public delegate void OnCameraZoomEvent(float distToFloor);

    public class CameraController : MonoBehaviour
    {
        public static CameraController   instance;
        
        #region Constants.
        private static readonly int cMaxTouches = 2;
#if UNITY_EDITOR
        private static readonly Color floorHighlightColor = new Color(0.695f, 0.132f, 0.132f, 0.75f);
#endif
        #endregion

        #region Public types.
        [Serializable]
        public struct CameraMove
        {
            public float touchMoveMultiplier;
            public float moveThreshold;
            public float returnSpeed;
            public float autoMoveMultiplier;
            public float roomGap;
            public float roomSizePlus;
            public float inertion;
        }

        [Serializable]
        public struct CameraZoom
        {
            public float zoomIn;
            public float zoomOut;
            public float zoomMultiplier;
            [Range(0.0f, 1.0f)]
            public float zoomInterpolator;
            public float autoZoomSpeed;
            public float inertion;
        }

        [Serializable]
        public struct CameraRotation
        {
            public float rotationSpeed;
            [Range(0.0f, 359f)]
            public float rotationAngle;
            public float autoRotationMultiplier;
            public float inertion;
        }

        [Serializable]
        public struct CameraTarget
        {
            public bool showTarget;
            public float targetShift;
            public Vector2 targetCoords;
        }

        public struct TStates
        {
            public bool dragEquip { get; set; }
            public bool windowOpened { get; set; }
            public bool expand { get; set; }
            public bool stickToPos { get; set; }
            public bool interruptAutomove { get; set; }

            public void Reset()
            {
                dragEquip = false;
                windowOpened = false;
                expand = false;
                stickToPos = false;
                interruptAutomove = true;
            }
        }

        #endregion

        #region Private types.
        struct TouchData
        {
            public Vector2 position;
            public int id; //--- unique Id
        }
        #endregion

        #region Events.
        public event OnCameraMoveEvent onCameraMoveEvent;
        public event Action onCameraDualTouchEvent;
        public event OnCameraZoomEvent onCameraZoomEvent;
        #endregion

        #region Public (Inspector) fields.
        public LayerMask ignoreMovementOverLayers;

        public CameraMove cameraMove;
        public CameraZoom cameraZoom;
        public CameraRotation cameraRotation;
        public CameraTarget cameraTarget;
        public float centrationRadius;
        public Vector2 floorPosition;
        public Vector2 floorSize;
        #endregion

        #region Public fields.
        public TStates states;
        public event Action onMoveTransformComplete;
        public event Action onTransformUnstashComplete;
        #endregion

        #region Properties
        public float moveDistance { get; private set; }
        public bool  mainCameraOn   { set { SetMainCamera(value); } }
        #endregion

        #region Private data.

        private Vector3 camLookPoint;

        private bool wasTouch = false;
        private bool wasDualTouch = false;
        private bool wasRotation = false;
        private Vector3 wasGroundTouch = Vector3.zero;
        private Vector3 totalMove = Vector3.zero;
        private Vector3 inertionMove = Vector3.zero;

        private TouchData[] touches = new TouchData[cMaxTouches];

        private float curZoom = 0.0f;
        private float curZoomInterpolator;

        public bool isAutoMove { get; private set; }
        public bool isCameraStashed     { get { return stashPosition != Vector3.zero; } }
        private Vector2 targetAutoMove;
        private bool isInertMove = false;

        private bool isAutoRotate = false;
        private float targetAutoRotateAngle;
        private float inertionRotate;
        private bool isInertRot = false;

        private bool isAutoZoom = false;
        private float targetAutoZoomValue;
        private float inertionZoom;
        private bool isInertZoom = false;

        private float wasRotationAngle;
        private float curRotationAngle = 0.0f;

        private bool ignoreCameraMovement = false;
        List<RaycastResult> ignoreCameraMovementHitsCache = new List<RaycastResult>(8);

        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);

        private Camera selfCamera;
        private EventSystem mainEventSystem;
        //private Collider selfCollider;
        //private Vector2? homePosition;

        Action<bool> onCompleteMove;

        private double defaultAzimut;
        private double wasAzimut;
        private Vector3 defaultAzimutAxis;

#if (SHOW_CAM_LOOKAT)
        private GameObject targetObj = null; // --- test
#endif

        Vector3 stashPosition;
        Vector3 stashRotation;
        float   stashTimeOut;
        #endregion

        #region Unity API.
        void Awake()
        {
            selfCamera = GetComponent<Camera>();
            instance = this;
            isAutoMove = false;
            states.Reset();
        }

        void OnDestroy ()
        {
            instance = null;
        }

        void Start()
        {
            mainEventSystem = EventSystem.current;

            if (mainEventSystem == null)
            {
                Debug.LogWarning("CameraController requires some EventSystem in Scene to communicate with.");
            }
 
            curZoomInterpolator = cameraZoom.zoomInterpolator = 0.5f;

            wasRotationAngle = cameraRotation.rotationAngle = 0.0f;

#if (SHOW_CAM_LOOKAT)
            //--- test
            if (cameraTarget.showTarget)
            {
                targetObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                targetObj.GetComponent<MeshRenderer>().enabled = false;
                //targetObj.GetComponent<Collider>().enabled = false;
                Destroy(targetObj.GetComponent<Collider>());
                targetObj.transform.SetParent(transform, true);
                targetObj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                targetObj.layer = gameObject.layer;
            }
#endif

            CalcLookAtPoint();
            CalcAzimut();
           
        }

        void Update()
        {
            if (IsCanUpdateTouchCamera())
            {
                UpdateCameraTouch();
            }
            else
            {
                Flush();
            }

            if (isAutoMove)
                UpdateAutoMove();
            else if (isInertMove)
                InertialMove();

            if (isAutoRotate)
                UpdateAutoRotate();
            else if (isInertRot)
                InertialRotate(); 

            if (isAutoZoom)
                UpdateAutoZoom();
            else if (isInertZoom)
                InertialZoom(); 

#if (SHOW_CAM_LOOKAT)
            //--- test
            if (targetObj && cameraTarget.showTarget)
                targetObj.transform.position = camLookPoint;
#endif
        }

        public void SetMainCamera(bool value)
        {
            if (value)
            {
                selfCamera.tag = "MainCamera";
            }
            else
            {
                selfCamera.tag = "Untagged";
            }
        }

        int cullingMaskBackup;
        public void ClearCullingMask()
        {
            cullingMaskBackup = selfCamera.cullingMask;
            selfCamera.cullingMask = 0;
        }

        public void ResotreCullingMask()
        {
            selfCamera.cullingMask = cullingMaskBackup;
        }

        public void SetCameraTransform(Transform tr)
        {
            selfCamera.transform.position = tr.position;
            selfCamera.transform.rotation = tr.rotation;

            CalcLookAtPoint();
            CalcAzimut();
        }

        bool stashMovementOn = false;
        public void StashCameraTransformAndMoveTo(Vector3 pos, Vector3 rotEuler, float time, float timeOut)
        {
            if (isAutoMove)
                return;
            
            if (stashPosition == Vector3.zero)
            {
                stashPosition = selfCamera.transform.position;
                stashRotation = selfCamera.transform.transform.rotation.eulerAngles;
                stashTimeOut = timeOut;

                LeanTween.move(gameObject, pos, time).setOnComplete(OnMoveComplete);
                LeanTween.rotate(gameObject, rotEuler, time);

                stashMovementOn = true;
            }
        }

        void OnMoveComplete()
        {
            onMoveTransformComplete?.Invoke(); 
            stashMovementOn = false;
        }

        public void UnstashCameraTransform()
        {
            if (stashMovementOn)
                return;
            
            if (stashPosition != Vector3.zero)
            {
                LeanTween.move(gameObject, stashPosition, stashTimeOut).setOnComplete(()=>onTransformUnstashComplete?.Invoke());
                LeanTween.rotate(gameObject, stashRotation, stashTimeOut);

                stashPosition = Vector3.zero;
                stashRotation = Vector3.zero;
            }
            else
            {
                Debug.LogError("UnstashCameraTransform : stashPosition == Vector3.zero");
            }
        }

        void OnApplicationPause (bool pauseStatus)
        {
            /*
            if (!pauseStatus)
            {
                if (homePosition != null && IsCanUpdateTouchCamera())
                {
                    AutoMoveTo(homePosition.Value.x, homePosition.Value.y);
                }
            }
            */
        }
        #endregion

        #region Unity Editor API.
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (floorSize.sqrMagnitude > Mathf.Epsilon && UnityEditor.Selection.Contains(gameObject))
            {
                Gizmos.color = floorHighlightColor;
                Gizmos.DrawWireCube(new Vector3(floorPosition.x, 0, floorPosition.y), new Vector3(floorSize.x, 0.1f, floorSize.y));
            }
        }
#endif
        #endregion

        #region Public API.
        public static float GetDpiCoef()
        {
            float result = 1.0f;

            //--- 264 - dpi for iPad3

            if (Screen.dpi > 0f)
                result = 1.0f / Screen.dpi;

            return result;
        }

        public void UpdateCurrentRoom(Vector2 floorCenterWorld, Vector2 floorSize)
        {
            floorPosition = floorCenterWorld;
            this.floorSize = floorSize;
        }

        void CalcAzimut()
        {
            //--- detect angle to target
            defaultAzimut = GetAzimut();
            wasAzimut = defaultAzimut;
            Vector3 floorToCamVector = transform.position - camLookPoint;
            Vector3 floorVector = floorToCamVector; floorVector.y = 0.0f;
            Vector3 tAxis = Vector3.Cross(floorToCamVector, floorVector);
            defaultAzimutAxis = -tAxis.normalized;
        }

        void CalcLookAtPoint()
        {
            Vector3 camDir = transform.forward.normalized;

            Ray ray = new Ray (transform.position, camDir);
            float dist;
            if (groundPlane.Raycast(ray, out dist))
            {
                //dist /= 2;

                camDir.y = 0.0f;
                camDir.Normalize();

                Vector3 shift = -camDir * cameraTarget.targetShift;

                camLookPoint = ray.GetPoint(dist) + shift;
            }

            cameraTarget.targetCoords = new Vector2(camLookPoint.x, camLookPoint.z);
        }

        public void AutoMoveTo(GameObject go, Action<bool> onComplete = null)
        {
            var pos = go.transform.position;
            AutoMoveTo(pos.x, pos.z, onComplete);
        }

        public void AutoMoveTo(float x, float y, Action<bool> onComplete = null)
        {
            if (isAutoMove && !states.interruptAutomove)
                return;

            if (isCameraStashed)
                return;

            if (onCompleteMove != null)
                onCompleteMove(false);

            onCompleteMove = onComplete;

            isAutoMove = true;
            targetAutoMove = new Vector2(x, y);
        }

        /*
        public void SetHomePosition(float x, float y)
        {
            homePosition = new Vector2(x, y);
        }*/

        public void AutoRotateTo(float angle)
        {
            if (isAutoRotate && !states.interruptAutomove)
                return;

            isAutoRotate = true;
            targetAutoRotateAngle = angle;
        }

        public void AutoZoomTo(float value)
        {
            if (isAutoZoom && !states.interruptAutomove)
                return;

            isAutoZoom = true;
            targetAutoZoomValue = value;
        }

        public void SetTouchMoveMultiplier(float value)
        {
            cameraMove.touchMoveMultiplier = value;
        }

        public void SetRotationSpeed(float value)
        {
            cameraRotation.rotationSpeed = value;
        }

        public void SetZoomMultiplier(float value)
        {
            cameraZoom.zoomMultiplier = value;
        }

        public Vector3 GetWorldOffsetForNormalizedScreenPoint(Vector2 screenPoint)
        {
            float rayDistance1, rayDistance2;
            var ray1 = Camera.main.ScreenPointToRay(new Vector3(Screen.width * screenPoint.x, Screen.height * screenPoint.y, 0));
            var ray2 = Camera.main.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0));

            if (groundPlane.Raycast(ray1, out rayDistance1) && groundPlane.Raycast(ray2, out rayDistance2))
            {
                var touch1 = ray1.GetPoint(rayDistance1);
                var touch2 = ray2.GetPoint(rayDistance2);
                return (touch2 - touch1);
            }

            return Vector3.zero;
        }

        public Vector3 GetLookAtPoint()
        {
            return camLookPoint;
        }

        public void FocusCameraOnWorldPosition(Vector3 position)
        {
            if (selfCamera != null)
            {
                Vector2 screenSize = new Vector2 (Screen.width / 2.0f, Screen.height / 2.0f);

                Vector3 screenOfObject = selfCamera.WorldToScreenPoint (position);

                Vector2 vecFromCentre = new Vector2 (screenOfObject.x, screenOfObject.y) - screenSize;

                float normalDist = new Vector2(vecFromCentre.x / screenSize.x, vecFromCentre.y / screenSize.y).magnitude;

                if (normalDist > centrationRadius)
                {
                    var pos = position;

                    Vector3 look = GetLookAtPoint ();
                    Vector3 tVec = pos - look;
                    AutoMoveTo(look.x + tVec.x / 2.0f, look.z + tVec.z / 2.0f);
                }
            }
        }

        public int GetTouchesCount()
        {
            if (Input.mousePresent) //--- mouse
            {
                if (Input.GetMouseButtonDown(0))
                {
                    return 1;
                }
            }

            if (Input.touchSupported)
            {
                return Input.touchCount;
            }

            return 0;
        }

        /*
                public void CheckCollision()
                {
                    if (selfCollider != null)
                    {
                        selfCollider.enabled = false;
                        selfCollider.enabled = true; 
                    }
                }
        */

        public void Stop()
        {
            isAutoMove = false;
            isAutoRotate = false;
            isAutoZoom = false;
            isInertMove = false;
            isInertRot = false;
            isInertZoom = false;
        }

        #endregion

        #region Private functions.
        void Flush()
        {
            if (mainEventSystem != null && !mainEventSystem.enabled)
                mainEventSystem.enabled = true;

            moveDistance = 0.0f;
            totalMove = Vector3.zero;
            wasRotation = false;
            wasTouch = false;
            wasDualTouch = false;
            //inertionMove = Vector3.zero;
            //inertionRotate = 0;
            //inertionZoom = 0;
        }

        void UpdateCameraTouch()
        {
            bool isSingleTouch = false;
            bool isDualTouch = false;

            Vector3 moveVec = DetectCameraMove (out isSingleTouch, out isDualTouch);

            if (isDualTouch)
            {
                Stop();
                UpdateDualTouch();
            }
            else if (isSingleTouch && !wasDualTouch)
            {
                Stop();
                UpdateSingleTouch(moveVec.x, moveVec.z);
            }
            else
            {
                //--- Inertion
                if (wasDualTouch && !isAutoRotate && !isAutoZoom)
                {
                    //AutoZoomTo(curZoom + inertionZoom * cameraZoom.inertion);
                    //AutoRotateTo(curRotationAngle + inertionRotate * cameraRotation.inertion);
                    isInertRot = true;
                    isInertZoom = true;

                }
                else if (wasTouch && !isAutoMove)
                {
                    //AutoMoveTo(camLookPoint.x + inertionMove.x * cameraMove.inertion, camLookPoint.z + inertionMove.z * cameraMove.inertion);
                    isInertMove = true;
                }

                Flush();
                ReturnCamera();
            }

            //--- manual zoom
            if (curZoomInterpolator != cameraZoom.zoomInterpolator)
            {
                isAutoZoom = false;
                float newZoom = 0.0f;

                if (cameraZoom.zoomInterpolator > 0.5f)
                {
                    newZoom = cameraZoom.zoomOut * ((cameraZoom.zoomInterpolator - 0.5f) / 0.5f);
                }
                else if (cameraZoom.zoomInterpolator < 0.5f)
                {
                    newZoom = cameraZoom.zoomIn * (1.0f - cameraZoom.zoomInterpolator / 0.5f);
                } 

                float zoomValue = newZoom - curZoom;
                Zoom(zoomValue);

                //if (!isAutoRotate && !isAutoZoom)
                //    AutoZoomTo(curZoom - zoomValue * cameraZoom.inertion);
            }

            //--- manual Rotation
            if (wasRotationAngle != cameraRotation.rotationAngle)
            {
                float dA = cameraRotation.rotationAngle - wasRotationAngle;
                if (Mathf.Abs(dA) > 180.0f)
                    dA = (360 - Mathf.Abs(dA)) * -Mathf.Sign(dA);

                Rotate(dA);
            }

            curZoomInterpolator = cameraZoom.zoomInterpolator;
            wasRotationAngle = cameraRotation.rotationAngle;
        }

        Vector3 DetectCameraMove(out bool isSingleTouch, out bool isDualTouch)
        {
            Vector3 result = Vector3.zero;
            Vector3 curPosition = Vector3.zero;
            isSingleTouch = false;
            isDualTouch = false;
            int fingerId = 0;

            if (Input.mousePresent) //--- mouse
            {
                if (Input.GetMouseButtonDown(0))
                {
                    ignoreCameraMovement = IgnoreMovement(Input.mousePosition.x, Input.mousePosition.y);
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    ignoreCameraMovement = false;
                }

                if (!ignoreCameraMovement)
                {
                    isSingleTouch = Input.GetMouseButton(0);

                    if (isSingleTouch)
                    {
                        curPosition = Input.mousePosition;
                    }
                }

                //--- Zoom By Mouse Wheel
                cameraZoom.zoomInterpolator = Mathf.Clamp01(cameraZoom.zoomInterpolator + Input.GetAxis("Mouse ScrollWheel"));

            }

            if (Input.touchSupported)
            {
                RegisterTouches();

                if (Input.touchCount == 0)
                {
                    ignoreCameraMovement = false;
                }
                else if (Input.touchCount == 1)
                {
                    var zeroTouch = Input.GetTouch(0);

                    if (zeroTouch.phase == TouchPhase.Began && IgnoreMovement(zeroTouch.position.x, zeroTouch.position.y))
                    {
                        ignoreCameraMovement = true;
                    }

                    if (zeroTouch.phase == TouchPhase.Ended)
                    {
                        ignoreCameraMovement = true;
                    }

                    if (!ignoreCameraMovement)
                    {
                        isSingleTouch = true;
                        curPosition = zeroTouch.position;
                        fingerId = zeroTouch.fingerId;
                    }
                }

                isDualTouch = (Input.touchCount == 2);
            }

            if (isSingleTouch && !isDualTouch)
            {
                Vector3 groundTouch = Vector3.zero;

                Ray ray = Camera.main.ScreenPointToRay(curPosition);
                float rayDistance;
                if (groundPlane.Raycast(ray, out rayDistance))
                    groundTouch = ray.GetPoint(rayDistance);

                if (!wasTouch || fingerId != touches[0].id)
                {
                    wasGroundTouch = groundTouch;
                    totalMove = Vector3.zero;
                    //wasLookAt = camLookPoint;
                }
                else
                {
                    //isAutoMove = false;
                    //isAutoRotate = false;

                    result = wasGroundTouch - groundTouch;
                    wasGroundTouch = groundTouch + result;
                }

                touches[0].position = curPosition;
                touches[0].id = fingerId;
            }

            //--- Rotation By Right Mouse Button 
            if (Input.mousePresent && Input.GetButton("Fire2"))
            {
                isAutoRotate = false;

                float rotVal = Input.GetAxis("Mouse X") * cameraRotation.rotationSpeed / 3.0f;
                Rotate(rotVal);

                if (!isAutoRotate && !isAutoZoom)
                    AutoRotateTo(curRotationAngle + rotVal * cameraRotation.inertion);
            }


            return result;
        }

        bool IgnoreMovement(float x, float y)
        {
            var eventSystem = EventSystem.current;

            if (eventSystem != null && ignoreMovementOverLayers.value != 0)
            {
                var pointer = new PointerEventData(eventSystem);
                pointer.position = new Vector2(x, y);

                Assert.IsNotNull(ignoreCameraMovementHitsCache);
                eventSystem.RaycastAll(pointer, ignoreCameraMovementHitsCache);

                for (int k = 0; k < ignoreCameraMovementHitsCache.Count; k++)
                {
                    var hit = ignoreCameraMovementHitsCache[k];
                    if (hit.isValid && hit.gameObject != null && ((1 << hit.gameObject.layer) & ignoreMovementOverLayers.value) != 0)
                        return true;
                }
            }

            return false;
        }

        void ReturnCamera()
        {
            //--- restrictions

            const float threshold = 0.001f;

            Vector2 overRoomDelta = GetOverRoomDelta ();

            if (overRoomDelta.x > threshold || overRoomDelta.y > threshold)
            {
                if (overRoomDelta.x <= threshold)
                {
                    overRoomDelta.x = 0.0f;
                }
                else if (camLookPoint.x >= floorPosition.x)
                {
                    overRoomDelta.x *= -1.0f;
                }

                if (overRoomDelta.y <= threshold)
                {
                    overRoomDelta.y = 0.0f;
                }
                else if (camLookPoint.z >= floorPosition.y)
                {
                    overRoomDelta.y *= -1.0f;
                }

                Vector3 mVec = new Vector3(overRoomDelta.x * Time.deltaTime * cameraMove.returnSpeed, 0.0f, overRoomDelta.y * Time.deltaTime * cameraMove.returnSpeed);

                Move(mVec.x, mVec.z);
            }

        }

        Vector2 GetOverRoomDelta()
        {
            float dX = Mathf.Abs (camLookPoint.x - floorPosition.x) - (floorSize.x + cameraMove.roomSizePlus) / 2;
            float dY = Mathf.Abs (camLookPoint.z - floorPosition.y) - (floorSize.y + cameraMove.roomSizePlus) / 2;

            return new Vector2(dX, dY);
        }

        Vector2 GetOverRoomMoveFactor()
        {
            Vector2 overRoomDelta = GetOverRoomDelta ();

            if (overRoomDelta.x < 0) overRoomDelta.x = 0.0f;
            if (overRoomDelta.y < 0) overRoomDelta.y = 0.0f;

            Vector2 result = new Vector2 (0.0f, 0.0f);
            if (cameraMove.roomGap > 0.0f)
            {

                if (overRoomDelta.x < cameraMove.roomGap)
                    result.x = 1.0f - overRoomDelta.x / cameraMove.roomGap;
                if (overRoomDelta.y < cameraMove.roomGap)
                    result.y = 1.0f - overRoomDelta.y / cameraMove.roomGap;
            }
            else
            {
                result.x = overRoomDelta.x > 0.0f ? 0.0f : 1.0f;
                result.y = overRoomDelta.y > 0.0f ? 0.0f : 1.0f;
            }

            return result;
        }

        bool IsCanUpdateTouchCamera()
        {
            return !(states.dragEquip ||
                    states.windowOpened ||
                    states.expand ||
                    states.stickToPos
            /*||
                isAutoMove ||
                isAutoRotate*/
            );
        }

        bool IsMoveOutX(float val)
        {
            if (val > 0.0f)
            {
                return (camLookPoint.x > floorPosition.x);
            }

            return (camLookPoint.x < floorPosition.x);
        }

        bool IsMoveOutY(float val)
        {
            if (val > 0.0f)
            {
                return (camLookPoint.z > floorPosition.y);
            }

            return (camLookPoint.z < floorPosition.y);
        }

        void UpdateSingleTouch(float moveX, float moveY)
        {
            if (!wasTouch)
            {
                CalcLookAtPoint();
            }

            wasTouch = true;

            Vector3 moveVec = new Vector3 (moveX, 0.0f, moveY);

            //--- constraint move
            float roomRight = floorPosition.x + (floorSize.x + cameraMove.roomSizePlus) / 2 + cameraMove.roomGap;
            float roomLeft = floorPosition.x - (floorSize.x + cameraMove.roomSizePlus) / 2 - cameraMove.roomGap;
            float roomTop = floorPosition.y + (floorSize.y + cameraMove.roomSizePlus) / 2 + cameraMove.roomGap;
            float roomBottom = floorPosition.y - (floorSize.y + cameraMove.roomSizePlus) / 2 - cameraMove.roomGap;

            inertionMove = (Time.deltaTime < 1.0f) ? moveVec * (1.0f - Time.deltaTime) : Vector3.zero;

            if (camLookPoint.x + moveVec.x > roomRight)
            {
                //Debug.Log ("right: ");
                moveVec.x = roomRight - camLookPoint.x;
                inertionMove.x = 0;
            }

            if (camLookPoint.x + moveVec.x < roomLeft)
            {
                //Debug.Log ("left");
                moveVec.x = roomLeft - camLookPoint.x;
                inertionMove.x = 0;
            }

            if (camLookPoint.z + moveVec.z > roomTop)
            {
                //Debug.Log ("top");
                moveVec.z = roomTop - camLookPoint.z;
                inertionMove.z = 0;
            }

            if (camLookPoint.z + moveVec.z < roomBottom)
            {
                //Debug.Log ("bottom");
                moveVec.z = roomBottom - camLookPoint.z;
                inertionMove.z = 0;
            }

            Move(moveVec.x, moveVec.z);
            
        }

        void UpdateDualTouch()
        {
            if (mainEventSystem != null)
                mainEventSystem.enabled = false;

            if ((Input.GetTouch(0).fingerId != touches[0].id) || (Input.GetTouch(1).fingerId != touches[1].id))
                wasDualTouch = false;

            if (!wasDualTouch)
            {
                wasRotation = false;

                if (onCameraDualTouchEvent != null)
                    onCameraDualTouchEvent();
            }
            else
            {
                //--- zoom
                float wasDist = Vector2.Distance (touches [0].position, touches [1].position);
                float curDist = Vector2.Distance (Input.GetTouch (0).position, Input.GetTouch (1).position);

                float tMultiplier = cameraZoom.zoomMultiplier * GetDpiCoef ();

                float zoomValue = (curDist - wasDist) * Time.deltaTime * tMultiplier;
                Zoom(zoomValue);
                inertionZoom = Time.deltaTime < 1.0f ? zoomValue = zoomValue * (1.0f - Time.deltaTime) : 0;

                //--- rotation
                Vector2 vectorBetweenTouches = Input.GetTouch (1).position - Input.GetTouch (0).position;
                Vector2 wasVectorBetweenTouches = touches [1].position - touches [0].position;

                Vector3 tV1 = new Vector3(vectorBetweenTouches.x, 0.0f, vectorBetweenTouches.y);
                Vector3 tV2 = new Vector3(wasVectorBetweenTouches.x, 0.0f, wasVectorBetweenTouches.y);

                if (tV1 != tV2)
                {
                    Vector3 rotateAxis = Vector3.Cross(tV1, tV2);

                    float angleBetweenTouches = Mathf.Asin(rotateAxis.magnitude / (tV1.magnitude * tV2.magnitude) );

                    if (angleBetweenTouches > 0.0009f || wasRotation)
                    {
                        rotateAxis.Normalize();
                        float rotateValue = angleBetweenTouches * Mathf.Rad2Deg * Time.deltaTime * cameraRotation.rotationSpeed;

                        float rotVal = rotateValue * Mathf.Sign(rotateAxis.y);
                        Rotate(rotVal);

                        inertionRotate = rotVal;

                        wasRotation = true;
                    }
                    else
                    {
                        wasRotation = false;
                    }
                }
            }

            touches[0].position = Input.GetTouch(0).position;
            touches[0].id = Input.GetTouch(0).fingerId;

            touches[1].position = Input.GetTouch(1).position;
            touches[1].id = Input.GetTouch(1).fingerId;

            wasDualTouch = true;
        }

        void Move(float x, float y)
        {
            Vector3 moveVec = new Vector3 (x, 0.0f, y);
            transform.Translate(moveVec, Space.World);
            camLookPoint += moveVec;

            totalMove += moveVec;
            moveDistance = totalMove.magnitude;

            if (onCameraMoveEvent != null)
                onCameraMoveEvent(moveDistance, moveDistance > cameraMove.moveThreshold);

            //Debug.LogFormat("Move distance: {0}", moveDistance);
            if (moveDistance > cameraMove.moveThreshold && wasTouch == true)
            {
                if (mainEventSystem != null)
                    mainEventSystem.enabled = false;

                isAutoMove = false;
                isAutoRotate = false;
                isAutoZoom = false;
            }
        }

        void Zoom(float value)
        {
            if (curZoom + value > cameraZoom.zoomIn)
                value = cameraZoom.zoomIn - curZoom;

            if (curZoom + value < cameraZoom.zoomOut)
                value = cameraZoom.zoomOut - curZoom;

            transform.Translate(new Vector3(0.0f, 0.0f, value));

            curZoom += value;

            if (onCameraZoomEvent != null)
                onCameraZoomEvent((camLookPoint - transform.position).magnitude);

            RotateForZoom();
        }

        void Rotate(float value)
        {
            Vector3 rotateAxis = new Vector3 (0.0f, 1.0f, 0.0f);
            transform.RotateAround(camLookPoint, rotateAxis, value);
            defaultAzimutAxis = Quaternion.Euler(0, value, 0) * defaultAzimutAxis;

            curRotationAngle += value;

            if (curRotationAngle >= 360)
                curRotationAngle -= 360;

            if (curRotationAngle < 0)
                curRotationAngle += 360;
        }

        void UpdateAutoMove()
        {
            Vector3 targetVec = new Vector3 (targetAutoMove.x, 0.0f, targetAutoMove.y);
            Vector3 dir = targetVec - camLookPoint;

            float dist = dir.magnitude;
            if (dist < 0.1f)
            {
                isAutoMove = false;

                if (onCompleteMove != null)
                {
                    onCompleteMove(true);
                    onCompleteMove = null;
                }
            }
            else
            {
                float moveCount = dist * Time.deltaTime * cameraMove.autoMoveMultiplier;
                dir.Normalize();

                if (moveCount >= dist)
                {
                    Move(dir.x * dist, dir.z * dist);
                    isAutoMove = false;
                }
                else
                {
                    Move(dir.x * moveCount, dir.z * moveCount);
                }

            }
        }

        void UpdateAutoRotate()
        {
            float dA = targetAutoRotateAngle - curRotationAngle;
            if (Mathf.Abs(dA) > 180.0f)
                dA = (360 - Mathf.Abs(dA)) * -Mathf.Sign(dA);

            if (Mathf.Abs(dA) < 0.0001f)
            {
                isAutoRotate = false;
            }
            else
            {
                float rotateCount = dA * Time.deltaTime * cameraRotation.autoRotationMultiplier;

                if (Mathf.Abs(rotateCount) >= Mathf.Abs(dA))
                {
                    Rotate(dA);
                    isAutoRotate = false;
                }
                else
                {
                    Rotate(rotateCount);
                }
            }
        }

        void UpdateAutoZoom()
        {
            float dist = Mathf.Abs(curZoom - targetAutoZoomValue);
            if (Mathf.Abs(dist) < 0.01f)
            {
                isAutoZoom = false;
            }
            else
            {
                float moveCount = dist * Time.deltaTime * cameraZoom.autoZoomSpeed;

                if (moveCount >= dist)
                {
                    Zoom(moveCount * Mathf.Sign(targetAutoZoomValue - curZoom));
                    isAutoZoom = false;
                }
                else
                {
                    Zoom(moveCount * Mathf.Sign(targetAutoZoomValue - curZoom));
                }

            }
        }

        void RegisterTouches()
        {
            if (Input.touchCount == 1)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    touches[0].id = -1;
                }
            }
            else if (Input.touchCount == 2)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    touches[0].id = -1;
                }

                if (Input.GetTouch(1).phase == TouchPhase.Began)
                {
                    touches[1].id = -1;
                }
            }
        }

        void RotateForZoom()
        {
            if (curZoom >= 0)
                return;

            float zoomCoef = curZoom / cameraZoom.zoomOut;
            double angle = defaultAzimut + (1.0 - defaultAzimut) * zoomCoef;
            double dA = angle - wasAzimut;
            wasAzimut = angle;

            Vector3 floorToCamVector = transform.position - camLookPoint;
            Vector3 floorVector = floorToCamVector; floorVector.y = 0.0f;

            float tangle = (float)dA * Mathf.Rad2Deg;

            transform.RotateAround(camLookPoint, defaultAzimutAxis, tangle);

        }

        double GetAzimut()
        {
            Vector3 floorToCamVector = transform.position - camLookPoint;
            Vector3 floorVector = floorToCamVector;
            floorVector.y = 0.0f;
            Vector3 tAxis = Vector3.Cross(floorToCamVector, floorVector);
            return Math.Asin(tAxis.magnitude / (floorToCamVector.magnitude * floorVector.magnitude));
        }

        void InertialMove()
        {
            Move(inertionMove.x, inertionMove.z);
            inertionMove *= cameraMove.inertion;

            if (inertionMove.magnitude < 0.0001f)
                isInertMove = false;
        }

        void InertialRotate()
        {
            Rotate(inertionRotate);
            inertionRotate *= cameraRotation.inertion;

            if (Mathf.Abs(inertionRotate) < 0.01f)
                isInertRot = false;
        }

        void InertialZoom()
        {
            Zoom(inertionZoom);
            inertionZoom *= cameraZoom.inertion;

            if (Mathf.Abs(inertionZoom) < 0.01f)
                isInertZoom = false;
        }   

        #endregion
    }
}
