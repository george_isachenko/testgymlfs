﻿using Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace View
{
    [RequireComponent(typeof(AudioSource))]
    public class Sound : MonoBehaviour
    {
        static readonly string musicSettingsProperty = "MusicEnabled";
        static readonly string soundSettingsProperty = "SoundEnabled";

        public static Sound instance;

        public bool musicOn { set { MusicOn(value); } get { return IsMusicOn(); } }
        public bool soundOn { set { SoundOn(value); } get { return soundEnabled; } }

        AudioSource[]   source;
        int             lastSourceIdx = 0;
        AudioSource     musicSource;

        public AudioClip windowOpen;
        public AudioClip claim;
        public AudioClip newVisitor;
        public AudioClip levelUp;
        public AudioClip visitorSelection;
        public AudioClip equipSelection;

        bool soundEnabled = false;

        void Awake ()
        {
            Assert.IsNull(instance);
            instance = this;

            source = GetComponents<AudioSource>();
            Assert.IsNotNull(source);
            Assert.IsTrue(source.Length == 3);

            musicSource = transform.Find("Music").GetComponent<AudioSource>();
        }

        void Start ()
        {
            MusicOn(Settings.GetBool(musicSettingsProperty, true));
            soundEnabled = Settings.GetBool(soundSettingsProperty, true);
        }

        public void WindowOpen (float volumeScale = 1.0f)
        {
            PlayOneShot(windowOpen, volumeScale);
        }

        public void Claim (float volumeScale = 1.0f)
        {
            PlayOneShot(claim, volumeScale);
        }

        public void NewVisitor (float volumeScale = 1.0f)
        {
            PlayOneShot(newVisitor, volumeScale);
        }

        public void LevelUp (float volumeScale = 1.0f)
        {
            PlayOneShot(levelUp, volumeScale);
        }

        public void VisitorSelect (float volumeScale = 1.0f)
        {
            PlayOneShot(visitorSelection, volumeScale);
        }

        public void EquipmentSelect (float volumeScale = 1.0f)
        {
            PlayOneShot(equipSelection, volumeScale);
        }

        void PlayOneShot (AudioClip clip, float volumeScale = 1.0f)
        {
            if (!soundOn)
                return;
            if (clip == null)
            {
                Debug.LogWarning("Trying to play NULL clip");
                return;
            }

            lastSourceIdx++;
            lastSourceIdx %= 3;

            // source[lastSourceIdx].clip = clip;
            source[lastSourceIdx].PlayOneShot(clip, volumeScale);
        }

        void Play(AudioClip clip)
        {
            if (!soundOn)
                return;
            if (clip == null)
            {
                Debug.LogWarning("Trying to play NULL clip");
                return;
            }

            lastSourceIdx++;
            lastSourceIdx %= 3;

            source[lastSourceIdx].clip = clip;
            source[lastSourceIdx].Play();
        }

        private void MusicOn (bool value)
        {
            if (musicSource != null)
            {
                if (value)
                    musicSource.Play();
                else
                    musicSource.Stop();

            }
            Settings.Set(musicSettingsProperty, value);
        }

        private bool IsMusicOn ()
        {
            if (musicSource != null)
            {
                return musicSource.isPlaying;
            }

            return false;
        }

        void SoundOn (bool value)
        {
            if (value != soundEnabled)
            {
                soundEnabled = value;
                Settings.Set(soundSettingsProperty, value);
            }
        }
    }
}