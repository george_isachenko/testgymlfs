﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Logic.Quests;
using UnityEngine.Events;
using Data;
using System.Collections.Generic;
using View.UI.Base;
using Logic.Quests.Tutorial;

namespace View.UI
{    
    public class ShopUltimateView : UIBaseView
    {
        private bool firstUpdate = true;

        public UIGenericView        categoriesView      {private set; get;}
        public UIGenericView        itemsView           {private set; get;}
        public ShopUltimateBankView bankCoinsView       {private set; get;}
        public ShopUltimateBankView bankBucksView       {private set; get;}
        public ShopUltimateViewList list                {private set; get;}
        public UnityEvent onItemsShow = new UnityEvent();

        public int                  coins               { set { genericView.SetText("txtCoins", value.ToString()); } }
        public int                  bucks               { set { genericView.SetText("txtBucks", value.ToString()); } }
        public Action               OnAddCoins          { set { genericView.SetButtonCallback("btnAddCoins", value); } }
        public Action               OnAddFitBucks       { set { genericView.SetButtonCallback("btnAddBucks", value); } }

        public bool                 btnBackOn           { set { genericView.SetActive("btnBack", value); } private get { return genericView.GetActive("btnBack"); } }

        public Action               btnBackCallback     { set { genericView.SetButtonCallback("btnBack", value); } }
        public bool                 lockSportFeatures   { set { LockSportFeatures(value); } }
        public bool                 lockDecors          { set { LockDecors(value); } }
        public int                  lockDecorsLevel     { set { categoriesView.SetText("txtUnlockDecorLevel", value.ToString()); } }

        public bool                 newEquipOn          { set { categoriesView.SetActive("newEquip", value); } }
        public bool                 newDecorOn          { set { categoriesView.SetActive("newDecor", value); } }
        public bool                 newInteriorOn       { set { categoriesView.SetActive("newInterior", value); } }
        public bool                 newSportEquipOn     { set { categoriesView.SetActive("newSportEquip", value); } }
        public bool                 newSportResOn       { set { categoriesView.SetActive("newSportRes", value); } }

        public bool                 isItemsVisible      { get { return itemsView.gameObject.activeSelf; } }
        
        [HideInInspector] public OnLiveUpdate_Delegate  OnLiveUpdate_Callback;
        [HideInInspector] public Action                 OnShowBankCoins_Callback;
        [HideInInspector] public Action                 OnShowBankBucks_Callback;
        [HideInInspector] public Action<UltimateShopItemType>   ShowCategory_Callback;

        private readonly List<ShopUltimateViewList> lists = new List<ShopUltimateViewList>();
        private ScrollRect scrollRect;

        void Awake()
        {
            genericView.SetButtonCallback("btnClose", HideInternallyByUserClick);

            itemsView = genericView.GetSubView("items");
            categoriesView = genericView.GetSubView("categories");
            bankCoinsView = genericView.GetComponent<ShopUltimateBankView>("bankCoins");
            bankBucksView = genericView.GetComponent<ShopUltimateBankView>("bankBucks");

            bankCoinsView.gameObject.SetActive(true);
            bankBucksView.gameObject.SetActive(true);

            scrollRect = genericView.GetComponent<ScrollRect>("items");
            list = itemsView.GetComponent<ShopUltimateViewList>("items");
            list.itemsRoot = list.transform;

            InitLists();
        }

        void InitLists()
        {
            Rename(list, 1);
            lists.Add(list);


            for (int i = 2; i < 6; i++)
            {
                var obj = Instantiate(list.gameObject);
                obj.transform.SetParent(list.transform.parent, false);
                var objList = obj.GetComponent<ShopUltimateViewList>();
                Rename(objList, i);
                lists.Add(objList);
            }
        }

        public void UpdateWindowNameForCategory(UltimateShopItemType value)
        {
            var windowName = Loc.Get("shopHeader");
            switch (value)
            {
                case UltimateShopItemType.Equip:
                    windowName = Loc.Get("shopTabEquipment");
                    break;
                case UltimateShopItemType.Interior:
                    windowName = Loc.Get("shopTabInterior");
                    break;
                case UltimateShopItemType.Decor:
                    windowName = Loc.Get("shopTabDecor");
                    break;
                case UltimateShopItemType.SportEquip:
                    windowName = Loc.Get("shopTabProEquip");
                    break;
                case UltimateShopItemType.SportResources:
                    windowName = Loc.Get("shopTabSportResources");
                    break;
                case UltimateShopItemType.Bank:
                    windowName = Loc.Get("bankHeader");
                    break;
            }
            genericView.SetText("shopNameLabel", windowName);
        }

        public void SwitchTo(UltimateShopItemType category)
        {
            switch (category)
            {
                case UltimateShopItemType.Equip:
                    list = lists[0];
                    break;

                case UltimateShopItemType.Decor:
                    list = lists[1];
                    break;

                case UltimateShopItemType.Interior:
                    list = lists[2];
                    break;

                case UltimateShopItemType.SportEquip:
                    list = lists[3];
                    break;

                case UltimateShopItemType.SportResources:
                    list = lists[4];
                    break;

                default:
                    Debug.LogError("Wrong Category for shop view SwitchTo");
                    break;
            }
            
            scrollRect.content = list.transform as RectTransform;

            foreach (var item in lists)
            {
                item.gameObject.SetActive(item == list);
            }
        }

        void Rename(ShopUltimateViewList list, int idx)
        {
            list.gameObject.name = "items_" + idx;
            var genericItem = list.gameObject.GetComponent<UIGenericViewItem>();
            genericItem.alias = list.gameObject.name;
        }

        public void ScrollToBegin()
        {
            //itemsView.gameObject.GetComponentInChildren<ScrollRect>().horizontalNormalizedPosition = 0;
            var pos = ((RectTransform) list.transform).anchoredPosition;
            pos.x = 0;
            ((RectTransform) list.transform).anchoredPosition = pos;
        }

        public void ShowCategories()
        {
            genericView.IsolateSubView("categories");
        }

        public void ShowCategory(UltimateShopItemType category)
        {
            if (gameObject.activeSelf == false)
                Show();

            ShowCategory_Callback(category);
        }

        public void ShowItems()
        {
            genericView.IsolateSubView("items");
            onItemsShow.Invoke();
        }

        public void ShowBankCoins()
        {
            genericView.IsolateSubView("bankCoins");
            if (OnShowBankCoins_Callback != null)
                OnShowBankCoins_Callback.Invoke();
            else
                Debug.LogError("OnShowCoinsShop_Callback is null");
        }

        public void ShowBankBucks()
        {
            genericView.IsolateSubView("bankBucks");
            if (OnShowBankBucks_Callback != null)
                OnShowBankBucks_Callback.Invoke();
            else
                Debug.LogError("OnShowBankBucks_Callback is null");
        }


        private void UpdateConstraints()
        {
            // == утвержденные данные по аспектам ==
            //  aspect      w       h   colls   cell aspect 
            //  1,25        5       4   2       460 width
            //  1,333333333 4       3   2       460 width
            //  1,5         3       2   2       460 width
            //  1,6         16      10  1       1.80 aspect
            //  1,777777778 16      9   1       1.75 aspect

            var gridLayoutGroup = list.GetComponent<GridLayoutGroup>();

            var screenRatio = (float) Screen.width/(float) Screen.height;
            var rowsCount = screenRatio < 1.45f ? 2 : 1;

            var rowVerticalSpace = ((RectTransform) gridLayoutGroup.transform).rect.height - gridLayoutGroup.padding.vertical - (rowsCount > 1 ? gridLayoutGroup.spacing.y : 0);


            var cellSize = gridLayoutGroup.cellSize;
            cellSize.y = rowVerticalSpace/rowsCount;
            cellSize.x = screenRatio < 1.55f ? 460 : (screenRatio < 1.7f ? cellSize.y/1.8f : cellSize.y/1.75f);

            foreach (var item in lists)
            {
                gridLayoutGroup = item.GetComponent<GridLayoutGroup>();
                gridLayoutGroup.constraintCount = rowsCount;
                gridLayoutGroup.cellSize = cellSize;
            }
        }

        public void LockSportFeatures(bool value)
        {
            categoriesView.SetActive("lockSportEquip", value);
            categoriesView.SetActive("lockSportResources", value);
        }

        public void LockDecors(bool value)
        {
            categoriesView.SetActive("lockDecors", value);
        }

        void Update()
        {
            if (firstUpdate)
            {
                UpdateConstraints();
                firstUpdate = false;
            }
            if (OnLiveUpdate_Callback != null)
                OnLiveUpdate_Callback.Invoke();
        }

        public override void HideByUserClick()
        {
            if (btnBackOn)
            {
                if (OnUpdateContentOnShow_Callback != null)
                    OnUpdateContentOnShow_Callback();
            }
            else
            {
                HideInternallyByUserClick();
            }
        }

        private void HideInternallyByUserClick()
        {
            base.HideByUserClick();
        }

        public void ScrollToPosition(float normalizedPosition)
        {
            itemsView.gameObject.GetComponentInChildren<ScrollRect>().horizontalNormalizedPosition = normalizedPosition;
        }

        public void LockItemsScrolling(bool isLocked)
        {
            itemsView.gameObject.GetComponent<ScrollRect>().horizontal = !isLocked;
        }

        public override void Show(bool show)
        {
            if (show)
            {
                if (OnShow_WndManager != null)
                    OnShow_WndManager.Invoke();
                else
                    Debug.LogError("WindowManager : onShow == null");

            }
            else
            {
                if (OnHide_WndManager != null)
                    OnHide_WndManager.Invoke();
                else
                    Debug.LogError("WindowManager : onHide == null");
            }
        }
    }
}
